<section class="engine"><a rel="external" href="https://nuesto.id">nuesto.id</a></section>
<section class="mbr-section mbr-section-hero mbr-section-full header2 mbr-parallax-background mbr-after-navbar" id="header6-3" style="background-image: url({{ url:site }}{{ theme:path }}/img/room-laptop.jpg);">

    <div class="mbr-overlay" style="opacity: 0.9; background-color: rgb(204, 204, 204);">
    </div>

    <div class="mbr-table mbr-table-full">
        <div class="mbr-table-cell">

            <div class="container">
                <div class="mbr-section row">
                    <div class="mbr-table-md-up">

                        <div class="mbr-table-cell mbr-right-padding-md-up mbr-valign-top col-md-7 image-size" style="width: 50%;">
                            <div class="mbr-figure"><iframe class="mbr-embedded-video" src="https://www.youtube.com/embed/Tq7p44IA-1U?rel=0&amp;amp;showinfo=0&amp;autoplay=0&amp;loop=0" width="1280" height="720" frameborder="0" allowfullscreen></iframe></div>
                        </div>
                        <div class="mbr-table-cell col-md-5 text-xs-center text-md-left content-size">

                            <h3 class="mbr-section-title display-2">INTRO WITH YOUTUBE VIDEO</h3>

                            <div class="mbr-section-text">
                                <p>Full screen intro with youtube video.<br>Mobirise helps you cut down development time by providing you with a flexible website editor.</p>
                            </div>

                            <div class="mbr-section-btn"><a class="btn btn-lg btn-black-outline btn-black" href="https://nuesto.id">MORE</a></div>

                        </div>




                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="mbr-arrow mbr-arrow-floating hidden-sm-down" aria-hidden="true"><a href="#features4-4"><i class="mbr-arrow-icon"></i></a></div>

</section>

<section class="mbr-cards mbr-section mbr-section-nopadding" id="features4-4" style="background-color: rgb(255, 255, 255);">



    <div class="mbr-cards-row row">
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img iconbox">
                        <a href="https://nuesto.id" class="etl-icon icon-phone mbr-iconfont mbr-iconfont-features4" style="color: black;"></a>
                    </div>
                    <div class="card-block">
                        <h4 class="card-title">Bootstrap 4</h4>
                        <h5 class="card-subtitle">Bootstrap 4 has been noted</h5>
                        <p class="card-text">Bootstrap 4 has been noted as one of the most reliable and proven frameworks and Mobirise has been equipped to develop websites using this framework.</p>
                        <div class="card-btn"><a href="https://nuesto.id" class="btn btn-primary">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img iconbox">
                        <a href="https://nuesto.id" class="etl-icon icon-edit mbr-iconfont mbr-iconfont-features4" style="color: black;"></a>
                    </div>
                    <div class="card-block">
                        <h4 class="card-title">Responsive</h4>
                        <h5 class="card-subtitle">One of Bootstrap 4's big points</h5>
                        <p class="card-text">One of Bootstrap 4's big points is responsiveness and Mobirise makes effective use of this by generating highly responsive website for you.</p>
                        <div class="card-btn"><a href="https://nuesto.id" class="btn btn-primary">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img iconbox">
                        <a href="https://nuesto.id" class="etl-icon icon-briefcase mbr-iconfont mbr-iconfont-features4" style="color: black;"></a>
                    </div>
                    <div class="card-block">
                        <h4 class="card-title">Web Fonts</h4>
                        <h5 class="card-subtitle">Google has a highly exhaustive list of fonts</h5>
                        <p class="card-text">Google has a highly exhaustive list of fonts compiled into its web font platform and Mobirise makes it easy for you to use them on your website easily and freely.</p>
                        <div class="card-btn"><a href="https://nuesto.id" class="btn btn-primary">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mbr-cards-col col-xs-12 col-lg-3" style="padding-top: 80px; padding-bottom: 80px;">
            <div class="container">
                <div class="card cart-block">
                    <div class="card-img iconbox">
                        <a href="https://nuesto.id" class="etl-icon icon-linegraph mbr-iconfont mbr-iconfont-features4" style="color: black;"></a>
                    </div>
                    <div class="card-block">
                        <h4 class="card-title">Unlimited Sites</h4>
                        <h5 class="card-subtitle">Mobirise gives you the freedom to develop</h5>
                        <p class="card-text">Mobirise gives you the freedom to develop as many websites as you like given the fact that it is a desktop app.</p>
                        <div class="card-btn"><a href="https://nuesto.id" class="btn btn-primary">MORE</a></div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>

<section class="mbr-section" id="pricing-table1-8" style="background-color: rgb(48, 48, 48); padding-top: 120px; padding-bottom: 120px;">



    <div class="mbr-section mbr-section__container mbr-section__container--middle">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-xs-center">
                    <h3 class="mbr-section-title display-2">PRICING TABLE</h3>
                    <small class="mbr-section-subtitle">Pricing table with four columns and solid color background.</small>
                </div>
            </div>
        </div>
    </div>

    <div class="mbr-section mbr-section-nopadding mbr-price-table">
        <div class="row">
            <div class="col-xs-12  col-md-6 col-xl-3">
                <div class="mbr-plan card text-xs-center">
                    <div class="mbr-plan-header card-block">

                        <div class="card-title">
                            <h3 class="mbr-plan-title">STANDARD</h3>
                            <small class="mbr-plan-subtitle">Description</small>
                        </div>
                        <div class="card-text">
                            <div class="mbr-price">
                                <span class="mbr-price-value">$</span>
                                <span class="mbr-price-figure">0</span><small class="mbr-price-term">/mo.</small>
                            </div>
                            <small class="mbr-plan-price-desc">Mobirise is perfect for non-techies who are not familiar with web development.</small>
                        </div>
                    </div>
                    <div class="mbr-plan-body card-block">
                        <div class="mbr-plan-list">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">32 GB storage</li>
                                <li class="list-group-item">Unlimited users</li>
                                <li class="list-group-item">15 GB bandwidth</li>
                            </ul>
                        </div>
                        <div class="mbr-plan-btn"><a href="https://nuesto.id" class="btn btn-white btn-white-outline">DEMO</a></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12  col-md-6 col-xl-3">
                <div class="mbr-plan card text-xs-center">
                    <div class="mbr-plan-header card-block bg-primary">
                        <div class="mbr-plan-label">HOT!</div>
                        <div class="card-title">
                            <h3 class="mbr-plan-title">BUSINESS</h3>
                            <small class="mbr-plan-subtitle">Description</small>
                        </div>
                        <div class="card-text">
                            <div class="mbr-price">
                                <span class="mbr-price-value">$</span>
                                <span class="mbr-price-figure">0</span><small class="mbr-price-term">/mo.</small>
                            </div>
                            <small class="mbr-plan-price-desc">Mobirise is perfect for non-techies who are not familiar with web development.</small>
                        </div>
                    </div>
                    <div class="mbr-plan-body card-block">
                        <div class="mbr-plan-list">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">32 GB storage</li>
                                <li class="list-group-item">Unlimited users</li>
                                <li class="list-group-item">15 GB bandwidth</li>
                            </ul>
                        </div>
                        <div class="mbr-plan-btn"><a href="https://nuesto.id" class="btn btn-white btn-white-outline">DEMO</a></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12  col-md-6 col-xl-3">
                <div class="mbr-plan card text-xs-center">
                    <div class="mbr-plan-header card-block">

                        <div class="card-title">
                            <h3 class="mbr-plan-title">PREMIUM</h3>
                            <small class="mbr-plan-subtitle">Description</small>
                        </div>
                        <div class="card-text">
                            <div class="mbr-price">
                                <span class="mbr-price-value">$</span>
                                <span class="mbr-price-figure">0</span><small class="mbr-price-term">/mo.</small>
                            </div>
                            <small class="mbr-plan-price-desc">Mobirise is perfect for non-techies who are not familiar with web development.</small>
                        </div>
                    </div>
                    <div class="mbr-plan-body card-block">
                        <div class="mbr-plan-list">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">32 GB storage</li>
                                <li class="list-group-item">Unlimited users</li>
                                <li class="list-group-item">15 GB bandwidth</li>
                            </ul>
                        </div>
                        <div class="mbr-plan-btn"><a href="https://nuesto.id" class="btn btn-white btn-white-outline">DEMO</a></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12  col-md-6 col-xl-3">
                <div class="mbr-plan card text-xs-center">
                    <div class="mbr-plan-header card-block">

                        <div class="card-title">
                            <h3 class="mbr-plan-title">ULTIMATE</h3>
                            <small class="mbr-plan-subtitle">Description</small>
                        </div>
                        <div class="card-text">
                            <div class="mbr-price">
                                <span class="mbr-price-value">$</span>
                                <span class="mbr-price-figure">0</span><small class="mbr-price-term">/mo.</small>
                            </div>
                            <small class="mbr-plan-price-desc">Mobirise is perfect for non-techies who are not familiar with web development.</small>
                        </div>
                    </div>
                    <div class="mbr-plan-body card-block">
                        <div class="mbr-plan-list">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">32 GB storage</li>
                                <li class="list-group-item">Unlimited users</li>
                                <li class="list-group-item">15 GB bandwidth</li>
                            </ul>
                        </div>
                        <div class="mbr-plan-btn"><a href="https://nuesto.id" class="btn btn-white btn-white-outline">DEMO</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="mbr-section" id="testimonials1-7" style="background-color: rgb(255, 255, 255); padding-top: 120px; padding-bottom: 120px;">



    <div class="mbr-section mbr-section__container mbr-section__container--middle">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-xs-center">
                    <h3 class="mbr-section-title display-2">WHAT OUR FANTASTIC USERS SAY</h3>
                    <small class="mbr-section-subtitle">Shape your future web project with sharp design and refine coded functions.</small>
                </div>
            </div>
        </div>
    </div>


    <div class="mbr-testimonials mbr-section mbr-section-nopadding">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-lg-4">

                    <div class="mbr-testimonial card mbr-testimonial-lg">
                        <div class="card-block">
                            <p>“its really very amazing app that makes me finish html page in 3 minutes ( that's usually takes more than 1 hours at least from me if i did it from scratch). i hope to have very big library and plugins for this APP thanks
                                again for your nice application”</p>
                        </div>
                        <div class="mbr-author card-footer">
                            <div class="mbr-author-img">{{ theme:image file="face3.jpg" class="img-circle" }}</div>
                            <div class="mbr-author-name">Abanoub S.</div>
                            <small class="mbr-author-desc">User</small>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-4">

                    <div class="mbr-testimonial card mbr-testimonial-lg">
                        <div class="card-block">
                            <p>“First of all hands off to you guys for your effort and nice, super tool. Good work mobirise team. We are expecting the new version soon with advance functionality with full bootstrap design. Great effort and super UI experience
                                with easy drag &amp; drop with no time design bootstrap builder in present web design world.”</p>
                        </div>
                        <div class="mbr-author card-footer">
                            <div class="mbr-author-img">{{ theme:image file="face1.jpg" class="img-circle" }}</div>
                            <div class="mbr-author-name">Suffian A.</div>
                            <small class="mbr-author-desc">User</small>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-4">

                    <div class="mbr-testimonial card mbr-testimonial-lg">
                        <div class="card-block">
                            <p>“At first view, looks like a nice innovative tool, i like the great focus and time that was given to the responsive design, i also like the simple and clear drag and drop features. Give me more control over the object's
                                properties and ill be using this tool for more serious projects. Regards.”</p>
                        </div>
                        <div class="mbr-author card-footer">
                            <div class="mbr-author-img">{{ theme:image file="face2.jpg" class="img-circle" }}</div>
                            <div class="mbr-author-name">Jhollman C.</div>
                            <small class="mbr-author-desc">User</small>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

</section>