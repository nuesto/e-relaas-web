<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_Base extends Theme
{
	public $name            = 'Base';
	public $author          = 'Aditya Satrya';
	public $description     = 'An basic theme for your project.';
	public $version         = '1.0';

}
/* End of file theme.php */