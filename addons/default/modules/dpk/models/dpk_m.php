<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Dpk model
 *
 * @author Aditya Satrya
 */
class Dpk_m extends MY_Model {
	
	public function get_dpk($pagination_config = NULL, $params = array())
	{
		$this->db->select('default_dpk_dpk.*');
		$this->db->select('default_dpk_batch_process.tanggal_data');
		$this->db->select('default_dpk_batch_process.created_by');
		$this->db->select('default_nasabah_nasabah.nama as nama_nasabah');

		$this->db->join('default_dpk_batch_process', 'default_dpk_batch_process.id = default_dpk_dpk.id_batch_process');
		$this->db->join('default_nasabah_nasabah', 'default_nasabah_nasabah.id = default_dpk_dpk.id_nasabah');
		
		foreach ($params as $key => $param) {
			$this->db->$param['function']($param['column'], $param['value']);
		}

		$this->db->order_by("tanggal_data", "desc");
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_dpk_dpk');

		$result = $query->result_array();
		
        return $result;
	}

	public function count_dpk_result($params = array())
	{
		$this->db->select('default_dpk_dpk.*');
		$this->db->select('default_dpk_batch_process.tanggal_data');
		$this->db->select('default_dpk_batch_process.created_by');
		$this->db->select('default_nasabah_nasabah.nama as nama_nasabah');

		$this->db->join('default_dpk_batch_process', 'default_dpk_batch_process.id = default_dpk_dpk.id_batch_process');
		$this->db->join('default_nasabah_nasabah', 'default_nasabah_nasabah.id = default_dpk_dpk.id_nasabah');
		
		foreach ($params as $key => $param) {
			$this->db->$param['function']($param['column'], $param['value']);
		}

		$this->db->order_by("tanggal_data", "desc");
		
		$result = $this->db->count_all_results('default_dpk_dpk');
		
        return $result;
	}
	
	public function get_dpk_by_id($id)
	{
		$this->db->select('default_dpk_dpk.*, default_dpk_batch_process.created_by');
		$this->db->select('default_nasabah_nasabah.nama as nama_nasabah');
		$this->db->join('default_dpk_batch_process', 'default_dpk_batch_process.id = default_dpk_dpk.id_batch_process');
		$this->db->join('default_nasabah_nasabah', 'default_nasabah_nasabah.id = default_dpk_dpk.id_nasabah');
		$this->db->where('default_dpk_dpk.id', $id);
		$query = $this->db->get('default_dpk_dpk');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_dpk()
	{
		return $this->db->count_all('dpk_dpk');
	}
	
	public function delete_dpk_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_dpk_dpk');
	}

	public function delete_dpk_by_id_batch_process($id_batch_process){
		$this->db->where('id_batch_process', $id_batch_process);
		$this->db->delete('default_dpk_dpk');	
	}
	
	public function insert_dpk($values)
	{	
		return $this->db->insert('default_dpk_dpk', $values);
	}


	public function insert_batch_dpk($values)
	{
		return $this->db->insert_batch('default_dpk_dpk', $values);
	}
	
	public function update_dpk($values, $row_id)
	{	
		$this->db->where('id', $row_id);
		return $this->db->update('default_dpk_dpk', $values); 
	}

	public function get_dpk_by_nasabah($id_nasabah){
		$this->db->select('*');
		$this->db->select('SUM(saldo_eq_idr) as total_ccy');
		$this->db->where('id_nasabah', $id_nasabah);
		$this->db->group_by(array('ccy','produk'));
		$query = $this->db->get('default_dpk_dpk');

		$result = $query->result_array();
		return $result;
	}

	public function get_history_dpk_by_nasabah($id_nasabah){
		$this->db->select('tanggal_data, ccy');
		$this->db->select('SUM(saldo_eq_idr) as total_ccy');
		$this->db->join('default_dpk_batch_process', 'default_dpk_batch_process.id = default_dpk_dpk.id_batch_process');
		$this->db->where('id_nasabah', $id_nasabah);
		$this->db->group_by(array('ccy','tanggal_data'));
		$this->db->order_by('tanggal_data');
		$query = $this->db->get('default_dpk_dpk');

		$result = $query->result_array();
		return $result;
	}
	
}