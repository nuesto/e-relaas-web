<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Batch_process model
 *
 * @author Aditya Satrya
 */
class Batch_process_m extends MY_Model {
	
	public function get_batch_process($pagination_config = NULL, $params = array())
	{
		$this->db->select('default_dpk_batch_process.*');
		$this->db->select('count(default_dpk_dpk.id) AS num_data_row');

		foreach ($params as $key => $param) {
			$this->db->$param['function']($param['column'], $param['value']);
		}

		$this->db->join('default_dpk_dpk', 'default_dpk_dpk.id_batch_process = default_dpk_batch_process.id', 'left');
		$this->db->group_by('default_dpk_batch_process.id');

		$this->db->order_by("default_dpk_batch_process.tanggal_data", "desc");
		$this->db->order_by("default_dpk_batch_process.id", "desc");
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_dpk_batch_process');
		$result = $query->result_array();
		
        return $result;
	}

	public function count_batch_process_result($params = array())
	{
		$this->db->select('default_dpk_batch_process.id');

		foreach ($params as $key => $param) {
			$this->db->$param['function']($param['column'], $param['value']);
		}
		
		$result = $this->db->count_all_results('default_dpk_batch_process');
		
        return $result;
	}
	
	public function get_batch_process_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_dpk_batch_process');
		$result = $query->row_array();
		
		return $result;
	}

	public function count_all_batch_process()
	{
		return $this->db->count_all('dpk_batch_process');
	}
	
	public function delete_batch_process_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_dpk_batch_process');
	}
	
	public function insert_batch_process($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_dpk_batch_process', $values);
	}
	
	public function update_batch_process($values, $row_id)
	{	
		$this->db->where('id', $row_id);
		return $this->db->update('default_dpk_batch_process', $values); 
	}
	
}