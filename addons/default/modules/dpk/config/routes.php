<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['dpk/c/batch_process(:any)'] = 'admin_batch_process$1';
$route['dpk/c/dpk(:any)'] = 'admin_dpk$1';
$route['dpk/batch_process(:any)'] = 'dpk_batch_process$1';
$route['dpk/dpk(:any)'] = 'dpk_dpk$1';
