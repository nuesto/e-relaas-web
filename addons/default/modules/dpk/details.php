<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Dpk extends Module
{
    public $version = '1.0.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'DPK',
			'id' => 'DPK',
		);
		$info['description'] = array(
			'en' => 'Dana Pihak Ketiga',
			'id' => 'Dana Pihak Ketiga',
		);
		$info['frontend'] = false;
		$info['backend'] = true;
		$info['menu'] = 'dpk';
		$info['roles'] = array(
			'access_batch_process_backend', 'view_all_batch_process', 'view_own_batch_process', 'delete_all_batch_process', 'delete_own_batch_process', 'create_batch_process',
			'access_dpk_backend', 'view_all_dpk', 'view_own_dpk', 'edit_all_dpk', 'edit_own_dpk', 'delete_all_dpk', 'delete_own_dpk', 'create_dpk',
		);
		
		if(group_has_role('dpk', 'access_batch_process_backend')){
			$info['sections']['batch_process']['name'] = 'dpk:batch_process:plural';
			$info['sections']['batch_process']['uri'] = array('urls'=>array('c/dpk/batch_process/index','c/dpk/batch_process/create','c/dpk/batch_process/view%1','c/dpk/batch_process/edit%1','c/dpk/batch_process/view%2','c/dpk/batch_process/edit%2'));
			
			if(group_has_role('dpk', 'create_batch_process')){
				$info['sections']['batch_process']['shortcuts']['create'] = array(
					'name' => 'dpk:batch_process:new',
					'uri' => 'c/dpk/batch_process/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('dpk', 'access_dpk_backend')){
			$info['sections']['dpk']['name'] = 'dpk:dpk:plural';
			$info['sections']['dpk']['uri'] = array('urls'=>array('c/dpk/dpk/index','c/dpk/dpk/create','c/dpk/dpk/view%1','c/dpk/dpk/edit%1','c/dpk/dpk/view%2','c/dpk/dpk/edit%2'));
			
			if(group_has_role('dpk', 'create_dpk')){
				$info['sections']['dpk']['shortcuts']['create'] = array(
					'name' => 'dpk:dpk:new',
					'uri' => 'c/dpk/dpk/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items){
		
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// batch_process
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'tanggal_data' => array(
				'type' => 'DATE',
				'null' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('dpk_batch_process', TRUE);
		$this->db->query("CREATE INDEX author_dpk_batch_process_idx ON ".$this->db->dbprefix("dpk_batch_process")."(created_by)");
		$this->db->query("CREATE INDEX tanggal_data_dpk_batch_process ON ".$this->db->dbprefix("dpk_batch_process")."(tanggal_data)");

		// add foreign key
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("dpk_batch_process")." 
			ADD CONSTRAINT `fk_dpk_batch_process_user` 
			FOREIGN KEY (`created_by`) 
			REFERENCES ".$this->db->dbprefix("users")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");


		// dpk
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'id_nasabah' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'produk' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'ccy' => array(
				'type' => 'VARCHAR',
				'constraint' => 3,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'saldo_eq_idr' => array(
				'type' => 'DECIMAL',
				'constraint' => array(15,2),
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'id_batch_process' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('dpk_dpk', TRUE);

		// add foreign key
		$this->db->query("CREATE INDEX fk_dpk_dpk_batch_process_idx ON ".$this->db->dbprefix("dpk_dpk")."(id_batch_process)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("dpk_dpk")." 
			ADD CONSTRAINT `fk_dpk_dpk_nasabah` 
			FOREIGN KEY (`id_nasabah`) 
			REFERENCES ".$this->db->dbprefix("nasabah_nasabah")." (`id`) 
			ON DELETE CASCADE 
			ON UPDATE CASCADE");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("dpk_dpk")." 
			ADD CONSTRAINT `fk_dpk_dpk_batch_process` 
			FOREIGN KEY (`id_batch_process`) 
			REFERENCES ".$this->db->dbprefix("dpk_batch_process")." (`id`) 
			ON DELETE CASCADE 
			ON UPDATE CASCADE");

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('dpk_dpk');
        $this->dbforge->drop_table('dpk_batch_process');
        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}