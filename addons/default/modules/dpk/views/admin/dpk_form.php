<?php
echo Asset::css('datepicker.css');
echo Asset::css('select2.css');

echo Asset::js('date-time/bootstrap-datepicker.js');
echo Asset::js('select2.full.min.js');
?>
<div class="page-header">
	<h1><?php echo lang('dpk:dpk:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_nasabah"><?php echo lang('dpk:dpk:nama_nasabah'); ?></label>

		<div class="col-sm-5">
			<?php 
				$value = NULL;
				if($this->input->post('id_nasabah') != NULL){
					$value = $this->input->post('id_nasabah');
				}elseif($this->input->get('f-id_nasabah') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-id_nasabah');
				}elseif($mode == 'edit'){
					$value = $fields['id_nasabah'];
				}
			?>
			<select id="id_nasabah" name="id_nasabah" class="col-sm-12 select-ajax" data-default="<?php echo $value; ?>" data-source="<?php echo base_url(); ?>c/dpk/dpk/ajax_get_nasabah">
				<?php if($value != NULL) { ?>
					<option value="<?php echo $nasabah['id'] ?>" selected><?php echo $nasabah['value'] ?></option>
				<?php } ?>
			</select>
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="produk"><?php echo lang('dpk:dpk:produk'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('produk') != NULL){
					$value = $this->input->post('produk');
				}elseif($this->input->get('f-produk') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-produk');
				}elseif($mode == 'edit'){
					$value = $fields['produk'];
				}
			?>
			<select name="produk" class="col-xs-10 col-sm-5" id="">
				<option value="">-Pilih-</option>
				<option <?php echo($value=='GIRO') ? 'selected' : ''; ?>>GIRO</option>
				<option <?php echo($value=='DEPOSITO') ? 'selected' : ''; ?>>DEPOSITO</option>
				<option <?php echo($value=='TABUNGAN') ? 'selected' : ''; ?>>TABUNGAN</option>
			</select>
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="rekening"><?php echo lang('dpk:dpk:rekening'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('rekening') != NULL){
					$value = $this->input->post('rekening');
				}elseif($this->input->get('f-rekening') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-rekening');
				}elseif($mode == 'edit'){
					$value = $fields['rekening'];
				}
			?>
			<input name="rekening" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="ccy"><?php echo lang('dpk:dpk:ccy'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('ccy') != NULL){
					$value = $this->input->post('ccy');
				}elseif($this->input->get('f-ccy') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-ccy');
				}elseif($mode == 'edit'){
					$value = $fields['ccy'];
				}
			?>
			<input name="ccy" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="rate"><?php echo lang('dpk:dpk:rate'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('rate') != NULL){
					$value = $this->input->post('rate');
				}elseif($this->input->get('f-rate') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-rate');
				}elseif($mode == 'edit'){
					$value = $fields['rate'];
				}
			?>
			<input name="rate" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="due_date"><?php echo lang('dpk:dpk:due_date'); ?></label>

		<div class="col-sm-2">
			<?php 
				$value = NULL;
				if($this->input->post('due_date') != NULL){
					$value = $this->input->post('due_date');
				}elseif($this->input->get('f-due_date') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-due_date');
				}elseif($mode == 'edit'){
					$value = $fields['due_date'];
				}
			?>
			<div class="input-group">
				<input class="form-control date-picker" name="due_date" type="text" data-date-format="yyyy-mm-dd" value="<?php echo $value; ?>">
				<span class="input-group-addon">
					<i class="icon-calendar bigger-110"></i>
				</span>
			</div>
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="saldo_org"><?php echo lang('dpk:dpk:saldo_org'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('saldo_org') != NULL){
					$value = $this->input->post('saldo_org');
				}elseif($this->input->get('f-saldo_org') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-saldo_org');
				}elseif($mode == 'edit'){
					$value = $fields['saldo_org'];
				}
			?>
			<input name="saldo_org" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="saldo_eq_idr"><?php echo lang('dpk:dpk:saldo_eq_idr'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('saldo_eq_idr') != NULL){
					$value = $this->input->post('saldo_eq_idr');
				}elseif($this->input->get('f-saldo_eq_idr') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-saldo_eq_idr');
				}elseif($mode == 'edit'){
					$value = $fields['saldo_eq_idr'];
				}
			?>
			<input name="saldo_eq_idr" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tanggal_data"><?php echo lang('dpk:dpk:tanggal_data'); ?></label>

		<div class="col-sm-2">
			<?php 
				$value = NULL;
				if($this->input->post('tanggal_data') != NULL){
					$value = $this->input->post('tanggal_data');
				}elseif($this->input->get('f-tanggal_data') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-tanggal_data');
				}elseif($mode == 'edit'){
					$value = $fields['tanggal_data'];
				}
			?>
			<div class="input-group">
				<input class="form-control date-picker" name="tanggal_data" type="text" data-date-format="yyyy-mm-dd" value="<?php echo $value; ?>">
				<span class="input-group-addon">
					<i class="icon-calendar bigger-110"></i>
				</span>
			</div>
		</div>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>

<script type="text/javascript">
	$(document).ready(function(){
		$('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
		  $(this).prev().focus();
		});

		$( ".select-ajax" ).select2({        
		    ajax: {
		        url: $('.select-ajax').data('source'),
		        dataType: 'json',
		        delay: 250,
		        data: function (params) {
		            return {
		                q: params.term // search term
		            };
		        },
		        processResults: function (data) {
		            return {
		                results: data
		            };
		        },
		        cache: true
		    },
		    minimumInputLength: 2
		});
	});
</script>