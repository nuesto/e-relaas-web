<style type="text/css">
    .alert-notif-check-file{
        font-size: 12px !important;
        padding: 8px !important;
    }
</style>
<div class="page-header">
    <h1><?php echo lang('dpk:batch_process:view'); ?></h1>
    
    <div class="btn-group content-toolbar">
        
        <a href="<?php echo site_url('c/dpk/batch_process/index'.$uri); ?>" class="btn btn-sm btn-yellow">
            <i class="icon-arrow-left"></i>
            <?php echo lang('dpk:back') ?>
        </a>

        <?php if(group_has_role('dpk', 'edit_all_batch_process')){ ?>
            <a href="<?php echo site_url('c/dpk/batch_process/edit/'.$batch_process['id'].$uri); ?>" class="btn btn-sm btn-yellow">
                <i class="icon-edit"></i>
                <?php echo lang('global:edit') ?>
            </a>
        <?php }elseif(group_has_role('dpk', 'edit_own_batch_process')){ ?>
            <?php if($batch_process->created_by_user_id == $this->current_user->id){ ?>
                <a href="<?php echo site_url('c/dpk/batch_process/edit/'.$batch_process->id.$uri); ?>" class="btn btn-sm btn-yellow">
                    <i class="icon-edit"></i>
                    <?php echo lang('global:edit') ?>
                </a>
            <?php } ?>
        <?php } ?>

        <?php if(group_has_role('dpk', 'delete_all_batch_process')){ ?>
            <a href="<?php echo site_url('c/dpk/batch_process/delete/'.$batch_process['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
                <i class="icon-trash"></i>
                <?php echo lang('global:delete') ?>
            </a>
        <?php }elseif(group_has_role('dpk', 'delete_own_batch_process')){ ?>
            <?php if($batch_process->created_by_user_id == $this->current_user->id){ ?>
                <a href="<?php echo site_url('c/dpk/batch_process/delete/'.$batch_process['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
                    <i class="icon-trash"></i>
                    <?php echo lang('global:delete') ?>
                </a>
            <?php } ?>
        <?php } ?>
        
    </div>
</div>

<div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row data-detail first">
                <div class="data-detail-label col-xs-4 col-sm-4">ID</div>
                <?php if(isset($batch_process['id'])){ ?>
                <div class="data-detail-value col-xs-8 col-sm-8"><?php echo $batch_process['id']; ?></div>
                <?php }else{ ?>
                <div class="data-detail-value col-xs-8 col-sm-8">-</div>
                <?php } ?>
            </div>

            <div class="row data-detail">
                <div class="data-detail-label col-xs-4 col-sm-4"><?php echo lang('dpk:batch_process:tanggal_data'); ?></div>
                <?php if(isset($batch_process['tanggal_data'])){ ?>
                <div class="data-detail-value col-xs-8 col-sm-8"><?php echo $batch_process['tanggal_data']; ?></div>
                <?php }else{ ?>
                <div class="data-detail-value col-xs-8 col-sm-8">-</div>
                <?php } ?>
            </div>

            <div class="row data-detail">
                <div class="data-detail-label col-xs-4 col-sm-4"><?php echo lang('dpk:batch_process:status'); ?></div>
                <?php if(isset($batch_process['status'])){ ?>
                <div class="data-detail-value col-xs-8 col-sm-8"><?php echo $batch_process['status']; ?></div>
                <?php }else{ ?>
                <div class="data-detail-value col-xs-8 col-sm-8">-</div>
                <?php } ?>
            </div>

            <div class="row data-detail">
                <div class="data-detail-label col-xs-4 col-sm-4"><?php echo lang('dpk:batch_process:status_last_updated'); ?></div>
                <?php if(isset($batch_process['status_last_updated'])){ ?>
                <div class="data-detail-value col-xs-8 col-sm-8"><?php echo $batch_process['status_last_updated']; ?></div>
                <?php }else{ ?>
                <div class="data-detail-value col-xs-8 col-sm-8">-</div>
                <?php } ?>
            </div>

            <div class="row data-detail">
                <div class="data-detail-label col-xs-4 col-sm-4"><?php echo lang('dpk:batch_process:task_count_total'); ?></div>
                <?php if(isset($batch_process['task_count_total'])){ ?>
                <div class="data-detail-value col-xs-8 col-sm-8"><?php echo number_format($batch_process['task_count_total'], 0, ',','.'); ?></div>
                <?php }else{ ?>
                <div class="data-detail-value col-xs-8 col-sm-8">-</div>
                <?php } ?>
            </div>

            <div class="row data-detail">
                <div class="data-detail-label col-xs-4 col-sm-4"><?php echo lang('dpk:batch_process:task_count_processed'); ?></div>
                <?php if(isset($batch_process['task_count_processed'])){ ?>
                <div class="data-detail-value col-xs-8 col-sm-8"><?php echo number_format($batch_process['task_count_processed'], 0, ',','.'); ?></div>
                <?php }else{ ?>
                <div class="data-detail-value col-xs-8 col-sm-8">-</div>
                <?php } ?>
            </div>

            <div class="row data-detail">
                <div class="data-detail-label col-xs-4 col-sm-4"><?php echo lang('dpk:created'); ?></div>
                <?php if(isset($batch_process['created_on'])){ ?>
                <div class="data-detail-value col-xs-8 col-sm-8"><?php echo format_date($batch_process['created_on'], 'd-m-Y G:i'); ?></div>
                <?php }else{ ?>
                <div class="data-detail-value col-xs-8 col-sm-8">-</div>
                <?php } ?>
            </div>

            <div class="row data-detail">
                <div class="data-detail-label col-xs-4 col-sm-4"><?php echo lang('dpk:updated'); ?></div>
                <?php if(isset($batch_process['updated_on'])){ ?>
                <div class="data-detail-value col-xs-8 col-sm-8"><?php echo format_date($batch_process['updated_on'], 'd-m-Y G:i'); ?></div>
                <?php }else{ ?>
                <div class="data-detail-value col-xs-8 col-sm-8">-</div>
                <?php } ?>
            </div>

            <div class="row data-detail">
                <div class="data-detail-label col-xs-4 col-sm-4"><?php echo lang('dpk:created_by'); ?></div>
                <div class="data-detail-value col-xs-8 col-sm-8"><?php echo user_displayname($batch_process['created_by'], true); ?></div>
            </div>
        </div>

        <div class="col-sm-6">
            <h4>Pilih FIle</h4>
            <input type="file" name="file" id="files">
            <h4>Progress Cek File DPK</h4>
            <div class="progress progress-striped active">
                <div class="progress-bar progress-bar-success" id="progress" style="width: <?php echo $batch_process['persentase_processed'] ?>%"><?php echo $batch_process['persentase_processed'] ?>%
                </div>
            </div>

            <div class="aler alert-success" id="alert-success" style="display: none;">
            </div>

            <div class="alert alert-danger alert-notif-check-file" id="alert-error" <?php echo ($batch_process['error_status'] == NULL) ? 'style="display: none;"' : ''; ?>>
                <?php if($batch_process['error_status'] != NULL) { ?>
                    <?php echo $batch_process['error_status'] ?>
                <?php } ?>
            </div>

            <?php if($batch_process['task_count_total'] == 0 || ($batch_process['task_count_processed'] < $batch_process['task_count_total'])){ ?>

            <button class="btn btn-sm btn-primary btn-start" id="btn-process" onclick="proses()" <?php echo ($batch_process['task_count_processed'] != 100) ? '' : 'style="display: none;"'; ?>><i class="fa fa-play"></i> <span class="lbl-start"><?php echo ($batch_process['task_count_processed'] > 0) ? 'Lanjutkan' : 'Mulai'; ?></span></button>

            <button class="btn btn-sm btn-danger disabled btn-stop" onclick="stop()"><i class="fa fa-stop"></i> Berhenti</button>
            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    var id = '<?php echo $batch_process['id'] ?>';
    var task_count_total = parseInt('<?php echo $batch_process['task_count_total'] ?>');
    var task_count_processed = parseInt('<?php echo $batch_process['task_count_processed'] ?>');
    var data_csv = [];
    var batas_eksekusi = 500;
    <?php if($batch_process['error_status'] != null){ ?>
        var error_status = true;
    <?php }else{ ?>
        var error_status = false;
    <?php } ?>

    var do_proses = true;
    var do_progress = true;

    function alert_show(tipe, msg){
        $("#alert-"+tipe).show();
        $("#alert-"+tipe).html(msg);
    }

    function alert_hide(tipe){
        if(tipe == 'all'){
            $(".alert").hide();
            $(".alert").html('');
        }else{
            $("#alert-"+tipe).hide();
            $("#alert-"+tipe).html('');
        }
    }


    function proses(){
        $(".btn-start").addClass('disabled');
        $(".btn-stop").removeClass('disabled');
        do_proses = true;
        do_progress = true;
        ajax_proses();
    }

    function ajax_proses(){

        if(do_proses){
            alert_hide('all');
            $.isLoading({ text: "Membuka File" });
            parsecsvtojson();
           
        }
    }

    function parsecsvtojson(){

        var files = $('#files')[0].files;
        var config = buildConfig();

        if (files.length > 0)
        {
            var ext = $('#files').val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['csv']) == -1) {
                alert_show('error','File yang diperbolehkan hanyalah file .csv');
                stop();
            }else{
                start = performance.now();
                
                $('#files').parse({
                    config: config,
                    before: function(file, inputElem)
                    {
                        console.log("Parsing file:", file);
                    },
                    complete: function(data)
                    {
                        console.log("Done with all files.");
                    }
                });
            }
        }
        else
        {
            alert_show('error','Tidak ada file yang dipilih');
            stop();
        }
    }

    function buildConfig()
    {
        return {
            delimiter: "",  // auto-detect
            newline: "",    // auto-detect
            quoteChar: '"',
            header: false,
            dynamicTyping: false,
            preview: 0,
            encoding: "",
            worker: false,
            comments: false,
            step: undefined,
            complete: completeFn,
            error: undefined,
            download: false,
            skipEmptyLines: false,
            chunk: undefined,
            fastMode: undefined,
            beforeFirstChunk: undefined,
            withCredentials: undefined
        };
    }

    function completeFn()
    {
        end = performance.now();
        if (!$('#stream').prop('checked')
                && !$('#chunk').prop('checked')
                && arguments[0]
                && arguments[0].data)
            rows = arguments[0].data.length;
        
        // task_count_total = parseInt(rows-1);
        data_csv = arguments[0].data;
        delete data_csv[0];
        task_count_total = parseInt(data_csv.length - 1);
        console.log(data_csv, task_count_total);
        $.isLoading( "hide" );
        insert_dpk();
    }


    function insert_dpk(){
        if(do_proses){
            var jumlah_eksekusi = 0;
            if(task_count_processed < task_count_total){
                // task_count_processed = (task_count_processed == 0) ? task_count_processed+1 : task_count_processed;
                task_count_processed = (!error_status) ? parseInt(task_count_processed + 1) : task_count_processed;
                var data_send = [];
                console.log(task_count_processed);
                for (var i = task_count_processed; i < data_csv.length; i++) {
                    // data_send = [];

                    if(jumlah_eksekusi == batas_eksekusi){
                        break;
                    }
                    data_csv[i].push(parseInt(i+1));
                    var row = data_csv[i];
                    data_send.push(row);
                    jumlah_eksekusi++;

                }
                console.log(data_send);
                ajax_insert_dpk(data_send);
            }else{
                console.log('done input csv');
                stop();
            }
        }
    }

    function ajax_insert_dpk(data_send){
        $.ajax({
            url:"<?php echo base_url(); ?>c/dpk/batch_process/insert_dpk/"+id,
            type:'POST',
            data:{"csv_json" : JSON.stringify(data_send), 'task_count_total' : task_count_total, 'task_count_processed':task_count_processed},
            dataType: 'json',
            success: function(data){
                
                $("#progress").css("width",data.persentase_processed+"%");
                $("#progress").html(data.persentase_processed+"%");
                task_count_processed = parseInt(data.task_count_processed);
                error_status = data.error_status;
                if(data.status == 'error'){
                    alert_show('error',data.messages);
                    stop();
                }else{                

                    console.log(task_count_processed, task_count_total);
                    if(task_count_processed < task_count_total){
                        insert_dpk();
                    }else{   
                        $(".btn-stop").hide();
                        $(".btn-start").hide();
                    }
                }
                
            },
            error: function(error){
                alert_show('error','Maaf, Terjadi Kesalahan');
                stop();
            }
        }).done(function(data){

            
        });
    }

    function stop(){
        $("#lbl-start").html('Lanjutkan');
        $("#btn-process").show();
        $(".btn-start").removeClass('disabled');
        $(".btn-stop").addClass('disabled');
        do_proses = false;
        do_progress = false;
    }
</script>