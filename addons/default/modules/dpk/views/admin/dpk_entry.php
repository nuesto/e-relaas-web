<div class="page-header">
	<h1><?php echo lang('dpk:dpk:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('c/dpk/dpk/index'.$uri); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('dpk:back') ?>
		</a>

		<?php if(group_has_role('dpk', 'edit_all_dpk')){ ?>
			<a href="<?php echo site_url('c/dpk/dpk/edit/'.$dpk['id'].$uri); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('dpk', 'edit_own_dpk')){ ?>
			<?php if($dpk->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('c/dpk/dpk/edit/'.$dpk->id.$uri); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('dpk', 'delete_all_dpk')){ ?>
			<a href="<?php echo site_url('c/dpk/dpk/delete/'.$dpk['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('dpk', 'delete_own_dpk')){ ?>
			<?php if($dpk->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('c/dpk/dpk/delete/'.$dpk['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row data-detail first">
		<div class="data-detail-label col-sm-2">ID</div>
		<div class="data-detail-value col-sm-10"><?php echo $dpk['id']; ?></div>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:dpk:id_nasabah'); ?></div>
		<?php if(isset($dpk['id_nasabah'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo $dpk['id_nasabah']; ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:dpk:produk'); ?></div>
		<?php if(isset($dpk['produk'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo $dpk['produk']; ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:dpk:rekening'); ?></div>
		<?php if(isset($dpk['rekening'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo $dpk['rekening']; ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:dpk:ccy'); ?></div>
		<?php if(isset($dpk['ccy'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo $dpk['ccy']; ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:dpk:rate'); ?></div>
		<?php if(isset($dpk['rate'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo $dpk['rate']; ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:dpk:due_date'); ?></div>
		<?php if(isset($dpk['due_date'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo $dpk['due_date']; ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:dpk:saldo_org'); ?></div>
		<?php if(isset($dpk['saldo_org'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo $dpk['saldo_org']; ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:dpk:saldo_eq_idr'); ?></div>
		<?php if(isset($dpk['saldo_eq_idr'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo $dpk['saldo_eq_idr']; ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:dpk:id_batch_process'); ?></div>
		<?php if(isset($dpk['id_batch_process'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo $dpk['id_batch_process']; ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:dpk:tanggal_data'); ?></div>
		<?php if(isset($dpk['tanggal_data'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo $dpk['tanggal_data']; ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:created'); ?></div>
		<?php if(isset($dpk['created_on'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo format_date($dpk['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:updated'); ?></div>
		<?php if(isset($dpk['updated_on'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo format_date($dpk['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('dpk:created_by'); ?></div>
		<div class="data-detail-value col-sm-10"><?php echo user_displayname($dpk['created_by'], true); ?></div>
	</div>
</div>