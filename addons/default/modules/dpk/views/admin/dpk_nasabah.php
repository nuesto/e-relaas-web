<div class="col-xs-12">

<?php if (count($dpk_table) > 0): ?>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th><?php echo lang('dpk:dpk:produk') ?></th>
					<?php foreach ($ccy_existing as $key => $ccy) { ?>
						<th><?php echo $ccy; ?></th>
					<?php } ?>
					<th><?php echo lang('dpk:dpk:no_of_account') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach ($dpk_table as $produk => $value) { ?>
					<tr>
						<td><?php echo $produk; ?></td>
						<?php foreach ($ccy_existing as $key => $ccy) { ?>
							<td><?php echo (isset($value['ccy_format'][$ccy])) ? $value['ccy_format'][$ccy] : 0; ?></td>
						<?php } ?>
						<td><?php echo (isset($value['rekening'])) ? $value['rekening'] : '-'; ?></td>
					</tr>
					<?php
				} 
				?>
			</tbody>
			<tfoot>
				<tr>
					<th>Total</th>
					<?php foreach ($ccy_existing as $key => $ccy) { ?>
						<th><?php echo (isset($total_ccy_format[$ccy])) ? $total_ccy_format[$ccy] : 0; ?></th>
					<?php } ?>
					<th></th>
				</tr>
			</tfoot>
		</table>
	</div>
	<hr>
	<div class="row">
		<div class="col-xs-12 col-md-4">
			<table border="0" width="100%">
				<tr>
					<td align="center"><b>DPK</b></td>
					<td colspan="2" align="right"><b><?php echo $last_updated; ?></b></td>
				</tr>
				<tr>
					<td rowspan="3" align="center" valign="middle" id="dpk" style="background: #5d96d0; font-size: 25px; color:  white; font-weight: bold;"><?php echo $total_dpk_format ?></td>
					<td align="center" id="gr" style="background: #f7a427; font-weight: bold;">GR</td>
					<td align="right" id="val_gr" style="background: #f7a427a1; font-weight: bold;"><?php echo (isset($dpk_table['GIRO']['total_ccy_format'])) ? $dpk_table['GIRO']['total_ccy_format'] : 0; ?></td>
				</tr>
				<tr>
					<td align="center" id="dp" style="background: #5baf3f; font-weight: bold;">DP</td>
					<td align="right" id="val_dp" style="background: #5baf3fa3; font-weight: bold;"><?php echo (isset($dpk_table['DEPOSITO']['total_ccy_format'])) ? $dpk_table['DEPOSITO']['total_ccy_format'] : 0; ?></td>
				</tr>
				<tr>
					<td align="center" id="tb" style="background: #de6d41; font-weight: bold;">TB</td>
					<td align="right" id="val_tb" style="background: #de6d41a6; font-weight: bold;"><?php echo (isset($dpk_table['TABUNGAN']['total_ccy_format'])) ? $dpk_table['TABUNGAN']['total_ccy_format'] : 0; ?></td>
				</tr>
			</table>

			<hr>
			<div id="idr_val" style="height: 355px; margin-top:10px;"></div>
		</div>
		<div class="col-xs-12 col-md-8">
			<div id="history" style="min-width: 310px; height: 500px;"></div>
		</div>
	</div>

	<hr>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Deposito</th>
					<th>CCY</th>
					<th>Rate</th>
					<th>Due Date</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($deposito_entries as $key => $deposito) { ?>
				<tr>
					<td><?php echo ($deposito['ccy'] == 'IDR') ? $deposito['saldo_org'] : $deposito['saldo_eq_idr']; ?></td>
					<td><?php echo $deposito['ccy'] ?></td>
					<td><?php echo $deposito['rate'] ?></td>
					<td><?php echo format_date_id($deposito['due_date'], 'd M  Y', ''); ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		
	</div>
	
<?php else: ?>
	<div class="well"><?php echo lang('dpk:dpk:no_entry'); ?></div>
<?php endif;?>

</div>

<script type="text/javascript">
	$(function () {
		Highcharts.chart('history', {
		    chart: {
		        type: 'area'
		    },
		    title: {
		        text: 'History DPK'
		    },
		    subtitle: {
		        text: '<?php echo $text_range_tanggal ?>'
		    },
		    xAxis: {
		        categories: [<?php echo $obj_tgl ?>],
		        tickmarkPlacement: 'on',
		        title: {
		            enabled: false
		        }
		    },
		    yAxis: {
		        title: {
		            text: 'Nilai'
		        },
		        labels: {
		            formatter: function () {
		                return this.value / 1000;
		            }
		        }
		    },
		    tooltip: {
		        split: true,
		        valueSuffix: ''
		    },
		    plotOptions: {
		        area: {
		            stacking: 'normal',
		            lineColor: '#666666',
		            lineWidth: 1,
		            marker: {
		                lineWidth: 1,
		                lineColor: '#666666'
		            }
		        }
		    },
		    series: [{
		        name: 'DPK',
		        data: [<?php echo $obj_dpk ?>]
		    }]
		});

		Highcharts.chart('idr_val', {
		    chart: {
		        type: 'column'
		    },
		    title: {
		        text: 'Total IDR + VAL'
		    },
		    subtitle: {
		        text: ''
		    },
		    xAxis: {
		        type: 'category'
		    },
		    yAxis: {
		        title: {
		            text: 'Total'
		        }

		    },
		    legend: {
		        enabled: false
		    },
		    plotOptions: {
		        series: {
		            borderWidth: 0,
		            dataLabels: {
		                enabled: true,
		                format: '{point.y:,.1f}'
		            }
		        }
		    },

		    tooltip: {
		        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
		        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:,.1f}</b> of total<br/>'
		    },

		    series: [{
		        name: 'Total',
		        colorByPoint: true,
		        data: [{
		            name: 'IDR',
		            y: <?php echo (isset($total_ccy['IDR'])) ? $total_ccy['IDR'] : 0; ?>,
		            drilldown: 'IDR',
		            color: '#90ef7f'
		        }, {
		            name: 'VA',
		            y: <?php echo (isset($total_ccy['VA'])) ? $total_ccy['VA'] : 0; ?>,
		            drilldown: 'VA',
		            color:'#f7a35c'
		        }]
		    }],
		});
	});
</script>