<?php
echo Asset::css('datepicker.css');

echo Asset::js('date-time/bootstrap-datepicker.js');
?>

<?php 
$dpk_err_msg = NULL;
if(isset($_SESSION['dpk_error_message'])){
	$dpk_err_msg = $_SESSION['dpk_error_message'];
	unset($_SESSION['dpk_error_message']);
}

if($dpk_err_msg != NULL){ 
?>
<div class="alert alert-danger">
	<?php echo $dpk_err_msg; ?>
</div>
<?php } ?>

<div class="page-header">
	<h1><?php echo lang('dpk:batch_process:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>
<div class="row">
	<div class="col-sm-6">
		<div>
			<h4 class="header smaller lighter no-margin-left">Format File</h4>
			
			<p>Urutan kolom:</p>
			<ol>
				<li><?php echo lang('dpk:dpk:csv_col_nomor'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_tanggal_data'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_divisi_kode'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_divisi_nama'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_crm_kode'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_crm_nama'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_group_kode'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_nasabah'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_produk'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_ccy'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_jumlah_cif'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_jumlah_rekening'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_saldo_eq_idr'); ?></li>
				<li><?php echo lang('dpk:dpk:csv_col_interest_expense'); ?></li>
			</ol>

			<p>Ketentuan:</p>
			<ul>
				<li><span class="grey"><?php echo lang('dpk:dpk:csv_note_header'); ?></span></li>
			</ul>
		</div>
	</div>

	<div class="col-sm-6">
		<div class="form-horizontal">
			<h4 class="header smaller lighter">Pilih File</h4>

			<div class="col-sm-12">
				<label class="control-label no-padding-right" for="tanggal_data"><?php echo lang('dpk:batch_process:tanggal_data'); ?></label>

				<div class="form-group">
					<?php 
						$value = NULL;
						if($this->input->post('tanggal_data') != NULL){
							$value = $this->input->post('tanggal_data');
						}elseif($this->input->get('f-tanggal_data') != NULL AND $mode == 'new'){
							$value = $this->input->get('f-tanggal_data');
						}elseif($mode == 'edit'){
							$value = $fields['tanggal_data'];
						}
					?>
					<div class="col-sm-6 input-group">
						<input class="form-control date-picker" name="tanggal_data" type="text" data-date-format="yyyy-mm-dd" value="<?php echo $value; ?>">
						<span class="input-group-addon">
							<i class="icon-calendar bigger-110"></i>
						</span>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<label class="control-label no-padding-right" for="file_dpk"><?php echo lang('dpk:batch_process:file_dpk'); ?></label>

				<div class="form-group">
					<?php 
						$value = NULL;
						if($this->input->post('file_dpk') != NULL){
							$value = $this->input->post('file_dpk');
						}elseif($this->input->get('f-file_dpk') != NULL AND $mode == 'new'){
							$value = $this->input->get('f-file_dpk');
						}elseif($mode == 'edit'){
							$value = $fields['file_dpk'];
						}
					?>
					<div class="col-sm-6 input-group">
						<input class="form-control" name="file_dpk" type="file" />
					</div>

				</div>
			</div>

			<div class="col-sm-12">
				<a href="<?php echo site_url('c/dpk/batch_process/csv_example'); ?>">Download contoh file CSV</a>
			</div>

			<div class="col-sm-12 clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
					<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
				</div>
			</div>
		</div>
	</div>

	
</div>

<?php echo form_close();?>

<script type="text/javascript">
	$(document).ready(function(){
		$('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
		  $(this).prev().focus();
		});
	});
</script>