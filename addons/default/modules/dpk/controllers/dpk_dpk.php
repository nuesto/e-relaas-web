<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * DPK Module
 *
 * Dana Pihak Ketiga
 *
 */
class Dpk_dpk extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'dpk';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('dpk');
		
		$this->load->model('dpk_m');
    }

    /**
	 * List all DPK
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'view_all_dpk') AND ! group_has_role('dpk', 'view_own_dpk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'dpk/dpk/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->dpk_m->count_all_dpk();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){

				$params[substr($key, 2)] = $value;
			}
		}
		
        $data['dpk']['entries'] = $this->dpk_m->get_dpk($pagination_config);
		$data['dpk']['total'] = count($data['dpk']['entries']);
		$data['dpk']['pagination'] = $this->pagination->create_links();

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('dpk:dpk:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('dpk:dpk:plural'))
			->build('dpk_index', $data);
    }
	
	/**
     * Display one DPK
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'view_all_dpk') AND ! group_has_role('dpk', 'view_own_dpk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['dpk'] = $this->dpk_m->get_dpk_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('dpk', 'view_all_dpk')){
			if($data['dpk']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('dpk:dpk:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('dpk', 'view_all_dpk') OR group_has_role('dpk', 'view_own_dpk')){
			$this->template->set_breadcrumb(lang('dpk:dpk:plural'), '/dpk/dpk/index');
		}

		$this->template->set_breadcrumb(lang('dpk:dpk:view'))
			->build('dpk_entry', $data);
    }
	
	/**
     * Create a new DPK entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'create_dpk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_dpk('new')){	
				$this->session->set_flashdata('success', lang('dpk:dpk:submit_success'));				
				redirect('dpk/dpk/index');
			}else{
				$data['messages']['error'] = lang('dpk:dpk:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'dpk/dpk/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dpk:dpk:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('dpk', 'view_all_dpk') OR group_has_role('dpk', 'view_own_dpk')){
			$this->template->set_breadcrumb(lang('dpk:dpk:plural'), '/dpk/dpk/index');
		}

		$this->template->set_breadcrumb(lang('dpk:dpk:new'))
			->build('dpk_form', $data);
    }
	
	/**
     * Edit a DPK entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the DPK to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'edit_all_dpk') AND ! group_has_role('dpk', 'edit_own_dpk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('dpk', 'edit_all_dpk')){
			$entry = $this->dpk_m->get_dpk_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_dpk('edit', $id)){	
				$this->session->set_flashdata('success', lang('dpk:dpk:submit_success'));				
				redirect('dpk/dpk/index');
			}else{
				$data['messages']['error'] = lang('dpk:dpk:submit_failure');
			}
		}
		
		$data['fields'] = $this->dpk_m->get_dpk_by_id($id);
		$data['mode'] = 'edit';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'dpk/dpk/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
       $this->template->title(lang('dpk:dpk:edit'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('dpk', 'view_all_dpk') OR group_has_role('dpk', 'view_own_dpk')){
			$this->template->set_breadcrumb(lang('dpk:dpk:plural'), '/dpk/dpk/index')
			->set_breadcrumb(lang('dpk:dpk:view'), '/dpk/dpk/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('dpk:dpk:plural'))
			->set_breadcrumb(lang('dpk:dpk:view'));
		}

		$this->template->set_breadcrumb(lang('dpk:dpk:edit'))
			->build('dpk_form', $data);
    }
	
	/**
     * Delete a DPK entry
     * 
     * @param   int [$id] The id of DPK to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'delete_all_dpk') AND ! group_has_role('dpk', 'delete_own_dpk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('dpk', 'delete_all_dpk')){
			$entry = $this->dpk_m->get_dpk_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->dpk_m->delete_dpk_by_id($id);
		$this->session->set_flashdata('error', lang('dpk:dpk:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(5);
        redirect('dpk/dpk/index'.$data['uri']);
    }
	
	/**
     * Insert or update DPK entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_dpk($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('dpk:dpk:field_name'), '');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->dpk_m->insert_dpk($values);
			}
			else
			{
				$result = $this->dpk_m->update_dpk($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}