<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * DPK Module
 *
 * Dana Pihak Ketiga
 *
 */
class Dpk extends Public_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('dpk/batch_process/index');
    }

}