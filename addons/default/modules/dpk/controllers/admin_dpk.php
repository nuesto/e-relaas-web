<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * DPK Module
 *
 * Dana Pihak Ketiga
 *
 */
class Admin_dpk extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'dpk';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'access_dpk_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('dpk');		
		$this->load->model('dpk_m');
		$this->load->model('nasabah/nasabah_m');
    }

    /**
	 * List all DPK
     *
     * @return	void
     */

    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'view_all_dpk') AND ! group_has_role('dpk', 'view_own_dpk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		// -------------------------------------
		// handle filter get params
		// -------------------------------------

		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){
				$column = substr($key, 2);
				$function = ($column == 'tanggal_data') ? 'where' : 'like';
				if($column == 'nama'){
					$column = 'nasabah_nasabah.'.$column;
				}elseif($column == 'tanggal_data'){
					$column = 'dpk_batch_process.'.$column;
				}
				$params[] = array('column'=>$column,'function'=>$function,'value'=>$value);
			}
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'c/dpk/dpk/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->dpk_m->count_dpk_result($params);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------		
		
        $data['dpk']['entries'] = $this->dpk_m->get_dpk($pagination_config, $params);
		$data['dpk']['total'] = $pagination_config['total_rows'];
		$data['dpk']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['dpk']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('c/dpk/dpk/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('c/dpk/dpk/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('dpk:dpk:plural'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('dpk:dpk:plural'))
			->append_css('module::dpk.css')
			->append_js('module::dpk.js')
			->build('admin/dpk_index', $data);
    }
	
	/**
     * Create a new DPK entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'create_dpk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			$nama_nasabah = $this->nasabah_m->get_nasabah_by_id($_POST['id_nasabah'])['nama'];
			$data['nasabah'] = array('id'=>$_POST['id_nasabah'], 'value'=>$nama_nasabah);
			if($this->_update_dpk('new')){	
				$this->session->set_flashdata('success', lang('dpk:dpk:submit_success'));				
				redirect('c/dpk/dpk/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('dpk:dpk:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/dpk/dpk/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dpk:dpk:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('dpk', 'view_all_dpk') OR group_has_role('dpk', 'view_own_dpk')){
			$this->template->set_breadcrumb(lang('dpk:dpk:plural'), '/c/dpk/dpk/index');
		}

		$this->template->set_breadcrumb(lang('dpk:dpk:new'))
			->append_css('module::dpk.css')
			->append_js('module::dpk.js')
			->build('admin/dpk_form', $data);
    }
	
	/**
     * Edit a DPK entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the DPK to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'edit_all_dpk') AND ! group_has_role('dpk', 'edit_own_dpk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('dpk', 'edit_all_dpk')){
			$entry = $this->dpk_m->get_dpk_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_dpk('edit', $id)){	
				$this->session->set_flashdata('success', lang('dpk:dpk:submit_success'));				
				redirect('c/dpk/dpk/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('dpk:dpk:submit_failure');
			}
		}
		
		$data['fields'] = $this->dpk_m->get_dpk_by_id($id);
		if($_POST){
			if($_POST['id_nasabah']){
				$nama_nasabah = $this->nasabah_m->get_nasabah_by_id($_POST['id_nasabah'])['nama'];
				$data['nasabah'] = array('id'=>$_POST['id_nasabah'], 'value'=>$nama_nasabah);
			}
		}else{
			$data['nasabah'] = array('id'=>$data['fields']['id_nasabah'], 'value'=>$data['fields']['nama_nasabah']);
			
		}
		$data['mode'] = 'edit';
		$data['return'] = 'c/dpk/dpk/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dpk:dpk:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('dpk', 'view_all_dpk') OR group_has_role('dpk', 'view_own_dpk')){
			$this->template->set_breadcrumb(lang('dpk:dpk:plural'), '/c/dpk/dpk/index')
			->set_breadcrumb(lang('dpk:dpk:view'), '/c/dpk/dpk/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('dpk:dpk:plural'))
			->set_breadcrumb(lang('dpk:dpk:view'));
		}

		$this->template->set_breadcrumb(lang('dpk:dpk:edit'))
			->append_css('module::dpk.css')
			->append_js('module::dpk.js')
			->build('admin/dpk_form', $data);
    }
	
	/**
     * Delete a DPK entry
     * 
     * @param   int [$id] The id of DPK to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'delete_all_dpk') AND ! group_has_role('dpk', 'delete_own_dpk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		// Check view all/own permission
		if(! group_has_role('dpk', 'delete_all_dpk')){
			$entry = $this->dpk_m->get_dpk_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->dpk_m->delete_dpk_by_id($id);
        $this->session->set_flashdata('error', lang('dpk:dpk:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('c/dpk/dpk/index'.$data['uri']);
    }
	
	/**
     * Insert or update DPK entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_dpk($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();
		if($values['due_date'] == ""){
			$values['due_date'] = NULL;
		}

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('id_nasabah', lang('dpk:dpk:nama_nasabah'), 'required');
		$this->form_validation->set_rules('produk', lang('dpk:dpk:produk'), 'required|max_length[200]');
		$this->form_validation->set_rules('rekening', lang('dpk:dpk:rekening'), 'required|numeric|max_length[15]');
		$this->form_validation->set_rules('ccy', lang('dpk:dpk:ccy'), 'required|max_length[3]');
		if($values['rate'] != 0){
			$this->form_validation->set_rules('rate', lang('dpk:dpk:rate'), 'required|decimal');
		}else{
			$this->form_validation->set_rules('rate', lang('dpk:dpk:rate'), 'required');
		}
		$this->form_validation->set_rules('saldo_org', lang('dpk:dpk:saldo_org'), 'required|decimal|max_length[15]');
		$this->form_validation->set_rules('saldo_eq_idr', lang('dpk:dpk:saldo_eq_idr'), 'required|decimal|max_length[15]');
		$this->form_validation->set_rules('tanggal_data', lang('dpk:dpk:tanggal_data'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$values['id_batch_process'] = 1;
				$result = $this->dpk_m->insert_dpk($values);
				
			}
			else
			{
				$result = $this->dpk_m->update_dpk($values, $row_id);
			}
		}
		
		return $result;
	}

	public function ajax_get_nasabah(){
		$this->load->model('nasabah/nasabah_m');

		$params = array();
		$data_json = array();
		// $data_json[] = array('id'=>'', 'text'=>'Tidak ada dataa');
		if($this->input->get('q')){
			$params[] = array('column'=>'nasabah_nasabah.nama','function'=>'like','value'=>$this->input->get('q'));
			$data_nasabah = $this->nasabah_m->get_nasabah(NULL, $params);
			foreach ($data_nasabah as $key => $nasabah) {
				$data_json[] = array('id'=>$nasabah['id'], 'text'=>$nasabah['nama']);
			}
			echo json_encode($data_json);
		}

	}

	// --------------------------------------------------------------------------

}