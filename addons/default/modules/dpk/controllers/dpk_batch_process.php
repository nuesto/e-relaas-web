<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * DPK Module
 *
 * Dana Pihak Ketiga
 *
 */
class Dpk_batch_process extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'batch_process';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('dpk');
		
		$this->load->model('batch_process_m');
    }

    /**
	 * List all Batch Process
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'view_all_batch_process') AND ! group_has_role('dpk', 'view_own_batch_process')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'dpk/batch_process/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->batch_process_m->count_all_batch_process();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){

				$params[substr($key, 2)] = $value;
			}
		}
		
        $data['batch_process']['entries'] = $this->batch_process_m->get_batch_process($pagination_config);
		$data['batch_process']['total'] = count($data['batch_process']['entries']);
		$data['batch_process']['pagination'] = $this->pagination->create_links();

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('dpk:batch_process:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('dpk:batch_process:plural'))
			->build('batch_process_index', $data);
    }
	
	/**
     * Display one Batch Process
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'view_all_batch_process') AND ! group_has_role('dpk', 'view_own_batch_process')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['batch_process'] = $this->batch_process_m->get_batch_process_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('dpk', 'view_all_batch_process')){
			if($data['batch_process']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('dpk:batch_process:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('dpk', 'view_all_batch_process') OR group_has_role('dpk', 'view_own_batch_process')){
			$this->template->set_breadcrumb(lang('dpk:batch_process:plural'), '/dpk/batch_process/index');
		}

		$this->template->set_breadcrumb(lang('dpk:batch_process:view'))
			->build('batch_process_entry', $data);
    }
	
	/**
     * Create a new Batch Process entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'create_batch_process')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_batch_process('new')){	
				$this->session->set_flashdata('success', lang('dpk:batch_process:submit_success'));				
				redirect('dpk/batch_process/index');
			}else{
				$data['messages']['error'] = lang('dpk:batch_process:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'dpk/batch_process/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dpk:batch_process:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('dpk', 'view_all_batch_process') OR group_has_role('dpk', 'view_own_batch_process')){
			$this->template->set_breadcrumb(lang('dpk:batch_process:plural'), '/dpk/batch_process/index');
		}

		$this->template->set_breadcrumb(lang('dpk:batch_process:new'))
			->build('batch_process_form', $data);
    }
	
	/**
     * Edit a Batch Process entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Batch Process to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'edit_all_batch_process') AND ! group_has_role('dpk', 'edit_own_batch_process')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('dpk', 'edit_all_batch_process')){
			$entry = $this->batch_process_m->get_batch_process_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_batch_process('edit', $id)){	
				$this->session->set_flashdata('success', lang('dpk:batch_process:submit_success'));				
				redirect('dpk/batch_process/index');
			}else{
				$data['messages']['error'] = lang('dpk:batch_process:submit_failure');
			}
		}
		
		$data['fields'] = $this->batch_process_m->get_batch_process_by_id($id);
		$data['mode'] = 'edit';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'dpk/batch_process/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
       $this->template->title(lang('dpk:batch_process:edit'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('dpk', 'view_all_batch_process') OR group_has_role('dpk', 'view_own_batch_process')){
			$this->template->set_breadcrumb(lang('dpk:batch_process:plural'), '/dpk/batch_process/index')
			->set_breadcrumb(lang('dpk:batch_process:view'), '/dpk/batch_process/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('dpk:batch_process:plural'))
			->set_breadcrumb(lang('dpk:batch_process:view'));
		}

		$this->template->set_breadcrumb(lang('dpk:batch_process:edit'))
			->build('batch_process_form', $data);
    }
	
	/**
     * Delete a Batch Process entry
     * 
     * @param   int [$id] The id of Batch Process to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'delete_all_batch_process') AND ! group_has_role('dpk', 'delete_own_batch_process')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('dpk', 'delete_all_batch_process')){
			$entry = $this->batch_process_m->get_batch_process_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->batch_process_m->delete_batch_process_by_id($id);
		$this->session->set_flashdata('error', lang('dpk:batch_process:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(5);
        redirect('dpk/batch_process/index'.$data['uri']);
    }
	
	/**
     * Insert or update Batch Process entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_batch_process($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('dpk:batch_process:field_name'), '');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->batch_process_m->insert_batch_process($values);
			}
			else
			{
				$result = $this->batch_process_m->update_batch_process($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}