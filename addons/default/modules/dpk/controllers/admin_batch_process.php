<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * DPK Module
 *
 * Dana Pihak Ketiga
 *
 */
class Admin_batch_process extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'batch_process';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'access_batch_process_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('dpk');	
		$this->load->model('batch_process_m');
		$this->load->model('dpk_m');
		$this->load->model('nasabah/nasabah_m');
    }

    /**
	 * List all Batch Process
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'view_all_batch_process') AND ! group_has_role('dpk', 'view_own_batch_process')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		// -------------------------------------
		// handle filter get params
		// -------------------------------------

		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){
				$column = substr($key, 2);
				$function = 'where';
				$params[] = array('column'=>$column,'function'=>$function,'value'=>$value);
			}
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'c/dpk/batch_process/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->batch_process_m->count_batch_process_result($params);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['batch_process']['entries'] = $this->batch_process_m->get_batch_process($pagination_config, $params);
		$data['batch_process']['total'] = $pagination_config['total_rows'];
		$data['batch_process']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['batch_process']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('c/dpk/batch_process/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('c/dpk/batch_process/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('dpk:batch_process:plural'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('dpk:batch_process:plural'))
			->append_css('module::dpk.css')
			->append_js('module::dpk.js')
			->build('admin/batch_process_index', $data);
    }
	
	/**
     * Create a new Batch Process entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */

	
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'create_batch_process')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_batch_process('new')){	
				$this->session->set_flashdata('success', lang('dpk:batch_process:submit_success'));				
				redirect('c/dpk/batch_process/index'.$data['uri']);
				// redirect('c/dpk/batch_process/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('dpk:batch_process:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/dpk/batch_process/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dpk:batch_process:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('dpk', 'view_all_batch_process') OR group_has_role('dpk', 'view_own_batch_process')){
			$this->template->set_breadcrumb(lang('dpk:batch_process:plural'), '/c/dpk/batch_process/index');
		}

		$this->template->set_breadcrumb(lang('dpk:batch_process:new'))
			->append_css('module::dpk.css')
			->append_js('module::dpk.js')
			->build('admin/batch_process_form', $data);
    }
	
	/**
     * Delete a Batch Process entry
     * 
     * @param   int [$id] The id of Batch Process to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dpk', 'delete_all_batch_process') AND ! group_has_role('dpk', 'delete_own_batch_process')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		// Check view all/own permission
		if(! group_has_role('dpk', 'delete_all_batch_process')){
			$entry = $this->batch_process_m->get_batch_process_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}

		// -------------------------------------
		// Delete entry
		// -------------------------------------

		$this->db->trans_start();
		
		$this->dpk_m->delete_dpk_by_id_batch_process($id);
		$this->batch_process_m->delete_batch_process_by_id($id);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', lang('dpk:batch_process:delete_failed'));
		}else{
			$this->session->set_flashdata('success', lang('dpk:batch_process:deleted'));
		}        
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('c/dpk/batch_process/index'.$data['uri']);
    }
	
	/**
     * Insert or update Batch Process entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_batch_process($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$value_batch_process = array();
		$value_batch_process['tanggal_data'] = $this->input->post('tanggal_data');
		$value_batch_process['file_dpk'] = $this->input->post('file_dpk');

		// get dpk data from file
		$row = 1;
		if ($_FILES['file_dpk']['tmp_name'] != NULL 
			AND ($handle = fopen($_FILES['file_dpk']['tmp_name'], "r")) !== FALSE) {
			
			$dpk_data = array();
			$temp_dpk_data = array();
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				
				if($row > 1 AND $data != ''){
					// parse each row
					$col_idx = 1;
					$temp_dpk_data[$row]['tanggal_data'] = trim($data[$col_idx]); $col_idx++;
					$temp_dpk_data[$row]['divisi_kode'] = trim($data[$col_idx]); $col_idx++;
					$temp_dpk_data[$row]['divisi_nama'] = trim($data[$col_idx]); $col_idx++;
					$temp_dpk_data[$row]['crm_kode'] = trim($data[$col_idx]); $col_idx++;
					$temp_dpk_data[$row]['crm_nama'] = trim($data[$col_idx]); $col_idx++;
					$temp_dpk_data[$row]['customer_kode'] = trim($data[$col_idx]); $col_idx++;
					$temp_dpk_data[$row]['customer_nama'] = trim($data[$col_idx]); $col_idx++;
					$dpk_data[$row]['id_nasabah'] = $this->nasabah_m->get_nasabah_id_by_name($temp_dpk_data[$row]['customer_nama']);
					$dpk_data[$row]['produk'] = trim($data[$col_idx]); $col_idx++;
					$dpk_data[$row]['ccy'] = trim($data[$col_idx]); $col_idx++;
					$temp_dpk_data[$row]['jumlah_cif'] = trim($data[$col_idx]); $col_idx++;
					$temp_dpk_data[$row]['jumlah_rekening'] = trim($data[$col_idx]); $col_idx++;
					$dpk_data[$row]['saldo_eq_idr'] = str_replace(",", "", trim($data[$col_idx])); $col_idx++;
					$temp_dpk_data[$row]['interest_expense'] = trim($data[$col_idx]); $col_idx++;
				}

				$row++;
			}
			fclose($handle);
		}

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('tanggal_data', lang('dpk:batch_process:tanggal_data'), 'required|callback__is_tanggal_data_not_exist');
		$this->form_validation->set_rules('file_dpk', 'File', 'callback__is_file_dpk_valid');
		
		// Manual valudation (not using form_validation class) for csv data
		$num_invalid_rows = 0;
		$err_msg = array();
		foreach ($dpk_data as $row => $value) {
			// Customer: check if null or empty
			if(!isset($temp_dpk_data[$row]['customer_nama']) 
				OR $temp_dpk_data[$row]['customer_nama'] == NULL 
				OR $temp_dpk_data[$row]['customer_nama'] == ''){

				$num_invalid_rows++;
				$err_msg[] = sprintf(lang('dpk:batch_process:row_customer_is_required'), $row);
			}else{
				// Customer: check if exist
				if($value['id_nasabah'] == NULL){
					$num_invalid_rows++;
					$err_msg[] = sprintf(lang('dpk:batch_process:row_customer_not_exist'), $row, $temp_dpk_data[$row]['customer_nama']);
				}
			}

			// Produk: check if null or empty
			if(!isset($value['produk']) 
				OR $value['produk'] == NULL 
				OR $value['produk'] == ''){

				$num_invalid_rows++;
				$err_msg[] = sprintf(lang('dpk:batch_process:row_produk_is_required'), $row);
			}

			// Currency: check if null or empty
			if(!isset($value['ccy']) 
				OR $value['ccy'] == NULL 
				OR $value['ccy'] == ''){

				$num_invalid_rows++;
				$err_msg[] = sprintf(lang('dpk:batch_process:row_currency_is_required'), $row);
			}

			// Customer: check is number
			if(!is_numeric($value['saldo_eq_idr'])){
				$num_invalid_rows++;
				$err_msg[] = sprintf(lang('dpk:batch_process:row_saldo_total_not_number'), $row);
			}
		}
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			// handle manual validation for csv data
			if($num_invalid_rows > 0){
				$err_msg_str = implode("<br />", $err_msg);
				$err_msg_str = lang('dpk:batch_process:file_dpk_data_not_valid').'<br /><br />'.$err_msg_str;

				$_SESSION['dpk_error_message'] = $err_msg_str;
				$result = FALSE;
			}else{
				if ($method == 'new')
				{
					$this->db->trans_begin();

					unset($value_batch_process['file_dpk']);
					$result = $this->batch_process_m->insert_batch_process($value_batch_process);
					if($result){
						$id_batch_process = $this->db->insert_id();
						foreach ($dpk_data as $key => $value) {
							$dpk_data[$key]['id_batch_process'] = $id_batch_process;
						}

						$this->dpk_m->insert_batch_dpk($dpk_data);
					}
					
					$result = $this->db->trans_status();
					
					if($result === FALSE){
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();
					}
				}
				else
				{
					$result = $this->batch_process_m->update_batch_process($value_batch_process, $row_id);
				}
			}
		}
		
		return $result;
	}

	public function _is_tanggal_data_not_exist($value)
	{
		$params = array();
		$params[0]['function'] = 'where';
		$params[0]['column'] = 'tanggal_data';
		$params[0]['value'] = $value;
		$result = $this->batch_process_m->count_batch_process_result($params);

		if($result > 0){
			$this->form_validation->set_message('_is_tanggal_data_not_exist',lang('dpk:batch_process:tanggal_data_exist'));
			return FALSE;
		}else{
			return TRUE;
		}
	}

	public function _is_file_dpk_valid()
	{
		$file_info = $_FILES['file_dpk'];
		// check if exist
		if(!is_uploaded_file($file_info['tmp_name'])) {
			$this->form_validation->set_message('_is_file_dpk_valid',lang('dpk:batch_process:file_dpk_is_not_exist'));
			return FALSE;
		}

		// check if type=text 
		$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
		if(!in_array($file_info['type'], $mimes)){
			$this->form_validation->set_message('_is_file_dpk_valid',lang('dpk:batch_process:file_dpk_is_not_csv'));
			return FALSE;
		}
	}

	public function _is_customer_exist($value)
	{
		if($value == NULL OR $value == '' OR $value < 1){
			$this->form_validation->set_message('_is_customer_exist','{field} '.lang('dpk:batch_process:customer_not_exist'));
			return FALSE;
		}else{
			return TRUE;
		}
	}

	public function _upload_csv_dpk()	
	{
		global $method_global;
		global $total_rows_of_csv_file;
		if(file_exists('uploads/dpk/'.$_POST['tanggal_data'].'.csv')){
	    	unlink('uploads/dpk/'.$_POST['tanggal_data'].'.csv');
			
		}

		$config['upload_path'] = 'uploads/dpk/';
	    $config['allowed_types'] = 'csv';
	    $config['file_name'] = $_POST['tanggal_data'].'.csv';
	    $config['overwrite'] = true;

	    $this->load->library('upload', $config);

	    if ($this->upload->do_upload('file')) {
	    	$total_rows_of_csv_file = count(file('uploads/dpk/'.$_POST['tanggal_data'].'.csv',FILE_SKIP_EMPTY_LINES));
	    }else{
	    	$this->form_validation->set_message('_upload_csv_dpk', $this->upload->display_errors('', ''));
	    	return false;
	    }
	}

	public function insert_dpk($id)
	{

		$batch_process = $this->batch_process_m->get_batch_process_by_id($id);
		$bp_tanggal_data = $batch_process['tanggal_data'];
		$data = json_decode($_POST['csv_json'], false);
		
		$errors = array();
		$baris = array();
		$insert_batch_dpk = array();
		$return = array('status'=>'success', 'messages'=>'Berhasil menginputkan data');
		$task_count_processed = $_POST['task_count_processed'];
		foreach ($data as $key => $value) {
			$row = (array) $value;
			if(count($row) == 26){
				$i = $row[25];

				$tanggal_data = $row[0];
		        if($tanggal_data == ""){
	        		$errors[] = "Baris {$i}: TANGGAL DATA. harus diisi.";
		        }else{
		        	$date = DateTime::createFromFormat('d/m/Y H:i', $tanggal_data);
		        	if(!$date){
		        		$errors[] = "Baris {$i}: TANGGAL DATA. <b>".$tanggal_data."</b> harus berisi format tanggal 'd/m/Y H:i'.";
		        	}else{
		        		$tanggal_data = $date->format('Y-m-d');
			        	if($tanggal_data != $bp_tanggal_data){
				        	$errors[] = "Baris {$i}: TANGGAL DATA. <b>".$tanggal_data."</b> tidak sesuai dengan tanggal batch process";
				        }
		        	}
		        }

				$produk = $row[1];
				$arr_produk = array('TABUNGAN','DEPOSITO','GIRO');
				if($produk == ""){
		        	$errors[] = "Baris {$i}: PRODUK. harus diisi.";
				}else{
					$produk = strtoupper($this->trim_all($row[1]));
					if(!in_array($produk, $arr_produk)){
		        		$errors[] = "Baris {$i}: PRODUK. <b>".$produk."</b> tidak diperbolehkan.";
					}
				}
		        
		        $rekening = $row[4];

		        if($rekening == ""){		        	
	        		$errors[] = "Baris {$i}: REKENING. harus diisi.";
		        }else{
		        	$rekening = $this->trim_all($row[4]);
			        if(strlen($rekening) > 15){
		        		$errors[] = "Baris {$i}: REKENING. <b>".$rekening."</b> tidak boleh lebih dari 15 karakter.";
			        }
		        }

	        	
		        $ccy = $row[6];
		        if($ccy == ""){
					$errors[] = "Baris {$i}: CCY. harus diisi.";
		        }else{
		        	$ccy = str_replace(' ','',$row[6]);
			        if(strlen($ccy) > 3){
			        	$errors[] = "Baris {$i}: CCY. <b>".$ccy."</b> tidak bleh lebih dari 3 karakter.";
			        }
		        }

		        $saldo_org = $row[7];
		        if($saldo_org == ""){
		        	$errors[] = "Baris {$i}: SALDO ORG. harus diisi.";
		        }else{
		        	$saldo_org = floatval(str_replace(' ','',$row[7]));
			        if(strlen($saldo_org) > 15){
		        		$errors[] = "Baris {$i}: SALDO ORG. <b>".$saldo_org."</b> tidak boleh lebih dari 15 karakter.";
			        }
		        }

		        $saldo_eq_rp = $row[8];
		        if($saldo_eq_rp == ""){
		        	$errors[] = "Baris {$i}: SALDO ORG RP. harus diisi.";
		        }else{
		        	$saldo_eq_rp = floatval(str_replace(' ','',$row[8]));
			        if(strlen($saldo_eq_rp) > 15){
		        		$errors[] = "Baris {$i}: SALDO ORG RP. <b>".$saldo_eq_rp."</b> tidak boleh lebih dari 15 karakter.";
			        }
		        }

		        $suku_bunga = $row[9];
		        if($suku_bunga == ""){
	        		$errors[] = "Baris {$i}: SUKU BUNGA. harus diisi";
		        }else{
		        	$suku_bunga = floatval(str_replace(' ','',$row[9]));
			        if(!is_float($suku_bunga)){
		        		$errors[] = "Baris {$i}: SUKU BUNGA. <b>".$suku_bunga."</b> harus memuat angka desimal";
			        }
		        }

		        $jatuh_tempo = $row[10];
		        if($jatuh_tempo != ""){
		        	$date = DateTime::createFromFormat('d/m/Y H:i', $jatuh_tempo);
		        	if(!$date){
		        		$errors[] = "Baris {$i}: JATUH TEMPO. <b>".$jatuh_tempo."</b> harus berisi format tanggal 'd/m/Y H:i'.";
		        	}else{
		        		$jatuh_tempo = $date->format('Y-m-d');
		        	}
		        }else{
		        	$jatuh_tempo = NULL;
		        }

		        $nama_nasabah = $row[18];
		        if($nama_nasabah == ""){
			        $errors[] = "Baris {$i}: NAMA NASABAH. harus diisi.";
		        }else{
			        $nama_nasabah = $this->trim_all($row[18]);
			        $this->db->where('nama', $nama_nasabah);
			        $query = $this->db->get('default_nasabah_nasabah');
			        $data_nasabah = $query->row_array();
			        if(count($data_nasabah) == 0){
			        	$errors[] = "Baris {$i}: NAMA NASABAH. <b>".$nama_nasabah."</b> tidak terdaftar.";
			        }
			        // $data_nasabah['id'] = 1;
		        }


		        if(count($errors) > 0){
		        	$return['status'] = 'error';
		        	$return['messages'] = 'Terjadi kesalahan. <br>'.implode('<br />', $errors);
		        	break;
		        }else{
		        	$insert_batch_dpk['id_nasabah'] = $data_nasabah['id'];
		        	$insert_batch_dpk['produk'] = $produk;
		        	$insert_batch_dpk['rekening'] = $rekening;
		        	$insert_batch_dpk['ccy'] = $ccy;
		        	$insert_batch_dpk['rate'] = $suku_bunga;
		        	$insert_batch_dpk['due_date'] = $jatuh_tempo;
		        	$insert_batch_dpk['saldo_org'] = $saldo_org;
		        	$insert_batch_dpk['saldo_eq_idr'] = $saldo_eq_rp;
		        	$insert_batch_dpk['id_batch_process'] = $id;
		        	$insert_batch_dpk['tanggal_data'] = $tanggal_data;

			        $result = $this->dpk_m->insert_dpk($insert_batch_dpk);
		        }
		        $task_count_processed = $i;

		    }
		}
		//..................................... 

    	$update_process = $this->update_batch_process2($id, $task_count_processed, $errors);
    	$return['errors'] = $errors;
		$return['task_count_processed'] = $task_count_processed;
        $return['persentase_processed'] = ($update_process['task_count_processed'] > 0) ? number_format($update_process['task_count_processed']*100/$update_process['task_count_total'],2) : 0;
        $return['baris'] = $baris;
        $return['error_status'] = ($update_process['error_status'] != null) ? true : false;
		echo json_encode($return);
	}

	//custom khusus untuk format 'd/m/Y'
	// public function validate_date($date, ){

	// }

	public function update_batch_process2($id, $task_count_processed, $errors){
		$val_update_batch['task_count_total'] = $_POST['task_count_total'];
		$val_update_batch['last_file_name'] = $_POST['last_file_name'];
		$val_update_batch['status'] = ($task_count_processed == $val_update_batch['task_count_total']) ? 'Complete' : $_POST['status'];
		$val_update_batch['task_count_processed'] = $task_count_processed;
	    $val_update_batch['status_last_updated'] = date('Y-m-d H:i:s');
	    $val_update_batch['error_status'] = null;
	    if(count($errors) > 0){
	    	$val_update_batch['error_status'] = 'Terjadi kesalahan. <br>'.implode('<br />', $errors);;
	    }
	    $this->batch_process_m->update_batch_process($val_update_batch, $id);
		$batch_process = $this->batch_process_m->get_batch_process_by_id($id);
		return $batch_process;
	}

	public function validateDate($date, $format = 'Y-m-d H:i:s')
	{
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	public function trim_all( $str , $what = NULL , $with = ' ' )
	{
	    if( $what === NULL )
	    {
	        //  Character      Decimal      Use
	        //  "\0"            0           Null Character
	        //  "\t"            9           Tab
	        //  "\n"           10           New line
	        //  "\x0B"         11           Vertical Tab
	        //  "\r"           13           New Line in Mac
	        //  " "            32           Space
	       
	        $what   = "\\x00-\\x20";    //all white-spaces and control chars
	    }
	   
	    return trim( preg_replace( "/[".$what."]+/" , $with , $str ) , $what );
	}

	public function csv_example()
	{
		$this->load->helper('download');
		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=dpk_csv_example.csv");
		header("Pragma: no-cache");
		header("Expires: 0");

    	$row = array();

    	// first row (header)
    	$col = array();
    	$col[] = 'Nomor';
		$col[] = 'TglData';
		$col[] = 'KdDivisi';
		$col[] = 'Divisi';
		$col[] = 'KdRM';
		$col[] = 'NamaRM';
		$col[] = 'KdGroup';
		$col[] = 'GroupNasabah';
		$col[] = 'Produk';
		$col[] = 'MataUang';
		$col[] = 'JmlCIF';
		$col[] = 'JmlRek';
		$col[] = 'SaldoTotal';
		$col[] = 'InterestExpense';
		$row[] = implode(",", $col);

		// second row
    	$col = array();
    	$col[] = '1';
		$col[] = date('Y-m-d');
		$col[] = '1030';
		$col[] = 'LMC-1';
		$col[] = '19150';
		$col[] = 'John Doe';
		$col[] = '116';
		$col[] = 'PT ABC COMPANY';
		$col[] = 'GIRO';
		$col[] = 'IDR';
		$col[] = '1';
		$col[] = '1';
		$col[] = '"99,999,999"';
		$col[] = '"99,000"';
		$row[] = implode(",", $col);

		// combine rows
		$data = implode("\n", $row);

    	$name = 'dpk_csv_example.csv';

    	force_download($name, $data);
	}

}