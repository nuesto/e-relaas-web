<div class="page-header">
	<h1><?php echo lang('produk:product:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama"><?php echo lang('produk:product:nama'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama') != NULL){
					$value = $this->input->post('nama');
				}elseif($this->input->get('f-nama') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-nama');
				}elseif($mode == 'edit'){
					$value = $fields['nama'];
				}
			?>
			<input name="nama" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_item"><?php echo lang('produk:product:id_item'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_item') != NULL){
					$value = $this->input->post('id_item');
				}elseif($this->input->get('f-id_item') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-id_item');
				}elseif($mode == 'edit'){
					$value = $fields['id_item'];
				}
			
				$options = array('' => '');
				foreach ($product_item_list as $key => $product_item) {
					$options[$product_item['id']] = $product_item['nama'];
				}
				echo form_dropdown('id_item', $options, $value);
			?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="kategori"><?php echo lang('produk:product:kategori'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('kategori') != NULL){
					$value = $this->input->post('kategori');
				}elseif($this->input->get('f-kategori') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-kategori');
				}elseif($mode == 'edit'){
					$value = $fields['kategori'];
				}

				$options = array(
					'' => '',
					'bisnis' => 'Bisnis',
					'konsumer' => 'Konsumer',
				);
				echo form_dropdown('kategori', $options, $value);
			?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('produk:ordering_count'); ?></label>

		<div class="col-sm-1">
			<?php 
				$value = NULL;
				if($this->input->post('ordering_count') != NULL){
					$value = $this->input->post('ordering_count');
				}elseif($mode == 'edit'){
					$value = $fields['ordering_count'];
				}
			
				$options = array();
				for ($i=0; $i <= 10; $i++) { 
					$options[$i] = $i;
				}
				echo form_dropdown('ordering_count', $options, $value);
			?>
		</div>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>