<div class="page-header">
	<h1><?php echo lang('produk:item:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama"><?php echo lang('produk:item:nama'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama') != NULL){
					$value = $this->input->post('nama');
				}elseif($this->input->get('f-nama') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-nama');
				}elseif($mode == 'edit'){
					$value = $fields['nama'];
				}
			?>
			<input name="nama" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_family"><?php echo lang('produk:item:id_family'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_family') != NULL){
					$value = $this->input->post('id_family');
				}elseif($this->input->get('f-id_family') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-id_family');
				}elseif($mode == 'edit'){
					$value = $fields['id_family'];
				}
			
				$options = array('' => '');
				foreach ($product_family_list as $key => $product_family) {
					$options[$product_family['id']] = $product_family['nama'];
				}
				echo form_dropdown('id_family', $options, $value);
			?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('produk:ordering_count'); ?></label>

		<div class="col-sm-1">
			<?php 
				$value = NULL;
				if($this->input->post('ordering_count') != NULL){
					$value = $this->input->post('ordering_count');
				}elseif($mode == 'edit'){
					$value = $fields['ordering_count'];
				}
			
				$options = array();
				for ($i=0; $i <= 10; $i++) { 
					$options[$i] = $i;
				}
				echo form_dropdown('ordering_count', $options, $value);
			?>
		</div>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>