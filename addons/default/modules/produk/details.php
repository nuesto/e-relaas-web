<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Produk extends Module
{
    public $version = '1.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Produk',
			'id' => 'Produk',
		);
		$info['description'] = array(
			'en' => 'Manajemen produk perbankan',
			'id' => 'Manajemen produk perbankan',
		);
		$info['frontend'] = false;
		$info['backend'] = true;
		$info['menu'] = 'produk';
		$info['roles'] = array(
			'access_family_backend', 'view_all_family', 'view_own_family', 'edit_all_family', 'edit_own_family', 'delete_all_family', 'delete_own_family', 'create_family',
			'access_item_backend', 'view_all_item', 'view_own_item', 'edit_all_item', 'edit_own_item', 'delete_all_item', 'delete_own_item', 'create_item',
			'access_product_backend', 'view_all_product', 'view_own_product', 'edit_all_product', 'edit_own_product', 'delete_all_product', 'delete_own_product', 'create_product',
		);
		
		if(group_has_role('produk', 'access_family_backend')){
			$info['sections']['family']['name'] = 'produk:family:plural';
			$info['sections']['family']['uri'] = array('urls'=>array('c/produk/family/index','c/produk/family/create','c/produk/family/view%1','c/produk/family/edit%1','c/produk/family/view%2','c/produk/family/edit%2'));
			
			if(group_has_role('produk', 'create_family')){
				$info['sections']['family']['shortcuts']['create'] = array(
					'name' => 'produk:family:new',
					'uri' => 'c/produk/family/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('produk', 'access_item_backend')){
			$info['sections']['item']['name'] = 'produk:item:plural';
			$info['sections']['item']['uri'] = array('urls'=>array('c/produk/item/index','c/produk/item/create','c/produk/item/view%1','c/produk/item/edit%1','c/produk/item/view%2','c/produk/item/edit%2'));
			
			if(group_has_role('produk', 'create_item')){
				$info['sections']['item']['shortcuts']['create'] = array(
					'name' => 'produk:item:new',
					'uri' => 'c/produk/item/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('produk', 'access_product_backend')){
			$info['sections']['product']['name'] = 'produk:product:plural';
			$info['sections']['product']['uri'] = array('urls'=>array('c/produk/product/index','c/produk/product/create','c/produk/product/view%1','c/produk/product/edit%1','c/produk/product/view%2','c/produk/product/edit%2'));
			
			if(group_has_role('produk', 'create_product')){
				$info['sections']['product']['shortcuts']['create'] = array(
					'name' => 'produk:product:new',
					'uri' => 'c/produk/product/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items){
		
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// family
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 512,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('produk_family', TRUE);
		$this->db->query("CREATE INDEX author_produk_family_idx ON ".$this->db->dbprefix("produk_family")."(created_by)");

		// edit this query to add foreign key
		/*$this->db->query("CREATE INDEX fk_produk_family_ref_idx ON ".$this->db->dbprefix("produk_family")."(id_ref)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("produk_family")." 
			ADD CONSTRAINT `fk_produk_family_ref` 
			FOREIGN KEY (`id_ref`) 
			REFERENCES ".$this->db->dbprefix("produk_ref")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");*/


		// item
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'id_family' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 512,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('produk_item', TRUE);
		$this->db->query("CREATE INDEX author_produk_item_idx ON ".$this->db->dbprefix("produk_item")."(created_by)");

		// edit this query to add foreign key
		/*$this->db->query("CREATE INDEX fk_produk_item_ref_idx ON ".$this->db->dbprefix("produk_item")."(id_ref)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("produk_item")." 
			ADD CONSTRAINT `fk_produk_item_ref` 
			FOREIGN KEY (`id_ref`) 
			REFERENCES ".$this->db->dbprefix("produk_ref")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");*/


		// product
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'id_item' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 512,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'kategori' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('produk_product', TRUE);
		$this->db->query("CREATE INDEX author_produk_product_idx ON ".$this->db->dbprefix("produk_product")."(created_by)");

		// edit this query to add foreign key
		/*$this->db->query("CREATE INDEX fk_produk_product_ref_idx ON ".$this->db->dbprefix("produk_product")."(id_ref)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("produk_product")." 
			ADD CONSTRAINT `fk_produk_product_ref` 
			FOREIGN KEY (`id_ref`) 
			REFERENCES ".$this->db->dbprefix("produk_ref")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");*/

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('produk_product');
        $this->dbforge->drop_table('produk_item');
        $this->dbforge->drop_table('produk_family');
        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}