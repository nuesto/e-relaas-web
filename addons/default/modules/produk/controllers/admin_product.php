<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Produk Module
 *
 * Manajemen produk perbankan
 *
 */
class Admin_product extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'product';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'access_product_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('produk');		
		$this->load->model('product_m');
		$this->load->model('item_m');
    }

    /**
	 * List all Product
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'view_all_product') AND ! group_has_role('produk', 'view_own_product')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){

				$params[substr($key, 2)] = $value;
			}
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'c/produk/product/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->product_m->count_all_product($params);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['product']['entries'] = $this->product_m->get_product($pagination_config, $params);
		$data['product']['total'] = $pagination_config['total_rows'];
		$data['product']['pagination'] = $this->pagination->create_links();
		$data['product_item_list'] = $this->item_m->get_item();

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['product']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('c/produk/product/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('c/produk/product/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('produk:product:plural'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('produk:product:plural'))
			->build('admin/product_index', $data);
    }
	
	/**
     * Create a new Product entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'create_product')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_product('new')){	
				$this->session->set_flashdata('success', lang('produk:product:submit_success'));				
				redirect('c/produk/product/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('produk:product:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/produk/product/index'.$data['uri'];
		$data['product_item_list'] = $this->item_m->get_item();
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('produk:product:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('produk', 'view_all_product') OR group_has_role('produk', 'view_own_product')){
			$this->template->set_breadcrumb(lang('produk:product:plural'), '/c/produk/product/index');
		}

		$this->template->set_breadcrumb(lang('produk:product:new'))
			->build('admin/product_form', $data);
    }
	
	/**
     * Edit a Product entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Product to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'edit_all_product') AND ! group_has_role('produk', 'edit_own_product')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('produk', 'edit_all_product')){
			$entry = $this->product_m->get_product_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_product('edit', $id)){	
				$this->session->set_flashdata('success', lang('produk:product:submit_success'));				
				redirect('c/produk/product/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('produk:product:submit_failure');
			}
		}
		
		$data['fields'] = $this->product_m->get_product_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'c/produk/product/index'.$data['uri'];
		$data['entry_id'] = $id;
		$data['product_item_list'] = $this->item_m->get_item();
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('produk:product:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('produk', 'view_all_product') OR group_has_role('produk', 'view_own_product')){
			$this->template->set_breadcrumb(lang('produk:product:plural'), '/c/produk/product/index')
			->set_breadcrumb(lang('produk:product:view'), '/c/produk/product/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('produk:product:plural'))
			->set_breadcrumb(lang('produk:product:view'));
		}

		$this->template->set_breadcrumb(lang('produk:product:edit'))
			->build('admin/product_form', $data);
    }
	
	/**
     * Delete a Product entry
     * 
     * @param   int [$id] The id of Product to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'delete_all_product') AND ! group_has_role('produk', 'delete_own_product')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		// Check view all/own permission
		if(! group_has_role('produk', 'delete_all_product')){
			$entry = $this->product_m->get_product_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->product_m->delete_product_by_id($id);
        $this->session->set_flashdata('error', lang('produk:product:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('c/produk/product/index'.$data['uri']);
    }
	
	/**
     * Insert or update Product entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_product($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama', lang('produk:product:nama'), 'required|max_length[512]');
		$this->form_validation->set_rules('kategori', lang('produk:product:kategori'), 'required|max_length[45]');
		$this->form_validation->set_rules('id_item', lang('produk:product:id_item'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->product_m->insert_product($values);
				
			}
			else
			{
				$result = $this->product_m->update_product($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}