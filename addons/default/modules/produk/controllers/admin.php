<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Produk Module
 *
 * Manajemen produk perbankan
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('c/produk/family/index');
    }

}