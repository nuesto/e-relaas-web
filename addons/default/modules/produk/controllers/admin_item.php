<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Produk Module
 *
 * Manajemen produk perbankan
 *
 */
class Admin_item extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'item';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'access_item_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('produk');		
		$this->load->model('item_m');
		$this->load->model('family_m');
    }

    /**
	 * List all Item
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'view_all_item') AND ! group_has_role('produk', 'view_own_item')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'c/produk/item/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->item_m->count_all_item();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){

				$params[substr($key, 2)] = $value;
			}
		}
		
        $data['item']['entries'] = $this->item_m->get_item($pagination_config, $params);
		$data['item']['total'] = count($data['item']['entries']);
		$data['item']['pagination'] = $this->pagination->create_links();
		$data['product_family_list'] = $this->family_m->get_family();

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['item']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('c/produk/item/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('c/produk/item/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('produk:item:plural'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('produk:item:plural'))
			->build('admin/item_index', $data);
    }
	
	/**
     * Create a new Item entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'create_item')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_item('new')){	
				$this->session->set_flashdata('success', lang('produk:item:submit_success'));				
				redirect('c/produk/item/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('produk:item:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/produk/item/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------

		$data['product_family_list'] = $this->family_m->get_family();
		
        $this->template->title(lang('produk:item:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('produk', 'view_all_item') OR group_has_role('produk', 'view_own_item')){
			$this->template->set_breadcrumb(lang('produk:item:plural'), '/c/produk/item/index');
		}

		$this->template->set_breadcrumb(lang('produk:item:new'))
			->build('admin/item_form', $data);
    }
	
	/**
     * Edit a Item entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Item to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'edit_all_item') AND ! group_has_role('produk', 'edit_own_item')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('produk', 'edit_all_item')){
			$entry = $this->item_m->get_item_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_item('edit', $id)){	
				$this->session->set_flashdata('success', lang('produk:item:submit_success'));				
				redirect('c/produk/item/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('produk:item:submit_failure');
			}
		}
		
		$data['fields'] = $this->item_m->get_item_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'c/produk/item/index'.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------

		$data['product_family_list'] = $this->family_m->get_family();
		
        $this->template->title(lang('produk:item:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('produk', 'view_all_item') OR group_has_role('produk', 'view_own_item')){
			$this->template->set_breadcrumb(lang('produk:item:plural'), '/c/produk/item/index')
			->set_breadcrumb(lang('produk:item:view'), '/c/produk/item/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('produk:item:plural'))
			->set_breadcrumb(lang('produk:item:view'));
		}

		$this->template->set_breadcrumb(lang('produk:item:edit'))
			->build('admin/item_form', $data);
    }
	
	/**
     * Delete a Item entry
     * 
     * @param   int [$id] The id of Item to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'delete_all_item') AND ! group_has_role('produk', 'delete_own_item')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		// Check view all/own permission
		if(! group_has_role('produk', 'delete_all_item')){
			$entry = $this->item_m->get_item_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->item_m->delete_item_by_id($id);
        $this->session->set_flashdata('error', lang('produk:item:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('c/produk/item/index'.$data['uri']);
    }
	
	/**
     * Insert or update Item entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_item($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama', lang('produk:item:nama'), 'required|max_length[512]');
		$this->form_validation->set_rules('id_family', lang('produk:item:id_family'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->item_m->insert_item($values);
				
			}
			else
			{
				$result = $this->item_m->update_item($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}