<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Produk Module
 *
 * Manajemen produk perbankan
 *
 */
class Admin_family extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'family';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'access_family_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('produk');		
		$this->load->model('family_m');
    }

    /**
	 * List all Family
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'view_all_family') AND ! group_has_role('produk', 'view_own_family')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'c/produk/family/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->family_m->count_all_family();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){

				$params[substr($key, 2)] = $value;
			}
		}
		
        $data['family']['entries'] = $this->family_m->get_family($pagination_config, $params);
		$data['family']['total'] = count($data['family']['entries']);
		$data['family']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['family']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('c/produk/family/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('c/produk/family/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('produk:family:plural'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('produk:family:plural'))
			->build('admin/family_index', $data);
    }
	
	/**
     * Create a new Family entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'create_family')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_family('new')){	
				$this->session->set_flashdata('success', lang('produk:family:submit_success'));				
				redirect('c/produk/family/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('produk:family:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/produk/family/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('produk:family:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('produk', 'view_all_family') OR group_has_role('produk', 'view_own_family')){
			$this->template->set_breadcrumb(lang('produk:family:plural'), '/c/produk/family/index');
		}

		$this->template->set_breadcrumb(lang('produk:family:new'))
			->build('admin/family_form', $data);
    }
	
	/**
     * Edit a Family entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Family to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'edit_all_family') AND ! group_has_role('produk', 'edit_own_family')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('produk', 'edit_all_family')){
			$entry = $this->family_m->get_family_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_family('edit', $id)){	
				$this->session->set_flashdata('success', lang('produk:family:submit_success'));				
				redirect('c/produk/family/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('produk:family:submit_failure');
			}
		}
		
		$data['fields'] = $this->family_m->get_family_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'c/produk/family/index'.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('produk:family:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('produk', 'view_all_family') OR group_has_role('produk', 'view_own_family')){
			$this->template->set_breadcrumb(lang('produk:family:plural'), '/c/produk/family/index')
			->set_breadcrumb(lang('produk:family:view'), '/c/produk/family/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('produk:family:plural'))
			->set_breadcrumb(lang('produk:family:view'));
		}

		$this->template->set_breadcrumb(lang('produk:family:edit'))
			->build('admin/family_form', $data);
    }
	
	/**
     * Delete a Family entry
     * 
     * @param   int [$id] The id of Family to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('produk', 'delete_all_family') AND ! group_has_role('produk', 'delete_own_family')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		// Check view all/own permission
		if(! group_has_role('produk', 'delete_all_family')){
			$entry = $this->family_m->get_family_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->family_m->delete_family_by_id($id);
        $this->session->set_flashdata('error', lang('produk:family:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('c/produk/family/index'.$data['uri']);
    }
	
	/**
     * Insert or update Family entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_family($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama', lang('produk:family:nama'), 'required|max_length[256]');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->family_m->insert_family($values);
				
			}
			else
			{
				$result = $this->family_m->update_family($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}