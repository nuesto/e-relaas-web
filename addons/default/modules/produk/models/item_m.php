<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Item model
 *
 * @author Aditya Satrya
 */
class Item_m extends MY_Model {
	
	public function get_item($pagination_config = NULL, $params = array())
	{
		$this->db->select('produk_item.id AS id');
		$this->db->select('produk_item.id_family AS id_family');
		$this->db->select('produk_family.nama AS family_nama');
		$this->db->select('produk_item.nama AS nama');
		$this->db->select('produk_item.created_on AS created_on');
		$this->db->select('produk_item.created_by AS created_by');
		$this->db->select('produk_item.updated_on AS updated_on');
		$this->db->select('produk_item.updated_by AS updated_by');
		$this->db->select('produk_item.ordering_count AS ordering_count');

		foreach ($params as $key => $value) {
			$this->db->like('produk_item.'.$key, $value);
		}

		$this->db->order_by("produk_family.ordering_count", "asc");
		$this->db->order_by("produk_item.ordering_count", "asc");

		$this->db->join('produk_family', 'produk_family.id = produk_item.id_family', 'left');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_produk_item');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_item_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_produk_item');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_item()
	{
		return $this->db->count_all('produk_item');
	}
	
	public function delete_item_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_produk_item');
	}
	
	public function insert_item($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_produk_item', $values);
	}
	
	public function update_item($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_produk_item', $values); 
	}
	
}