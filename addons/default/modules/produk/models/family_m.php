<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Family model
 *
 * @author Aditya Satrya
 */
class Family_m extends MY_Model {
	
	public function get_family($pagination_config = NULL, $params = array())
	{
		$this->db->select('*');

		foreach ($params as $key => $value) {
			$this->db->like($key, $value);
		}

		$this->db->order_by("ordering_count", "asc");
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_produk_family');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_family_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_produk_family');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_family()
	{
		return $this->db->count_all('produk_family');
	}
	
	public function delete_family_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_produk_family');
	}
	
	public function insert_family($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_produk_family', $values);
	}
	
	public function update_family($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_produk_family', $values); 
	}
	
}