<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Product model
 *
 * @author Aditya Satrya
 */
class Product_m extends MY_Model {
	
	public function get_product($pagination_config = NULL, $params = array())
	{
		$this->db->select('produk_product.id AS id');
		$this->db->select('produk_product.id_item AS id_item');
		$this->db->select('produk_item.nama AS item_nama');
		$this->db->select('produk_item.id_family AS id_family');
		$this->db->select('produk_family.nama AS family_nama');
		$this->db->select('produk_product.nama AS nama');
		$this->db->select('produk_product.kategori AS kategori');
		$this->db->select('produk_product.created_on AS created_on');
		$this->db->select('produk_product.created_by AS created_by');
		$this->db->select('produk_product.updated_on AS updated_on');
		$this->db->select('produk_product.updated_by AS updated_by');
		$this->db->select('produk_product.ordering_count AS ordering_count');

		foreach ($params as $key => $value) {
			$this->db->like('produk_product.'.$key, $value);
		}

		$this->db->join('produk_item', 'produk_item.id = produk_product.id_item', 'left');
		$this->db->join('produk_family', 'produk_family.id = produk_item.id_family', 'left');

		$this->db->order_by("produk_family.ordering_count", "asc");
		$this->db->order_by("produk_item.ordering_count", "asc");
		$this->db->order_by("produk_product.ordering_count", "asc");
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_produk_product');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_product_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_produk_product');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_product($params = array())
	{
		return count($this->get_product(NULL, $params));
	}
	
	public function delete_product_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_produk_product');
	}
	
	public function insert_product($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_produk_product', $values);
	}
	
	public function update_product($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_produk_product', $values); 
	}
	
}