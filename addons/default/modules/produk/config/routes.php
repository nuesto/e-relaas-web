<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['produk/c/family(:any)'] = 'admin_family$1';
$route['produk/c/item(:any)'] = 'admin_item$1';
$route['produk/c/product(:any)'] = 'admin_product$1';
$route['produk/family(:any)'] = 'produk_family$1';
$route['produk/item(:any)'] = 'produk_item$1';
$route['produk/product(:any)'] = 'produk_product$1';
