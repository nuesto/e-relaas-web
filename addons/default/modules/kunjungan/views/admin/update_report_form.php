<?php echo Asset::css('datepicker.css'); ?>
<?php echo Asset::css('bootstrap-timepicker.css'); ?>

<?php echo Asset::js('date-time/bootstrap-datepicker.js'); ?>
<?php echo Asset::js('date-time/bootstrap-timepicker.js'); ?>
<script type="text/javascript" src="<?php echo site_url('addons/default/modules/kunjungan/js/jquery.form.min.js'); ?>"></script>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="page-header modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h1 id="modal-header-title"><?php echo lang('kunjungan:update_report:'.$mode); ?></h1>
		</div>

		<?php echo form_open_multipart(uri_string().$uri, array('id'=>'form-update-report')); ?>

		<div class="modal-body">

			<div class="row">

				<div id="form-message"></div>

				<div class="form-group">
					<label class="col-xs-12 control-label no-padding-right" for="jadwal"><?php echo lang('kunjungan:jadwal:singular'); ?></label>

					<div class="col-xs-12">
						<div class="well">
							<?php if(isset($fields['nama_nasabah'])){
								echo '<strong>'.$fields['nama_nasabah'].'</strong><br />';
								echo $fields['jenis_kunjungan'].'<br />';
								echo $fields['tanggal'].', '.$fields['mulai'].' - '.$fields['selesai'];

								echo '<input type="hidden" name="id" value="'.$fields['id'].'" />';
							}else{
								echo '<i>Tidak ada informasi jadwal</i>';
							} ?>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-xs-12 control-label no-padding-right" for="memo"><?php echo lang('kunjungan:jadwal:memo'); ?></label>

					<div class="col-xs-12">
						<?php 
							$value = NULL;
							if($this->input->post('memo') != NULL){
								$value = $this->input->post('memo');
							}elseif($this->input->get('f-memo') != NULL AND $mode == 'new'){
								$value = $this->input->get('f-memo');
							}else{
								$value = $fields['memo'];
							}
						?>
						
						<textarea name="memo" rows="10" class="form-control" id="" ><?php echo $value; ?></textarea>
					</div>
				</div>

			</div>

		</div>

		<div class="modal-footer">
			<button type="submit" class="btn btn-primary">
				<i class="icon-ok"></i>
				<span><?php echo lang('buttons:save'); ?></span>
			</button>

			<a href="<?php echo site_url($return); ?>" class="btn btn-danger btn-cancel">
				<i class="icon-remove"></i>
				<?php echo lang('buttons:cancel'); ?>
			</a>
		</div>

		<?php echo form_close(); ?>

	</div>


</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.date-picker').datepicker({
			autoclose:true,
			dateFormat: 'yy-mm-dd',
			setDate:new Date(),
		}
		).next().on(ace.click_event, function(){
			$(this).prev().focus();
		});

		$('.timepicker').timepicker({
			showSeconds: false,
			showMeridian: false
		});

		$('#form-update-report').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(formData, jqForm, options){
				jqForm.find('button[type=submit]')
					.html('<span>Submitting...</span>')
					.attr("disabled", true);

				jqForm.find(":input").prop("disabled", true);

				return true;
			},
        	success: function(responseText, statusText, xhr, $form){
        		
        		var modal = $form.closest('.modal.in');

        		if(responseText.status=='error') {
					$form.find('#form-message').html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+responseText.messages+'</div>');

					modal.stop().animate({scrollTop: modal.offset().top}, 500);

					$form.find(":input").prop("disabled", false);
					$form.find('button[type=submit]')
						.html('<span>Submit</span>')
						.attr("disabled", false);
				} else {
					$form.find('#form-message').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+responseText.messages+'</div>');

					modal.stop().animate({scrollTop: modal.offset().top}, 500);
					
					setTimeout(function() {
						// if active modal is more than 1
						if($('.modal:visible').length > 1){
							// just close this modal
							modal.modal('hide');
							modal.find(".modal-body").html('');
						}else{
							// go to base page (without hash)
							var url = location.href.replace(location.hash,"");
							window.location.href = url;
						}
					},2000);
				}
        	}
		}); 

		$('.btn-cancel').click(function(e) {
			var modal = $(this).closest('.modal.in');
			if(modal.length > 0) {
				e.preventDefault();
				$(this).closest('.modal.in').modal('hide');
			}
		});
	});
</script>
