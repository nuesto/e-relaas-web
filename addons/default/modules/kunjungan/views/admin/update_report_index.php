<?php echo Asset::css('datepicker.css'); ?>
<?php echo Asset::css('bootstrap-timepicker.css'); ?>

<?php echo Asset::js('date-time/bootstrap-datepicker.js'); ?>
<?php echo Asset::js('date-time/bootstrap-timepicker.js'); ?>

<style type="text/css">
	.datepicker {
      z-index: 1600 !important; /* has to be larger than 1050 */
    }
</style>

<div class="page-header">
	<h1><?php echo lang('kunjungan:update_report:plural'); ?></h1>
</div>

<?php echo form_open('', array('class' => 'form-inline', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
	
	<div class="form-group">
		<label><?php echo lang('kunjungan:update_report:status'); ?>:&nbsp;</label>
		<select name="f-status_report">
			<option value=""><?php echo lang('kunjungan:choose'); ?></option>
			<option <?php echo ($this->input->get('f-status_report')=='has_submitted') ? 'selected="selected"' : ''; ?> value="has_submitted"><?php echo lang('kunjungan:update_report:has_submitted'); ?></option>
			<option <?php echo ($this->input->get('f-status_report')=='has_not_submitted') ? 'selected="selected"' : ''; ?> value="has_not_submitted"><?php echo lang('kunjungan:update_report:has_not_submitted'); ?></option>
		</select>
	</div>
	<div class="form-group">
		<label><?php echo lang('kunjungan:jadwal:id_petugas'); ?>:&nbsp;</label>
		<select name="f-id_petugas">
			<?php if(count($petugas)>1) { ?>
				<option value=""><?php echo lang('kunjungan:choose').' '.lang('kunjungan:jadwal:id_petugas'); ?></option>
			<?php } ?>
			<?php foreach ($petugas as $petugas_entry) {
				$selected = '';
				if($petugas_entry->id==$this->input->get('f-id_petugas')) {
					$selected = 'selected="selected"';
				}

				echo '<option value="'.$petugas_entry->id.'" '.$selected.'>'.$petugas_entry->display_name.'</option>';
			} ?>
		</select>
	</div>
	<div class="form-group">
		<label><?php echo lang('kunjungan:jadwal:jenis_kunjungan'); ?>:&nbsp;</label>
		<select name="f-jenis_kunjungan">
			<option value=""><?php echo lang('kunjungan:choose').' '.lang('kunjungan:jadwal:jenis_kunjungan'); ?></option>
			<option <?php echo ($this->input->get('f-jenis_kunjungan')=='Tatap Muka') ? 'selected="selected"' : ''; ?> value="Tatap Muka">Tatap Muka</option>
			<option <?php echo ($this->input->get('f-jenis_kunjungan')=='Telpon') ? 'selected="selected"' : ''; ?> value="Telpon">Telpon</option>
			<option <?php echo ($this->input->get('f-jenis_kunjungan')=='Email') ? 'selected="selected"' : ''; ?> value="Email">Email</option>
		</select>
	</div>
	<div class="form-group">
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
			<i class="icon-filter"></i>
			<?php echo lang('global:filters'); ?>
		</button>
		
		<a href="<?php echo current_url() . '#'; ?>" class="btn btn-xs" type="reset">
			<i class="icon-remove"></i>
			<?php echo lang('buttons:clear'); ?>
		</a>
	</div>

<?php echo form_close() ?>

<?php if ($update_report['total'] > 0): ?>
	
	<p class="pull-right"><?php echo lang('kunjungan:showing').' '.count($update_report['entries']).' '.lang('kunjungan:of').' '.$update_report['total'] ?></p>
	
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th><?php echo lang('kunjungan:jadwal:plural'); ?></th>
				<th><?php echo lang('kunjungan:jadwal:id_petugas'); ?></th>
				<th><?php echo lang('kunjungan:update_report:status'); ?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
			if($cur_page != 0){
				if(isset($pagination_config['use_page_numbers']) AND $pagination_config['use_page_numbers'] === true) {
					$item_per_page = $pagination_config['per_page'];
					$no = (($cur_page -1) * $item_per_page) + 1;
				} else {
					$no = $cur_page+1;
				}
			}else{
				$no = 1;
			}
			?>
			
			<?php foreach ($update_report['entries'] as $update_report_entry): ?>
			<tr>
				<td><?php echo $no; $no++; ?></td>
				<td>
					<?php 
					echo $update_report_entry['nama_nasabah'].'<br />'
						.$update_report_entry['jenis_kunjungan'].'<br />'
						.$update_report_entry['tanggal'].' '.$update_report_entry['mulai'].' - '.$update_report_entry['selesai']; 
					?>	
				</td>
				<td><?php echo $update_report_entry['petugas_display_name']; ?></td>
				<td>
					<?php if($update_report_entry['memo'] == NULL){
						echo '<span class="label label-danger">'.lang('kunjungan:update_report:has_not_submitted').'</span>';
					}else{
						echo '<span class="label label-success">'.lang('kunjungan:update_report:has_submitted').'</span>';
					} ?>
				</td>
				
				<td class="actions">
				
				<?php 
				if(group_has_role('kunjungan', 'edit_all_update_report')){
					echo '<a href="#report-edit?id='.$update_report_entry['id'].'" class="btn btn-xs btn-info">'.lang('kunjungan:update_report:edit').'</a>';
				}elseif(group_has_role('kunjungan', 'edit_own_update_report')){
					if($update_report_entry['id_petugas'] == $this->current_user->id){
						echo '<a href="#report-edit?id='.$update_report_entry['id'].'" class="btn btn-xs btn-info">'.lang('kunjungan:update_report:edit').'</a>';
					}else{
						echo '<a href="#event-view?id='.$update_report_entry['id'].'" class="btn btn-xs btn-info">'.lang('kunjungan:view').'</a>';
					}
				}else{
					echo '<a href="#event-view?id='.$update_report_entry['id'].'" class="btn btn-xs btn-info">'.lang('kunjungan:view').'</a>';
				}
				?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	
	<?php echo $update_report['pagination']; ?>
	
<?php else: ?>
	<div class="well"><?php echo lang('kunjungan:update_report:no_entry'); ?></div>
<?php endif;?>

<div id="report-edit-modal" class="modal fade"></div>
<div id="event-view-modal" class="modal fade"></div>

<script type="text/javascript" src="<?php echo site_url('addons/default/modules/kunjungan/js/multimodal.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('addons/default/modules/kunjungan/js/jquery.getUrlParam.js'); ?>"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
		  $(this).prev().focus();
		});
	});

	// go back to previous page if selected modal is closed/hidden
 	$('#report-edit-modal, #event-view-modal').on('hidden.bs.modal', function (e) {
 		window.history.back();
 	});
 	
 	// show modal on hashchange
 	$(window).on('hashchange', function(e){
 	
 		// show edit form of report
 		if(window.location.href.indexOf('#report-edit') != -1){
 			$('#report-edit-modal').html('<div class="modal-dialog"><div class="modal-content"><div class="modal-body">Loading...</div></div></div>');
			var event_id = $(document).getUrlParam("id");
			$.ajax({
				url: BASE_URL+'c/kunjungan/update_report/edit/' + event_id,
				success: function(rst) {
					$("#report-edit-modal").html(rst);
				}
			});
			$('#report-edit-modal').modal();
 		}

 		// show jadwal view
 		if(window.location.href.indexOf('#event-view') != -1){
 			$('#event-view-modal').html('<div class="modal-dialog"><div class="modal-content"><div class="modal-body">Loading...</div></div></div>');
			var event_id = $(document).getUrlParam("id");
			$.ajax({
				url: BASE_URL+'c/kunjungan/jadwal/view/' + event_id,
				success: function(rst) {
					$("#event-view-modal").html(rst);
				}
			});
			$('#event-view-modal').modal();
 		}
 	
 	}).trigger('hashchange');
 	
 	// trigger hashchange event when a link/button is clicked
 	$(document).on('click', 'a', function(e){
 		if ($(this).attr('href') == window.location.hash) {
 			$(window).trigger('hashchange');
 		}
 	});
</script>