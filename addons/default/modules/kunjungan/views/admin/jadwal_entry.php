<div class="modal-dialog">
	<div class="modal-content">
		<div class="page-header modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h1 id="modal-header-title"><?php echo lang('kunjungan:jadwal:view'); ?></h1>
		</div>
		<div class="modal-body">
			<?php if($this->input->is_ajax_request()) { ?>

			<?php if($jadwal['memo'] == NULL){ ?>
			<div class="btn-group content-toolbar">

				<?php if(group_has_role('kunjungan', 'edit_all_jadwal')){ ?>
					<a href="<?php echo "#jadwal-edit?id=".$jadwal['id']; ?>" class="btn btn-sm btn-yellow hashchange">
						<i class="icon-edit"></i>
						<?php echo lang('global:edit') ?>
					</a>
				<?php }elseif(group_has_role('kunjungan', 'edit_own_jadwal')){ ?>
					<?php if($jadwal['created_by'] == $this->current_user->id 
						OR $jadwal['id_petugas'] == $this->current_user->id){ ?>

						<a href="<?php echo "#jadwal-edit?id=".$jadwal['id']; ?>" class="btn btn-sm btn-yellow hashchange">
							<i class="icon-edit"></i>
							<?php echo lang('global:edit') ?>
						</a>
					<?php } ?>
				<?php } ?>

				<?php if(group_has_role('kunjungan', 'delete_all_jadwal')){ ?>
					<a href="<?php echo site_url('c/kunjungan/jadwal/deletex/'.$jadwal['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
						<i class="icon-trash"></i>
						<?php echo lang('global:delete') ?>
					</a>
				<?php }elseif(group_has_role('kunjungan', 'delete_own_jadwal')){ ?>
					<?php if($jadwal['created_by'] == $this->current_user->id){ ?>
						<a href="<?php echo site_url('c/kunjungan/jadwal/deletex/'.$jadwal['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
							<i class="icon-trash"></i>
							<?php echo lang('global:delete') ?>
						</a>
					<?php } ?>
				<?php } ?>
			</div>
			<?php } ?>

			<div>
				<div class="row data-detail" style="display: none">
					<div class="data-detail-label col-xs-4">ID</div>
					<div class="data-detail-value col-xs-8"><?php echo $jadwal['id']; ?></div>
				</div>

				<div class="row data-detail first">
					<div class="data-detail-label col-xs-4"><?php echo lang('kunjungan:jadwal:jenis_kunjungan'); ?></div>
					<?php if(isset($jadwal['jenis_kunjungan'])){ ?>
					<div class="data-detail-value col-xs-8"><?php echo $jadwal['jenis_kunjungan']; ?></div>
					<?php }else{ ?>
					<div class="data-detail-value col-xs-8">-</div>
					<?php } ?>
				</div>

				<div class="row data-detail">
					<div class="data-detail-label col-xs-4"><?php echo lang('kunjungan:jadwal:id_nasabah'); ?></div>
					<?php if(isset($jadwal['id_nasabah'])){ ?>
					<div class="data-detail-value col-xs-8">
						<a href="<?php echo site_url('c/nasabah/nasabah/view/'.$jadwal['id_nasabah']); ?>"><?php echo $jadwal['nama_nasabah']; ?></a>
					</div>
					<?php }else{ ?>
					<div class="data-detail-value col-xs-8">-</div>
					<?php } ?>
				</div>

				<div class="row data-detail">
					<div class="data-detail-label col-xs-4"><?php echo lang('kunjungan:jadwal:id_petugas'); ?></div>
					<?php if(isset($jadwal['id_petugas'])){ ?>
					<div class="data-detail-value col-xs-8">
						<a href="<?php echo site_url('admin/users/view/'.$jadwal['id_petugas']); ?>"><?php echo $jadwal['petugas_display_name']; ?></a>
					</div>
					<?php }else{ ?>
					<div class="data-detail-value col-xs-8">-</div>
					<?php } ?>
				</div>

				<div class="row data-detail">
					<div class="data-detail-label col-xs-4"><?php echo lang('kunjungan:jadwal:tanggal'); ?></div>
					<?php if(isset($jadwal['tanggal'])){ ?>
					<div class="data-detail-value col-xs-8"><?php echo $jadwal['tanggal']; ?></div>
					<?php }else{ ?>
					<div class="data-detail-value col-xs-8">-</div>
					<?php } ?>
				</div>

				<div class="row data-detail">
					<div class="data-detail-label col-xs-4"><?php echo lang('kunjungan:jadwal:mulai'); ?>/<?php echo lang('kunjungan:jadwal:selesai'); ?></div>
					<?php if(isset($jadwal['mulai'])){ ?>
					<div class="data-detail-value col-xs-8"><?php echo $jadwal['mulai']; ?> - <?php echo $jadwal['selesai']; ?></div>
					<?php }else{ ?>
					<div class="data-detail-value col-xs-8">-</div>
					<?php } ?>
				</div>

				<div class="row data-detail">
					<div class="data-detail-label col-xs-4"><?php echo lang('kunjungan:jadwal:deskripsi'); ?></div>
					<?php if(isset($jadwal['deskripsi']) and $jadwal['deskripsi']!=''){ ?>
					<div class="data-detail-value col-xs-8"><?php echo $jadwal['deskripsi']; ?></div>
					<?php }else{ ?>
					<div class="data-detail-value col-xs-8">-</div>
					<?php } ?>
				</div>

				<div class="row data-detail">
					<div class="data-detail-label col-xs-4"><?php echo lang('kunjungan:created'); ?></div>
					<?php if(isset($jadwal['created_on'])){ ?>
					<div class="data-detail-value col-xs-8"><?php echo format_date($jadwal['created_on'], 'd-m-Y G:i'); ?></div>
					<?php }else{ ?>
					<div class="data-detail-value col-xs-8">-</div>
					<?php } ?>
				</div>

				<div class="row data-detail">
					<div class="data-detail-label col-xs-4"><?php echo lang('kunjungan:updated'); ?></div>
					<?php if(isset($jadwal['updated_on'])){ ?>
					<div class="data-detail-value col-xs-8"><?php echo format_date($jadwal['updated_on'], 'd-m-Y G:i'); ?></div>
					<?php }else{ ?>
					<div class="data-detail-value col-xs-8">-</div>
					<?php } ?>
				</div>

				<div class="row data-detail">
					<div class="data-detail-label col-xs-4"><?php echo lang('kunjungan:created_by'); ?></div>
					<div class="data-detail-value col-xs-8"><?php echo user_displayname($jadwal['created_by'], true); ?></div>
				</div>

				<div class="row data-detail">
					<div class="data-detail-label col-xs-4"><?php echo lang('kunjungan:update_report:singular'); ?></div>
					<div class="data-detail-value col-xs-8">
						<?php if($jadwal['memo'] == NULL){
							echo '<i>'.lang('kunjungan:jadwal:memo_empty').'</i>';
						}else{
							echo $jadwal['memo'];
						}

						if($jadwal['id_petugas'] == $this->current_user->id
							AND strtotime($jadwal['tanggal'].' '.$jadwal['mulai']) <= time()){
							
							echo '<br /><strong>[</strong><a href="#report-create?event_id='.$jadwal['id'].'">Update Laporan</a><strong>]</strong>';
						} ?>
					</div>
				</div>
			</div>

			<?php } ?>
		</div>
	</div>
</div>