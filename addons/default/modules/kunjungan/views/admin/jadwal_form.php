<?php echo Asset::css('datepicker.css'); ?>
<?php echo Asset::css('bootstrap-timepicker.css'); ?>

<?php echo Asset::js('date-time/bootstrap-datepicker.js'); ?>
<?php echo Asset::js('date-time/bootstrap-timepicker.js'); ?>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="page-header modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h1 id="modal-header-title"><?php echo lang('kunjungan:jadwal:'.$mode); ?></h1>
		</div>
		<div class="modal-body">
			<?php echo form_open_multipart(uri_string().$uri, array('id'=>'form-jadwal')); ?>

			<div class="form-horizontal">

				<div class="form-group">
					<label class="col-sm-4 control-label no-padding-right" for="tanggal"><?php echo lang('kunjungan:jadwal:tanggal'); ?></label>

					<div class="col-sm-8">
						<?php 
							$value = NULL;
							if($this->input->post('tanggal') != NULL){
								$value = $this->input->post('tanggal');
							}elseif($this->input->get('f-tanggal') != NULL AND $mode == 'new'){
								$value = $this->input->get('f-tanggal');
							}elseif($mode == 'edit'){
								$value = $fields['tanggal'];
							}
						?>
						<div class="col-xs-12 col-sm-8 no-padding-left no-padding-right">
							<div class="input-group">
								<input class="form-control date-picker" name="tanggal" type="text" data-date-format="yyyy-mm-dd" value="<?php echo $value; ?>" autocomplete="off">
								<span class="input-group-addon">
									<i class="icon-calendar bigger-110"></i>
								</span>
							</div>
						</div>
						
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label no-padding-right" for="mulai"><?php echo lang('kunjungan:jadwal:jam'); ?> <?php echo lang('kunjungan:jadwal:mulai'); ?>/<?php echo lang('kunjungan:jadwal:selesai'); ?></label>

					<div class="col-sm-8 no-padding-left">
						<div class="col-sm-4 no-padding-right">
							<?php 
							$value = NULL;
							if($this->input->post('mulai') != NULL){
								$value = $this->input->post('mulai');
							}elseif($this->input->get('f-mulai') != NULL AND $mode == 'new'){
								$value = $this->input->get('f-mulai');
							}elseif($mode == 'edit'){
								$value = $fields['mulai'];
							}
							?>

							<div class="input-group bootstrap-timepicker">
								<input type="text" name="mulai" value="<?php echo $value; ?>" id="mulai" class="form-control timepicker" />
								<span class="input-group-addon"><i class="icon-time"></i></span>
							</div>
						</div>
						<div class="col-sm-4 no-padding-right">
							<?php 
							$value = NULL;
							if($this->input->post('selesai') != NULL){
								$value = $this->input->post('selesai');
							}elseif($this->input->get('f-selesai') != NULL AND $mode == 'new'){
								$value = $this->input->get('f-selesai');
							}elseif($mode == 'edit'){
								$value = $fields['selesai'];
							}
							?>
							<div class="input-group bootstrap-timepicker">
								<input type="text" name="selesai" value="<?php echo $value; ?>" id="selesai" class="form-control timepicker" />
								<span class="input-group-addon"><i class="icon-time"></i></span>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label no-padding-right" for="jenis_kunjungan"><?php echo lang('kunjungan:jadwal:jenis_kunjungan'); ?></label>

					<div class="col-sm-8">
						<?php 
							$value = NULL;
							if($this->input->post('jenis_kunjungan') != NULL){
								$value = $this->input->post('jenis_kunjungan');
							}elseif($this->input->get('f-jenis_kunjungan') != NULL AND $mode == 'new'){
								$value = $this->input->get('f-jenis_kunjungan');
							}elseif($mode == 'edit'){
								$value = $fields['jenis_kunjungan'];
							}
						?>
						
						<div class="col-xs-12 col-sm-8 no-padding">
							<select class="form-control" name="jenis_kunjungan">
								<option value=""><?php echo lang('kunjungan:choose').' '.lang('kunjungan:jadwal:jenis_kunjungan'); ?></option>
								<?php $jenis_kunjungan = array('Tatap Muka','Telpon','Email'); foreach ($jenis_kunjungan as $jenis_kunjungan_entry) {
									$selected = '';
									if($jenis_kunjungan_entry==$value) {
										$selected = 'selected="selected"';
									}

									echo '<option value="'.$jenis_kunjungan_entry.'" '.$selected.'>'.$jenis_kunjungan_entry.'</option>';
								} ?>
							</select>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label no-padding-right" for="id_nasabah"><?php echo lang('kunjungan:jadwal:id_nasabah'); ?></label>

					<div class="col-sm-8">
						<?php 
							$value = NULL;
							if($this->input->post('id_nasabah') != NULL){
								$value = $this->input->post('id_nasabah');
							}elseif($this->input->get('f-id_nasabah') != NULL AND $mode == 'new'){
								$value = $this->input->get('f-id_nasabah');
							}elseif($mode == 'edit'){
								$value = $fields['id_nasabah'];
							}
						?>
						<div id="scrollable-dropdown-menu" class="col-xs-12 col-sm-8 no-padding">
							<select class="form-control" id="select-id-nasabah" name="id_nasabah">
								<option value=""><?php echo lang('kunjungan:choose').' '.lang('kunjungan:jadwal:id_nasabah'); ?></option>
								<?php foreach ($nasabah as $nasabah_entry) {
									$selected = '';
									if($nasabah_entry['id']==$value) {
										$selected = 'selected="selected"';
									}

									echo '<option value="'.$nasabah_entry['id'].'" '.$selected.'>'.$nasabah_entry['nama'].'</option>';
								} ?>
							</select>
						</div>
						
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-4 control-label no-padding-right" for="id_petugas"><?php echo lang('kunjungan:jadwal:id_petugas'); ?></label>

					<div class="col-sm-8">
						<?php 
							$value = NULL;
							if($this->input->post('id_petugas') != NULL){
								$value = $this->input->post('id_petugas');
							}elseif($this->input->get('f-id_petugas') != NULL AND $mode == 'new'){
								$value = $this->input->get('f-id_petugas');
							}elseif($mode == 'edit'){
								$value = $fields['id_petugas'];
							}
						?>
						<div class="col-xs-12 col-sm-8 no-padding">
							<select class="form-control" name="id_petugas">
								<?php if(count($petugas)>1) { ?>
									<option value=""><?php echo lang('kunjungan:choose').' '.lang('kunjungan:jadwal:id_petugas'); ?></option>
								<?php } ?>
								<?php foreach ($petugas as $petugas_entry) {
									$selected = '';
									if($petugas_entry->id==$value) {
										$selected = 'selected="selected"';
									}

									echo '<option value="'.$petugas_entry->id.'" '.$selected.'>'.$petugas_entry->display_name.'</option>';
								} ?>
							</select>
						</div>
						
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-4 control-label no-padding-right" for="deskripsi"><?php echo lang('kunjungan:jadwal:deskripsi'); ?></label>

					<div class="col-sm-8">
						<?php 
							$value = NULL;
							if($this->input->post('deskripsi') != NULL){
								$value = $this->input->post('deskripsi');
							}elseif($this->input->get('f-deskripsi') != NULL AND $mode == 'new'){
								$value = $this->input->get('f-deskripsi');
							}elseif($mode == 'edit'){
								$value = $fields['deskripsi'];
							}
						?>
						
						<textarea name="deskripsi" rows="4" class="col-xs-12 col-sm-8" id="" ><?php echo $value; ?></textarea>
					</div>
				</div>
				
			</div>

			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
					<a href="<?php echo site_url($return); ?>" class="btn btn-danger btn-cancel"><?php echo lang('buttons:cancel'); ?></a>
				</div>
			</div>

			<?php echo form_close();?>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.date-picker').datepicker({
			autoclose:true,
			dateFormat: 'yy-mm-dd',
			setDate:new Date(),
		}
		).next().on(ace.click_event, function(){
			$(this).prev().focus();
		});

		$('.timepicker').timepicker({
			showSeconds: false,
			showMeridian: false
		});

		$('#form-jadwal').find('select').chosen({
			disable_search_threshold: 4,
			width: "100%"
		});

		$('#form-jadwal').submit(function(e) {
			var modal = $(this).closest('.modal.in');
			if(modal.length > 0) {
				e.preventDefault();

				var submitButton = modal.find('button[type=submit]');
				submitButton.html('<span>Submitting...</span>')
					.attr("disabled", true);
				modal.animate({
	        		scrollTop: submitButton.offset().top
		    	}, 500);

				var formData = new FormData($(this)[0]);
				$('.alert').remove();
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					dataType: 'json',
					success: function (msg) {
						if(msg.status=='error') {
							modal.find('.modal-body').prepend('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+msg.messages+'</div>');
							modal.animate({
								scrollTop: 0
						    }, 500);
						} else {
							modal.find('.modal-body').prepend('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>'+msg.messages+'</div>');
							
							modal.animate({
						        scrollTop: 0
						    }, 500);

							setTimeout(function() {
								modal.modal('hide');
								modal.find(".modal-body").html('');

								
							},2000);
						}

						modal.find('button[type=submit]')
							.html('<span><?php echo lang('buttons:save'); ?></span>')
							.attr("disabled", false);
					},
					cache: false,
					contentType: false,
					processData: false
				});
			} else {
			}
		});

		$('.btn-cancel').click(function(e) {
			var modal = $(this).closest('.modal.in');
			if(modal.length > 0) {
				e.preventDefault();
				$(this).closest('.modal.in').modal('hide');
			}
		});

		$(document).on('hidden.bs.modal', '.modal', function () {
			var modalData = $(this).data('bs.modal');

			// Destroy modal if has remote source – don't want to destroy modals with static content.
			if (modalData && modalData.options.remote) {
				// Destroy component. Next time new component is created and loads fresh content
				$(this).removeData('bs.modal');
				// Also clear loaded content, otherwise it would flash before new one is loaded.
				$(this).find(".modal-content").html('<p>Loading...</p>');
			}
		});
	});
</script>
