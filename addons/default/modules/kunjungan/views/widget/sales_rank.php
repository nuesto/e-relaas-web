<!-- start browser widget -->
<div class="widget-body no-padding">
	<table class="table table-bordered-inside no-margin">
		<thead class="thin-border-bottom">
			<tr>
				<th>No</th>
				<th>Sales</th>
				<th>Jumlah Call</th>
				<th>Update Report</th>
			</tr>
		</thead>

		<tbody>
			<?php foreach($jumlah_call_by_update as $key => $sales) { ?>
				<tr>
					<td><?php echo $key+1; ?></td>
					<td><?php echo $sales['petugas_display_name']; ?></td>
					<td><?php echo $sales['jumlah_jadwal']; ?></td>
					<td><?php echo $sales['update_report']; ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<!-- end browser widget -->