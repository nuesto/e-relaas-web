<!-- start browser widget -->
<div class="widget-body row">
	<div class="col-xs-12 center">
		<div>
			<span class="profile-picture">
				<?php if(isset($jumlah_call_by_update[0]['picture_id']) and $jumlah_call_by_update[0]['picture_id']!='') { ?>
						<img class="img-responsive" src="<?php echo base_url().'files/thumb/'.$jumlah_call_by_update[0]['picture_id'].'/190/210/fit'; ?>" class="img-responsive">
					<?php } else { ?>
						<img class="img-responsive" src="<?php echo base_url().'system/cms/themes/ace/img/default_avatar_190x210.png'; ?>" class="img-responsive">
					<?php } ?>
			</span>

			<div class="space-4"></div>

			<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
				<div class="inline position-relative">
					<i class="icon-star orange2 middle"></i>
					&nbsp;
					<span class="white">
						<?php echo 'No #1 Sales in '.$bulan; ?>
					</span>
				</div>
			</div>
		</div>

		<div class="space-6"></div>

		<div class="profile-contact-info">
			<div class="profile-contact-links align-left">
				<span class="btn btn-link" href="#">
					<i class="icon-user bigger-120 blue"></i>
					<?php 
					if(isset($jumlah_call_by_update[0]['petugas_display_name'])){
						echo $jumlah_call_by_update[0]['petugas_display_name'];
					} 
					?>
				</span>
				<br />
				<span class="btn btn-link" href="#">
					<i class="icon-phone bigger-120 green"></i>
					<?php echo (isset($jumlah_call_by_update[0]['no_hp']) AND $jumlah_call_by_update[0]['no_hp'] != '') ? $jumlah_call_by_update[0]['no_hp'] : '-'; ?>
				</span>
				<br />
				<span class="btn btn-link" href="#">
					<i class="icon-envelope bigger-120 pink"></i>
					<?php echo (isset($jumlah_call_by_update[0]['email'])) ? $jumlah_call_by_update[0]['email'] : '-'; ?>
				</span>
			</div>

			<div class="space-6"></div>
		</div>

		<div class="clearfix">
			<div class="grid2">
				<span class="bigger-175 blue">
					<?php echo (isset($jumlah_call_by_update[0]['jumlah_jadwal'])) ? $jumlah_call_by_update[0]['jumlah_jadwal'] : '-'; ?>
				</span>

				<br>
				Jadwal kunjungan
			</div>

			<div class="grid2">
				<span class="bigger-175 blue">
					<?php echo (isset($jumlah_call_by_update[0]['update_report'])) ? $jumlah_call_by_update[0]['update_report'] : '-'; ?>
				</span>

				<br>
				Update report
			</div>
		</div>
	</div>
</div>
<!-- end browser widget -->