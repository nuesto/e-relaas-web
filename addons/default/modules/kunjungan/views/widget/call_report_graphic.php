<div class="widget-body row">
	<div class="widget-main no-padding" id="container">
		<?php
		if(count($chart['categories'])==0) {
			echo '<div class="well">'.lang('kunjungan:jadwal:no_entry').'</div>';
		}
		?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		Highcharts.chart('container', {
			chart: {
				type: 'column'
			},
			title: {
				text: ''
			},
			xAxis: {
				categories: <?php echo json_encode($chart['categories']) ?>
			},
			yAxis: [{
				min: 0,
				title: {
					text: 'Jumlah Call'
				}
			}],
			legend: {
				shadow: false
			},
			tooltip: {
				shared: true
			},
			plotOptions: {
				column: {
					grouping: false,
					shadow: false,
					borderWidth: 0
				}
			},
			series: [{
				name: 'Jumlah Call',
				color: 'rgba(165,170,217,1)',
				data: <?php echo json_encode($chart['jumlah_call']) ?>,
				pointPadding: 0.3,
				pointPlacement: -0.2
			}, {
				name: 'Update Report',
				color: 'rgba(126,86,134,.9)',
				data: <?php echo json_encode($chart['update_report']) ?>,
				pointPadding: 0.4,
				pointPlacement: -0.2
			}]
		});
	});
</script>