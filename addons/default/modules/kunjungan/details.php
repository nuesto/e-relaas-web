<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Kunjungan extends Module
{
    public $version = '1.0.4';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Kunjungan',
			'id' => 'Kunjungan',
		);
		$info['description'] = array(
			'en' => 'Mengelola daftar kunjungan dan memo',
			'id' => 'Mengelola daftar kunjungan dan memo',
		);
		$info['frontend'] = false;
		$info['backend'] = true;
		$info['menu'] = 'kunjungan';
		$info['roles'] = array(
			'access_jadwal_backend', 'view_all_jadwal', 'view_own_jadwal', 'edit_all_jadwal', 'edit_own_jadwal', 'delete_all_jadwal', 'delete_own_jadwal', 'create_jadwal',
			'access_update_report_backend', 'view_all_update_report', 'view_own_update_report', 'edit_all_update_report', 'edit_own_update_report', 'delete_all_update_report', 'delete_own_update_report', 'create_update_report'
		);
		
		if(group_has_role('kunjungan', 'access_jadwal_backend')){
			$info['sections']['jadwal']['name'] = 'kunjungan:jadwal:plural';
			$info['sections']['jadwal']['uri'] = array('urls'=>array('c/kunjungan/jadwal/index','c/kunjungan/jadwal/create','c/kunjungan/jadwal/view%1','c/kunjungan/jadwal/edit%1','c/kunjungan/jadwal/view%2','c/kunjungan/jadwal/edit%2'));
			
			if(group_has_role('kunjungan', 'create_jadwal')){
				$info['sections']['jadwal']['shortcuts']['create'] = array(
					'name' => 'kunjungan:jadwal:new',
					'uri' => 'c/kunjungan/jadwal/create',
					'class' => 'add',
					'id' => 'addJadwal'
				);
			}
		}
		
		if(group_has_role('kunjungan', 'access_update_report_backend')){
			$info['sections']['update_report']['name'] = 'kunjungan:update_report:plural';
			$info['sections']['update_report']['uri'] = array('urls'=>array('c/kunjungan/update_report/index','c/kunjungan/update_report/create','c/kunjungan/update_report/view%1','c/kunjungan/update_report/edit%1','c/kunjungan/update_report/view%2','c/kunjungan/update_report/edit%2'));
			$info['sections']['update_report']['count_notif'] = array(
				'module' => 'kunjungan',
				'library' => 'kunjungan',
				'method' => 'count_my_unreported_jadwal',
			);
			
			if(group_has_role('kunjungan', 'create_update_report')){
				$info['sections']['update_report']['shortcuts']['create'] = array(
					'name' => 'kunjungan:update_report:new',
					'uri' => 'c/kunjungan/update_report/create',
					'class' => 'add',
					'id' => 'addUpdateReport'
				);
			}
		}
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items){
		
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// jadwal
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'tanggal' => array(
				'type' => 'DATE',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'mulai' => array(
				'type' => 'TIME',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'selesai' => array(
				'type' => 'TIME',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'jenis_kunjungan' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'id_nasabah' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'id_petugas' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'memo' => array(
				'type' => 'TEXT',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'waktu_berangkat' => array(
				'type' => 'DATETIME',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'waktu_kembali' => array(
				'type' => 'DATETIME',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('kunjungan_jadwal', TRUE);
		$this->db->query("CREATE INDEX author_kunjungan_jadwal_idx ON ".$this->db->dbprefix("kunjungan_jadwal")."(created_by)");

		// edit this query to add foreign key
		/*$this->db->query("CREATE INDEX fk_kunjungan_jadwal_ref_idx ON ".$this->db->dbprefix("kunjungan_jadwal")."(id_ref)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("kunjungan_jadwal")." 
			ADD CONSTRAINT `fk_kunjungan_jadwal_ref` 
			FOREIGN KEY (`id_ref`) 
			REFERENCES ".$this->db->dbprefix("kunjungan_ref")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");*/

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		/********************************************************
		 * delete all widget instance
		 ********************************************************/
		$this->load->library('widgets/widgets');
		$this->load->model('widgets/widget_m');

		$status = TRUE;

		$this->db->trans_begin();

		// get widget dashboard
		$widget_dashboard = $this->widgets->get_widget('dashboard');

		if ($widget_dashboard != NULL)
		{
			// create instance
			$instances = array(
				array(
					'title' => 'No #1 Sales',
					'widget_id' => $widget_dashboard->id,
				),
				array(
					'title' => 'Peringkat Update Call Bulan Ini',
					'widget_id' => $widget_dashboard->id,
				),
			);

			foreach ($instances as $instance) {
				$this->widget_m->delete_instance_by_params($instance);
			}
		}else{
			$status = FALSE;
		}
		
		if ($this->db->trans_status() === FALSE OR $status == FALSE){
			$status = FALSE;
			$this->db->trans_rollback();
		}else{
			$this->db->trans_commit();
		}

		/********************************************************
		 * drop kunjungan_jadwal table
		 ********************************************************/
		if ($status == TRUE) {
			$this->load->dbforge();
        	$this->dbforge->drop_table('kunjungan_jadwal');	
		}

        return $status;
    }

    public function upgrade($old_version)
    {
    	switch ($old_version) {
    		case '1.0.0':
    			$fields = array(
    				'deskripsi' => array(
    					'type' => 'TEXT',
    					'constraint' => NULL,
    					'unsigned' => FALSE,
    					'default' => NULL,
    					'null' => TRUE,
    					'auto_increment' => FALSE,
    				)
    			);

    			$this->dbforge->add_column('kunjungan_jadwal', $fields,'jenis_kunjungan');

    			$this->version = '1.0.1';
    			break;

    		case '1.0.1':

    			$this->load->library('widgets/widgets');

    			$this->db->trans_begin();
    			
    			// get widget dashboard
				$widget = $this->widgets->get_widget('dashboard');
				// create area dashboard right
				$area = $this->widgets->get_area('dashboard-right');
				if ($area != NULL AND $widget != NULL)
				{
					// create instance
					$instances = array(
						array(
							'title' => 'No #1 Sales',
							'view_uri' => 'c/kunjungan/jadwal/widget/sales_champion',
							'color_scheme' => 'success',
							'show_title' => '1',
							'has_container' => '1',
							'widget_area_id' => $area->id,
							'widget_id' => $widget->id,
						),
					);

					foreach ($instances as $instance) {
						$this->widgets->add_instance($instance['title'], $instance['widget_id'], $instance['widget_area_id'], $instance);
					}
				}
    			
    			if ($this->db->trans_status() === FALSE){
    				$this->version = '1.0.1';
    				$this->db->trans_rollback();
    			}else{
    				$this->version = '1.0.2';
    				$this->db->trans_commit();
    			}

    			break;

    		case '1.0.2':

    			$this->load->library('widgets/widgets');

    			$this->db->trans_begin();
    			
    			// get widget dashboard
				$widget = $this->widgets->get_widget('dashboard');
				// get area dashboard
				$area = $this->widgets->get_area('dashboard-left');
				if ($area != NULL AND $widget != NULL)
				{
					// create instance
					$instances = array(
						array(
							'title' => 'Peringkat Update Call Bulan Ini',
							'view_uri' => 'c/kunjungan/jadwal/widget/sales_rank',
							'color_scheme' => 'success',
							'show_title' => '1',
							'has_container' => '1',
							'widget_area_id' => $area->id,
							'widget_id' => $widget->id,
						),
					);

					foreach ($instances as $instance) {
						$this->widgets->add_instance($instance['title'], $instance['widget_id'], $instance['widget_area_id'], $instance);
					}
				}
    			
    			if ($this->db->trans_status() === FALSE){
    				$this->version = '1.0.2';
    				$this->db->trans_rollback();
    			}else{
    				$this->version = '1.0.3';
    				$this->db->trans_commit();
    			}

    			break;

    		case '1.0.3':

    			$this->load->library('widgets/widgets');

    			$this->db->trans_begin();
    			
    			// get widget dashboard
				$widget = $this->widgets->get_widget('dashboard');
				// get area dashboard
				$area = $this->widgets->get_area('dashboard-left');
				if ($area != NULL AND $widget != NULL)
				{
					// create instance
					$instances = array(
						array(
							'title' => 'Call & Report Bulan Ini',
							'view_uri' => 'c/kunjungan/jadwal/widget/call_report_graphic',
							'color_scheme' => 'success',
							'show_title' => '1',
							'has_container' => '1',
							'widget_area_id' => $area->id,
							'widget_id' => $widget->id,
						),
					);

					foreach ($instances as $instance) {
						$this->widgets->add_instance($instance['title'], $instance['widget_id'], $instance['widget_area_id'], $instance);
					}
				}
    			
    			if ($this->db->trans_status() === FALSE){
    				$this->version = '1.0.3';
    				$this->db->trans_rollback();
    			}else{
    				$this->version = '1.0.4';
    				$this->db->trans_commit();
    			}

    			break;
    		
    		default:
    			return FALSE;
    			break;
    	}
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}