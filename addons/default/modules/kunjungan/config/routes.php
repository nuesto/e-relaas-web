<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['kunjungan/api(/:any)?']		= 'api_kunjungan$1';
$route['kunjungan/c/jadwal(:any)'] = 'admin_jadwal$1';
$route['kunjungan/c/update_report(:any)'] = 'admin_update_report$1';
