<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Kunjungan Module
 *
 * Mengelola daftar kunjungan dan memo
 *
 */
class Admin_update_report extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'update_report';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'access_update_report_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('kunjungan');		
		$this->load->model('jadwal_m');
		$this->load->model('nasabah/nasabah_m');
    }

    /**
	 * List all update_report
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'view_all_update_report') AND ! group_has_role('kunjungan', 'view_own_update_report')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$cra_group = $this->group_m->get_group_by_slug('cra');
		$id_cra_group = $cra_group->id;
		$data['petugas'] = $this->user_m->get_many_by(array('user_id' => $this->current_user->id));
		if($this->current_user->group_id==$id_cra_group) {
			$crm_group = $this->group_m->get_group_by_slug('crm');
			$id_crm_group = $crm_group->id;
			$data['petugas'] = array_merge($data['petugas'], $this->user_m->get_many_by(array('group_id' => $id_crm_group, 'id_cra' => $this->current_user->id)));
		}

		// handle filter get params
		$params = array();
		if($this->input->get('f-nasabah')) {
			$params['nasabah_nasabah.nama'] = array('type'=>'like','value'=>$this->input->get('f-nasabah'));
		}
		if($this->input->get('f-jenis_kunjungan')) {
			$params['jenis_kunjungan'] = array('type'=>'like','value'=>$this->input->get('f-jenis_kunjungan'));
		}
		if(group_has_role('kunjungan','view_all_update_report')){
			if($this->input->get('f-id_petugas') != NULL){
				$params['id_petugas'] = $this->input->get('f-id_petugas');
			}
		}else{
			if(group_has_role('kunjungan','view_own_update_report')) {
				$params['id_petugas'] = array(
					'type' => 'where',
					'value' => array(),
				);
				foreach ($data['petugas'] as $key => $value) {
					if($this->input->get('f-id_petugas') == NULL OR $this->input->get('f-id_petugas') == $this->current_user->id){
						$params['id_petugas']['value'][] = $value->id;
					}
				}
			}
		}

		if($this->input->get('f-id_petugas')) {
			$params['id_petugas'] = $this->input->get('f-id_petugas');
			unset($params['kunjungan_jadwal.created_by']);
		}

		if($this->input->get('f-status_report')) {
			if($this->input->get('f-status_report') == 'has_submitted'){
				$params['memo'] = 'NOT NULL';
			}else{
				$params['memo'] = 'NULL';
			}
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'c/kunjungan/update_report/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->jadwal_m->count_all_jadwal($params);
		$pagination_config['per_page'] = Settings::get('records_per_page');;
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['update_report']['entries'] = $this->jadwal_m->get_jadwal($pagination_config, $params);
		$data['update_report']['total'] = $pagination_config['total_rows'];
		$data['update_report']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['update_report']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('c/kunjungan/update_report/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('c/kunjungan/update_report/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('kunjungan:update_report:plural'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('kunjungan:update_report:plural'))
			->append_css('module::kunjungan.css')
			->append_js('module::kunjungan.js')
			->build('admin/update_report_index', $data);
    }
	
	/**
     * Display one update_report
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'view_all_update_report') AND ! group_has_role('kunjungan', 'view_own_update_report')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['update_report'] = $this->jadwal_m->get_updatejadwal($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'view_all_update_report')){
			if($data['update_report']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		if($this->input->is_ajax_request()) {
			$this->template->set_layout(false);
		}
        $this->template->title(lang('kunjungan:update_report:view'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('kunjungan', 'view_all_update_report') OR group_has_role('kunjungan', 'view_own_update_report')){
			$this->template->set_breadcrumb(lang('kunjungan:update_report:plural'), '/c/kunjungan/update_report/index');
		}

		$this->template->set_breadcrumb(lang('kunjungan:update_report:view'))
			->append_css('module::kunjungan.css')
			->append_js('module::kunjungan.js')
			->build('admin/update_report_entry', $data);
    }
	
	/**
     * Create a new update_report entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($event_id = NULL)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'create_update_report')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_update_report('new')){	
				$this->session->set_flashdata('success', lang('kunjungan:update_report:submit_success'));
				if($this->input->is_ajax_request()) {
					$result = array('status'=>'success','messages'=>lang('kunjungan:update_report:submit_success'));
					echo json_encode($result);
					exit();
				}
				redirect('c/kunjungan/update_report/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('kunjungan:update_report:submit_failure');
				if($this->input->is_ajax_request()) {
					$result = array('status'=>'error','messages'=>validation_errors());
					echo json_encode($result);
					exit();
				}
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/kunjungan/update_report/index'.$data['uri'];

		$params['kunjungan_jadwal.id'] = array('type'=>'where', 'value'=>$event_id);
		$data['jadwal'] = $this->jadwal_m->get_jadwal(null, $params);
		$data['fields'] = $data['jadwal'][0];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		if($this->input->is_ajax_request()) {
			$this->template->set_layout(false);
		}
        $this->template->title(lang('kunjungan:update_report:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('kunjungan', 'view_all_update_report') OR group_has_role('kunjungan', 'view_own_update_report')){
			$this->template->set_breadcrumb(lang('kunjungan:update_report:plural'), '/c/kunjungan/update_report/index');
		}

		$this->template->set_breadcrumb(lang('kunjungan:update_report:new'))
			->append_css('module::kunjungan.css')
			->append_js('module::kunjungan.js')
			->build('admin/update_report_form', $data);
    }
	
	/**
     * Edit a update_report entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the update_report to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'edit_all_update_report') AND ! group_has_role('kunjungan', 'edit_own_update_report')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('kunjungan', 'edit_all_update_report')){
			$entry = $this->jadwal_m->get_jadwal_by_id($id);
			$created_by_user_id = $entry['created_by'];
			$petugas_user_id = $entry['id_petugas'];
			if($created_by_user_id != $this->current_user->id AND $petugas_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_update_report('edit', $id)){	
				$this->session->set_flashdata('success', lang('kunjungan:update_report:submit_success'));
				if($this->input->is_ajax_request()) {
					$result = array('status'=>'success','messages'=>lang('kunjungan:update_report:submit_success'));
					echo json_encode($result);
					exit();
				}
				redirect('c/kunjungan/update_report/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('kunjungan:update_report:submit_failure');
				if($this->input->is_ajax_request()) {
					$result = array('status'=>'error','messages'=>validation_errors());
					echo json_encode($result);
					exit();
				}
			}
		}
		
		$data['fields'] = $this->jadwal_m->get_jadwal_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'c/kunjungan/update_report/index/'.$data['uri'];
		$data['entry_id'] = $id;

		$cra_group = $this->group_m->get_group_by_slug('cra');
		$id_cra_group = $cra_group->id;
		$data['petugas'] = $this->user_m->get_many_by(array('user_id' => $this->current_user->id));
		if($this->current_user->group_id==$id_cra_group) {
			$crm_group = $this->group_m->get_group_by_slug('crm');
			$id_crm_group = $crm_group->id;
			$data['petugas'] = array_merge($data['petugas'], $this->user_m->get_many_by(array('group_id' => $id_crm_group, 'id_cra' => $this->current_user->id)));
		}

		if(!group_has_role('kunjungan','view_all_jadwal') and group_has_role('kunjungan','view_own_jadwal')) {
			$params['id_petugas'] = $this->current_user->id;
			if($this->current_user->group_id==$id_cra_group) {
				$params['kunjungan_jadwal.created_by'] = array('type'=>'or_where', 'value'=>$this->current_user->id);
			}
		}

		$data['jadwal'] = $this->jadwal_m->get_jadwal(null, $params);
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		if($this->input->is_ajax_request()) {
			$this->template->set_layout(false);
		}
        $this->template->title(lang('kunjungan:update_report:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('kunjungan', 'view_all_update_report') OR group_has_role('kunjungan', 'view_own_update_report')){
			$this->template->set_breadcrumb(lang('kunjungan:update_report:plural'), '/c/kunjungan/update_report/index');
		}else{
			$this->template->set_breadcrumb(lang('kunjungan:update_report:plural'))
			->set_breadcrumb(lang('kunjungan:update_report:view'));
		}

		$this->template->set_breadcrumb(lang('kunjungan:update_report:edit'))
			->append_css('module::kunjungan.css')
			->append_js('module::kunjungan.js')
			->build('admin/update_report_form', $data);
    }
	
	/**
     * Delete a update_report entry
     * 
     * @param   int [$id] The id of update_report to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'delete_all_update_report') AND ! group_has_role('kunjungan', 'delete_own_update_report')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('kunjungan', 'delete_all_update_report')){
			$entry = $this->jadwal_m->get_jadwal_by_id($id);
			$created_by_user_id = $entry['created_by'];
			$petugas_user_id = $entry['id_petugas'];
			if($created_by_user_id != $this->current_user->id AND $petugas_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->jadwal_m->delete_jadwal_by_id($id);
        $this->session->set_flashdata('error', lang('kunjungan:update_report:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('c/kunjungan/update_report/index'.$data['uri']);
    }
	
	/**
     * Insert or update update_report entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_update_report($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('id', lang('kunjungan:jadwal:singular'), 'required');
		$this->form_validation->set_rules('memo', lang('kunjungan:jadwal:memo'), 'required|max_length[2000]');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			// reformat
			$row_id = $values['id'];
			unset($values['id']);
			
			$result = $this->jadwal_m->update_jadwal($values, $row_id);
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	function _valid_mysql_date($input){
		if($input == NULL OR $input == ''){
			return TRUE;
		}

		if(preg_match('/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])$/', $input)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function _valid_mysql_time($input){
		if($input == NULL OR $input == ''){
			return TRUE;
		}

		if(preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/", $input)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function get_update_report()
	{
		$params = array();
		if($this->input->get('start')) {
			$params['tanggal >='] = $this->input->get('start');
		}
		if($this->input->get('end')) {
			$params['tanggal <='] = $this->input->get('end');
		}
		if($this->input->get('nasabah')) {
			$params['nasabah_nasabah.nama'] = array('type'=>'like','value'=>$this->input->get('nasabah'));
		}
		if($this->input->get('jenis_kunjungan')) {
			$params['jenis_kunjungan'] = array('type'=>'like','value'=>$this->input->get('jenis_kunjungan'));
		}
		if(!group_has_role('kunjungan','view_all_update_report') and group_has_role('kunjungan','view_own_update_report')) {
			$params['id_petugas'] = $this->current_user->id;
			$params['kunjungan_jadwal.created_by'] = array('type'=>'or_where', 'value'=>$this->current_user->id);
		}

		if($this->input->get('id_petugas')) {
			$params['id_petugas'] = $this->input->get('id_petugas');
			unset($params['kunjungan_jadwal.created_by']);
		}
		$update_report = $this->jadwal_m->get_jadwal(NULL, $params);

		$events = array();
		foreach ($update_report as $key => $value) {
			$events[$key]['id'] = $value['id'];
			$events[$key]['title'] = $value['nama_nasabah'];
			$events[$key]['start'] = $value['tanggal'].'T'.$value['mulai'];
			$events[$key]['end'] = $value['tanggal'].'T'.$value['selesai'];
			$events[$key]['editable'] = true;
			$events[$key]['viewable'] = true;

			if(!group_has_role('kunjungan','edit_all_update_report') and group_has_role('kunjungan','edit_own_update_report')) {
				if($value['id_petugas']!=$this->current_user->id and $value['created_by']!=$this->current_user->id) {
					$events[$key]['editable'] = false;
				}
			}

			if(!group_has_role('kunjungan','view_all_update_report') and group_has_role('kunjungan','view_own_update_report')) {
				if($value['id_petugas']!=$this->current_user->id and $value['created_by']!=$this->current_user->id) {
					$events[$key]['viewable'] = false;
				}
			}
		}
		echo json_encode($events);
	}
}