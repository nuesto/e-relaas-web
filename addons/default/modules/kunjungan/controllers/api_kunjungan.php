<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Kunjungan Module
 *
 * Manajemen kunjungan corporate
 *
 */
class Api_kunjungan extends API2_Controller
{
    // -------------------------------------
    // This will set the active section tab
    // -------------------------------------
    
    protected $section = 'kunjungan';

    public function __construct()
    {
        parent::__construct();

        
        // -------------------------------------
        // Load everything we need
        // -------------------------------------

        $this->lang->load('kunjungan');       
        $this->load->model('jadwal_m');

        $this->current_user = $this->authorize_api_access();
    }

    /**
     * Jumlah Call/Update
     *
     * @return  void
     */
    public function get_jumlah_call()
    {        
        // data call terbanyak
        $this->load->model('kunjungan/jadwal_m');
        $params['month(tanggal)'] = array('value'=>date('m'));
        $data['jumlah_call'] = $this->jadwal_m->count_jadwal_by_staff($params);

        $data['chart']['categories'] = array();
        $data['chart']['jumlah_call'] = array();
        $data['chart']['update_report'] = array();
        if(count($data['jumlah_call'])>0) {
            foreach ($data['jumlah_call'] as $key => $value) {
                $data['chart']['categories'][] = $value['petugas_display_name'];
                $data['chart']['jumlah_call'][] = (Int)$value['jumlah_jadwal'];
                $data['chart']['update_report'][] = (Int)$value['update_report'];
            }
        }

        // sort
        $data['jumlah_call_by_update'] = $data['jumlah_call'];
        usort($data['jumlah_call_by_update'], array($this,"cmp"));

        $month = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $data['bulan'] = $month[date('n')-1].' '.date('Y');

        $result = $data;

        $status = '200';

        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>'Data call tidak ditemukan');
            $status = '404';
        }

        _output($result,$status);
    }

    public function get_sales_champion()
    {
        $data = array();

        // month
        if($this->input->get('f-month') >= 1 AND $this->input->get('f-month') <= 12){
            $month = $this->input->get('f-month');
        }else{
            $month = date('m');
        }

        // year
        if($this->input->get('f-year') != NULL AND $this->input->get('f-year') != ''){
            $year = $this->input->get('f-year');
        }else{
            $year = date('Y');
        }

        // data call terbanyak
        $this->load->model('kunjungan/jadwal_m');


        $all_years = $this->jadwal_m->get_all_year();
        $data['all_years'] = array();
        foreach ($all_years as $key => $years) {
            $data['all_years'][$years] = $years;
        }
        $data['default_month'] = $month;
        $data['default_year'] = $year;

        $params_jadwal['month(tanggal)'] = array('value' => $month);
        $params_jadwal['year(tanggal)'] = array('value' => $year);

        $data['jumlah_call_by_update'] = $this->jadwal_m->count_jadwal_by_staff($params_jadwal)[0];

        if(isset($data['jumlah_call_by_update']['picture_id']) and $data['jumlah_call_by_update']['picture_id']!='') {
            $data['jumlah_call_by_update']['picture_id'] = base_url().'files/thumb/'.$data['jumlah_call_by_update']['picture_id'].'/190/210/fit';
        } else {
            $data['jumlah_call_by_update']['picture_id'] = base_url().'system/cms/themes/ace/img/default_avatar_190x210.png';
        }

        $months = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $data['bulan'] = $months[$month-1].' '.$year;

        $result = $data;

        $status = '200';

        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>'Data sales tidak ditemukan');
            $status = '404';
        }

        _output($result,$status);
    }

    public function get_sales_rank()
    {
        $data = array();

        // month
        if($this->input->get('f-month') >= 1 AND $this->input->get('f-month') <= 12){
            $month = $this->input->get('f-month');
        }else{
            $month = date('m');
        }

        // year
        if($this->input->get('f-year') != NULL AND $this->input->get('f-year') != ''){
            $year = $this->input->get('f-year');
        }else{
            $year = date('Y');
        }

        // data call terbanyak
        $this->load->model('kunjungan/jadwal_m');

        $params_jadwal['month(tanggal)'] = array('value' => $month);
        $params_jadwal['year(tanggal)'] = array('value' => $year);

        $data['jumlah_call_by_update'] = $this->jadwal_m->count_jadwal_by_staff($params_jadwal, NULL, 10);
        foreach ($data['jumlah_call_by_update'] as $key => &$value) {
            if($value['jumlah_jadwal'] == NULL){
                $value['jumlah_jadwal'] = 0;
            }
            if($value['update_report'] == NULL){
                $value['update_report'] = 0;
            }
        }

        $months = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $data['bulan'] = $months[$month-1].' '.$year;

        $result = $data;

        $status = '200';

        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>'Data sales rank ditemukan');
            $status = '404';
        }

        _output($result,$status);
    }

    public function get_call_report_graphic()
    {
        $data = array();

        // month
        if($this->input->get('f-month') >= 1 AND $this->input->get('f-month') <= 12){
            $month = $this->input->get('f-month');
        }else{
            $month = date('m');
        }

        // year
        if($this->input->get('f-year') != NULL AND $this->input->get('f-year') != ''){
            $year = $this->input->get('f-year');
        }else{
            $year = date('Y');
        }

        // data call terbanyak
        $this->load->model('kunjungan/jadwal_m');

        $params_jadwal['month(tanggal)'] = array('value' => $month);
        $params_jadwal['year(tanggal)'] = array('value' => $year);

        $params_staff = array();
        if(!group_has_role('kunjungan', 'view_all_jadwal', $this->current_user)){
            $id_cra = $this->current_user->id_cra;
            if($id_cra != NULL){
                $params_staff['id_cra'] = $id_cra;
            }else{
                $params_staff['id_cra'] = $this->current_user->id;
            }
            $params_staff['users.id'] = array(
                'type' => 'or_where',
                'value' => $this->current_user->id,
            );
        }

        $data['jumlah_call'] = $this->jadwal_m->count_jadwal_by_staff($params_jadwal, $params_staff);

        $data['chart']['categories'] = array();
        $data['chart']['jumlah_call'] = array();
        $data['chart']['update_report'] = array();
        if(count($data['jumlah_call'])>0) {
            foreach ($data['jumlah_call'] as $key => $value) {
                $data['chart']['categories'][] = $value['petugas_display_name'];
                $data['chart']['jumlah_call'][] = (Int)$value['jumlah_jadwal'];
                $data['chart']['update_report'][] = (Int)$value['update_report'];
            }
        }

        // sort
        $data['jumlah_call_by_update'] = $data['jumlah_call'];
        usort($data['jumlah_call_by_update'], array($this,"cmp"));

        $months = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $data['bulan'] = $months[$month-1].' '.$year;

        $result = $data;

        $status = '200';

        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>'Data call report ditemukan');
            $status = '404';
        }

        _output($result,$status);
    }

    function cmp($a,$b) 
    {
        return ($a['update_report'] >= $b['update_report']) ? -1 : 1;
    }
}