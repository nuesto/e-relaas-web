<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Kunjungan Module
 *
 * Mengelola daftar kunjungan dan memo
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('c/kunjungan/jadwal/index');
    }

}