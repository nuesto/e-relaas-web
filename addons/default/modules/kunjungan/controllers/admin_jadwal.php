<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Kunjungan Module
 *
 * Mengelola daftar kunjungan dan memo
 *
 */
class Admin_jadwal extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'jadwal';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'access_jadwal_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('kunjungan');		
		$this->load->model('jadwal_m');
		$this->load->model('nasabah/nasabah_m');
    }

    /**
	 * List all Jadwal
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'view_all_jadwal') AND ! group_has_role('kunjungan', 'view_own_jadwal')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){

				$params[substr($key, 2)] = $value;
			}
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'c/kunjungan/jadwal/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->jadwal_m->count_all_jadwal($params);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['jadwal']['entries'] = $this->jadwal_m->get_jadwal($pagination_config, $params);
		$data['jadwal']['total'] = count($data['jadwal']['entries']);
		$data['jadwal']['pagination'] = $this->pagination->create_links();

		$cra_group = $this->group_m->get_group_by_slug('cra');
		$crm_group = $this->group_m->get_group_by_slug('crm');
		$id_cra_group = $cra_group->id;
		$id_crm_group = $crm_group->id;

		$data['petugas_own'] = array();
		$data['petugas_cra'] = array();
		$data['petugas_crm'] = array();
		if(group_has_role('kunjungan', 'view_all_jadwal')){
			// add himself to list
			$data['petugas_own'] = $this->user_m->get_many_by(array('user_id' => $this->current_user->id));

			// get all user that is CRA or CRM
			$data['petugas_cra'] = $this->user_m->get_many_by(array('group_id' => $id_cra_group));
			$data['petugas_crm'] = $this->user_m->get_many_by(array('group_id' => $id_crm_group));
		}else{
			if(group_has_role('kunjungan', 'view_own_jadwal')){
				// get only user under him
				$data['petugas_crm'] = $this->user_m->get_many_by(array(
					'group_id' => $id_crm_group,
					'id_cra' => $this->current_user->id,
				));
				$data['petugas_cra'] = $this->user_m->get_many_by(array(
					'group_id' => $id_cra_group,
					'user_id' => $this->current_user->id,
				));
			}
		}

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['jadwal']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('c/kunjungan/jadwal/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('c/kunjungan/jadwal/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('kunjungan:jadwal:plural'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('kunjungan:jadwal:plural'))
			->append_css('module::kunjungan.css')
			->append_js('module::kunjungan.js')
			->build('admin/jadwal_index', $data);
    }
	
	/**
     * Display one Jadwal
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'view_all_jadwal') AND ! group_has_role('kunjungan', 'view_own_jadwal')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['jadwal'] = $this->jadwal_m->get_jadwal_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'view_all_jadwal')){
			if($data['jadwal']['id_petugas'] != $this->current_user->id
				AND $data['jadwal']['id_cra'] != $this->current_user->id){
				
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		if($this->input->is_ajax_request()) {
			$this->template->set_layout(false);
		}
        $this->template->title(lang('kunjungan:jadwal:view'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('kunjungan', 'view_all_jadwal') OR group_has_role('kunjungan', 'view_own_jadwal')){
			$this->template->set_breadcrumb(lang('kunjungan:jadwal:plural'), '/c/kunjungan/jadwal/index');
		}

		$this->template->set_breadcrumb(lang('kunjungan:jadwal:view'))
			->append_css('module::kunjungan.css')
			->append_js('module::kunjungan.js')
			->build('admin/jadwal_entry', $data);
    }
	
	/**
     * Create a new Jadwal entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'create_jadwal')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_jadwal('new')){	
				$this->session->set_flashdata('success', lang('kunjungan:jadwal:submit_success'));
				if($this->input->is_ajax_request()) {
					$result = array('status'=>'success','messages'=>lang('kunjungan:jadwal:submit_success'));
					echo json_encode($result);
					exit();
				}
				redirect('c/kunjungan/jadwal/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('kunjungan:jadwal:submit_failure');
				if($this->input->is_ajax_request()) {
					$result = array('status'=>'error','messages'=>validation_errors());
					echo json_encode($result);
					exit();
				}
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/kunjungan/jadwal/index'.$data['uri'];

		// Check permission if user just has view own
		$params = array();
		if(! group_has_role('nasabah', 'view_all_nasabah') AND group_has_role('nasabah', 'view_own_nasabah')){
			$params[] = array(
				'column'=>'nasabah_nasabah.id_cra',
				'function'=>'where',
				'value'=>$this->current_user->id
			);
			$params[] = array(
				'column'=>'nasabah_nasabah.id_crm',
				'function'=>'or_where',
				'value'=>$this->current_user->id
			);
		}
		$data['nasabah'] = $this->nasabah_m->get_nasabah(NULL, $params);

		$cra_group = $this->group_m->get_group_by_slug('cra');
		$id_cra_group = $cra_group->id;
		$data['petugas'] = $this->user_m->get_many_by(array('user_id' => $this->current_user->id));
		if($this->current_user->group_id==$id_cra_group) {
			$crm_group = $this->group_m->get_group_by_slug('crm');
			$id_crm_group = $crm_group->id;
			$data['petugas'] = array_merge($data['petugas'], $this->user_m->get_many_by(array('group_id' => $id_crm_group, 'id_cra' => $this->current_user->id)));
		}
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		if($this->input->is_ajax_request()) {
			$this->template->set_layout(false);
		}
        $this->template->title(lang('kunjungan:jadwal:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('kunjungan', 'view_all_jadwal') OR group_has_role('kunjungan', 'view_own_jadwal')){
			$this->template->set_breadcrumb(lang('kunjungan:jadwal:plural'), '/c/kunjungan/jadwal/index');
		}

		$this->template->set_breadcrumb(lang('kunjungan:jadwal:new'))
			->append_css('module::kunjungan.css')
			->append_js('module::kunjungan.js')
			->build('admin/jadwal_form', $data);
    }
	
	/**
     * Edit a Jadwal entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Jadwal to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        $jadwal = $this->jadwal_m->get_jadwal_by_id($id);

        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'edit_all_jadwal') AND ! group_has_role('kunjungan', 'edit_own_jadwal')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('kunjungan', 'edit_all_jadwal')){
			$entry = $this->jadwal_m->get_jadwal_by_id($id);
			$created_by_user_id = $entry['created_by'];
			$petugas_user_id = $entry['id_petugas'];
			if($created_by_user_id != $this->current_user->id AND $petugas_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// check if report is not submitted yet
		if($jadwal['memo'] != NULL){
			exit();
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_jadwal('edit', $id)){	
				$this->session->set_flashdata('success', lang('kunjungan:jadwal:submit_success'));
				if($this->input->is_ajax_request()) {
					$result = array('status'=>'success','messages'=>lang('kunjungan:jadwal:submit_success'));
					echo json_encode($result);
					exit();
				}
				redirect('c/kunjungan/jadwal/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('kunjungan:jadwal:submit_failure');
				if($this->input->is_ajax_request()) {
					$result = array('status'=>'error','messages'=>validation_errors());
					echo json_encode($result);
					exit();
				}
			}
		}
		
		$data['fields'] = $jadwal;
		$data['mode'] = 'edit';
		$data['return'] = 'c/kunjungan/jadwal/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;

		$data['nasabah'] = $this->nasabah_m->get_nasabah();

		$cra_group = $this->group_m->get_group_by_slug('cra');
		$id_cra_group = $cra_group->id;
		$data['petugas'] = $this->user_m->get_many_by(array('user_id' => $this->current_user->id));
		if($this->current_user->group_id==$id_cra_group) {
			$crm_group = $this->group_m->get_group_by_slug('crm');
			$id_crm_group = $crm_group->id;
			$data['petugas'] = array_merge($data['petugas'], $this->user_m->get_many_by(array('group_id' => $id_crm_group, 'id_cra' => $this->current_user->id)));
		}
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		if($this->input->is_ajax_request()) {
			$this->template->set_layout(false);
		}
        $this->template->title(lang('kunjungan:jadwal:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('kunjungan', 'view_all_jadwal') OR group_has_role('kunjungan', 'view_own_jadwal')){
			$this->template->set_breadcrumb(lang('kunjungan:jadwal:plural'), '/c/kunjungan/jadwal/index')
			->set_breadcrumb(lang('kunjungan:jadwal:view'), '/c/kunjungan/jadwal/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('kunjungan:jadwal:plural'))
			->set_breadcrumb(lang('kunjungan:jadwal:view'));
		}

		$this->template->set_breadcrumb(lang('kunjungan:jadwal:edit'))
			->append_css('module::kunjungan.css')
			->append_js('module::kunjungan.js')
			->build('admin/jadwal_form', $data);
    }
	
	/**
     * Delete a Jadwal entry
     * 
     * @param   int [$id] The id of Jadwal to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('kunjungan', 'delete_all_jadwal') AND ! group_has_role('kunjungan', 'delete_own_jadwal')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('kunjungan', 'delete_all_jadwal')){
			$entry = $this->jadwal_m->get_jadwal_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        // $this->jadwal_m->delete_jadwal_by_id($id);
        $this->session->set_flashdata('error', lang('kunjungan:jadwal:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('c/kunjungan/jadwal/index'.$data['uri']);
    }
	
	/**
     * Insert or update Jadwal entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_jadwal($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		unset($values['hour']);
		unset($values['minute']);

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('tanggal', lang('kunjungan:jadwal:tanggal'), 'required|callback__valid_mysql_date');
		$this->form_validation->set_rules('mulai', lang('kunjungan:jadwal:mulai'), 'required|callback__valid_mysql_time');
		$this->form_validation->set_rules('selesai', lang('kunjungan:jadwal:selesai'), 'required|callback__valid_mysql_time');
		$this->form_validation->set_rules('jenis_kunjungan', lang('kunjungan:jadwal:jenis_kunjungan'), 'required');
		$this->form_validation->set_rules('id_nasabah', lang('kunjungan:jadwal:id_nasabah'), 'required');
		$this->form_validation->set_rules('id_petugas', lang('kunjungan:jadwal:id_petugas'), 'required');
		$this->form_validation->set_rules('deskripsi', lang('kunjungan:jadwal:deskripsi'), 'max_length[500]');

		$this->form_validation->set_message('_valid_mysql_date', lang('kunjungan:validation_message:_valid_mysql_date'));
		$this->form_validation->set_message('_valid_mysql_time', lang('kunjungan:validation_message:_valid_mysql_time'));
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->jadwal_m->insert_jadwal($values);
				
			}
			else
			{
				$result = $this->jadwal_m->update_jadwal($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	function _valid_mysql_date($input){
		if($input == NULL OR $input == ''){
			return TRUE;
		}

		if(preg_match('/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])$/', $input)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function _valid_mysql_time($input){
		if($input == NULL OR $input == ''){
			return TRUE;
		}

		if(preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/", $input)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function get_jadwal()
	{
		$params = array();
		if($this->input->get('start')) {
			$params['tanggal >='] = $this->input->get('start');
		}
		if($this->input->get('end')) {
			$params['tanggal <='] = $this->input->get('end');
		}
		if($this->input->get('nasabah')) {
			$params['nasabah_nasabah.nama'] = array('type'=>'like','value'=>$this->input->get('nasabah'));
		}
		if($this->input->get('jenis_kunjungan')) {
			$params['jenis_kunjungan'] = array('type'=>'like','value'=>$this->input->get('jenis_kunjungan'));
		}
		if(!group_has_role('kunjungan','view_all_jadwal') and group_has_role('kunjungan','view_own_jadwal')) {
			$params['id_petugas'] = array('type' => 'where', 'value' => $this->current_user->id);
			$params['nasabah_nasabah.id_cra'] = array('type' => 'or_where', 'value' => $this->current_user->id);
		}

		if($this->input->get('id_petugas')) {
			$params['id_petugas'] = $this->input->get('id_petugas');
			unset($params['nasabah_nasabah.id_cra']);
		}
		$jadwal = $this->jadwal_m->get_jadwal(NULL, $params);

		$events = array();
		foreach ($jadwal as $key => $value) {
			$events[$key]['id'] = $value['id'];
			$events[$key]['title'] = $value['nama_nasabah'];
			$events[$key]['jenis_kunjungan'] = $value['jenis_kunjungan'];
			$events[$key]['start'] = $value['tanggal'].'T'.$value['mulai'];
			$events[$key]['end'] = $value['tanggal'].'T'.$value['selesai'];
			$events[$key]['editable'] = true;
			$events[$key]['viewable'] = true;

			// set different colors based on id_petugas
			if($value['id_petugas'] == $this->current_user->id){
				$events[$key]['color'] = '#FF6600';
			}else{
				$events[$key]['color'] = '#00535D';
			}
		}
		echo json_encode($events);
	}

	public function widget($widget_name)
	{
		$data = array();

		// month
		if($this->input->get('f-month') >= 1 AND $this->input->get('f-month') <= 12){
			$month = $this->input->get('f-month');
		}else{
			$month = date('m');
		}

		// year
		if($this->input->get('f-year') != NULL AND $this->input->get('f-year') != ''){
			$year = $this->input->get('f-year');
		}else{
			$year = date('Y');
		}

		if($widget_name == 'sales_champion'){
			$data = $this->_widget_sales_champion($month, $year);
		}elseif ($widget_name == 'sales_rank') {
			$data = $this->_widget_sales_rank($month, $year);
		}elseif ($widget_name == 'call_report_graphic') {
			$data = $this->_widget_call_report_graphic($month, $year);
		}

		echo $this->load->view('widget/'.$widget_name, $data, TRUE);
	}

	private function _widget_sales_champion($month, $year)
	{
		$data = array();

		// data call terbanyak
		$this->load->model('kunjungan/jadwal_m');

		$params_jadwal['month(tanggal)'] = array('value' => $month);
		$params_jadwal['year(tanggal)'] = array('value' => $year);

		$data['jumlah_call_by_update'] = $this->jadwal_m->count_jadwal_by_staff($params_jadwal);

		$data['chart']['categories'] = array();
		$data['chart']['jumlah_call_by_update'] = array();
		$data['chart']['update_report'] = array();
		if(count($data['jumlah_call_by_update'])>0) {
			foreach ($data['jumlah_call_by_update'] as $key => $value) {
				$data['chart']['categories'][] = $value['petugas_display_name'];
				$data['chart']['jumlah_call_by_update'][] = (Int)$value['jumlah_jadwal'];
				$data['chart']['update_report'][] = (Int)$value['update_report'];
			}
		}

		$months = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$data['bulan'] = $months[$month-1].' '.$year;

		return $data;
	}

	private function _widget_sales_rank($month, $year)
	{
		$data = array();

		// data call terbanyak
		$this->load->model('kunjungan/jadwal_m');

		$params_jadwal['month(tanggal)'] = array('value' => $month);
		$params_jadwal['year(tanggal)'] = array('value' => $year);

		$data['jumlah_call_by_update'] = $this->jadwal_m->count_jadwal_by_staff($params_jadwal, NULL, 10);

		$data['chart']['categories'] = array();
		$data['chart']['jumlah_call_by_update'] = array();
		$data['chart']['update_report'] = array();
		if(count($data['jumlah_call_by_update'])>0) {
			foreach ($data['jumlah_call_by_update'] as $key => $value) {
				$data['chart']['categories'][] = $value['petugas_display_name'];
				$data['chart']['jumlah_call_by_update'][] = (Int)$value['jumlah_jadwal'];
				$data['chart']['update_report'][] = (Int)$value['update_report'];
			}
		}

		$months = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$data['bulan'] = $months[$month-1].' '.$year;

		return $data;
	}

	private function _widget_call_report_graphic($month, $year)
	{
		$data = array();

		// data call terbanyak
		$this->load->model('kunjungan/jadwal_m');

		$params_jadwal['month(tanggal)'] = array('value' => $month);
		$params_jadwal['year(tanggal)'] = array('value' => $year);

		$params_staff = array();
		if(!group_has_role('kunjungan', 'view_all_jadwal')){
			$id_cra = $this->current_user->id_cra;
			if($id_cra != NULL){
				$params_staff['id_cra'] = $id_cra;
			}else{
				$params_staff['id_cra'] = $this->current_user->id;
			}
			$params_staff['users.id'] = array(
				'type' => 'or_where',
				'value' => $this->current_user->id,
			);
		}

		$data['jumlah_call'] = $this->jadwal_m->count_jadwal_by_staff($params_jadwal, $params_staff);

		$data['chart']['categories'] = array();
		$data['chart']['jumlah_call'] = array();
		$data['chart']['update_report'] = array();
		if(count($data['jumlah_call'])>0) {
			foreach ($data['jumlah_call'] as $key => $value) {
				$data['chart']['categories'][] = $value['petugas_display_name'];
				$data['chart']['jumlah_call'][] = (Int)$value['jumlah_jadwal'];
				$data['chart']['update_report'][] = (Int)$value['update_report'];
			}
		}

		// sort
		$data['jumlah_call_by_update'] = $data['jumlah_call'];
		usort($data['jumlah_call_by_update'], array($this,"cmp"));

		$months = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$data['bulan'] = $months[$month-1].' '.$year;

		return $data;
	}

	private function cmp($a,$b) 
	{
		return ($a['update_report'] >= $b['update_report']) ? -1 : 1;
	}
}