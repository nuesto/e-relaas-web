<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Kunjungan library
 *
 * @author		Aditya Satrya
 * @author		Nuesto Technology
 */

class Kunjungan
{
	public function __construct()
	{
	}

	public function count_my_unreported_jadwal()
	{
		ci()->load->model('Kunjungan/jadwal_m');
		return ci()->jadwal_m->count_unreported_jadwal();
	}
}