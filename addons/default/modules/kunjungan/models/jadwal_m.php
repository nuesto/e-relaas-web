<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Jadwal model
 *
 * @author Aditya Satrya
 */
class Jadwal_m extends MY_Model {
	
	public function get_jadwal($pagination_config = NULL, $params = array())
	{
		$this->db->select('kunjungan_jadwal.*');
		$this->db->select('nasabah_nasabah.id_cra');
		$this->db->select('nasabah_nasabah.id_crm');
		$this->db->select('nasabah_nasabah.nama AS nama_nasabah');
		$this->db->select('nasabah_nasabah.is_starred_item');
		$this->db->select('nasabah_group.id AS id_group');
		$this->db->select('nasabah_group.nama AS group_nama');
		$this->db->select('nasabah_segmen.id AS id_segmen');
		$this->db->select('nasabah_segmen.nama AS segmen_nama');
		$this->db->select('cra_users.user_id AS cra_user_id');
		$this->db->select('cra_users.display_name AS cra_display_name');
		$this->db->select('crm_users.user_id AS crm_user_id');
		$this->db->select('crm_users.display_name AS crm_display_name');
		$this->db->select('petugas_users.user_id AS petugas_user_id');
		$this->db->select('petugas_users.display_name AS petugas_display_name');

		if(isset($params) and is_array($params)) {
			foreach ($params as $key => $value) {
				if(is_array($value)) {
					$type = isset($value['type']) ? $value['type'] : 'where';
					
					if(!is_array($value['value'])){
						$escape = isset($value['escape']) ? $value['escape'] : true;
						$this->db->{$type}($key,$value['value'],$escape);
					}else{
						$this->db->{$type}($key.' IN ('.implode(",", $value['value']).')');
					}
				} elseif ($value == 'NULL') {
					$this->db->where($key.' IS NULL');
				} elseif ($value == 'NOT NULL') {
					$this->db->where($key.' IS NOT NULL');
				} else {
					$this->db->where($key,$value);
				}
			}
		}

		$this->db->order_by("kunjungan_jadwal.tanggal", "desc");
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$this->db->join('nasabah_nasabah','kunjungan_jadwal.id_nasabah=nasabah_nasabah.id', 'left');
		$this->db->join('nasabah_group', 'nasabah_group.id = nasabah_nasabah.id_group', 'left');
		$this->db->join('nasabah_segmen', 'nasabah_segmen.id = nasabah_nasabah.id_segmen', 'left');
		$this->db->join('profiles AS cra_users', 'cra_users.user_id = nasabah_nasabah.id_cra', 'left');
		$this->db->join('profiles AS crm_users', 'crm_users.user_id = nasabah_nasabah.id_crm', 'left');
		$this->db->join('profiles AS petugas_users', 'petugas_users.user_id = kunjungan_jadwal.id_petugas', 'left');
		
		$query = $this->db->get('default_kunjungan_jadwal');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_jadwal_by_id($id)
	{
		$this->db->select('kunjungan_jadwal.*');
		$this->db->select('nasabah_nasabah.id_cra');
		$this->db->select('nasabah_nasabah.id_crm');
		$this->db->select('nasabah_nasabah.nama AS nama_nasabah');
		$this->db->select('nasabah_nasabah.is_starred_item');
		$this->db->select('nasabah_group.id AS id_group');
		$this->db->select('nasabah_group.nama AS group_nama');
		$this->db->select('nasabah_segmen.id AS id_segmen');
		$this->db->select('nasabah_segmen.nama AS segmen_nama');
		$this->db->select('cra_users.display_name AS cra_display_name');
		$this->db->select('crm_users.display_name AS crm_display_name');
		$this->db->select('petugas_users.display_name AS petugas_display_name');
		
		$this->db->where('kunjungan_jadwal.id', $id);

		$this->db->join('nasabah_nasabah','kunjungan_jadwal.id_nasabah=nasabah_nasabah.id');
		$this->db->join('nasabah_group', 'nasabah_group.id = nasabah_nasabah.id_group', 'left');
		$this->db->join('nasabah_segmen', 'nasabah_segmen.id = nasabah_nasabah.id_segmen', 'left');
		$this->db->join('profiles AS cra_users', 'cra_users.user_id = nasabah_nasabah.id_cra', 'left');
		$this->db->join('profiles AS crm_users', 'crm_users.user_id = nasabah_nasabah.id_crm', 'left');
		$this->db->join('profiles AS petugas_users', 'petugas_users.user_id = kunjungan_jadwal.id_petugas', 'left');
		$query = $this->db->get('default_kunjungan_jadwal');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_jadwal($params=array())
	{
		return count($this->get_jadwal(null, $params));
	}

	public function count_jadwal_by_staff($params_jadwal=array(), $params_staff=array(), $limit = NULL)
	{
		// subquery
		$this->db->select('kunjungan_jadwal.id_petugas');
		$this->db->select('count(tanggal) AS jumlah_jadwal');
		$this->db->select('sum(if(memo!="", 1, 0)) AS update_report', false);
		$this->db->from('kunjungan_jadwal');
		if(isset($params_jadwal) and is_array($params_jadwal)) {
			foreach ($params_jadwal as $key => $value) {
				if(is_array($value)) {
					$type = isset($value['type']) ? $value['type'] : 'where';
					$escape = isset($value['escape']) ? $value['escape'] : true;
					$this->db->{$type}($key,$value['value'],$escape);
				} else {
					$this->db->where($key,$value);
				}
			}
		}
		$this->db->group_by('kunjungan_jadwal.id_petugas');
		$sub_sql = $this->db->get_compiled_select();

		// main query
		$this->db->select('profiles.display_name AS petugas_display_name, picture_id, email, no_hp');
		$this->db->select("jadwal.jumlah_jadwal, jadwal.update_report", false);
		$this->db->from('users');
		$this->db->join('profiles', 'profiles.user_id = users.id', 'left');
		$this->db->join('('.$sub_sql.') jadwal', 'jadwal.id_petugas = users.id', 'left');
		if(isset($params_staff) and is_array($params_staff)) {
			foreach ($params_staff as $key => $value) {
				if(is_array($value)) {
					$type = isset($value['type']) ? $value['type'] : 'where';
					$escape = isset($value['escape']) ? $value['escape'] : true;
					$this->db->{$type}($key,$value['value'],$escape);
				} else {
					$this->db->where($key,$value);
				}
			}
		}
		$this->db->order_by('update_report', 'desc');
		$this->db->order_by('jumlah_jadwal', 'asc');
		if($limit != NULL){
			$this->db->limit($limit);
		}

		// execute
		$query = $this->db->get();
		$result = $query->result_array();
		
		return $result;
	}
	
	public function delete_jadwal_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_kunjungan_jadwal');
	}
	
	public function insert_jadwal($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_kunjungan_jadwal', $values);
	}
	
	public function update_jadwal($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_kunjungan_jadwal', $values); 
	}

	public function count_unreported_jadwal($user_id = NULL)
	{
		if($user_id == NULL){
			$user_id = $this->current_user->id;
		}

		$this->db->select("sum(if(memo='' || memo is null,1,0)) as unreported", false);
		$this->db->where('tanggal <=', date('Y-m-d'));
		$this->db->where('id_petugas = '.$user_id);
		$query = $this->db->get('default_kunjungan_jadwal');
		$result = $query->row_array();
		
		return (int)$result['unreported'];
	}

	public function get_all_year()
	{
		$this->db->select('year(tanggal) AS year');
		$this->db->distinct();
		$this->db->order_by('year', 'desc');
		$query = $this->db->get('kunjungan_jadwal');
		$result = $query->result_array();

		return array_column($result, 'year');
	}
	
}