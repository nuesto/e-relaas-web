<style>
  @media (max-width: 660px) {
    .btn-group.content-toolbar{
      float:none;
      margin-top:10px;
    }
  }
</style>
<script src="<?php echo base_url() ?>addons/default/modules/dpk/js/highcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>addons/default/modules/dpk/js/exporting.js" type="text/javascript"></script>
<div id="page-content">
	<div class="page-header">
		<h1>
			<?php echo $nasabah['nama']; ?>
		</h1>
		
		<div class="btn-group content-toolbar">
			<a href="<?php echo site_url('c/nasabah/nasabah/index'.$uri); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-arrow-left"></i>
				<?php echo lang('nasabah:back') ?>
			</a>

			<?php if(group_has_role('nasabah', 'delete_all_nasabah')){ ?>
				<a href="<?php echo site_url('c/nasabah/nasabah/delete/'.$nasabah['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php }elseif(group_has_role('nasabah', 'delete_own_nasabah')){ ?>
				<?php if($nasabah['id_'.$this->current_user->group] == $this->current_user->id){ ?>
					<a href="<?php echo site_url('c/nasabah/nasabah/delete/'.$nasabah['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
						<i class="icon-trash"></i>
						<?php echo lang('global:delete') ?>
					</a>
				<?php } ?>
			<?php } ?>
		</div>
	</div>

	<div class="row row-ikhtisar">
		<h3 class="header smaller lighter">
			<?php echo lang('nasabah:nasabah:ikhtisar'); ?>

			<div class="btn-group content-toolbar subheader-toolbar">
				<?php if(group_has_role('nasabah', 'edit_all_nasabah')){ ?>
					<a href="<?php echo site_url('c/nasabah/nasabah/edit/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
						<i class="icon-edit"></i>
						<?php echo lang('global:edit') ?>
					</a>
				<?php }elseif(group_has_role('nasabah', 'edit_own_nasabah')){ ?>
					<?php if($nasabah['id_'.$this->current_user->group] == $this->current_user->id){ ?>
						<a href="<?php echo site_url('c/nasabah/nasabah/edit/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
							<i class="icon-edit"></i>
							<?php echo lang('global:edit') ?>
						</a>
					<?php } ?>
				<?php } ?>

			
				<!-- <button class="btn btn-sm btn-yellow" onclick="window.print()"> -->
				<!-- <button class="btn btn-sm btn-yellow" onclick="PrintInnerHTML()"> -->
				<button class="btn btn-sm btn-yellow">
					<i class="icon-print"></i>
					Print
				</button>
			</div>	
		</h3>

		<div class="col-xs-12">
			<div class="row">
				<div class="col-sm-5">
					<?php if($nasabah['id_logo'] == '' OR $nasabah['id_logo'] == NULL){ ?>
						&nbsp;
					<?php }else{ ?>
						<img width="100%" src="<?php echo base_url(); ?>files/thumb/<?php echo $nasabah['id_logo']; ?>/800/800" />
					<?php } ?>
				</div>
				<?php if(isset($nasabah['deskripsi'])){ ?>
				<div class="col-sm-7"><?php echo nl2br($nasabah['deskripsi']); ?></div>
				<?php }else{ ?>
				<div class="col-sm-7">-</div>
				<?php } ?>
			</div>

			<div class="row" style="height: 20px"></div>

			<div class="row data-detail first">
				<div class="data-detail-label col-xs-4 col-sm-2"><?php echo lang('nasabah:nasabah:nama'); ?></div>
				<?php if(isset($nasabah['nama'])){ ?>
				<div class="data-detail-value col-xs-8 col-sm-10"><?php echo $nasabah['nama']; ?></div>
				<?php }else{ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">-</div>
				<?php } ?>
			</div>

			<div class="row data-detail">
				<div class="data-detail-label col-xs-4 col-sm-2"><?php echo lang('nasabah:nasabah:anniversary'); ?></div>
				<?php if(isset($nasabah['anniversary'])){ ?>
				<div class="data-detail-value col-xs-8 col-sm-10"><?php echo $nasabah['anniversary']; ?></div>
				<?php }else{ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">-</div>
				<?php } ?>
			</div>

			<div class="row data-detail">
				<div class="data-detail-label col-xs-4 col-sm-2"><?php echo lang('nasabah:nasabah:id_group'); ?></div>
				<?php if(isset($nasabah['group_nama'])){ ?>
				<div class="data-detail-value col-xs-8 col-sm-10"><?php echo $nasabah['group_nama']; ?></div>
				<?php }else{ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">-</div>
				<?php } ?>
			</div>

			<div class="row data-detail">
				<div class="data-detail-label col-xs-4 col-sm-2"><?php echo lang('nasabah:nasabah:id_segmen'); ?></div>
				<?php if(isset($nasabah['segmen_nama'])){ ?>
				<div class="data-detail-value col-xs-8 col-sm-10"><?php echo $nasabah['segmen_nama']; ?></div>
				<?php }else{ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">-</div>
				<?php } ?>
			</div>

			<div class="row data-detail">
				<div class="data-detail-label col-xs-4 col-sm-2"><?php echo lang('nasabah:nasabah:id_industri'); ?></div>
				<?php if(isset($nasabah['industri_nama'])){ ?>
				<div class="data-detail-value col-xs-8 col-sm-10"><?php echo $nasabah['industri_nama']; ?></div>
				<?php }else{ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">-</div>
				<?php } ?>
			</div>

			<div class="row" style="height: 30px"></div>

			<div class="row data-detail first">
				<div class="data-detail-label col-xs-4 col-sm-2"><?php echo lang('nasabah:nasabah:pic'); ?></div>
				<?php if(isset($nasabah['pic_nama'])){ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">
					<?php  
						$pic = array();
						$pic[] = '<i class="icon-user light-orange bigger-110"></i> ' . $nasabah['pic_nama'];
						$pic[] = '<i class="icon-envelope light-orange bigger-110"></i> ' . $nasabah['pic_email'];
						$pic[] = '<i class="icon-phone light-orange bigger-110"></i> ' . $nasabah['pic_handphone'];
						$pic[] = '<i class="icon-calendar light-orange bigger-110"></i> ' . lang('nasabah:nasabah:pic_birthdate') . ': ' . substr($nasabah['pic_birthdate'], 0, 10);

						echo implode("<br />", $pic);
					?>
				</div>
				<?php }else{ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">-</div>
				<?php } ?>
			</div>

			<div class="row data-detail first">
				<div class="data-detail-label col-xs-4 col-sm-2"><?php echo lang('nasabah:nasabah:id_cra'); ?></div>
				<?php if(isset($nasabah['cra_display_name'])){ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">
					<?php  
						$pic = array();
						$pic[] = '<i class="icon-user light-orange bigger-110"></i> ' . $nasabah['cra_display_name'];
						$pic[] = '<i class="icon-envelope light-orange bigger-110"></i> ' . $nasabah['cra_email'];

						echo implode("<br />", $pic);
					?>
				</div>
				<?php }else{ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">-</div>
				<?php } ?>
			</div>

			<div class="row data-detail">
				<div class="data-detail-label col-xs-4 col-sm-2"><?php echo lang('nasabah:nasabah:id_crm'); ?></div>
				<?php if(isset($nasabah['crm_display_name'])){ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">
					<?php  
						$pic = array();
						$pic[] = '<i class="icon-user light-orange bigger-110"></i> ' . $nasabah['crm_display_name'];
						$pic[] = '<i class="icon-envelope light-orange bigger-110"></i> ' . $nasabah['crm_email'];

						echo implode("<br />", $pic);
					?>
				</div>
				<?php }else{ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">-</div>
				<?php } ?>
			</div>

			<div class="row" style="height: 30px"></div>

			<div class="row data-detail first">
				<div class="data-detail-label col-xs-4 col-sm-2"><?php echo lang('nasabah:nasabah:website'); ?></div>
				<?php if(isset($nasabah['website'])){ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">
					<a href="<?php echo $nasabah['website']; ?>" target="window"><?php echo $nasabah['website']; ?></a>
				</div>
				<?php }else{ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">-</div>
				<?php } ?>
			</div>

			<div class="row data-detail">
				<div class="data-detail-label col-xs-4 col-sm-2"><?php echo lang('nasabah:nasabah:alamat'); ?></div>
				<?php if(isset($nasabah['alamat'])){ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">
					<?php echo $nasabah['alamat']; ?>
					<br />
					<div class="row" style="height: 20px"></div>
					<div id="map" style="height: 200px"></div>
				</div>
				<?php }else{ ?>
				<div class="data-detail-value col-xs-8 col-sm-10">-</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="row row-manajemen">
		<h3 class="header smaller lighter">
			<a name="manajemen" class="subheader"><?php echo lang('nasabah:manajemen:singular'); ?></a>
			<?php if(group_has_role('nasabah', 'edit_all_nasabah')){ ?>
				<div class="btn-group content-toolbar subheader-toolbar">
					<a href="<?php echo site_url('c/nasabah/manajemen/create/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
						<i class="icon-plus"></i>
						<?php echo lang('nasabah:manajemen:new'); ?>
					</a>
				</div>
			<?php }elseif(group_has_role('nasabah', 'edit_own_nasabah')){ ?>
				<?php if($nasabah['id_'.$this->current_user->group] == $this->current_user->id){ ?>
					<div class="btn-group content-toolbar subheader-toolbar">
						<a href="<?php echo site_url('c/nasabah/manajemen/create/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
							<i class="icon-plus"></i>
							<?php echo lang('nasabah:manajemen:new'); ?>
						</a>
					</div>
				<?php } ?>
			<?php } ?>
		</h3>
		
		<div id="nasabah-manajemen">
	        <div class="col-sm-6">
		        <div class="row row-manajemen-entry">
					<div class="col-xs-3">
						<div class="animated-background manajemen-foto"></div>
					</div>
					<div class="col-xs-9">
						<div class="animated-background text-line"></div>
						<div class="animated-background text-line"></div>
					</div>
				</div>
				<div class="row row-manajemen-entry">
					<div class="col-xs-3">
						<div class="animated-background manajemen-foto"></div>
					</div>
					<div class="col-xs-9">
						<div class="animated-background text-line"></div>
						<div class="animated-background text-line"></div>
					</div>
				</div>
				<div class="row row-manajemen-entry">
					<div class="col-xs-3">
						<div class="animated-background manajemen-foto"></div>
					</div>
					<div class="col-xs-9">
						<div class="animated-background text-line"></div>
						<div class="animated-background text-line"></div>
					</div>
				</div>
			</div>

			<div class="col-sm-6">
		        <div class="row row-manajemen-entry">
					<div class="col-xs-3">
						<div class="animated-background manajemen-foto"></div>
					</div>
					<div class="col-xs-9">
						<div class="animated-background text-line"></div>
						<div class="animated-background text-line"></div>
					</div>
				</div>
				<div class="row row-manajemen-entry">
					<div class="col-xs-3">
						<div class="animated-background manajemen-foto"></div>
					</div>
					<div class="col-xs-9">
						<div class="animated-background text-line"></div>
						<div class="animated-background text-line"></div>
					</div>
				</div>
				<div class="row row-manajemen-entry">
					<div class="col-xs-3">
						<div class="animated-background manajemen-foto"></div>
					</div>
					<div class="col-xs-9">
						<div class="animated-background text-line"></div>
						<div class="animated-background text-line"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row row-struktur">
		<h3 class="header smaller lighter">
			<a name="struktur" class="subheader"><?php echo lang('nasabah:struktur:singular'); ?></a>
			<?php if(group_has_role('nasabah', 'edit_all_nasabah')){ ?>
		
			<?php }elseif(group_has_role('nasabah', 'edit_own_nasabah')){ ?>
				<?php if($nasabah['id_'.$this->current_user->group] == $this->current_user->id){ ?>
					
				<?php } ?>
			<?php } ?>
			<?php if(group_has_role('nasabah', 'edit_all_nasabah')){ ?>
				<div class="btn-group content-toolbar subheader-toolbar">
					<?php if($nasabah['id_file_struktur'] == NULL){ ?>

					<a href="<?php echo site_url('c/nasabah/nasabah/create_struktur/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
						<i class="icon-plus"></i>
						<?php echo lang('nasabah:struktur:new'); ?>
					</a>
					
					<?php }else{ ?>
					
					<a href="<?php echo site_url('c/nasabah/nasabah/edit_struktur/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
						<i class="icon-edit"></i>
						<?php echo lang('nasabah:struktur:edit'); ?>
					</a>
					
					<?php } ?>
				</div>
			<?php }elseif(group_has_role('nasabah', 'edit_own_nasabah')){ ?>
				<?php if($nasabah['id_'.$this->current_user->group] == $this->current_user->id){ ?>
					<div class="btn-group content-toolbar subheader-toolbar">
						<?php if($nasabah['id_file_struktur'] == NULL){ ?>

						<a href="<?php echo site_url('c/nasabah/nasabah/create_struktur/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
							<i class="icon-plus"></i>
							<?php echo lang('nasabah:struktur:new'); ?>
						</a>
						
						<?php }else{ ?>
						
						<a href="<?php echo site_url('c/nasabah/nasabah/edit_struktur/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
							<i class="icon-edit"></i>
							<?php echo lang('nasabah:struktur:edit'); ?>
						</a>
						
						<?php } ?>
					</div>
				<?php } ?>
			<?php } ?>
		</h3>

		<div>
			<div class="col-sm-12">
				<?php if($nasabah['id_file_struktur'] == NULL){ ?>

				<div class="well"><?php echo lang('nasabah:struktur:no_entry'); ?></div>

				<?php }else{ ?>

				<img width="100%" src="<?php echo base_url(); ?>files/thumb/<?php echo $nasabah['id_file_struktur']; ?>/800/800" />

				<?php } ?>
			</div>
		</div>
	</div>

	<div class="row row-area">
		<h3 class="header smaller lighter">
			<a name="area" class="subheader"><?php echo lang('nasabah:area:singular'); ?></a>
			<?php if(group_has_role('nasabah', 'edit_all_nasabah')){ ?>
				<div class="btn-group content-toolbar subheader-toolbar">
					<?php if($nasabah['id_file_area'] == NULL){ ?>

					<a href="<?php echo site_url('c/nasabah/nasabah/create_area/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
						<i class="icon-plus"></i>
						<?php echo lang('nasabah:area:new'); ?>
					</a>
					
					<?php }else{ ?>
					
					<a href="<?php echo site_url('c/nasabah/nasabah/edit_area/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
						<i class="icon-edit"></i>
						<?php echo lang('nasabah:area:edit'); ?>
					</a>
					
					<?php } ?>
				</div>
			<?php }elseif(group_has_role('nasabah', 'edit_own_nasabah')){ ?>
				<?php if($nasabah['id_'.$this->current_user->group] == $this->current_user->id){ ?>
					<div class="btn-group content-toolbar subheader-toolbar">
						<?php if($nasabah['id_file_area'] == NULL){ ?>

						<a href="<?php echo site_url('c/nasabah/nasabah/create_area/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
							<i class="icon-plus"></i>
							<?php echo lang('nasabah:area:new'); ?>
						</a>
						
						<?php }else{ ?>
						
						<a href="<?php echo site_url('c/nasabah/nasabah/edit_area/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
							<i class="icon-edit"></i>
							<?php echo lang('nasabah:area:edit'); ?>
						</a>
						
						<?php } ?>
					</div>
				<?php } ?>
			<?php } ?>
		</h3>

		<div>
			<div class="col-sm-12">
				<?php if($nasabah['id_file_area'] == NULL){ ?>

				<div class="well"><?php echo lang('nasabah:area:no_entry'); ?></div>

				<?php }else{ ?>

				<img width="100%" src="<?php echo base_url(); ?>files/thumb/<?php echo $nasabah['id_file_area']; ?>/800/800" />

				<?php } ?>
			</div>
		</div>
	</div>

	<div class="row row-dpk">
		<h3 class="header smaller lighter">
			<a name="dpk_customer" class="subheader"><?php echo lang('nasabah:dpk:dpk_customer'); ?></a>
		</h3>
		<div id="nasabah-dpk">
	        <div class="col-xs-12">
		        <div class="row row-dpk-customer">
					<div class="col-xs-5">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-3">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
				</div>
		        <!-- <div class="row">
					<div class="col-xs-12 col-md-3">
						<table border="0" width="100%">
							<tr>
								<td rowspan="4" align="center" valign="middle" style="background: #5d96d0; font-size: 25px; color:  white; font-weight: bold;">199,734</td>
							</tr>
							<tr>
								<td align="center" style="background: #f7a427; font-weight: bold;">GR</td>
								<td align="right" style="background: #f7a427a1; font-weight: bold;">92,893</td>
							</tr>
							<tr>
								<td align="center" style="background: #5baf3f; font-weight: bold;">DP</td>
								<td align="right" style="background: #5baf3fa3; font-weight: bold;">92,893</td>
							</tr>
							<tr>
								<td align="center" style="background: #de6d41; font-weight: bold;">TB</td>
								<td align="right" style="background: #de6d41a6; font-weight: bold;">92,893</td>
							</tr>
						</table>
					</div>
					<div class="col-xs-12 col-md-3">
					</div>
					<div class="col-xs-12 col-md-6">
					</div>
				</div> -->
		    </div>
		</div>
	</div>

	<div class="row row-strategi">
		<h3 class="header smaller lighter">
			<a name="strategi" class="subheader"><?php echo lang('nasabah:strategi:singular'); ?></a>
			
			<?php if(group_has_role('nasabah', 'edit_all_nasabah')){ ?>
				<div class="btn-group content-toolbar subheader-toolbar">
					<a href="<?php echo site_url('c/nasabah/strategi/create/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
						<i class="icon-plus"></i>
						<?php echo lang('nasabah:strategi:new'); ?>
					</a>
				</div>
			<?php }elseif(group_has_role('nasabah', 'edit_own_nasabah')){ ?>
				<?php if($nasabah['id_'.$this->current_user->group] == $this->current_user->id){ ?>
					<div class="btn-group content-toolbar subheader-toolbar">
						<a href="<?php echo site_url('c/nasabah/strategi/create/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
							<i class="icon-plus"></i>
							<?php echo lang('nasabah:strategi:new'); ?>
						</a>
					</div>
				<?php } ?>
			<?php } ?>
		</h3>

		<div id="nasabah-strategi">
	        <div class="col-xs-12">
		        <div class="row row-strategi-entry">
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row row-produk">
		<h3 class="header smaller lighter">
			<a name="produk" class="subheader"><?php echo lang('nasabah:produk:singular'); ?></a>
			<?php if(group_has_role('nasabah', 'edit_all_nasabah')){ ?>
				<div class="btn-group content-toolbar subheader-toolbar">
					<a href="<?php echo site_url('c/nasabah/produk/edit/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
						<i class="icon-edit"></i>
						<?php echo lang('nasabah:produk:edit'); ?>
					</a>
				</div>
			<?php }elseif(group_has_role('nasabah', 'edit_own_nasabah')){ ?>
				<?php if($nasabah['id_'.$this->current_user->group] == $this->current_user->id){ ?>
					<div class="btn-group content-toolbar subheader-toolbar">
						<a href="<?php echo site_url('c/nasabah/produk/edit/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
							<i class="icon-edit"></i>
							<?php echo lang('nasabah:produk:edit'); ?>
						</a>
					</div>
				<?php } ?>
			<?php } ?>
		</h3>
		
		<div id="nasabah-produk">
	        <div class="col-sm-6">
		        <div class="row row-produk-entry">
					<div class="col-xs-10">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
				</div>
			</div>

			<div class="col-sm-6">
		        <div class="row row-produk-entry">
					<div class="col-xs-10">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<h3 class="header smaller lighter">
			<a name="pks" class="subheader"><?php echo lang('nasabah:pks:singular'); ?></a>
			<?php if(group_has_role('nasabah', 'edit_all_nasabah')){ ?>
				<div class="btn-group content-toolbar subheader-toolbar">
					<a href="<?php echo site_url('c/nasabah/pks/create/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
						<i class="icon-plus"></i>
						<?php echo lang('nasabah:pks:new'); ?>
					</a>
				</div>
			<?php }elseif(group_has_role('nasabah', 'edit_own_nasabah')){ ?>
				<?php if($nasabah['id_'.$this->current_user->group] == $this->current_user->id){ ?>
					<div class="btn-group content-toolbar subheader-toolbar">
						<a href="<?php echo site_url('c/nasabah/pks/create/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
							<i class="icon-plus"></i>
							<?php echo lang('nasabah:pks:new'); ?>
						</a>
					</div>
				<?php } ?>
			<?php } ?>
		</h3>

		<div id="nasabah-pks">
	        <div class="col-xs-12">
		        <div class="row row-pks-entry">
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
					<div class="col-xs-2">
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
						<div style="width: 100%" class="animated-background text-line"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<h3 class="header smaller lighter">
			<a name="catatan" class="subheader"><?php echo lang('nasabah:catatan:singular'); ?></a>
			<?php if(group_has_role('nasabah', 'edit_all_nasabah')){ ?>
				<div class="btn-group content-toolbar subheader-toolbar">
				
					<?php if(! isset($nasabah['catatan']) OR $nasabah['catatan'] == ''){ ?>
				
					<a href="<?php echo site_url('c/nasabah/nasabah/create_catatan/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
						<i class="icon-plus"></i>
						<?php echo lang('nasabah:catatan:new'); ?>
					</a>
				
					<?php }else{ ?>

					<a href="<?php echo site_url('c/nasabah/nasabah/edit_catatan/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
						<i class="icon-edit"></i>
						<?php echo lang('nasabah:catatan:edit'); ?>
					</a>

					<?php } ?>

				</div>
			<?php }elseif(group_has_role('nasabah', 'edit_own_nasabah')){ ?>
				<?php if($nasabah['id_'.$this->current_user->group] == $this->current_user->id){ ?>
					<div class="btn-group content-toolbar subheader-toolbar">
					
						<?php if(! isset($nasabah['catatan']) OR $nasabah['catatan'] == ''){ ?>
					
						<a href="<?php echo site_url('c/nasabah/nasabah/create_catatan/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
							<i class="icon-plus"></i>
							<?php echo lang('nasabah:catatan:new'); ?>
						</a>
					
						<?php }else{ ?>

						<a href="<?php echo site_url('c/nasabah/nasabah/edit_catatan/'.$nasabah['id'].$uri); ?>" class="btn btn-sm btn-yellow">
							<i class="icon-edit"></i>
							<?php echo lang('nasabah:catatan:edit'); ?>
						</a>

						<?php } ?>

					</div>
				<?php } ?>
			<?php } ?>
		</h3>

		<div id="nasabah-catatan">
	        <div class="col-xs-12">
		        <div class="row row-catatan-entry">
					<?php 
					if(isset($nasabah['catatan']) AND $nasabah['catatan'] != ''){ ?>
					<div class="col-sm-9"><?php echo nl2br($nasabah['catatan']); ?></div>
					<?php }else{ ?>
					<div class="col-sm-12">
						<div class="well"><?php echo lang('nasabah:catatan:no_entry'); ?></div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	/* SECTIONS */
	$(document).ready(function(){
		$("#nasabah-manajemen").load("<?php echo site_url('c/nasabah/manajemen/index_html/'.$nasabah['id']); ?>");
		$("#nasabah-strategi").load("<?php echo site_url('c/nasabah/strategi/index_html/'.$nasabah['id']); ?>");
		$("#nasabah-produk").load("<?php echo site_url('c/nasabah/produk/index_html/'.$nasabah['id']); ?>");
		$("#nasabah-pks").load("<?php echo site_url('c/nasabah/pks/index_html/'.$nasabah['id']); ?>");
		$("#nasabah-dpk").load("<?php echo site_url('nasabah/c/nasabah/nasabah_dpk/'.$nasabah['id']); ?>");
	});

	function initAutocomplete() {
		<?php
		$lat = -6.203638;
		$long = 106.820273;
		if(isset($nasabah['loc_latitude']) AND $nasabah['loc_latitude'] != ''){
			$lat = $nasabah['loc_latitude'];
		}
		if(isset($nasabah['loc_longitude']) AND $nasabah['loc_longitude'] != ''){
			$long = $nasabah['loc_longitude'];
		}
		?>
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: <?php echo $lat; ?>, lng: <?php echo $long; ?>},
			zoom: 15,
			mapTypeId: 'roadmap'
		});

		// Place a draggable marker on the map
		var marker = new google.maps.Marker({
			position: {lat: <?php echo $lat; ?>, lng: <?php echo $long; ?>},
			map: map,
			draggable:false
		});
	}

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJFlCAAMGo8fQ0tSOxhAZ4rphJBAo2uVA&libraries=places&callback=initAutocomplete" async defer></script>