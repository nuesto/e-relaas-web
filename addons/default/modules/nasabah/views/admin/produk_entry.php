<div class="page-header">
	<h1><?php echo lang('nasabah:produk:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('c/nasabah/produk/index'.$uri); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('nasabah:back') ?>
		</a>

		<?php if(group_has_role('nasabah', 'edit_all_produk')){ ?>
			<a href="<?php echo site_url('c/nasabah/produk/edit/'.$produk['id'].$uri); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('nasabah', 'edit_own_produk')){ ?>
			<?php if($produk->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('c/nasabah/produk/edit/'.$produk->id.$uri); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('nasabah', 'delete_all_produk')){ ?>
			<a href="<?php echo site_url('c/nasabah/produk/delete/'.$produk['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('nasabah', 'delete_own_produk')){ ?>
			<?php if($produk->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('c/nasabah/produk/delete/'.$produk['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $produk['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:produk:id_nasabah'); ?></div>
		<?php if(isset($produk['id_nasabah'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $produk['id_nasabah']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:produk:id_produk'); ?></div>
		<?php if(isset($produk['id_produk'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $produk['id_produk']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:produk:status'); ?></div>
		<?php if(isset($produk['status'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $produk['status']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:produk:keterangan'); ?></div>
		<?php if(isset($produk['keterangan'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $produk['keterangan']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:created'); ?></div>
		<?php if(isset($produk['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($produk['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:updated'); ?></div>
		<?php if(isset($produk['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($produk['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($produk['created_by'], true); ?></div>
	</div>
</div>