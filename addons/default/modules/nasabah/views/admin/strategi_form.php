<div class="page-header">
	<h1><?php echo lang('nasabah:strategi:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<?php 
		$value = NULL;
		if($this->input->post('id_nasabah') != NULL){
			$value = $this->input->post('id_nasabah');
		}elseif($mode == 'new'){
			$value = $id_nasabah;
		}elseif($mode == 'edit'){
			$value = $fields['id_nasabah'];
		}
	?>
	<input name="id_nasabah" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="proyek"><?php echo lang('nasabah:strategi:proyek'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('proyek') != NULL){
					$value = $this->input->post('proyek');
				}elseif($this->input->get('f-proyek') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-proyek');
				}elseif($mode == 'edit'){
					$value = $fields['proyek'];
				}
			?>
			<input name="proyek" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="deskripsi"><?php echo lang('nasabah:strategi:deskripsi'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('deskripsi') != NULL){
					$value = $this->input->post('deskripsi');
				}elseif($this->input->get('f-deskripsi') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-deskripsi');
				}elseif($mode == 'edit'){
					$value = $fields['deskripsi'];
				}
			?>
			<input name="deskripsi" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="financial_impact"><?php echo lang('nasabah:strategi:financial_impact'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('financial_impact') != NULL){
					$value = $this->input->post('financial_impact');
				}elseif($this->input->get('f-financial_impact') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-financial_impact');
				}elseif($mode == 'edit'){
					$value = $fields['financial_impact'];
				}
			?>
			<input name="financial_impact" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="target"><?php echo lang('nasabah:strategi:target'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('target') != NULL){
					$value = $this->input->post('target');
				}elseif($this->input->get('f-target') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-target');
				}elseif($mode == 'edit'){
					$value = $fields['target'];
				}
			?>
			<input name="target" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('nasabah:ordering_count'); ?></label>

		<div class="col-sm-1">
			<?php 
				$value = NULL;
				if($this->input->post('ordering_count') != NULL){
					$value = $this->input->post('ordering_count');
				}elseif($mode == 'edit'){
					$value = $fields['ordering_count'];
				}
			
				$options = array();
				for ($i=0; $i <= 10; $i++) { 
					$options[$i] = $i;
				}
				echo form_dropdown('ordering_count', $options, $value);
			?>
		</div>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>