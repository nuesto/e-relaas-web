<div class="page-header">
	<h1><?php echo lang('nasabah:area:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<?php 
		$value = NULL;
		if($this->input->post('id_nasabah') != NULL){
			$value = $this->input->post('id_nasabah');
		}elseif($id_nasabah != NULL AND $mode == 'new'){
			$value = $id_nasabah;
		}elseif($mode == 'edit'){
			$value = $fields['id_nasabah'];
		}
	?>
	<input name="id_nasabah" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_file_area"><?php echo lang('nasabah:area:id_file_area'); ?></label>

		<div class="col-sm-10">
			<?php 
			    $value = NULL;
			    if($this->input->post('id_file_area') != NULL){
			        $value = $this->input->post('id_file_area');
			    }elseif($mode == 'edit'){
			        $value = $fields['id_file_area'];
			    }
			?>

			<?php if ($value != NULL) : ?>
			    <span id="picture">
			        <span id="delete-pic">&#10006;</span>
			        <img src="<?php echo base_url(); ?>files/thumb/<?php echo $value; ?>/100/100" />
			    </span>
			<?php endif; ?>
			<input type="file" name="area_file">

			<input name="id_file_area_existing" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="id-file-area-existing" />
			<input name="id_file_area" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="id-file-area" />
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>