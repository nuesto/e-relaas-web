<div class="page-header">
	<h1><?php echo lang('nasabah:produk:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<input type="hidden" name="id_nasabah" value="<?php echo $id_nasabah; ?>">

	<div class="row">
		<div class="col-sm-6">
			<h4 class="subheader smaller lighter">
				<?php echo lang('nasabah:produk:bisnis'); ?>
			</h4>

			<?php if(count($all_bisnis_products) > 0): ?>

			<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>No</th>
						<th><?php echo lang('nasabah:produk:singular').' & '.lang('nasabah:produk:status'); ?></th>
						<th><?php echo lang('nasabah:produk:keterangan'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no = 1;
					?>
					
					<?php foreach ($all_bisnis_products as $bisnis_product): ?>
					<tr>
						<td>
							<?php echo $no; $no++; ?>	
							<input type="hidden" name="id_product[<?php echo $bisnis_product['id_produk'] ?>]" value="<?php echo $bisnis_product['id_produk']; ?>">
						</td>

						<?php
						$product_full_name = array();
						$product_full_name[] = $bisnis_product['family_nama'];
						$product_full_name[] = $bisnis_product['item_nama'];
						$product_full_name[] = '<br /><strong>'.$bisnis_product['produk_nama'].'</strong>';
						?>
						<td>
							<?php echo implode(' &raquo; ', $product_full_name); ?>
							<br />
							<br />
							<input name="status[<?php echo $bisnis_product['id_produk'] ?>]" class="ace ace-switch ace-switch-2" type="checkbox" <?php if($bisnis_product['status'] == 1){echo 'checked';} ?>>
							<span class="lbl"></span>	
						</td>
						<td>
							<input type="text" name="keterangan[<?php echo $bisnis_product['id_produk'] ?>]" value="<?php echo $bisnis_product['keterangan'] ?>" />
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

			<?php else: ?>
				<div class="well"><?php echo lang('nasabah:manajemen:no_entry'); ?></div>
			<?php endif;?>
		</div>

		<div class="col-sm-6">
			<h4 class="subheader smaller lighter">
				<?php echo lang('nasabah:produk:konsumer'); ?>
			</h4>

			<?php if(count($all_konsumer_products) > 0): ?>

			<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>No</th>
						<th><?php echo lang('nasabah:produk:singular').' & '.lang('nasabah:produk:status'); ?></th>
						<th><?php echo lang('nasabah:produk:keterangan'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no = 1;
					?>
					
					<?php foreach ($all_konsumer_products as $konsumer_product): ?>
					<tr>
						<td>
							<?php echo $no; $no++; ?>	
							<input type="hidden" name="id_product[<?php echo $konsumer_product['id_produk'] ?>]" value="<?php echo $konsumer_product['id_produk']; ?>">
						</td>

						<?php
						$product_full_name = array();
						$product_full_name[] = $konsumer_product['family_nama'];
						$product_full_name[] = $konsumer_product['item_nama'];
						$product_full_name[] = '<br /><strong>'.$konsumer_product['produk_nama'].'</strong>';
						?>
						<td>
							<?php echo implode(' &raquo; ', $product_full_name); ?>
							<br />
							<br />
							<input name="status[<?php echo $konsumer_product['id_produk'] ?>]" class="ace ace-switch ace-switch-2" type="checkbox" <?php if($konsumer_product['status'] == 1){echo 'checked';} ?>>
							<span class="lbl"></span>	
						</td>
						<td>
							<input type="text" name="keterangan[<?php echo $konsumer_product['id_produk'] ?>]" value="<?php echo $konsumer_product['keterangan'] ?>" />
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

			<?php else: ?>
				<div class="well"><?php echo lang('nasabah:manajemen:no_entry'); ?></div>
			<?php endif;?>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>