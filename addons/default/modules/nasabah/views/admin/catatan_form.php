<?php echo Asset::js('jquery.autosize.min.js'); ?>

<div class="page-header">
	<h1><?php echo lang('nasabah:catatan:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<?php 
		$value = NULL;
		if($this->input->post('id_nasabah') != NULL){
			$value = $this->input->post('id_nasabah');
		}elseif($id_nasabah != NULL AND $mode == 'new'){
			$value = $id_nasabah;
		}elseif($mode == 'edit'){
			$value = $fields['id_nasabah'];
		}
	?>
	<input name="id_nasabah" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="catatan"><?php echo lang('nasabah:catatan:singular'); ?>*</label>

		<div class="col-sm-10">
			<?php 
			    $value = NULL;
			    if($this->input->post('catatan') != NULL){
			        $value = $this->input->post('catatan');
			    }elseif($mode == 'edit'){
			        $value = $fields['catatan'];
			    }
			?>
			
			<textarea name="catatan" class="autosize-transition form-control" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 52px;"><?php echo $value; ?></textarea>

			<script type="text/javascript">
				$(document).ready(function(){
					$('textarea[class*=autosize]').autosize({append: "\n"});
				});
			</script>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>