<?php
echo Asset::css('datepicker.css');

echo Asset::js('date-time/bootstrap-datepicker.js');
?>

<div class="page-header">
	<h1><?php echo lang('nasabah:nasabah:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama"><?php echo lang('nasabah:nasabah:nama'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama') != NULL){
					$value = $this->input->post('nama');
				}elseif($this->input->get('f-nama') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-nama');
				}elseif($mode == 'edit'){
					$value = $fields['nama'];
				}
			?>
			<input name="nama" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama"><?php echo lang('nasabah:nasabah:deskripsi'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('deskripsi') != NULL){
					$value = $this->input->post('deskripsi');
				}elseif($this->input->get('f-deskripsi') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-deskripsi');
				}elseif($mode == 'edit'){
					$value = $fields['deskripsi'];
				}
			?>
			<textarea name="deskripsi" type="text" class="col-xs-12 col-sm-8" rows="10" id="" /><?php echo $value; ?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_logo"><?php echo lang('nasabah:nasabah:id_logo'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_logo') != NULL){
					$value = $this->input->post('id_logo');
				}elseif($mode == 'edit'){
					$value = $fields['id_logo'];
				}
			?>

			<?php if ($value != NULL) : ?>
				<span id="picture">
					<span id="delete-pic">&#10006;</span>
					<img src="<?php echo base_url(); ?>files/thumb/<?php echo $value; ?>/200/250" />
				</span>
			<?php endif; ?>
			<input type="file" name="logo_file">

			<input name="id_logo_existing" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="id-logo-existing" />
			<input name="id_logo" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="id-logo" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="anniversary"><?php echo lang('nasabah:nasabah:anniversary'); ?></label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('anniversary') != NULL){
					$value = $this->input->post('anniversary');
				}elseif($this->input->get('f-anniversary') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-anniversary');
				}elseif($mode == 'edit'){
					$value = $fields['anniversary'];
				}
			?>
			<div class="input-group">
				<input class="form-control date-picker" name="anniversary" type="text" data-date-format="yyyy-mm-dd" value="<?php echo $value; ?>">
				<span class="input-group-addon">
					<i class="icon-calendar bigger-110"></i>
				</span>
			</div>
		</div>
	</div>

	<h3 class="header smaller lighter"><?php echo lang('nasabah:nasabah:classification'); ?></h3>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_group"><?php echo lang('nasabah:nasabah:id_group'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_group') != NULL){
					$value = $this->input->post('id_group');
				}elseif($this->input->get('f-id_group') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-id_group');
				}elseif($mode == 'edit'){
					$value = $fields['id_group'];
				}

				$options = array('' => '');
				foreach ($nasabah_group_list as $key => $nasabah_group) {
					$options[$nasabah_group['id']] = $nasabah_group['nama'];
				}
				echo form_dropdown('id_group', $options, $value);
			?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_segmen"><?php echo lang('nasabah:nasabah:id_segmen'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_segmen') != NULL){
					$value = $this->input->post('id_segmen');
				}elseif($this->input->get('f-id_segmen') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-id_segmen');
				}elseif($mode == 'edit'){
					$value = $fields['id_segmen'];
				}

				$options = array('' => '');
				foreach ($segmen_list as $key => $segmen) {
					$options[$segmen['id']] = $segmen['nama'];
				}
				echo form_dropdown('id_segmen', $options, $value);
			?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_industri"><?php echo lang('nasabah:nasabah:id_industri'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_industri') != NULL){
					$value = $this->input->post('id_industri');
				}elseif($this->input->get('f-id_industri') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-id_industri');
				}elseif($mode == 'edit'){
					$value = $fields['id_industri'];
				}

				$options = array('' => '');
				foreach ($industri_list as $key => $industri) {
					$options[$industri['id']] = $industri['nama'];
				}
				echo form_dropdown('id_industri', $options, $value);
			?>
		</div>
	</div>

	<h3 class="header smaller lighter"><?php echo lang('nasabah:nasabah:pic'); ?></h3>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="pic_nama"><?php echo lang('nasabah:nasabah:pic_nama'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('pic_nama') != NULL){
					$value = $this->input->post('pic_nama');
				}elseif($this->input->get('f-pic_nama') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-pic_nama');
				}elseif($mode == 'edit'){
					$value = $fields['pic_nama'];
				}
			?>
			<input name="pic_nama" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="pic_email"><?php echo lang('nasabah:nasabah:pic_email'); ?></label>

		<div class="col-sm-8">
			<?php 
				$value = NULL;
				if($this->input->post('pic_email') != NULL){
					$value = $this->input->post('pic_email');
				}elseif($this->input->get('f-pic_email') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-pic_email');
				}elseif($mode == 'edit'){
					$value = $fields['pic_email'];
				}
			?>
			<input name="pic_email" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="pic_handphone"><?php echo lang('nasabah:nasabah:pic_handphone'); ?></label>

		<div class="col-sm-6">
			<?php 
				$value = NULL;
				if($this->input->post('pic_handphone') != NULL){
					$value = $this->input->post('pic_handphone');
				}elseif($this->input->get('f-pic_handphone') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-pic_handphone');
				}elseif($mode == 'edit'){
					$value = $fields['pic_handphone'];
				}
			?>
			<input name="pic_handphone" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="pic_birthdate"><?php echo lang('nasabah:nasabah:pic_birthdate'); ?></label>

		<div class="col-sm-4">
			<?php 
				$value = NULL;
				if($this->input->post('pic_birthdate') != NULL){
					$value = $this->input->post('pic_birthdate');
				}elseif($this->input->get('f-pic_birthdate') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-pic_birthdate');
				}elseif($mode == 'edit'){
					$value = $fields['pic_birthdate'];
				}
			?>

			<div class="input-group">
				<input class="form-control date-picker" name="pic_birthdate" type="text" data-date-format="yyyy-mm-dd" value="<?php echo $value; ?>">
				<span class="input-group-addon">
					<i class="icon-calendar bigger-110"></i>
				</span>
			</div>
		</div>
	</div>

	<h3 class="header smaller lighter"><?php echo lang('nasabah:nasabah:relation'); ?></h3>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_cra"><?php echo lang('nasabah:nasabah:id_cra'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_cra') != NULL){
					$value = $this->input->post('id_cra');
				}elseif($this->input->get('f-id_cra') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-id_cra');
				}elseif($mode == 'edit'){
					$value = $fields['id_cra'];
				}
				$selected_id_cra = $value;

				$options = array('' => '');
				foreach ($cra_list as $key => $cra) {
					$options[$cra->id] = $cra->display_name;
				}
				echo form_dropdown('id_cra', $options, $value, 'id=id-cra');
			?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_crm"><?php echo lang('nasabah:nasabah:id_crm'); ?></label>

		<div class="col-sm-10" id="id-crm-container">
			<?php 
				$value = NULL;
				if($this->input->post('id_crm') != NULL){
					$value = $this->input->post('id_crm');
				}elseif($this->input->get('f-id_crm') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-id_crm');
				}elseif($mode == 'edit'){
					$value = $fields['id_crm'];
				}

				$options = array('' => '');
				if($selected_id_cra != NULL OR $selected_id_cra != ''){
					$crm_group = $this->group_m->get_group_by_slug('crm');
					$id_crm_group = $crm_group->id;
					$crm_list = $this->user_m->get_many_by(array(
						'group_id' => $id_crm_group,
						'id_cra' => $selected_id_cra,
					));

					foreach ($crm_list as $key => $crm) {
						$options[$crm->id] = $crm->display_name;
					}
				}
				echo form_dropdown('id_crm', $options, $value);
			?>
		</div>
	</div>

	<h3 class="header smaller lighter"><?php echo lang('nasabah:nasabah:contact'); ?></h3>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="website"><?php echo lang('nasabah:nasabah:website'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('website') != NULL){
					$value = $this->input->post('website');
				}elseif($this->input->get('f-website') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-website');
				}elseif($mode == 'edit'){
					$value = $fields['website'];
				}
			?>
			<input name="website" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="alamat"><?php echo lang('nasabah:nasabah:alamat'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('alamat') != NULL){
					$value = $this->input->post('alamat');
				}elseif($this->input->get('f-alamat') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-alamat');
				}elseif($mode == 'edit'){
					$value = $fields['alamat'];
				}
			?>
			<textarea name="alamat" class="col-xs-12 col-sm-8" rows="5"><?php echo $value; ?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="loc_langitude"><?php echo lang('nasabah:nasabah:location'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = -6.204;
				if($this->input->post('loc_latitude') != NULL){
					$value = $this->input->post('loc_latitude');
				}elseif($this->input->get('f-loc_latitude') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-loc_latitude');
				}elseif($mode == 'edit'){
					$value = ($fields['loc_latitude']!='') ? $fields['loc_latitude'] : $value;
				}
				$value_loc_latitude = $value;
			?>
			<input name="loc_latitude" id="loc_latitude" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />

			<?php 
				$value = 106.821;
				if($this->input->post('loc_langitude') != NULL){
					$value = $this->input->post('loc_langitude');
				}elseif($this->input->get('f-loc_langitude') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-loc_langitude');
				}elseif($mode == 'edit'){
					$value = ($fields['loc_langitude']!='') ? $fields['loc_langitude'] : $value;
				}
				$value_loc_langitude = $value;
			?>
			<input name="loc_langitude" id="loc_langitude" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />

			<input id="pac-input" class="controls" type="text" placeholder="Search Box">
			<div class="form-control" id="map"></div>
		</div>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#id-cra').change(function(){
			var selected_id_cra = $('#id-cra').val();
			loadCrmDropdownHtml(selected_id_cra);
		});

		function loadCrmDropdownHtml(id_cra){
			$('#id-crm-container').load("<?php echo site_url('c/nasabah/nasabah/crm_dropdown_html'); ?>/" + id_cra);
		}

		$('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
		  $(this).prev().focus();
		});
	});

	function initAutocomplete() {
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: <?php echo $value_loc_latitude; ?>, lng: <?php echo $value_loc_langitude; ?>},
			zoom: 17,
			mapTypeId: 'roadmap'
		});

		// Create the search box and link it to the UI element.
		var input = document.getElementById('pac-input');
		var searchBox = new google.maps.places.SearchBox(input);
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

		// Bias the SearchBox results towards current map's viewport.
		map.addListener('bounds_changed', function() {
			searchBox.setBounds(map.getBounds());
		});

		// Listen for the event fired when the user selects a prediction and retrieve
		// more details for that place.
		searchBox.addListener('places_changed', function() {
			var places = searchBox.getPlaces();

			if (places.length == 0) {
				return;
			}

			// For each place, get the icon, name and location.
			var bounds = new google.maps.LatLngBounds();
			places.forEach(function(place) {
				if (!place.geometry) {
					return;
				}

				if (place.geometry.viewport) {
					// Only geocodes have viewport.
					bounds.union(place.geometry.viewport);
				} else {
					bounds.extend(place.geometry.location);
				}
			});
			map.fitBounds(bounds);
		});

		// Place a draggable marker on the map
		var marker = new google.maps.Marker({
			position: {lat: <?php echo $value_loc_latitude; ?>, lng: <?php echo $value_loc_langitude; ?>},
			map: map,
			draggable:true,
			title:"Drag me!"
		});

		//get marker position and store in hidden input
		google.maps.event.addListener(marker, 'dragend', function (evt) {
			document.getElementById("loc_latitude").value = evt.latLng.lat().toFixed(3);
			document.getElementById("loc_langitude").value = evt.latLng.lng().toFixed(3);
		});

		google.maps.event.addListener(map, 'click', function(evt) {
			placeMarker(evt.latLng);
			document.getElementById("loc_latitude").value = evt.latLng.lat().toFixed(3);
			document.getElementById("loc_langitude").value = evt.latLng.lng().toFixed(3);
		});

		function placeMarker(location) {
			marker.setPosition(location);
		}
	}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJFlCAAMGo8fQ0tSOxhAZ4rphJBAo2uVA&libraries=places&callback=initAutocomplete" async defer></script>