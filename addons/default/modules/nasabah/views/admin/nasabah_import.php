<?php 
$import_nasabah_error_message = NULL;
if(isset($_SESSION['import_nasabah_error_message'])){
	$import_nasabah_error_message = $_SESSION['import_nasabah_error_message'];
	unset($_SESSION['import_nasabah_error_message']);
}

if($import_nasabah_error_message != NULL){ 
?>
<div class="alert alert-danger">
	<?php echo $import_nasabah_error_message; ?>
</div>
<?php } ?>

<div class="page-header">
	<h1><?php echo lang('nasabah:nasabah:'.$mode); ?></h1>
</div>


<?php echo form_open_multipart(uri_string()); ?>
<div class="row">
	<div class="col-sm-6">
		<div>
			<h4>Format File</h4>
			<hr>     
			<b>1. <?php echo lang('nasabah:nasabah:nama'); ?>.</b><i> - <span class="red">wajib, maksimal 255 karakter</span></i><br>
			<b>2. <?php echo lang('nasabah:nasabah:deskripsi'); ?>.</b><i> - <span class="red">opsional, maksimal 10000 karakter</span></i><br>
			<b>3. <?php echo lang('nasabah:nasabah:anniversary'); ?>.</b><i> - <span class="red">opsional, format yyyy-mm-dd</span></i><br>
			<b>4. <?php echo lang('nasabah:nasabah:id_group'); ?>.</b><i> - <span class="red">wajib</span></i><br>
			<b>5. <?php echo lang('nasabah:nasabah:id_segmen'); ?>.</b><i> - <span class="red">wajib</span></i><br>
			<b>6. <?php echo lang('nasabah:nasabah:id_industri'); ?>.</b><i> - <span class="red">opsional</span></i><br>
			<b>7. <?php echo lang('nasabah:nasabah:pic_nama'); ?>.</b><i> - <span class="red">opsional, maksimal 255 karakter</span></i><br>
			<b>8. <?php echo lang('nasabah:nasabah:pic_email'); ?>.</b><i> - <span class="red">opsional, maksimal 255 karakter</span></i><br>
			<b>9. <?php echo lang('nasabah:nasabah:pic_handphone'); ?>.</b><i> - <span class="red">opsional, maksimal 25 karakter</span></i><br>
			<b>10. <?php echo lang('nasabah:nasabah:pic_birthdate'); ?>.</b><i> - <span class="red">opsional, format yyyy-mm-dd</span></i><br>
			<b>11. <?php echo lang('nasabah:nasabah:id_crm'); ?>.</b><i> - <span class="red">wajib</span></i><br>
			<br>
			<br>
			<b>
				<span class="red">Catatan * :</span>    
				<br><span class="red">- File CSV harus disertai dengan header</span>
				<br><span class="red">- Nama Group, Segmen, Industri, dan CRM harus sama dengan yang sudah diinput ke dalam sistem</span>
			</b>
			<hr>
		</div>
	</div>

	<div class="col-sm-6">
		<div>
			<h4>Pilih FIle</h4>
			<hr>
			<input type="file" name="file" id="files">
			<div style="padding:5px 0px;">
				<a href="<?php echo base_url() ?>c/nasabah/nasabah/csv_example">Download contoh csv</a>
			</div>

			<button class="btn btn-sm btn-primary btn-start" id="btn-process" type="submit"><i class="fa fa-play"></i> <span class="lbl-start">Mulai</span></button>
		</div>
	</div>
</div>

<?php echo form_close();?>
