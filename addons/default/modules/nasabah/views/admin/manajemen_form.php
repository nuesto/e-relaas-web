<?php
echo Asset::css('datepicker.css');

echo Asset::js('date-time/bootstrap-datepicker.js');
?>
<div class="page-header">
	<h1><?php echo lang('nasabah:manajemen:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<?php 
		$value = NULL;
		if($this->input->post('id_nasabah') != NULL){
			$value = $this->input->post('id_nasabah');
		}elseif($id_nasabah != NULL AND $mode == 'new'){
			$value = $id_nasabah;
		}elseif($mode == 'edit'){
			$value = $fields['id_nasabah'];
		}
	?>
	<input name="id_nasabah" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="kategori"><?php echo lang('nasabah:manajemen:kategori'); ?>*</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('kategori') != NULL){
					$value = $this->input->post('kategori');
				}elseif($this->input->get('f-kategori') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-kategori');
				}elseif($mode == 'edit'){
					$value = $fields['kategori'];
				}
			?>

			<select name="kategori" class="col-xs-10 col-sm-4">
				<option <?php if($value == 'Dewan Komisaris'){echo 'selected';} ?> value="Dewan Komisaris">Dewan Komisaris</option>
				<option <?php if($value == 'Dewan Direksi'){echo 'selected';} ?> value="Dewan Direksi">Dewan Direksi</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama"><?php echo lang('nasabah:manajemen:nama'); ?>*</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama') != NULL){
					$value = $this->input->post('nama');
				}elseif($this->input->get('f-nama') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-nama');
				}elseif($mode == 'edit'){
					$value = $fields['nama'];
				}
			?>
			<input name="nama" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="jabatan"><?php echo lang('nasabah:manajemen:jabatan'); ?>*</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('jabatan') != NULL){
					$value = $this->input->post('jabatan');
				}elseif($this->input->get('f-jabatan') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-jabatan');
				}elseif($mode == 'edit'){
					$value = $fields['jabatan'];
				}
			?>
			<input name="jabatan" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_file_foto"><?php echo lang('nasabah:manajemen:id_file_foto'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_file_foto') != NULL){
					$value = $this->input->post('id_file_foto');
				}elseif($mode == 'edit'){
					$value = $fields['id_file_foto'];
				}
			?>

			<?php if ($value != NULL && file_exists(base_url().'files/thumb/'.$value)) : ?>
				<span id="picture">
					<span id="delete-pic">&#10006;</span>
					<img src="<?php echo base_url(); ?>files/thumb/<?php echo $value; ?>/100/100" />
				</span>
			<?php endif; ?>
			<input type="file" name="foto_file">

			<input name="id_file_foto_existing" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="id-foto-existing" />
			<input name="id_file_foto" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="id-foto" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="birthdate"><?php echo lang('nasabah:manajemen:birthdate'); ?></label>

		<div class="col-sm-2">
			<?php 
				$value = NULL;
				if($this->input->post('birthdate') != NULL){
					$value = $this->input->post('birthdate');
				}elseif($this->input->get('f-birthdate') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-birthdate');
				}elseif($mode == 'edit'){
					$value = $fields['birthdate'];
				}

				$value = ($value != NULL) ? date_idr($value, 'Y-m-d', NULL) : $value;
			?>
			<div class="input-group">
				<input class="form-control date-picker" name="birthdate" type="text" data-date-format="yyyy-mm-dd" value="<?php echo $value; ?>">
				<span class="input-group-addon">
					<i class="icon-calendar bigger-110"></i>
				</span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('nasabah:ordering_count'); ?></label>

		<div class="col-sm-1">
			<?php 
				$value = NULL;
				if($this->input->post('ordering_count') != NULL){
					$value = $this->input->post('ordering_count');
				}elseif($mode == 'edit'){
					$value = $fields['ordering_count'];
				}
			
				$options = array();
				for ($i=0; $i <= 10; $i++) { 
					$options[$i] = $i;
				}
				echo form_dropdown('ordering_count', $options, $value);
			?>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>

<script type="text/javascript">
	$(document).ready(function(){
		$('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
		  $(this).prev().focus();
		});
	});
</script>