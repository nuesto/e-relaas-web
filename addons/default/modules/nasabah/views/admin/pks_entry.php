<div class="page-header">
	<h1><?php echo lang('nasabah:pks:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('c/nasabah/pks/index'.$uri); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('nasabah:back') ?>
		</a>

		<?php if(group_has_role('nasabah', 'edit_all_pks')){ ?>
			<a href="<?php echo site_url('c/nasabah/pks/edit/'.$pks['id'].$uri); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('nasabah', 'edit_own_pks')){ ?>
			<?php if($pks->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('c/nasabah/pks/edit/'.$pks->id.$uri); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('nasabah', 'delete_all_pks')){ ?>
			<a href="<?php echo site_url('c/nasabah/pks/delete/'.$pks['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('nasabah', 'delete_own_pks')){ ?>
			<?php if($pks->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('c/nasabah/pks/delete/'.$pks['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $pks['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:pks:id_nasabah'); ?></div>
		<?php if(isset($pks['id_nasabah'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $pks['id_nasabah']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:pks:deskripsi'); ?></div>
		<?php if(isset($pks['deskripsi'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $pks['deskripsi']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:pks:tipe'); ?></div>
		<?php if(isset($pks['tipe'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $pks['tipe']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:pks:nomor'); ?></div>
		<?php if(isset($pks['nomor'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $pks['nomor']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:pks:tanggal_ttd'); ?></div>
		<?php if(isset($pks['tanggal_ttd'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $pks['tanggal_ttd']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:pks:jangka_waktu'); ?></div>
		<?php if(isset($pks['jangka_waktu'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $pks['jangka_waktu']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:pks:pejabat_bni'); ?></div>
		<?php if(isset($pks['pejabat_bni'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $pks['pejabat_bni']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:pks:pejabat_nasabah'); ?></div>
		<?php if(isset($pks['pejabat_nasabah'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $pks['pejabat_nasabah']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:pks:keterangan'); ?></div>
		<?php if(isset($pks['keterangan'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $pks['keterangan']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:created'); ?></div>
		<?php if(isset($pks['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($pks['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:updated'); ?></div>
		<?php if(isset($pks['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($pks['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($pks['created_by'], true); ?></div>
	</div>
</div>