<div class="page-header">
	<h1><?php echo lang('nasabah:group:'.$mode); ?></h1>
</div>


<?php echo form_open_multipart(uri_string()); ?>
<div class="row">
	<div class="col-sm-6">
		<div>
			<h4>Format File</h4>
			<hr>     
			<b>1. <?php echo lang('nasabah:group:nama'); ?>.</b><i> - <span class="red">wajib diisi, max 255 karakter</span></i><br>
			<br>
			<br>
			<b>
				<span class="red">Catatan * :</span>    
				<br><span class="red">- File CSV harus disertai dengan header</span>
				<br><span class="red">- Nama grup yang sudah terdapat pada database akan di-<i>skip</i></span>
			</b>
			<hr>
		</div>
	</div>

	<div class="col-sm-6">
		<div>
			<h4>Pilih File</h4>
			<hr>
			<input type="file" name="file" id="files">
			<div style="padding:5px 0px;">
				<a href="<?php echo base_url() ?>c/nasabah/group/csv_example">Download contoh csv</a>
			</div>

			<button class="btn btn-sm btn-primary btn-start" id="btn-process" type="submit"><i class="fa fa-play"></i> <span class="lbl-start">Upload</span></button>
		</div>
	</div>
</div>

<?php echo form_close();?>
