<?php echo Asset::css('datepicker.css'); ?>

<?php echo Asset::js('date-time/bootstrap-datepicker.js'); ?>
<?php echo Asset::js('fuelux/fuelux.spinner.min.js'); ?>

<div class="page-header">
	<h1><?php echo lang('nasabah:pks:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<?php 
		$value = NULL;
		if($this->input->post('id_nasabah') != NULL){
			$value = $this->input->post('id_nasabah');
		}elseif($mode == 'new'){
			$value = $id_nasabah;
		}elseif($mode == 'edit'){
			$value = $fields['id_nasabah'];
		}
	?>
	<input name="id_nasabah" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="deskripsi"><?php echo lang('nasabah:pks:deskripsi'); ?>*</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('deskripsi') != NULL){
					$value = $this->input->post('deskripsi');
				}elseif($this->input->get('f-deskripsi') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-deskripsi');
				}elseif($mode == 'edit'){
					$value = $fields['deskripsi'];
				}
			?>
			<input name="deskripsi" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tipe"><?php echo lang('nasabah:pks:tipe'); ?>*</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('tipe') != NULL){
					$value = $this->input->post('tipe');
				}elseif($this->input->get('f-tipe') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-tipe');
				}elseif($mode == 'edit'){
					$value = $fields['tipe'];
				}
			?>
			<input name="tipe" type="text" value="<?php echo $value; ?>" class="col-xs-8 col-sm-2" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nomor"><?php echo lang('nasabah:pks:nomor'); ?>*</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nomor') != NULL){
					$value = $this->input->post('nomor');
				}elseif($this->input->get('f-nomor') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-nomor');
				}elseif($mode == 'edit'){
					$value = $fields['nomor'];
				}
			?>
			<input name="nomor" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tanggal_ttd"><?php echo lang('nasabah:pks:tanggal_ttd'); ?>*</label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('tanggal_ttd') != NULL){
					$value = $this->input->post('tanggal_ttd');
				}elseif($this->input->get('f-tanggal_ttd') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-tanggal_ttd');
				}elseif($mode == 'edit'){
					$value = $fields['tanggal_ttd'];
				}

				if($value != NULL AND $value != ''){
		        	$value = substr($value, 0, 10);
		        }
				
			?>
			
			<div class="input-group">
				<input class="form-control date-picker" id="id-date-picker-1" name="tanggal_ttd" type="text" data-date-format="yyyy-mm-dd" value="<?php echo $value; ?>">
				<span class="input-group-addon">
					<i class="icon-calendar bigger-110"></i>
				</span>
			</div>

			<script type="text/javascript">
				$(document).ready(function(){
					$('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
						$(this).prev().focus();
					});
				});
			</script>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="jangka_waktu"><?php echo lang('nasabah:pks:jangka_waktu'); ?>*</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('jangka_waktu') != NULL){
					$value = $this->input->post('jangka_waktu');
				}elseif($this->input->get('f-jangka_waktu') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-jangka_waktu');
				}elseif($mode == 'edit'){
					$value = $fields['jangka_waktu'];
				}
			?>

			<div class="ace-spinner">
				<div class="input-group">
					<input type="text" name="jangka_waktu" class="spinner-input form-control" id="spinner1" maxlength="3" value="<?php echo $value; ?>">
				</div>
			</div>

			<script type="text/javascript">
				$(document).ready(function(){
					$('#spinner1').ace_spinner({<?php if($value != NULL AND $value != ''){echo 'value:'.$value.',';} ?> min:0,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'});
				});
			</script>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="keterangan"><?php echo lang('nasabah:pks:keterangan'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('keterangan') != NULL){
					$value = $this->input->post('keterangan');
				}elseif($this->input->get('f-keterangan') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-keterangan');
				}elseif($mode == 'edit'){
					$value = $fields['keterangan'];
				}
			?>
			<input name="keterangan" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('nasabah:ordering_count'); ?></label>

		<div class="col-sm-1">
			<?php 
				$value = NULL;
				if($this->input->post('ordering_count') != NULL){
					$value = $this->input->post('ordering_count');
				}elseif($mode == 'edit'){
					$value = $fields['ordering_count'];
				}
			
				$options = array();
				for ($i=0; $i <= 10; $i++) { 
					$options[$i] = $i;
				}
				echo form_dropdown('ordering_count', $options, $value);
			?>
		</div>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>