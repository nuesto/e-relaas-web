<div class="page-header">
	<h1><?php echo lang('nasabah:manajemen:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('c/nasabah/manajemen/index'.$uri); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('nasabah:back') ?>
		</a>

		<?php if(group_has_role('nasabah', 'edit_all_nasabah')){ ?>
			<a href="<?php echo site_url('c/nasabah/manajemen/edit/'.$manajemen['id'].$uri); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('nasabah', 'edit_own_nasabah')){ ?>
			<?php if($manajemen->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('c/nasabah/manajemen/edit/'.$manajemen->id.$uri); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('nasabah', 'edit_all_nasabah')){ ?>
			<a href="<?php echo site_url('c/nasabah/manajemen/delete/'.$manajemen['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('nasabah', 'edit_own_nasabah')){ ?>
			<?php if($manajemen->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('c/nasabah/manajemen/delete/'.$manajemen['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $manajemen['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:manajemen:id_nasabah'); ?></div>
		<?php if(isset($manajemen['id_nasabah'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $manajemen['id_nasabah']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:manajemen:kategori'); ?></div>
		<?php if(isset($manajemen['kategori'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $manajemen['kategori']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:manajemen:nama'); ?></div>
		<?php if(isset($manajemen['nama'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $manajemen['nama']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:manajemen:jabatan'); ?></div>
		<?php if(isset($manajemen['jabatan'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $manajemen['jabatan']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:created'); ?></div>
		<?php if(isset($manajemen['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($manajemen['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:updated'); ?></div>
		<?php if(isset($manajemen['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($manajemen['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($manajemen['created_by'], true); ?></div>
	</div>
</div>