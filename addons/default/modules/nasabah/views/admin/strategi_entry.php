<div class="page-header">
	<h1><?php echo lang('nasabah:strategi:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('c/nasabah/strategi/index'.$uri); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('nasabah:back') ?>
		</a>

		<?php if(group_has_role('nasabah', 'edit_all_strategi')){ ?>
			<a href="<?php echo site_url('c/nasabah/strategi/edit/'.$strategi['id'].$uri); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('nasabah', 'edit_own_strategi')){ ?>
			<?php if($strategi->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('c/nasabah/strategi/edit/'.$strategi->id.$uri); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('nasabah', 'delete_all_strategi')){ ?>
			<a href="<?php echo site_url('c/nasabah/strategi/delete/'.$strategi['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('nasabah', 'delete_own_strategi')){ ?>
			<?php if($strategi->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('c/nasabah/strategi/delete/'.$strategi['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $strategi['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:strategi:id_nasabah'); ?></div>
		<?php if(isset($strategi['id_nasabah'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $strategi['id_nasabah']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:strategi:konten'); ?></div>
		<?php if(isset($strategi['konten'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $strategi['konten']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:strategi:change_history'); ?></div>
		<?php if(isset($strategi['change_history'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $strategi['change_history']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:created'); ?></div>
		<?php if(isset($strategi['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($strategi['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:updated'); ?></div>
		<?php if(isset($strategi['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($strategi['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('nasabah:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($strategi['created_by'], true); ?></div>
	</div>
</div>