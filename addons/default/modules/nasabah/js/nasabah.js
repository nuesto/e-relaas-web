$(document).ready(function(){
	$('#delete-pic').click(function(){
		$('#picture').hide();
		$('#id-logo').val('');
		$('#id-file-foto').val('');
		$('#id-file-struktur').val('');
		$('#id-file-area').val('');
	});

	$('#pac-input').keypress(function(e) {
	    if(e.which == 13) {
	        event.preventDefault();
			event.stopPropagation();
			return false;
	    }
	});
});