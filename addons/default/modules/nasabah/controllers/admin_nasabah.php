<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Nasabah Module
 *
 * Manajemen nasabah corporate
 *
 */
class Admin_nasabah extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'nasabah';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'access_nasabah_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('nasabah');		
		$this->load->model('nasabah_m');
		$this->load->model('nasabah_group_m');
		$this->load->model('segmen_m');
		$this->load->model('industri_m');
		$this->load->model('group_m');
		$this->load->model('user_m');
		$this->load->library('files/files');
    }

    /**
	 * List all Nasabah
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_nasabah') AND ! group_has_role('nasabah', 'view_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){
				$column = substr($key, 2);
				$function = ($column != 'nama') ? 'where' : 'like';
				$params[] = array('column'=>'nasabah_nasabah.'.$column,'function'=>$function,'value'=>$value);
			}
		}

		$data['show_cra'] = true;
		$data['show_crm'] = true;

		// Check permission if user just has view own

		if($this->current_user->group == 'cra' || $this->current_user->group == 'crm'){
			if(! group_has_role('nasabah', 'view_all_nasabah') AND group_has_role('nasabah', 'view_own_nasabah')){
				$params[] = array('column'=>'nasabah_nasabah.id_'.$this->current_user->group,'function'=>'where','value'=>$this->current_user->id);

				if($this->current_user->group == 'crm'){
					$data['show_cra'] = false;
					$data['show_crm'] = false;
				}
				if($this->current_user->group == 'cra'){
					$data['show_cra'] = false;
					$data['selected_id_cra'] = $this->current_user->id;
				}
			}
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'c/nasabah/nasabah/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->nasabah_m->count_all_nasabah($params);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

        $data['nasabah']['entries'] = $this->nasabah_m->get_nasabah($pagination_config, $params);
		$data['nasabah']['total'] = $pagination_config['total_rows'];
		$data['nasabah']['pagination'] = $this->pagination->create_links();

		$data['nasabah_group_list'] = $this->nasabah_group_m->get_group();
		$data['segmen_list'] = $this->segmen_m->get_segmen();

		// pupulate list of users which group = CRA
		$cra_group = $this->group_m->get_group_by_slug('cra');
		$id_cra_group = $cra_group->id;
		$data['cra_list'] = $this->user_m->get_many_by(array('group_id' => $id_cra_group));


		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['nasabah']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('c/nasabah/nasabah/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('c/nasabah/nasabah/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('nasabah:nasabah:plural'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->append_css('module::nasabah.css')
			->append_js('module::nasabah.js')
			->build('admin/nasabah_index', $data);
    }
	
	/**
     * Display one Nasabah
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_nasabah') AND ! group_has_role('nasabah', 'view_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c/nasabah/nasabah/index');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['nasabah'] = $this->nasabah_m->get_nasabah_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_nasabah')){
			if($data['nasabah']['id_'.$this->current_user->group] != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c/nasabah/nasabah/index');
			}
		}

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('nasabah:nasabah:view'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index');
		}

		$this->template->set_breadcrumb(lang('nasabah:nasabah:view'))
			->append_css('module::nasabah.css')
			->append_js('module::nasabah.js')
			->build('admin/nasabah_entry', $data);
    }
	
	/**
     * Create a new Nasabah entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'create_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_nasabah('new')){	
				$this->session->set_flashdata('success', lang('nasabah:nasabah:submit_success'));				
				redirect('c/nasabah/nasabah/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('nasabah:nasabah:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/nasabah/nasabah/index'.$data['uri'];
		$data['nasabah_group_list'] = $this->nasabah_group_m->get_group();
		$data['segmen_list'] = $this->segmen_m->get_segmen();
		$data['industri_list'] = $this->industri_m->get_industri();

		// pupulate list of users which group = CRA
		$cra_group = $this->group_m->get_group_by_slug('cra');
		$id_cra_group = $cra_group->id;
		$data['cra_list'] = $this->user_m->get_many_by(array('group_id' => $id_cra_group));
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:nasabah:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index');
		}

		$this->template->set_breadcrumb(lang('nasabah:nasabah:new'))
			->append_css('module::nasabah.css')
			->append_js('module::nasabah.js')
			->build('admin/nasabah_form', $data);
    }

    /**
     * Edit a Nasabah entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Nasabah to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c/nasabah/nasabah/index');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c/nasabah/nasabah/index');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_nasabah('edit', $id)){	
				$this->session->set_flashdata('success', lang('nasabah:nasabah:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id.$data['uri']);
			}else{
				$data['messages']['error'] = lang('nasabah:nasabah:submit_failure');
			}
		}
		
		$data['fields'] = $this->nasabah_m->get_nasabah_by_id($id);
		$data['fields']['pic_birthdate'] = substr($data['fields']['pic_birthdate'], 0, 10);
		$data['mode'] = 'edit';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		$data['nasabah_group_list'] = $this->nasabah_group_m->get_group();
		$data['segmen_list'] = $this->segmen_m->get_segmen();
		$data['industri_list'] = $this->industri_m->get_industri();

		// pupulate list of users which group = CRA
		$cra_group = $this->group_m->get_group_by_slug('cra');
		$id_cra_group = $cra_group->id;
		$data['cra_list'] = $this->user_m->get_many_by(array('group_id' => $id_cra_group));
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:nasabah:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:nasabah:edit'))
			->append_css('module::nasabah.css')
			->append_js('module::nasabah.js')
			->build('admin/nasabah_form', $data);
    }

     public function nasabah_dpk($id_nasabah){

    	if(! group_has_role('nasabah', 'view_all_nasabah') AND ! group_has_role('nasabah', 'view_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'view_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}


		$this->load->model('dpk/dpk_m');

		$dpk_table = array();
		$dpk_table['GIRO'] = array();
		$dpk_table['DEPOSITO'] = array();
		$dpk_table['TABUNGAN'] = array();
		$ccy_existing = array('IDR','VA');
		$data_dpk = $this->dpk_m->get_dpk_by_nasabah($id_nasabah);
		$total_ccy = array();
		$total_ccy_format = array();
		$total_dpk = 0;
		foreach ($data_dpk as $key => $dpk) {
			$dpk['ccy'] = ($dpk['ccy'] != 'IDR') ? 'VA' : 'IDR';
			$dpk_table[$dpk['produk']]['ccy'][$dpk['ccy']] = (float) $dpk['total_ccy'];
			$dpk_table[$dpk['produk']]['ccy_format'][$dpk['ccy']] = number_format($dpk['total_ccy'],2);
			$dpk_table[$dpk['produk']]['rekening'] = $dpk['rekening'];
			$dpk_table[$dpk['produk']]['total_ccy'] = array_sum($dpk_table[$dpk['produk']]['ccy']);
			$dpk_table[$dpk['produk']]['total_ccy_format'] = number_format($dpk_table[$dpk['produk']]['total_ccy'],2);

			if(!isset($total_ccy[$dpk['ccy']])){
				$total_ccy[$dpk['ccy']] = 0;
			}
			$total_ccy[$dpk['ccy']] += (float) $dpk['total_ccy'];
			$total_ccy_format[$dpk['ccy']] = number_format($total_ccy[$dpk['ccy']],2);
			
		}
		$total_per_produk = array();
		$total_dpk = array_sum($total_ccy);

		$data['dpk_table'] = $dpk_table;
		$data['ccy_existing'] =$ccy_existing;
		$data['data_dpk'] = $data_dpk;
		$data['total_ccy'] = $total_ccy;
		$data['total_ccy_format'] = $total_ccy_format;
		$data['total_dpk'] = $total_dpk;

		$data['total_dpk_format'] = number_format($total_dpk,2);

		// DPK HISTORY
		$dpk_history = $this->dpk_m->get_history_dpk_by_nasabah($id_nasabah);
		$history = array();
		$tanggal_data = array();
		$total_ccy_history = array();
		$data['last_updated'] = format_date_id(date('Y-m-d'), 'd M  Y', '');
		$data['obj_tgl'] = '';
		$data['obj_dpk'] = '';
		$data['text_range_tanggal'] = $data['last_updated'];
		foreach ($dpk_history as $key => $dpk) {
			$dpk['ccy'] = ($dpk['ccy'] != 'IDR') ? 'VA' : 'IDR';
			if(!isset($total_ccy_history[$dpk['ccy']])){
				$total_ccy_history[$dpk['ccy']] = 0;
			}

			if(!array_search($dpk['tanggal_data'], $tanggal_data)){

				$tanggal[] = format_date($dpk['tanggal_data'], 'd/m/Y');
				$tanggal_data[] = $dpk['tanggal_data'];
			}
			$total_ccy_history[$dpk['ccy']] += (float) $dpk['total_ccy'];

			$history[$dpk['tanggal_data']] = array_sum($total_ccy_history);
			
		}
		if(count($tanggal_data) > 0){
			$data['last_updated'] = format_date_id($tanggal_data[count($tanggal_data)-1], 'd M  Y', '');
			$data['text_range_tanggal'] = ($data['last_updated'] != format_date_id($tanggal_data[0], 'd M  Y', '')) ? format_date_id($tanggal_data[0], 'd M  Y', '') . ' sampai '.$data['last_updated'] : $data['last_updated'];
			$data['obj_tgl'] = "'".implode("','", $tanggal)."'";
			$data['obj_dpk'] = implode(',', $history);
		}

		// tabel deposito
		$params[] = array('column'=>'id_nasabah','function'=>'where','value'=>$id_nasabah);
		$params[] = array('column'=>'produk','function'=>'where','value'=>'DEPOSITO');
		$data['deposito_entries'] = $this->dpk_m->get_dpk(NULL, $params);
		
    	$this->load->view('dpk/admin/dpk_nasabah', $data);
    }

    /**
     * Create a new Struktur entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create_struktur($id_nasabah)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}

		$data['uri'] = get_query_string(6);

		$data['id_nasabah'] = $id_nasabah;
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_struktur('new', $id_nasabah)){	
				$this->session->set_flashdata('success', lang('nasabah:struktur:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#struktur');
			}else{
				$data['messages']['error'] = lang('nasabah:struktur:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#struktur';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:struktur:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id_nasabah);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:struktur:new'))
			->build('admin/struktur_form', $data);
    }

    public function edit_struktur($id_nasabah = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_struktur('edit', $id_nasabah)){	
				$this->session->set_flashdata('success', lang('nasabah:struktur:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#struktur');
			}else{
				$data['messages']['error'] = lang('nasabah:struktur:submit_failure');
			}
		}
		
		$data['fields'] = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
		$data['fields']['id_nasabah'] = $id_nasabah;
		$data['mode'] = 'edit';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#struktur';

		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:struktur:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id_nasabah);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:struktur:edit'))
			->append_css('module::nasabah.css')
			->append_js('module::nasabah.js')
			->build('admin/struktur_form', $data);
    }

    /**
     * Create a new Area entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create_area($id_nasabah)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}

		$data['uri'] = get_query_string(6);

		$data['id_nasabah'] = $id_nasabah;
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_area('new', $id_nasabah)){	
				$this->session->set_flashdata('success', lang('nasabah:area:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#area');
			}else{
				$data['messages']['error'] = lang('nasabah:area:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#area';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:area:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id_nasabah);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:area:new'))
			->build('admin/area_form', $data);
    }

    public function edit_area($id_nasabah = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_area('edit', $id_nasabah)){	
				$this->session->set_flashdata('success', lang('nasabah:area:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#area');
			}else{
				$data['messages']['error'] = lang('nasabah:area:submit_failure');
			}
		}
		
		$data['fields'] = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
		$data['fields']['id_nasabah'] = $id_nasabah;
		$data['mode'] = 'edit';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#area';

		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:area:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id_nasabah);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:area:edit'))
			->append_css('module::nasabah.css')
			->append_js('module::nasabah.js')
			->build('admin/area_form', $data);
    }

    /**
     * Create a new Catatan entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create_catatan($id_nasabah)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}

		$data['uri'] = get_query_string(6);

		$data['id_nasabah'] = $id_nasabah;
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_catatan('new', $id_nasabah)){	
				$this->session->set_flashdata('success', lang('nasabah:catatan:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#catatan');
			}else{
				$data['messages']['error'] = lang('nasabah:catatan:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#catatan';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:catatan:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id_nasabah);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:catatan:new'))
			->build('admin/catatan_form', $data);
    }

    public function edit_catatan($id_nasabah = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_catatan('edit', $id_nasabah)){	
				$this->session->set_flashdata('success', lang('nasabah:catatan:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#catatan');
			}else{
				$data['messages']['error'] = lang('nasabah:catatan:submit_failure');
			}
		}
		
		$data['fields'] = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
		$data['fields']['id_nasabah'] = $id_nasabah;
		$data['mode'] = 'edit';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#catatan';

		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:area:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id_nasabah);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:area:edit'))
			->append_css('module::nasabah.css')
			->append_js('module::nasabah.js')
			->build('admin/catatan_form', $data);
    }

    public function crm_dropdown_html($id_cra = NULL)
    {
    	$crm_list = array();
    	if($id_cra != NULL){
    		$crm_group = $this->group_m->get_group_by_slug('crm');
			$id_crm_group = $crm_group->id;
			$crm_list = $this->user_m->get_many_by(array(
				'group_id' => $id_crm_group,
				'id_cra' => $id_cra,
			));
    	}

		$options = array('' => '');
		foreach ($crm_list as $key => $crm) {
			$options[$crm->id] = $crm->display_name;
		}
		echo form_dropdown('id_crm', $options, '', 'id=id-cra');
    }

    /**
	 * Method for handling different form actions
	 */
	public function action()
	{
		// Pyro demo version restrction
		if (PYRO_DEMO)
		{
			$this->session->set_flashdata('notice', lang('global:demo_restrictions'));
			redirect('admin/users');
		}

		// Determine the type of action
		switch ($this->input->post('btnAction'))
		{
			case 'delete':
				$this->delete();
				break;
			default:
				redirect('c/nasabah/nasabah/index');
				break;
		}
	}
	
	/**
     * Delete a Nasabah entry
     * 
     * @param   int [$id] The id of Nasabah to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'delete_all_nasabah') AND ! group_has_role('nasabah', 'delete_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		// Check view all/own permission
		if(! group_has_role('nasabah', 'delete_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------

		$ids = ($id > 0) ? array($id) : $this->input->post('action_to');

		if ( ! empty($ids))
		{
			$deleted = 0;
			$to_delete = 0;
			$deleted_ids = array();
			foreach ($ids as $id)
			{
				if ($this->nasabah_m->delete_nasabah_by_id($id))
				{
					$deleted_ids[] = $id;
					$deleted++;
				}
				$to_delete++;
			}

			if ($to_delete > 0)
			{
				$this->session->set_flashdata('success', sprintf(lang('nasabah:mass_delete_success'), $deleted, $to_delete));
			}
		}
		// The array of id's to delete is empty
		else
		{
			$this->session->set_flashdata('error', lang('nasabah:mass_delete_error'));
		}
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('c/nasabah/nasabah/index'.$data['uri']);
    }

 	/**
     * Import new Nasabah entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function import()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'import_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------

		if($_FILES) {
			$name = $_FILES['file']['name'];
			
			if($name!=null){
				if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
					$values = array();
					
					$skip_header = true;
					$row = 0;
					$error = array('');
					while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
						if($skip_header) {
							$skip_header = false;
						} else {
							$post_data[$row]['nama'] = trim($data[0]);
							$post_data[$row]['deskripsi'] = trim($data[1]);
							$post_data[$row]['anniversary'] = $data[2]!='' ? date('Y-m-d', strtotime(trim($data[2]))) : NULL;
							$temp[$row]['group'] = trim($data[3]);
							$post_data[$row]['id_group'] = $this->nasabah_group_m->get_group_id_by_name($temp[$row]['group']);
							$temp[$row]['segmen'] = trim($data[4]);
							$post_data[$row]['id_segmen'] = $this->segmen_m->get_segmen_id_by_name($temp[$row]['segmen']);
							$temp[$row]['industri'] = trim($data[5]);
							$post_data[$row]['id_industri'] = $this->industri_m->get_industri_id_by_name($temp[$row]['industri']);
							$post_data[$row]['pic_nama'] = trim($data[6]);
							$post_data[$row]['pic_email'] = $data[7]!='' ? trim($data[7]) : NULL;
							$post_data[$row]['pic_handphone'] = trim($data[8]);
							$post_data[$row]['pic_birthdate'] = $data[9]!='' ? date('Y-m-d', strtotime(trim($data[9]))) : NULL;

							$temp[$row]['crm'] = trim($data[10]);
							$crm_user = $this->_get_crm_or_cra_id_by_name($temp[$row]['crm']);
							$post_data[$row]['id_crm'] = $crm_user->id;
							// if the CRM is actually a CRA, assign himself as CRA
							if($crm_user->group_name == 'CRA' AND $crm_user->id_cra == NULL){
								$post_data[$row]['id_cra'] = $crm_user->id;
							}elseif($crm_user->group_name == 'CRM'){
								$post_data[$row]['id_cra'] = $crm_user->id_cra;
							}
							
							$post_data[$row]['website'] = trim($data[11]);
							$post_data[$row]['alamat'] = trim($data[12]);
						}

						$row++;
					}

					// data validation
					// Manual valudation (not using form_validation class) for csv data
					$num_invalid_rows = 0;
					$err_msg = array();
					$num_row_skipped = 0;
					$num_row_inserted = 0;
					$inserted_data = array();
					foreach ($post_data as $row => $value) {
						$skip_this_row = FALSE;

						// Nama: check if null or empty
						if($this->is_empty($value['nama'])){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_nama_required'), $row);
						}else{
							// Nama: check if already exist
							// if exist, will be skipped
							if($this->nasabah_m->get_nasabah_id_by_name($value['nama']) != NULL){
								// $skip_this_row = TRUE;
								// $num_row_skipped++;
								$num_invalid_rows++;
								$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_nama_exist'), $row, $value['nama']);
							}else{
								$num_row_inserted++;
							}

							// Nama: check if exceed max length
							if(!$this->is_empty($value['nama']) AND strlen($value['nama']) > 255){
								$num_invalid_rows++;
								$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_nama_max_length'), $row, 255);
							}
						}

						// Deskripsi: check if exceed max length
						if(!$this->is_empty($value['deskripsi']) AND strlen($value['deskripsi']) > 10000){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_deskripsi_max_length'), $row, 10000);
						}

						// Anniversary: check if valid date format
						if(!$this->is_empty($value['anniversary']) AND !$this->_valid_mysql_date($value['anniversary'])){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_anniversary_valid_date'), $row);
						}

						// Group: check if exist
						if($this->is_empty($temp[$row]['group'])){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_group_required'), $row);
						}else{
							// Group: check if exist
							if($post_data[$row]['id_group'] == NULL){
								$num_invalid_rows++;
								$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_group_not_exist'), $row, $temp[$row]['group']);
							}
						}

						// Segmen: check if empty
						if($this->is_empty($temp[$row]['segmen'])){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_segmen_required'), $row);
						}else{
							// Segmen: check if not exist
							if($post_data[$row]['id_segmen'] == NULL){
								$num_invalid_rows++;
								$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_segmen_not_exist'), $row, $temp[$row]['segmen']);
							}
						}

						// Industri: check if not exist
						if(!$this->is_empty($temp[$row]['industri']) AND $post_data[$row]['id_industri'] == NULL){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_industri_not_exist'), $row, $temp[$row]['industri']);
						}

						// PIC Nama: check if exceed max length
						if(!$this->is_empty($value['pic_nama']) AND strlen($value['pic_nama']) > 255){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_pic_nama_max_length'), $row, 255);
						}

						// PIC Email: check if exceed max length
						if(!$this->is_empty($value['pic_email']) AND strlen($value['pic_email']) > 255){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_pic_email_max_length'), $row, 255);
						}

						// PIC Email: check if not valid email format
						if(!$this->is_empty($value['pic_email']) AND !filter_var($value['pic_email'], FILTER_VALIDATE_EMAIL)){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_pic_email_not_valid'), $row);
						}

						// PIC Handphone: check if exceed max length
						if(!$this->is_empty($value['pic_handphone']) AND strlen($value['pic_handphone']) > 25){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_pic_handphone_max_length'), $row, 25);
						}

						// PIC Handphone: check if not valid phone number format
						if(!$this->is_empty($value['pic_handphone']) AND !$this->_valid_handphone($value['pic_handphone'])){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_pic_handphone_not_valid'), $row);
						}

						// PIC Birthdate: check if valid date format
						if(!$this->is_empty($value['pic_birthdate']) AND !$this->_valid_mysql_date($value['pic_birthdate'])){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_pic_birthdate_valid_date'), $row);
						}

						// CRM: check if empty
						if($this->is_empty($temp[$row]['crm'])){
							$num_invalid_rows++;
							$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_crm_required'), $row);
						}else{
							// CRM: check if not exist
							if($post_data[$row]['id_crm'] == NULL){
								$num_invalid_rows++;
								$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_crm_not_exist'), $row, $temp[$row]['crm']);
							}else{
								// CRA: check if empty
								if($this->is_empty($post_data[$row]['id_cra'])){
									$num_invalid_rows++;
									$err_msg[] = sprintf(lang('nasabah:validation_message:row_nasabah_cra_required'), $row, $temp[$row]['crm']);
								}
							}
						}

						if(!$skip_this_row){
							$inserted_data[] = $value;
						}
					}

					// handle manual validation for csv data
					if($num_invalid_rows > 0){
						$err_msg_str = implode("<br />", $err_msg);
						$err_msg_str = lang('nasabah:validation_message:file_not_valid').'<br /><br />'.$err_msg_str;

						$_SESSION['import_nasabah_error_message'] = $err_msg_str;
						$data['messages']['error'] = lang('nasabah:nasabah:submit_failure');

						$result = FALSE;
					}else{
						$this->db->trans_start();
						
						if(count($inserted_data) > 0){
							$this->nasabah_m->insert_batch_nasabah($inserted_data);
						}

						$this->db->trans_complete();
						$result = $this->db->trans_status();
						
						if($result) {
							$success_notif = lang('nasabah:nasabah:submit_success').'<br />'.sprintf(lang('nasabah:nasabah:submit_success_insert_skip_count'), $num_row_inserted, $num_row_skipped);
							
							$this->session->set_flashdata('success', $success_notif);
							redirect($this->uri->uri_string());
						}
					}
				}
			}
		}
		
		$data['mode'] = 'import';
		$data['return'] = 'c/nasabah/nasabah/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:nasabah:import'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index');
		}

		$this->template->set_breadcrumb(lang('nasabah:nasabah:import'))
			->build('admin/nasabah_import', $data);
    }
	
	/**
     * Insert or update Nasabah entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_nasabah($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		if($values['anniversary'] == ''){
			$values['anniversary'] = NULL;
		}
		if($values['pic_birthdate'] == ''){
			$values['pic_birthdate'] = NULL;
		}
		if($values['id_industri'] == ''){
			$values['id_industri'] = NULL;
		}

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama', lang('nasabah:nasabah:nama'), 'required|max_length[256]');
		$this->form_validation->set_rules('deskripsi', lang('nasabah:nasabah:deskripsi'), 'required|max_length[10000]');

		// -------------------------------------
		// Image Upload Handling
		// -------------------------------------

		if(strlen($_FILES['logo_file']['name']) <= 100){
			// Create folder if doesn't exist
			$id_folder = Files::get_id_by_name('Logo');
			if($id_folder == NULL){
				Files::create_folder(0, 'Logo', 'local', '', 0);
				$id_folder = Files::get_id_by_name('Logo');
			}
			
			// 0 = General error
			// 1 = Size error
			// 2 = Type error
			// 3 = OK
			$logo_file_valid_code = 3;
			
			// If there is a file uploaded, check for any error
			if($_FILES['logo_file']['error'] !== UPLOAD_ERR_NO_FILE){
				$info = getimagesize($_FILES['logo_file']['tmp_name']);

				if ($_FILES['logo_file']['error'] !== UPLOAD_ERR_OK) {
					$logo_file_valid_code = 0;
				}elseif ($info === FALSE) {
					$logo_file_valid_code = 1;
				}elseif (($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
					$logo_file_valid_code = 2;
				}

				// If no error, upload file
				if($logo_file_valid_code == 3){
					$data = Files::upload($id_folder, $_FILES['logo_file']['name'], 'logo_file');
					$_POST['id_logo'] = $data['data']['id'];
					$values['id_logo'] = $data['data']['id'];
				}
			}elseif($values['id_logo'] == ''){
				$values['id_logo'] = NULL;
			}

			// keep existing id for post processing
			$id_logo_existing = $this->input->post('id_logo_existing');
			unset($values['id_logo_existing']);

			// assign logo_file_valid_code value to a post parameter
			$_POST['logo_file_valid_code'] = $logo_file_valid_code;

			$this->form_validation->set_rules('logo_file_valid_code', lang('nasabah:nasabah:logo_file_valid_code'), 'callback__valid_upload_file');
		}else{
			$_POST['logo_file_name'] = $_FILES['logo_file']['name'];
			$this->form_validation->set_rules('logo_file_name', '<b>Nama File Logo</b>', 'max_length[100]');
		}


		$this->form_validation->set_rules('anniversary', lang('nasabah:nasabah:anniversary'), 'callback__valid_mysql_date');
		$this->form_validation->set_rules('id_group', lang('nasabah:nasabah:id_group'), 'required');
		$this->form_validation->set_rules('id_segmen', lang('nasabah:nasabah:id_segmen'), 'required');
		// $this->form_validation->set_rules('id_industri', lang('nasabah:nasabah:id_industri'), 'required');
		$this->form_validation->set_rules('pic_nama', lang('nasabah:nasabah:pic_nama'), 'max_length[256]');
		$this->form_validation->set_rules('pic_email', lang('nasabah:nasabah:pic_email'), 'valid_email|max_length[256]');
		$this->form_validation->set_rules('pic_handphone', lang('nasabah:nasabah:pic_handphone'), 'callback__valid_handphone|max_length[25]');
		$this->form_validation->set_rules('pic_birthdate', lang('nasabah:nasabah:pic_birthdate'), 'callback__valid_mysql_date');
		$this->form_validation->set_rules('id_cra', lang('nasabah:nasabah:id_cra'), 'required');
		$this->form_validation->set_rules('id_crm', lang('nasabah:nasabah:id_crm'), 'required');

		$this->form_validation->set_message('_valid_upload_file', lang('nasabah:validation_message:_valid_upload_file_'.$logo_file_valid_code));
		$this->form_validation->set_message('_valid_handphone', lang('nasabah:validation_message:_valid_handphone'));
		$this->form_validation->set_message('_valid_mysql_date', lang('nasabah:validation_message:_valid_mysql_date'));
		// Set Error Delimns

		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->nasabah_m->insert_nasabah($values);
			}
			else
			{
				$result = $this->nasabah_m->update_nasabah($values, $row_id);
			}
		}

		// post processing, clean up replaced or deleted file
		if($id_logo_existing != NULL AND $id_logo_existing != ''
			AND $id_logo_existing != $values['id_logo']){

			Files::delete_file($id_logo_existing);
		}
		
		return $result;
	}

	/**
     * Insert or update Struktur entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_struktur($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		$values['id'] = $values['id_nasabah'];
		unset($values['id_nasabah']);

		if(strlen($_FILES['struktur_file']['name']) <= 100){
			// -------------------------------------
			// Image Upload Handling
			// -------------------------------------

			// Create folder if doesn't exist
			$id_folder = Files::get_id_by_name('Struktur');
			if($id_folder == NULL){
				Files::create_folder(0, 'Struktur', 'local', '', 0);
				$id_folder = Files::get_id_by_name('Struktur');
			}
			
			// 0 = General error
			// 1 = Size error
			// 2 = Type error
			// 3 = OK
			$struktur_file_valid_code = 3;
			
			// If there is a file uploaded, check for any error
			if($_FILES['struktur_file']['error'] !== UPLOAD_ERR_NO_FILE){
				$info = getimagesize($_FILES['struktur_file']['tmp_name']);

				if ($_FILES['struktur_file']['error'] !== UPLOAD_ERR_OK) {
					$struktur_file_valid_code = 0;
				}elseif ($info === FALSE) {
					$struktur_file_valid_code = 1;
				}elseif (($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
					$struktur_file_valid_code = 2;
				}

				// If no error, upload file
				if($struktur_file_valid_code == 3){
					$data = Files::upload($id_folder, $_FILES['struktur_file']['name'], 'struktur_file');
					$_POST['id_file_struktur'] = $data['data']['id'];
					$values['id_file_struktur'] = $data['data']['id'];
				}
			}elseif($values['id_file_struktur'] == ''){
				$values['id_file_struktur'] = NULL;
			}

			// keep existing id for post processing
			$id_file_struktur_existing = $this->input->post('id_file_struktur_existing');
			unset($values['id_file_struktur_existing']);

			// assign struktur_file_valid_code value to a post parameter
			$_POST['struktur_file_valid_code'] = $struktur_file_valid_code;

			// -------------------------------------
			// Validation
			// -------------------------------------
			
			// Set validation rules
			$this->form_validation->set_rules('struktur_file_valid_code', lang('nasabah:nasabah:struktur_file_valid_code'), 'callback__valid_upload_file');

			$this->form_validation->set_message('_valid_upload_file', lang('nasabah:validation_message:_valid_upload_file_'.$struktur_file_valid_code));

		}else{
			$_POST['struktur_file_name'] = $_FILES['struktur_file']['name'];
			$this->form_validation->set_rules('struktur_file_name', '<b>Nama File</b>', 'max_length[100]');
		}
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;
		
		if ($this->form_validation->run() === true)
		{
			$result = $this->nasabah_m->update_nasabah($values, $row_id);
		}

		// post processing, clean up replaced or deleted file
		if($return){
			if($id_file_struktur_existing != NULL AND $id_file_struktur_existing != ''
				AND $id_file_struktur_existing != $values['id_file_struktur']){

				Files::delete_file($id_file_struktur_existing);
			}
		}
		
		return $result;
	}

	/**
     * Insert or update Area entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_area($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		$values['id'] = $values['id_nasabah'];
		unset($values['id_nasabah']);

		// -------------------------------------
		// Image Upload Handling
		// -------------------------------------

		// Create folder if doesn't exist
		if(strlen($_FILES['area_file']['name']) <= 100){
			$id_folder = Files::get_id_by_name('Area');
			if($id_folder == NULL){
			    Files::create_folder(0, 'Area', 'local', '', 0);
			    $id_folder = Files::get_id_by_name('Area');
			}
			
			// 0 = General error
			// 1 = Size error
			// 2 = Type error
			// 3 = OK
			$area_file_valid_code = 3;

			// If there is a file uploaded, check for any error
			if($_FILES['area_file']['error'] !== UPLOAD_ERR_NO_FILE){
			    $info = getimagesize($_FILES['area_file']['tmp_name']);

			    if ($_FILES['area_file']['error'] !== UPLOAD_ERR_OK) {
			        $area_file_valid_code = 0;
			    }elseif ($info === FALSE) {
			        $area_file_valid_code = 1;
			    }elseif (($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
			        $area_file_valid_code = 2;
			    }

			    // If no error, upload file
			    if($area_file_valid_code == 3){
			        $data = Files::upload($id_folder, $_FILES['area_file']['name'], 'area_file');
			        $_POST['id_file_area'] = $data['data']['id'];
			        $values['id_file_area'] = $data['data']['id'];
			    }
			}elseif($values['id_file_area'] == ''){
			    $values['id_file_area'] = NULL;
			}

			// keep existing id for post processing
			$id_file_area_existing = $this->input->post('id_file_area_existing');
			unset($values['id_file_area_existing']);

			// assign area_file_valid_code value to a post parameter
			$_POST['area_file_valid_code'] = $area_file_valid_code;

			// -------------------------------------
			// Validation
			// -------------------------------------
			
			// Set validation rules
			$this->form_validation->set_rules('area_file_valid_code', lang('nasabah:nasabah:area_file_valid_code'), 'callback__valid_upload_file');

			$this->form_validation->set_message('_valid_upload_file', lang('nasabah:validation_message:_valid_upload_file_'.$area_file_valid_code));
		}else{
			$_POST['area_file_name'] = $_FILES['area_file']['name'];
			$this->form_validation->set_rules('area_file_name', '<b>Nama File</b>', 'max_length[100]');
		}
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;
		
		if ($this->form_validation->run() === true)
		{
			$result = $this->nasabah_m->update_nasabah($values, $row_id);
		}

		// post processing, clean up replaced or deleted file
		if($return){
		    if($id_file_area_existing != NULL AND $id_file_area_existing != ''
		        AND $id_file_area_existing != $values['id_file_area']){

		        Files::delete_file($id_file_area_existing);
		    }
		}
		
		return $result;
	}

	/**
     * Insert or update Area entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_catatan($method, $id_nasabah = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();
		$values['id'] = $values['id_nasabah'];
		unset($values['id_nasabah']);

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('id_nasabah', lang('nasabah:nasabah:id_nasabah'), 'required');
		$this->form_validation->set_rules('catatan', lang('nasabah:nasabah:catatan'), 'max_length[5000]');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;
		
		if ($this->form_validation->run() === true)
		{
			$result = $this->nasabah_m->update_nasabah($values, $id_nasabah);
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	function _valid_handphone($input){
		if($input == NULL OR $input == ''){
			return TRUE;
		}

		if(preg_match('/^[0-9 ()+-]+$/', $input)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function _valid_mysql_date($input){
		if($input == NULL OR $input == ''){
			return TRUE;
		}

		if(preg_match('/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])$/', $input)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function _valid_upload_file($input){
	    // 0 = General error
	    // 1 = Size error
	    // 2 = Type error
	    // 3 = OK
	    if($input != 3){
	        return FALSE;
	    }else{
	        return TRUE;
	    }
	}

	function is_empty($value)
	{
		if($value == NULL OR $value == '' OR !isset($value)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function _get_crm_or_cra_id_by_name($name)
	{
		$crm_group = $this->group_m->get_group_by_slug('crm');
		$cra_group = $this->group_m->get_group_by_slug('cra');
		$id_crm_group = $crm_group->id;
		$id_cra_group = $cra_group->id;

		$crm_cra_list = $this->user_m->get_many_by(array(
				'group_id' => $id_crm_group.','.$id_cra_group, 
				'exact_name' => $name, 
			)
		);
		
		if(count($crm_cra_list)==0) {
			return NULL;
		}else{
			return $crm_cra_list[0];
		}
	}

	public function csv_example()
	{

    	// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=contoh_data_nasabah.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		// output the column headings
		$headings = array(
			lang('nasabah:nasabah:nama'),
			lang('nasabah:nasabah:deskripsi'),
			lang('nasabah:nasabah:anniversary'),
			lang('nasabah:nasabah:id_group'),
			lang('nasabah:nasabah:id_segmen'),
			lang('nasabah:nasabah:id_industri'),
			lang('nasabah:nasabah:pic_nama'),
			lang('nasabah:nasabah:pic_email'),
			lang('nasabah:nasabah:pic_handphone'),
			lang('nasabah:nasabah:pic_birthdate'),
			lang('nasabah:nasabah:id_crm'),
			lang('nasabah:nasabah:website'),
			lang('nasabah:nasabah:alamat'),
		);
		fputcsv($output, $headings);

		// fetch the data
		$list = array(
			array(
				'CITILINK GROUP',
				'Citilink telah menjadi maskapai yang paling cepat berkembang di Indonesia sejak tahun 2011, ketika mengambil A320 pertama dan percepatan ekspansi sebagai bagian dari upaya oleh grup Garuda untuk bersaing lebih agresif pada segment budget traveler.

				PT Citilink Indonesia ("Citilink" atau "Perusahaan") adalah anak perusahaan Garuda Indonesia, didirikan berdasarkan Akta Notaris Natakusumah No. 01 tanggal 6 Januari 2009, berkedudukan di Sidoarjo, Jawa Timur, dengan pengesahan dari Menkhumham No. AHU-14555.AH.01.01 Tahun 2009 tanggal 22 April 2009. Kepemilikan saham Citilink pada saat didirikan adalah 67% PT Garuda Indonesia (Persero), Tbk. ("Garuda") dan 33% PT Aerowisata ("Aerowisata").',
				'1987-06-25',
				'Garuda Indonesia',
				'BUMN',
				'Transportasi',
				'Adi Santoso',
				'',
				'',
				'',
				'CRM 4',
				'https://www.citilink.co.id/company-profile',
				'Jakarta'
			),
			array(
				'Taxi Coorporate',
				'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
				'1969-02-03',
				'Group Foundation',
				'BUMN',
				'Plastik',
				'Brology Magazine',
				'brologymagz@gmail.com',
				'08923748932',
				'2010-02-10',
				'Tekoari',
				'',
				''
			)
		);

		foreach ($list as $fields) {
			fputcsv($output, $fields);
		}

	}
}