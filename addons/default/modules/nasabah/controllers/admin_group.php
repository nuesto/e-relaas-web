<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Nasabah Module
 *
 * Manajemen nasabah corporate
 *
 */
class Admin_group extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'group';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'access_group_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('nasabah');		
		$this->load->model('nasabah_group_m');
    }

    /**
	 * List all Group
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_group') AND ! group_has_role('nasabah', 'view_own_group')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'c/nasabah/group/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->nasabah_group_m->count_all_group();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){

				$params[substr($key, 2)] = array('type'=>'like', 'value'=>$value);
			}
		}
		
        $data['group']['entries'] = $this->nasabah_group_m->get_group($pagination_config, $params);
		$data['group']['total'] = $pagination_config['total_rows'];
		$data['group']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['group']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('c/nasabah/group/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('c/nasabah/group/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('nasabah:group:plural'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('nasabah:group:plural'))
			->build('admin/group_index', $data);
    }
	
	/**
     * Create a new Group entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'create_group')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_group('new')){	
				$this->session->set_flashdata('success', lang('nasabah:group:submit_success'));				
				redirect('c/nasabah/group/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('nasabah:group:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/nasabah/group/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:group:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_group') OR group_has_role('nasabah', 'view_own_group')){
			$this->template->set_breadcrumb(lang('nasabah:group:plural'), '/c/nasabah/group/index');
		}

		$this->template->set_breadcrumb(lang('nasabah:group:new'))
			->build('admin/group_form', $data);
    }
	
	/**
     * Edit a Group entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Group to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_group') AND ! group_has_role('nasabah', 'edit_own_group')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_group')){
			$entry = $this->nasabah_group_m->get_group_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_group('edit', $id)){	
				$this->session->set_flashdata('success', lang('nasabah:group:submit_success'));
				redirect('c/nasabah/group/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('nasabah:group:submit_failure');
			}
		}
		
		$data['fields'] = $this->nasabah_group_m->get_group_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'c/nasabah/group/index'.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:group:edit'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('nasabah:group:plural'), '/c/nasabah/group/index');
		// if(group_has_role('nasabah', 'view_all_group') OR group_has_role('nasabah', 'view_own_group')){
		// 	$this->template->set_breadcrumb(lang('nasabah:group:plural'), '/c/nasabah/group/index')
		// 	->set_breadcrumb(lang('nasabah:group:view'), '/c/nasabah/group/view/'.$id);
		// }else{
		// 	$this->template->set_breadcrumb(lang('nasabah:group:plural'))
		// 	->set_breadcrumb(lang('nasabah:group:view'));
		// }

		$this->template->set_breadcrumb(lang('nasabah:group:edit'))
			->build('admin/group_form', $data);
    }

    /**
     * Import new Group entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function import()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'import_group')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------

		if($_FILES) {
			$name = $_FILES['file']['name'];
			
			if($name!=null){
				if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
					$values = array();
					
					$skip_header = true;
					$row = 1;
					$error = array();
					$row_skipped = 0;
					$row_inserted = 0;
					while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
						
						if($skip_header) {
							$skip_header = false;
						} else {							
							if($data[0]=='') {
								$error[] = 'Nama Group pada baris-'.$row.' tidak boleh kosong';
							}
							if(strlen($data[0])>'255') {
								$error[] = 'Nama Group pada baris-'.$row.' tidak boleh lebih dari 255 karakter';
							}

							$group = $this->nasabah_group_m->get_group(null, array('nama'=>$data[0]));
							// already exist
							if(count($group)>0) {
								$row_skipped++;
							}else{
								// add to to-be inserted array
								$data_values[] = array(
									'nama' => $data[0],
								);
								$row_inserted++;
							}
						}

						$row++;
					}

					if(count($error)==0) {
						$this->db->trans_start();
						foreach ($data_values as $key => $value) {
							$result = $this->nasabah_group_m->insert_group($value);
						}
						$this->db->trans_complete();
						$result = $this->db->trans_status();
						if($result) {
							$data['messages']['success'] = lang('nasabah:group:submit_success').'<br />'.sprintf(lang('nasabah:group:submit_success_insert_skip_count'), $row_inserted, $row_skipped);
						} else {
							$data['messages']['error'] = lang('nasabah:group:submit_failure').'<br />';
						}
					} else {
						$result = false;
						$data['messages']['error'] = lang('nasabah:group:submit_failure').'<br />';
						$data['messages']['error'] .= implode('<br />', $error);
					}
				}
			}
		}
		
		$data['mode'] = 'import';
		$data['return'] = 'c/nasabah/group/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:group:import'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_group') OR group_has_role('nasabah', 'view_own_group')){
			$this->template->set_breadcrumb(lang('nasabah:group:plural'), '/c/nasabah/group/index');
		}

		$this->template->set_breadcrumb(lang('nasabah:group:import'))
			->build('admin/group_import', $data);
    }
	
	/**
     * Delete a Group entry
     * 
     * @param   int [$id] The id of Group to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'delete_all_group') AND ! group_has_role('nasabah', 'delete_own_group')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		// Check view all/own permission
		if(! group_has_role('nasabah', 'delete_all_group')){
			$entry = $this->nasabah_group_m->get_group_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// Cek entry relationship
		if($this->nasabah_group_m->is_used($id)){
        	$this->session->set_flashdata('error', lang('nasabah:group:is_used'));
		}else{
			// -------------------------------------
			// Delete entry
			// -------------------------------------
			
	        $this->nasabah_group_m->delete_group_by_id($id);
	        $this->session->set_flashdata('error', lang('nasabah:group:deleted'));
		}

 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('c/nasabah/group/index'.$data['uri']);
    }
	
	/**
     * Insert or update Group entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_group($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama', lang('nasabah:group:nama'), 'required|max_length[256]');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->nasabah_group_m->insert_group($values);
				
			}
			else
			{
				$result = $this->nasabah_group_m->update_group($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	public function csv_example()
	{

    	// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=contoh_data_group.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		// output the column headings
		fputcsv($output, array(lang('nasabah:group:nama')));

		// fetch the data
		$list = array(
			array('Garuda Indonesia'),
			array('Group Foundation'),
		);

		foreach ($list as $fields) {
			fputcsv($output, $fields);
		}

	}

}