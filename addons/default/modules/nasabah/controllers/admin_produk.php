<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Nasabah Module
 *
 * Manajemen nasabah corporate
 *
 */
class Admin_produk extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'produk';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'access_nasabah_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('nasabah');		
		$this->load->model('produk_m');
		$this->load->model('nasabah_m');
    }

    /**
	 * List all Produk
     *
     * @return	void
     */
    public function index_html($id_nasabah)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_nasabah') AND ! group_has_role('nasabah', 'view_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'view_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config = NULL;
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(6);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){
				$params[] = array('column'=>substr($key, 2),'function'=>'like','value'=>$value);
			}
		}
		
        // get all product data

		$produk_nasabah = $this->produk_m->get_produk_nasabah($id_nasabah, $params);
		if(count($produk_nasabah) == 0){
			$produk_nasabah = $this->produk_m->get_produk($params);
		}

		$data['all_bisnis_products'] = array();  
		$data['all_konsumer_products'] =  array();
		foreach ($produk_nasabah as $key => $produk) {
			$produk['status'] = (isset($produk['status'])) ? $produk['status'] : 0;
			$produk['keterangan'] = (isset($produk['keterangan'])) ? $produk['keterangan'] : null;
			if($produk['kategori'] == 'bisnis'){
				$data['all_bisnis_products'][] = $produk;
			}
			if($produk['kategori'] == 'konsumer'){
				$data['all_konsumer_products'][] = $produk;
			}
		}
		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------
		
        $this->load->view('admin/produk_index', $data);
    }
	
	/**
     * Display one Produk
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_produk') AND ! group_has_role('nasabah', 'view_own_produk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['produk'] = $this->produk_m->get_produk_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_produk')){
			if($data['produk']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('nasabah:produk:view'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_produk') OR group_has_role('nasabah', 'view_own_produk')){
			$this->template->set_breadcrumb(lang('nasabah:produk:plural'), '/c/nasabah/produk/index');
		}

		$this->template->set_breadcrumb(lang('nasabah:produk:view'))
			->build('admin/produk_entry', $data);
    }
	
	/**
     * Create a new Produk entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'create_produk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_produk('new')){	
				$this->session->set_flashdata('success', lang('nasabah:produk:submit_success'));				
				redirect('c/nasabah/produk/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('nasabah:produk:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/nasabah/produk/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:produk:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_produk') OR group_has_role('nasabah', 'view_own_produk')){
			$this->template->set_breadcrumb(lang('nasabah:produk:plural'), '/c/nasabah/produk/index');
		}

		$this->template->set_breadcrumb(lang('nasabah:produk:new'))
			->build('admin/produk_form', $data);
    }
	
	/**
     * Edit a Produk entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Produk to the be deleted.
     * @return	void
     */
    public function edit($id_nasabah, $is_new=false)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(6);

		$data['id_nasabah'] = $id_nasabah;
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_produk('edit', $id_nasabah)){	
				$this->session->set_flashdata('success', lang('nasabah:produk:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#produk');
			}else{
				$data['messages']['error'] = lang('nasabah:produk:submit_failure');
			}
		}

		// get all product data
		
		$produk_nasabah = $this->produk_m->get_produk_nasabah($id_nasabah);
		if(count($produk_nasabah) == 0){
			$produk_nasabah = $this->produk_m->get_produk();
		}

		$data['all_bisnis_products'] = array();  
		$data['all_konsumen_products'] =  array();
		foreach ($produk_nasabah as $key => $produk) {
			$produk['status'] = (isset($produk['status'])) ? $produk['status'] : 0;
			$produk['keterangan'] = (isset($produk['keterangan'])) ? $produk['keterangan'] : null;
			if($produk['kategori'] == 'bisnis'){
				$data['all_bisnis_products'][] = $produk;
			}
			if($produk['kategori'] == 'konsumer'){
				$data['all_konsumer_products'][] = $produk;
			}
		}
		
		$data['mode'] = 'edit';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#produk';
		$data['entry_id'] = $id_nasabah;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:produk:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'edit_all_nasabah') OR group_has_role('nasabah', 'edit_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id_nasabah);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:produk:edit'))
			->build('admin/produk_form', $data);
    }
	
	/**
     * Delete a Produk entry
     * 
     * @param   int [$id] The id of Produk to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'delete_all_produk') AND ! group_has_role('nasabah', 'delete_own_produk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		// Check view all/own permission
		if(! group_has_role('nasabah', 'delete_all_produk')){
			$entry = $this->produk_m->get_produk_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->produk_m->delete_produk_by_id($id);
        $this->session->set_flashdata('error', lang('nasabah:produk:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('c/nasabah/produk/index'.$data['uri']);
    }
	
	/**
     * Insert or update Produk entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_produk($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('id_nasabah', lang('nasabah:produk:id_nasabah'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			// delete all product data for current id_nasabah
			$this->produk_m->delete_produk_by_id_nasabah($values['id_nasabah']);

			// add each product data
			foreach ($values['id_product'] as $id_product => $id_product_1) {
				$single_value = array();
				$single_value['id_nasabah'] = $values['id_nasabah'];
				$single_value['id_produk'] = $id_product;
				
				if(isset($values['status'][$id_product])){
					$single_value['status'] = 1;
				}else{
					$single_value['status'] = 0;
				}
				
				$keterangan_str = substr($values['keterangan'][$id_product], 0, 512);
				if($keterangan_str === FALSE OR $keterangan_str === 0){
					$single_value['keterangan'] = '';
				}else{
					$single_value['keterangan'] = $keterangan_str;
				}
				$this->produk_m->insert_produk($single_value);
			}

			// if it reaches here, so it must return true
			$result = TRUE;
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}