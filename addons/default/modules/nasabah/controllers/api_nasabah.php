<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Nasabah Module
 *
 * Manajemen nasabah corporate
 *
 */
class Api_nasabah extends API2_Controller
{
    // -------------------------------------
    // This will set the active section tab
    // -------------------------------------
    
    protected $section = 'nasabah';

    public function __construct()
    {
        parent::__construct();

        
        // -------------------------------------
        // Load everything we need
        // -------------------------------------

        $this->lang->load('nasabah');       
        $this->load->model('nasabah_m');
        $this->load->model('produk_m');
        $this->load->model('pks_m');
        $this->load->model('strategi_m');
        $this->load->model('manajemen_m');
        $this->load->model('dpk/dpk_m');

        $this->load->model('nasabah_group_m');
        $this->load->model('segmen_m');
        $this->load->model('industri_m');
        $this->load->model('groups/group_m');
        $this->load->model('users/user_m');

        $this->current_user = $this->authorize_api_access();
    }

    /**
     * List all Nasabah
     *
     * @return  void
     */
    public function get_list_nasabah()
    {

        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND ! group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
        }
        
        $params = array();
        foreach ($_GET as $key => $value){
            if(strpos($key, "f-") === 0 
                AND $value != ''
                AND substr($key, 2) != ''){
                $column = substr($key, 2);
                $function = ($column != 'nama') ? 'where' : 'like';
                $params[] = array('column'=>'nasabah_nasabah.'.$column,'function'=>$function,'value'=>$value);
            }
        }

        // Check permission if user just has view own
        if($this->current_user->group == 'cra' || $this->current_user->group == 'crm'){
            if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
                $params[] = array('column'=>'nasabah_nasabah.id_'.$this->current_user->group,'function'=>'where','value'=>$this->current_user->id);
            }
        }

        $nasabah_entries = $this->nasabah_m->get_nasabah(NULL, $params);
        $sort_nasabah = array();
        foreach ($nasabah_entries as $key => $nasabah) {
            $sort_nasabah[substr($nasabah['nama'],0,1)][] = $nasabah;
        }
        $data['nasabah'] = $sort_nasabah;

        $group_list = $this->nasabah_group_m->get_group();
        $data['nasabah_group_list'] = array();
        foreach ($group_list as $key => $value) {
            $data['nasabah_group_list'][$value['id']] = $value['nama'];
        }

        $segmen_list = $this->segmen_m->get_segmen();
        $data['segmen_list'] = array();
        foreach ($segmen_list as $key => $value) {
            $data['segmen_list'][$value['id']] = $value['nama'];
        }

        $result = $data;

        $status = '200';

        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>'Data nasabah tidak ditemukan');
            $status = '404';
        }

        _output($result,$status);
    }

    public function get_detail_nasabah($id_nasabah=0){


        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND ! group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
        }

        if(!$id_nasabah){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"Tidak ada nasabah yang dipilih")));
        }
        
        // -------------------------------------
        // Get our entry.
        // -------------------------------------
        
        $data['nasabah'] = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
        
        // -------------------------------------
        // Check view all/own permission
        // -------------------------------------
        
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user)){
            if($data['nasabah']['id_'.$this->current_user->group] != $this->current_user->id){
                header('Content-Type: application/json');
                header("HTTP Error 401 Unauthorized");
                die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
            }
        }

        if(count($data['nasabah']) > 0){
            if($data['nasabah']['id_logo'] != ""){
                $path = 'http:'.base_url().'files/thumb/'.$data['nasabah']['id_logo'].'/800/800';
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $file = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($file);
                $data['nasabah']['logo'] = $base64;
            }
            if($data['nasabah']['id_file_struktur'] != ""){
                $path = 'http:'.base_url().'files/thumb/'.$data['nasabah']['id_file_struktur'].'/800/800';
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $file = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($file);
                $data['nasabah']['struktur'] = $base64;
            }
            if($data['nasabah']['id_file_area'] != ""){
                $path = 'http:'.base_url().'files/thumb/'.$data['nasabah']['id_file_area'].'/800/800';
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $file = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($file);
                $data['nasabah']['area'] = $base64;
            }
        }

        $status = '200';
        $result = $data;
        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>'Data detail nasabah tidak ditemukan');
            $status = '404';
        }

        _output($result,$status);
    }
    

    public function get_product_holding_nasabah($id_nasabah=0)
    {

        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND ! group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){

            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
        }

        if(!$id_nasabah){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"Tidak ada nasabah yang dipilih")));
        }
        
        // Check view all/own permission
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user)){
            $entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
            $id_petugas = $entry['id_'.$this->current_user->group];
            if($id_petugas != $this->current_user->id){
                header('Content-Type: application/json');
                header("HTTP Error 401 Unauthorized");
                die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
            }
        }
        

        // -------------------------------------
        // Get entries
        // -------------------------------------

        // handle filter get params
        $params = array();
        foreach ($_GET as $key => $value){
            if(strpos($key, "f-") === 0 
                AND $value != ''
                AND substr($key, 2) != ''){
                $params[] = array('column'=>substr($key, 2),'function'=>'like','value'=>$value);
            }
        }
        
        // get all product data

        $produk_nasabah = $this->produk_m->get_produk_nasabah($id_nasabah, $params);
        if(count($produk_nasabah) == 0){
            $produk_nasabah = $this->produk_m->get_produk($params);
        }

        $data['all_bisnis_products'] = array();  
        $data['all_konsumen_products'] =  array();
        foreach ($produk_nasabah as $key => $produk) {
            $produk['status'] = (isset($produk['status'])) ? $produk['status'] : 0;
            $produk['keterangan'] = (isset($produk['keterangan'])) ? $produk['keterangan'] : null;

            $product_full_name = array();
            $product_full_name[] = $produk['family_nama'];
            $product_full_name[] = $produk['item_nama'];
            $product_full_name[] = '<br /><strong>'.$produk['produk_nama'].'</strong>';
            $produk['product_full_name'] = implode(' &raquo; ', $product_full_name); 

            if($produk['kategori'] == 'bisnis'){
                $data['all_bisnis_products'][] = $produk;
            }
            if($produk['kategori'] == 'konsumer'){
                $data['all_konsumer_products'][] = $produk;
            }
        }



        $status = '200';
        $result = $data;
        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>'Data product holding nasabah tidak ditemukan');
            $status = '404';
        }

        _output($result,$status);
    }


    public function get_pks_nasabah($id_nasabah=0)
    {
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND ! group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
        }

        if(!$id_nasabah){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"Tidak ada nasabah yang dipilih")));
        }
        
        // Check view all/own permission
        $entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user)){
            $id_petugas = $entry['id_'.$this->current_user->group];
            if($id_petugas != $this->current_user->id){
                header('Content-Type: application/json');
                header("HTTP Error 401 Unauthorized");
                die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
            }
        }
        
        // -------------------------------------
        // Pagination
        // -------------------------------------
        
        $pagination_config = NULL;
        
        // -------------------------------------
        // Get entries
        // -------------------------------------

        // handle filter get params
        $params = array();
        $params[] = array('column'=>'id_nasabah','function'=>'where','value'=>$id_nasabah);
        foreach ($_GET as $key => $value){
            if(strpos($key, "f-") === 0 
                AND $value != ''
                AND substr($key, 2) != ''){
                $params[] = array('column'=>substr($key, 2),'function'=>'like','value'=>$value);
            }
        }
        
        $data['pks']['entries'] = $this->pks_m->get_pks($pagination_config, $params);

        foreach ($data['pks']['entries'] as $key => &$pks) {
            $pks['txt_jangka_waktu'] = $this->to_tahun_bulan($pks['jangka_waktu']);
            $pks['tanggal_ttd'] = date('d-M-Y', strtotime($pks['tanggal_ttd'])); 
            $pks['txt_berlaku_hingga'] = date('d-M-Y', strtotime('+'.$pks['jangka_waktu']. ' months', strtotime($pks['tanggal_ttd']))); 

        }

        $data['pks']['total'] = count($data['pks']['entries']);

        $status = '200';
        $result = $data;
        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>'Data pks nasabah tidak ditemukan');
            $status = '404';
        }

        _output($result,$status);
    }

    private function to_tahun_bulan($jangka_waktu){
        $hasil_waktu = '';
        $sisa = $jangka_waktu;
        if($jangka_waktu > 12){
            $hasil_waktu = floor($jangka_waktu/12)." Tahun ";
            $sisa = $jangka_waktu%12;
        }
        $hasil_waktu .= $sisa." Bulan";

        return $hasil_waktu;
    }

    public function get_strategi_nasabah($id_nasabah=0){
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND ! group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
        }

        if(!$id_nasabah){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"Tidak ada nasabah yang dipilih")));
        }
        
        // Check view all/own permission
        $entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user)){
            $id_petugas = $entry['id_'.$this->current_user->group];
            if($id_petugas != $this->current_user->id){
                header('Content-Type: application/json');
                header("HTTP Error 401 Unauthorized");
                die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
            }
        }
        
        // -------------------------------------
        // Pagination
        // -------------------------------------
        
        $pagination_config = NULL;
        $data['id_nasabah'] = $id_nasabah;
        
        // -------------------------------------
        // Get entries
        // -------------------------------------

        // handle filter get params
        $params = array();
        foreach ($_GET as $key => $value){
            if(strpos($key, "f-") === 0 
                AND $value != ''
                AND substr($key, 2) != ''){

                $params[substr($key, 2)] = $value;
            }
        }
        
        $params['id_nasabah'] = $id_nasabah;

        $data['strategi']['entries'] = $this->strategi_m->get_strategi($pagination_config, $params);
        
        foreach ($data['strategi']['entries'] as &$value) {
            $konten = json_decode($value['konten'], TRUE);
            if($konten != NULL AND is_array($konten)){
                if(isset($konten['proyek'])){
                    $konten_proyek = $konten['proyek'];
                }else{
                    $konten_proyek = '';
                }
                $value['konten_proyek'] = $konten_proyek;

                if(isset($konten['deskripsi'])){
                    $konten_deskripsi = $konten['deskripsi'];
                }else{
                    $konten_deskripsi = '';
                }
                $value['konten_deskripsi'] = $konten_deskripsi;

                if(isset($konten['financial_impact'])){
                    $konten_financial_impact = $konten['financial_impact'];
                }else{
                    $konten_financial_impact = '';
                }
                $value['konten_financial_impact'] = $konten_financial_impact;

                if(isset($konten['target'])){
                    $konten_target = $konten['target'];
                }else{
                    $konten_target = '';
                }
                $value['konten_target'] = $konten_target;
            }

            $change_history = json_decode($value['change_history'], TRUE);
            $change_history = end($change_history);
            $value['change_history'] = $change_history;

            $txt_change_history = '';
            if(isset($change_history['on']) AND isset($change_history['by'])){
                $txt_change_history = $change_history['on'].' by '.$change_history['by'];
            }
            $value['txt_change_history'] = $txt_change_history;
        }
        $data['strategi']['total'] = count($data['strategi']['entries']);
        $status = '200';
        $result = $data;
        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>'Data strategi nasabah tidak ditemukan');
            $status = '404';
        }

        _output($result,$status);


    }


    public function get_dpk_nasabah($id_nasabah=0){

        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND ! group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
        }

        if(!$id_nasabah){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"Tidak ada nasabah yang dipilih")));
        }
        
        // Check view all/own permission
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user)){
            $entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
            $id_petugas = $entry['id_'.$this->current_user->group];
            if($id_petugas != $this->current_user->id){
                header('Content-Type: application/json');
                header("HTTP Error 401 Unauthorized");
                die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
            }
        }
        $dpk_table = array();
        $dpk_table['GIRO'] = array();
        $dpk_table['DEPOSITO'] = array();
        $dpk_table['TABUNGAN'] = array();
        $ccy_existing = array('IDR','VA');
        $data_dpk = $this->dpk_m->get_dpk_by_nasabah($id_nasabah);
        $total_ccy = array();
        $total_ccy_format = array();
        $total_dpk = 0;
        foreach ($data_dpk as $key => $dpk) {
            $dpk['ccy'] = ($dpk['ccy'] != 'IDR') ? 'VA' : 'IDR';
            $dpk_table[$dpk['produk']]['ccy'][$dpk['ccy']] = (float) $dpk['total_ccy'];
            $dpk_table[$dpk['produk']]['ccy_format'][$dpk['ccy']] = number_format($dpk['total_ccy'],2);
            $dpk_table[$dpk['produk']]['total_ccy'] = array_sum($dpk_table[$dpk['produk']]['ccy']);
            $dpk_table[$dpk['produk']]['total_ccy_format'] = number_format($dpk_table[$dpk['produk']]['total_ccy'],2);

            if(!isset($total_ccy[$dpk['ccy']])){
                $total_ccy[$dpk['ccy']] = 0;
            }
            $total_ccy[$dpk['ccy']] += (float) $dpk['total_ccy'];
            $total_ccy_format[$dpk['ccy']] = number_format($total_ccy[$dpk['ccy']],2);
            
        }
        $total_per_produk = array();
        $total_dpk = array_sum($total_ccy);

        $data['dpk_table'] = $dpk_table;
        $data['ccy_existing'] =$ccy_existing;
        $data['data_dpk'] = $data_dpk;
        $data['total_ccy'] = $total_ccy;
        $data['total_ccy_format'] = $total_ccy_format;
        $data['total_dpk'] = $total_dpk;

        $data['total_dpk_format'] = number_format($total_dpk,2);

        // DPK HISTORY
        $dpk_history = $this->dpk_m->get_history_dpk_by_nasabah($id_nasabah);
        $history = array();
        $obj_dpk = array();
        $tanggal_data = array();
        $total_ccy_history = array();
        $data['last_updated'] = format_date_id(date('Y-m-d'), 'd M  Y', '');
        $data['obj_tgl'] = array();
        $data['obj_dpk'] = array();
        $data['text_range_tanggal'] = $data['last_updated'];
        foreach ($dpk_history as $key => $dpk) {
            $dpk['ccy'] = ($dpk['ccy'] != 'IDR') ? 'VA' : 'IDR';
            if(!isset($total_ccy_history[$dpk['ccy']])){
                $total_ccy_history[$dpk['ccy']] = 0;
            }

            if(!array_search($dpk['tanggal_data'], $tanggal_data)){

                $tanggal[] = format_date($dpk['tanggal_data'], 'd/m/Y');
                $tanggal_data[] = $dpk['tanggal_data'];
            }
            $total_ccy_history[$dpk['ccy']] += (float) $dpk['total_ccy'];

            $history[$dpk['tanggal_data']] = array_sum($total_ccy_history);
            
        }
        if(count($tanggal_data) > 0){
            $data['last_updated'] = format_date_id($tanggal_data[count($tanggal_data)-1], 'd M  Y', '');
            $data['text_range_tanggal'] = ($data['last_updated'] != format_date_id($tanggal_data[0], 'd M  Y', '')) ? format_date_id($tanggal_data[0], 'd M  Y', '') . ' sampai '.$data['last_updated'] : $data['last_updated'];
            foreach ($history as $key => $dpk_value) {
                $obj_dpk[] = $dpk_value;
            }
            $data['obj_tgl'] = $tanggal;
            $data['obj_dpk'] = $obj_dpk;
        }

        // tabel deposito
        $params[] = array('column'=>'id_nasabah','function'=>'where','value'=>$id_nasabah);
        $params[] = array('column'=>'produk','function'=>'where','value'=>'DEPOSITO');
        $data['deposito_entries'] = $this->dpk_m->get_dpk(NULL, $params);

        $status = '200';
        $result = $data;
        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>'Data dpk nasabah tidak ditemukan');
            $status = '404';
        }

        _output($result,$status);
    }

    public function get_manajemen_nasabah($id_nasabah=0){
        // -------------------------------------
        // Check permission
        // -------------------------------------
        
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND ! group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
        }

        if(!$id_nasabah){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"Tidak ada nasabah yang dipilih")));
        }
        
        // Check view all/own permission
        $entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user)){
            $id_petugas = $entry['id_'.$this->current_user->group];
            if($id_petugas != $this->current_user->id){
                header('Content-Type: application/json');
                header("HTTP Error 401 Unauthorized");
                die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
            }
        }
        

        
        // -------------------------------------
        // Get entries
        // -------------------------------------

        // handle filter get params
        $params = array();
        $params[] = array('column'=>'id_nasabah','function'=>'where','value'=>$id_nasabah);
        foreach ($_GET as $key => $value){
            if(strpos($key, "f-") === 0 
                AND $value != ''
                AND substr($key, 2) != ''){
                $params[] = array('column'=>substr($key, 2),'function'=>'like','value'=>$value);
            }
        }
        
        // get komisaris
        $params_komisaris = $params;
        $params_komisaris[] = array('column'=>'kategori','function'=>'where','value'=>'Dewan Komisaris');
        $manajemen_komisaris = $this->manajemen_m->get_manajemen(NULL, $params_komisaris);
        
        $arr_komisaris = array();
        foreach ($manajemen_komisaris as $key => $komisaris) {
            $arr_komisaris[$key] = $komisaris;
            if($komisaris['id_file_foto'] != ""){
                $path = 'http:'.base_url().'files/thumb/'.$komisaris['id_file_foto'].'/79/79';
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $file = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($file);
                $arr_komisaris[$key]['file_foto'] = $base64;
            }
        }

        $data['manajemen']['entries_komisaris'] = $arr_komisaris;

        // get direksi
        $params_direksi = $params;
        $params_direksi[] = array('column'=>'kategori','function'=>'where','value'=>'Dewan Direksi');
        $manajemen_direksi = $this->manajemen_m->get_manajemen(NULL, $params_direksi);

        $arr_direksi = array();
        foreach ($manajemen_direksi as $key => $direksi) {
            $arr_direksi[$key] = $direksi;
            if($direksi['id_file_foto'] != ""){
                $path = 'http:'.base_url().'files/thumb/'.$direksi['id_file_foto'].'/79/79';
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $file = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($file);
                $arr_direksi[$key]['file_foto'] = $base64;
            }
        }

        $data['manajemen']['entries_direksi'] = $arr_direksi;

        $status = '200';
        $result = $data;
        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>'Data manajemen nasabah tidak ditemukan');
            $status = '404';
        }

        _output($result,$status);
    }

    public function set_starred_item($id_nasabah = 0, $is_starred_item = 0)
    {
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND ! group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
        }

        if(!$id_nasabah){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"Tidak ada nasabah yang dipilih")));
        }
        
        // -------------------------------------
        // Get our entry.
        // -------------------------------------
        
        $nasabah = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
        
        // -------------------------------------
        // Check view all/own permission
        // -------------------------------------
        
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user)){
            if($nasabah['id_'.$this->current_user->group] != $this->current_user->id){
                header('Content-Type: application/json');
                header("HTTP Error 401 Unauthorized");
                die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
            }
        }

        $result = array('status'=>'error', 'message'=>'Data nasabah gagal diproses');
        $status = '404';

        $alert_status = ($is_starred_item==0) ? 'dihapus' : 'ditambahkan';
        
        $values['is_starred_item'] = $is_starred_item;
        $result = $this->nasabah_m->update_nasabah($values, $id_nasabah);
        
        if($result) {
            $status = '200';
            $result = array('status'=>'success', 'message'=>'Nasabah telah '.$alert_status.' ke starred items');
        }

        _output($result,$status);
    
    }

    public function get_starred_items(){
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND ! group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
        }
        
        $params = array();
        foreach ($_GET as $key => $value){
            if(strpos($key, "f-") === 0 
                AND $value != ''
                AND substr($key, 2) != ''){
                $column = substr($key, 2);
                $function = ($column != 'nama') ? 'where' : 'like';
                $params[] = array('column'=>'nasabah_nasabah.'.$column,'function'=>$function,'value'=>$value);
            }
        }

        // Check permission if user just has view own
        if($this->current_user->group == 'cra' || $this->current_user->group == 'crm'){
            if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
                $params[] = array('column'=>'nasabah_nasabah.id_'.$this->current_user->group,'function'=>'where','value'=>$this->current_user->id);
            }
        }
        $params[] = array('column'=>'nasabah_nasabah.is_starred_item','function'=>'where','value'=>1);

        $nasabah_entries = $this->nasabah_m->get_nasabah(NULL, $params);

        $sort_nasabah = array();
        foreach ($nasabah_entries as $key => $nasabah) {
            $sort_nasabah[substr($nasabah['nama'],0,1)][] = $nasabah;
        }
        $data['nasabah'] = $sort_nasabah;

        $group_list = $this->nasabah_group_m->get_group();
        $data['nasabah_group_list'] = array();
        foreach ($group_list as $key => $value) {
            $data['nasabah_group_list'][$value['id']] = $value['nama'];
        }

        $segmen_list = $this->segmen_m->get_segmen();
        $data['segmen_list'] = array();
        foreach ($segmen_list as $key => $value) {
            $data['segmen_list'][$value['id']] = $value['nama'];
        }
        
        $result = $data;

        $status = '200';

        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>'Data nasabah tidak ditemukan');
            $status = '404';
        }

        _output($result,$status);
    }

    public function get_customers_birthdate(){
        if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND ! group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
            header('Content-Type: application/json');
            header("HTTP Error 401 Unauthorized");
            die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
        }

        $Current = Date('N');
        $DaysToSunday = 7 - $Current;
        $DaysFromMonday = $Current - 1;
        $Monday = Date('Y-m-d', StrToTime("- {$DaysFromMonday} Days"));
        // $Sunday = Date('Y-m-d', StrToTime("+ {$DaysToSunday} Days"));
        $arr_date = array();
        for($i=0; $i<7; $i++){
            $arr_date[] = date('m-d', strtotime("+".$i." day", strtotime($Monday)));
        }
        
        $params = array();
        $params[] = array('column'=>"DATE_FORMAT(default_nasabah_nasabah.anniversary,'%m-%d')",'function'=>'where_in','value'=>$arr_date);

        // Check permission if user just has view own
        if($this->current_user->group == 'cra' || $this->current_user->group == 'crm'){
            if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
                $params[] = array('column'=>'nasabah_nasabah.id_'.$this->current_user->group,'function'=>'where','value'=>$this->current_user->id);
            }
        }

        $nasabah_entries = $this->nasabah_m->get_nasabah_birthdate(NULL, $params);
        $nasabah_birth_date = array();
        foreach ($nasabah_entries as $key => &$nasabah) {
            $nasabah['anniversary_format'] =  date('d M Y', strtotime($nasabah['anniversary']));
            $nasabah['age'] = date_diff(date_create($nasabah['anniversary']), date_create('today'))->y;
            $nasabah_current_anniv = explode('-',$nasabah['anniversary']);
            unset($nasabah_current_anniv[0]);
            $nasabah_current_anniv = date('Y').'-'.implode('-',$nasabah_current_anniv);
            $return_day = date('D, d M Y', strtotime($nasabah_current_anniv));
            $nasabah_birth_date[$return_day][] = $nasabah;
        }
        $data['nasabah_entries'] = $nasabah_birth_date;

        $params = array();
        $params[] = array('column'=>"DATE_FORMAT(default_nasabah_manajemen.birthdate,'%m-%d')",'function'=>'where_in','value'=>$arr_date);

        // Check permission if user just has view own
        if($this->current_user->group == 'cra' || $this->current_user->group == 'crm'){
            if(! group_has_role('nasabah', 'view_all_nasabah', $this->current_user) AND group_has_role('nasabah', 'view_own_nasabah', $this->current_user)){
                $params[] = array('column'=>'nasabah_nasabah.id_'.$this->current_user->group,'function'=>'where','value'=>$this->current_user->id);
            }
        }
        $manajemen_entries = $this->manajemen_m->get_manajemen(NULL, $params);
        $manajemen_birth_date = array();
        foreach ($manajemen_entries as $key => &$manajemen) {
            $manajemen['anniversary_format'] =  date('d M Y', strtotime($manajemen['birthdate']));
            $manajemen['age'] = date_diff(date_create($manajemen['birthdate']), date_create('today'))->y;
            $manajemen_current_anniv = explode('-',$manajemen['birthdate']);
            unset($manajemen_current_anniv[0]);
            $manajemen_current_anniv = date('Y').'-'.implode('-',$manajemen_current_anniv);
            $return_day = date('D, d M Y', strtotime($manajemen_current_anniv));
            $manajemen_birth_date[$return_day][] = $manajemen;
        }
        $data['manajemen_entries'] = $manajemen_birth_date;

        $data['total_anniversary'] = count($data['nasabah_entries'])+count($data['manajemen_entries']);
        $result = $data;

        $status = '200';

        if(empty($result)) {
            $result = array('status'=>'error', 'message'=>lang('global:error_action'));
            $status = '404';
        }

        _output($result,$status);
    }
}