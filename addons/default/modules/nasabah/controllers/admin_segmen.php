<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Nasabah Module
 *
 * Manajemen nasabah corporate
 *
 */
class Admin_segmen extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'segmen';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'access_segmen_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('nasabah');		
		$this->load->model('segmen_m');
    }

    /**
	 * List all Segmen
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_segmen') AND ! group_has_role('nasabah', 'view_own_segmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'c/nasabah/segmen/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->segmen_m->count_all_segmen();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){

				$params[substr($key, 2)] = array('type'=>'like', 'value'=>$value);
			}
		}
		
        $data['segmen']['entries'] = $this->segmen_m->get_segmen($pagination_config, $params);
		$data['segmen']['total'] = $pagination_config['total_rows'];
		$data['segmen']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['segmen']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('c/nasabah/segmen/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('c/nasabah/segmen/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('nasabah:segmen:plural'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('nasabah:segmen:plural'))
			->build('admin/segmen_index', $data);
    }
	
	/**
     * Create a new Segmen entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'create_segmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_segmen('new')){	
				$this->session->set_flashdata('success', lang('nasabah:segmen:submit_success'));				
				redirect('c/nasabah/segmen/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('nasabah:segmen:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/nasabah/segmen/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:segmen:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_segmen') OR group_has_role('nasabah', 'view_own_segmen')){
			$this->template->set_breadcrumb(lang('nasabah:segmen:plural'), '/c/nasabah/segmen/index');
		}

		$this->template->set_breadcrumb(lang('nasabah:segmen:new'))
			->build('admin/segmen_form', $data);
    }
	
	/**
     * Edit a Segmen entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Segmen to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_segmen') AND ! group_has_role('nasabah', 'edit_own_segmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_segmen')){
			$entry = $this->segmen_m->get_segmen_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_segmen('edit', $id)){	
				$this->session->set_flashdata('success', lang('nasabah:segmen:submit_success'));				
				redirect('c/nasabah/segmen/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('nasabah:segmen:submit_failure');
			}
		}
		
		$data['fields'] = $this->segmen_m->get_segmen_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'c/nasabah/segmen/index'.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:segmen:edit'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('nasabah:segmen:plural'), '/c/nasabah/segmen/index');
		
		// if(group_has_role('nasabah', 'view_all_segmen') OR group_has_role('nasabah', 'view_own_segmen')){
		// 	$this->template->set_breadcrumb(lang('nasabah:segmen:plural'), '/c/nasabah/segmen/index')
		// 	->set_breadcrumb(lang('nasabah:segmen:view'), '/c/nasabah/segmen/view/'.$id);
		// }else{
		// 	$this->template->set_breadcrumb(lang('nasabah:segmen:plural'))
		// 	->set_breadcrumb(lang('nasabah:segmen:view'));
		// }

		$this->template->set_breadcrumb(lang('nasabah:segmen:edit'))
			->build('admin/segmen_form', $data);
    }
	
	/**
     * Delete a Segmen entry
     * 
     * @param   int [$id] The id of Segmen to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'delete_all_segmen') AND ! group_has_role('nasabah', 'delete_own_segmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		// Check view all/own permission
		if(! group_has_role('nasabah', 'delete_all_segmen')){
			$entry = $this->segmen_m->get_segmen_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->segmen_m->delete_segmen_by_id($id);
        $this->session->set_flashdata('error', lang('nasabah:segmen:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('c/nasabah/segmen/index'.$data['uri']);
    }
	
	/**
     * Insert or update Segmen entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_segmen($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama', lang('nasabah:segmen:nama'), 'required|max_length[256]');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->segmen_m->insert_segmen($values);
				
			}
			else
			{
				$result = $this->segmen_m->update_segmen($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}