<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Nasabah Module
 *
 * Manajemen nasabah corporate
 *
 */
class Admin_pks extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'pks';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'access_nasabah_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('nasabah');		
		$this->load->model('pks_m');
		$this->load->model('nasabah_m');
    }

    /**
	 * List all PKS
     *
     * @return	void
     */
    public function index_html($id_nasabah)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_nasabah') AND ! group_has_role('nasabah', 'view_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
		$data['fields_nasabah'] = $entry;
		if(! group_has_role('nasabah', 'view_all_nasabah')){
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config = NULL;
		$data['uri'] = get_query_string(6);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		$params[] = array('column'=>'id_nasabah','function'=>'where','value'=>$id_nasabah);
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){
				$params[] = array('column'=>substr($key, 2),'function'=>'like','value'=>$value);
			}
		}
		
        $data['pks']['entries'] = $this->pks_m->get_pks($pagination_config, $params);
		$data['pks']['total'] = count($data['pks']['entries']);

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------

		echo $this->load->view('admin/pks_index', $data);
    }
	
	/**
     * Display one PKS
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_pks') AND ! group_has_role('nasabah', 'view_own_pks')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['pks'] = $this->pks_m->get_pks_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_pks')){
			if($data['pks']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('nasabah:pks:view'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_pks') OR group_has_role('nasabah', 'view_own_pks')){
			$this->template->set_breadcrumb(lang('nasabah:pks:plural'), '/c/nasabah/pks/index');
		}

		$this->template->set_breadcrumb(lang('nasabah:pks:view'))
			->build('admin/pks_entry', $data);
    }
	
	/**
     * Create a new PKS entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($id_nasabah)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}

		$data['uri'] = get_query_string(6);

		$data['id_nasabah'] = $id_nasabah;
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_pks('new')){	
				$this->session->set_flashdata('success', lang('nasabah:pks:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#pks');
			}else{
				$data['messages']['error'] = lang('nasabah:pks:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#pks';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:pks:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id_nasabah);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:pks:new'))
			->append_css('module::nasabah.css')
			->append_js('module::nasabah.js')
			->build('admin/pks_form', $data);
    }
	
	/**
     * Edit a PKS entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the PKS to the be deleted.
     * @return	void
     */
    public function edit($id_nasabah, $id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(7);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_pks('edit', $id)){	
				$this->session->set_flashdata('success', lang('nasabah:pks:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.'/'.$data['uri'].'#pks');
			}else{
				$data['messages']['error'] = lang('nasabah:pks:submit_failure');
			}
		}
		
		$data['fields'] = $this->pks_m->get_pks_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.'/'.$data['uri'].'#pks';
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:pks:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id_nasabah);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:pks:edit'))
			->build('admin/pks_form', $data);
    }
	
	/**
     * Delete a PKS entry
     * 
     * @param   int [$id] The id of PKS to be deleted
     * @return  void
     */
    public function delete($id_nasabah, $id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->pks_m->delete_pks_by_id($id);
        $this->session->set_flashdata('error', lang('nasabah:pks:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(7);
        redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#pks');
    }
	
	/**
     * Insert or update PKS entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_pks($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('id_nasabah', lang('nasabah:pks:id_nasabah'), 'required');
		$this->form_validation->set_rules('deskripsi', lang('nasabah:pks:deskripsi'), 'required|max_length[256]');
		$this->form_validation->set_rules('tipe', lang('nasabah:pks:tipe'), 'required|max_length[45]');
		$this->form_validation->set_rules('nomor', lang('nasabah:pks:nomor'), 'required|max_length[128]');
		$this->form_validation->set_rules('tanggal_ttd', lang('nasabah:pks:tanggal_ttd'), 'required|callback__valid_mysql_date');
		$this->form_validation->set_rules('jangka_waktu', lang('nasabah:pks:jangka_waktu'), 'required|integer|greater_than[0]|max_length[6]');
		$this->form_validation->set_rules('keterangan', lang('nasabah:pks:keterangan'), 'max_length[512]');

		$this->form_validation->set_message('_valid_mysql_date', lang('nasabah:pks:tanggal_ttd:validation_message:_valid_mysql_date'));
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->pks_m->insert_pks($values);	
			}
			else
			{
				$result = $this->pks_m->update_pks($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	function _valid_mysql_date($input){
		if($input == NULL OR $input == ''){
			return TRUE;
		}

		if(preg_match('/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])$/', $input)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

}