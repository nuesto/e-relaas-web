<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Nasabah Module
 *
 * Manajemen nasabah corporate
 *
 */
class Admin_manajemen extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'manajemen';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'access_nasabah_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('nasabah');		
		$this->load->model('manajemen_m');
		$this->load->model('nasabah_m');

		$this->load->library('files/files');
    }

    /**
	 * List all Manajemen
     *
     * @return	void
     */
    public function index_html($id_nasabah)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_nasabah') AND ! group_has_role('nasabah', 'view_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
		$data['fields_nasabah'] = $entry;
		if(! group_has_role('nasabah', 'view_all_nasabah')){
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$data['uri'] = get_query_string(6);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		$params[] = array('column'=>'id_nasabah','function'=>'where','value'=>$id_nasabah);
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){
				$params[] = array('column'=>substr($key, 2),'function'=>'like','value'=>$value);
			}
		}
		
		// get komisaris
		$params_komisaris = $params;
		$params_komisaris[] = array('column'=>'kategori','function'=>'where','value'=>'Dewan Komisaris');
        $data['manajemen']['entries_komisaris'] = $this->manajemen_m->get_manajemen(NULL, $params_komisaris);

        // get direksi
		$params_direksi = $params;
		$params_direksi[] = array('column'=>'kategori','function'=>'where','value'=>'Dewan Direksi');
        $data['manajemen']['entries_direksi'] = $this->manajemen_m->get_manajemen(NULL, $params_direksi);

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------
		
        echo $this->load->view('admin/manajemen_index', $data, TRUE);
    }
	
	/**
     * Display one Manajemen
     *
     * @return  void
     */
    public function view_html($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_manajemen') AND ! group_has_role('nasabah', 'view_own_manajemen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['manajemen'] = $this->manajemen_m->get_manajemen_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_manajemen')){
			if($data['manajemen']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        echo $this->load->view('admin/manajemen_entry', $data, TRUE);
    }
	
	/**
     * Create a new Manajemen entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($id_nasabah)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}

		$data['uri'] = get_query_string(6);

		$data['id_nasabah'] = $id_nasabah;
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_manajemen('new')){	
				$this->session->set_flashdata('success', lang('nasabah:manajemen:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#manajemen');
			}else{
				$data['messages']['error'] = lang('nasabah:manajemen:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#manajemen';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:manajemen:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'edit_all_nasabah') OR group_has_role('nasabah', 'edit_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id_nasabah);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:manajemen:new'))
			->build('admin/manajemen_form', $data);
    }
	
	/**
     * Edit a Manajemen entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Manajemen to the be deleted.
     * @return	void
     */
    public function edit($id_nasabah, $id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(7);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_manajemen('edit', $id)){	
				$this->session->set_flashdata('success', lang('nasabah:manajemen:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#manajemen');
			}else{
				$data['messages']['error'] = lang('nasabah:manajemen:submit_failure');
			}
		}
		
		$data['fields'] = $this->manajemen_m->get_manajemen_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#manajemen';
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:manajemen:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_manajemen') OR group_has_role('nasabah', 'view_own_manajemen')){
			$this->template->set_breadcrumb(lang('nasabah:manajemen:plural'), '/c/nasabah/manajemen/index')
			->set_breadcrumb(lang('nasabah:manajemen:view'), '/c/nasabah/manajemen/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:manajemen:plural'))
			->set_breadcrumb(lang('nasabah:manajemen:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:manajemen:edit'))
			->build('admin/manajemen_form', $data);
    }
	
	/**
     * Delete a Manajemen entry
     * 
     * @param   int [$id] The id of Manajemen to be deleted
     * @return  void
     */
    public function delete($id_nasabah, $id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->manajemen_m->delete_manajemen_by_id($id);
        $this->session->set_flashdata('error', lang('nasabah:manajemen:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(7);
        redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#manajemen');
    }
	
	/**
     * Insert or update Manajemen entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_manajemen($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		if($values['birthdate'] == ''){
			$values['birthdate'] = NULL;
		}

		// -------------------------------------
		// Image Upload Handling
		// -------------------------------------

		// Create folder if doesn't exist
		$id_folder = Files::get_id_by_name('Manajemen');
		if($id_folder == NULL){
			Files::create_folder(0, 'Manajemen', 'local', '', 0);
			$id_folder = Files::get_id_by_name('Manajemen');
		}
		
		// 0 = General error
		// 1 = Size error
		// 2 = Type error
		// 3 = OK
		$foto_file_valid_code = 3;
		
		// If there is a file uploaded, check for any error
		if($_FILES['foto_file']['error'] !== UPLOAD_ERR_NO_FILE){
			$info = getimagesize($_FILES['foto_file']['tmp_name']);

			if ($_FILES['foto_file']['error'] !== UPLOAD_ERR_OK) {
				$foto_file_valid_code = 0;
			}elseif ($info === FALSE) {
				$foto_file_valid_code = 1;
			}elseif (($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
				$foto_file_valid_code = 2;
			}

			// If no error, upload file
			if($foto_file_valid_code == 3){
				$data = Files::upload($id_folder, $_FILES['foto_file']['name'], 'foto_file');
				$_POST['id_file_foto'] = $data['data']['id'];
				$values['id_file_foto'] = $data['data']['id'];
			}
		}elseif($values['id_file_foto'] == ''){
			$values['id_file_foto'] = NULL;
		}

		// keep existing id for post processing
		$id_file_foto_existing = $this->input->post('id_file_foto_existing');
		unset($values['id_file_foto_existing']);

		// assign foto_file_valid_code value to a post parameter
		$_POST['foto_file_valid_code'] = $foto_file_valid_code;

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('kategori', lang('nasabah:manajemen:kategori'), 'required');
		$this->form_validation->set_rules('nama', lang('nasabah:manajemen:nama'), 'required|max_length[256]');
		$this->form_validation->set_rules('jabatan', lang('nasabah:manajemen:jabatan'), 'required|max_length[256]');
		$this->form_validation->set_rules('foto_file_valid_code', lang('nasabah:manajemen:foto_file_valid_code'), 'callback__valid_id_file_foto');
		$this->form_validation->set_rules('birthdate', lang('nasabah:manajemen:birthdate'), 'callback__valid_mysql_date');

		$this->form_validation->set_message('_valid_id_file_foto', lang('nasabah:validation_message:_valid_id_file_foto_'.$foto_file_valid_code));
		$this->form_validation->set_message('_valid_mysql_date', lang('nasabah:validation_message:_valid_mysql_date'));
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->manajemen_m->insert_manajemen($values);
				
			}
			else
			{
				$result = $this->manajemen_m->update_manajemen($values, $row_id);
			}
		}

		// post processing, clean up replaced or deleted file
		if($id_file_foto_existing != NULL AND $id_file_foto_existing != ''
			AND $id_file_foto_existing != $values['id_file_foto']){

			Files::delete_file($id_file_foto_existing);
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	function _valid_mysql_date($input){
		if($input == NULL OR $input == ''){
			return TRUE;
		}

		if(preg_match('/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9])$/', $input)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function _valid_id_file_foto($input){
		// 0 = General error
		// 1 = Size error
		// 2 = Type error
		// 3 = OK
		if($input != 3){
			return FALSE;
		}else{
			return TRUE;
		}
	}

}