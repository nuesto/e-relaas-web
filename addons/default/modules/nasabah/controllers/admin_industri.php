<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Nasabah Module
 *
 * Manajemen nasabah corporate
 *
 */
class Admin_industri extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'industri';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'access_industri_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('nasabah');		
		$this->load->model('industri_m');
    }

    /**
	 * List all Industri
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_industri') AND ! group_has_role('nasabah', 'view_own_industri')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'c/nasabah/industri/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->industri_m->count_all_industri();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){

				$params[substr($key, 2)] = array('type'=>'like', 'value'=>$value);
			}
		}
		
        $data['industri']['entries'] = $this->industri_m->get_industri($pagination_config, $params);
		$data['industri']['total'] = $pagination_config['total_rows'];
		$data['industri']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['industri']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('c/nasabah/industri/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('c/nasabah/industri/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('nasabah:industri:plural'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('nasabah:industri:plural'))
			->build('admin/industri_index', $data);
    }
	
	/**
     * Create a new Industri entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'create_industri')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_industri('new')){	
				$this->session->set_flashdata('success', lang('nasabah:industri:submit_success'));				
				redirect('c/nasabah/industri/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('nasabah:industri:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/nasabah/industri/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:industri:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_industri') OR group_has_role('nasabah', 'view_own_industri')){
			$this->template->set_breadcrumb(lang('nasabah:industri:plural'), '/c/nasabah/industri/index');
		}

		$this->template->set_breadcrumb(lang('nasabah:industri:new'))
			->build('admin/industri_form', $data);
    }
	
	/**
     * Edit a Industri entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Industri to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_industri') AND ! group_has_role('nasabah', 'edit_own_industri')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_industri')){
			$entry = $this->industri_m->get_industri_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_industri('edit', $id)){	
				$this->session->set_flashdata('success', lang('nasabah:industri:submit_success'));				
				redirect('c/nasabah/industri/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('nasabah:industri:submit_failure');
			}
		}
		
		$data['fields'] = $this->industri_m->get_industri_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'c/nasabah/industri/index'.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:industri:edit'))
			->set_breadcrumb('Home', '/c')
			->set_breadcrumb(lang('nasabah:industri:plural'), '/c/nasabah/industri/index');
			
		// if(group_has_role('nasabah', 'view_all_industri') OR group_has_role('nasabah', 'view_own_industri')){
		// 	$this->template->set_breadcrumb(lang('nasabah:industri:plural'), '/c/nasabah/industri/index')
		// 	->set_breadcrumb(lang('nasabah:industri:view'), '/c/nasabah/industri/view/'.$id);
		// }else{
		// 	$this->template->set_breadcrumb(lang('nasabah:industri:plural'))
		// 	->set_breadcrumb(lang('nasabah:industri:view'));
		// }

		$this->template->set_breadcrumb(lang('nasabah:industri:edit'))
			->build('admin/industri_form', $data);
    }

    /**
     * Import new Industri entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function import()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'import_industri')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------

		if($_FILES) {
			$name = $_FILES['file']['name'];
			
			if($name!=null){
				if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
					$values = array();
					
					$skip_header = true;
					$row = 1;
					$error = array();
					while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
						
						if($skip_header) {
							$skip_header = false;
						} else {							
								if($data[0]=='') {
									$error[] = 'Nama industri pada baris-'.$row.' tidak boleh kosong';
								}
								if(strlen($data[0])>'255') {
									$error[] = 'Nama industri pada baris-'.$row.' tidak boleh lebih dari 255 karakter';
								}

								$industri = $this->industri_m->get_industri(null, array('nama'=>$data[0]));
								if(count($industri)>0) {
									$error[] = 'Nama industri pada baris-'.$row.' sudah ada';
								}
							
							$data_values[] = array(
								'nama' => $data[0],
							);
						}

						$row++;
					}

					if(count($error)==0) {
						$this->db->trans_start();
						foreach ($data_values as $key => $value) {
							$result = $this->industri_m->insert_industri($value);
						}
						$this->db->trans_complete();
						$result = $this->db->trans_status();
						if($result) {
							$data['messages']['success'] = lang('nasabah:industri:submit_success').'<br />';
						} else {
							$data['messages']['error'] = lang('nasabah:industri:submit_failure').'<br />';
						}
					} else {
						$result = false;
						$data['messages']['error'] = lang('nasabah:industri:submit_failure').'<br />';
						$data['messages']['error'] .= implode('<br />', $error);
					}
				}
			}
		}
		
		$data['mode'] = 'import';
		$data['return'] = 'c/nasabah/industri/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:industri:import'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_group') OR group_has_role('nasabah', 'view_own_group')){
			$this->template->set_breadcrumb(lang('nasabah:industri:plural'), '/c/nasabah/industri/index');
		}

		$this->template->set_breadcrumb(lang('nasabah:industri:import'))
			->build('admin/industri_import', $data);
    }
	
	/**
     * Delete a Industri entry
     * 
     * @param   int [$id] The id of Industri to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'delete_all_industri') AND ! group_has_role('nasabah', 'delete_own_industri')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		// Check view all/own permission
		if(! group_has_role('nasabah', 'delete_all_industri')){
			$entry = $this->industri_m->get_industri_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->industri_m->delete_industri_by_id($id);
        $this->session->set_flashdata('error', lang('nasabah:industri:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('c/nasabah/industri/index'.$data['uri']);
    }
	
	/**
     * Insert or update Industri entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_industri($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama', lang('nasabah:industri:nama'), 'required|max_length[256]');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->industri_m->insert_industri($values);
				
			}
			else
			{
				$result = $this->industri_m->update_industri($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	public function csv_example()
	{

    	// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=contoh_data_industri.csv');

		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');

		// output the column headings
		fputcsv($output, array(lang('nasabah:industri:nama')));

		// fetch the data
		$list = array(
			array('Penerbangan'),
			array('Perikanan'),
		);

		foreach ($list as $fields) {
			fputcsv($output, $fields);
		}

	}

}