<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Nasabah Module
 *
 * Manajemen nasabah corporate
 *
 */
class Admin_strategi extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'strategi';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'access_nasabah_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('nasabah');		
		$this->load->model('strategi_m');
		$this->load->model('nasabah_m');
    }

    /**
	 * List all Strategi
     *
     * @return	void
     */
    public function index_html($id_nasabah)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_nasabah') AND ! group_has_role('nasabah', 'view_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
		$data['fields_nasabah'] = $entry;
		if(! group_has_role('nasabah', 'view_all_nasabah')){
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config = NULL;

		$data['uri'] = get_query_string(6);

		$data['id_nasabah'] = $id_nasabah;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){

				$params[substr($key, 2)] = $value;
			}
		}
		
		$params['id_nasabah'] = $id_nasabah;

        $data['strategi']['entries'] = $this->strategi_m->get_strategi($pagination_config, $params);
		$data['strategi']['total'] = count($data['strategi']['entries']);
		$data['strategi']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------
		
        echo $this->load->view('admin/strategi_index', $data);
    }
	
	/**
     * Display one Strategi
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_strategi') AND ! group_has_role('nasabah', 'view_own_strategi')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['strategi'] = $this->strategi_m->get_strategi_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'view_all_strategi')){
			if($data['strategi']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}

		// -------------------------------------
        // Build the page. See views/c/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('nasabah:strategi:view'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_strategi') OR group_has_role('nasabah', 'view_own_strategi')){
			$this->template->set_breadcrumb(lang('nasabah:strategi:plural'), '/c/nasabah/strategi/index');
		}

		$this->template->set_breadcrumb(lang('nasabah:strategi:view'))
			->build('admin/strategi_entry', $data);
    }
	
	/**
     * Create a new Strategi entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($id_nasabah)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}

		$data['uri'] = get_query_string(6);

		$data['id_nasabah'] = $id_nasabah;
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_strategi('new')){	
				$this->session->set_flashdata('success', lang('nasabah:strategi:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#strategi');
			}else{
				$data['messages']['error'] = lang('nasabah:strategi:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#strategi';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:strategi:new'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'edit_all_nasabah') OR group_has_role('nasabah', 'edit_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id_nasabah);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:strategi:new'))
			->build('admin/strategi_form', $data);
    }
	
	/**
     * Edit a Strategi entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Strategi to the be deleted.
     * @return	void
     */
    public function edit($id_nasabah, $id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		$data['uri'] = get_query_string(7);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_strategi('edit', $id)){	
				$this->session->set_flashdata('success', lang('nasabah:strategi:submit_success'));				
				redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#strategi');
			}else{
				$data['messages']['error'] = lang('nasabah:strategi:submit_failure');
			}
		}
		
		$data['fields'] = $this->strategi_m->get_strategi_by_id($id);
		
		// decode konten JSON value to array
		$konten = json_decode($data['fields']['konten'], TRUE);
		
		if(isset($konten['proyek'])){
			$data['fields']['proyek'] = $konten['proyek'];
		}else{
			$data['fields']['proyek'] = '';
		}

		if(isset($konten['deskripsi'])){
			$data['fields']['deskripsi'] = $konten['deskripsi'];
		}else{
			$data['fields']['deskripsi'] = '';
		}

		if(isset($konten['financial_impact'])){
			$data['fields']['financial_impact'] = $konten['financial_impact'];
		}else{
			$data['fields']['financial_impact'] = '';
		}

		if(isset($konten['target'])){
			$data['fields']['target'] = $konten['target'];
		}else{
			$data['fields']['target'] = '';
		}

		unset($data['fields']['konten']);
		
		$data['mode'] = 'edit';
		$data['return'] = 'c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#strategi';
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('nasabah:strategi:edit'))
			->set_breadcrumb('Home', '/c');
			
		if(group_has_role('nasabah', 'view_all_nasabah') OR group_has_role('nasabah', 'view_own_nasabah')){
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'), '/c/nasabah/nasabah/index')
			->set_breadcrumb(lang('nasabah:nasabah:view'), '/c/nasabah/nasabah/view/'.$id_nasabah);
		}else{
			$this->template->set_breadcrumb(lang('nasabah:nasabah:plural'))
			->set_breadcrumb(lang('nasabah:nasabah:view'));
		}

		$this->template->set_breadcrumb(lang('nasabah:strategi:edit'))
			->build('admin/strategi_form', $data);
    }
	
	/**
     * Delete a Strategi entry
     * 
     * @param   int [$id] The id of Strategi to be deleted
     * @return  void
     */
    public function delete($id_nasabah, $id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('nasabah', 'edit_all_nasabah') AND ! group_has_role('nasabah', 'edit_own_nasabah')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('c');
		}
		
		// Check view all/own permission
		if(! group_has_role('nasabah', 'edit_all_nasabah')){
			$entry = $this->nasabah_m->get_nasabah_by_id($id_nasabah);
			$id_petugas = $entry['id_'.$this->current_user->group];
			if($id_petugas != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('c');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
        $this->strategi_m->delete_strategi_by_id($id);
        $this->session->set_flashdata('error', lang('nasabah:strategi:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(7);
        redirect('c/nasabah/nasabah/view/'.$id_nasabah.$data['uri'].'#strategi');
    }
	
	/**
     * Insert or update Strategi entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_strategi($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('id_nasabah', lang('nasabah:strategi:id_nasabah'), 'required');
		$this->form_validation->set_rules('proyek', lang('nasabah:strategi:proyek'), 'required|max_length[256]');
		$this->form_validation->set_rules('deskripsi', lang('nasabah:strategi:deskripsi'), 'max_length[512]');
		$this->form_validation->set_rules('financial_impact', lang('nasabah:strategi:financial_impact'), 'max_length[512]');
		$this->form_validation->set_rules('target', lang('nasabah:strategi:target'), 'max_length[512]');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			// merge some fields into one single JSON value
			$values['konten'] = array(
				'proyek' => $values['proyek'],
				'deskripsi' => $values['deskripsi'],
				'financial_impact' => $values['financial_impact'],
				'target' => $values['target'],
			);
			$values['konten'] = json_encode($values['konten']);

			// unset merged fields
			unset($values['proyek']);
			unset($values['deskripsi']);
			unset($values['financial_impact']);
			unset($values['target']);

			if ($method == 'new')
			{
				// add change history
				$update_log = array();
				$update_log['action'] = 'created';
				$update_log['on'] = date('Y-m-d H:i:s');
				$update_log['by'] = $this->current_user->display_name;

				$change_history = array();
				$change_history[] = $update_log;
				$values['change_history'] = json_encode($change_history);

				$result = $this->strategi_m->insert_strategi($values);
				
			}
			else //edit
			{
				// append change history
				$existing_strategi = $this->strategi_m->get_strategi_by_id($row_id);
				$change_history = json_decode($existing_strategi['change_history'], TRUE);

				$update_log = array();
				$update_log['action'] = 'updated';
				$update_log['on'] = date('Y-m-d H:i:s');
				$update_log['by'] = $this->current_user->display_name;
				$change_history[] = $update_log;

				$values['change_history'] = json_encode($change_history);

				$result = $this->strategi_m->update_strategi($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}