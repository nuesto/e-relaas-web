<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Group model
 *
 * @author Aditya Satrya
 */
class Nasabah_group_m extends MY_Model {
	
	public function get_group($pagination_config = NULL, $params = array())
	{
		$this->db->select('*');

		if(isset($params) and is_array($params)) {
			foreach ($params as $key => $value) {
				if(is_array($value)) {
					$type = isset($value['type']) ? $value['type'] : 'where';
					$escape = isset($value['escape']) ? $value['escape'] : true;
					$this->db->{$type}($key,$value['value'],$escape);
				} else {
					$this->db->where($key,$value);
				}
			}
		}

		$this->db->order_by("nama", "asc");
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_nasabah_group');
		$result = $query->result_array();
		
        return $result;
	}

	public function is_used($id_group){
		$this->db->where('id_group', $id_group);
		$this->db->from('default_nasabah_nasabah');
		$num_rows = $this->db->count_all_results();
		if($num_rows>0){
			return true;
		}
		return false;
	}
	
	public function get_group_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_nasabah_group');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_group_id_by_name($name)
	{
		$this->db->select('id');
		$this->db->where('nama', $name);
		$query = $this->db->get('default_nasabah_group');
		$result = $query->row_array();
		
		if(isset($result['id'])){
			return $result['id'];
		}else{
			return NULL;
		}
	}
	
	public function count_all_group()
	{
		return $this->db->count_all('nasabah_group');
	}
	
	public function delete_group_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_nasabah_group');
	}
	
	public function insert_group($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_nasabah_group', $values);
	}
	
	public function update_group($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_nasabah_group', $values); 
	}
	
}