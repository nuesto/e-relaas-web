<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Produk model
 *
 * @author Aditya Satrya
 */
class Produk_m extends MY_Model {
	
	public function get_produk($params = array())
	{
		$this->db->select('produk_product.id AS id_produk');
		$this->db->select('produk_product.nama AS produk_nama');
		$this->db->select('produk_product.id_item AS id_item');
		$this->db->select('produk_item.nama AS item_nama');
		$this->db->select('produk_item.id_family AS id_family');
		$this->db->select('produk_family.nama AS family_nama');
		$this->db->select('produk_product.kategori AS kategori');
		$this->db->select('produk_product.created_on AS created_on');
		$this->db->select('produk_product.created_by AS created_by');
		$this->db->select('produk_product.updated_on AS updated_on');
		$this->db->select('produk_product.updated_by AS updated_by');
		$this->db->select('produk_product.ordering_count AS ordering_count');

		foreach ($params as $key => $param) {
			$this->db->$param['function']($param['column'], $param['value']);
		}

		$this->db->join('produk_item', 'produk_item.id = produk_product.id_item', 'left');
		$this->db->join('produk_family', 'produk_family.id = produk_item.id_family', 'left');

		$this->db->order_by("produk_family.ordering_count", "asc");
		$this->db->order_by("produk_item.ordering_count", "asc");
		$this->db->order_by("produk_product.ordering_count", "asc");
		// $this->db->group_by("produk_product.id");
		
		$query = $this->db->get('default_produk_product');
		$result = $query->result_array();
		
        return $result;
	}

	public function get_produk_nasabah($id_nasabah, $params = array()){
		$this->db->select('nasabah_produk.id_nasabah AS id_nasabah');
		$this->db->select('nasabah_produk.status AS status');
		$this->db->select('nasabah_produk.keterangan AS keterangan');
		$this->db->select('produk_product.id AS id_produk');
		$this->db->select('produk_product.nama AS produk_nama');
		$this->db->select('produk_product.id_item AS id_item');
		$this->db->select('produk_item.nama AS item_nama');
		$this->db->select('produk_item.id_family AS id_family');
		$this->db->select('produk_family.nama AS family_nama');
		$this->db->select('produk_product.kategori AS kategori');
		$this->db->select('produk_product.created_on AS created_on');
		$this->db->select('produk_product.created_by AS created_by');
		$this->db->select('produk_product.updated_on AS updated_on');
		$this->db->select('produk_product.updated_by AS updated_by');
		$this->db->select('produk_product.ordering_count AS ordering_count');

		foreach ($params as $key => $param) {
			$this->db->$param['function']($param['column'], $param['value']);
		}

		$this->db->where('nasabah_produk.id_nasabah', $id_nasabah);

		$this->db->join('produk_item', 'produk_item.id = produk_product.id_item');
		$this->db->join('produk_family', 'produk_family.id = produk_item.id_family');
		$this->db->join('nasabah_produk', 'nasabah_produk.id_produk = produk_product.id');

		// $this->db->order_by("nasabah_produk.status", "desc");
		$this->db->order_by("produk_family.ordering_count", "asc");
		$this->db->order_by("produk_item.ordering_count", "asc");
		$this->db->order_by("produk_product.ordering_count", "asc");
		// $this->db->group_by("produk_product.id");
		
		$query = $this->db->get('default_produk_product');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_produk_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_nasabah_produk');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_produk()
	{
		return $this->db->count_all('nasabah_produk');
	}
	
	public function delete_produk_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_nasabah_produk');
	}

	public function delete_produk_by_id_nasabah($id_nasabah)
	{
		$this->db->where('id_nasabah', $id_nasabah);
		$this->db->delete('default_nasabah_produk');
	}
	
	public function insert_produk($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_nasabah_produk', $values);
	}
	
	public function update_produk($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_nasabah_produk', $values); 
	}
	
}