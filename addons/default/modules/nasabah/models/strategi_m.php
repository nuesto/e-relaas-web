<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Strategi model
 *
 * @author Aditya Satrya
 */
class Strategi_m extends MY_Model {
	
	public function get_strategi($pagination_config = NULL, $params = array())
	{
		$this->db->select('*');

		foreach ($params as $key => $value) {
			if($key == 'id_nasabah'){
				$this->db->where($key, $value);
			}else{
				$this->db->like($key, $value);
			}
		}

		$this->db->order_by("ordering_count", "asc");
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_nasabah_strategi');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_strategi_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_nasabah_strategi');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_strategi()
	{
		return $this->db->count_all('nasabah_strategi');
	}
	
	public function delete_strategi_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_nasabah_strategi');
	}
	
	public function insert_strategi($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_nasabah_strategi', $values);
	}
	
	public function update_strategi($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_nasabah_strategi', $values); 
	}
	
}