<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Nasabah model
 *
 * @author Aditya Satrya
 */
class Nasabah_m extends MY_Model {
	
	public function get_nasabah($pagination_config = NULL, $params = array())
	{
		$this->db->select('nasabah_nasabah.id AS id');
		$this->db->select('nasabah_nasabah.id_cra');
		$this->db->select('nasabah_nasabah.id_crm');
		$this->db->select('nasabah_nasabah.nama AS nama');
		$this->db->select('nasabah_nasabah.is_starred_item');
		$this->db->select('nasabah_group.id AS id_group');
		$this->db->select('nasabah_group.nama AS group_nama');
		$this->db->select('nasabah_segmen.id AS id_segmen');
		$this->db->select('nasabah_segmen.nama AS segmen_nama');
		$this->db->select('cra_users.user_id AS cra_user_id');
		$this->db->select('cra_users.display_name AS cra_display_name');
		$this->db->select('crm_users.user_id AS crm_user_id');
		$this->db->select('crm_users.display_name AS crm_display_name');

		foreach ($params as $key => $param) {
			$this->db->$param['function']($param['column'], $param['value']);
		}

		$this->db->join('nasabah_group', 'nasabah_group.id = nasabah_nasabah.id_group', 'left');
		$this->db->join('nasabah_segmen', 'nasabah_segmen.id = nasabah_nasabah.id_segmen', 'left');
		$this->db->join('profiles AS cra_users', 'cra_users.user_id = nasabah_nasabah.id_cra', 'left');
		$this->db->join('profiles AS crm_users', 'crm_users.user_id = nasabah_nasabah.id_crm', 'left');

		$this->db->order_by("nasabah_segmen.ordering_count", "asc");
		$this->db->order_by("nasabah_group.ordering_count", "asc");
		$this->db->order_by("nasabah_nasabah.ordering_count", "asc");
		$this->db->order_by("nasabah_nasabah.nama", "asc");
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_nasabah_nasabah');
		$result = $query->result_array();
		
        return $result;
	}

	public function get_nasabah_birthdate($pagination_config = NULL, $params = array())
	{
		$this->db->select('nasabah_nasabah.id AS id');
		$this->db->select('nasabah_nasabah.id_cra');
		$this->db->select('nasabah_nasabah.id_crm');
		$this->db->select('nasabah_nasabah.nama AS nama');
		$this->db->select('nasabah_nasabah.anniversary AS anniversary');
		$this->db->select('nasabah_nasabah.is_starred_item');
		$this->db->select('nasabah_group.id AS id_group');
		$this->db->select('nasabah_group.nama AS group_nama');
		$this->db->select('nasabah_segmen.id AS id_segmen');
		$this->db->select('nasabah_segmen.nama AS segmen_nama');
		$this->db->select('cra_users.display_name AS cra_display_name');
		$this->db->select('crm_users.display_name AS crm_display_name');

		foreach ($params as $key => $param) {
			$this->db->$param['function']($param['column'], $param['value']);
		}

		$this->db->join('nasabah_group', 'nasabah_group.id = nasabah_nasabah.id_group', 'left');
		$this->db->join('nasabah_segmen', 'nasabah_segmen.id = nasabah_nasabah.id_segmen', 'left');
		$this->db->join('profiles AS cra_users', 'cra_users.user_id = nasabah_nasabah.id_cra', 'left');
		$this->db->join('profiles AS crm_users', 'crm_users.user_id = nasabah_nasabah.id_crm', 'left');

		$this->db->order_by("nasabah_nasabah.anniversary", "asc");
		$this->db->order_by("nasabah_segmen.ordering_count", "asc");
		$this->db->order_by("nasabah_group.ordering_count", "asc");
		$this->db->order_by("nasabah_nasabah.ordering_count", "asc");
		$this->db->order_by("nasabah_nasabah.nama", "asc");
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_nasabah_nasabah');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_nasabah_by_id($id)
	{
		$this->db->select('nasabah_nasabah.id AS id');
		$this->db->select('nasabah_nasabah.id_cra');
		$this->db->select('nasabah_nasabah.id_crm');
		$this->db->select('nasabah_nasabah.nama AS nama');
		$this->db->select('nasabah_nasabah.deskripsi AS deskripsi');
		$this->db->select('nasabah_nasabah.anniversary AS anniversary');
		$this->db->select('nasabah_group.id AS id_group');
		$this->db->select('nasabah_group.nama AS group_nama');
		$this->db->select('nasabah_segmen.id AS id_segmen');
		$this->db->select('nasabah_segmen.nama AS segmen_nama');
		$this->db->select('nasabah_industri.id AS id_industri');
		$this->db->select('nasabah_industri.nama AS industri_nama');
		$this->db->select('nasabah_nasabah.id_logo AS id_logo');
		$this->db->select('nasabah_nasabah.pic_nama AS pic_nama');
		$this->db->select('nasabah_nasabah.pic_handphone AS pic_handphone');
		$this->db->select('nasabah_nasabah.pic_email AS pic_email');
		$this->db->select('nasabah_nasabah.pic_birthdate AS pic_birthdate');
		$this->db->select('nasabah_nasabah.website AS website');
		$this->db->select('nasabah_nasabah.alamat AS alamat');
		$this->db->select('nasabah_nasabah.loc_latitude AS loc_latitude');
		$this->db->select('nasabah_nasabah.loc_langitude AS loc_langitude');
		$this->db->select('nasabah_nasabah.id_file_struktur AS id_file_struktur');
		$this->db->select('nasabah_nasabah.id_file_area AS id_file_area');
		$this->db->select('nasabah_nasabah.catatan AS catatan');
		$this->db->select('cra_profile.display_name AS cra_display_name');
		$this->db->select('cra_profile.no_hp AS cra_handphone');
		$this->db->select('cra_user.id AS id_cra');
		$this->db->select('cra_user.email AS cra_email');
		$this->db->select('crm_profile.display_name AS crm_display_name');
		$this->db->select('crm_user.id AS id_crm');
		$this->db->select('crm_user.email AS crm_email');

		$this->db->where('nasabah_nasabah.id', $id);

		$this->db->join('nasabah_group', 'nasabah_group.id = nasabah_nasabah.id_group', 'left');
		$this->db->join('nasabah_segmen', 'nasabah_segmen.id = nasabah_nasabah.id_segmen', 'left');
		$this->db->join('nasabah_industri', 'nasabah_industri.id = nasabah_nasabah.id_industri', 'left');
		$this->db->join('users AS cra_user', 'cra_user.id = nasabah_nasabah.id_cra', 'left');
		$this->db->join('profiles AS cra_profile', 'cra_profile.user_id = nasabah_nasabah.id_cra', 'left');
		$this->db->join('users AS crm_user', 'crm_user.id = nasabah_nasabah.id_crm', 'left');
		$this->db->join('profiles AS crm_profile', 'crm_profile.user_id = nasabah_nasabah.id_crm', 'left');

		$query = $this->db->get('default_nasabah_nasabah');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_nasabah_id_by_name($name)
	{
		$this->db->select('nasabah_nasabah.id AS id');
		$this->db->where('nasabah_nasabah.nama', $name);
		$query = $this->db->get('default_nasabah_nasabah');
		$result = $query->row_array();
		
		if(isset($result['id'])){
			return $result['id'];
		}else{
			return NULL;
		}
	}
	
	public function count_all_nasabah($params = array())
	{
		return count($this->get_nasabah(null, $params));
	}
	
	public function delete_nasabah_by_id($id)
	{
		$this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->delete('default_nasabah_nasabah');
		$this->db->trans_complete();
		return $this->db->trans_status();
	}
	
	public function insert_nasabah($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_nasabah_nasabah', $values);
	}

	public function insert_batch_nasabah($values)
	{
		foreach ($values as $key => $value) {
			$values[$key]['created_on'] = date("Y-m-d H:i:s");
			$values[$key]['created_by'] = $this->current_user->id;
		}
		
		return $this->db->insert_batch('default_nasabah_nasabah', $values);
	}
	
	public function update_nasabah($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_nasabah_nasabah', $values); 
	}
	
}