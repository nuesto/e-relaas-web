<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Pks model
 *
 * @author Aditya Satrya
 */
class Pks_m extends MY_Model {
	
	public function get_pks($pagination_config = NULL, $params = array())
	{
		$this->db->select('*');

		foreach ($params as $key => $param) {
			$this->db->$param['function']($param['column'], $param['value']);
		}

		$this->db->order_by("ordering_count", "asc");
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_nasabah_pks');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_pks_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_nasabah_pks');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_pks()
	{
		return $this->db->count_all('nasabah_pks');
	}
	
	public function delete_pks_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_nasabah_pks');
	}
	
	public function insert_pks($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_nasabah_pks', $values);
	}
	
	public function update_pks($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_nasabah_pks', $values); 
	}
	
}