<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Manajemen model
 *
 * @author Aditya Satrya
 */
class Manajemen_m extends MY_Model {
	
	public function get_manajemen($pagination_config = NULL, $params = array())
	{
		$this->db->select('nasabah_manajemen.*');

		foreach ($params as $key => $param) {
			$this->db->$param['function']($param['column'], $param['value']);
		}

		$this->db->order_by("ordering_count", "asc");
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$this->db->join('nasabah_nasabah','nasabah_manajemen.id_nasabah=nasabah_nasabah.id');
		$query = $this->db->get('default_nasabah_manajemen');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_manajemen_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_nasabah_manajemen');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_manajemen()
	{
		return $this->db->count_all('nasabah_manajemen');
	}
	
	public function delete_manajemen_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_nasabah_manajemen');
	}
	
	public function insert_manajemen($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_nasabah_manajemen', $values);
	}
	
	public function update_manajemen($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_nasabah_manajemen', $values); 
	}
	
}