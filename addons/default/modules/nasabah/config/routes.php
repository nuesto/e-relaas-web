<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['nasabah/api(/:any)?']		= 'api_nasabah$1';
$route['nasabah/c/nasabah(:any)'] = 'admin_nasabah$1';
$route['nasabah/c/group(:any)'] = 'admin_group$1';
$route['nasabah/c/segmen(:any)'] = 'admin_segmen$1';
$route['nasabah/c/industri(:any)'] = 'admin_industri$1';
$route['nasabah/c/manajemen(:any)'] = 'admin_manajemen$1';
$route['nasabah/c/strategi(:any)'] = 'admin_strategi$1';
$route['nasabah/c/pks(:any)'] = 'admin_pks$1';
$route['nasabah/c/produk(:any)'] = 'admin_produk$1';
$route['nasabah/group(:any)'] = 'nasabah_group$1';
$route['nasabah/segmen(:any)'] = 'nasabah_segmen$1';
$route['nasabah/industri(:any)'] = 'nasabah_industri$1';
$route['nasabah/nasabah(:any)'] = 'nasabah_nasabah$1';
$route['nasabah/manajemen(:any)'] = 'nasabah_manajemen$1';
$route['nasabah/strategi(:any)'] = 'nasabah_strategi$1';
$route['nasabah/pks(:any)'] = 'nasabah_pks$1';
$route['nasabah/produk(:any)'] = 'nasabah_produk$1';
