<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Nasabah extends Module
{
    public $version = '1.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Nasabah',
			'id' => 'Nasabah',
		);
		$info['description'] = array(
			'en' => 'Manajemen nasabah corporate',
			'id' => 'Manajemen nasabah corporate',
		);
		$info['frontend'] = false;
		$info['backend'] = true;
		$info['menu'] = 'nasabah';
		$info['roles'] = array(
			'access_group_backend', 'view_all_group', 'view_own_group', 'edit_all_group', 'edit_own_group', 'delete_all_group', 'delete_own_group', 'create_group', 'import_group',
			'access_segmen_backend', 'view_all_segmen', 'view_own_segmen', 'edit_all_segmen', 'edit_own_segmen', 'delete_all_segmen', 'delete_own_segmen', 'create_segmen',
			'access_industri_backend', 'view_all_industri', 'view_own_industri', 'edit_all_industri', 'edit_own_industri', 'delete_all_industri', 'delete_own_industri', 'create_industri', 'import_industri',
			'access_nasabah_backend', 'view_all_nasabah', 'view_own_nasabah', 'edit_all_nasabah', 'edit_own_nasabah', 'delete_all_nasabah', 'delete_own_nasabah', 'create_nasabah', 'import_nasabah',
			// 'access_manajemen_backend', 'view_all_manajemen', 'view_own_manajemen', 'edit_all_manajemen', 'edit_own_manajemen', 'delete_all_manajemen', 'delete_own_manajemen', 'create_manajemen',
			// 'access_strategi_backend', 'view_all_strategi', 'view_own_strategi', 'edit_all_strategi', 'edit_own_strategi', 'delete_all_strategi', 'delete_own_strategi', 'create_strategi',
			// 'access_pks_backend', 'view_all_pks', 'view_own_pks', 'edit_all_pks', 'edit_own_pks', 'delete_all_pks', 'delete_own_pks', 'create_pks',
			// 'access_produk_backend', 'view_all_produk', 'view_own_produk', 'edit_all_produk', 'edit_own_produk', 'delete_all_produk', 'delete_own_produk', 'create_produk',
		);
		
		if(group_has_role('nasabah', 'access_group_backend')){
			$info['sections']['group']['name'] = 'nasabah:group:plural';
			$info['sections']['group']['uri'] = array('urls'=>array('c/nasabah/group/index','c/nasabah/group/create','c/nasabah/group/view%1','c/nasabah/group/edit%1','c/nasabah/group/view%2','c/nasabah/group/edit%2','c/nasabah/group/import'));

			if(group_has_role('nasabah', 'import_group')){
				$info['sections']['group']['shortcuts']['import'] = array(
					'name' => 'nasabah:group:import',
					'uri' => 'c/nasabah/group/import',
					'class' => 'add'
				);
			}
			
			if(group_has_role('nasabah', 'create_group')){
				$info['sections']['group']['shortcuts']['create'] = array(
					'name' => 'nasabah:group:new',
					'uri' => 'c/nasabah/group/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('nasabah', 'access_segmen_backend')){
			$info['sections']['segmen']['name'] = 'nasabah:segmen:plural';
			$info['sections']['segmen']['uri'] = array('urls'=>array('c/nasabah/segmen/index','c/nasabah/segmen/create','c/nasabah/segmen/view%1','c/nasabah/segmen/edit%1','c/nasabah/segmen/view%2','c/nasabah/segmen/edit%2'));
			
			if(group_has_role('nasabah', 'create_segmen')){
				$info['sections']['segmen']['shortcuts']['create'] = array(
					'name' => 'nasabah:segmen:new',
					'uri' => 'c/nasabah/segmen/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('nasabah', 'access_industri_backend')){
			$info['sections']['industri']['name'] = 'nasabah:industri:plural';
			$info['sections']['industri']['uri'] = array('urls'=>array('c/nasabah/industri/index','c/nasabah/industri/create','c/nasabah/industri/view%1','c/nasabah/industri/edit%1','c/nasabah/industri/view%2','c/nasabah/industri/edit%2'));
			
			if(group_has_role('nasabah', 'create_industri')){
				$info['sections']['industri']['shortcuts']['create'] = array(
					'name' => 'nasabah:industri:new',
					'uri' => 'c/nasabah/industri/create',
					'class' => 'add'
				);
			}

			if(group_has_role('nasabah', 'import_industri')){
				$info['sections']['industri']['shortcuts']['import'] = array(
					'name' => 'nasabah:industri:import',
					'uri' => 'c/nasabah/industri/import',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('nasabah', 'access_nasabah_backend')){
			$info['sections']['nasabah']['name'] = 'nasabah:nasabah:plural';
			$info['sections']['nasabah']['uri'] = array('urls'=>array('c/nasabah/nasabah/index','c/nasabah/nasabah/create','c/nasabah/nasabah/view%1','c/nasabah/nasabah/edit%1','c/nasabah/nasabah/view%2','c/nasabah/nasabah/edit%2', 'c/nasabah/manajemen/create%1', 'c/nasabah/manajemen/edit%1', 'c/nasabah/manajemen/edit%2', 'c/nasabah/nasabah/edit_struktur%1', 'c/nasabah/nasabah/edit_struktur%2', 'c/nasabah/nasabah/edit_area%1', 'c/nasabah/nasabah/edit_area%2', 'c/nasabah/strategi/create%1', 'c/nasabah/strategi/edit%1','c/nasabah/strategi/create%2','c/nasabah/strategi/edit%2', 'c/nasabah/produk/edit%1', 'c/nasabah/produk/edit%2', 'c/nasabah/pks/create%1', 'c/nasabah/pks/edit%1','c/nasabah/pks/edit%2', 'c/nasabah/nasabah/create_catatan%1', 'c/nasabah/nasabah/create_catatan%2', 'c/nasabah/nasabah/edit_catatan%1', 'c/nasabah/nasabah/edit_catatan%2', 'c/nasabah/nasabah/import'));
			
			if(group_has_role('nasabah', 'import_nasabah')){
				$info['sections']['nasabah']['shortcuts']['import'] = array(
					'name' => 'nasabah:nasabah:import',
					'uri' => 'c/nasabah/nasabah/import',
					'class' => 'add'
				);
			}

			if(group_has_role('nasabah', 'create_nasabah')){
				$info['sections']['nasabah']['shortcuts']['create'] = array(
					'name' => 'nasabah:nasabah:new',
					'uri' => 'c/nasabah/nasabah/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items){
		if($this->current_user->group == 'cra' || $this->current_user->group == 'crm'){
			if(group_has_role('nasabah','access_nasabah_backend')){
				if(group_has_role('nasabah', 'view_all_nasabah') || group_has_role('nasabah','view_own_nasabah')) {
					$menu_items['lang:cp:nav_nasabah']['urls'] = array('c/nasabah/nasabah/index','c/nasabah/nasabah/create','c/nasabah/nasabah/create%1','c/nasabah/nasabah/view%1','c/nasabah/nasabah/edit%1','c/nasabah/manajemen/create%1','c/nasabah/manajemen/edit%1/%1','c/nasabah/nasabah/create_struktur%1','c/nasabah/nasabah/edit_area%1','c/nasabah/strategi/create%1','c/nasabah/produk/edit%1','c/nasabah/pks/create%1','c/nasabah/nasabah/create_catatan%1','c/nasabah/manajemen/edit%2','c/nasabah/nasabah/edit_struktur%1','c/nasabah/nasabah/edit_area%1','c/nasabah/strategi/edit%2','c/nasabah/pks/edit%2','c/nasabah/nasabah/edit_catatan%1');
				}
			}
		}
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// group
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 256,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('nasabah_group', TRUE);
		$this->db->query("CREATE INDEX author_nasabah_group_idx ON ".$this->db->dbprefix("nasabah_group")."(created_by)");

		// segmen
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 256,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('nasabah_segmen', TRUE);
		$this->db->query("CREATE INDEX author_nasabah_segmen_idx ON ".$this->db->dbprefix("nasabah_segmen")."(created_by)");

		// industri
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 256,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('nasabah_industri', TRUE);
		$this->db->query("CREATE INDEX author_nasabah_industri_idx ON ".$this->db->dbprefix("nasabah_industri")."(created_by)");

		// nasabah
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 256,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'deskripsi' => array(
				'type' => 'TEXT',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'anniversary' => array(
				'type' => 'DATE',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'id_group' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'id_segmen' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'id_industri' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'id_logo' => array(
				'type' => 'CHAR',
				'constraint' => 15,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'pic_nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 256,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'pic_email' => array(
				'type' => 'VARCHAR',
				'constraint' => 256,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'pic_handphone' => array(
				'type' => 'VARCHAR',
				'constraint' => 25,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'pic_birthdate' => array(
				'type' => 'DATETIME',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'id_cra' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'id_crm' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'website' => array(
				'type' => 'VARCHAR',
				'constraint' => 256,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'alamat' => array(
				'type' => 'TEXT',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'loc_latitude' => array(
				'type' => 'DECIMAL',
				'constraint' => array(10, 6),
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'loc_langitude' => array(
				'type' => 'DECIMAL',
				'constraint' => array(10, 6),
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'id_file_struktur' => array(
				'type' => 'CHAR',
				'constraint' => 15,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'id_file_area' => array(
				'type' => 'CHAR',
				'constraint' => 15,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'catatan' => array(
				'type' => 'TEXT',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('nasabah_nasabah', TRUE);
		$this->db->query("CREATE INDEX author_nasabah_nasabah_idx ON ".$this->db->dbprefix("nasabah_nasabah")."(created_by)");

		// foreign key to group
		$this->db->query("CREATE INDEX fk_nasabah_nasabah_group_idx ON ".$this->db->dbprefix("nasabah_nasabah")."(id_group)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_nasabah")." 
			ADD CONSTRAINT `fk_nasabah_nasabah_group` 
			FOREIGN KEY (`id_group`) 
			REFERENCES ".$this->db->dbprefix("nasabah_group")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");

		// foreign key to segmen
		$this->db->query("CREATE INDEX fk_nasabah_nasabah_segmen_idx ON ".$this->db->dbprefix("nasabah_nasabah")."(id_segmen)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_nasabah")." 
			ADD CONSTRAINT `fk_nasabah_nasabah_segmen` 
			FOREIGN KEY (`id_segmen`) 
			REFERENCES ".$this->db->dbprefix("nasabah_segmen")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");

		// foreign key to industri
		$this->db->query("CREATE INDEX fk_nasabah_nasabah_industri_idx ON ".$this->db->dbprefix("nasabah_nasabah")."(id_industri)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_nasabah")." 
			ADD CONSTRAINT `fk_nasabah_nasabah_industri` 
			FOREIGN KEY (`id_industri`) 
			REFERENCES ".$this->db->dbprefix("nasabah_industri")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");

		// foreign key to cra
		$this->db->query("CREATE INDEX fk_nasabah_nasabah_cra_idx ON ".$this->db->dbprefix("nasabah_nasabah")."(id_cra)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_nasabah")." 
			ADD CONSTRAINT `fk_nasabah_nasabah_cra` 
			FOREIGN KEY (`id_cra`) 
			REFERENCES ".$this->db->dbprefix("users")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");

		// foreign key to crm
		$this->db->query("CREATE INDEX fk_nasabah_nasabah_crm_idx ON ".$this->db->dbprefix("nasabah_nasabah")."(id_crm)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_nasabah")." 
			ADD CONSTRAINT `fk_nasabah_nasabah_crm` 
			FOREIGN KEY (`id_crm`) 
			REFERENCES ".$this->db->dbprefix("users")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");

		// foreign key to logo
		$this->db->query("CREATE INDEX fk_nasabah_nasabah_logo_idx ON ".$this->db->dbprefix("nasabah_nasabah")."(id_logo)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_nasabah")." 
			ADD CONSTRAINT `fk_nasabah_nasabah_logo` 
			FOREIGN KEY (`id_logo`) 
			REFERENCES ".$this->db->dbprefix("files")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");

		// foreign key to file_struktur
		$this->db->query("CREATE INDEX fk_nasabah_nasabah_file_struktur_idx ON ".$this->db->dbprefix("nasabah_nasabah")."(id_file_struktur)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_nasabah")." 
			ADD CONSTRAINT `fk_nasabah_nasabah_file_struktur` 
			FOREIGN KEY (`id_file_struktur`) 
			REFERENCES ".$this->db->dbprefix("files")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");

		// foreign key to file_area
		$this->db->query("CREATE INDEX fk_nasabah_nasabah_file_area_idx ON ".$this->db->dbprefix("nasabah_nasabah")."(id_file_area)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_nasabah")." 
			ADD CONSTRAINT `fk_nasabah_nasabah_file_area` 
			FOREIGN KEY (`id_file_area`) 
			REFERENCES ".$this->db->dbprefix("files")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");

		// manajemen
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'id_nasabah' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'kategori' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 256,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'jabatan' => array(
				'type' => 'VARCHAR',
				'constraint' => 256,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'id_file_foto' => array(
				'type' => 'CHAR',
				'constraint' => 15,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'birthdate' => array(
				'type' => 'DATETIME',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('nasabah_manajemen', TRUE);
		$this->db->query("CREATE INDEX author_nasabah_manajemen_idx ON ".$this->db->dbprefix("nasabah_manajemen")."(created_by)");

		// add foreign key to nasabah
		$this->db->query("CREATE INDEX fk_nasabah_manajemen_nasabah_idx ON ".$this->db->dbprefix("nasabah_manajemen")."(id_nasabah)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_manajemen")." 
			ADD CONSTRAINT `fk_nasabah_manajemen_nasabah` 
			FOREIGN KEY (`id_nasabah`) 
			REFERENCES ".$this->db->dbprefix("nasabah_nasabah")." (`id`) 
			ON DELETE CASCADE 
			ON UPDATE CASCADE");

		// foreign key to file foto
		$this->db->query("CREATE INDEX fk_nasabah_manajemen_foto_idx ON ".$this->db->dbprefix("nasabah_manajemen")."(id_file_foto)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_manajemen")." 
			ADD CONSTRAINT `fk_nasabah_nasabah_file_foto` 
			FOREIGN KEY (`id_file_foto`) 
			REFERENCES ".$this->db->dbprefix("files")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");


		// strategi
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'id_nasabah' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'konten' => array(
				'type' => 'TEXT',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'change_history' => array(
				'type' => 'TEXT',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('nasabah_strategi', TRUE);
		$this->db->query("CREATE INDEX author_nasabah_strategi_idx ON ".$this->db->dbprefix("nasabah_strategi")."(created_by)");

		// add foreign key to nasabah
		$this->db->query("CREATE INDEX fk_nasabah_strategi_nasabah_idx ON ".$this->db->dbprefix("nasabah_strategi")."(id_nasabah)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_strategi")." 
			ADD CONSTRAINT `fk_nasabah_strategi_nasabah` 
			FOREIGN KEY (`id_nasabah`) 
			REFERENCES ".$this->db->dbprefix("nasabah_nasabah")." (`id`) 
			ON DELETE CASCADE 
			ON UPDATE CASCADE");


		// pks
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'id_nasabah' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'deskripsi' => array(
				'type' => 'VARCHAR',
				'constraint' => 256,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'tipe' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'nomor' => array(
				'type' => 'VARCHAR',
				'constraint' => 128,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'tanggal_ttd' => array(
				'type' => 'DATETIME',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'jangka_waktu' => array(
				'type' => 'SMALLINT',
				'constraint' => NULL,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'pejabat_bni' => array(
				'type' => 'VARCHAR',
				'constraint' => 256,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'pejabat_nasabah' => array(
				'type' => 'VARCHAR',
				'constraint' => 256,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'keterangan' => array(
				'type' => 'VARCHAR',
				'constraint' => 512,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('nasabah_pks', TRUE);
		$this->db->query("CREATE INDEX author_nasabah_pks_idx ON ".$this->db->dbprefix("nasabah_pks")."(created_by)");

		// add foreign key to nasabah
		$this->db->query("CREATE INDEX fk_nasabah_pks_nasabah_idx ON ".$this->db->dbprefix("nasabah_pks")."(id_nasabah)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_pks")." 
			ADD CONSTRAINT `fk_nasabah_pks_nasabah` 
			FOREIGN KEY (`id_nasabah`) 
			REFERENCES ".$this->db->dbprefix("nasabah_nasabah")." (`id`) 
			ON DELETE CASCADE 
			ON UPDATE CASCADE");


		// produk
		$fields = array(
			'id_nasabah' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'id_produk' => array(
				'type' => 'INT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'status' => array(
				'type' => 'TINYINT',
				'constraint' => NULL,
				'unsigned' => TRUE,
				'default' => NULL,
				'null' => FALSE,
				'auto_increment' => FALSE,
			),
			'keterangan' => array(
				'type' => 'VARCHAR',
				'constraint' => 512,
				'unsigned' => FALSE,
				'default' => NULL,
				'null' => TRUE,
				'auto_increment' => FALSE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id_nasabah', TRUE);
		$this->dbforge->add_key('id_produk', TRUE);
		$this->dbforge->create_table('nasabah_produk', TRUE);
		$this->db->query("CREATE INDEX author_nasabah_produk_idx ON ".$this->db->dbprefix("nasabah_produk")."(created_by)");

		// add foreign key to nasabah
		$this->db->query("CREATE INDEX fk_nasabah_produk_nasabah_idx ON ".$this->db->dbprefix("nasabah_produk")."(id_nasabah)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_produk")." 
			ADD CONSTRAINT `fk_nasabah_produk_nasabah` 
			FOREIGN KEY (`id_nasabah`) 
			REFERENCES ".$this->db->dbprefix("nasabah_nasabah")." (`id`) 
			ON DELETE CASCADE 
			ON UPDATE CASCADE");

		// add foreign key to product
		$this->db->query("CREATE INDEX fk_nasabah_produk_product_idx ON ".$this->db->dbprefix("nasabah_produk")."(id_nasabah)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("nasabah_produk")." 
			ADD CONSTRAINT `fk_nasabah_produk_produk` 
			FOREIGN KEY (`id_produk`) 
			REFERENCES ".$this->db->dbprefix("produk_product")." (`id`) 
			ON DELETE CASCADE 
			ON UPDATE CASCADE");

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('nasabah_produk');
        $this->dbforge->drop_table('nasabah_pks');
        $this->dbforge->drop_table('nasabah_strategi');
        $this->dbforge->drop_table('nasabah_manajemen');
        $this->dbforge->drop_table('nasabah_nasabah');
        $this->dbforge->drop_table('nasabah_industri');
        $this->dbforge->drop_table('nasabah_segmen');
        $this->dbforge->drop_table('nasabah_group');
        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}