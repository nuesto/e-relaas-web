<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['landing_page/admin/konten(:any)'] = 'admin_konten$1';
$route['landing_page/admin/leads(:any)'] = 'admin_leads$1';
$route['landing_page/konten(:any)'] = 'landing_page_konten$1';
$route['landing_page/leads(:any)'] = 'landing_page_leads$1';
