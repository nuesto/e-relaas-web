<div class="page-header">
	<h1>
		<span><?php echo lang('landing_page:konten:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('landing_page/konten/index'.$uri); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('landing_page:back') ?>
		</a>

		<?php if(group_has_role('landing_page', 'edit_all_konten')){ ?>
			<a href="<?php echo site_url('landing_page/konten/edit/'.$konten['id'].$uri); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('landing_page', 'edit_own_konten')){ ?>
			<?php if($konten->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('landing_page/konten/edit/'.$konten['id'].$uri); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('landing_page', 'delete_all_konten')){ ?>
			<a href="<?php echo site_url('landing_page/konten/delete/'.$konten['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('landing_page', 'delete_own_konten')){ ?>
			<?php if($konten->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('landing_page/konten/delete/'.$konten['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $konten['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:id'); ?></div>
		<?php if(isset($konten['id'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $konten['id']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:title'); ?></div>
		<?php if(isset($konten['title'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $konten['title']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:id_affiliate'); ?></div>
		<?php if(isset($konten['id_affiliate'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $konten['id_affiliate']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:konten'); ?></div>
		<?php if(isset($konten['konten'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $konten['konten']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:id_foto'); ?></div>
		<?php if(isset($konten['id_foto'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $konten['id_foto']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:link_video'); ?></div>
		<?php if(isset($konten['link_video'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $konten['link_video']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:testimoni'); ?></div>
		<?php if(isset($konten['testimoni'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $konten['testimoni']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:uri_segment_string'); ?></div>
		<?php if(isset($konten['uri_segment_string'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $konten['uri_segment_string']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:created_by'); ?></div>
		<?php if(isset($konten['created_by'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $konten['created_by']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:created'); ?></div>
		<?php if(isset($konten['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($konten['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:updated'); ?></div>
		<?php if(isset($konten['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($konten['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($konten['created_by'], true); ?></div>
	</div>
</div>