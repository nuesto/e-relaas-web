<div class="page-header">
	<h1>
		<span><?php echo lang('landing_page:konten:'.$mode); ?></span>
	</h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id"><?php echo lang('landing_page:id'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id') != NULL){
					$value = $this->input->post('id');
				}elseif($mode == 'edit'){
					$value = $fields['id'];
				}
			?>
			<input name="id" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="title"><?php echo lang('landing_page:title'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('title') != NULL){
					$value = $this->input->post('title');
				}elseif($mode == 'edit'){
					$value = $fields['title'];
				}
			?>
			<input name="title" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_affiliate"><?php echo lang('landing_page:id_affiliate'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_affiliate') != NULL){
					$value = $this->input->post('id_affiliate');
				}elseif($mode == 'edit'){
					$value = $fields['id_affiliate'];
				}
			?>
			<input name="id_affiliate" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="konten"><?php echo lang('landing_page:konten'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('konten') != NULL){
					$value = $this->input->post('konten');
				}elseif($mode == 'edit'){
					$value = $fields['konten'];
				}
			?>
			<input name="konten" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_foto"><?php echo lang('landing_page:id_foto'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_foto') != NULL){
					$value = $this->input->post('id_foto');
				}elseif($mode == 'edit'){
					$value = $fields['id_foto'];
				}
			?>
			<input name="id_foto" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="link_video"><?php echo lang('landing_page:link_video'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('link_video') != NULL){
					$value = $this->input->post('link_video');
				}elseif($mode == 'edit'){
					$value = $fields['link_video'];
				}
			?>
			<input name="link_video" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="testimoni"><?php echo lang('landing_page:testimoni'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('testimoni') != NULL){
					$value = $this->input->post('testimoni');
				}elseif($mode == 'edit'){
					$value = $fields['testimoni'];
				}
			?>
			<input name="testimoni" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="uri_segment_string"><?php echo lang('landing_page:uri_segment_string'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('uri_segment_string') != NULL){
					$value = $this->input->post('uri_segment_string');
				}elseif($mode == 'edit'){
					$value = $fields['uri_segment_string'];
				}
			?>
			<input name="uri_segment_string" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="created_by"><?php echo lang('landing_page:created_by'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('created_by') != NULL){
					$value = $this->input->post('created_by');
				}elseif($mode == 'edit'){
					$value = $fields['created_by'];
				}
			?>
			<input name="created_by" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>