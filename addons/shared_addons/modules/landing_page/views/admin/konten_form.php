<style type="text/css">
	#no-result-affiliate {
		position: relative !important;
		display: inline-block;
		margin-left: 10px;
		margin-top: 3px;
	}
	.ui-widget-content {
		background: #fff;
	}
	.ui-menu .ui-menu-item a:hover, .ui-menu .ui-menu-item a.ui-state-focus, .ui-menu .ui-menu-item a.ui-state-active {
		background: rgb(237, 237, 237) none repeat scroll 0% 0%;
		color: rgb(68, 68, 68);
		border: #444;
	}
	.default-checkbox {
		padding-left: 20px;
	}
	.default-checkbox-testimoni {
		padding-left: 20px;
	}
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>streams_core/field_asset/js/video_url/video_url.js"></script>
<div class="page-header">
	<h1><?php echo lang('landing_page:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="title"><?php echo lang('landing_page:title'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('title') != NULL){
					$value = $this->input->post('title');
				}elseif($this->input->get('f-title') != NULL){
					$value = $this->input->get('f-title');
				}elseif($mode == 'edit'){
					$value = $fields['title'];
				}
			?>
			<input name="title" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="uri_segment_string"><?php echo lang('landing_page:uri_segment_string'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('uri_segment_string') != NULL){
					$value = $this->input->post('uri_segment_string');
				}elseif($this->input->get('f-uri_segment_string') != NULL){
					$value = $this->input->get('f-uri_segment_string');
				}elseif($mode == 'edit'){
					$value = $fields['uri_segment_string'];
				}
			?>
			<input name="uri_segment_string" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group" id="facebook_pixel_id">
		<label class="col-sm-2 control-label no-padding-right" for="facebook_pixel_id"><?php echo lang('landing_page:facebook_pixel_id'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('facebook_pixel_id') != NULL){
					$value = $this->input->post('facebook_pixel_id');
				}elseif($this->input->get('f-facebook_pixel_id') != NULL){
					$value = $this->input->get('f-facebook_pixel_id');
				}elseif($mode == 'edit'){
					$value = $fields['facebook_pixel_id'];
				}
			?>
			<input name="facebook_pixel_id" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" /><br>
			
		</div>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>
<script type="text/javascript" src="<?php echo base_url(); ?>system/cms/themes/ace/plugins/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var client = new ZeroClipboard( $(".copy-button") );

		client.on( "ready", function( readyEvent ) {
	  	// alert( "ZeroClipboard SWF is ready!" );

		client.on( "aftercopy", function( event ) {
			// `this` === `client`
			// `event.target` === the element that was clicked
			$(event.target).text('Link Copied');
			$(event.target).addClass('copied');
			$(event.target).removeClass('btn-info').addClass('btn-success');
			setTimeout(function() {
				$('.copied').text('Copy to clipboard');
				$('.copied').removeClass('btn-success').addClass('btn-info');
				$('.copied').removeClass('copied');
			},2000);
			} );
		} );
	});
</script>