<div class="page-header">
	<h1><?php echo lang('landing_page:konten:view'); ?></h1>
	<?php
	$show = false;
	if($show) {
	?>
	<div class="btn-group content-toolbar">
		<?php if(group_has_role('landing_page', 'view_all_konten')){ ?>		
		<a href="<?php echo site_url('admin/landing_page/konten/index'.$uri); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('landing_page:back') ?>
		</a>
		<?php } ?>

		<?php if(group_has_role('landing_page', 'edit_all_konten')){ ?>
			<a href="<?php echo site_url('admin/landing_page/konten/edit/'.$konten['id'].$uri); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('landing_page', 'edit_own_konten')){ ?>
			<?php if($konten['id_affiliate'] == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/landing_page/konten/edit/'.$konten['id'].$uri); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('landing_page', 'delete_all_konten')){ ?>
			<a href="<?php echo site_url('admin/landing_page/konten/delete/'.$konten['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('landing_page', 'delete_own_konten')){ ?>
			<?php if($konten['id_affiliate'] == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/landing_page/konten/delete/'.$konten['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
	<?php } ?>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">URL <?php echo lang('landing_page:konten:singular'); ?></div>
		<?php if(isset($konten['uri_segment_string'])){ ?>
		<div class="entry-value col-sm-8">
			<a target="_blank" href="<?php echo base_url().'affiliate/'.$konten['uri_segment_string']; ?>"><?php echo base_url().'affiliate/'.$konten['uri_segment_string']; ?></a>
			<span class="btn btn-xs btn-info copy-button" data-clipboard-text="<?php echo base_url().'affiliate/'.$konten['uri_segment_string']; ?>" title="Copy to clipboard">Copy to clipboard</span>
		</div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	<?php if($show) { ?>
	<h3><?php echo lang('landing_page:konten'); ?></h3>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:title'); ?></div>
		<?php if(isset($konten['title'])){ ?>
		<div class="entry-value col-sm-8"><?php echo ($konten['title']=='default') ? $default_konten['title'] : $konten['title']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:headline'); ?></div>
		<?php if(isset($konten['headline'])){ ?>
		<div class="entry-value col-sm-8"><?php echo ($konten['headline']=='default') ? $default_konten['headline'] : $konten['headline']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:secondary_headline'); ?></div>
		<?php if(isset($konten['secondary_headline'])){ ?>
		<div class="entry-value col-sm-8"><?php echo ($konten['secondary_headline']=='default') ? $default_konten['secondary_headline'] : $konten['secondary_headline']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:deskripsi'); ?></div>
		<?php if(isset($konten['deskripsi'])){ ?>
		<div class="entry-value col-sm-8"><?php echo ($konten['deskripsi']=='default') ? $default_konten['deskripsi'] : $konten['deskripsi']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:call_to_action'); ?></div>
		<?php if(isset($konten['call_to_action'])){ ?>
		<div class="entry-value col-sm-8"><?php echo ($konten['call_to_action']=='default') ? $default_konten['call_to_action'] : $konten['call_to_action']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<?php if($konten['file_type']=='image') { ?>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:id_foto'); ?></div>
		<?php if(isset($konten['id_foto'])){ ?>
		<div class="entry-value col-sm-8">
			<?php $id_foto = ($konten['id_foto']=='default') ? $default_konten['id_foto'] : $konten['id_foto']; ?>
			<a target="_blank" href="<?php echo base_url(); ?>files/large/<?php echo $id_foto; ?>"><img src="<?php echo base_url(); ?>files/large/<?php echo $id_foto; ?>/" style="display:block; width:200px; height:auto; margin-bottom:10px;" /></a>
		</div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	<?php } else { ?>
	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('landing_page:link_video'); ?></div>
		<?php if(isset($konten['link_video'])){ 
			$video = json_decode($konten['link_video'],true);
			?>
		<div class="entry-value col-sm-8" style="width:400px;height:300px;"><?php echo html_entity_decode($video['html']); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	<?php } ?>

	<h3><?php echo lang('landing_page:testimoni'); ?></h3>

	<div class="table-responsive" id="testimoni-table">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th><?php echo lang('landing_page:id_foto'); ?></th>
					<th><?php echo lang('landing_page:nama'); ?></th>
					<th><?php echo lang('landing_page:pekerjaan'); ?></th>
					<th><?php echo lang('landing_page:deskripsi'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
				$testimoni = $konten['testimoni'];
				?>
				<?php foreach($testimoni as $k => $t) { ?>
				<?php if($t['nama'] != '') { ?>
				<tr class="item">
					<td style="width:250px;">
						<?php if($t['id_foto'] != ''){ ?>
						<img src="<?php echo base_url(); ?>files/large/<?php echo $t['id_foto']; ?>/" style="display:block; width:200px; height:auto; margin-bottom:10px;" />
						<?php } ?>
					</td>
					<td><?php echo $t['nama']; ?></td>
					<td><?php echo $t['pekerjaan']; ?></td>
					<td><?php echo $t['testimoni']; ?></td>
				</tr>
				<?php } ?>
				<?php } ?>
				
				<?php
				if($konten['show_default_testimoni']==1) {
				?>
				<?php foreach($default_konten['testimoni'] as $k => $t) { ?>
				<tr class="item">
					<td style="width:250px;">
						<?php if($t['id_foto'] != ''){ ?>
						<img src="<?php echo base_url(); ?>files/large/<?php echo $t['id_foto']; ?>/" style="display:block; width:200px; height:auto; margin-bottom:10px;" />
						<?php } ?>
					</td>
					<td><?php echo $t['nama']; ?></td>
					<td><?php echo $t['pekerjaan']; ?></td>
					<td><?php echo $t['testimoni']; ?></td>
				</tr>
				<?php } ?>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<?php } ?>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>system/cms/themes/ace/plugins/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript">
	var client = new ZeroClipboard( $(".copy-button") );

	client.on( "ready", function( readyEvent ) {
  	// alert( "ZeroClipboard SWF is ready!" );

	client.on( "aftercopy", function( event ) {
		// `this` === `client`
		// `event.target` === the element that was clicked
		$(event.target).before('<span class="label label-success copied">Link Copied</span>');
		setTimeout(function() {
			$('.copied').fadeOut('slow');
		},1000);
		} );
	} );
</script>