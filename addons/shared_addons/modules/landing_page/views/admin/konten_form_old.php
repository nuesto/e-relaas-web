<style type="text/css">
	#no-result-affiliate {
		position: relative !important;
		display: inline-block;
		margin-left: 10px;
		margin-top: 3px;
	}
	.ui-widget-content {
		background: #fff;
	}
	.ui-menu .ui-menu-item a:hover, .ui-menu .ui-menu-item a.ui-state-focus, .ui-menu .ui-menu-item a.ui-state-active {
		background: rgb(237, 237, 237) none repeat scroll 0% 0%;
		color: rgb(68, 68, 68);
		border: #444;
	}
	.default-checkbox {
		padding-left: 20px;
	}
	.default-checkbox-testimoni {
		padding-left: 20px;
	}
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>streams_core/field_asset/js/video_url/video_url.js"></script>
<div class="page-header">
	<h1><?php echo lang('landing_page:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">
	<h3 class="header smaller lighter"><?php echo lang('landing_page:konten'); ?></h3>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="url"><?php echo lang('landing_page:konten:url'); ?></label>
		<?php if(isset($fields['uri_segment_string'])){ ?>
		<div class="col-sm-10">
			<a target="_blank" href="<?php echo base_url().'affiliate/'.$fields['uri_segment_string']; ?>"><?php echo base_url().'affiliate/'.$fields['uri_segment_string']; ?></a>
			<span class="btn btn-xs btn-info copy-button" data-clipboard-text="<?php echo base_url().'affiliate/'.$fields['uri_segment_string']; ?>" title="Copy to clipboard">Copy to clipboard</span>
		</div>
		<?php }else{ ?>
		<div class="col-sm-10">-</div>
		<?php } ?>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="url_checkout"><?php echo lang('landing_page:konten:url_checkout'); ?><br><span style="font-size: 12px;">(Produk : Membership SB-DKK)</span></label>
		<?php if(isset($fields['uri_segment_string'])){ ?>
		<div class="col-sm-10">
			<a target="_blank" href="<?php echo base_url().'commerce/cart/add?product='.$default_product['id'].'&affiliate='.$fields['uri_segment_string'].'&product_type=membership'; ?>"><?php echo base_url().'commerce/cart/add?product='.$default_product['id'].'&affiliate='.$fields['uri_segment_string'].'&product_type=membership'; ?></a>
			<span class="btn btn-xs btn-info copy-button" data-clipboard-text="<?php echo base_url().'commerce/cart/add?product='.$default_product['id'].'&affiliate='.$fields['uri_segment_string'].'&product_type=membership'; ?>" title="Copy to clipboard">Copy to clipboard</span>
		</div>
		<?php }else{ ?>
		<div class="col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="landing_page"><?php echo lang('landing_page:konten:singular'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('landing_page') != NULL){
					$value = $this->input->post('landing_page');
				}elseif($this->input->get('f-landing_page') != NULL){
					$value = $this->input->get('f-landing_page');
				}elseif($mode == 'edit'){
					$value = ($fields['redirect_to']!='') ? 'redirect_to' : 'default';
				}
			?>
			<select class="col-xs-10 col-sm-5" name="landing_page">
				<option <?php echo ($value=='default') ? 'selected="selected"' : ''; ?> value="default">Gunakan Default</option>
				<option <?php echo ($value=='redirect_to') ? 'selected="selected"' : ''; ?> value="redirect_to">Redirect ke halaman/website lain</option>
			</select>
			
		</div>
	</div>

	<div class="form-group" id="redirect_to" style="display:none">
		<label class="col-sm-2 control-label no-padding-right" for="redirect_to"><?php echo lang('landing_page:redirect_to'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('redirect_to') != NULL){
					$value = $this->input->post('redirect_to');
				}elseif($this->input->get('f-redirect_to') != NULL){
					$value = $this->input->get('f-redirect_to');
				}elseif($mode == 'edit'){
					$value = $fields['redirect_to'];
				}
			?>
			<input name="redirect_to" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" /><br>
			
		</div>
	</div>

	<div class="form-group" id="facebook_pixel_id">
		<label class="col-sm-2 control-label no-padding-right" for="facebook_pixel_id"><?php echo lang('landing_page:facebook_pixel_id'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('facebook_pixel_id') != NULL){
					$value = $this->input->post('facebook_pixel_id');
				}elseif($this->input->get('f-facebook_pixel_id') != NULL){
					$value = $this->input->get('f-facebook_pixel_id');
				}elseif($mode == 'edit'){
					$value = $fields['facebook_pixel_id'];
				}
			?>
			<input name="facebook_pixel_id" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" /><br>
			
		</div>
	</div>

	<?php
	$show = false;
	if($show) {
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="headline"><?php echo lang('landing_page:headline'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('headline') != NULL){
					$value = $this->input->post('headline');
				}elseif($this->input->get('f-headline') != NULL){
					$value = $this->input->get('f-headline');
				}elseif($mode == 'edit'){
					$value = $fields['headline'];
				}

				$default_checked = '';
				if($value=='default') {
					$value = $default_konten['headline'];
					$default_checked = 'checked="checked"';
				}
			?>
			<input name="headline" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-10" id="" /><br>
			<div class="col-xs-10 col-sm-10 default-checkbox">
				<div class="checkbox">
					<label>
						<input <?php echo $default_checked; ?> type="checkbox" name="default_headline" value="<?php echo $default_konten['headline']; ?>"> Gunakan konten default 
					</label>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="secondary_headline"><?php echo lang('landing_page:secondary_headline'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('secondary_headline') != NULL){
					$value = $this->input->post('secondary_headline');
				}elseif($this->input->get('f-secondary_headline') != NULL){
					$value = $this->input->get('f-secondary_headline');
				}elseif($mode == 'edit'){
					$value = $fields['secondary_headline'];
				}

				$default_checked = '';
				if($value=='default') {
					$value = $default_konten['secondary_headline'];
					$default_checked = 'checked="checked"';
				}
			?>
			<input name="secondary_headline" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-10" id="" />
			<div class="col-xs-10 col-sm-10 default-checkbox">
				<div class="checkbox">
					<label>
						<input <?php echo $default_checked; ?> type="checkbox" name="default_secondary_headline" value="<?php echo $default_konten['secondary_headline']; ?>"> Gunakan konten default 
					</label>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="deskripsi"><?php echo lang('landing_page:deskripsi'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('deskripsi') != NULL){
					$value = $this->input->post('deskripsi');
				}elseif($this->input->get('f-deskripsi') != NULL){
					$value = $this->input->get('f-deskripsi');
				}elseif($mode == 'edit'){
					$value = $fields['deskripsi'];
				}

				$default_checked = '';
				if($value=='default') {
					$value = $default_konten['deskripsi'];
					$default_checked = 'checked="checked"';
				}
			?>
			<input name="deskripsi" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-10" id="" />
			<div class="col-xs-10 col-sm-10 default-checkbox">
				<div class="checkbox">
					<label>
						<input <?php echo $default_checked; ?> type="checkbox" name="default_deskripsi" value="<?php echo $default_konten['deskripsi']; ?>"> Gunakan konten default 
					</label>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="call_to_action"><?php echo lang('landing_page:call_to_action'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('call_to_action') != NULL){
					$value = $this->input->post('call_to_action');
				}elseif($this->input->get('f-call_to_action') != NULL){
					$value = $this->input->get('f-call_to_action');
				}elseif($mode == 'edit'){
					$value = $fields['call_to_action'];
				}

				$default_checked = '';
				if($value=='default') {
					$value = $default_konten['call_to_action'];
					$default_checked = 'checked="checked"';
				}
			?>
			<input name="call_to_action" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			<div class="col-xs-10 col-sm-10 default-checkbox">
				<div class="checkbox">
					<label>
						<input <?php echo $default_checked; ?> type="checkbox" name="default_call_to_action" value="<?php echo $default_konten['call_to_action']; ?>"> Gunakan konten default 
					</label>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="file_type"><?php echo lang('landing_page:file_type'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('file_type') != NULL){
					$value = $this->input->post('file_type');
				}elseif($this->input->get('f-file_type') != NULL){
					$value = $this->input->get('f-file_type');
				}elseif($mode == 'edit'){
					$value = $fields['file_type'];
				}
			?>
			<div class="radio">
				<label>
					<input <?php echo ($value == NULL OR $value == 'image') ? 'checked="checked"' : ''; ?> name="file_type" class="ace" type="radio" value="image">
					<span class="lbl"> <?php echo lang('landing_page:id_foto'); ?></span>
				</label>
			</div>
			<div class="radio">
				<label>
					<input disabled="disabled" <?php echo ($value == 'video') ? 'checked="checked"' : ''; ?> name="file_type" class="ace" type="radio" value="video">
					<span class="lbl"> <?php echo lang('landing_page:link_video'); ?></span>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group file_type" data-type="image">
		<label class="col-sm-2 control-label no-padding-right" for="id_foto"><?php echo lang('landing_page:id_foto'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_id_foto') != NULL){
					$value = $this->input->post('id_id_foto');
				}elseif($this->input->get('f-id_foto') != NULL){
					$value = $this->input->get('f-id_foto');
				}elseif($mode == 'edit'){
					$value = $fields['id_foto'];
				}

				$default_checked = '';
				if($value=='default') {
					$value = $default_konten['id_foto'];
					$default_checked = 'checked="checked"';
				}
			?>
			<div class="row">
				<div class="col-xs-10 col-sm-5" style="padding-right:0px;">
					<?php if(!empty($value)){ ?>
					<img src="<?php echo base_url(); ?>files/large/<?php echo $value; ?>/" style="display:block; width:200px; height:auto; margin-bottom:10px;" />
					<?php } ?>
					<input type="hidden" name="id_id_foto" value="<?php echo $value; ?>">
					<input class="file-input" id="id_foto" type="file" name="id_foto">
				</div>
				<div class="col-xs-10 col-sm-10 default-checkbox" style="padding-left:30px;">
					<div class="checkbox">
						<label>
							<input <?php echo $default_checked; ?> type="checkbox" name="default_id_foto" value="<?php echo $default_konten['id_foto']; ?>"> Gunakan konten default 
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group file_type" data-type="video">
		<label class="col-sm-2 control-label no-padding-right" for="link_video"><?php echo lang('landing_page:link_video'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('link_video') != NULL){
					$value = $this->input->post('link_video');
				}elseif($this->input->get('f-link_video') != NULL){
					$value = $this->input->get('f-link_video');
				}elseif($mode == 'edit'){
					$video_url = json_decode($fields['link_video'], true);;
					$value = $video_url['url'];
				}
			?>
			
			<input name="link_video" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="video_url" data-fieldtype="video_url" data-video-width="100%" data-video-height="100%" data-video-autoplay="0" placeholder="Copy and paste the video url from youtube" type="text"><div class="clearfix"></div>
			<div class="preview_video_url" style="display: none; margin-top:15px;">
			    <div class="iframe-preview"></div>
			    
				<input name="video_url_url" value="" type="hidden">
			</div>
		</div>
	</div>
	
	<?php if(isset($fields['uri_segment_string']) AND $fields['uri_segment_string']=='default') {
		echo form_hidden('uri_segment_string',$fields['uri_segment_string']);
	} else {?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="uri_segment_string"><?php echo lang('landing_page:uri_segment_string'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('uri_segment_string') != NULL){
					$value = $this->input->post('uri_segment_string');
				}elseif($this->input->get('f-uri_segment_string') != NULL){
					$value = $this->input->get('f-uri_segment_string');
				}elseif($mode == 'edit'){
					$value = $fields['uri_segment_string'];
				}
			?>
			<input name="uri_segment_string" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>
	<?php } ?>
	<?php } ?>

	<?php if($mode=='new') { ?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_affiliate"><?php echo lang('landing_page:id_affiliate'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_affiliate') != NULL){
					$value = $this->input->post('id_affiliate');
				}elseif($this->input->get('f-id_affiliate') != NULL){
					$value = $this->input->get('f-id_affiliate');
				}elseif($mode == 'edit'){
					$value = $fields['id_affiliate'];
				}
			?>
			<input id="affiliate" type="text" value="" class="col-xs-10 col-sm-5" id="" />
			<input name="id_affiliate" type="hidden" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			<span id="no-result-affiliate"></span>
		</div>
	</div>
	<?php } else {
		echo form_hidden('id_affiliate',$fields['id_affiliate']);
	} ?>

	<?php
	if($show) {
	?>
	<h3 class="header smaller lighter"><?php echo lang('landing_page:testimoni'); ?></h3>

	<div class="table-responsive" id="testimoni-table">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th style="width:250px;"><?php echo lang('landing_page:id_foto'); ?></th>
					<th><?php echo lang('landing_page:nama'); ?></th>
					<th><?php echo lang('landing_page:pekerjaan'); ?></th>
					<th><?php echo lang('landing_page:deskripsi'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
				$testimoni = array(array('id_foto'=>'','nama'=>'','pekerjaan'=>'','testimoni'=>''));
				if($this->input->post('testimoni') != null) {
					for ($i=0; $i < count($this->input->post('testimoni')); $i++) { 
						if($this->input->post('id_id_foto') != NULL){
							$testimoni[$i]['id_foto'] = $this->input->post('id_foto')[$i];
						}
						if($this->input->post('nama')[$i] != NULL){
							$testimoni[$i]['nama'] = $this->input->post('nama')[$i];
						}
						if($this->input->post('pekerjaan')[$i] != NULL){
							$testimoni[$i]['pekerjaan'] = $this->input->post('pekerjaan')[$i];
						}
						if($this->input->post('testimoni')[$i] != NULL){
							$testimoni[$i]['testimoni'] = $this->input->post('testimoni')[$i];
						}
					}
				} elseif($mode=='edit') {
					$testimoni = $fields['testimoni'];
				}
				?>
				<?php foreach($testimoni as $k => $t) { ?>
				<tr class="item">
					<td style="width:250px;">
						<input class="file-input" type="file" name="foto[]">
						<?php if($t['id_foto'] != ''){ ?>
						<img src="<?php echo base_url(); ?>files/large/<?php echo $t['id_foto']; ?>/" style="display:block; width:200px; height:auto; margin-bottom:10px;" />
						<?php } ?>
						<input type="hidden" name="id_foto[]" value="<?php echo $t['id_foto']; ?>">
					</td>
					<td><input type="text" name="nama[]" value="<?php echo $t['nama']; ?>"></td>
					<td><input type="text" name="pekerjaan[]" value="<?php echo $t['pekerjaan']; ?>"></td>
					<td><textarea name="testimoni[]"><?php echo $t['testimoni']; ?></textarea></td>
					<td>
					
						<span class="btn btn-sm btn-danger remove-testimoni">hapus</span>
					
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<span class="btn btn-success" id="add-testimoni">Tambah Testimoni</span>
	<div class="default-checkbox-testimoni">
		<?php
		$value = 0;
		if($this->input->post('default_testimoni') != NULL){
			$value = $this->input->post('default_testimoni');
		}elseif($mode == 'edit'){
			$value = $fields['show_default_testimoni'];
		}

		$default_checked = '';
		if($value=='1') {
			$value = '1';
			$default_checked = 'checked="checked"';
		}
		?>
		<div class="checkbox">
			<label>
				<input <?php echo $default_checked; ?> type="checkbox" name="default_testimoni" value="1"> Tampilkan testimoni default
			</label>
		</div>
	</div>
	<div class="table-responsive" id="default-testimoni-table" style="display:none">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th style="width:250px;"><?php echo lang('landing_page:id_foto'); ?></th>
					<th><?php echo lang('landing_page:nama'); ?></th>
					<th><?php echo lang('landing_page:pekerjaan'); ?></th>
					<th><?php echo lang('landing_page:deskripsi'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
				$testimoni = $default_konten['testimoni'];
				?>
				<?php foreach($testimoni as $k => $t) { ?>
				<tr class="item">
					<td style="width:250px;">
						<?php if($t['id_foto'] != ''){ ?>
						<img src="<?php echo base_url(); ?>files/large/<?php echo $t['id_foto']; ?>/" style="display:block; width:200px; height:auto; margin-bottom:10px;" />
						<?php } ?>
					</td>
					<td><?php echo $t['nama']; ?></td>
					<td><?php echo $t['pekerjaan']; ?></td>
					<td><?php echo $t['testimoni']; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<?php } ?>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>
<script type="text/javascript" src="<?php echo base_url(); ?>system/cms/themes/ace/plugins/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var client = new ZeroClipboard( $(".copy-button") );

		client.on( "ready", function( readyEvent ) {
	  	// alert( "ZeroClipboard SWF is ready!" );

		client.on( "aftercopy", function( event ) {
			// `this` === `client`
			// `event.target` === the element that was clicked
			$(event.target).text('Link Copied');
			$(event.target).addClass('copied');
			$(event.target).removeClass('btn-info').addClass('btn-success');
			setTimeout(function() {
				$('.copied').text('Copy to clipboard');
				$('.copied').removeClass('btn-success').addClass('btn-info');
				$('.copied').removeClass('copied');
			},2000);
			} );
		} );


		if($('select[name=landing_page]').val()=='default') {
			$('#redirect_to').slideUp();
		} else {
			$('#redirect_to').slideDown();
		}

		$('select[name=landing_page]').change(function() {
			var choosed = $(this).val();
			if(choosed=='default') {
				$('#redirect_to').slideUp();
			} else {
				$('#redirect_to').slideDown();
			}
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		hide_tipe_file();
		$('input[type=radio][name=file_type]').change(function() {
			hide_tipe_file();
		});

		$('.file-input').ace_file_input({
			no_file:'No File ...',
			btn_choose:'Choose',
			btn_change:'Change',
			droppable:false,
			onchange:null,
			thumbnail:false, //| true | large
			whitelist:'gif|png|jpg|jpeg'
		});

		$('#video_url').keyup(function() {
			if($(this).val()=='') {
				$('.preview_video_url .iframe-preview').html('');
				$('.preview_video_url input[name=video_url_url]').val('');
			}
		});

		var length = $('.item').length;
		if(length == 1) {
			$('.item:first td:last .btn-danger').remove();
		}
	});
	$('#add-testimoni').click(function() {
		var row_html = '<td><input class="file-input" type="file" name="foto[]"></td>'
					+'<td><input type="text" name="nama[]"></td>'
					+'<td><input type="text" name="pekerjaan[]"></td>'
					+'<td><textarea name="testimoni[]"></textarea></td>'
					+'<td><span class="btn btn-sm btn-danger remove-testimoni">hapus</span></td>';
		var length = $('.item').length;

		$('#testimoni-table table tbody').append('<tr class="item">'+row_html+'</tr>');
		$('.item:first td:last').html('<span class="btn btn-sm btn-danger remove-testimoni">hapus</span>');

		$('.file-input').ace_file_input({
			no_file:'No File ...',
			btn_choose:'Choose',
			btn_change:'Change',
			droppable:false,
			onchange:null,
			thumbnail:false, //| true | large
			whitelist:'gif|png|jpg|jpeg'
		});
	});

	$('#testimoni-table').on('click','.remove-testimoni',function() {
		var item = $(this);
		bootbox.confirm("Anda yakin akan menjalankan aksi ini? Aksi ini tidak dapat dikembalikan.", function(result) {
			if(result) {
				item.closest('tr').remove();
				var length = $('.item').length;
				if(length == 1) {
					$('.item:first td:last .btn-danger').remove();
				}
			}
		});
	});

	function hide_tipe_file() {
		if($('input[type=radio][name=file_type]:checked').val()=='image') {
			$('.file_type[data-type=image]').show();
			$('.file_type[data-type=video]').hide();
		} else {
			$('.file_type[data-type=image]').hide();
			$('.file_type[data-type=video]').show();
		}
	}

	$(document).ready(function() {
		$('.default-checkbox').on('click',function(e){
			if(e.target.tagName=='INPUT') {
				if($(this).find('input[type=checkbox]').is(':checked')) {
					var val = $(this).find('input[type=checkbox]').val();
					var name = $(this).find('input[type=checkbox]').attr('name');
					var target_name = name.substr(8);
					if(target_name=='id_foto') {
						target_name = 'id_id_foto';
					}
					$(this).closest('.form-group').find('input[name='+target_name+']').val(val);
				} else {
					$(this).closest('.form-group').find('input[name='+target_name+']').val('');
				}
			}
		});
		$('.default-checkbox-testimoni').on('click',function(e){
			if(e.target.tagName=='INPUT') {
				if($(this).find('input[type=checkbox]').is(':checked')) {
					$('#default-testimoni-table').fadeIn();
				} else {
					$('#default-testimoni-table').fadeOut();
				}
			}
		});

		if($('.default-checkbox-testimoni input[type=checkbox]').is(':checked')) {
			$('#default-testimoni-table').fadeIn();
		} else {
			$('#default-testimoni-table').fadeOut();
		}
	});

	$(document).ready(function() {
		var affiliate = $('input[name=id_affiliate]');
		if(affiliate.length!=0) {
			if($('input[name=id_affiliate]').val() !== '') {
				$('#no-result-affiliate').html('<i class="fa fa-spin fa-refresh"></i>');
				$.ajax({
					url:"<?php echo base_url(); ?>admin/landing_page/konten/get_affiliate",
					data:'term='+$('input[name=id_affiliate]').val(),
					dataType:'JSON',
					success: function(res){
						if(res.length>0) {
							$.each(res,function(k,item) {
								$('#affiliate').val(item.label);
								$('input[name=id_affiliate]').val(item.value);
								$('#no-result-affiliate').empty();
							});
						} else {
							$('#no-result-affiliate').text('Affiliate tidak ditemukan');
						}
					}
				})
			}
			$('#affiliate').keyup(function() {
				if($(this).length==0) {
					$('input[name=id_affiliate]').val('');
				}
			});
			$('#affiliate').autocomplete({
				source : "<?php echo base_url(); ?>admin/landing_page/konten/get_affiliate",
				minLength : 3,
				response: function(event, ui) {
					if(!ui.content.length) {
						$('#no-result-affiliate').text('Affiliate tidak ditemukan');
					} else {
						$('#no-result-affiliate').empty();
					}
				},
				select : function( event, ui ) {
					event.preventDefault();
					if(ui.item === null) {
						$('input[name=id_affiliate]').val('');
					} else {
						$('#affiliate').val(ui.item.label);
						$('input[name=id_affiliate]').val(ui.item.value);
					}
				},
				focus : function( event, ui ) {
					event.preventDefault();
					if(ui.item === null) {
						$('input[name=id_affiliate]').val('');
					} else {
						$('#affiliate').val(ui.item.label);
						$('input[name=id_affiliate]').val(ui.item.value);
					}
				},
				search : function( event, ui ) {
					$('#no-result-affiliate').html('<i class="fa fa-spin fa-refresh"></i>');
				},
				change : function( event, ui ) {
					event.preventDefault();
					if(ui.item === null) {
						$('input[name=id_affiliate]').val('');
					}
				}
			}).data('ui-autocomplete')._renderItem = function(ul,item) {
					var state = '';
					var state_message = '';
					if(item.landing_page != 0){
						return $('<li class="ui-state-disabled"><a>'+item.label+' (Sudah memiliki landing page)</a>').appendTo(ul);
					}else{
						return $("<li>")
						.append("<a>" + item.label + "</a>")
						.appendTo(ul);
					}
			};
		}
	});
</script>