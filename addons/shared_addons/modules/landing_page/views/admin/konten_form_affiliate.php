<style type="text/css">
	#no-result-affiliate {
		position: relative !important;
		display: inline-block;
		margin-left: 10px;
		margin-top: 3px;
	}
	.ui-widget-content {
		background: #fff;
	}
	.ui-menu .ui-menu-item a:hover, .ui-menu .ui-menu-item a.ui-state-focus, .ui-menu .ui-menu-item a.ui-state-active {
		background: rgb(237, 237, 237) none repeat scroll 0% 0%;
		color: rgb(68, 68, 68);
		border: #444;
	}
	.default-checkbox {
		padding-left: 20px;
	}
	.default-checkbox-testimoni {
		padding-left: 20px;
	}
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>streams_core/field_asset/js/video_url/video_url.js"></script>
<div class="page-header">
	<h1><?php echo lang('landing_page:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>
<div class="row">
	<div class="col-sm-10">
		<h4><?php echo lang('landing_page:konten:url'); ?></h4>
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th><?php echo lang('landing_page:title'); ?></th>
						<th><?php echo lang('landing_page:uri_segment_string'); ?></th>
						<th><?php echo lang('landing_page:redirect_to'); ?></th>
						<th>Facebook Pixel ID</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($fields as $key => $konten_entry): ?>
						<tr>
							<td><?php echo $konten_entry['title']; echo form_hidden('id_landing_page[]',$konten_entry['id']) ?></td>
							<td>
								<a target="_blank" href="<?php echo site_url($konten_entry['uri_segment_string'].'/affiliate/'.$konten_entry['affiliate_uri']); ?>"><?php echo site_url($konten_entry['uri_segment_string'].'/affiliate/'.$konten_entry['affiliate_uri']); ?></a>
								<span class="btn btn-xs btn-info copy-button" data-clipboard-text="<?php echo site_url($konten_entry['uri_segment_string'].'/affiliate/'.$konten_entry['affiliate_uri']); ?>" title="Copy to clipboard">Copy to clipboard</span>
							</td>
							<td>
								<?php 
								$value = NULL;
								if($this->input->post('landing_page') != NULL){
									$value = $this->input->post('landing_page')[$key];
								}elseif($this->input->get('f-landing_page') != NULL){
									$value = $this->input->get('f-landing_page');
								}elseif($mode == 'edit'){
									$value = ($konten_entry['affiliate_redirect']!='') ? 'redirect_to' : 'default';
								}
								?>
								<select class="form-control landing_page" name="landing_page[]">
									<option <?php echo ($value=='default') ? 'selected="selected"' : ''; ?> value="default">Gunakan Default</option>
									<option <?php echo ($value=='redirect_to') ? 'selected="selected"' : ''; ?> value="redirect_to">Redirect ke halaman/website lain</option>
								</select><br>

								<?php 
								$value = NULL;
								if($this->input->post('redirect_to') != NULL){
									$value = $this->input->post('redirect_to')[$key];
								}elseif($this->input->get('f-redirect_to') != NULL){
									$value = $this->input->get('f-redirect_to');
								}elseif($mode == 'edit'){
									$value = $konten_entry['affiliate_redirect'];
								}
								?>
								<input name="redirect_to[]" type="text" value="<?php echo $value; ?>" class="form-control redirect_to" id="" style="display:none"/>
							</td>
							<td>
								<?php 
								$value = NULL;
								if($this->input->post('facebook_pixel_id') != NULL){
									$value = $this->input->post('facebook_pixel_id')[$key];
								}elseif($this->input->get('f-facebook_pixel_id') != NULL){
									$value = $this->input->get('f-facebook_pixel_id');
								}elseif($mode == 'edit'){
									$value = $konten_entry['affiliate_facebook_pixel'];
								}
								?>
								<input name="facebook_pixel_id[]" type="text" value="<?php echo $value; ?>" class="form-control" id="" />
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-10">
		<div class="text-center">
			<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
			<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-10">
		<h4><?php echo lang('landing_page:konten:url_checkout'); ?></h4>
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>Nama Produk</th>
						<th>URL</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($products as $product) : ?>
						<tr>
							<td><?php echo $product['nama']; ?></td>
							<td>
								<a target="_blank" href="<?php echo base_url().'commerce/cart/add?product='. $product['id'].'&affiliate='.$this->current_user->id.'&product_type='.$product['type']; ?>"><?php echo base_url().'commerce/cart/add?product='. $product['id'].'&affiliate='.$this->current_user->id.'&product_type='.$product['type']; ?></a>
								<span class="btn btn-xs btn-info copy-button" data-clipboard-text="<?php echo base_url().'commerce/cart/add?product='. $product['id'].'&affiliate='.$this->current_user->id.'&product_type='.$product['type']; ?>" title="Copy to clipboard">Copy to clipboard</span>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php echo form_close();?>
<script type="text/javascript" src="<?php echo base_url(); ?>system/cms/themes/ace/plugins/zeroclipboard/ZeroClipboard.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var client = new ZeroClipboard( $(".copy-button") );

		client.on( "ready", function( readyEvent ) {
	  	// alert( "ZeroClipboard SWF is ready!" );

		client.on( "aftercopy", function( event ) {
			// `this` === `client`
			// `event.target` === the element that was clicked
			$(event.target).text('Link Copied');
			$(event.target).addClass('copied');
			$(event.target).removeClass('btn-info').addClass('btn-success');
			setTimeout(function() {
				$('.copied').text('Copy to clipboard');
				$('.copied').removeClass('btn-success').addClass('btn-info');
				$('.copied').removeClass('copied');
			},2000);
			} );
		} );


		if($('select[name=landing_page]').val()=='default') {
			$('#redirect_to').slideUp();
		} else {
			$('#redirect_to').slideDown();
		}

		$('.landing_page').change(function() {
			var choosed = $(this).val();
			if(choosed=='default') {
				$(this).closest('td').find('.redirect_to').slideUp();
			} else {
				$(this).closest('td').find('.redirect_to').slideDown();
			}
		});
		$('.landing_page').trigger('change');
	});
</script>