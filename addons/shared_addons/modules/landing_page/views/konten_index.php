<div class="page-header">	<h1>		<span><?php echo lang('landing_page:konten:plural'); ?></span>	</h1>		<?php if(group_has_role('landing_page', 'create_konten')){ ?>	<div class="btn-group content-toolbar">		<a class="btn btn-default btn-sm" href="<?php echo site_url('landing_page/konten/create'.$uri); ?>">			<i class="icon-plus"></i>			<span class="no-text-shadow"><?php echo lang('landing_page:konten:new'); ?></span>		</a>	</div>	<?php } ?></div><fieldset>	<legend><?php echo lang('global:filters') ?></legend>		<?php echo form_open('', array('class' => 'form-inline', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>		<div class="form-group">
			<label><?php echo lang('landing_page:id'); ?>:&nbsp;</label>
			<input type="text" name="f-id" value="<?php echo $this->input->get('f-id'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('landing_page:title'); ?>:&nbsp;</label>
			<input type="text" name="f-title" value="<?php echo $this->input->get('f-title'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('landing_page:id_affiliate'); ?>:&nbsp;</label>
			<input type="text" name="f-id_affiliate" value="<?php echo $this->input->get('f-id_affiliate'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('landing_page:konten'); ?>:&nbsp;</label>
			<input type="text" name="f-konten" value="<?php echo $this->input->get('f-konten'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('landing_page:id_foto'); ?>:&nbsp;</label>
			<input type="text" name="f-id_foto" value="<?php echo $this->input->get('f-id_foto'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('landing_page:link_video'); ?>:&nbsp;</label>
			<input type="text" name="f-link_video" value="<?php echo $this->input->get('f-link_video'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('landing_page:testimoni'); ?>:&nbsp;</label>
			<input type="text" name="f-testimoni" value="<?php echo $this->input->get('f-testimoni'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('landing_page:uri_segment_string'); ?>:&nbsp;</label>
			<input type="text" name="f-uri_segment_string" value="<?php echo $this->input->get('f-uri_segment_string'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('landing_page:created_by'); ?>:&nbsp;</label>
			<input type="text" name="f-created_by" value="<?php echo $this->input->get('f-created_by'); ?>">
		</div>
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">			<i class="icon-ok"></i>			<?php echo lang('buttons:submit'); ?>		</button>				<button href="<?php echo current_url() . '#'; ?>" class="btn btn-danger btn-xs" type="reset">			<i class="icon-remove"></i>			<?php echo lang('buttons:clear'); ?>		</button>	<?php echo form_close() ?></fieldset><hr /><?php if ($konten['total'] > 0): ?>		<p class="pull-right"><?php echo lang('landing_page:showing').' '.count($konten['entries']).' '.lang('landing_page:of').' '.$konten['total'] ?></p>		<table class="table table-striped table-bordered table-hover">		<thead>			<tr>				<th>No</th>				<th><?php echo lang('landing_page:id'); ?></th>				<th><?php echo lang('landing_page:title'); ?></th>				<th><?php echo lang('landing_page:id_affiliate'); ?></th>				<th><?php echo lang('landing_page:konten'); ?></th>				<th><?php echo lang('landing_page:id_foto'); ?></th>				<th><?php echo lang('landing_page:link_video'); ?></th>				<th><?php echo lang('landing_page:testimoni'); ?></th>				<th><?php echo lang('landing_page:uri_segment_string'); ?></th>				<th><?php echo lang('landing_page:created_by'); ?></th>				<th><?php echo lang('landing_page:created'); ?></th>				<th><?php echo lang('landing_page:updated'); ?></th>				<th><?php echo lang('landing_page:created_by'); ?></th>				<th></th>			</tr>		</thead>		<tbody>			<?php 			$cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);			if($cur_page != 0){				$item_per_page = $pagination_config['per_page'];				$no = (($cur_page -1) * $item_per_page) + 1;			}else{				$no = 1;			}			?>						<?php foreach ($konten['entries'] as $konten_entry): ?>			<tr>				<td><?php echo $no; $no++; ?></td>				<td><?php echo $konten_entry['id']; ?></td>				<td><?php echo $konten_entry['title']; ?></td>				<td><?php echo $konten_entry['id_affiliate']; ?></td>				<td><?php echo $konten_entry['konten']; ?></td>				<td><?php echo $konten_entry['id_foto']; ?></td>				<td><?php echo $konten_entry['link_video']; ?></td>				<td><?php echo $konten_entry['testimoni']; ?></td>				<td><?php echo $konten_entry['uri_segment_string']; ?></td>				<td><?php echo $konten_entry['created_by']; ?></td>							<?php if($konten_entry['created_on']){ ?>				<td><?php echo format_date($konten_entry['created_on'], 'd-m-Y G:i'); ?></td>				<?php }else{ ?>				<td>-</td>				<?php } ?>								<?php if($konten_entry['updated_on']){ ?>				<td><?php echo format_date($konten_entry['updated_on'], 'd-m-Y G:i'); ?></td>				<?php }else{ ?>				<td>-</td>				<?php } ?>								<td><?php echo user_displayname($konten_entry['created_by'], true); ?></td>				<td class="actions">				<?php 				if(group_has_role('landing_page', 'view_all_konten')){					echo anchor('landing_page/konten/view/' . $konten_entry['id'].$uri, lang('global:view'), 'class="btn btn-xs btn-info view"');				}elseif(group_has_role('landing_page', 'view_own_konten')){					if($konten_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('landing_page/konten/view/' . $konten_entry['id'].$uri, lang('global:view'), 'class="btn btn-xs btn-info view"');					}				}				?>				<?php 				if(group_has_role('landing_page', 'edit_all_konten')){					echo anchor('landing_page/konten/edit/' . $konten_entry['id'].$uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');				}elseif(group_has_role('landing_page', 'edit_own_konten')){					if($konten_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('landing_page/konten/edit/' . $konten_entry['id'].$uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');					}				}				?>				<?php 				if(group_has_role('landing_page', 'delete_all_konten')){					echo anchor('landing_page/konten/delete/' . $konten_entry['id'].$uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));				}elseif(group_has_role('landing_page', 'delete_own_konten')){					if($konten_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('landing_page/konten/delete/' . $konten_entry['id'].$uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));					}				}				?>				</td>			</tr>			<?php endforeach; ?>		</tbody>	</table>		<?php echo $konten['pagination']; ?>	<?php else: ?>	<div class="well"><?php echo lang('landing_page:konten:no_entry'); ?></div><?php endif;?>