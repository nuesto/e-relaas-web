<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Landing Page Module
 *
 * Modul untuk me-manage landing page
 *
 */
class Landing_page_konten extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'konten';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('landing_page');
		
		$this->load->model('konten_m');
    }

    /**
	 * List all konten
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'view_all_konten') AND ! group_has_role('landing_page', 'view_own_konten')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'landing_page/konten/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->konten_m->count_all_konten();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['konten']['entries'] = $this->konten_m->get_konten($pagination_config);
		$data['konten']['total'] = count($data['konten']['entries']);
		$data['konten']['pagination'] = $this->pagination->create_links();

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('landing_page:konten:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('landing_page:konten:plural'))
			->build('konten_index', $data);
    }
	
	/**
     * Display one konten
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'view_all_konten') AND ! group_has_role('landing_page', 'view_own_konten')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['konten'] = $this->konten_m->get_konten_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('landing_page', 'view_all_konten')){
			if($data['konten']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('landing_page:konten:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('landing_page', 'view_all_konten') OR group_has_role('landing_page', 'view_own_konten')){
			$this->template->set_breadcrumb(lang('landing_page:konten:plural'), '/landing_page/konten/index');
		}

		$this->template->set_breadcrumb(lang('landing_page:konten:view'))
			->build('konten_entry', $data);
    }
	
	/**
     * Create a new konten entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'create_konten')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_konten('new')){	
				$this->session->set_flashdata('success', lang('landing_page:konten:submit_success'));				
				redirect('landing_page/konten/index');
			}else{
				$data['messages']['error'] = lang('landing_page:konten:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'landing_page/konten/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('landing_page:konten:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('landing_page', 'view_all_konten') OR group_has_role('landing_page', 'view_own_konten')){
			$this->template->set_breadcrumb(lang('landing_page:konten:plural'), '/landing_page/konten/index');
		}

		$this->template->set_breadcrumb(lang('landing_page:konten:new'))
			->build('konten_form', $data);
    }
	
	/**
     * Edit a konten entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the konten to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'edit_all_konten') AND ! group_has_role('landing_page', 'edit_own_konten')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('landing_page', 'edit_all_konten')){
			$entry = $this->konten_m->get_konten_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_konten('edit', $id)){	
				$this->session->set_flashdata('success', lang('landing_page:konten:submit_success'));				
				redirect('landing_page/konten/index');
			}else{
				$data['messages']['error'] = lang('landing_page:konten:submit_failure');
			}
		}
		
		$data['fields'] = $this->konten_m->get_konten_by_id($id);
		$data['mode'] = 'edit';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'landing_page/konten/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
       $this->template->title(lang('landing_page:konten:edit'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('landing_page', 'view_all_konten') OR group_has_role('landing_page', 'view_own_konten')){
			$this->template->set_breadcrumb(lang('landing_page:konten:plural'), '/landing_page/konten/index')
			->set_breadcrumb(lang('landing_page:konten:view'), '/landing_page/konten/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('landing_page:konten:plural'))
			->set_breadcrumb(lang('landing_page:konten:view'));
		}

		$this->template->set_breadcrumb(lang('landing_page:konten:edit'))
			->build('konten_form', $data);
    }
	
	/**
     * Delete a konten entry
     * 
     * @param   int [$id] The id of konten to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'delete_all_konten') AND ! group_has_role('landing_page', 'delete_own_konten')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('landing_page', 'delete_all_konten')){
			$entry = $this->konten_m->get_konten_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->konten_m->delete_konten_by_id($id);
		$this->session->set_flashdata('error', lang('landing_page:konten:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(5);
        redirect('landing_page/konten/index'.$data['uri']);
    }
	
	/**
     * Insert or update konten entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_konten($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('landing_page:konten:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->konten_m->insert_konten($values);
			}
			else
			{
				$result = $this->konten_m->update_konten($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}