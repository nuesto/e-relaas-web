<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Landing Page Module
 *
 * Modul untuk me-manage landing page
 *
 */
class Landing_page extends Public_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        if($this->uri->segment('2') != 'leads') {
	        redirect('landing_page/konten/index');
	    }
    }

}