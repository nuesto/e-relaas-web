<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Landing Page Module
 *
 * Modul untuk me-manage landing page
 *
 */
class Admin_leads extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'leads';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'access_leads_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('landing_page');		
		$this->load->model('leads_m');
    }

    /**
	 * List all leads
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'view_all_leads') AND ! group_has_role('landing_page', 'view_own_leads')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// -------------------------------------
		// If they don't have permission to view all,
		// redirect to view their own landing page
		// -------------------------------------

		if(! group_has_role('landing_page', 'view_all_leads')) {
			$landing_page = $this->leads_m->get_leads_by_id_affiliate($this->current_user->id);
			redirect('admin/landing_page/leads/view/'.$landing_page['id']);
		}

		// -------------------------------------
		// Filter
		// -------------------------------------
		$filter = NULL;
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'admin/landing_page/leads/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->leads_m->count_all_leads($filter);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		$data['leads']['entries'] = $this->leads_m->get_leads($pagination_config);
		$data['leads']['total'] = $pagination_config['total_rows'];
		$data['leads']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['leads']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('admin/landing_page/leads/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/landing_page/leads/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title('Waiting List')
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb('Waiting List')
			->build('admin/leads_index', $data);
    }
	
	// --------------------------------------------------------------------------

	function download() {
		$this->load->helper('download_excel');

		$data = $this->leads_m->get_leads();

		download_excel($data,'waiting_list_'.date('Y-m-d').'.xls');
	}

}