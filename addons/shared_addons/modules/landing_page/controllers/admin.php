<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Landing Page Module
 *
 * Modul untuk me-manage landing page
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('admin/landing_page/konten/index');
    }

}