<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Landing Page Module
 *
 * Modul untuk me-manage landing page
 *
 */
class Admin_konten extends Admin_Controller
{
	private $_uploaded;
	private $_files;
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'konten';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'access_konten_backend') AND $this->current_user->is_affiliate==0){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('landing_page');		
		$this->load->model('konten_m');
		$this->load->model('commerce/produk_m');
		$this->load->library('variables/variables');

		$this->load->library('files/files');
		if(isset($_FILES['foto']['name']) && !is_null($_FILES['foto']['name']))
			$this->_files = $_FILES['foto'];
    }

    /**
	 * List all konten
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'view_all_konten') AND ! group_has_role('landing_page', 'view_own_konten')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// -------------------------------------
		// If they don't have permission to view all,
		// redirect to view their own landing page
		// -------------------------------------

		if(! group_has_role('landing_page', 'view_all_konten')) {
			$landing_page = $this->konten_m->get_konten_by_id_affiliate($this->current_user->id);
			if($this->session->flashdata('success')) {
				$this->session->set_flashdata('success', lang('landing_page:konten:submit_success'));
			}
			redirect('admin/landing_page/konten/edit/'.$landing_page['id']);
		}

		// -------------------------------------
		// Filter
		// -------------------------------------
		// clear id_affiliate if affiliate is empty
		if($this->input->get('id_affiliate')) {
			$filter['id_affiliate'] = $this->input->get('id_affiliate');
		}
		if(!$this->input->get('affiliate')) {
			$_GET['id_affiliate'] = '';
			$filter = NULL;
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'admin/landing_page/konten/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->konten_m->count_all_konten($filter);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		$data['konten']['entries'] = $this->konten_m->get_konten($pagination_config,$filter);
		$data['konten']['total'] = $pagination_config['total_rows'];
		$data['konten']['pagination'] = $this->pagination->create_links();

		$data['default_product'] = $this->produk_m->get_produk_by_id($this->variables->__get('default_id_product'));

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['konten']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('admin/landing_page/konten/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/landing_page/konten/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('landing_page:konten:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('landing_page:konten:plural'))
			->build('admin/konten_index', $data);
    }
	
	/**
     * Display one konten
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'view_all_konten') AND ! group_has_role('landing_page', 'view_own_konten')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------

		// get default konten for landing page
		$landing_page = $this->konten_m->get_konten_by_uri('default');
		$konten = json_decode($landing_page['konten'],true);
		$testimoni = json_decode($landing_page['testimoni'],true);
		$data['default_konten'] = $konten;
		$data['default_konten']['id'] = $landing_page['id'];
		$data['default_konten']['title'] = $konten['headline'];
		$data['default_konten']['file_type'] = $konten['file_type'];
		$data['default_konten']['id_foto'] = $landing_page['id_foto'];
		$data['default_konten']['link_video'] = $landing_page['link_video'];
		$data['default_konten']['uri_segment_string'] = $landing_page['uri_segment_string'];
		$data['default_konten']['id_affiliate'] = $landing_page['id_affiliate'];
		$data['default_konten']['testimoni'] = $testimoni;
		
        $landing_page = $this->konten_m->get_konten_by_id($id);
		$konten = json_decode($landing_page['konten'],true);
		$testimoni = json_decode($landing_page['testimoni'],true);
		$data['konten'] = $konten;
		$data['konten']['id'] = $landing_page['id'];
		$data['konten']['title'] = $konten['headline'];
		$data['konten']['file_type'] = $konten['file_type'];
		$data['konten']['id_foto'] = $landing_page['id_foto'];
		$data['konten']['link_video'] = $landing_page['link_video'];
		$data['konten']['uri_segment_string'] = $landing_page['uri_segment_string'];
		$data['konten']['id_affiliate'] = $landing_page['id_affiliate'];
		$data['konten']['testimoni'] = $testimoni;
		$data['konten']['show_default_testimoni'] = $landing_page['show_default_testimoni'];
		$data['konten']['created_by'] = $landing_page['created_by'];
		$data['konten']['created_on'] = $landing_page['created_on'];
		$data['konten']['updated_by'] = $landing_page['updated_by'];
		$data['konten']['updated_on'] = $landing_page['updated_on'];
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'view_all_konten')){
			
			if($data['konten']['id_affiliate'] != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('landing_page:konten:view'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('landing_page', 'view_all_konten')){
			$this->template->set_breadcrumb(lang('landing_page:konten:singular'), '/admin/landing_page/konten/index');
		}

		$this->template->set_breadcrumb(lang('global:view').' '.lang('cp:nav_Landing Page'))
			->build('admin/konten_entry', $data);
    }
	
	/**
     * Create a new konten entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'create_konten')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		$this->load->library('files/files');
		if($_SERVER['CONTENT_LENGTH'] > Files::$max_size_possible) {
			$data['messages']['error'] = lang('landing_page:konten:submit_failure');
			$data['messages']['error'] .= '<br>'.lang('landing_page:exceeded_post_max_size') .' '.Files::$max_size_possible.' Bytes';
		}

		if($_POST){
			if($this->_update_konten('new')){	
				$this->session->set_flashdata('success', lang('landing_page:konten:submit_success'));				
				redirect('admin/landing_page/konten/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('landing_page:konten:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/landing_page/konten/index'.$data['uri'];

		// get default konten for landing page
		$landing_page = $this->konten_m->get_konten_by_uri('default');
		$konten = json_decode($landing_page['konten'],true);
		$testimoni = json_decode($landing_page['testimoni'],true);
		$data['default_konten'] = $konten;
		$data['default_konten']['id'] = $landing_page['id'];
		$data['default_konten']['title'] = $konten['headline'];
		$data['default_konten']['file_type'] = $konten['file_type'];
		$data['default_konten']['id_foto'] = $landing_page['id_foto'];
		$data['default_konten']['link_video'] = $landing_page['link_video'];
		$data['default_konten']['uri_segment_string'] = $landing_page['uri_segment_string'];
		$data['default_konten']['id_affiliate'] = $landing_page['id_affiliate'];
		$data['default_konten']['testimoni'] = $testimoni;

		$data['default_product'] = $this->produk_m->get_produk_by_id($this->variables->__get('default_id_product'));
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('landing_page:konten:new'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('landing_page', 'view_all_konten') OR group_has_role('landing_page', 'view_own_konten')){
			$this->template->set_breadcrumb(lang('landing_page:konten:plural'), '/admin/landing_page/konten/index');
		}

		$this->template->set_breadcrumb(lang('landing_page:konten:new'))
			->build('admin/konten_form', $data);
    }
	
	/**
     * Edit a konten entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the konten to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'edit_all_konten') AND ! group_has_role('landing_page', 'edit_own_konten')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(group_has_role('landing_page', 'edit_all_konten')){
			$data['fields'] = $this->konten_m->get_konten_by_id($id);
		} elseif(group_has_role('landing_page', 'edit_own_konten')) {
			$data['fields'] = $this->konten_m->get_konten_by_id_affiliate($this->current_user->id);
		}

		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_konten('edit', $id)){	
				$this->session->set_flashdata('success', lang('landing_page:konten:submit_success'));				
				redirect('admin/landing_page/konten/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('landing_page:konten:submit_failure');
			}
		}
		
		$data['mode'] = 'edit';
		$data['return'] = 'admin/landing_page/konten/index/'.$data['uri'];
		$data['entry_id'] = $id;
		
		$data['products'] = $this->produk_m->get_produk();
		$data['landing_page'] = $this->konten_m->get_konten();

		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('landing_page:konten:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('landing_page', 'view_all_konten') OR group_has_role('landing_page', 'view_own_konten')){
			$this->template->set_breadcrumb(lang('landing_page:konten:plural'), '/admin/landing_page/konten/index')
			->set_breadcrumb(lang('landing_page:konten:view'));
		}else{
			$this->template->set_breadcrumb(lang('landing_page:konten:plural'))
			->set_breadcrumb(lang('landing_page:konten:view'));
		}

		$this->template->set_breadcrumb(lang('landing_page:konten:edit'));
		
		if(group_has_role('landing_page', 'edit_all_konten')){
			$this->template->build('admin/konten_form', $data);
		} else {
			$this->template->build('admin/konten_form_affiliate', $data);
		}
    }
	
	/**
     * Delete a konten entry
     * 
     * @param   int [$id] The id of konten to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('landing_page', 'delete_all_konten') AND ! group_has_role('landing_page', 'delete_own_konten')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('landing_page', 'delete_all_konten')){
			$entry = $this->konten_m->get_konten_by_id($id);
			$created_by_user_id = $entry['id_affiliate'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->konten_m->delete_konten_by_id($id);
        $this->session->set_flashdata('error', lang('landing_page:konten:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('admin/landing_page/konten/index'.$data['uri']);
    }
	
	/**
     * Insert or update konten entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_konten($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();
		
		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		if(group_has_role('landing_page', 'edit_all_konten')){
			$this->form_validation->set_rules('title',lang('landing_page:title'),'required|max_length[100]');
			// $this->form_validation->set_rules('facebook_pixel_id',lang('landing_page:facebook_pixel_id'),'');
		}elseif(group_has_role('landing_page', 'edit_own_konten')){
			$this->form_validation->set_rules('id_landing_page[]',lang('landing_page:id'),'required');
		}
		

		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;
		if ($this->form_validation->run() === true)
		{
			if(group_has_role('landing_page', 'edit_all_konten')){
				$values['konten'] = json_encode(array());
				$values['testimoni'] = json_encode(array());
				$values['show_default_testimoni'] = 0;
			}

			if ($method == 'new')
			{
				if(group_has_role('landing_page', 'edit_all_konten')){
					$result = $this->konten_m->insert_konten($values);
				}
			}
			else
			{
				if(group_has_role('landing_page', 'edit_all_konten')){
					$result = $this->konten_m->update_konten($values, $row_id);
				} else if(group_has_role('landing_page', 'edit_own_konten')){
					for ($i=0; $i < count($values['id_landing_page']); $i++) { 

						if(isset($values['landing_page'][$i])) {
							$values['redirect_to'][$i] = ($values['landing_page'][$i]=='redirect_to') ? $this->addScheme($values['redirect_to'][$i]) : null;
							unset($values['landing_page'][$i]);
						}
						
						$row_id = $values['id_landing_page'][$i];
						$insert_data = array(
							'id_affiliate' => $this->current_user->id,
							'id_landing_page' => $values['id_landing_page'][$i],
							'redirect_to' => $values['redirect_to'][$i],
							'facebook_pixel_id' => $values['facebook_pixel_id'][$i],
							);

						$result = $this->konten_m->update_konten_affiliate($insert_data, $row_id, $this->current_user->id);
					}
				}
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------
	public function check_landing_page($id, $values)
	{
		$landing_page = $this->konten_m->get_konten_by_id($id);
		
		if($landing_page['uri_segment_string']=='default') {
			$konten = json_decode($landing_page['konten'],true);
			$testimoni = json_decode($landing_page['testimoni'],true);
			$values = $konten;
			$values['id'] = $landing_page['id'];
			$values['title'] = $konten['headline'];
			$values['file_type'] = $konten['file_type'];
			$values['id_id_foto'] = $landing_page['id_foto'];
			$values['video_url_url'] = $landing_page['link_video'];
			$values['uri_segment_string'] = $landing_page['uri_segment_string'];
			$values['id_affiliate'] = $landing_page['id_affiliate'];
			$values['testimoni'] = $testimoni;
			$values['show_default_testimoni'] = $landing_page['show_default_testimoni'];
		}
		
		return $values;
	}

	public function addScheme($url, $scheme = 'http://')
	{
		$url = ltrim($url,'/');
		return parse_url($url, PHP_URL_SCHEME) === null ?
		$scheme . $url : $url;
	}

	public function check_youtube_link($fields) {
		if(strpos($fields, 'youtube.com/watch') === false) {
			$this->form_validation->set_message('check_youtube_link','<strong>Link Video</strong> format link video tidak sesuai');
			return false;
		}
		return true;
	}

	// --------------------------------------------------------------------------

	public function image_false($fields, $message){
		$this->form_validation->set_message('image_false',$message);
		return false;
	}

	// --------------------------------------------------------------------------

	public function check_image($image,$param) {
		$this->load->library('files/files');
		$id_folder = Files::get_id_by_name('Landing Page');
		$affiliate_folder = Files::get_id_by_name('affiliate'.$this->current_user->id);
		if(!isset($affiliate_folder)) { 
			$create_folder = Files::create_folder($id_folder,'affiliate'.$this->current_user->id);
			$affiliate_folder = Files::get_id_by_name('affiliate'.$this->current_user->id);
			$affiliate_folder_id = $affiliate_folder;
		} else {
			$affiliate_folder_id = $affiliate_folder;
		}

		list($first_param, $second_param) = split(',', $param);

		// if index is not null then thats mean it is from multi upload
		// so we need to set the variable
		if($second_param != NULL) {
			$_FILES[$first_param]['name'] = $this->_files['name'][$second_param];
			$_FILES[$first_param]['type'] = $this->_files['type'][$second_param];
			$_FILES[$first_param]['tmp_name'] = $this->_files['tmp_name'][$second_param];
			$_FILES[$first_param]['error'] = $this->_files['error'][$second_param];
			$_FILES[$first_param]['size'] = $this->_files['size'][$second_param];
		}

		$image_id = null;
		
		$allowed_type = array('bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'png', 'tiff', 'tif', 'BMP', 'GIF', 'JPEG', 'JPG', 'PNG');
		$data = Files::upload($affiliate_folder_id, $_FILES[$first_param]['name'], $first_param,false,false,false,implode("|", $allowed_type));

		if($data['status']===false) {
			if($second_param != NULL) {
				$first_param = 'Testimoni baris '.($second_param+1).' '.ucwords($first_param);
			} else {
				$first_param = lang('landing_page:'.$first_param);
			}

			if($second_param != NULL) {
				$data['message'] = '<strong>'.$first_param.'</strong> '.strip_tags($data['message']);
			} else {
				$this->form_validation->set_message('check_image','<strong>'.$first_param.'</strong> '.strip_tags($data['message']));
				return false;
			}
		} else {
			$image_id = $data['data']['id'];
			if($second_param == NULL) {
				$_POST['id_'.$first_param] = $image_id;
			} else {
				$_POST['id_'.$first_param][$second_param] = $image_id;
			}
		}
		return $data;
	}

	public function get_affiliate() {
		$term = trim(strip_tags($this->input->get('term')));

		$affiliate = $this->konten_m->get_affiliate_autocomplete($term);

		echo json_encode($affiliate);
	}

}