<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Landing Page Module
 *
 * Modul untuk me-manage landing page
 *
 */
class Landing_page_leads extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'leads';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('landing_page');
		
		$this->load->model('leads_m');
    }

    public function join()
    {
    	// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_leads('new')){	
				$messages = 'Berhasil terdaftar pada waiting list';
				$this->session->set_flashdata('success', $messages);

				if($this->input->is_ajax_request()) {
					exit(json_encode(array('status'=>'success','messages'=>$messages)));
				}
			}else{
				$data['messages']['error'] = validation_errors();

				if($this->input->is_ajax_request()) {
					exit(json_encode(array('status'=>'failed','messages'=>$data['messages']['error'])));
				}
			}
		}

		$data['mode'] = 'new';
		
		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        redirect('');
    }

    /**
     * Insert or update leads entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_leads($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();
		$userdata  = $this->session->all_userdata();
		$values['ip_address'] = $userdata['ip_address'];
		$values['session_id'] = $userdata['session_id'];
		$values['user_agent'] = $userdata['user_agent'];

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('email', 'Email Aktif', 'trim|required|valid_email|max_length[100]|callback_check_leads');
		$this->form_validation->set_rules('full_name', 'Nama Lengkap', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('mobile_phone', 'No Handphone', 'trim|required|numeric|max_length[13]');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->leads_m->insert_leads($values);
			}
			else
			{
				$result = $this->leads_m->update_leads($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	public function check_leads($email)
	{
		$result = $this->leads_m->get_leads_by_email($email);
		if($result) {
			$this->form_validation->set_message('check_leads','Email yang anda masukkan sudah terdaftar dalam waiting list');
			return false;
		}
		return true;
	}

}