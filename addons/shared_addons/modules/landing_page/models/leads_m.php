<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * leads model
 *
 * @author Aditya Satrya
 */
class leads_m extends MY_Model {
	
	public function get_leads($pagination_config = NULL,$filter = NULL)
	{
		$this->db->select('full_name,email,mobile_phone,created_on as join_time');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		$this->db->order_by('ordering_count');
		
		$query = $this->db->get('default_landing_page_leads');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_leads_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_landing_page_leads');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_leads_by_email($email)
	{
		$this->db->select('*');
		$this->db->where('email', $email);
		$query = $this->db->get('default_landing_page_leads');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_leads($filter = NULL)
	{
		return $this->db->count_all_results('landing_page_leads');
	}
	
	public function delete_leads_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_landing_page_leads');
	}
	
	public function insert_leads($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		
		return $this->db->insert('default_landing_page_leads', $values);
	}
	
	public function update_leads($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_landing_page_leads', $values); 
	}
	
}