<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Konten model
 *
 * @author Aditya Satrya
 */
class Konten_m extends MY_Model {
	
	public function get_konten($pagination_config = NULL,$filter = NULL)
	{
		$this->db->select('*');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		if(isset($filter['id_affiliate']) AND $filter['id_affiliate'] != '') {
			$this->db->where('id_affiliate',$filter['id_affiliate']);
		}

		$this->db->order_by('uri_segment_string');
		
		$query = $this->db->get('default_landing_page_konten');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_konten_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_landing_page_konten');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_konten_by_id_affiliate($id)
	{
		$this->db->select('default_landing_page_konten.*');
		$this->db->select('id_affiliate, default_landing_page_affiliate.facebook_pixel_id affiliate_facebook_pixel, default_landing_page_affiliate.uri_segment_string affiliate_uri, default_landing_page_affiliate.redirect_to affiliate_redirect');

		$this->db->where('id_affiliate', $id);

		$this->db->join('default_landing_page_affiliate','default_landing_page_konten.id=default_landing_page_affiliate.id_landing_page');
		$query = $this->db->get('default_landing_page_konten');
		$result = $query->result_array();
		
		return $result;
	}

	public function get_konten_by_uri($affiliate)
	{
		$this->db->select('*');
		$this->db->where('uri_segment_string', $affiliate);
		$query = $this->db->get('default_landing_page_konten');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_konten($filter = NULL)
	{
		if(isset($filter['id_affiliate']) AND $filter['id_affiliate'] != '') {
			$this->db->where('id_affiliate',$filter['id_affiliate']);
		}
		return $this->db->count_all_results('landing_page_konten');
	}
	
	public function delete_konten_by_id($id)
	{
		$this->db->where('id_landing_page', $id);
		$this->db->delete('default_landing_page_affiliate');

		$this->db->where('id', $id);
		$this->db->delete('default_landing_page_konten');
	}
	
	public function insert_konten($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		$this->db->insert('default_landing_page_konten', $values);
		$insert_id = $this->db->insert_id();

		$this->db->query('INSERT INTO default_landing_page_affiliate (id_landing_page,id_affiliate,uri_segment_string) (SELECT '.$insert_id.', id_affiliate, uri_segment_string FROM default_landing_page_affiliate GROUP BY id_affiliate)');
		
		return $insert_id;
	}
	
	public function update_konten($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_landing_page_konten', $values); 
	}

	public function insert_konten_affiliate($values, $id_affiliate = NULL)
	{
		if(!isset($values['id_affiliate']) AND isset($id_affiliate)) {
			$this->db->set('id_affiliate',$id_affiliate);
		}
		return $this->db->insert('default_landing_page_affiliate', $values);
	}
	
	public function update_konten_affiliate($values, $row_id, $id_affiliate)
	{
		$this->db->where('id_landing_page', $row_id);
		$this->db->where('id_affiliate', $id_affiliate);
		return $this->db->update('default_landing_page_affiliate', $values); 
	}

	public function get_affiliate_autocomplete($search_term) {
		$this->db->select('default_users.id AS value, default_profiles.display_name AS label, IFNULL(default_landing_page_konten.uri_segment_string,0) as landing_page',false);
		$this->db->join('default_profiles','default_users.id=default_profiles.user_id');
		$this->db->join('default_groups','default_groups.id=default_users.group_id');
		$this->db->join('default_landing_page_konten','default_users.id=default_landing_page_konten.id_affiliate','left');

		$this->db->where('default_groups.name','affiliate');
		$this->db->where("(default_users.id = '".$search_term."' OR default_users.email like '%".$search_term."%' OR default_profiles.display_name like '%".$search_term."%')",null,false);

		return $this->db->get('default_users')->result_array();
	}
	
}