<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Landing_page extends Module
{
    public $version = '1.6';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Landing Page',
			'id' => 'Landing Page',
		);
		$info['description'] = array(
			'en' => 'Modul untuk me-manage landing page',
			'id' => 'Modul untuk me-manage landing page',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		$info['menu'] = 'Landing Page';
		$info['roles'] = array(
			'access_konten_backend', 'view_all_konten', 'view_own_konten', 'edit_all_konten', 'edit_own_konten', 'delete_all_konten', 'delete_own_konten', 'create_konten',
			'access_leads_backend', 'view_all_leads', 'view_own_leads', 'edit_all_leads', 'edit_own_leads', 'delete_all_leads', 'delete_own_leads', 'create_leads',
		);
		
		if(group_has_role('landing_page', 'access_konten_backend') OR (isset($this->current_user->is_affiliate) AND $this->current_user->is_affiliate == 1)){
			$info['sections']['konten']['name'] = 'landing_page:konten:plural';
			$info['sections']['konten']['uri'] = array('urls'=>array('admin/landing_page/konten/index','admin/landing_page/konten/create','admin/landing_page/konten/view%1','admin/landing_page/konten/edit%1','admin/landing_page/konten/view%2','admin/landing_page/konten/edit%2'));
			
			if(group_has_role('landing_page', 'create_konten')){
				$info['sections']['konten']['shortcuts']['create'] = array(
					'name' => 'landing_page:konten:new',
					'uri' => 'admin/landing_page/konten/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('landing_page', 'access_leads_backend')){
			$info['sections']['leads']['name'] = 'landing_page:leads:plural';
			$info['sections']['leads']['uri'] = array('urls'=>array('admin/landing_page/leads/index','admin/landing_page/leads/create','admin/landing_page/leads/view%1','admin/landing_page/leads/edit%1','admin/landing_page/leads/view%2','admin/landing_page/leads/edit%2'));
			
			if(group_has_role('landing_page', 'create_leads')){
				$info['sections']['leads']['shortcuts']['create'] = array(
					'name' => 'landing_page:leads:new',
					'uri' => 'admin/landing_page/leads/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items){
		
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// konten
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'title' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'id_affiliate' => array(
				'type' => 'SMALLINT',
				'constraint' => 5,
			),
			'konten' => array(
				'type' => 'TEXT',
			),
			'id_foto' => array(
				'type' => 'CHAR',
				'constraint' => 15,
				'null' => true,
			),
			'link_video' => array(
				'type' => 'TEXT',
				'null' => true,
			),
			'testimoni' => array(
				'type' => 'TEXT',
			),
			'uri_segment_string' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
			),
			'created_by' => array(
				'type' => '',
				'constraint' => '',
				'unsigned' => TRUE,
				'default' => '',
				'null' => TRUE,
				'auto_increment' => TRUE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('landing_page_konten', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_landing_page_konten(created_by)");

		$default_konten = array(
			'title' => 'Rasakan Inspirasi Dahsyat dari Buku ini.',
			'id_affiliate' => 0,
			'konten' => '{"headline":"Rasakan Inspirasi Dahsyat dari Buku ini.","secondary_headline":"40 Kisah Pembelajaran hidup pemilik keke Busana.","deskripsi":"Teks deskripsi Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis. ","call_to_action":"Dapatkan Sekarang","file_type":"image"}',
			'id_foto' => '331b531d0557c89',
			'testimoni' => '[{"id_foto":"02e819817bf77c5","nama":"Rizki Kurniawan","pekerjaan":"Mahasiswa","testimoni":"Baru pertama kali dalam hidup saya bisa baca buku sampai selesai secara berurutan dari depan sampai belakang dan dalam waktu kurang dari seminggu."},{"id_foto":"82310cca5dfa05b","nama":"Arys Muhlisin","pekerjaan":"Business Owner","testimoni":"Alhamdulillah, paket Buku Dua Kodi Kartika + baju Keke mendarat dengan selamat sampai Malang. Terima kasih Keke Busana Pusat , Kang Rendy Saputra, dan Zid Club...."},{"id_foto":"5d9eb1a6af1a03f","nama":"Wulansary Mumun","pekerjaan":"Enterpreneur","testimoni":"Ya, buku ini memiliki daya dorong yang wwwooooowwww...\r\nSetelah baca buku ini, saya terdorong untuk terus bergerak,\r\nSetelah baca buku ini saya terdorong untuk tidak diam dan membuang waktu!"}]',
			'uri_segment_string' => 'default',
			'created_by' => 1,
			'created_on' => date('Y-m-d h:i'),
			'ordering_count' => 1,
			);
		$this->db->insert('landing_page_konten',$default_konten);

		$this->version = '1.0';
		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('landing_page_affiliate');
        $this->dbforge->drop_table('landing_page_leads');
        $this->dbforge->drop_table('landing_page_konten');
        return true;
    }

    public function upgrade($old_version)
    {
    	switch ($old_version) {
    		case '1.0':
    			$new_field = array(
					'show_default_testimoni' => array(
						'type' => 'TINYINT',
						'default' => 0,
						'after' => 'testimoni'
						),
    				);

    			$this->dbforge->add_column('landing_page_konten',$new_field);

    			$this->db->where('uri_segment_string','default');
    			$row = $this->db->get('landing_page_konten')->result_array();
    			if(count($row)<1) {
	    			$default_konten = array(
	    				'title' => 'Rasakan Inspirasi Dahsyat dari Buku ini.',
	    				'id_affiliate' => 0,
	    				'konten' => '{"headline":"Rasakan Inspirasi Dahsyat dari Buku ini.","secondary_headline":"40 Kisah Pembelajaran hidup pemilik keke Busana.","deskripsi":"Teks deskripsi Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis. ","call_to_action":"Dapatkan Sekarang","file_type":"image"}',
	    				'id_foto' => '331b531d0557c89',
	    				'testimoni' => '[{"id_foto":"02e819817bf77c5","nama":"Rizki Kurniawan","pekerjaan":"Mahasiswa","testimoni":"Baru pertama kali dalam hidup saya bisa baca buku sampai selesai secara berurutan dari depan sampai belakang dan dalam waktu kurang dari seminggu."},{"id_foto":"82310cca5dfa05b","nama":"Arys Muhlisin","pekerjaan":"Business Owner","testimoni":"Alhamdulillah, paket Buku Dua Kodi Kartika + baju Keke mendarat dengan selamat sampai Malang. Terima kasih Keke Busana Pusat , Kang Rendy Saputra, dan Zid Club...."},{"id_foto":"5d9eb1a6af1a03f","nama":"Wulansary Mumun","pekerjaan":"Enterpreneur","testimoni":"Ya, buku ini memiliki daya dorong yang wwwooooowwww...\r\nSetelah baca buku ini, saya terdorong untuk terus bergerak,\r\nSetelah baca buku ini saya terdorong untuk tidak diam dan membuang waktu!"}]',
	    				'uri_segment_string' => 'default',
	    				'created_by' => 1,
	    				'created_on' => date('Y-m-d h:i'),
	    				'ordering_count' => 1,
	    			);
	    			$this->db->insert('landing_page_konten',$default_konten);
	    		}
    			$this->version = '1.1';
    			break;
			case '1.1':
				// leads
				$fields = array(
					'id' => array(
						'type' => 'INT',
						'constraint' => 11,
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
					'session_id' => array(
						'type' => 'VARCHAR',
						'constraint' => 40,
						),
					'full_name' => array(
						'type' => 'VARCHAR',
						'constraint' => 100,
						),
					'mobile_phone' => array(
						'type' => 'VARCHAR',
						'constraint' => 100,
						),
					'email' => array(
						'type' => 'VARCHAR',
						'constraint' => 100,
						),
					'ip_address' => array(
						'type' => 'VARCHAR',
						'constraint' => 16,
						'null' => true,
						),
					'user_agent' => array(
						'type' => 'VARCHAR',
						'constraint' => 120,
						'null' => true,
						),
					'id_list' => array(
						'type' => 'INT',
						'constraint' => 11,
						'null' => true,
						),
					'created_on' => array(
						'type' => 'DATETIME',
						),
					'created_by' => array(
						'type' => 'INT',
						'constraint' => 11,
						'unsigned' => TRUE,
						'null' => TRUE,
						),
					'updated_on' => array(
						'type' => 'DATETIME',
						'null' => TRUE,
						),
					'updated_by' => array(
						'type' => 'INT',
						'constraint' => 11,
						'unsigned' => TRUE,
						'null' => TRUE,
						),
					'ordering_count' => array(
						'type' => 'INT',
						'constraint' => 11,
						'unsigned' => TRUE,
						'null' => TRUE,
						),
					);
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('landing_page_leads', TRUE);
				$this->db->query("CREATE INDEX author_index ON default_landing_page_leads(created_by)");

				$this->version = '1.2';
    			break;
    		case '1.2' : 
    			// landing page
    			// konten
    			// add column
    			$this->db->query("ALTER TABLE `default_landing_page_konten`
    				ADD COLUMN `redirect_to`  varchar(255) NULL DEFAULT NULL AFTER `id_affiliate`,
    				MODIFY COLUMN `id_affiliate`  int(11) UNSIGNED NULL DEFAULT NULL
    				");

    			// add foreign key
    			// update default landing page
    			$this->db->query("UPDATE default_landing_page_konten SET id_affiliate=NULL WHERE id_affiliate=0");
    			// clean landing page without affiliate
    			$this->db->query("DELETE FROM default_landing_page_konten WHERE id_affiliate IS NOT NULL AND id_affiliate NOT IN (SELECT id FROM default_users)");

    			$this->db->query("ALTER TABLE `default_landing_page_konten` ADD CONSTRAINT `fk_landing_affiliate_users` FOREIGN KEY (`id_affiliate`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_landing_page_konten` ADD CONSTRAINT `fk_landing_createdby_users` FOREIGN KEY (`created_by`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_landing_page_konten` ADD CONSTRAINT `fk_landing_updatedby_users` FOREIGN KEY (`updated_by`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			// leads
    			// add foreign key
    			$this->db->query("ALTER TABLE `default_landing_page_leads` ADD CONSTRAINT `fk_leads_createdby_users` FOREIGN KEY (`created_by`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_landing_page_leads` ADD CONSTRAINT `fk_leads_updatedby_users` FOREIGN KEY (`updated_by`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->version = '1.3';
    			break;
    		case '1.3' : 
    			// landing page
    			// konten
    			// add column
    			$this->db->query("ALTER TABLE `default_landing_page_konten`
    				ADD COLUMN `facebook_pixel_id` varchar(255) NULL DEFAULT NULL AFTER `uri_segment_string`");

    			$this->version = '1.5';
    			break;
    		case '1.4' : 
    			$this->version = '1.5';
    			break;
    		case '1.5':
    			// affiliate
    			if(!$this->db->table_exists('landing_page_affiliate')) {
					$fields = array(
						'id_landing_page' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
							),
						'id_affiliate' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
							'null' => TRUE,
							),
						'facebook_pixel_id' => array(
							'type' => 'VARCHAR',
							'constraint' => 255,
							'null' => TRUE,
							),
						'uri_segment_string' => array(
							'type' => 'VARCHAR',
							'constraint' => 45,
							'null' => TRUE,
							),
						'redirect_to' => array(
							'type' => 'VARCHAR',
							'constraint' => 255,
							'null' => TRUE,
							),
						);
					$this->dbforge->add_field($fields);
					
					$this->dbforge->create_table('landing_page_affiliate', TRUE);
				}

    			// move existing data
    			$this->db->query('insert into default_landing_page_affiliate (id_landing_page,id_affiliate,facebook_pixel_id,uri_segment_string) select id,id_affiliate,facebook_pixel_id,uri_segment_string from default_landing_page_konten where id_affiliate is not null');

    			$this->db->query('UPDATE default_landing_page_affiliate SET id_landing_page = (SELECT id FROM default_landing_page_konten WHERE id_affiliate IS NULL)');

    			$this->db->query('DELETE FROM default_landing_page_konten WHERE id_affiliate IS NOT NULL');

    			// remove foreign key
    			$this->db->query('ALTER TABLE `default_landing_page_konten` DROP FOREIGN KEY `fk_landing_affiliate_users`');

    			// remove column
    			if($this->db->field_exists('id_affiliate','landing_page_konten')) {
	    			$this->dbforge->drop_column('landing_page_konten','id_affiliate');
	    		}

    			// add foreign key
    			$this->db->query('ALTER TABLE `default_landing_page_affiliate` ADD CONSTRAINT `fk_landing_page_konten` FOREIGN KEY (`id_landing_page`) REFERENCES `default_landing_page_konten` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');

				$this->db->query('ALTER TABLE `default_landing_page_affiliate` ADD CONSTRAINT `fk_landing_page_affiliate` FOREIGN KEY (`id_affiliate`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');

    			$this->version = '1.6';
    			break;
    		default:
    			# code...
    			break;
    	}
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}