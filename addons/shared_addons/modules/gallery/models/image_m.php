<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Image model
 *
 * @author Aditya Satrya
 */
class Image_m extends MY_Model {
	
	public function get_image($pagination_config = NULL, $params = array())
	{
		$this->db->select('default_gallery_image.*');
		$this->db->select('default_gallery_album.nama album_nama, default_gallery_album.slug album_slug');
		$this->db->select('default_files.name image_aliasname, default_files.filename image_filename');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		if(isset($params['id_album'])) {
			$this->db->where('id_album', $params['id_album']);
		}

		$this->db->join('default_gallery_album', 'default_gallery_image.id_album=default_gallery_album.id', 'left');
		$this->db->join('default_files', 'default_gallery_image.image=default_files.id', 'left');
		$query = $this->db->get('default_gallery_image');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_image_by_id($id)
	{
		$this->db->select('default_gallery_image.*');
		$this->db->select('default_gallery_album.nama album_nama, default_gallery_album.slug album_slug');
		$this->db->select('default_files.name image_aliasname, default_files.filename image_filename');
		$this->db->where('default_gallery_image.id', $id);

		$this->db->join('default_gallery_album', 'default_gallery_image.id_album=default_gallery_album.id', 'left');
		$this->db->join('default_files', 'default_gallery_image.image=default_files.id', 'left');

		$query = $this->db->get('default_gallery_image');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_image_by_album($id)
	{
		$this->db->select('default_gallery_image.*');
		$this->db->select('default_gallery_album.nama album_nama, default_gallery_album.slug album_slug');
		$this->db->select('default_files.name image_aliasname, default_files.filename image_filename');
		$this->db->where('default_gallery_image.id_album', $id);

		$this->db->join('default_gallery_album', 'default_gallery_image.id_album=default_gallery_album.id', 'left');
		$this->db->join('default_files', 'default_gallery_image.image=default_files.id', 'left');

		$query = $this->db->get('default_gallery_image');
		$result = $query->result_array();
		
		return $result;
	}

	public function get_image_by_album_slug($slug)
	{
		$this->db->select('default_gallery_image.*');
		$this->db->select('default_gallery_album.nama album_nama, default_gallery_album.slug album_slug');
		$this->db->select('default_files.name image_aliasname, default_files.filename image_filename');
		$this->db->where('default_gallery_album.slug', $slug);

		$this->db->join('default_gallery_album', 'default_gallery_image.id_album=default_gallery_album.id', 'left');
		$this->db->join('default_files', 'default_gallery_image.image=default_files.id', 'left');

		$query = $this->db->get('default_gallery_image');
		$result = $query->result_array();
		
		return $result;
	}
	
	public function count_all_image($params = array())
	{
		if(isset($params['id_album'])) {
			$this->db->where('id_album', $params['id_album']);
		}

		return $this->db->count_all_results('gallery_image');
	}
	
	public function delete_image_by_id($id)
	{
		$this->db->where('id_image', $id);
		$this->db->delete('default_gallery_image_translation');

		$this->db->where('id', $id);
		$this->db->delete('default_gallery_image');
	}
	
	public function insert_image($values)
	{
		$this->db->trans_start();

		// set nama, slug
		if(count(explode(",",$values['available_lang']))>1) { 
			$image['title'] = $values['title_'.$this->settings->get('site_lang')];
		}elseif(count(explode(",",$values['available_lang']))==1) { 
			$image['title'] = $values['title'];
		}
		$image['image'] = $values['image'];
		$image['id_album'] = $values['id_album'];
		$image['created'] = date("Y-m-d H:i:s");
		$image['created_by'] = $this->current_user->id;
		
		$this->db->insert('default_gallery_image', $image);
		$id_image = $this->db->insert_id();

		// save the translation
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		
		$translation_lang = array();
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		foreach ($public_lang as $pl) {
			$translation_content = array(
						'title_'.$pl => $values['title_'.$pl],
					);
			$translation_lang = array(
				'id_image' => $id_image,
				'lang_code' => $pl,
				'lang_name' => $lang_list[$pl],
				'translation' => json_encode($translation_content)
				);
			$this->db->insert('gallery_image_translation',$translation_lang);
		}

		$this->db->trans_complete();

		return ($this->db->trans_status() === false) ? false : $id_image;
	}
	
	public function update_image($values, $row_id)
	{
		$this->db->trans_start();

		// set nama, slug
		if(count(explode(",",$values['available_lang']))>1) { 
			$image['title'] = $values['title_'.$this->settings->get('site_lang')];
		}elseif(count(explode(",",$values['available_lang']))==1) { 
			$image['title'] = $values['title'];
		}
		$image['image'] = $values['image'];
		$image['id_album'] = $values['id_album'];
		$image['updated'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		$result = $this->db->update('default_gallery_image', $image); 

		if ( ! $result) return false;

		// save the translation
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		
		// for add lang
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		$translation_lang = array();
		foreach ($public_lang as $pl) {
			$translation_content = array(
						'title_'.$pl => $values['title_'.$pl],
					);
			$translation_lang = array(
				'id_image' => $row_id,
				'lang_code' => $pl,
				'lang_name' => $lang_list[$pl],
				'translation' => json_encode($translation_content)
				);

			$this->db->where('id_image',$row_id);
			$this->db->where('lang_code',$pl);
			$translation = $this->db->get('gallery_image_translation')->result();
			if(count($translation)<1) {
				$this->db->insert('gallery_image_translation',$translation_lang);
			} else {
				$this->db->where('id_image',$row_id);
				$this->db->where('lang_code',$pl);
				$this->db->update('gallery_image_translation',$translation_lang);
			}
		}

		$this->db->trans_complete();

		return (bool)$this->db->trans_status();
	}
	
	public function get_translation($id, $lang_code = NULL)
	{
		if($lang_code) {

			$this->db->where('gallery_image_translation.lang_code', $lang_code);
		}
		return $this->db
			->select('gallery_image_translation.*')
			->where('id_image', $id)
			->join('gallery_image_translation','gallery_image.id=gallery_image_translation.id_image','left')
			->get('gallery_image')
			->result();
	}
}