<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Album model
 *
 * @author Aditya Satrya
 */
class Album_m extends MY_Model {
	
	public function get_album($pagination_config = NULL)
	{
		$this->db->select('default_gallery_album.*, ifnull(count_image,0) count_image, ifnull(count_video,0) count_video', false);
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$this->db->join('(select id_album, count(id) count_image from default_gallery_image group by id_album) image', 'default_gallery_album.id=image.id_album', 'left');
		$this->db->join('(select id_album, count(id) count_video from default_gallery_video group by id_album) video', 'default_gallery_album.id=video.id_album', 'left');
		$this->db->group_by('default_gallery_album.id');
		$query = $this->db->get('default_gallery_album');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_album_by_id($id)
	{
		$this->db->select('default_gallery_album.*, ifnull(count_image,0) count_image, ifnull(count_video,0) count_video', false);

		$this->db->where('default_gallery_album.id', $id);
		$this->db->join('(select id_album, count(id) count_image from default_gallery_image group by id_album) image', 'default_gallery_album.id=image.id_album', 'left');
		$this->db->join('(select id_album, count(id) count_video from default_gallery_video group by id_album) video', 'default_gallery_album.id=video.id_album', 'left');
		$this->db->group_by('default_gallery_album.id');

		$query = $this->db->get('default_gallery_album');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_album_by_slug($slug)
	{
		$this->db->select('default_gallery_album.*, ifnull(count_image,0) count_image, ifnull(count_video,0) count_video', false);

		$this->db->where('default_gallery_album.slug', $slug);
		$this->db->join('(select id_album, count(id) count_image from default_gallery_image group by id_album) image', 'default_gallery_album.id=image.id_album', 'left');
		$this->db->join('(select id_album, count(id) count_video from default_gallery_video group by id_album) video', 'default_gallery_album.id=video.id_album', 'left');
		$this->db->group_by('default_gallery_album.id');

		$query = $this->db->get('default_gallery_album');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_many_by($params = array())
	{
		$this->db->select('default_gallery_album.*')
				->select('files.name image_name')
				->select('files.filename filename')
				->select('files.path image_path');
		
		// Limit the results based on 1 number or 2 (2nd is offset)
		if (isset($params['limit']) && is_array($params['limit']))
		{
			$this->db->limit($params['limit'][0], $params['limit'][1]);
		}
		elseif (isset($params['limit']))
		{
			$this->db->limit($params['limit']);
		}


		$this->db->join('files', 'default_gallery_album.image = files.id', 'left');
		
		$query = $this->db->get('default_gallery_album');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function count_all_album()
	{
		return $this->db->count_all('gallery_album');
	}
	
	public function delete_album_by_id($id)
	{
		$this->db->where('id_album', $id);
		$del_image = $this->db->delete('default_gallery_image');

		$this->db->where('id_album', $id);
		$del_video = $this->db->delete('default_gallery_video');

		if($del_image AND $del_video) {
			$this->db->where('id_album', $id);
			$this->db->delete('default_gallery_album_translation');

			$this->db->where('id', $id);
			$this->db->delete('default_gallery_album');
			return true;
		} else {
			return false;
		}
	}
	
	public function insert_album($values)
	{
		$this->db->trans_start();

		// set nama, slug
		if(count(explode(",",$values['available_lang']))>1) { 
			$album['nama'] = $values['nama_'.$this->settings->get('site_lang')];
			$album['slug'] = $values['slug_'.$this->settings->get('site_lang')];
		}elseif(count(explode(",",$values['available_lang']))==1) { 
			$album['nama'] = $values['nama'];
			$album['slug'] = $values['slug'];
		}
		$album['image'] = $values['image'];
		$album['created'] = date("Y-m-d H:i:s");
		$album['created_by'] = $this->current_user->id;
		
		$this->db->insert('default_gallery_album', $album);
		$id_album = $this->db->insert_id();

		// save the translation
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		
		$translation_lang = array();
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		foreach ($public_lang as $pl) {
			$translation_content = array(
						'nama_'.$pl => $values['nama_'.$pl],
						'slug_'.$pl => $values['slug_'.$pl],
					);
			$translation_lang = array(
				'id_album' => $id_album,
				'lang_code' => $pl,
				'lang_name' => $lang_list[$pl],
				'translation' => json_encode($translation_content)
				);
			$this->db->insert('gallery_album_translation',$translation_lang);
		}

		$this->db->trans_complete();

		return ($this->db->trans_status() === false) ? false : $id_album;
	}
	
	public function update_album($values, $row_id)
	{
		$this->db->trans_start();

		// set nama, slug
		if(count(explode(",",$values['available_lang']))>1) { 
			$album['nama'] = $values['nama_'.$this->settings->get('site_lang')];
			$album['slug'] = $values['slug_'.$this->settings->get('site_lang')];
		}elseif(count(explode(",",$values['available_lang']))==1) { 
			$album['nama'] = $values['nama'];
			$album['slug'] = $values['slug'];
		}
		$album['image'] = $values['image'];
		$album['updated'] = date("Y-m-d H:i:s");

		
		$this->db->where('id', $row_id);
		$result = $this->db->update('default_gallery_album', $album); 

		if ( ! $result) return false;

		// save the translation
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		
		// for add lang
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		$translation_lang = array();
		foreach ($public_lang as $pl) {
			$translation_content = array(
						'nama_'.$pl => $values['nama_'.$pl],
						'slug_'.$pl => $values['slug_'.$pl],
					);
			$translation_lang = array(
				'id_album' => $row_id,
				'lang_code' => $pl,
				'lang_name' => $lang_list[$pl],
				'translation' => json_encode($translation_content)
				);

			$this->db->where('id_album',$row_id);
			$this->db->where('lang_code',$pl);
			$translation = $this->db->get('gallery_album_translation')->result();
			if(count($translation)<1) {
				$this->db->insert('gallery_album_translation',$translation_lang);
			} else {
				$this->db->where('id_album',$row_id);
				$this->db->where('lang_code',$pl);
				$this->db->update('gallery_album_translation',$translation_lang);
			}
		}

		$this->db->trans_complete();

		return (bool)$this->db->trans_status();
	}

	public function get_translation($id, $lang_code = NULL)
	{
		if($lang_code) {

			$this->db->where('gallery_album_translation.lang_code', $lang_code);
		}
		return $this->db
			->select('gallery_album_translation.*')
			->where('id_album', $id)
			->join('gallery_album_translation','gallery_album.id=gallery_album_translation.id_album','left')
			->get('gallery_album')
			->result();
	}
	
}