<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Video model
 *
 * @author Aditya Satrya
 */
class Video_m extends MY_Model {
	
	public function get_video($pagination_config = NULL, $params = array())
	{
		$this->db->select('default_gallery_video.*');
		$this->db->select('default_gallery_album.nama album_nama, default_gallery_album.slug album_slug');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		if(isset($params['id_album'])) {
			$this->db->where('id_album', $params['id_album']);
		}

		$this->db->join('default_gallery_album', 'default_gallery_video.id_album=default_gallery_album.id', 'left');
		$query = $this->db->get('default_gallery_video');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_video_by_id($id)
	{
		$this->db->select('default_gallery_video.*');
		$this->db->select('default_gallery_album.nama album_nama, default_gallery_album.slug album_slug');
		$this->db->where('default_gallery_video.id', $id);

		$this->db->join('default_gallery_album', 'default_gallery_video.id_album=default_gallery_album.id', 'left');
		
		$query = $this->db->get('default_gallery_video');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_video_by_album($id)
	{
		$this->db->select('default_gallery_video.*');
		$this->db->select('default_gallery_album.nama album_nama, default_gallery_album.slug album_slug');
		$this->db->where('default_gallery_video.id_album', $id);

		$this->db->join('default_gallery_album', 'default_gallery_video.id_album=default_gallery_album.id', 'left');

		$query = $this->db->get('default_gallery_video');
		$result = $query->result_array();
		
		return $result;
	}

	public function get_video_by_album_slug($slug)
	{
		$this->db->select('default_gallery_video.*');
		$this->db->select('default_gallery_album.nama album_nama, default_gallery_album.slug album_slug');
		$this->db->where('default_gallery_album.slug', $slug);

		$this->db->join('default_gallery_album', 'default_gallery_video.id_album=default_gallery_album.id', 'left');

		$query = $this->db->get('default_gallery_video');
		$result = $query->result_array();
		
		return $result;
	}
	
	public function count_all_video($params = array())
	{
		if(isset($params['id_album'])) {
			$this->db->where('id_album', $params['id_album']);
		}

		return $this->db->count_all_results('gallery_video');
	}
	
	public function delete_video_by_id($id)
	{
		$this->db->where('id_video', $id);
		$this->db->delete('default_gallery_video_translation');

		$this->db->where('id', $id);
		$this->db->delete('default_gallery_video');
	}
	
	public function insert_video($values)
	{
		$this->db->trans_start();

		// set nama, slug
		$video['title'] = $values['title'];
		$video['description'] = $values['description'];
		if(count(explode(",",$values['available_lang']))>1) { 
			$video['title'] = $values['title_'.$this->settings->get('site_lang')];
			$video['description'] = $values['description_'.$this->settings->get('site_lang')];
		}elseif(count(explode(",",$values['available_lang']))==1) { 
			$video['title'] = $values['title'];
		}
		$video['video_url'] = $values['video_url'];
		$video['id_album'] = $values['id_album'];
		$video['created'] = date("Y-m-d H:i:s");
		$video['created_by'] = $this->current_user->id;
		
		$this->db->insert('default_gallery_video', $video);
		$id_video = $this->db->insert_id();

		// save the translation
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		
		// for add lang
		// check if there is title and description for default lang in values
		// if not, use site lang as default lang
		if(!array_key_exists('title_'.$this->settings->get('site_lang'), $values)) {
			$values['title_'.$this->settings->get('site_lang')] = $values['title'];
		}
		if(!array_key_exists('description_'.$this->settings->get('site_lang'), $values)) {
			$values['description_'.$this->settings->get('site_lang')] = $values['description'];
		}

		$translation_lang = array();
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		foreach ($public_lang as $pl) {
			$translation_content = array(
						'title_'.$pl => $values['title_'.$pl],
						'description_'.$pl => $values['description_'.$pl],
					);
			$translation_lang = array(
				'id_video' => $id_video,
				'lang_code' => $pl,
				'lang_name' => $lang_list[$pl],
				'translation' => json_encode($translation_content)
				);
			$this->db->insert('gallery_video_translation',$translation_lang);
		}

		$this->db->trans_complete();

		return ($this->db->trans_status() === false) ? false : $id_video;
	}
	
	public function update_video($values, $row_id)
	{
		$this->db->trans_start();

		// set nama, slug
		if(count(explode(",",$values['available_lang']))>1) { 
			$video['title'] = $values['title_'.$this->settings->get('site_lang')];
			$video['description'] = $values['description_'.$this->settings->get('site_lang')];
		}elseif(count(explode(",",$values['available_lang']))==1) { 
			$video['title'] = $values['title'];
			$video['description'] = $values['description'];
		}
		$video['video_url'] = $values['video_url'];
		$video['id_album'] = $values['id_album'];
		$video['updated'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		$result = $this->db->update('default_gallery_video', $video); 

		if ( ! $result) return false;

		// save the translation
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		
		// for add lang
		// check if there is title and description for default lang in values
		// if not, use site lang as default lang
		if(!array_key_exists('title_'.$this->settings->get('site_lang'), $values)) {
			$values['title_'.$this->settings->get('site_lang')] = $values['title'];
		}
		if(!array_key_exists('description_'.$this->settings->get('site_lang'), $values)) {
			$values['description_'.$this->settings->get('site_lang')] = $values['description'];
		}

		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		$translation_lang = array();
		foreach ($public_lang as $pl) {
			$translation_content = array(
						'title_'.$pl => $values['title_'.$pl],
						'description_'.$pl => $values['description_'.$pl],
					);
			$translation_lang = array(
				'id_video' => $row_id,
				'lang_code' => $pl,
				'lang_name' => $lang_list[$pl],
				'translation' => json_encode($translation_content)
				);

			$this->db->where('id_video',$row_id);
			$this->db->where('lang_code',$pl);
			$translation = $this->db->get('gallery_video_translation')->result();
			if(count($translation)<1) {
				$this->db->insert('gallery_video_translation',$translation_lang);
			} else {
				$this->db->where('id_video',$row_id);
				$this->db->where('lang_code',$pl);
				$this->db->update('gallery_video_translation',$translation_lang);
			}
		}

		$this->db->trans_complete();

		return (bool)$this->db->trans_status();
	}
	
	public function get_translation($id, $lang_code = NULL)
	{
		if($lang_code) {

			$this->db->where('gallery_video_translation.lang_code', $lang_code);
		}
		return $this->db
			->select('gallery_video_translation.*')
			->where('id_video', $id)
			->join('gallery_video_translation','gallery_video.id=gallery_video_translation.id_video','left')
			->get('gallery_video')
			->result();
	}
}