<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Gallery Module
 *
 * Album & Image
 *
 */
class Admin_image extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'image';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'access_image_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('gallery');		
		$this->load->model('album_m');
		$this->load->model('image_m');
    }

    /**
	 * List all image
     *
     * @return	void
     */
    public function index($id_album = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_image') AND ! group_has_role('gallery', 'view_own_image')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// if($id_album==0) {
		// 	$this->session->set_flashdata('error', lang('cp:access_denied'));
		// 	redirect('admin/gallery/album/index');	
		// }
		
		$data['album'] = $this->album_m->get_album();
		if(count($data['album'])==1) {
			$id_album = $data['album'][0]['id'];
		}

		// -------------------------------------
		// Pagination
		// -------------------------------------
		$params['id_album'] = $id_album;
		if($this->input->get('f-id_album')) {
			$params['id_album'] = $this->input->get('f-id_album');
		}

		$pagination_config['base_url'] = base_url(). 'admin/gallery/image/index/'.$params['id_album'];
		$pagination_config['uri_segment'] = 6;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->image_m->count_all_image($params);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(6);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		$data['id_album'] = $params['id_album'];
		
        $data['image']['entries'] = $this->image_m->get_image($pagination_config, $params);
		$data['image']['total'] = $pagination_config['total_rows'];
		$data['image']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		if(count($data['image']['entries'])==0 AND ($this->uri->segment(6) != NULL AND $this->uri->segment(6)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(6), '', $data['uri']);
			if($this->uri->segment(6) != '' AND $this->uri->segment(6)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(6)-$pagination_config['per_page'];
				redirect('admin/gallery/image/index/'.$params['id_album'].$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/gallery/image/index/'.$params['id_album'].$data['uri']);
				}
			}
		}

		$album_entry = $this->album_m->get_album_by_id($params['id_album']);

		Asset::js('jquery.colorbox-min.js');
		Asset::css('colorbox.css');
		
        $this->template->title(lang('gallery:image:plural'))
			->set_breadcrumb('Home', '/admin');
		if(group_has_role('gallery','access_album_backend')) {
			$this->template->set_breadcrumb(lang('gallery:album:plural'), '/admin/album');
		} else {
			$this->template->set_breadcrumb(lang('gallery:album:plural'), '');
		}
		if (isset($album_entry)) {
			$this->template->set_breadcrumb($album_entry['nama']);
		}
		$this->template->set_breadcrumb(lang('gallery:image:plural'))
			->build('admin/image_index', $data);
    }
	
	/**
     * Display one image
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_image') AND ! group_has_role('gallery', 'view_own_image')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['image'] = $this->image_m->get_image_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_image')){
			if($data['image']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('gallery:image:view'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('gallery', 'view_all_album%') OR group_has_role('gallery', 'view_own_album')){
			$this->template->set_breadcrumb(lang('gallery:image:plural'), '/admin/gallery/image/index/'.$data['image']['id_album']);
		}

		$this->template->set_breadcrumb(lang('gallery:image:view'))
			->build('admin/image_entry', $data);
    }
	
	/**
     * Create a new image entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($id_album = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'create_image')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// if($id_album==0) {
		// 	$this->session->set_flashdata('error', lang('cp:access_denied'));
		// 	redirect('admin/gallery/album/index');	
		// }
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_image('new')){	
				$this->session->set_flashdata('success', lang('gallery:image:submit_success'));
				$id_album = $this->input->post('id_album');
				$data['uri'] = str_replace("f-id_album=".$this->input->get('f-id_album'), "f-id_album=".$id_album, $data['uri']);
				redirect('admin/gallery/image/index/'.$id_album.$data['uri']);
			}else{
				$data['messages']['error'] = lang('gallery:image:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/gallery/image/index/'.$id_album.$data['uri'];

		$data['id_album'] = $id_album;
		$data['album'] = $this->album_m->get_album();
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('gallery:image:new'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('gallery', 'view_all_album%') OR group_has_role('gallery', 'view_own_album')){
			$this->template->set_breadcrumb(lang('gallery:image:plural'), '/admin/gallery/image/index/'.$id_album);
		}

		$this->template->set_breadcrumb(lang('gallery:image:new'))
			->build('admin/image_form', $data);
    }
	
	/**
     * Edit a image entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the image to the be deleted.
     * @return	void
     */
    public function edit($id = 0, $id_album = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'edit_all_image') AND ! group_has_role('gallery', 'edit_own_image')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('gallery', 'edit_all_image')){
			$entry = $this->image_m->get_image_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		$data['uri'] = get_query_string(7);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_image('edit', $id)){	
				$this->session->set_flashdata('success', lang('gallery:image:submit_success'));
				$id_album = $this->input->post('id_album');
				$data['uri'] = str_replace("f-id_album=".$this->input->get('f-id_album'), "f-id_album=".$id_album, $data['uri']);
				redirect('admin/gallery/image/index/'.$id_album.$data['uri']);
			}else{
				$data['messages']['error'] = lang('gallery:image:submit_failure');
			}
		}
		
		$data['fields'] = $this->image_m->get_image_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/gallery/image/index/'.$id_album.$data['uri'];
		$data['entry_id'] = $id;

		$translation = $this->image_m->get_translation($id);
		foreach ($translation as $trans) {
			$tmp_translation = json_decode($trans->translation,true);
			$data['fields'] = array_merge($data['fields'],$tmp_translation);
		}

		$id_album = $data['fields']['id_album'];

		$data['id_album'] = $id_album;
		$data['album'] = $this->album_m->get_album();
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('gallery:image:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('gallery', 'view_all_album') OR group_has_role('gallery', 'view_own_album')){
			$this->template->set_breadcrumb(lang('gallery:image:plural'), '/admin/gallery/image/index/'.$id_album)
			->set_breadcrumb(lang('gallery:image:view'), '/admin/gallery/image/view/'.$id.$data['uri']);
		}else{
			$this->template->set_breadcrumb(lang('gallery:image:plural'))
			->set_breadcrumb(lang('gallery:image:view'));
		}

		$this->template->set_breadcrumb(lang('gallery:image:edit'))
			->build('admin/image_form', $data);
    }
	
	/**
     * Delete a image entry
     * 
     * @param   int [$id] The id of image to be deleted
     * @return  void
     */
    public function delete($id = 0, $id_album = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'delete_all_image') AND ! group_has_role('gallery', 'delete_own_image')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('gallery', 'delete_all_image')){
			$entry = $this->image_m->get_image_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->image_m->delete_image_by_id($id);
        $this->session->set_flashdata('error', lang('gallery:image:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
		$data['uri'] = get_query_string(7);
        redirect('admin/gallery/image/index/'.$id_album.$data['uri']);
    }
	
	/**
     * Insert or update image entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_image($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		// language
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		array_unshift($public_lang, $this->settings->get('site_lang'));
		$public_lang = array_unique($public_lang);
		foreach ($public_lang as $pl) {
			$available_lang[] = array('code'=>$pl,'name'=>$lang_list[$pl]);
		}
		if(count($available_lang) > 1) {
			// for additional lang
			foreach ($available_lang as $al) {
				$this->form_validation->set_rules('title_'.$al['code'], lang('gallery:title').' '.$al['name'], 'required|xss_clean|min_length[5]|max_length[250]');
			}
		} else {
			$this->form_validation->set_rules('title', lang('gallery:title'), 'required|xss_clean|min_length[5]|max_length[250]');
		}
		$this->form_validation->set_rules('image','Image','callback_check_image');

		$this->form_validation->set_rules('id_album', lang('gallery:id_album'), 'required|xss_clean');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			$values['image'] = $_POST['id_image'];

			if ($method == 'new')
			{

				$result = $this->image_m->insert_image($values);
				
			}
			else
			{

				$result = $this->image_m->update_image($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	public function check_image($image) {
		$this->load->library('files/files');
		$id_folder = Files::get_id_by_name('Gallery Album');
		$image_id = null;
		if($_FILES['image']['name']!=''){
			$allowed_type = array('bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'png', 'tiff', 'tif', 'BMP', 'GIF', 'JPEG', 'JPG', 'PNG');
			$data = Files::upload($id_folder, $_FILES['image']['name'], 'image',false,false,false,implode("|", $allowed_type));

			if($data['status']===false) {
				$this->form_validation->set_message('check_image','<strong>Image</strong> '.strip_tags($data['message']));
				return false;
			} else {
				$image_id = $data['data']['id'];
				$_POST['id_image'] = $image_id;
				return true;
			}
		} else {
			if($this->input->post('id_image')=='') {
				$this->form_validation->set_message('check_image','<strong>Image</strong> '.lang('gallery:required'));
				return false;
			}
		}
	}

}