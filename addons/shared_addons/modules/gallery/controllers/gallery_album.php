<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Gallery Module
 *
 * Album & Image
 *
 */
class Gallery_album extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'album';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('gallery');
		
		$this->load->model('album_m');
    }

    /**
	 * List all album
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_album') AND ! group_has_role('gallery', 'view_own_album')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'gallery/album/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->album_m->count_all_album();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['album']['entries'] = $this->album_m->get_album($pagination_config);
		$data['album']['total'] = count($data['album']['entries']);
		$data['album']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('gallery:album:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('gallery:album:plural'))
			->build('album_index', $data);
    }
	
	/**
     * Display one album
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_album') AND ! group_has_role('gallery', 'view_own_album')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['album'] = $this->album_m->get_album_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('gallery', 'view_all_album')){
			if($data['album']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('gallery:album:view'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('gallery:album:plural'), '/gallery/album/index')
			->set_breadcrumb(lang('gallery:album:view'))
			->build('album_entry', $data);
    }
	
	/**
     * Create a new album entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'create_album')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_album('new')){	
				$this->session->set_flashdata('success', lang('gallery:album:submit_success'));				
				redirect('gallery/album/index');
			}else{
				$data['messages']['error'] = lang('gallery:album:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'gallery/album/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('gallery:album:new'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('gallery:album:plural'), '/gallery/album/index')
			->set_breadcrumb(lang('gallery:album:new'))
			->build('album_form', $data);
    }
	
	/**
     * Edit a album entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the album to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'edit_all_album') AND ! group_has_role('gallery', 'edit_own_album')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('gallery', 'edit_all_album')){
			$entry = $this->album_m->get_album_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_album('edit', $id)){	
				$this->session->set_flashdata('success', lang('gallery:album:submit_success'));				
				redirect('gallery/album/index');
			}else{
				$data['messages']['error'] = lang('gallery:album:submit_failure');
			}
		}
		
		$data['fields'] = $this->album_m->get_album_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'gallery/album/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
        $this->template->title(lang('gallery:album:edit'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('gallery:album:plural'), '/gallery/album/index')
			->set_breadcrumb(lang('gallery:album:view'), '/gallery/album/view/'.$id)
			->set_breadcrumb(lang('gallery:album:edit'))
			->build('album_form', $data);
    }
	
	/**
     * Delete a album entry
     * 
     * @param   int [$id] The id of album to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'delete_all_album') AND ! group_has_role('gallery', 'delete_own_album')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('gallery', 'delete_all_album')){
			$entry = $this->album_m->get_album_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->album_m->delete_album_by_id($id);
		$this->session->set_flashdata('error', lang('gallery:album:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('gallery/album/index');
    }
	
	/**
     * Insert or update album entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_album($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('gallery:album:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->album_m->insert_album($values);
			}
			else
			{
				$result = $this->album_m->update_album($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}