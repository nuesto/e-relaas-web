<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Gallery Module
 *
 * Album & Image
 *
 */
class Gallery_video extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'image';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('gallery');
		
		$this->load->model('image_m');
    }

    /**
	 * List all image
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_image') AND ! group_has_role('gallery', 'view_own_image')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'gallery/image/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->image_m->count_all_image();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['image']['entries'] = $this->image_m->get_image($pagination_config);
		$data['image']['total'] = count($data['image']['entries']);
		$data['image']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('gallery:image:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('gallery:image:plural'))
			->build('image_index', $data);
    }
	
	/**
     * Display one image
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'view_all_image') AND ! group_has_role('gallery', 'view_own_image')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['image'] = $this->image_m->get_image_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('gallery', 'view_all_image')){
			if($data['image']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('gallery:image:view'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('gallery:image:plural'), '/gallery/image/index')
			->set_breadcrumb(lang('gallery:image:view'))
			->build('image_entry', $data);
    }
	
	/**
     * Create a new image entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'create_image')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_image('new')){	
				$this->session->set_flashdata('success', lang('gallery:image:submit_success'));				
				redirect('gallery/image/index');
			}else{
				$data['messages']['error'] = lang('gallery:image:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'gallery/image/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('gallery:image:new'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('gallery:image:plural'), '/gallery/image/index')
			->set_breadcrumb(lang('gallery:image:new'))
			->build('image_form', $data);
    }
	
	/**
     * Edit a image entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the image to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'edit_all_image') AND ! group_has_role('gallery', 'edit_own_image')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('gallery', 'edit_all_image')){
			$entry = $this->image_m->get_image_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_image('edit', $id)){	
				$this->session->set_flashdata('success', lang('gallery:image:submit_success'));				
				redirect('gallery/image/index');
			}else{
				$data['messages']['error'] = lang('gallery:image:submit_failure');
			}
		}
		
		$data['fields'] = $this->image_m->get_image_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'gallery/image/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
        $this->template->title(lang('gallery:image:edit'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('gallery:image:plural'), '/gallery/image/index')
			->set_breadcrumb(lang('gallery:image:view'), '/gallery/image/view/'.$id)
			->set_breadcrumb(lang('gallery:image:edit'))
			->build('image_form', $data);
    }
	
	/**
     * Delete a image entry
     * 
     * @param   int [$id] The id of image to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('gallery', 'delete_all_image') AND ! group_has_role('gallery', 'delete_own_image')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('gallery', 'delete_all_image')){
			$entry = $this->image_m->get_image_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->image_m->delete_image_by_id($id);
		$this->session->set_flashdata('error', lang('gallery:image:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('gallery/image/index');
    }
	
	/**
     * Insert or update image entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_image($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('gallery:image:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->image_m->insert_image($values);
			}
			else
			{
				$result = $this->image_m->update_image($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}