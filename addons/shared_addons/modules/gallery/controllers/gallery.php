<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Gallery Module
 *
 * Album & Image
 *
 */
class Gallery extends Public_Controller
{
	// -------------------------------------
	// This will set the active section tab
	// -------------------------------------

	protected $section = 'album';

	public function __construct()
	{
		parent::__construct();

		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
		$this->lang->load('gallery');

		$this->load->model('album_m');
		$this->load->model('image_m');
		$this->load->model('video_m');
	}

	/**
	* List all album
	*
	* @return	void
	*/
	public function index()
	{

		// -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'gallery/album/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->album_m->count_all_album();
		$pagination_config['per_page'] = 12;
		$pagination_config['full_tag_open']		= '<div class="cat-pagintn float-width"><ul>';
		$pagination_config['full_tag_close']	= '</ul></div>';
		$pagination_config['cur_tag_open']		= '<li><a class="crnt-pg">';
		$pagination_config['cur_tag_close']		= '</a></li>';

		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;

		// -------------------------------------
		// Get entries
		// -------------------------------------

		$data['album']['entries'] = $this->album_m->get_album($pagination_config);
		$data['album']['total'] = count($data['album']['entries']);
		$data['album']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
		// Build the page.
		// -------------------------------------

		$this->load->model('tautan/tautan_m');
		$params['limit'] = 5;
		$footer['tautan']['entries'] = $this->tautan_m->get_tautan($params);
		
		$this->template->set_partial('footer', '../partials/footer.html', $footer);

		$this->template->title(lang('gallery:album:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('gallery:album:plural'))
			->build('list', $data);
	}

	/**
	* Display one album
	*
	* @return  void
	*/
	public function view($slug = '')
	{
		// -------------------------------------
		// Get our entry.
		// -------------------------------------

		$data['album'] = $this->album_m->get_album_by_slug($slug);
		if (count($data['album']) < 1) {
        	show_404();
        }

		$data['images']['entries'] = $this->image_m->get_image_by_album_slug($slug);
		$data['images']['total'] = count($data['images']['entries']);

		$data['videos']['entries'] = $this->video_m->get_video_by_album_slug($slug);
		$data['videos']['total'] = count($data['videos']['entries']);
		
		// -------------------------------------
		// Build the page.
		// -------------------------------------
		// $this->load->model('tautan/tautan_m');
		// $params['limit'] = 5;
		// $footer['tautan']['entries'] = $this->tautan_m->get_tautan($params);
		
		// $this->template->set_partial('footer', '../partials/footer.html', $footer);

		$this->template->title(lang('gallery:album:view'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('gallery:album:plural'), '/gallery/album/index')
			->set_breadcrumb(lang('gallery:album:view'))
			->build('detail', $data);
	}

}