<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Gallery Module
 *
 * Album & Image
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        if(group_has_role('gallery','access_album_backend')) {
	        redirect('admin/gallery/album/index');
	    }
	    if(group_has_role('gallery','access_image_backend')) {
	        redirect('admin/gallery/image/index');
	    }
	    if(group_has_role('gallery','access_video_backend')) {
	        redirect('admin/gallery/video/index');
	    }
    }

}