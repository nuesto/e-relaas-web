<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Gallery extends Module
{
    public $version = '1.2.4';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Gallery',
			'id' => 'Gallery',
		);
		$info['description'] = array(
			'en' => 'Album & Image',
			'id' => 'Album & Image',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		$info['menu'] = 'Gallery';
		$info['roles'] = array(
			'access_album_backend', 'view_all_album', 'view_own_album', 'edit_all_album', 'edit_own_album', 'delete_all_album', 'delete_own_album', 'create_album',
			'access_image_backend', 'view_all_image', 'view_own_image', 'edit_all_image', 'edit_own_image', 'delete_all_image', 'delete_own_image', 'create_image',
			'access_video_backend', 'view_all_video', 'view_own_video', 'edit_all_video', 'edit_own_video', 'delete_all_video', 'delete_own_video', 'create_video',
		);

		if(group_has_role('gallery', 'access_album_backend')){
			$info['sections']['album']['name'] = 'gallery:album:plural';
			$info['sections']['album']['uri'] = array('urls'=>array('admin/gallery/album/index','admin/gallery/album/create','admin/gallery/album/view%1','admin/gallery/album/edit%1','admin/gallery/album/view%2','admin/gallery/album/edit%2','admin/gallery/album/view%3','admin/gallery/album/edit%3'));

			if(group_has_role('gallery', 'create_album')){
				$info['sections']['album']['shortcuts']['create'] = array(
					'name' => 'gallery:album:new',
					'uri' => 'admin/gallery/album/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('gallery', 'access_image_backend')){
			$info['sections']['image']['name'] = 'gallery:image:plural';
			$info['sections']['image']['uri'] = array('urls'=>array('admin/gallery/image/index','admin/gallery/image/create','admin/gallery/image/create%1','admin/gallery/image/create%2','admin/gallery/image/view%1','admin/gallery/image/edit%1','admin/gallery/image/view%2','admin/gallery/image/edit%2','admin/gallery/image/view%3','admin/gallery/image/edit%3'));

			if(group_has_role('gallery', 'create_image')){
				$info['sections']['image']['shortcuts']['create'] = array(
					'name' => 'gallery:image:new',
					'uri' => 'admin/gallery/image/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('gallery', 'access_video_backend')){
			$info['sections']['video']['name'] = 'gallery:video:plural';
			$info['sections']['video']['uri'] = array('urls'=>array('admin/gallery/video/index','admin/gallery/video/create','admin/gallery/video/create%1','admin/gallery/video/create%2','admin/gallery/video/view%1','admin/gallery/video/edit%1','admin/gallery/video/view%2','admin/gallery/video/edit%2','admin/gallery/video/view%3','admin/gallery/video/edit%3'));

			if(group_has_role('gallery', 'create_video')){
				$info['sections']['video']['shortcuts']['create'] = array(
					'name' => 'gallery:video:new',
					'uri' => 'admin/gallery/video/create',
					'class' => 'add'
				);
			}
		}

		return $info;
    }

	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items){
		
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// album
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama' => array(
				'type' => 'varchar',
				'constraint' => '50',
				'null' => FALSE,
			),
			'slug' => array(
				'type' => 'varchar',
				'constraint' => '50',
				'null' => FALSE,
			),
			'image' => array(
				'type' => 'char',
				'constraint' => '15',
				'default' => NULL,
				'null' => TRUE,
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('gallery_album', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_gallery_album(created_by)");


		// image
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'id_album' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
			),
			'title' => array(
				'type' => 'varchar',
				'constraint' => '50',
				'null' => FALSE,
			),
			'image' => array(
				'type' => 'char',
				'constraint' => '15',
				'default' => NULL,
				'null' => TRUE,
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('gallery_image', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_gallery_image(created_by)");

		// video
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'id_album' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
			),
			'title' => array(
				'type' => 'varchar',
				'constraint' => '50',
				'null' => FALSE,
			),
			'video_url' => array(
				'type' => 'text',
				'null' => TRUE,
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('gallery_video', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_gallery_video(created_by)");

        // album translation
        $fields = array(
            'id_album' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ),
            'lang_code' => array(
                'type' => 'VARCHAR',
                'constraint' => 10,
                'null' => FALSE,
            ),
            'lang_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => FALSE,
            ),
            'translation' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('gallery_album_translation', TRUE);
        $this->db->query("CREATE INDEX album_trans_idx ON default_gallery_album_translation(id_album)");

        // image translation
        $fields = array(
            'id_image' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ),
            'lang_code' => array(
                'type' => 'VARCHAR',
                'constraint' => 10,
                'null' => FALSE,
            ),
            'lang_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => FALSE,
            ),
            'translation' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('gallery_image_translation', TRUE);
        $this->db->query("CREATE INDEX image_trans_idx ON default_gallery_image_translation(id_image)");

        // video translation
        $fields = array(
            'id_video' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
            ),
            'lang_code' => array(
                'type' => 'VARCHAR',
                'constraint' => 10,
                'null' => FALSE,
            ),
            'lang_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => FALSE,
            ),
            'translation' => array(
                'type' => 'TEXT',
                'null' => TRUE,
            ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('gallery_video_translation', TRUE);
        $this->db->query("CREATE INDEX video_trans_idx ON default_gallery_video_translation(id_video)");

		$this->load->library('files/files');
		Files::create_folder(0,'Gallery Album');
		Files::create_folder(0,'Gallery Image');

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('gallery_album_translation');
        $this->dbforge->drop_table('gallery_image_translation');
        $this->dbforge->drop_table('gallery_video_translation');
        $this->dbforge->drop_table('gallery_image');
        $this->dbforge->drop_table('gallery_video');
        $this->dbforge->drop_table('gallery_album');

		$this->load->library('files/files');
		$id1 = Files::get_id_by_name('Gallery Album');
		$id2 = Files::get_id_by_name('Gallery Image');

		Files::delete_folder($id1);
		Files::delete_folder($id2);

        return true;
    }

    public function upgrade($old_version)
    {
    	switch ($old_version) {
    		case '1.0':
    			// add table translation for album
				if(!$this->db->table_exists('gallery_album_translation')) {
					$fields = array(
						'id_album' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
						),
						'lang_code' => array(
							'type' => 'VARCHAR',
							'constraint' => 10,
							'null' => FALSE,
						),
						'lang_name' => array(
							'type' => 'VARCHAR',
							'constraint' => 100,
							'null' => FALSE,
						),
						'translation' => array(
							'type' => 'TEXT',
							'null' => TRUE,
						),
					);
					$this->dbforge->add_field($fields);
					$this->dbforge->create_table('gallery_album_translation', TRUE);
					$this->db->query("CREATE INDEX album_trans_idx ON default_gallery_album_translation(id_album)");
				}

				// add table translation for image
				if(!$this->db->table_exists('gallery_image_translation')) {
					$fields = array(
						'id_image' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
						),
						'lang_code' => array(
							'type' => 'VARCHAR',
							'constraint' => 10,
							'null' => FALSE,
						),
						'lang_name' => array(
							'type' => 'VARCHAR',
							'constraint' => 100,
							'null' => FALSE,
						),
						'translation' => array(
							'type' => 'TEXT',
							'null' => TRUE,
						),
					);
					$this->dbforge->add_field($fields);
					$this->dbforge->create_table('gallery_image_translation', TRUE);
					$this->db->query("CREATE INDEX image_trans_idx ON default_gallery_image_translation(id_image)");
				}

				// add table translation for video
				if(!$this->db->table_exists('gallery_video_translation')) {
					$fields = array(
						'id_video' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
						),
						'lang_code' => array(
							'type' => 'VARCHAR',
							'constraint' => 10,
							'null' => FALSE,
						),
						'lang_name' => array(
							'type' => 'VARCHAR',
							'constraint' => 100,
							'null' => FALSE,
						),
						'translation' => array(
							'type' => 'TEXT',
							'null' => TRUE,
						),
					);
					$this->dbforge->add_field($fields);
					$this->dbforge->create_table('gallery_video_translation', TRUE);
					$this->db->query("CREATE INDEX video_trans_idx ON default_gallery_video_translation(id_video)");
				}

    		case '1.1':
                // convert all tables to InnoDB engine
                $this->db->query("ALTER TABLE `default_gallery_album` ENGINE=InnoDB");
                $this->db->query("ALTER TABLE `default_gallery_image` ENGINE=InnoDB");
                $this->db->query("ALTER TABLE `default_gallery_video` ENGINE=InnoDB");
                $this->db->query("ALTER TABLE `default_gallery_album_translation` ENGINE=InnoDB");
                $this->db->query("ALTER TABLE `default_gallery_image_translation` ENGINE=InnoDB");
                $this->db->query("ALTER TABLE `default_gallery_video_translation` ENGINE=InnoDB");

                // force to stop in this version. to upgrade to higher version, do upgrade again from admin panel
                $this->version = '1.1.1';
                break;

            case '1.1.1':
            	// add column 'description'
            	$this->db->query("ALTER TABLE ".$this->db->dbprefix('gallery_video')." ADD COLUMN `description` TEXT NULL DEFAULT NULL COMMENT '' AFTER `title`;");

            	// force to stop in this version. to upgrade to higher version, do upgrade again from admin panel
                $this->version = '1.2.0';
                break;

            case '1.2.0':
            	// modify column 'title' add length
            	$fields = array(
            		'title' => array(
            			'name' => 'title',
            			'type' => 'varchar',
            			'constraint' => 255
            			)
            		);

            	$this->dbforge->modify_column('gallery_image',$fields);
            	$this->dbforge->modify_column('gallery_video',$fields);

            	// force to stop in this version. to upgrade to higher version, do upgrade again from admin panel
                $this->version = '1.2.1';
                break;
            case '1.2.1':
				// ------------------------------------------
				// CREATE FK FOR `gallery_video_translation`.`id_video`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row

				// parent widget
				$this->db->select('id');
				$existing_ids = $this->db->get('gallery_video')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('id_video', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('gallery_video_translation');

				// Step 3 - equalize child's column data type to parent's
				// no need

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('gallery_video_translation')." ADD FOREIGN KEY(`id_video`) REFERENCES ".$this->db->dbprefix('gallery_video')."(`id`)");
				
				// ------------------------------------------
				// CREATE FK FOR `gallery_video`.`id_album`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row

				// parent widget
				$this->db->select('id');
				$existing_ids = $this->db->get('gallery_album')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('id_album', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('gallery_video');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('gallery_video')." CHANGE COLUMN `id_album` `id_album` INT(11) UNSIGNED NOT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('gallery_video')." ADD FOREIGN KEY(`id_album`) REFERENCES ".$this->db->dbprefix('gallery_album')."(`id`)");
				
				// ------------------------------------------
				// CREATE FK FOR `gallery_video`.`created_by`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row

				// parent widget
				$this->db->select('id');
				$existing_ids = $this->db->get('users')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('created_by', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('gallery_video');

				// Step 3 - equalize child's column data type to parent's
				// no need

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('gallery_video')." ADD FOREIGN KEY(`created_by`) REFERENCES ".$this->db->dbprefix('users')."(`id`)");
				
				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '1.2.2';
				break;

			case '1.2.2':
				// ------------------------------------------
				// CREATE FK FOR `gallery_image_translation`.`id_image`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row

				// parent widget
				$this->db->select('id');
				$existing_ids = $this->db->get('gallery_image')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('id_image', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('gallery_image_translation');

				// Step 3 - equalize child's column data type to parent's
				// no need

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('gallery_image_translation')." ADD FOREIGN KEY(`id_image`) REFERENCES ".$this->db->dbprefix('gallery_image')."(`id`)");
				
				// ------------------------------------------
				// CREATE FK FOR `gallery_image`.`id_album`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row

				// parent widget
				$this->db->select('id');
				$existing_ids = $this->db->get('gallery_album')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('id_album', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('gallery_image');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('gallery_image')." CHANGE COLUMN `id_album` `id_album` INT(11) UNSIGNED NOT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('gallery_image')." ADD FOREIGN KEY(`id_album`) REFERENCES ".$this->db->dbprefix('gallery_album')."(`id`)");
				
				// ------------------------------------------
				// CREATE FK FOR `gallery_image`.`created_by`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row

				// parent widget
				$this->db->select('id');
				$existing_ids = $this->db->get('users')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('created_by', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('gallery_image');

				// Step 3 - equalize child's column data type to parent's
				// no need

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('gallery_image')." ADD FOREIGN KEY(`created_by`) REFERENCES ".$this->db->dbprefix('users')."(`id`)");
				
				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '1.2.3';
				break;

			case '1.2.3':
				// ------------------------------------------
				// CREATE FK FOR `gallery_album_translation`.`id_album`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row

				// parent widget
				$this->db->select('id');
				$existing_ids = $this->db->get('gallery_album')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('id_album', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('gallery_album_translation');

				// Step 3 - equalize child's column data type to parent's
				// no need

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('gallery_album_translation')." ADD FOREIGN KEY(`id_album`) REFERENCES ".$this->db->dbprefix('gallery_album')."(`id`)");
				
				// ------------------------------------------
				// CREATE FK FOR `gallery_album`.`created_by`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row

				// parent widget
				$this->db->select('id');
				$existing_ids = $this->db->get('users')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('created_by', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('gallery_album');

				// Step 3 - equalize child's column data type to parent's
				// no need

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('gallery_album')." ADD FOREIGN KEY(`created_by`) REFERENCES ".$this->db->dbprefix('users')."(`id`)");
				
				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '1.2.4';
				break;

    	}
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}
