<script type="text/javascript" src="<?php echo base_url(); ?>streams_core/field_asset/js/video_url/video_url.js"></script>

<div class="page-header">
	<h1><?php echo lang('gallery:video:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">
<?php
$supported_lang = get_supported_lang();
foreach ($supported_lang as $sl) {
	$tmp_lang = explode("=", $sl);
	$lang_list[$tmp_lang[0]] = $tmp_lang[1];
}
$public_lang = explode(",", $this->settings->get('site_public_lang'));
array_unshift($public_lang, $this->settings->get('site_lang'));
$public_lang = array_unique($public_lang);
foreach ($public_lang as $pl) {
	$available_lang[] = array('code'=>$pl,'name'=>$lang_list[$pl]);
}

echo form_hidden('available_lang',implode(",", $public_lang));
?>
<?php
foreach ($available_lang as $key => $al) { 
	$lang_code = '';
	if(count($available_lang)>1) {
		echo '<h2 class="smaller lighter">'.$al['name'].'</h2>';
		$lang_code = '_'.$al['code'];
	}
	?>
	<div class="form-group">
		<label class="col-xs-12 col-sm-2 control-label no-padding-right" for="title"><?php echo lang('gallery:title'); ?></label>

		<div class="col-xs-12 col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('title'.$lang_code) != NULL){
					$value = $this->input->post('title'.$lang_code);
				}elseif($mode == 'edit'){
					$value = (isset($fields['title'.$lang_code])) ? $fields['title'.$lang_code] : '';
				}
			?>
			<input name="title<?php echo $lang_code; ?>" type="text" value="<?php echo $value; ?>" class="col-xs-12 col-sm-10" id="" />
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-12 col-sm-2 control-label no-padding-right" for="description"><?php echo lang('gallery:description'); ?></label>

		<div class="col-xs-12 col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('description'.$lang_code) != NULL){
					$value = $this->input->post('description'.$lang_code);
				}elseif($mode == 'edit'){
					$value = (isset($fields['description'.$lang_code])) ? $fields['description'.$lang_code] : '';
				}
			?>
			
			<textarea class="col-xs-12 col-sm-10" name="description<?php echo $lang_code; ?>"><?php echo $value; ?></textarea>
			
			
		</div>
	</div>

<?php } ?>

<?php
if(count($available_lang)>1) {
	echo '<h2 class="smaller lighter">General</h2>';
}
?>

	<div class="form-group">
		<label class="col-xs-12 col-sm-2 control-label no-padding-right" for="video_url"><?php echo lang('gallery:video_url'); ?></label>

		<div class="col-xs-12 col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('video_url') != NULL){
					$value = $this->input->post('video_url');
				}elseif($mode == 'edit'){
					$video_url = json_decode($fields['video_url'], true);
					$value = $video_url['url'];
				}
			?>
			
			<input name="video_url" value="<?php echo $value; ?>" class="col-xs-12 col-sm-6" id="video_url" data-fieldtype="video_url" data-video-width="100%" data-video-height="100%" data-video-autoplay="0" placeholder="Copy and paste the video url from youtube or vimeo" type="text"><div class="clearfix"></div>
			
			<div class="preview_video_url videowrapper" style="display: none; margin-top:15px;">
			    <div class="iframe-preview"></div>
			    
				<input name="video_url_url" value="" type="hidden">
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-12 col-sm-2 control-label no-padding-right" for="id_album"><?php echo lang('gallery:id_album'); ?></label>

		<div class="col-xs-12 col-sm-10">
			<?php 
				$value = ($id_album!=0) ? $id_album : NULL;
				
				if($this->input->post('id_album') != NULL){
					$value = $this->input->post('id_album');
				}elseif($mode == 'edit'){
					$value = $fields['id_album'];
				}
			?>
			
			<select name="id_album" class="col-xs-12 col-sm-5" id="">
				<option value="">-- <?php echo lang('gallery:album:choose'); ?> --</option>
				<?php
				foreach ($album as $key => $a) {
					$selected = ($a['id']==$value) ? 'selected' : '';
					echo '<option value="'.$a['id'].'" '.$selected.'>'.$a['nama'].'</option>';
				}
				?>
			</select>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-sm-offset-2 col-sm-10">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>