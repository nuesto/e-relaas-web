<script type="text/javascript" src="<?php echo base_url(); ?>system/cms/themes/ace/js/jquery/jquery.slugify.js"></script>
<div class="page-header">
	<h1><?php echo lang('gallery:album:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">
<?php
$supported_lang = get_supported_lang();
foreach ($supported_lang as $sl) {
	$tmp_lang = explode("=", $sl);
	$lang_list[$tmp_lang[0]] = $tmp_lang[1];
}
$public_lang = explode(",", $this->settings->get('site_public_lang'));
array_unshift($public_lang, $this->settings->get('site_lang'));
$public_lang = array_unique($public_lang);
foreach ($public_lang as $pl) {
	$available_lang[] = array('code'=>$pl,'name'=>$lang_list[$pl]);
}

echo form_hidden('available_lang',implode(",", $public_lang));
?>
<?php
foreach ($available_lang as $key => $al) { 
	$lang_code = '';
	if(count($available_lang)>1) {
		echo '<h2>'.$al['name'].'</h2>';
		$lang_code = '_'.$al['code'];
	}
	?>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="nama"><?php echo lang('gallery:nama'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama'.$lang_code) != NULL){
					$value = $this->input->post('nama'.$lang_code);
				}elseif($mode == 'edit'){
					$value = (isset($fields['nama'.$lang_code])) ? $fields['nama'.$lang_code] : '';
				}
			?>
			<input name="nama<?php echo $lang_code; ?>" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="name<?php echo $lang_code; ?>" />
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="slug"><?php echo lang('gallery:slug'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('slug'.$lang_code) != NULL){
					$value = $this->input->post('slug'.$lang_code);
				}elseif($mode == 'edit'){
					$value = (isset($fields['slug'.$lang_code])) ? $fields['slug'.$lang_code] : '';
				}
			?>
			<input name="slug<?php echo $lang_code; ?>" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="slug<?php echo $lang_code; ?>" />
			<script>
			$('#name<?php echo $lang_code; ?>').slugify({ slug: '#slug<?php echo $lang_code; ?>', type: '-' });
		</script>
		</div>
	</div>

<?php } ?>

<?php
if(count($available_lang)>1) {
	echo '<h2>General</h2>';
}
?>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="image"><?php echo lang('gallery:image'); ?></label>

		<div class="col-xs-4">
			<?php 
				$value = NULL;
				if($this->input->post('image') != NULL){
					$value = $this->input->post('image');
				}elseif($mode == 'edit'){
					$value = $fields['image'];
				}
			?>

			<?php if(!empty($value)){ ?>
			<img src="<?php echo base_url(); ?>files/large/<?php echo $value; ?>/" style="display:block; width:200px; height:auto; margin-bottom:10px;" />
			<?php } ?>
			<input type="hidden" name="id_image" value="<?php echo $value; ?>">
			<input name="image" id="foto" type="file" />
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>
<script type="text/javascript">
$(document).ready(function() {
	// $('#name').keyup(function(event) {
	// 	TextGanti = $(this).val();
	// 	$('#slug').val(convertToSlug(TextGanti));
	// });

	$('#foto').ace_file_input({
		no_file:'No File ...',
		btn_choose:'Choose',
		btn_change:'Change',
		droppable:true,
		onchange:null,
		thumbnail:'large', //| true | large
		whitelist:'gif|png|jpg|jpeg',
		blacklist:'exe|php|html|js|css'
		//onchange:''
		//
	});
});

function convertToSlug(TextGanti)
{
    return TextGanti.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'_');
}
</script>