<div class="page-header">
	<h1>
		<span><?php echo lang('gallery:image:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('gallery/image/index'); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('gallery:back') ?>
		</a>

		<?php if(group_has_role('gallery', 'edit_all_image')){ ?>
			<a href="<?php echo site_url('gallery/image/edit/'.$image['id']); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('gallery', 'edit_own_image')){ ?>
			<?php if($image->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('gallery/image/edit/'.$image['id']); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('gallery', 'delete_all_image')){ ?>
			<a href="<?php echo site_url('gallery/image/delete/'.$image['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('gallery', 'delete_own_image')){ ?>
			<?php if($image->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('gallery/image/delete/'.$image['id']); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="entry-detail-row">
		<div class="entry-detail-name">ID</div>
		<div class="entry-detail-value"><?php echo $image['id']; ?></div>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:id_album'); ?></div>
		<?php if(isset($image['id_album'])){ ?>
		<div class="entry-detail-value"><?php echo $image['id_album']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:title'); ?></div>
		<?php if(isset($image['title'])){ ?>
		<div class="entry-detail-value"><?php echo $image['title']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:image'); ?></div>
		<?php if(isset($image['image'])){ ?>
		<div class="entry-detail-value"><?php echo $image['image']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:created'); ?></div>
		<?php if(isset($image['created'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($image['created'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:updated'); ?></div>
		<?php if(isset($image['updated'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($image['updated'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('gallery:created_by'); ?></div>
		<div class="entry-detail-value"><?php echo user_displayname($image['created_by'], true); ?></div>
	</div>
</div>