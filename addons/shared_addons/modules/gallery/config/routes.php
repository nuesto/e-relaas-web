<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['gallery/admin/album(:any)'] = 'admin_album$1';
$route['gallery/admin/image(:any)'] = 'admin_image$1';
$route['gallery/admin/video(:any)'] = 'admin_video$1';
$route['gallery/admin(:any)'] = 'admin$1';
$route['gallery/album(:any)'] = 'gallery_album$1';
$route['gallery/image(:any)'] = 'gallery_image$1';
$route['gallery/video(:any)'] = 'gallery_video$1';
$route['gallery(:any)'] = 'gallery$1';
