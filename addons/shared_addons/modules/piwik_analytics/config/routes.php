<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['piwik_analytics/admin(:any)'] = 'admin_piwik_analytics$1';
$route['piwik_analytics(:any)'] = 'piwik_analytics$1';
