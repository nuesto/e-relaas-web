<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Piwik Analytics Module
 *
 * Prototype dashboard user interface
 *
 */
class Piwik_analytics extends Public_Controller
{
	// -------------------------------------
	// This will set the active section tab
	// -------------------------------------

	protected $section = 'piwik_analytics';

	public function __construct()
	{
		parent::__construct();

		$this->lang->load('piwik_analytics');
	}

	public function index() {
	}

	/**
	* Sample load data stats to widget
	*
	* @return  void
	*/
	public function stats()
	{
		$max_item = rand(5, 15);
		$data = array();
		for ($i=0; $i < $max_item; $i++) {
			$data['items'][$i] = rand(1, 15);
		}

		$data['total'] = rand(5, 99999);

		die(json_encode($data));
	}

	/**
	* Sample load view to widget
	*
	* @return  void
	*/
	public function view($view)
	{
		$data = array();

		echo $this->load->view('piwik_analytics/'.$view, $data, true);
	}

	public function piwik_traffic($date = 'previous7') {
		$data = array();
		$data['date_range'] = $date;

		if($date=='today') {
			$date = 'last1';
		} elseif($date=='yesterday') {
			$date = 'last2';
		}
		// this token is used to authenticate your API request.
		// You can get the token on the API page inside your Piwik interface

		$token_auth = Settings::get('piwik_token_auth');

		$url = Settings::get('piwik_soap');
		$url .= "?module=API&method=VisitsSummary.getVisits";
		$url .= "&idSite=".Settings::get('piwik_id_site');
		$url .= "&period=day&date=".$date;
		$url .= "&format=json&filter_limit=20";
		$url .= "&token_auth=$token_auth";
		
		$fetched = @file_get_contents($url);
		$fetched_data = json_decode($fetched);
		$data['visitor'] = json_encode(array());
		
		if($fetched_data) {
			if(!isset($fetched_data->result) OR $fetched_data->result!='error') {
				if(count($fetched_data)==0) {
					$data['result'] = json_encode(array('result'=>'error','message'=>'There is no result found for this query'));
				} else {
					$visitor = array();
					foreach (json_decode($fetched) as $key => $value) {
						if($key=='value') {
							$key = $date;
						}
						$visitor[] = array('tanggal'=>date('d-m-Y',strtotime($key)),'data'=>$value);
					}
					$data['visitor'] = json_encode($visitor);
				}
			} else {
				$data['result'] = json_encode($fetched_data);
			}
		} else {
			$data['result'] = json_encode(array('result'=>'error','message'=>'Can\'t connect to server, Please check your SOAP URL in Settings'));
		}
		echo $this->load->view('piwik_analytics/website_traffic_piwik', $data, true);

	}

	public function piwik_today($date = 'today') {
		$data = array();
		$data['date_range'] = $date;

		if($date=='today' || $date=='yesterday') {
			// $date = date('Y-m-d', strtotime($date));
			$date1 = date('Y-m-d', strtotime($date)).','.date('Y-m-d');
			if($date=='today') $date2 = 'last1';
			elseif($date=='yesterday') $date2 = 'last2';
		} else {
			$date1 = $date;
			$date2 = $date;
		}

		// this token is used to authenticate your API request.
		// You can get the token on the API page inside your Piwik interface
		$token_auth = Settings::get('piwik_token_auth');

		// we call the REST API and request the 100 first keywords for the last month for the idsite=7
		$url = Settings::get('piwik_soap');
		$url .= "?module=API&method=API.getBulkRequest";
		$url .= "&format=json";

		$url1 = "method=VisitsSummary.get";
		$url1 .= "&idSite=".Settings::get('piwik_id_site');
		$url1 .= "&period=day&date=".$date1;
		$url1 .= "&token_auth=$token_auth";
		$url .= '&urls[0]='.urlencode($url1);

		$url2 = "method=VisitsSummary.get";
		$url2 .= "&idSite=".Settings::get('piwik_id_site');
		$url2 .= "&period=range&date=".$date2;
		$url2 .= "&token_auth=$token_auth";
		$url .= '&urls[1]='.urlencode($url2);		
		
		$fetched = @file_get_contents($url);
		
		$graph = array();
		$fetched_data = json_decode($fetched);
		$data['display'] = json_encode(array());
		$data['graph'] = json_encode(array());

		if($fetched_data[0]) {
			if(!isset($fetched_data[0]->result) OR $fetched_data[0]->result!='error') {
				if(count($fetched_data[0])==0) {
					$data['result'] = json_encode(array('result'=>'error','message'=>'There is no result found for this query'));
				} else {
					$index = 0;
					foreach ($fetched_data[0] as $key => $value) {
						if(gettype($value)=='array' and count($value)==0) {
							$value = array(
								'nb_uniq_visitors'=>0,
								'nb_users'=>0,
								'nb_visits'=>0,
								'nb_actions'=>0,
								'nb_visits_converted'=>0,
								'bounce_count'=>0,
								'sum_visit_length'=>0,
								'max_actions'=>0,
								'bounce_rate'=>0,
								'nb_actions_per_visit'=>0,
								'avg_time_on_site'=>0,
								);
						}

						if($date=='previous30') {
							if($index%2==0) {
								$graph[] = (object)$value;
							}
						} else {
							$graph[] = $value;
						}
						$index++;
					}
					$data['graph'] = json_encode($graph);
				}
			} else {
				$data['result'] = json_encode($fetched_data[0]);
			}
		} else {
			$data['result'] = json_encode(array('result'=>'error','message'=>'Can\'t connect to server, Please check your SOAP URL in Settings'));
		}

		if($fetched_data[0]) {
			if(!isset($fetched_data[1]->result) OR $fetched_data[1]->result!='error') {
				if(count($fetched_data[1])==0) {
					$data['result'] = json_encode(array('result'=>'error','message'=>'There is no result found for this query'));
				} else {
					$data['display'] = json_encode($fetched_data[1]);
				}
			} else {
				$data['result'] = json_encode($fetched_data);
			}
		} else {
			$data['result'] = json_encode(array('result'=>'error','message'=>'Can\'t connect to server, Please check your SOAP URL in Settings'));
		}
		
		echo $this->load->view('piwik_analytics/today_status_piwik', $data, true);

	}

	public function piwik_location($date = 'today') {
		$data = array();
		$data['date_range'] = $date;

		if($date=='today') {
			$date = 'last1';
		} elseif($date=='yesterday') {
			$date = 'last2';
		}
		// this token is used to authenticate your API request.
		// You can get the token on the API page inside your Piwik interface
		$token_auth = Settings::get('piwik_token_auth');

		$url = Settings::get('piwik_soap');
		$url .= "?module=API&method=UserCountry.getCountry";
		$url .= "&idSite=".Settings::get('piwik_id_site');
		$url .= "&period=range&date=".$date;
		$url .= "&format=json&filter_limit=7";
		$url .= "&token_auth=$token_auth";

		$fetched = @file_get_contents($url);
		$fetched_data = json_decode($fetched);
		$data['countries'] = array();

		if(gettype($fetched_data)=='array') {
			if(!isset($fetched_data->result) OR $fetched_data->result!='error') {
				if(count($fetched_data)==0) {
					$data['result'] = json_encode(array('result'=>'error','message'=>'There is no result found for this query'));
				} else {
					$data['countries'] = $fetched_data;
				}
			} else {
				$data['result'] = json_encode($fetched_data);
			}
		} else {
			$data['result'] = json_encode(array('result'=>'error','message'=>'Can\'t connect to server, Please check your SOAP URL in Settings'));
		}
		
		echo $this->load->view('piwik_analytics/visitor_location_piwik', $data, true);

	}

	public function piwik_os($date = 'today') {
		$data = array();
		$data['date_range'] = $date;

		if($date=='today') {
			$date = 'last1';
		} elseif($date=='yesterday') {
			$date = 'last2';
		}

		// this token is used to authenticate your API request.
		// You can get the token on the API page inside your Piwik interface
		$token_auth = Settings::get('piwik_token_auth');

		$url = Settings::get('piwik_soap');
		$url .= "?module=API&method=DevicesDetection.getOsFamilies";
		$url .= "&idSite=".Settings::get('piwik_id_site');
		$url .= "&period=range&date=".$date;
		$url .= "&format=json&filter_limit=7";
		$url .= "&token_auth=$token_auth";
		
		$fetched = @file_get_contents($url);
		$fetched_data = json_decode($fetched);
		$data['os_list'] = array();

		if(gettype($fetched_data)=='array') {
			if(!isset($fetched_data->result) OR $fetched_data->result!='error') {
				if(count($fetched_data)==0) {
					$data['result'] = json_encode(array('result'=>'error','message'=>'There is no result found for this query'));
				} else {
					$data['os_list'] = $fetched_data;
				}
			} else {
				$data['result'] = json_encode($fetched_data);
			}
		} else {
			$data['result'] = json_encode(array('result'=>'error','message'=>'Can\'t connect to server, Please check your SOAP URL in Settings'));
		}

		echo $this->load->view('piwik_analytics/os_access_piwik', $data, true);

	}

	public function piwik_browser($date = 'today') {
		$data = array();
		$data['date_range'] = $date;

		if($date=='today') {
			$date = 'last1';
		} elseif($date=='yesterday') {
			$date = 'last2';
		}

		// this token is used to authenticate your API request.
		// You can get the token on the API page inside your Piwik interface
		$token_auth = Settings::get('piwik_token_auth');

		
		$url = Settings::get('piwik_soap');
		$url .= "?module=API&method=DevicesDetection.getBrowserFamilies";
		$url .= "&idSite=".Settings::get('piwik_id_site');
		$url .= "&period=range&date=".$date;
		$url .= "&format=json&filter_limit=6";
		$url .= "&token_auth=$token_auth";

		$fetched = @file_get_contents($url);
		
		$data['browsers'] = array();
		$fetched_data = json_decode($fetched);

		if(gettype($fetched_data)=='array') {
			if(!isset($fetched_data->result) OR $fetched_data->result!='error') {
				if(count($fetched_data)==0) {
					$data['result'] = json_encode(array('result'=>'error','message'=>'There is no result found for this query'));
				} else {
					$data['browsers'] = $fetched_data;
				}
			} else {
				$data['result'] = json_encode($fetched_data);
			}
		} else {
			$data['result'] = json_encode(array('result'=>'error','message'=>'Can\'t connect to server, Please check your SOAP URL in Settings'));
		}
		echo $this->load->view('piwik_analytics/browser_access_piwik', $data, true);

	}
}