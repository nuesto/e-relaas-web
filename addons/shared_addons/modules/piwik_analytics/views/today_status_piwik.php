<!-- start today status widget -->
<div class="widget-body">
	<div class="row">
		<div class="col-md-12">
			<label><?php echo lang('piwik_analytics:choose_interval'); ?> : </label>
			<select id="today_status_piwik">
				<option value="today" <?php echo ($date_range=='today') ? 'selected' : ''; ?>><?php echo lang('piwik_analytics:today'); ?></option>
				<option value="yesterday" <?php echo ($date_range=='yesterday') ? 'selected' : ''; ?>><?php echo lang('piwik_analytics:yesterday'); ?></option>
				<option value="previous7" <?php echo ($date_range=='previous7') ? 'selected' : ''; ?>><?php echo lang('piwik_analytics:last7'); ?></option>
				<option value="previous30" <?php echo ($date_range=='previous30') ? 'selected' : ''; ?>><?php echo lang('piwik_analytics:last30'); ?></option>
			</select>
		</div>
	</div>
<?php
$data = (isset($result)) ? json_decode($result) : '';
if(gettype($data)!='string' AND $data->result=='error') {
?>
<div class="widget-not-loaded">
	<?php echo $data->message; ?> <br> 
	<a class="btn btn-link widget-refresh">refresh here</a>
</div>
<?php
} else {
?>

	<div class="row">
		<div class="col-md-12">
			<!-- Visitors, pageview, bounce rate, etc., Sparklines plugin used -->
			<ul class="current-status">
			  <li>
					<span id="status_visits" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9"></span>
					<span class="bold">Visit : <span id="nb_visits">0</span></span>
			  </li>
			  <li>
					<span id="status_users" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9"></span>
					<span class="bold">Users : <span id="nb_users">0</span></span>
			  </li>
			  <li>
					<span id="status_pageviews" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9"></span>
					<span class="bold">Pageviews : <span id="nb_actions">0</span></span>
			  </li>
			  <li>
					<span id="status_pageviewsPerSession" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9"></span>
					<span class="bold">Pageviews Per Visit : <span id="nb_actions_per_visit">0</span></span>
			  </li>
			  <li>
					<span id="status_avgSessionDuration" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9"></span>
					<span class="bold">Avg. Visit Duration : <span id="avg_time_on_site">0</span></span>
			  </li>
			  <li>
					<span id="status_bounceRate" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9"></span>
					<span class="bold">Bounce Rate : <span id="bounce_rate">0</span></span>
			  </li>
			  <li>
					<span id="status_max_actions" class="sparkinline" sparkHeight="20" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="5" sparkLineColor="#63C4ED" sparkFillColor="#E5F3F9"></span>
					<span class="bold">Max Action Per Visit : <span id="max_actions">0</span></span>
			  </li>
			</ul>
		</div>
	</div>
</div>
<!-- end today status widget -->

<script>
var graph_data = JSON.parse('<?php echo $graph; ?>');

			var nb_visits = [];
			var nb_users = [];
			var nb_actions_per_visit = [];
			var nb_actions = [];
			var max_actions = [];
			var avg_time_on_site = [];
			var bounce_rate = [];
			
			for (var i = 0, row; row = graph_data[i]; ++i) {
					nb_visits.push(parseInt(row.nb_visits));
					nb_actions_per_visit.push(row.nb_actions_per_visit);
					nb_users.push(parseInt(row.nb_users));
					nb_actions.push(parseInt(row.nb_actions));
					max_actions.push(round(parseFloat(row.max_actions),2));
					avg_time_on_site.push(parseInt(row.avg_time_on_site));
					bounce_rate.push(round(parseFloat(row.bounce_rate),2));
			}

			$graphic = $('.sparkinline#status_visits');
			if ($graphic.length > 0 ) {
				$graphic.html(nb_visits.join(','));
				$graphic.sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });
			}
			
			$graphic = $('.sparkinline#status_pageviewsPerSession');
			if ($graphic.length > 0 ) {
				$graphic.html(nb_actions_per_visit.join(','));
				$graphic.sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });
			}

			$graphic = $('.sparkinline#status_users');
			if ($graphic.length > 0 ) {
				$graphic.html(nb_users.join(','));
				$graphic.sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });
			}

			$graphic = $('.sparkinline#status_pageviews');
			if ($graphic.length > 0 ) {
				$graphic.html(nb_actions.join(','));
				$graphic.sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });
			}

			$graphic = $('.sparkinline#status_max_actions');
			if ($graphic.length > 0 ) {
				$graphic.html(max_actions.join(','));
				$graphic.sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });
			}

			$graphic = $('.sparkinline#status_avgSessionDuration');
			if ($graphic.length > 0 ) {
				$graphic.html(avg_time_on_site.join(','));
				$graphic.sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });
			}

			$graphic = $('.sparkinline#status_bounceRate');
			if ($graphic.length > 0 ) {
				$graphic.html(bounce_rate.join(','));
				$graphic.sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });
			}
	var today_data = JSON.parse('<?php echo $display; ?>');

	$('#nb_visits').html(today_data.nb_visits);
	$('#nb_actions_per_visit').html(today_data.nb_actions_per_visit);
	$('#users').html(today_data.nb_users);
	$('#nb_actions').html(today_data.nb_actions);
	$('#max_actions').html(today_data.max_actions);
	$('#avg_time_on_site').html(today_data.avg_time_on_site);
	$('#bounce_rate').html(today_data.bounce_rate);

function round(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

</script>
<?php } ?>
<script type="text/javascript">
	$('#today_status_piwik').change(function() {
		widgetRefresh($(this).closest('.widget'), $(this).val());
	});
</script>