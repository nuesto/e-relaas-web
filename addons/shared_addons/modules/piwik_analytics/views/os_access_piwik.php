<!-- start browser widget -->
<div class="widget-body" style="min-height:315px;">
	<div class="col-md-12">
		<label><?php echo lang('piwik_analytics:choose_interval'); ?> : </label>
		<select id="os_access_piwik">
			<option value="today" <?php echo ($date_range=='today') ? 'selected' : ''; ?>><?php echo lang('piwik_analytics:today'); ?></option>
			<option value="yesterday" <?php echo ($date_range=='yesterday') ? 'selected' : ''; ?>><?php echo lang('piwik_analytics:yesterday'); ?></option>
			<option value="previous7" <?php echo ($date_range=='previous7') ? 'selected' : ''; ?>><?php echo lang('piwik_analytics:last7'); ?></option>
			<option value="previous30" <?php echo ($date_range=='previous30') ? 'selected' : ''; ?>><?php echo lang('piwik_analytics:last30'); ?></option>
		</select>
	</div>
<?php
$data = (isset($result)) ? json_decode($result) : '';
if(gettype($data)!='string' AND $data->result=='error') {
?>
<div class="widget-not-loaded">
	<?php echo $data->message; ?> <br> 
	<a class="btn btn-link widget-refresh">refresh here</a>
</div>
<?php
} else {
?>
	<div class="table-responsive clearfix">
		<table class="table table-hover no-margin" id="">
	  		<thead>
				<tr>
				  <th>Browsers</th>
				  <th class="text-center">Visits</th>
				</tr>
	  		</thead>
			<tbody>
				<?php foreach ($os_list as $os) { ?>
				<tr>
					<td><?php echo $os->label; ?></td>
					<td class="text-center"><?php echo $os->nb_actions; ?></td>
				</tr>
				<?php } ?>
		  </tbody>
		</table>
	</div>
</div>
<!-- end browser widget -->
<?php } ?>
<script>
	$('#os_access_piwik').change(function() {
		widgetRefresh($(this).closest('.widget'), $(this).val());
	});
</script>