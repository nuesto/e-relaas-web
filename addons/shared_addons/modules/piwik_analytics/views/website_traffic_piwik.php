<!-- start website traffic widget -->
<div class="widget-body" style="min-height:340px;">
	<div class="row">
		<div class="col-md-12">
			<label><?php echo lang('piwik_analytics:choose_interval'); ?> : </label>
			<select id="website_traffic_piwik">
				<option value="today" <?php echo ($date_range=='today') ? 'selected' : ''; ?>><?php echo lang('piwik_analytics:today'); ?></option>
				<option value="yesterday" <?php echo ($date_range=='yesterday') ? 'selected' : ''; ?>><?php echo lang('piwik_analytics:yesterday'); ?></option>
				<option value="previous7" <?php echo ($date_range=='previous7') ? 'selected' : ''; ?>><?php echo lang('piwik_analytics:last7'); ?></option>
				<option value="previous30" <?php echo ($date_range=='previous30') ? 'selected' : ''; ?>><?php echo lang('piwik_analytics:last30'); ?></option>
			</select>
		</div>
	</div>
<?php
$data = (isset($result)) ? json_decode($result) : '';
if(gettype($data)!='string' AND $data->result=='error') {
?>
	<div class="widget-not-loaded">
		<?php echo $data->message; ?> <br> 
		<a class="btn btn-link widget-refresh">refresh here</a>
	</div>
<?php
} else {
?>
	<div class="row">
		<div id="website-traffic-piwik-graphic" style="height:250px"></div>
	</div>
</div>
<!-- end website traffic widget -->

<script type="text/javascript">
	var graph_data = '<?php echo $visitor; ?>';
	
	//Morris Chart
	//Website traffic chart
	Morris.Line({
					element: 'website-traffic-piwik-graphic',
					data: JSON.parse(graph_data),
					xkey: 'tanggal',
					ykeys: ['data'],
					parseTime: false,
					labels: ['Pengunjung']
				});
</script>
<?php } ?>
<script>
	$('#website_traffic_piwik').change(function() {
		widgetRefresh($(this).closest('.widget'), $(this).val());
	});
</script>