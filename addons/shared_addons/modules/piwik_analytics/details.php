<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Piwik_analytics extends Module
{
	public $version = '1.0';

	public function info()
	{
		$info = array();
		$info['name'] = array(
			'en' => 'Piwik Analytics',
			'id' => 'Piwik Analytics',
		);
		$info['description'] = array(
			'en' => 'Module Piwik Analytics.',
			'id' => 'Modul Piwik Analisis.',
		);
		$info['frontend'] = true;
		$info['backend'] = false;
		$info['roles'] = array();

		return $info;
	}

	/**
	 * Install
	 *
	 * This function will set up our streams
	 *
	 */
	public function install()
	{
		$piwik_soap = array(
			'slug' => 'piwik_soap',
			'title' => 'Piwik SOAP URL',
			'description' => 'This URL is acquired when you sign up at <a href="https://cloud.piwik.pro/checkout/client/sign-up">https://cloud.piwik.pro/checkout/client/sign-up</a>',
			'type' => 'text',
			'default' => '',
			'value' => '',
			'options' => '',
			'is_required' => '1',
			'is_gui' => '1',
			'module' => 'integration',
			'order' => '980',
			);
		$this->db->insert('default_settings',$piwik_soap);

		$piwik_token_auth = array(
			'slug' => 'piwik_token_auth',
			'title' => 'Piwik Token Auth',
			'description' => 'The parameter used to request data from PIWIK API',
			'type' => 'text',
			'default' => '',
			'value' => '',
			'options' => '',
			'is_required' => '1',
			'is_gui' => '1',
			'module' => 'integration',
			'order' => '979',
			);
		$this->db->insert('default_settings',$piwik_token_auth);

		$piwik_id_site = array(
			'slug' => 'piwik_id_site',
			'title' => 'Piwik Id Site',
			'description' => 'This is your id site in PIWIK',
			'type' => 'text',
			'default' => '',
			'value' => '',
			'options' => '',
			'is_required' => '1',
			'is_gui' => '1',
			'module' => 'integration',
			'order' => '978',
			);
		$this->db->insert('default_settings',$piwik_id_site);

		return true;
	}

	/**
	 * Uninstall
	 *
	 * Uninstall our module - this should tear down
	 * all information associated with it.
	 */
	public function uninstall()
	{
		$slug = array('piwik_soap','piwik_token_auth','piwik_id_site');
		$this->db->where_in('slug',$slug);
		$this->db->delete('default_settings');
		return true;
	}

	public function upgrade($old_version)
	{
		return true;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}