<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Widget_Piwik_today_status extends Widgets
{
	// The widget title,  this is displayed in the admin interface
	public $title = 'Piwik Today Status';

	//The widget description, this is also displayed in the admin interface.  Keep it brief.
	public $description = 'Display today status from Piwik Analytics.';

	// The author's name
	public $author = 'sidi.mustaqbal';

	// The authors website for the widget
	public $website = 'http://example.com/';

	//current version of your widget
	public $version = '1.0';

	/**
	 * $fields array fore storing widget options in the database.
	 * values submited through the widget instance form are serialized and
	 * stored in the database.
	 */
	public $fields = array(
		array(
			'field'   => 'view_uri',
			'label'   => 'URL View',
		),
	);

	/**
	 * the $options param is passed by the core Widget class.  If you have
	 * stored options in the database,  you must pass the paramater to access
	 * them.
	 */
	public function run($options)
	{
		// Store the feed items
		return array('opt' => $options);
	}

	/**
	 * form() is used to prepare/pass data to the widget admin form
	 * data returned from this method will be available to views/form.php
	 */
	public function form()
	{
		$droption = array();

		$droption['opt_color_scheme'] = array(
			'danger' => 'Red',
			'info' => 'Blue',
			'success' => 'Green',
			'warning' => 'Yellow',
		);
		return $droption;
	}

	/**
	 * save() is used to alter submited data before their insertion in database
	 */
	public function save($options)
	{
	   $options['view_uri'] = 'piwik_analytics/piwik_today';
	   
	   return $options;
	}
}