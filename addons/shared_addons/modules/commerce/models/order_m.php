<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Order model
 *
 * @author Aditya Satrya
 */
class Order_m extends MY_Model {

	public function get_order($pagination_config = NULL, $params = NULL, $include_batal = 1)
	{
		$this->db->select('default_commerce_order.*');
		$this->db->select('default_commerce_payment.status status_pembayaran, payment_method, ipaymu_trx_id, ipaymu_url');
		$this->db->select('display_name,no_handphone');

		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		$this->db->order_by('tanggal_order','desc');
		$this->db->order_by('placed_on','desc');
		$this->db->order_by('default_commerce_payment.created_on','desc');
		$this->db->order_by('default_commerce_order.id','desc');

		if($params != NULL) {
			if(isset($params['tanggal_order'])) {
				$this->db->where("(tanggal_order like '%".$params['tanggal_order']."%' OR placed_on like '%".$params['tanggal_order']."%' OR default_commerce_order.created_on like '%".$params['tanggal_order']."%')",null,false);
				unset($params['tanggal_order']);
			}
			if(isset($params['display_name'])) {
				$this->db->where("(email = '".$params['display_name']."' OR display_name like '%".str_replace("'", "''", $params['display_name'])."%')",null,false);
				unset($params['display_name']);
			}
			if(isset($params['code'])) {
				$this->db->like('code',$params['code']);
				unset($params['code']);
			}
			if(isset($params['payment_status']) AND $params['payment_status']=='process') {
				$this->db->where('status_pesanan','waiting-payment');
				$this->db->where("(status is null or status != 'declined')",null,false);
				unset($params['payment_status']);
			}

			if(isset($params['payment_status']) AND in_array($this->input->get('payment_status'),array('completed','expired','cancelled'))) {
				// $this->db->where("status_pesanan IN ('".implode("','",array('expired','completed','cancelled'))."') OR status = 'declined')",null,false);
				$this->db->where('status_pesanan',$this->input->get('payment_status'));
				unset($params['payment_status']);
			}

			if(isset($params['payment_status']) AND $this->input->get('payment_status')=='declined') {
				$this->db->where('status',$this->input->get('payment_status'));
				unset($params['payment_status']);
			}

			if(isset($params['payment_status']) AND $this->input->get('payment_status')=='all') {
				unset($params['payment_status']);
			}

			foreach ($params as $field => $value) {
				if(is_array($value)) {
					$this->db->where_in($field, $value);
				} elseif(is_string($value)) {
					$this->db->where($field, $value);
				}
			}
		}

		if($include_batal == 0) {
			$this->db->not_like('status_pesanan','cancelled');
		}

		$this->db->join('default_commerce_payment','default_commerce_order.id=default_commerce_payment.id_order','left');
		$this->db->join('default_commerce_voucher','default_commerce_order.voucher_id=default_commerce_voucher.id','left');
		$this->db->join('default_users','default_commerce_order.created_by=default_users.id');
		$this->db->join('default_profiles','default_users.id=default_profiles.user_id','left');
		$query = $this->db->get('default_commerce_order');
		$result = $query->result_array();
        return $result;
	}

	public function get_order_by_many($params)
	{
		$this->db->select('default_commerce_order.*');
		$this->db->select('default_commerce_payment.id as id_payment, default_commerce_payment.status status_pembayaran, payment_method, ipaymu_url, resend_attempt');
		$this->db->select('default_commerce_payment.id_bukti');
		$this->db->select('default_files.filename');
		$this->db->select('default_commerce_payment.amazon_filename');
		$this->db->select('default_commerce_payment.created_on waktu_konfirmasi_pembayaran');

		foreach ($params as $field => $value) {
			if(is_array($value)) {
				$this->db->where_in($field, $value);
			} else {
				$this->db->where($field, $value);
			}
		}
		$this->db->join('default_commerce_payment','default_commerce_order.id=default_commerce_payment.id_order','left');
		$this->db->join('default_files','default_commerce_payment.id_bukti=default_files.id','left');

		$this->db->order_by('tanggal_order','desc');
		$this->db->order_by('placed_on','desc');
		$query = $this->db->get('default_commerce_order');
		$result = $query->row_array();

		return $result;
	}

	public function get_order_product($id_order) {
		$this->db->select('*');
		$this->db->where('id_order',$id_order);

		$this->db->join('default_commerce_produk','default_commerce_cart.id_produk=default_commerce_produk.id');
		$query = $this->db->get('default_commerce_cart');
		$result = $query->result_array();
		return $result;
	}

	public function get_order_by_id($id)
	{
		$this->db->select('default_commerce_order.*');
		$this->db->select('default_commerce_payment.status status_pembayaran, payment_method, ipaymu_url');
		$this->db->select('default_commerce_payment.id_bukti');
		$this->db->select('default_files.filename');
		$this->db->select('default_commerce_payment.amazon_filename');
		$this->db->select('default_commerce_payment.created_on waktu_konfirmasi_pembayaran');

		$this->db->where('default_commerce_order.id', $id);


		$this->db->join('default_commerce_payment','default_commerce_order.id=default_commerce_payment.id_order','left');
		$this->db->join('default_files','default_commerce_payment.id_bukti=default_files.id','left');

		$query = $this->db->get('default_commerce_order');
		$result = $query->row_array();

		return $result;
	}

	public function count_all_order($params = NULL, $include_batal = 1)
	{
		if($params != NULL) {
			if(isset($params['tanggal_order'])) {
				$this->db->where("(tanggal_order like '%".$params['tanggal_order']."%' OR placed_on like '%".$params['tanggal_order']."%' OR default_commerce_order.created_on like '%".$params['tanggal_order']."%')",null,false);
				unset($params['tanggal_order']);
			}
			if(isset($params['display_name'])) {
				$this->db->where("(email = '".$params['display_name']."' OR display_name like '%".str_replace("'", "''", $params['display_name'])."%')",null,false);
				unset($params['display_name']);
			}
			if(isset($params['code'])) {
				$this->db->like('code',$params['code']);
				unset($params['code']);
			}
			if(isset($params['payment_status']) AND $params['payment_status']=='process') {
				$this->db->where('status_pesanan','waiting-payment');
				$this->db->where("(status is null or status != 'declined')",null,false);
				unset($params['payment_status']);
			}

			if(isset($params['payment_status']) AND in_array($this->input->get('payment_status'),array('completed','expired','cancelled'))) {
				// $this->db->where("status_pesanan IN ('".implode("','",array('expired','completed','cancelled'))."') OR status = 'declined')",null,false);
				$this->db->where('status_pesanan',$this->input->get('payment_status'));
				unset($params['payment_status']);
			}

			if(isset($params['payment_status']) AND $this->input->get('payment_status')=='declined') {
				$this->db->where('status',$this->input->get('payment_status'));
				unset($params['payment_status']);
			}

			if(isset($params['payment_status']) AND $this->input->get('payment_status')=='all') {
				unset($params['payment_status']);
			}

			foreach ($params as $field => $value) {
				if(is_array($value)) {
					$this->db->where_in($field, $value);
				} elseif(is_string($value)) {
					$this->db->where($field, $value);
				}
			}
		}

		if($include_batal == 0) {
			$this->db->not_like('status_pesanan','cancelled');
		}

		$this->db->join('default_commerce_payment','default_commerce_order.id=default_commerce_payment.id_order','left');
		$this->db->join('default_commerce_voucher','default_commerce_order.voucher_id=default_commerce_voucher.id','left');
		$this->db->join('default_users','default_commerce_order.created_by=default_users.id');
		$this->db->join('default_profiles','default_users.id=default_profiles.user_id','left');
		$result = $this->db->count_all_results('commerce_order');
		return $result;
	}

	public function delete_order_by_id($id)
	{
		$this->db->where('id_order', $id);
		$this->db->delete('default_commerce_payment');

		$this->db->where('id_order', $id);
		$this->db->delete('default_commerce_cart');

		$this->db->where('id', $id);
		$this->db->delete('default_commerce_order');
	}

	public function insert_order($order, $cart = NULL)
	{
		$this->db->trans_start();


		if(isset($this->current_user->id)) {
			$this->update_order(array('id_customer'=> $this->current_user->id), array('id_session'=>$this->session->userdata('session_id')));

			$this->db->where('id_customer',$this->current_user->id);
			$this->db->where('status_pesanan','in-cart');
			$this->db->like('tanggal_order',date('Y-m-d'));
		} else {
			$this->db->where('id_session',$this->session->userdata('session_id'));
		}

		$order_res = $this->db->get('default_commerce_order')->row_array();
		if($order_res == NULL) {
			$order['created_on'] = date("Y-m-d H:i:s");
			if(isset($this->current_user->id)) {
				$order['id_customer'] = $this->current_user->id;
				$order['created_by'] = $this->current_user->id;
			}

			$order['tanggal_order'] = date('Y-m-d H:i:s');

			$this->db->insert('default_commerce_order', $order);
			$id_order = $this->db->insert_id();
		} else {
			$id_order = $order_res['id'];
			if(isset($this->current_user->id)) {
				$order['created_by'] = $this->current_user->id;
				$order['id_customer'] = $this->current_user->id;
			}

			$this->db->where('id',$id_order);
			$this->db->update('default_commerce_order',$order);
		}
		if($cart != NULL) {
			foreach ($cart as $key => $produk) {
				$this->db->where('id_order',$id_order);
				$this->db->where('id_produk',$cart[$key]['id_produk']);
				$cart_res = $this->db->get('default_commerce_cart')->row_array();
				if($cart_res == NULL) {
					$cart[$key]['created_on'] = date("Y-m-d H:i:s");
					$cart[$key]['created_by'] = $this->current_user->id;
					$cart[$key]['id_order'] = $id_order;
					$this->db->insert('default_commerce_cart', $cart[$key]);
					$id_cart = $id_order;
				} else {
					$cart[$key]['created_by'] = $this->current_user->id;
					$this->db->where('id_order',$cart_res['id_order']);
					$this->db->update('default_commerce_cart', $cart[$key]);
					$id_cart = $cart_res['id_order'];
				}
			}
		}

		$this->db->trans_complete();
		return $id_order;
	}

	public function update_order($values, $by)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		if(array_key_exists("id", $by)) {
			$this->db->where('id', $by['id']);
		}
		if(array_key_exists("id_session", $by)) {
			$this->db->where('id_session', $by['id_session']);
		}
		if(array_key_exists("status_pesanan", $by)) {
			$this->db->where('status_pesanan', $by['status_pesanan']);
		}
		$this->db->set($values,false);
		return $this->db->update('default_commerce_order');
	}

	function generate_unique() {
		$this->db->select('max(SUBSTR(no_invoice,-3)) AS no_invoice',false);
		$this->db->like('tanggal_order',date('Y-m-d'));
		$order = $this->db->get('default_commerce_order')->row_array();
		if($order['no_invoice']==NULL) {
			$no_urut = 0;
		} else {
			$no_urut = $order['no_invoice'];
		}

		$user_code = (isset($this->current_user->id)) ? $this->current_user->id : 'INST';
		$data['no_invoice'] = Settings::get('invoice_prefix').'/'.date('dmy').'/'.$user_code.'/'.str_pad($no_urut+1, 3,'0',STR_PAD_LEFT);

		$this->db->like('tanggal_order',date('Y-m-d'));
		$this->db->where('status_pesanan','in-cart');
		$order = $this->db->get('default_commerce_order')->result_array();
		$exist = array();
		foreach ($order as $ord) {
			$exist[] = $ord['kode_unik'];
		}
		$unique = mt_rand(100, 999);
		if(!in_array($unique, $exist) ) {
			$data['kode_unik'] = $unique;
		}

		return $data;
	}

	function new_order($values)
	{
		$this->db->trans_start();

		$this->db->trans_complete();
	}

	public function get_current_order($with_idcustomer = NULL, $add_status = NULL, $order_type = NULL)
	{

		if($with_idcustomer != NULL) {
			$this->db->where("(id_customer = ".$with_idcustomer." or id_session = '".$this->session->userdata('cart_session')."')");
		}else{
			$this->db->where('id_session',$this->session->userdata('cart_session'));
		}

		if(!isset($add_status)) {
			$this->db->where('status_pesanan','in-cart');
		}
		if(isset($add_status)) {
			$this->db->where('status_pesanan',$add_status);
		}

		if(isset($order_type)) {
			$this->db->where('order_type',$order_type);
		}

		$this->db->order_by('created_on','desc');
		$order = $this->db->get('commerce_order')->row_array();

		return $order;
	}

	public function get_my_current_order($pagination_config = NULL)
	{
		$this->db->select('default_commerce_order.*');
		$this->db->select('default_commerce_payment.status status_pembayaran, payment_method, ipaymu_url');

		$this->db->where('id_customer',$this->current_user->id);
		// $this->db->where("((status_pesanan = 'waiting-payment' AND (default_commerce_payment. STATUS IS NULL OR default_commerce_payment. STATUS = '' OR default_commerce_payment. STATUS = 'unpaid') ) OR ( 	status_pesanan = 'pending' AND default_commerce_payment. STATUS LIKE 'pending' ) OR (status_pesanan = 'completed' AND default_commerce_payment. STATUS LIKE 'pending' ) OR (status_pesanan = 'completed' AND default_commerce_payment. STATUS LIKE 'paid' ) OR ( status_pesanan = 'completed' ))",NULL,false);
		$this->db->where_not_in('status_pesanan',array('completed','cancelled','expired'));

		$this->db->join('default_commerce_payment','default_commerce_order.id=default_commerce_payment.id_order','left');

		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		$this->db->order_by('tanggal_order','desc');
		$this->db->order_by('placed_on','desc');

		$query = $this->db->get('default_commerce_order');
		$result = $query->result_array();

		return $result;
	}

	public function get_my_finished_order($pagination_config = NULL)
	{
		$this->db->select('default_commerce_order.*');
		$this->db->select('default_commerce_payment.status status_pembayaran, payment_method, ipaymu_url');

		$this->db->where('id_customer',$this->current_user->id);
		// $this->db->where("((status_pesanan like '%expired%') OR (status_pesanan like '%cancelled%') OR (status_pesanan = 'completed' AND default_commerce_payment. STATUS LIKE '%declined%') OR (status_pesanan = 'completed'))",NULL,false);
		$this->db->where_in('status_pesanan',array('completed','cancelled','expired'));

		$this->db->join('default_commerce_payment','default_commerce_order.id=default_commerce_payment.id_order','left');

		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		$this->db->order_by('tanggal_order','desc');
		$this->db->order_by('placed_on','desc');

		$query = $this->db->get('default_commerce_order');
		$result = $query->result_array();

		return $result;
	}

	public function check_affiliate_order($id_customer) {
		$this->db->select('default_commerce_order.*');
		$this->db->select('default_commerce_payment.status status_pembayaran, payment_method, ipaymu_url');


		$this->db->where('id_customer',$id_customer);
		$this->db->where("((default_commerce_payment.status LIKE 'paid') OR (status_pesanan = 'completed'))",NULL,false);

		$this->db->join('default_commerce_payment','default_commerce_order.id=default_commerce_payment.id_order','left');

		$query = $this->db->get('default_commerce_order');
		$result = $query->result_array();

		return $result;
	}

	function expired_order($time) {
		// first lets get all order which will be update
		$this->db->select('commerce_order.*,users.email');
		$this->db->where("expired_on <='".$time."'",null,false);
		$this->db->where_in('status_pesanan',array('in-cart','waiting-payment'));
		$this->db->where("(`status` NOT IN ('pending') or (status = 'pending' AND payment_method='ipaymu'))",null,false);
		$this->db->join('users','users.id=commerce_order.id_customer','left');
		$this->db->join('commerce_payment','commerce_order.id=commerce_payment.id_order');
		$orders = $this->db->get('commerce_order')->result_array();

		// take out the id for update and email for notif
		foreach ($orders as $key=>$order) {
			$update_id[$key] = $order['id'];
			$expired_email[$key]['id'] = $order['id'];
			$expired_email[$key]['email'] = $order['email'];
			$expired_email[$key]['no_invoice'] = $order['no_invoice'];
		}

		//let's update the order
		if(isset($update_id)) {
			$this->db->where_in('id',$update_id);
			$result = $this->db->update('commerce_order',array('status_pesanan'=>'expired'));
		}
		return (isset($result)) ? $expired_email : array();
	}

	public function get_membership_album($id_user)
	{
		$this->db->select('gallery_album.*');
		$this->db->where('id_customer', $id_user);
		$this->db->where('order_type','membership');
		$this->db->where('status_pesanan','completed');

		$this->db->join('commerce_cart','commerce_order.id=commerce_cart.id_order');
		$this->db->join('commerce_membership_album','commerce_cart.id_produk=commerce_membership_album.id_membership');
		$this->db->join('gallery_album','commerce_membership_album.id_album=gallery_album.id');

		$this->db->group_by('gallery_album.id');

		$result = $this->db->get('commerce_order');

		return $result->result_array();
	}
}
