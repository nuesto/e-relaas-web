<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * voucher model
 *
 * @author Aditya Satrya
 */
class voucher_m extends MY_Model {
	
	public function get_voucher($pagination_config = NULL, $filters = NULL)
	{
		$this->db->select('default_commerce_voucher.*');
		$this->db->select('count(default_commerce_order.id) as usage_count',false);
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		if(isset($filters['created_by'])) {
			$this->db->where('(default_commerce_voucher.created_by = '.$filters['created_by'].' OR affiliate_id = '.$filters['created_by'].')',null,false);
		}

		if(isset($filters['code'])) {
			$this->db->like('code',$filters['code']);
		}

		if(isset($filters['benefit_type'])) {
			$this->db->where('benefit_type',$filters['benefit_type']);
		}

		if(isset($filters['affiliate_id'])) {
			$this->db->where('affiliate_id',$filters['affiliate_id']);
		}

		if(isset($filters['state_active'])) {
			if($this->input->get('f-state_active')==0) {
				$this->db->where('state_active',$this->input->get('f-state_active'));
			} elseif ($this->input->get('f-state_active')==1) {
				$this->db->where('state_active',$this->input->get('f-state_active'));
				$this->db->where('valid_unit > DATE(NOW())',null, false);
			} elseif ($this->input->get('f-state_active')==2) {
				$this->db->where('valid_unit < DATE(NOW())',null, false);
				$this->db->having('count(default_commerce_order.id)',0);
			} elseif ($this->input->get('f-state_active')==3) {
				$this->db->where_in('status_pesanan',array('completed'));
				$this->db->having('count(default_commerce_order.id) >','0');
			} elseif ($this->input->get('f-state_active')==4) {
				$this->db->where_in('status_pesanan',array('in-cart','waiting-payment'));
				$this->db->having('count(default_commerce_order.id) >','0');
			}
		}

		$this->db->join('default_commerce_order','default_commerce_voucher.id=default_commerce_order.voucher_id','left');
		$this->db->group_by('default_commerce_voucher.id');

		$this->db->order_by('valid_unit,code');
		
		$query = $this->db->get('default_commerce_voucher');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_voucher_by_id($id)
	{
		$this->db->select('default_commerce_voucher.*');
		$this->db->select('count(default_commerce_order.id) as usage_count',false);

		$this->db->join('default_commerce_order','default_commerce_voucher.id=default_commerce_order.voucher_id','left');
		$this->db->group_by('default_commerce_voucher.id');

		$this->db->order_by('valid_unit,code');

		$this->db->where('default_commerce_voucher.id', $id);
		$query = $this->db->get('default_commerce_voucher');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_voucher_by_code($code)
	{
		$this->db->select('default_commerce_voucher.*');
		$this->db->select('count(default_commerce_order.id) as usage_count',false);

		$this->db->where('code', $code);

		$this->db->join('default_commerce_order','default_commerce_voucher.id=default_commerce_order.voucher_id','left');
		$this->db->group_by('default_commerce_voucher.id');

		$query = $this->db->get('default_commerce_voucher');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_voucher_usage($filters)
	{
		$this->db->select('default_commerce_voucher.*');
		$this->db->select('count(default_commerce_order.id) as usage_count',false);

		if(isset($filters['code'])) {
			$this->db->where('code', $filters['code']);
		}

		if(isset($filters['id_customer'])) {
			$this->db->where('id_customer', $filters['id_customer']);
		}

		$this->db->join('default_commerce_order','default_commerce_voucher.id=default_commerce_order.voucher_id','left');
		$this->db->group_by('default_commerce_voucher.id');

		$query = $this->db->get('default_commerce_voucher');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_voucher($filters = NULL)
	{
		$this->db->select('default_commerce_voucher.*');
		$this->db->select('count(default_commerce_order.id) as usage_count',false);

		if(isset($filters['created_by'])) {
			$this->db->where('(default_commerce_voucher.created_by = '.$filters['created_by'].' OR affiliate_id = '.$filters['created_by'].')',null,false);
		}

		if(isset($filters['code'])) {
			$this->db->like('code',$filters['code']);
		}

		if(isset($filters['benefit_type'])) {
			$this->db->where('benefit_type',$filters['benefit_type']);
		}

		if(isset($filters['affiliate_id'])) {
			$this->db->where('affiliate_id',$filters['affiliate_id']);
		}

		if(isset($filters['state_active'])) {
			if($this->input->get('f-state_active')==0) {
				$this->db->where('state_active',$this->input->get('f-state_active'));
			} elseif ($this->input->get('f-state_active')==1) {
				$this->db->where('state_active',$this->input->get('f-state_active'));
				$this->db->where('valid_unit > DATE(NOW())',null, false);
			} elseif ($this->input->get('f-state_active')==2) {
				$this->db->where('valid_unit < DATE(NOW())',null, false);
				$this->db->having('count(default_commerce_order.id)',0);
			} elseif ($this->input->get('f-state_active')==3) {
				$this->db->where_in('status_pesanan',array('completed'));
				$this->db->having('count(default_commerce_order.id) >','0');
			} elseif ($this->input->get('f-state_active')==4) {
				$this->db->where_in('status_pesanan',array('in-cart','waiting-payment'));
				$this->db->having('count(default_commerce_order.id) >','0');
			}
		}

		$this->db->join('default_commerce_order','default_commerce_voucher.id=default_commerce_order.voucher_id','left');
		$this->db->group_by('default_commerce_voucher.id');
		$query = $this->db->get('default_commerce_voucher');
		$result = $query->result_array();

		return count($result);
	}

	public function get_template_voucher()
	{
		$this->db->where('is_affiliate_template',1);
		$this->db->where("(valid_unit >= '".date('Y-m-d')."' OR valid_unit IS NULL)");
		$result = $this->db->get('commerce_voucher');
		return $result->result_array();
	}
	
	public function delete_voucher_by_id($id)
	{
		$entry = $this->get_voucher_by_id($id);

		if($entry['usage_count']>0) {
			return false;
		}

		$this->db->where('id', $id);
		return $this->db->delete('default_commerce_voucher');
	}
	
	public function insert_voucher($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_commerce_voucher', $values);
	}
	
	public function update_voucher($values, $row_id)
	{
		$values['last_edited_on'] = date("Y-m-d H:i:s");
		$values['last_edited_by'] = $this->current_user->id;
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_commerce_voucher', $values); 
	}
	
}