<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Payment model
 *
 * @author Aditya Satrya
 */
class Payment_m extends MY_Model {
	
	public function get_payment($pagination_config = NULL)
	{
		$this->db->select('default_commerce_order.*');
		$this->db->select('default_commerce_payment.id,default_commerce_payment.status,default_commerce_payment.id_order,default_commerce_payment.payment_method,default_commerce_payment.billed_on,default_commerce_payment.paid_on,default_commerce_payment.jumlah_pembayaran,default_commerce_payment.history,default_commerce_payment.ordering_count,default_commerce_payment.tanggal_transfer,default_commerce_payment.nama_rekening_pengirim,default_commerce_payment.bank_rekening_pengirim,default_commerce_payment.bank_rekening_tujuan,default_commerce_payment.no_rekening_pengirim,default_commerce_payment.keterangan,default_commerce_payment.id_bukti,default_commerce_payment.amazon_filename,default_commerce_payment.closed_on,default_commerce_payment.closed_by,default_commerce_payment.transfer_verification_status,default_commerce_payment.transfer_verification_note,default_commerce_payment.ipaymu_session_id,default_commerce_payment.ipaymu_url,default_commerce_payment.ipaymu_trx_id,default_commerce_payment.ipaymu_data,default_commerce_payment.ipaymu_type,default_commerce_payment.ipaymu_status_code,default_commerce_payment.ipaymu_status_name,default_commerce_payment.ipaymu_status_last_updated_on,default_commerce_payment.transfer_verified_by,default_commerce_payment.transfer_verified_on,default_commerce_payment.transfer_confirmed_by,default_commerce_payment.transfer_confirmed_on');
		$this->db->select('default_commerce_rekening.nama_bank, default_commerce_rekening.no_rekening, default_commerce_rekening.atas_nama');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_commerce_payment');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_payment_by_id($id)
	{
		$this->db->select('default_commerce_order.*');
		$this->db->select('default_commerce_payment.id,default_commerce_payment.status,default_commerce_payment.id_order,default_commerce_payment.payment_method,default_commerce_payment.billed_on,default_commerce_payment.paid_on,default_commerce_payment.jumlah_pembayaran,default_commerce_payment.history,default_commerce_payment.ordering_count,default_commerce_payment.tanggal_transfer,default_commerce_payment.nama_rekening_pengirim,default_commerce_payment.bank_rekening_pengirim,default_commerce_payment.bank_rekening_tujuan,default_commerce_payment.no_rekening_pengirim,default_commerce_payment.keterangan,default_commerce_payment.id_bukti,default_commerce_payment.amazon_filename,default_commerce_payment.closed_on,default_commerce_payment.closed_by,default_commerce_payment.transfer_verification_status,default_commerce_payment.transfer_verification_note,default_commerce_payment.ipaymu_session_id,default_commerce_payment.ipaymu_url,default_commerce_payment.ipaymu_trx_id,default_commerce_payment.ipaymu_data,default_commerce_payment.ipaymu_type,default_commerce_payment.ipaymu_status_code,default_commerce_payment.ipaymu_status_name,default_commerce_payment.ipaymu_status_last_updated_on,default_commerce_payment.transfer_verified_by,default_commerce_payment.transfer_verified_on,default_commerce_payment.transfer_confirmed_by,default_commerce_payment.transfer_confirmed_on');
		$this->db->select('default_commerce_rekening.nama_bank, default_commerce_rekening.no_rekening, default_commerce_rekening.atas_nama');
		$this->db->select('default_files.filename');
		$this->db->where('id_order', $id);

		$this->db->join('default_commerce_order','default_commerce_order.id=default_commerce_payment.id_order');
		$this->db->join('default_commerce_rekening','default_commerce_rekening.id=default_commerce_payment.bank_rekening_tujuan','left');
		$this->db->join('default_files','default_files.id=default_commerce_payment.id_bukti','left');

		$query = $this->db->get('default_commerce_payment');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_payment_by_order($id)
	{
		$this->db->select('*');
		$this->db->where('id_order', $id);
		$query = $this->db->get('default_commerce_payment');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_payment_by_id_payment($id)
	{
		$this->db->select('default_commerce_payment.*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_commerce_payment');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_payment_by_many($params)
	{
		$this->db->select('*');
		foreach ($params as $field => $value) {
			$this->db->where($field, $value);
		}
		$query = $this->db->get('default_commerce_payment');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_ipaymu_payment($params=NULL)
	{
		if(isset($params['ipaymu_status_code'])) {
			$this->db->where_in('ipaymu_status_code',$params['ipaymu_status_code']);
		}

		$this->db->where('payment_method','ipaymu');

		$result = $this->db->get('default_commerce_payment');

		return $result->result_array();
	}
	
	public function count_all_payment()
	{
		return $this->db->count_all('commerce_payment');
	}
	
	public function delete_payment_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_commerce_payment');
	}
	
	public function insert_payment($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_commerce_payment', $values);
	}
	
	public function update_payment($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		if(isset($this->current_user->id)) {
			$values['updated_by'] = $this->current_user->id;
		}
		
		$this->db->where('id_order', $row_id);
		return $this->db->update('default_commerce_payment', $values); 
	}

	public function update_payment_by_id_payment($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		if(isset($this->current_user->id)) {
			$values['updated_by'] = $this->current_user->id;
		}
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_commerce_payment', $values); 
	}
	
}