<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * pengiriman model
 *
 * @author Aditya Satrya
 */
class pengiriman_m extends MY_Model {
	
	public function get_pengiriman($pagination_config = NULL, $params = NULL)
	{
		$this->db->select('*');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		if($params != NULL) {
			foreach ($params as $field => $value) {
				$this->db->where($field,$value);
			}
		}
		
		$query = $this->db->get('default_commerce_pengiriman');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_pengiriman_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_commerce_pengiriman');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_pengiriman($params = NULL)
	{
		if($params != NULL) {
			foreach ($params as $field => $value) {
				$this->db->where($field,$value);
			}
		}

		return $this->db->count_all_results('commerce_pengiriman');
	}
	
	public function delete_pengiriman_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_commerce_pengiriman');
	}
	
	public function insert_pengiriman($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_commerce_pengiriman', $values);
	}
	
	public function update_pengiriman($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_commerce_pengiriman', $values); 
	}
	
}