<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Produk model
 *
 * @author Aditya Satrya
 */
class Produk_m extends MY_Model {

	public function get_produk($pagination_config = NULL, $filters = NULL)
	{
		$this->db->select('default_commerce_produk.*, group_concat(nama_kurir) nama_kurir, default_files.filename filename');

		if($pagination_config != null) {
			$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
			$this->db->limit($pagination_config['per_page'], $start);
		}

		if(isset($filters) and is_array($filters)) {
			foreach ($filters as $key => $value) {
				if(is_array($value)) {
					$type = isset($value['type']) ? $value['type'] : 'where';
					$escape = isset($value['escape']) ? $value['escape'] : true;
					$this->db->{$type}($key,$value['value'],$escape);
				} else {
					$this->db->where($key,$value);
				}
			}
		}

		$this->db->join('default_commerce_pengiriman_produk','default_commerce_produk.id=default_commerce_pengiriman_produk.id_produk','left');
		$this->db->join('default_commerce_pengiriman','default_commerce_pengiriman.id=default_commerce_pengiriman_produk.id_pengiriman','left');
		$this->db->join('default_files','default_files.id=default_commerce_produk.id_file');

		$this->db->group_by('default_commerce_produk.id');
		$query = $this->db->get('default_commerce_produk');
		$result = $query->result_array();

        return $result;
	}

	public function get_produk_by_id($id)
	{
		$this->db->select('default_commerce_produk.*,group_concat(id_pengiriman) pengiriman,default_files.filename filename');
		$this->db->where('default_commerce_produk.id', $id);
		$this->db->join('default_commerce_pengiriman_produk','default_commerce_produk.id=default_commerce_pengiriman_produk.id_produk','left');
		$this->db->join('default_files','default_files.id=default_commerce_produk.id_file');

		$this->db->group_by('default_commerce_produk.id');

		$query = $this->db->get('default_commerce_produk');
		$result = $query->row_array();

		return $result;
	}

	public function get_produk_by_ids($ids, $is_external = false)
	{
		$this->db->select('default_commerce_produk.*, group_concat(nama_kurir) nama_kurir,default_files.filename filename');

		if(!$is_external) {
			$this->db->where_in('default_commerce_produk.id',$ids);
		} else {
			$this->db->where_in('default_commerce_produk.id_external_product',$ids);
		}

		$this->db->join('default_commerce_pengiriman_produk','default_commerce_produk.id=default_commerce_pengiriman_produk.id_produk','left');
		$this->db->join('default_commerce_pengiriman','default_commerce_pengiriman.id=default_commerce_pengiriman_produk.id_pengiriman','left');
		$this->db->join('default_files','default_files.id=default_commerce_produk.id_file');

		$this->db->group_by('default_commerce_produk.id');

		$query = $this->db->get('default_commerce_produk');
		$result = $query->result_array();

        return $result;
	}

	public function count_all_produk()
	{
		return $this->db->count_all('commerce_produk');
	}

	public function delete_produk_by_id($id)
	{
		$this->db->where('id_produk',$id);
		$this->db->delete('default_commerce_pengiriman_produk');

		$this->db->where('id', $id);
		$this->db->delete('default_commerce_produk');
	}

	public function insert_produk($values)
	{
		$pengiriman = $values['pengiriman'];
		unset($values['pengiriman']);

		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;

		$this->db->insert('default_commerce_produk', $values);
		$row_id = $this->db->insert_id();

		$this->db->where('id_produk',$row_id);
		$this->db->delete('default_commerce_pengiriman_produk');

		foreach ($pengiriman as $kurir) {
			$this->db->insert('default_commerce_pengiriman_produk',array('id_produk'=>$row_id,'id_pengiriman'=>$kurir));
		}

		return true;
	}

	public function update_produk($values, $row_id)
	{
		$this->db->where('id_produk',$row_id);
		$this->db->delete('default_commerce_pengiriman_produk');

		foreach ($values['pengiriman'] as $pengiriman) {
			$this->db->insert('default_commerce_pengiriman_produk',array('id_produk'=>$row_id,'id_pengiriman'=>$pengiriman));
		}
		unset($values['pengiriman']);

		$values['updated_on'] = date("Y-m-d H:i:s");

		$this->db->where('id', $row_id);
		return $this->db->update('default_commerce_produk', $values);
	}

	public function get_membership_by_album($id)
	{
		$this->db->where('id_album', $id);
		$result = $this->db->get('commerce_membership_album');
		return $result->result_array();
	}
}
