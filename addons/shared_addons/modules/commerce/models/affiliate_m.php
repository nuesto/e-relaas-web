<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * affiliate model
 *
 * @author Aditya Satrya
 */
class affiliate_m extends MY_Model {
	
	public function get_affiliate()
	{
		$this->db->select('default_users.*');
		$this->db->select('display_name, no_handphone, jenis_kelamin, id_kota, kode_pos, alamat');
		$this->db->select('default_landing_page_konten.uri_segment_string, default_landing_page_affiliate.uri_segment_string affiliate_uri');

		$this->db->where('is_affiliate','1');
		
		$this->db->join('default_profiles','default_users.id=default_profiles.user_id');
		$this->db->join('default_landing_page_affiliate','default_users.id=default_landing_page_affiliate.id_affiliate');
		$this->db->join('default_landing_page_konten','default_landing_page_konten.id=default_landing_page_affiliate.id_landing_page');

		$query = $this->db->get('default_users');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_affiliate_by_id($id)
	{
		$this->db->select('*');

		$this->db->where('group_id',9);
		$this->db->where('default_users.id', $id);
		$this->db->join('default_profiles','default_users.id=default_profiles.user_id');
		$query = $this->db->get('default_users');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_affiliate()
	{
		$this->db->where('group_id',9);
		$this->db->join('default_profiles','default_users.id=default_profiles.user_id');
		return $this->db->count_all_results('default_users');
	}
	
	public function get_commission($filters = NULL)
	{
		$with_range = '';
		if(isset($filters['range'])) {
			$with_range = "AND (default_commerce_order.closed_on BETWEEN '".$filters['range']['range_start']." 00:00:00' AND '".$filters['range']['range_end']." 23:59:59' OR default_commerce_order.updated_on BETWEEN '".$filters['range']['range_start']." 00:00:00' AND '".$filters['range']['range_end']." 23:59:59')";
		}

		$result = $this->db->query("SELECT
		default_users.`id` user_id,
		email,
		display_name,
		affiliate.*,
		jumlah_semua_pesanan
		FROM
		default_users
		JOIN default_profiles ON (
		default_users.id = default_profiles.user_id
		)
		LEFT JOIN (
		SELECT
		default_commerce_order.id_affiliate,
		count(*) AS jumlah_pesanan,
		sum((biaya_order - ifnull(discount_nominal,0)) + ifnull(biaya_pengiriman,0) + ifnull(kode_unik,0)) AS total_penjualan,
		0.25 * sum((biaya_order - ifnull(discount_nominal,0)) + ifnull(biaya_pengiriman,0) + ifnull(kode_unik,0)) AS komisi
		FROM
		default_commerce_order
		JOIN default_commerce_payment ON (
		default_commerce_order.id = default_commerce_payment.id_order
		)
		WHERE
		default_commerce_payment.`status` LIKE 'paid' ".$with_range."
		GROUP BY
		id_affiliate
		) affiliate ON (
		default_users.id = affiliate.id_affiliate
		)
		LEFT JOIN (
		SELECT
		default_commerce_order.id_affiliate,
		count(*) AS jumlah_semua_pesanan
		FROM
		default_commerce_order
		JOIN default_commerce_payment ON (
		default_commerce_order.id = default_commerce_payment.id_order
		)
		WHERE 
		default_commerce_order.`id_affiliate` is not null ".$with_range."
		GROUP BY
		id_affiliate
		) all_order ON (
		default_users.id = all_order.id_affiliate
		)
		WHERE
		default_profiles.is_affiliate = '1'
		ORDER BY
		jumlah_pesanan DESC");
		
		return $result->result_array();
	}

	public function get_commission_non_affiliate($filters=NULL)
	{
		$with_range = '';
		if(isset($filters['range'])) {
			$with_range = "AND (default_commerce_order.closed_on BETWEEN '".$filters['range']['range_start']." 00:00:00' AND '".$filters['range']['range_end']." 23:59:59' OR default_commerce_order.updated_on BETWEEN '".$filters['range']['range_start']." 00:00:00' AND '".$filters['range']['range_end']." 23:59:59')";
		}

		$result = $this->db->query("SELECT
			paid.*, all_order.jumlah_semua_pesanan
			FROM
			(
			SELECT
			'0' id_affiliate,
			count(*) AS jumlah_pesanan,
			sum(
			(
			biaya_order - ifnull(discount_nominal, 0)
			) + ifnull(biaya_pengiriman, 0) + ifnull(kode_unik, 0)
			) AS total_penjualan,
			0.25 * sum(
			(
			biaya_order - ifnull(discount_nominal, 0)
			) + ifnull(biaya_pengiriman, 0) + ifnull(kode_unik, 0)
			) AS komisi
			FROM
			default_commerce_order
			JOIN default_commerce_payment ON (
			default_commerce_order.id = default_commerce_payment.id_order
			)
			WHERE
			default_commerce_payment.`status` LIKE 'paid' ".$with_range."
			AND (
			default_commerce_order.id_affiliate IS NULL
			OR default_commerce_order.id_affiliate = 0
			)
			) paid
			LEFT JOIN (
			SELECT
			'0' id_affiliate,
			count(*) AS jumlah_semua_pesanan
			FROM
			default_commerce_order
			JOIN default_commerce_payment ON (
			default_commerce_order.id = default_commerce_payment.id_order
			)
			WHERE
			(default_commerce_order.id_affiliate is null or default_commerce_order.id_affiliate=0)
			".$with_range."
			) all_order ON (
			paid.id_affiliate = all_order.id_affiliate
			)
			ORDER BY
			jumlah_pesanan DESC");
		echo $this->db->last_query();
		return $result->result_array();
	}

	public function get_statistics($filters = NULL)
	{
		$with_range = '';
		if(isset($filters['range'])) {
			$with_range = "AND (default_commerce_order.placed_on BETWEEN '".$filters['range']['range_start']." 00:00:00' AND '".$filters['range']['range_end']." 23:59:59')";
		}

		$with_landing_page = '';
		if(isset($filters['id_landing_page'])) {
			$with_landing_page = 'AND id_landing_page='.$filters['id_landing_page'];
		}

		$result = $this->db->query("SELECT
		default_users.`id` user_id,
		email,
		display_name,
		affiliate.*,
		jumlah_semua_pesanan
		FROM
		default_users
		JOIN default_profiles ON (
		default_users.id = default_profiles.user_id
		)
		LEFT JOIN (
		SELECT
		default_commerce_order.id_affiliate,
		count(*) AS jumlah_pesanan,
		sum((biaya_order - ifnull(discount_nominal,0)) + ifnull(biaya_pengiriman,0) + ifnull(kode_unik,0)) AS total_penjualan,
		0.25 * sum((biaya_order - ifnull(discount_nominal,0)) + ifnull(biaya_pengiriman,0) + ifnull(kode_unik,0)) AS komisi
		FROM
		default_commerce_order
		JOIN default_commerce_payment ON (
		default_commerce_order.id = default_commerce_payment.id_order
		)
		WHERE
		default_commerce_payment.`status` LIKE 'paid' ".$with_range." ".$with_landing_page." 
		GROUP BY
		id_affiliate
		) affiliate ON (
		default_users.id = affiliate.id_affiliate
		)
		LEFT JOIN (
		SELECT
		default_commerce_order.id_affiliate,
		count(*) AS jumlah_semua_pesanan
		FROM
		default_commerce_order
		JOIN default_commerce_payment ON (
		default_commerce_order.id = default_commerce_payment.id_order
		)
		WHERE 
		default_commerce_order.`id_affiliate` is not null ".$with_range." ".$with_landing_page." 
		GROUP BY
		id_affiliate
		) all_order ON (
		default_users.id = all_order.id_affiliate
		)
		WHERE
		default_profiles.is_affiliate = '1'
		ORDER BY
		jumlah_pesanan DESC");
		
		return $result->result_array();
	}

	public function get_statistics_non_affiliate($filters=NULL)
	{
		$with_range = '';
		if(isset($filters['range'])) {
			$with_range = "AND (default_commerce_order.placed_on BETWEEN '".$filters['range']['range_start']." 00:00:00' AND '".$filters['range']['range_end']." 23:59:59')";
		}

		$with_landing_page = '';
		if(isset($filters['id_landing_page'])) {
			$with_landing_page = 'AND id_landing_page='.$filters['id_landing_page'];
		}

		$result = $this->db->query("SELECT
			paid.*, all_order.jumlah_semua_pesanan
			FROM
			(
			SELECT
			'0' id_affiliate,
			count(*) AS jumlah_pesanan,
			sum(
			(
			biaya_order - ifnull(discount_nominal, 0)
			) + ifnull(biaya_pengiriman, 0) + ifnull(kode_unik, 0)
			) AS total_penjualan,
			0.25 * sum(
			(
			biaya_order - ifnull(discount_nominal, 0)
			) + ifnull(biaya_pengiriman, 0) + ifnull(kode_unik, 0)
			) AS komisi
			FROM
			default_commerce_order
			JOIN default_commerce_payment ON (
			default_commerce_order.id = default_commerce_payment.id_order
			)
			WHERE
			default_commerce_payment.`status` LIKE 'paid' ".$with_range." ".$with_landing_page."
			AND (
			default_commerce_order.id_affiliate IS NULL
			OR default_commerce_order.id_affiliate = 0
			)
			) paid
			LEFT JOIN (
			SELECT
			'0' id_affiliate,
			count(*) AS jumlah_semua_pesanan
			FROM
			default_commerce_order
			JOIN default_commerce_payment ON (
			default_commerce_order.id = default_commerce_payment.id_order
			)
			WHERE
			(default_commerce_order.id_affiliate is null or default_commerce_order.id_affiliate=0)
			".$with_range." ".$with_landing_page."
			) all_order ON (
			paid.id_affiliate = all_order.id_affiliate
			)
			ORDER BY
			jumlah_pesanan DESC");
		return $result->result_array();
	}

	public function get_chart_data($filters=NULL)
	{
		$with_range = '';
		if(isset($filters['range'])) {
			$with_range = "AND (default_commerce_order.placed_on BETWEEN '".$filters['range']['range_start']." 00:00:00' AND '".$filters['range']['range_end']." 23:59:59')";
		}

		$with_affiliate = '';
		if(isset($filters['id_affiliate'])) {
			$with_affiliate = 'AND id_affiliate='.$filters['id_affiliate'];
		}

		$group_by = 'substr(placed_on, 1, 10)';
		if(isset($filters['group_by'])) {
			if($filters['group_by']=='month') {
				$group_by = 'substr(placed_on, 1, 7)';
			}
		}

		$with_landing_page = '';
		if(isset($filters['id_landing_page'])) {
			$with_landing_page = 'AND id_landing_page='.$filters['id_landing_page'];
		}

		$result = $this->db->query("select all_order.date, all_order.quantity all_order_quantity, completed_order.quantity completed_order_quantity, completed_order.total_penjualan
			from
			(SELECT
			".$group_by." date,
			count(*) quantity
			FROM
			default_commerce_order
			WHERE
			status_pesanan IS NOT NULL ".$with_range." ".$with_affiliate." ".$with_landing_page."
			GROUP BY
			".$group_by."
			) all_order
			left join 
			(SELECT
			".$group_by." date,
			count(*) quantity,
			sum(
			(
			biaya_order - ifnull(discount_nominal, 0)
			) + ifnull(biaya_pengiriman, 0) + ifnull(kode_unik, 0)
			) AS total_penjualan
			FROM
			default_commerce_order
			WHERE
			status_pesanan='completed' ".$with_range." ".$with_affiliate." ".$with_landing_page."
			GROUP BY
			".$group_by."
			) completed_order
			on (all_order.date=completed_order.date)");

		return $result->result_array();
	}
	
}