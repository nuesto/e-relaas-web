<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Cart model
 *
 * @author Aditya Satrya
 */
class Cart_m extends MY_Model {

	public function get_cart($pagination_config = NULL)
	{
		$this->db->select('*');

		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		$query = $this->db->get('default_commerce_cart');
		$result = $query->result_array();

        return $result;
	}

	public function get_cart_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_commerce_cart');
		$result = $query->row_array();

		return $result;
	}

	public function count_all_cart()
	{
		return $this->db->count_all('commerce_cart');
	}

	public function delete_cart_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_commerce_cart');
	}

	public function insert_cart($order_values,$cart_values,$order_type, $current_order = NULL)
	{
		$this->db->trans_start();

		if($current_order == NULL) {
			$order_values['created_on'] = date("Y-m-d H:i:s");
			if($this->session->userdata('id_affiliate') !== NULL) {
				$order_values['id_affiliate'] = $this->session->userdata('id_affiliate');
			}
			if(get_cookie('id_affiliate',true)) {
				$order_values['id_affiliate'] = get_cookie('id_affiliate',true);
			}

			if(isset($this->current_user->id)) {
				$order_values['created_by'] = $this->current_user->id;
			}

			$this->db->insert('commerce_order',$order_values);
			$id_order = $this->db->insert_id();
		} else {
			if(isset($this->current_user->id)) {
				unset($order_values['id_session']);
			}
			$this->db->where('id',$current_order['id']);
			$this->db->update('commerce_order',$order_values);
			$id_order = $current_order['id'];
		}

		// dump($id_order,$this->session->userdata('cart_session'));
		// die();

		foreach ($cart_values as $cart_value) {
			$this->db->where('id_order',$id_order);
			$this->db->where('id_produk',$cart_value['id_produk']);
			$cart = $this->db->get('commerce_cart')->row_array();

			if($cart_value['type']=='membership') {
				if(count($cart)<1) {
					unset($cart_value['is_digital_product']);
					unset($cart_value['type']);

					$cart_value['id_order'] = $id_order;
					$cart_value['created_on'] = date("Y-m-d H:i:s");
					$cart_value['id_unique'] = $this->generate_unique();

					$this->db->insert('commerce_cart',$cart_value);
				}
			}elseif ($cart_value['type']=='donation') {
				unset($cart_value['is_digital_product']);
				unset($cart_value['type']);

				if(isset($cart) AND $cart['id_order'] == $id_order AND $cart['id_produk'] == $cart_value['id_produk'] AND $cart['harga'] == $cart_value['harga']) {
					$this->db->where('id_order',$id_order);
					$this->db->where('id_produk',$cart_value['id_produk']);
					$this->db->where('harga',$cart_value['harga']);
					$this->db->delete('commerce_cart');
				}

				$cart_value['id_order'] = $id_order;
				$cart_value['created_on'] = date("Y-m-d H:i:s");
				$cart_value['id_unique'] = $this->generate_unique();

				$this->db->insert('commerce_cart',$cart_value);
			}elseif ($cart_value['type']=='physical') {
				unset($cart_value['is_digital_product']);
				unset($cart_value['type']);

				$cart_value['id_order'] = $id_order;
				$this->db->set('jumlah','(`jumlah`+'.$cart_value['jumlah'].')',false);
				unset($cart_value['jumlah']);
				$cart_value['created_on'] = date("Y-m-d H:i:s");
				$cart_value['id_unique'] = $this->generate_unique();

				if(count($cart)<1) {
					$this->db->insert('commerce_cart',$cart_value);
				} else {
					$this->db->where('id_order',$id_order);
					$this->db->where('id_produk',$cart_value['id_produk']);
					$this->db->update('commerce_cart',$cart_value);
				}
			}
		}

		$this->recalculate_biaya_order($id_order);

		$this->db->trans_complete();
		return (isset($id_order));
	}

	public function update_cart($cart_values, $id_order)
	{
		$this->db->trans_start();

		foreach ($cart_values as $cart_value) {
			$this->db->where('id_order',$id_order);
			$this->db->where('id_produk',$cart_value['id_produk']);
			$cart = $this->db->get('commerce_cart')->row_array();

			if($cart_value['type']=='membership') {
				if(count($cart)<1) {
					unset($cart_value['is_digital_product']);
					unset($cart_value['type']);

					$cart_value['id_order'] = $id_order;
					$cart_value['created_on'] = date("Y-m-d H:i:s");
					$cart_value['id_unique'] = $this->generate_unique();

					$this->db->insert('commerce_cart',$cart_value);
				}
			}elseif ($cart_value['type']=='donation') {
				unset($cart_value['is_digital_product']);
				unset($cart_value['type']);

				if(isset($cart) AND $cart['id_order'] == $id_order AND $cart['id_produk'] == $cart_value['id_produk'] AND $cart['harga'] == $cart_value['harga']) {
					$this->db->where('id_order',$id_order);
					$this->db->where('id_produk',$cart_value['id_produk']);
					$this->db->where('harga',$cart_value['harga']);
					$this->db->delete('commerce_cart');
				}

				$cart_value['id_order'] = $id_order;
				$cart_value['created_on'] = date("Y-m-d H:i:s");
				$cart_value['id_unique'] = $this->generate_unique();

				$this->db->insert('commerce_cart',$cart_value);
			}elseif ($cart_value['type']=='physical') {
				unset($cart_value['is_digital_product']);
				unset($cart_value['type']);

				$cart_value['id_order'] = $id_order;
				$cart_value['created_on'] = date("Y-m-d H:i:s");
				$cart_value['id_unique'] = $this->generate_unique();

				if(count($cart)<1) {
					$this->db->insert('commerce_cart',$cart_value);
				} else {
					$this->db->where('id_order',$id_order);
					$this->db->where('id_produk',$cart_value['id_produk']);
					$this->db->update('commerce_cart',$cart_value);
				}
			}

			$updated_id_product[] = $cart_value['id_produk'];
		}

		// remove product that are not supposed to be in cart
		$this->db->where('id_order',$id_order);
		$this->db->where_not_in('id_produk',$updated_id_product);
		$this->db->delete('commerce_cart');

		$this->recalculate_biaya_order($id_order);

		$this->db->trans_complete();
		return (isset($id_order));
	}

	public function recalculate_biaya_order($id_order)
	{
		$sub_total = $this->db->query("select sum(total_per_item) sub_total, voucher_applied_data from (SELECT id_order,id_produk,jumlah*harga as total_per_item FROM `default_commerce_cart` where id_order=".$id_order.") cart join default_commerce_order on (cart.id_order=default_commerce_order.id)")->row_array();

		$order_values = array('biaya_order'=>$sub_total['sub_total']);

		// if it already used voucher
		if(isset($sub_total['voucher_applied_data'])) {
			$voucher_applied_data =  json_decode($sub_total['voucher_applied_data'],true);
			if($voucher_applied_data['benefit_type']=='discount_nominal') {
				$order_values['discount_nominal'] = $voucher_applied_data['benefit_value'];
			} elseif($voucher_applied_data['benefit_type']=='discount_percentage') {
				$order_values['discount_nominal'] = $sub_total['sub_total'] * ($voucher_applied_data['benefit_value']/100);
			}
		}

		$this->db->where('id',$id_order);

		$this->db->update('commerce_order',$order_values);
		return $order_values;
	}

	public function get_cart_by_order($id)
	{
		$this->db->select('commerce_produk.type,commerce_produk.nama,deskripsi,additional_info,berat,id_kota_pengirim,is_digital_product,membership_period,membership_level,digital_id_file_asset');
		$this->db->select('commerce_cart.*,files.filename');
		$this->db->where('id_order', $id);
		$this->db->join('commerce_produk','commerce_cart.id_produk=commerce_produk.id');
		$this->db->join('files','files.id=commerce_produk.id_file');

		$query = $this->db->get('commerce_cart');
		$result = $query->result_array();

		return $result;
	}

	public function generate_unique()
	{
		$is_unique = false;
		$unique_id = '';
		while (!$is_unique) {
			// $unique_id = strtotime(date('Y-m-d H:i:s')).$this->uniqid_base36();
			$unique_id = $this->uniqid_base36(true);
			$this->db->where('id_unique',$unique_id);
			$unique_res = $this->db->count_all_results('commerce_cart');
			if($unique_res==0) {
				$is_unique = true;
			}
		}

		return $unique_id;
	}

	function uniqid_base36($more_entropy=false) {
		$s = uniqid('', $more_entropy);
		if (!$more_entropy) {
			return base_convert($s, 16, 36);
		}

		$hex = substr($s, 0, 13);
		$dec = $s[13] . substr($s, 15); // skip the dot
		return base_convert($hex, 16, 36) . base_convert($dec, 10, 36);
	}
}
