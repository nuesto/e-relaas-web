<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * rekening model
 *
 * @author Aditya Satrya
 */
class rekening_m extends MY_Model {
	
	public function get_rekening($pagination_config = NULL, $filters = NULL)
	{
		$this->db->select('*');
		
		if($pagination_config != "" && $pagination_config != NULL){
			$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
			$this->db->limit($pagination_config['per_page'], $start);
		}

		if(isset($filters['created_by'])) {
			$this->db->where('created_by',$filters['created_by']);
		}

		if(isset($filters['id_affiliate'])) {
			$this->db->where('id_affiliate',$filters['id_affiliate']);
		}

		if(isset($filters['rekening_utama']) AND $filters['rekening_utama']==1) {
			$this->db->where('id_affiliate',NULL);
		}
		
		$query = $this->db->get('default_commerce_rekening');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_rekening_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_commerce_rekening');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_rekening($filters = NULL)
	{
		if(isset($filters['created_by'])) {
			$this->db->where('created_by',$filters['created_by']);
		}
		if(isset($filters['id_affiliate'])) {
			$this->db->where('id_affiliate',$filters['id_affiliate']);
		}
		if(isset($filters['rekening_utama']) AND $filters['rekening_utama']==1) {
			$this->db->where('id_affiliate',NULL);
		}
		return $this->db->count_all_results('commerce_rekening');
	}
	
	public function delete_rekening_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_commerce_rekening');
	}
	
	public function insert_rekening($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_commerce_rekening', $values);
	}
	
	public function update_rekening($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_commerce_rekening', $values); 
	}
	
}