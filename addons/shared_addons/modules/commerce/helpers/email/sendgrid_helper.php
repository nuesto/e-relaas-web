<?php
// Send email using sendgrid api
function send_email_api($to, $content, $cc = NULL) {
    $ci =& get_instance();
    $ci->load->library('variables/variables');

    $sendgrid_api_key = $ci->variables->__get('sendgrid_api_key');
    if(isset($sendgrid_api_key) AND $sendgrid_api_key != '') {
        $data = array(
        	'personalizations'=>array('0'=>array(
        		'to'=>array('0'=>array(
        			'email'=>$to,
                  )),
             )),
        	'from' => array(
        		'email'=>$ci->variables->__get('email_sender_email'),
                'name'=>$ci->variables->__get('email_sender_name'),
        		),
        	'subject' => $content['subject'],
        	'content' => array('0'=>array(
        		'type' => 'text/html',
        		'value' => $content['body'],
        		))
        	);

        if(is_array($to)) {
            $reformat_to = array();
            foreach ($to as $key => $to_entry) {
                $reformat_to[$key]['email'] = $to_entry;
            }
            $to = $reformat_to;
            $data['personalizations'][0]['to'] = array('0'=>array('email'=>'member@financialwisdom.id'));
            $data['personalizations'][0]['bcc'] = $to;
        }

        $data_string = json_encode($data);
    	// set up the curl resource
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: Bearer '.$ci->variables->__get('sendgrid_api_key')));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

    	// execute the request

        $output = curl_exec($ch);

        if(curl_error($ch)!='') {
            log_message('error', curl_error($ch));
        }


        // close curl resource to free up system resources
        curl_close($ch);

        // output the profile information - includes the header
        echo $output;
    }
}

function sendgrid($to, $content, $cc = NULL) {
    $ci =& get_instance();
    $ci->load->library('variables/variables');
    $sendgrid_api_key = $ci->variables->__get('sendgrid_api_key');
    if(isset($sendgrid_api_key) AND $sendgrid_api_key != '') {
        $data = array(
            'personalizations'=>array('0'=>array(
                'to'=>array('0'=>array(
                    'email'=>$to,
                  )),
             )),
            'from' => array(
                'email'=>$ci->variables->__get('sendgrid_sender_email'),
                'name'=>$ci->variables->__get('sendgrid_sender_name')
                ),
            'subject' => $content['subject'],
            'content' => array('0'=>array(
                'type' => 'text/html',
                'value' => $content['body'],
                ))
            );
        if(is_array($to)) {
            $data['personalizations'][0]['to'] = array('0'=>array('email'=>'memberlist@rekadana.id'));
            $data['personalizations'][0]['bcc'] = $to;
        }
        $data_string = json_encode($data);

        // set up the curl resource
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.sendgrid.com/v3/mail/send");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: Bearer '.$ci->variables->__get('sendgrid_api_key')));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        // execute the request
        $output = curl_exec($ch);
        log_message('error', curl_error($ch));
        // close curl resource to free up system resources
        curl_close($ch);
        // output the profile information - includes the header
        echo $output;
    }
}
