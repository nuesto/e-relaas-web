<?php
// Send email using sendgrid api
function send_email_api($to, $content, $cc = NULL) {
    $ci =& get_instance();
    $ci->load->library('variables/variables');

    $AWSPUB = 'AKIAJ6MUTVSYKOZCPGAQ';
    $AWSPRI = 'gA62smjxymFcyj4+J/cB0cXaDlKQGs9VJSXh9L87';

    $DATE = gmdate('D, d M Y H:i:s e');
    $HASH = hash_hmac('sha1', $DATE, $AWSPRI, true);
    $KEYS = base64_encode($HASH);
    $headers = array();
    $headers[] = "Host: email.us-west-2.amazonaws.com";
    $headers[] = "Content-Type: application/x-www-form-urlencoded";
    $headers[] = "Date: ".$DATE;
    $auth = "AWS3-HTTPS AWSAccessKeyId=".$AWSPUB;
    $auth .= ",Algorithm=HmacSHA1,Signature=".$KEYS;
    $headers[] = "X-Amzn-Authorization: ".$auth;
    $url = "https://email.us-west-2.amazonaws.com/";

    $MAIL = array(
        'Action' => 'SendEmail',
        'Source' => $ci->variables->__get('email_sender_name').' <'.$ci->variables->__get('email_sender_email').'>',
        'Message.Subject.Data' => $content['subject'],
        'Message.Body.Html.Data' => $content['body']
    );
    if(is_string($to)) {
        $MAIL = array_merge($MAIL, array('Destination.ToAddresses.member.1' => $to));
    } else if(is_array($to)) {
        foreach ($to as $key => $value) {
            $bcc['Destination.BccAddresses.member.'.($key+1)] = $value;
        }
        $MAIL = array_merge($MAIL, $bcc);
    }

    $field_data = http_build_query($MAIL);

    $aws = curl_init();
    curl_setopt($aws, CURLOPT_POSTFIELDS, $field_data);
    curl_setopt($aws, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($aws, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($aws, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($aws, CURLOPT_HEADER, false);
    curl_setopt($aws, CURLOPT_URL, $url);
    curl_setopt($aws, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($aws);
    curl_close($aws);

    // output the profile information - includes the header
    // echo $output;
}
