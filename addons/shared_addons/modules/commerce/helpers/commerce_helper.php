<?php
// simple download excel

function download_csv($data, $filename = NULL) {
    // filename for download
    if($filename == NULL) {
        $filename = "data_csv_" . date('Ymd');
    }

    // header("Content-Disposition: attachment; filename=\"$filename\"");
    // header("Content-Type: application/vnd.ms-excel");

    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename='.$filename.'.csv');

    // create a file pointer connected to the output stream
    $fp = fopen('php://output', 'w');

    $flag = false;
    foreach($data as $row) {
        if(!$flag) {
            // display field/column names as first row
            // echo implode("\t", array_keys($row)) . "\r\n";
            fputcsv($fp, array_keys($row));
            $flag = true;
        }
        // array_walk($row, 'cleanData');
        // echo implode("\t", array_values($row)) . "\r\n";
        fputcsv($fp, $row);
    }
    fclose($fp);
    exit;
}
