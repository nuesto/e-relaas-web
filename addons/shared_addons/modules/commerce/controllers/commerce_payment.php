<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Commerce Module
 *
 * Modul untu me-manage simple commerce
 *
 */
class Commerce_payment extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'payment';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('commerce');
		
		$this->load->model('payment_m');
    }

    /**
	 * List all payment
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_payment') AND ! group_has_role('commerce', 'view_own_payment')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'commerce/payment/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->payment_m->count_all_payment();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['payment']['entries'] = $this->payment_m->get_payment($pagination_config);
		$data['payment']['total'] = count($data['payment']['entries']);
		$data['payment']['pagination'] = $this->pagination->create_links();

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('commerce:payment:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('commerce:payment:plural'))
			->build('payment_index', $data);
    }
	
	/**
     * Display one payment
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_payment') AND ! group_has_role('commerce', 'view_own_payment')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['payment'] = $this->payment_m->get_payment_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('commerce', 'view_all_payment')){
			if($data['payment']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:payment:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('commerce', 'view_all_payment') OR group_has_role('commerce', 'view_own_payment')){
			$this->template->set_breadcrumb(lang('commerce:payment:plural'), '/commerce/payment/index');
		}

		$this->template->set_breadcrumb(lang('commerce:payment:view'))
			->build('payment_entry', $data);
    }
	
	/**
     * Create a new payment entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'create_payment')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_payment('new')){	
				$this->session->set_flashdata('success', lang('commerce:payment:submit_success'));				
				redirect('commerce/payment/index');
			}else{
				$data['messages']['error'] = lang('commerce:payment:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'commerce/payment/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:payment:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('commerce', 'view_all_payment') OR group_has_role('commerce', 'view_own_payment')){
			$this->template->set_breadcrumb(lang('commerce:payment:plural'), '/commerce/payment/index');
		}

		$this->template->set_breadcrumb(lang('commerce:payment:new'))
			->build('payment_form', $data);
    }
	
	/**
     * Edit a payment entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the payment to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'edit_all_payment') AND ! group_has_role('commerce', 'edit_own_payment')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('commerce', 'edit_all_payment')){
			$entry = $this->payment_m->get_payment_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_payment('edit', $id)){	
				$this->session->set_flashdata('success', lang('commerce:payment:submit_success'));				
				redirect('commerce/payment/index');
			}else{
				$data['messages']['error'] = lang('commerce:payment:submit_failure');
			}
		}
		
		$data['fields'] = $this->payment_m->get_payment_by_id($id);
		$data['mode'] = 'edit';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'commerce/payment/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
       $this->template->title(lang('commerce:payment:edit'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('commerce', 'view_all_payment') OR group_has_role('commerce', 'view_own_payment')){
			$this->template->set_breadcrumb(lang('commerce:payment:plural'), '/commerce/payment/index')
			->set_breadcrumb(lang('commerce:payment:view'), '/commerce/payment/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('commerce:payment:plural'))
			->set_breadcrumb(lang('commerce:payment:view'));
		}

		$this->template->set_breadcrumb(lang('commerce:payment:edit'))
			->build('payment_form', $data);
    }
	
	/**
     * Delete a payment entry
     * 
     * @param   int [$id] The id of payment to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'delete_all_payment') AND ! group_has_role('commerce', 'delete_own_payment')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('commerce', 'delete_all_payment')){
			$entry = $this->payment_m->get_payment_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->payment_m->delete_payment_by_id($id);
		$this->session->set_flashdata('error', lang('commerce:payment:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(5);
        redirect('commerce/payment/index'.$data['uri']);
    }
	
	/**
     * Insert or update payment entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_payment($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('commerce:payment:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->payment_m->insert_payment($values);
			}
			else
			{
				$result = $this->payment_m->update_payment($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}