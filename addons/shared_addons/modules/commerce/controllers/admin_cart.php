<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Commerce Module
 *
 * Modul untu me-manage simple commerce
 *
 */
class Admin_cart extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'cart';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'access_cart_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('commerce');		
		$this->load->model('cart_m');
    }

    /**
	 * List all cart
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_cart') AND ! group_has_role('commerce', 'view_own_cart')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'admin/commerce/cart/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->cart_m->count_all_cart();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['cart']['entries'] = $this->cart_m->get_cart($pagination_config);
		$data['cart']['total'] = $pagination_config['total_rows'];
		$data['cart']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['cart']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('admin/commerce/cart/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/commerce/cart/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('commerce:cart:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('commerce:cart:plural'))
			->build('admin/cart_index', $data);
    }
	
	/**
     * Display one cart
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_cart') AND ! group_has_role('commerce', 'view_own_cart')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['cart'] = $this->cart_m->get_cart_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_cart')){
			if($data['cart']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('commerce:cart:view'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_cart') OR group_has_role('commerce', 'view_own_cart')){
			$this->template->set_breadcrumb(lang('commerce:cart:plural'), '/admin/commerce/cart/index');
		}

		$this->template->set_breadcrumb(lang('commerce:cart:view'))
			->build('admin/cart_entry', $data);
    }
	
	/**
     * Create a new cart entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'create_cart')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_cart('new')){	
				$this->session->set_flashdata('success', lang('commerce:cart:submit_success'));				
				redirect('admin/commerce/cart/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:cart:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/commerce/cart/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:cart:new'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_cart') OR group_has_role('commerce', 'view_own_cart')){
			$this->template->set_breadcrumb(lang('commerce:cart:plural'), '/admin/commerce/cart/index');
		}

		$this->template->set_breadcrumb(lang('commerce:cart:new'))
			->build('admin/cart_form', $data);
    }
	
	/**
     * Edit a cart entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the cart to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'edit_all_cart') AND ! group_has_role('commerce', 'edit_own_cart')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('commerce', 'edit_all_cart')){
			$entry = $this->cart_m->get_cart_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_cart('edit', $id)){	
				$this->session->set_flashdata('success', lang('commerce:cart:submit_success'));				
				redirect('admin/commerce/cart/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:cart:submit_failure');
			}
		}
		
		$data['fields'] = $this->cart_m->get_cart_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/commerce/cart/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:cart:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_cart') OR group_has_role('commerce', 'view_own_cart')){
			$this->template->set_breadcrumb(lang('commerce:cart:plural'), '/admin/commerce/cart/index')
			->set_breadcrumb(lang('commerce:cart:view'), '/admin/commerce/cart/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('commerce:cart:plural'))
			->set_breadcrumb(lang('commerce:cart:view'));
		}

		$this->template->set_breadcrumb(lang('commerce:cart:edit'))
			->build('admin/cart_form', $data);
    }
	
	/**
     * Delete a cart entry
     * 
     * @param   int [$id] The id of cart to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'delete_all_cart') AND ! group_has_role('commerce', 'delete_own_cart')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('commerce', 'delete_all_cart')){
			$entry = $this->cart_m->get_cart_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->cart_m->delete_cart_by_id($id);
        $this->session->set_flashdata('error', lang('commerce:cart:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('admin/commerce/cart/index'.$data['uri']);
    }
	
	/**
     * Insert or update cart entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_cart($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('commerce:cart:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->cart_m->insert_cart($values);
				
			}
			else
			{
				$result = $this->cart_m->update_cart($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}