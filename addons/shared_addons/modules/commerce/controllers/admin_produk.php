<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Commerce Module
 *
 * Modul untu me-manage simple commerce
 *
 */
class Admin_produk extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------

    protected $section = 'produk';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'access_produk_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('commerce');
		$this->load->model('produk_m');
		$this->load->model('pengiriman_m');

		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');
    }

    /**
	 * List all produk
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'view_all_produk') AND ! group_has_role('commerce', 'view_own_produk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'admin/commerce/produk/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->produk_m->count_all_produk();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);

        // -------------------------------------
		// Get entries
		// -------------------------------------

        $data['produk']['entries'] = $this->produk_m->get_produk($pagination_config);
		$data['produk']['total'] = $pagination_config['total_rows'];
		$data['produk']['pagination'] = $this->pagination->create_links();

		foreach ($data['produk']['entries'] as $key => $entry) {
			$data['produk']['entries'][$key]['kota_pengirim'] = $this->kota_m->get_kota_by_id($entry['id_kota_pengirim']);
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['produk']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('admin/commerce/produk/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/commerce/produk/index'.$data['uri']);
				}
			}
		}

        $this->template->title(lang('commerce:produk:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('commerce:produk:plural'))
			->build('admin/produk_index', $data);
    }

	/**
     * Display one produk
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'view_all_produk') AND ! group_has_role('commerce', 'view_own_produk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// -------------------------------------
		// Get our entry.
		// -------------------------------------

        $data['produk'] = $this->produk_m->get_produk_by_id($id);

		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------

		if(! group_has_role('commerce', 'view_all_produk')){
			if($data['produk']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);

        $this->template->title(lang('commerce:produk:view'))
			->set_breadcrumb('Home', '/admin');

		if(group_has_role('commerce', 'view_all_produk') OR group_has_role('commerce', 'view_own_produk')){
			$this->template->set_breadcrumb(lang('commerce:produk:plural'), '/admin/commerce/produk/index');
		}

		$this->template->set_breadcrumb(lang('commerce:produk:view'))
			->build('admin/produk_entry', $data);
    }

	/**
     * Create a new produk entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'create_produk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Process POST input
		// -------------------------------------

		if($_POST){
			if($this->_update_produk('new')){
				$this->session->set_flashdata('success', lang('commerce:produk:submit_success'));
				redirect('admin/commerce/produk/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:produk:submit_failure');
			}
		}

		$data['mode'] = 'new';
		$data['return'] = 'admin/commerce/produk/index'.$data['uri'];

		$data['provinsi'] = $this->provinsi_m->get_provinsi();
		$data['kota'] = array();
		if($this->input->post('id_provinsi')) {
			$data['kota'] = $this->kota_m->get_kota_by_provinsi($this->input->post('id_provinsi'));
		}elseif($this->input->get('f-provinsi')) {
			$data['kota'] = $this->kota_m->get_kota_by_provinsi($this->input->get('f-provinsi'));
		}

		$data['pengiriman'] = $this->pengiriman_m->get_pengiriman(NULL, array('is_active'=>1));

		// -------------------------------------
		// Build the form page.
		// -------------------------------------

        $this->template->title(lang('commerce:produk:new'))
			->set_breadcrumb('Home', '/admin');

		if(group_has_role('commerce', 'view_all_produk') OR group_has_role('commerce', 'view_own_produk')){
			$this->template->set_breadcrumb(lang('commerce:produk:plural'), '/admin/commerce/produk/index');
		}

		$this->template->set_breadcrumb(lang('commerce:produk:new'))
			->build('admin/produk_form', $data);
    }

	/**
     * Edit a produk entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the produk to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'edit_all_produk') AND ! group_has_role('commerce', 'edit_own_produk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// Check view all/own permission
		if(! group_has_role('commerce', 'edit_all_produk')){
			$entry = $this->produk_m->get_produk_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		$data['uri'] = get_query_string(6);

		// -------------------------------------
		// Process POST input
		// -------------------------------------

		if($_POST){
			if($this->_update_produk('edit', $id)){
				$this->session->set_flashdata('success', lang('commerce:produk:submit_success'));
				redirect('admin/commerce/produk/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:produk:submit_failure');
			}
		}

		$data['fields'] = $this->produk_m->get_produk_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/commerce/produk/index/'.$data['uri'];
		$data['entry_id'] = $id;

		$data['provinsi'] = $this->provinsi_m->get_provinsi();
		$data['provinsi_kota'] = $this->kota_m->get_provinsi_by_kota($data['fields']['id_kota_pengirim']);
		if(isset($data['provinsi_kota'][0]['id'])) {
			$data['kota'] = $this->kota_m->get_kota_by_provinsi($data['provinsi_kota'][0]['id']);
			$data['fields']['id_provinsi'] = $data['provinsi_kota'][0]['id'];
		} else {
			$data['kota'] = array();
			$data['fields']['id_provinsi'] = '';
		}


		$data['pengiriman'] = $this->pengiriman_m->get_pengiriman(NULL, array('is_active'=>1));

		// -------------------------------------
		// Build the form page.
		// -------------------------------------

        $this->template->title(lang('commerce:produk:edit'))
			->set_breadcrumb('Home', '/admin');

		if(group_has_role('commerce', 'view_all_produk') OR group_has_role('commerce', 'view_own_produk')){
			$this->template->set_breadcrumb(lang('commerce:produk:plural'), '/admin/commerce/produk/index');
		}else{
			$this->template->set_breadcrumb(lang('commerce:produk:plural'))
			->set_breadcrumb(lang('commerce:produk:view'));
		}

		$this->template->set_breadcrumb(lang('commerce:produk:edit'))
			->build('admin/produk_form', $data);
    }

	/**
     * Delete a produk entry
     *
     * @param   int [$id] The id of produk to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'delete_all_produk') AND ! group_has_role('commerce', 'delete_own_produk')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('commerce', 'delete_all_produk')){
			$entry = $this->produk_m->get_produk_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
		// Delete entry
		// -------------------------------------

        $this->produk_m->delete_produk_by_id($id);
        $this->session->set_flashdata('error', lang('commerce:produk:deleted'));

		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('admin/commerce/produk/index'.$data['uri']);
    }

	/**
     * Insert or update produk entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_produk($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->load->helper(array('form', 'url'));

 		// -------------------------------------
		// Set Values
		// -------------------------------------

		$values = $this->input->post();

        $product = null;
        if($row_id) {
            $product = $this->produk_m->get_produk_by_id($row_id);
        }

		// -------------------------------------
		// Validation
		// -------------------------------------

		// Set validation rules
		$this->form_validation->set_rules('type', lang('commerce:type'), 'required|max_length[100]');
		$this->form_validation->set_rules('nama', lang('commerce:nama'), 'required|max_length[100]');
		$this->form_validation->set_rules('harga', lang('commerce:harga'), 'required|numeric|greater_than[0]|less_than[9999999999]');
		$this->form_validation->set_rules('deskripsi', lang('commerce:deskripsi'), 'required|max_length[1000]');
		$this->form_validation->set_rules('id_file',lang('commerce:id_file'),'callback_check_image');

		if($this->input->post('type')=='physical') {
		$this->form_validation->set_rules('berat', lang('commerce:berat'), 'required|numeric|greater_than[0]|less_than[9999999]');

		$this->form_validation->set_rules('pengiriman[]', lang('commerce:nama_kurir'), 'required');
		}

		if($this->input->post('type')=='membership') {
		}

		if($this->input->post('type')=='donation') {
		}

        $is_unique = '|is_unique[commerce_produk.id_external_product]';
        if($product) {
            if($product['id'] == $row_id AND $product['id_external_product'] == $values['id_external_product']) {
                $is_unique = '';
            }
        }

		$this->form_validation->set_rules('id_external_product', lang('commerce:id_external_product'), 'numeric|greater_than[0]|less_than[999999999]'.$is_unique);
        
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');

		$result = false;

		if ($this->form_validation->run() === true)
		{
			$values['id_file'] = ($_POST['image_id'] == '') ? $_POST['id_file'] : $_POST['image_id'];
			$values['berat'] = ($values['berat']=='') ? 0 : $values['berat'];
			unset($values['id_provinsi']);
			$values['id_kota_pengirim'] = ($values['id_kota_pengirim']!='') ? $values['id_kota_pengirim'] : NULL;

            $values['id_external_product'] = ($values['id_external_product']!='') ? $values['id_external_product'] : null;
			if ($method == 'new')
			{
				$result = $this->produk_m->insert_produk($values);

			}
			else
			{
				$result = $this->produk_m->update_produk($values, $row_id);
			}
		}

		return $result;
	}

	// --------------------------------------------------------------------------

	public function check_image($image) {
		$this->load->library('files/files');
		$id_folder = Files::get_id_by_name('Public');
		$image_id = null;
		if($_FILES['image']['name']!=''){
			$allowed_type = array('bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'png', 'tiff', 'tif', 'BMP', 'GIF', 'JPEG', 'JPG', 'PNG');
			$data = Files::upload($id_folder, $_FILES['image']['name'], 'image',false,false,false,implode("|", $allowed_type));

			if($data['status']===false) {
				$this->form_validation->set_message('check_image','<strong>'.lang('commerce:id_file').'</strong> '.strip_tags($data['message']));
				return false;
			} else {
				$image_id = $data['data']['id'];
				$_POST['image_id'] = $image_id;
				return true;
			}
		} else {
			if($this->input->post('id_file')=='') {
				$this->form_validation->set_message('check_image','<strong>'.lang('commerce:id_file').'</strong> '.lang('commerce:required'));
				return false;
			}
		}
	}

}
