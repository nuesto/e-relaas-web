<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Commerce Module
 *
 * Modul untu me-manage simple commerce
 *
 */
class Admin_voucher extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'voucher';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('commerce');		
		$this->load->model('voucher_m');
		$this->load->model('affiliate_m');
		$this->config->load('config');
    }

    /**
	 * List all voucher
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_voucher') AND ! group_has_role('commerce', 'view_own_voucher') AND $this->current_user->is_affiliate == 0){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// -------------------------------------
		// Filters
		// -------------------------------------

		$filters = NULL;
		if(! group_has_role('commerce', 'view_all_voucher') AND (group_has_role('commerce', 'view_own_voucher') OR $this->current_user->is_affiliate == 1)){
			$filters['created_by'] = $this->current_user->id;
		}

		if($this->input->get('f-code')) {
			$filters['code'] = $this->input->get('f-code');
		}

		if(is_numeric($this->input->get('f-state_active'))) {
			$filters['state_active'] = $this->input->get('f-state_active');
		}

		if($this->input->get('f-benefit_type')) {
			$filters['benefit_type'] = $this->input->get('f-benefit_type');
		}

		if($this->input->get('f-affiliate_id')) {
			$filters['affiliate_id'] = $this->input->get('f-affiliate_id');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'admin/commerce/voucher/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->voucher_m->count_all_voucher($filters);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['voucher']['entries'] = $this->voucher_m->get_voucher($pagination_config,$filters);
		$data['voucher']['total'] = $pagination_config['total_rows'];
		$data['voucher']['pagination'] = $this->pagination->create_links();

		$data['affiliate_lists'] = $this->affiliate_m->get_affiliate();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['voucher']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('admin/commerce/voucher/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/commerce/voucher/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('commerce:voucher:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('commerce:voucher:plural'))
			->build('admin/voucher_index', $data);
    }

    /**
	 * Method for handling different form actions
	 */
	public function action()
	{
		// Determine the type of action
		switch ($this->input->post('btnAction'))
		{
			case 'delete':
				$this->delete();
				break;
			default:
				redirect('admin/commerce/voucher/index');
				break;
		}
	}

    /**
     * Create a new voucher entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'create_voucher') AND $this->current_user->is_affiliate == 0){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_voucher('new')){	
				$this->session->set_flashdata('success', lang('commerce:voucher:submit_success'));				
				redirect('admin/commerce/voucher/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:voucher:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/commerce/voucher/index'.$data['uri'];

		$data['affiliate_lists'] = $this->affiliate_m->get_affiliate();
		
		$data['vdkk'] = $this->voucher_m->get_voucher_by_code($this->config->item('active_voucher_code'));

		$data['template_voucher'] = $this->voucher_m->get_template_voucher();
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:voucher:new'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_voucher') OR group_has_role('commerce', 'view_own_voucher')){
			$this->template->set_breadcrumb(lang('commerce:voucher:plural'), '/admin/commerce/voucher/index');
		}

		$this->template->set_breadcrumb(lang('commerce:voucher:new'))
			->build('admin/voucher_form', $data);
    }

    /**
     * Edit a voucher entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the voucher to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'edit_all_voucher') AND ! group_has_role('commerce', 'edit_own_voucher') AND $this->current_user->is_affiliate == 0){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('commerce', 'edit_all_voucher')){
			$entry = $this->voucher_m->get_voucher_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id OR $entry['affiliate_id'] != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_voucher('edit', $id)){	
				$this->session->set_flashdata('success', lang('commerce:voucher:submit_success'));				
				redirect('admin/commerce/voucher/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:voucher:submit_failure');
			}
		}
		
		$data['fields'] = $this->voucher_m->get_voucher_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/commerce/voucher/index/'.$data['uri'];
		$data['entry_id'] = $id;

		$data['affiliate_lists'] = $this->affiliate_m->get_affiliate();

		$data['vdkk'] = $this->voucher_m->get_voucher_by_code($this->config->item('active_voucher_code'));

		$data['template_voucher'] = $this->voucher_m->get_template_voucher();
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:voucher:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_voucher') OR group_has_role('commerce', 'view_own_voucher')){
			$this->template->set_breadcrumb(lang('commerce:voucher:plural'), '/admin/commerce/voucher/index');
		}else{
			$this->template->set_breadcrumb(lang('commerce:voucher:plural'))
			->set_breadcrumb(lang('commerce:voucher:view'));
		}

		$this->template->set_breadcrumb(lang('commerce:voucher:edit'))
			->build('admin/voucher_form', $data);
    }

    public function get_voucher($id_voucher='')
    {
    	$voucher = $this->voucher_m->get_voucher_by_id($id_voucher);
    	if(!$voucher) {
    		$voucher = array('error'=>'Voucher tidak ditemukan');
    	}
    	exit(json_encode($voucher));
    }

    /**
     * Delete a voucher entry
     * 
     * @param   int [$id] The id of voucher to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
    	$data['uri'] = get_query_string(6);
    	
    	// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'delete_all_voucher') AND ! group_has_role('commerce', 'delete_own_voucher') AND $this->current_user->is_affiliate == 0){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $ids = ($id > 0) ? array($id) : $this->input->post('action_to');

		if ( ! empty($ids))
		{
			$deleted = 0;
			$to_delete = 0;
			$used_voucher = array();
			$deleted_ids = array();
			foreach ($ids as $id)
			{
				$to_delete++;
				// Make sure the voucher haven't been used
				$entry = $this->voucher_m->get_voucher_by_id($id);
				// Check view all/own permission
				if(! group_has_role('commerce', 'delete_all_voucher')){
					$created_by_user_id = $entry['created_by'];
					if($created_by_user_id != $this->current_user->id OR $entry['affiliate_id'] != $this->current_user->id){
						$this->session->set_flashdata('error', lang('cp:access_denied'));
						redirect('admin');
					}
				}

				if($entry['usage_count']>0) 
				{
					$used_voucher[] = $entry['code'];
					continue;
				}

				if ($this->voucher_m->delete_voucher_by_id($id))
				{
					$deleted_ids[] = $id;
					$deleted++;
				}
			}

			if(count($used_voucher) > 0) {
				$this->session->set_flashdata('notice','Voucher '.implode(",", $used_voucher).'. '.lang('commerce:voucher:already_used'));
			}

			if ($to_delete > 0)
			{
				$this->session->set_flashdata('success', sprintf(lang('commerce:voucher:mass_delete_success'), $deleted, $to_delete));
			}
		}
		// The array of id's to delete is empty
		else
		{
			$this->session->set_flashdata('error', lang('commerce:voucher:mass_delete_error'));
		}
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		redirect('admin/commerce/voucher/index'.$data['uri']);
    }

    /**
     * Insert or update voucher entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_voucher($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));

		// set value for affiliate
		if($this->current_user->is_affiliate == 1) {
			$vdkk = $this->voucher_m->get_voucher_by_id($this->input->post('template_voucher'));
			$_POST['valid_unit'] = $vdkk['valid_unit'];
			$_POST['benefit_type'] = $vdkk['benefit_type'];
			$_POST['benefit_value'] = $vdkk['benefit_value'];
			$_POST['state_active'] = $vdkk['state_active'];
			$_POST['product_type'] = $vdkk['product_type'];

			if($vdkk['valid_unit'] != NULL) {
				unset($_POST['no_valid_unit']);
			}
		}
		
 		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$unique_code = '';
		if($method=='new') {
			$unique_code = '|callback_unique_code';
		} else {
			$unique_code = '|callback_unique_code['.$row_id.']';
		}
		
		if($this->input->post('is_multi')) {
			$unique_code = '|callback_check_multi[40]';
		}else{
			$unique_code = '|max_length[40]';
		}

		$this->form_validation->set_rules('code', 'Kode', 'required|strtoupper'.$unique_code);
		if(!$this->input->post('no_valid_unit')) {
			$this->form_validation->set_rules('valid_unit', 'Berlaku Hingga', 'required|check_date_format');
		}
		if(!$this->input->post('no_limit_usage')) {
			$this->form_validation->set_rules('usage_limit_count', 'Batas Penggunaan', 'required|numeric|integer|greater_than[0]|less_than[4294967295]');
		} else {
			$this->form_validation->set_rules('usage_limit_count', 'Batas Penggunaan', 'numeric|less_than[4294967295]');
		}
		$this->form_validation->set_rules('benefit_type', 'Tipe Benefit', 'required');
		if($this->input->post('benefit_type')=='discount_nominal') {
			$this->form_validation->set_rules('benefit_value', 'Nilai Benefit', 'required|numeric|greater_than[0]|less_than[4294967295]');
		} else {
			$this->form_validation->set_rules('benefit_value', 'Nilai Benefit', 'required|numeric|greater_than[0]|less_than_equal_to[100]');
		}
		$this->form_validation->set_rules('state_active', 'Status', 'required');
		$this->form_validation->set_rules('affiliate_id', 'Affiliate', '');

		$this->form_validation->set_rules('product_type', 'Berlaku pada Tipe Produk', 'required');

		$this->form_validation->set_message('is_unique','Bagian %s sudah pernah digunakan');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			// -------------------------------------
			// Set Values
			// -------------------------------------

			$values = $this->input->post();

			// unset is multi
			unset($values['is_multi']);

			// unset template voucher
			unset($values['template_voucher']);

			// reformat valid unit if exist
			if(isset($values['valid_unit'])) {
				$values['valid_unit'] = date('Y-m-d H:i',strtotime($values['valid_unit']));
			}

			// set valid_unit to null
			if($this->input->post('no_valid_unit')) {
				$values['valid_unit'] = NULL;
				unset($values['no_valid_unit']);
			}

			// set usage count and usage scope to null
			if($this->input->post('no_limit_usage')) {
				$values['usage_limit_count'] = NULL;
				$values['usage_limit_scope'] = NULL;
				unset($values['no_limit_usage']);
			}

			// set null if affiliate id is empty
			if($this->input->post('affiliate_id')=='') {
				$values['affiliate_id'] = NULL;
			}
			
			if ($method == 'new')
			{
				if($this->input->post('is_multi') == 1) {
					$codes = preg_split('/[\n\r]+/', $this->input->post('code'));
					foreach ($codes as $key => $code_entry) {
						$values['code'] = $code_entry;
						$result = $this->voucher_m->insert_voucher($values);
					}
				} else {
					$result = $this->voucher_m->insert_voucher($values);
				}
			}
			else
			{
				$result = $this->voucher_m->update_voucher($values, $row_id);
			}
		}
		
		return $result;
	}

	public function check_multi($code, $length)
	{
		$codes = preg_split('/[\n\r]+/', $code);
		$any_false = 0;
		foreach ($codes as $key => $code_entry) {
			if(strlen($code_entry)>=$length) {
				$this->form_validation->set_message('check_multi','Terdapat <strong>Kode</strong> yang mempunyai panjang melebihi '.$length.' karakter');
				$any_false++;
			}
		}

		return ($any_false == 0) ? true : false;
	}

	public function unique_code($code, $id_voucher = NULL)
	{
		$voucher = $this->voucher_m->get_voucher_by_code($code);
		if(isset($voucher)) {
			if(!isset($id_voucher)) {
				$this->form_validation->set_message('unique_code','<strong>%s</strong> sudah pernah digunakan');
				return false;
			}
			if(isset($id_voucher) AND $id_voucher != $voucher['id']) {
				$this->form_validation->set_message('unique_code','<strong>%s</strong> sudah pernah digunakan');
				return false;
			}
		}
		return true;
	}

	public function check_date_format($date) {
		$test_arr  = explode('-', $date);
		if (count($test_arr) == 3) {
			// checkdate(m,d,Y)
			if (checkdate($test_arr[1], $test_arr[0], $test_arr[2])) {
        		return true;
			} else {
        		$this->form_validation->set_message('check_date_format','Bagian <strong>%s</strong> format tanggal tidak sesuai. Format yyyy-mm-dd');
				return false;
			}
		} else {
    		$this->form_validation->set_message('check_date_format','Bagian <strong>%s</strong> format tanggal tidak sesuai. Format yyyy-mm-dd');
			return false;
		}
	}

	// --------------------------------------------------------------------------

}