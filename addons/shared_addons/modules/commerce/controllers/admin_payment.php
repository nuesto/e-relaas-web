<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Commerce Module
 *
 * Modul untu me-manage simple commerce
 *
 */
class Admin_payment extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'payment';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'access_payment_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('commerce');		
		$this->load->model('order_m');
		$this->load->model('payment_m');

		$this->load->model('location/kota_m');

		$this->load->helper('commerce');
		$this->load->helper('email/sendgrid');
    }

    /**
	 * List all payment
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_payment') AND ! group_has_role('commerce', 'view_own_payment')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'admin/commerce/payment/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->payment_m->count_all_payment();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['payment']['entries'] = $this->payment_m->get_payment($pagination_config);
		$data['payment']['total'] = $pagination_config['total_rows'];
		$data['payment']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['payment']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('admin/commerce/payment/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/commerce/payment/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('commerce:payment:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('commerce:payment:plural'))
			->build('admin/payment_index', $data);
    }
	
	/**
     * Display one payment
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_payment') AND ! group_has_role('commerce', 'view_own_payment') AND ! group_has_role('commerce', 'view_affiliate_payment') AND $this->current_user->is_affiliate == 0){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['payment'] = $this->payment_m->get_payment_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_payment')){
			if($data['payment']['created_by'] == $this->current_user->id){
				exit(lang('cp:access_denied'));
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('commerce:payment:view'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_payment') OR group_has_role('commerce', 'view_own_payment')){
			$this->template->set_breadcrumb(lang('commerce:payment:plural'), '/admin/commerce/payment/index');
		}
		
		if ($this->input->is_ajax_request()){
			$this->template->set_layout(false);
		}

		$this->template->set_breadcrumb(lang('commerce:payment:view'))
			->build('admin/payment_entry', $data);
    }
	
	/**
     * Create a new payment entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'create_payment')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_payment('new')){	
				$this->session->set_flashdata('success', lang('commerce:payment:submit_success'));				
				redirect('admin/commerce/payment/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:payment:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/commerce/payment/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:payment:new'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_payment') OR group_has_role('commerce', 'view_own_payment')){
			$this->template->set_breadcrumb(lang('commerce:payment:plural'), '/admin/commerce/payment/index');
		}

		$this->template->set_breadcrumb(lang('commerce:payment:new'))
			->build('admin/payment_form', $data);
    }
	
	/**
     * Edit a payment entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the payment to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'edit_all_payment') AND ! group_has_role('commerce', 'edit_own_payment')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('commerce', 'edit_all_payment')){
			$entry = $this->payment_m->get_payment_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_payment('edit', $id)){	
				$this->session->set_flashdata('success', lang('commerce:payment:submit_success'));				
				redirect('admin/commerce/payment/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:payment:submit_failure');
			}
		}
		
		$data['fields'] = $this->payment_m->get_payment_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/commerce/payment/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:payment:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_payment') OR group_has_role('commerce', 'view_own_payment')){
			$this->template->set_breadcrumb(lang('commerce:payment:plural'), '/admin/commerce/payment/index')
			->set_breadcrumb(lang('commerce:payment:view'), '/admin/commerce/payment/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('commerce:payment:plural'))
			->set_breadcrumb(lang('commerce:payment:view'));
		}

		$this->template->set_breadcrumb(lang('commerce:payment:edit'))
			->build('admin/payment_form', $data);
    }
	
	/**
     * Delete a payment entry
     * 
     * @param   int [$id] The id of payment to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'delete_all_payment') AND ! group_has_role('commerce', 'delete_own_payment')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('commerce', 'delete_all_payment')){
			$entry = $this->payment_m->get_payment_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->payment_m->delete_payment_by_id($id);
        $this->session->set_flashdata('error', lang('commerce:payment:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('admin/commerce/payment/index'.$data['uri']);
    }
	
	/**
     * Insert or update payment entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_payment($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		if($method == 'confirm') {
			$this->form_validation->set_rules('field_name', lang('commerce:payment:field_name'), 'required');
		} else {
			$this->form_validation->set_rules('field_name', lang('commerce:payment:field_name'), 'required');

		}
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->payment_m->insert_payment($values);
				
			}
			else
			{
				$result = $this->payment_m->update_payment($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	/**
     * confirm a payment entry
     *
     * We're passing the
     * id of the entry, which will be confirmed
     *
     * @param   int [$id] The id of the payment to the be confirm.
     * @return	void
     */
    public function confirm($id = 0)
    {
		$data['uri'] = get_query_string(6);
        
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'edit_order_status')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin/commerce/order/index'.$data['uri']);
		}		
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------

		$order_entry = $this->order_m->get_order_by_id($id);

		if(!$order_entry) {
			$this->session->set_flashdata('error','Pesanan tidak ditemukan');
        	redirect('admin/commerce/order/index');
		}

		$payment = $this->payment_m->get_payment_by_order($order_entry['id']);
		$exist_history = (isset($payment['history'])) ? json_decode($payment['history'],true) : array();
		$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>'paid','updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'));
		$state_history = array_merge($exist_history,$new_history);

		$keterangan = (isset($payment['keterangan']) AND $payment['keterangan']!='') ? $payment['keterangan'].'<br>Catatan verifikator : '.$this->input->post('keterangan') : 'Catatan verifikator : '.$this->input->post('keterangan');
		$values['status'] = 'paid';
		$values['keterangan'] = $keterangan;
		$values['closed_by'] = $this->current_user->id;
		$values['closed_on'] = date('Y-m-d H:i:s');
		$values['transfer_confirmed_by'] = $this->current_user->id;
		$values['transfer_confirmed_on'] = date('Y-m-d H:i:s');
		$values['history'] = json_encode($state_history);
		$update_payment = $this->payment_m->update_payment($values, $id);

		$values = NULL;
		$order = $this->order_m->get_order_by_id($id);
		$exist_history = (isset($order['state_history'])) ? json_decode($order['state_history'],true) : array();
		$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>'completed','updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'));
		$state_history = array_merge($exist_history,$new_history);

		$values['state_history'] = json_encode($state_history);
		$values['status_pesanan'] = 'completed';
		$update_order = $this->order_m->update_order($values, array('id' => $id));

		if($update_payment !== false AND $update_order !== false) {
			// update group user to member
			if($order_entry['order_type']=='membership') {
				$this->load->library('users/ion_auth');
	            $default_group = $this->ion_auth->get_group_by_name('member');
	            $customer_group = $this->ion_auth->get_group_by_name('customer');
	            $user = $this->ion_auth->get_user_array($order_entry['id_customer']);
	            
	            if($user['group_id'] == $customer_group->id) {
		        	$result = $this->ion_auth->update_user_group($order_entry['id_customer'],$default_group->id);
		        	$this->load->model('users/user_m');
	        		$this->user_m->set_membership_time($order_entry['id_customer']);
		        }
		    }

            $email_data['order'] = $order_entry;
            $this->load->helper('commerce/commerce');
            $pembayaran_dikonfirmasi = array(
				'subject' => '[DuaKodiKartika.com] Pesanan #'.$email_data['order']['no_invoice'].' Telah Dikonfirmasi',
				'body' => $this->load->view('email_template/pembayaran_dikonfirmasi',$email_data,true)
				);

			$customer = $this->ion_auth->get_user($order_entry['id_customer']);
			send_email_api($customer->email,$pembayaran_dikonfirmasi);

			// send notif to affiliate 

			$email_data['order_product'] = $this->order_m->get_order_product($email_data['order']['id']);
			$email_data['order']['kota'] = $this->kota_m->get_kota_by_id($email_data['order']['id_kota_pengiriman']);

			// check if all product is digital product
			// then remove field 'Pengiriman' and 'Alamat'
			$email_data['all_digital'] = 1;
			foreach ($email_data['order_product'] as $k=>$op) {
				if($op['is_digital_product']==0) {
					$email_data['all_digital'] = $op['is_digital_product'];
					continue;
				}
			}

			if($order_entry['id_affiliate'] != NULL AND $order_entry['id_affiliate'] != '0') {
				$email_data['to_affiliate'] = 1;

				$tagihan_pembayaran = array(
					'subject' => '[DuaKodiKartika.com] Terdapat pesanan baru melalui link affiliate anda',
					'body' => $this->load->view('email_template/affiliate_pesanan_baru',$email_data,true)
					);

				$affiliate_user = $this->ion_auth->get_user($order_entry['id_affiliate']);
				send_email_api($affiliate_user->email,$tagihan_pembayaran);	
			}

			$this->session->set_flashdata('success', 'Pembayaran berhasil diverifikasi');
		} else {
			$this->session->set_flashdata('error', lang('commerce:payment:submit_failure'));
		}
		if($this->input->get('order_type')) {
			$type = $this->input->get('order_type');
		}
		redirect('admin/commerce/order/index/'.$type.$data['uri']);

    }

    /**
     * Cancel a order entry
     * 
     * @param   none
     * @return  void
     */
    public function cancel($row_id)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'edit_order_status')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin/login');
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		$payment = $this->payment_m->get_payment_by_order($row_id);

		if(!$payment) {
			$this->session->set_flashdata('error','Pesanan tidak ditemukan');
        	redirect('admin/commerce/order/index');
		}

		$order = $this->order_m->get_order_by_id($row_id);
		$exist_history = (isset($order['state_history'])) ? json_decode($order['state_history'],true) : array();
		$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>'cancelled','updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'));
		$state_history = array_merge($exist_history,$new_history);

		$values['status_pesanan'] = 'cancelled';
		$values['state_history'] = json_encode($state_history);
		$update_order = $this->order_m->update_order($values, array('id' => $row_id));

		$values = NULL;
		$exist_history = (isset($payment['history'])) ? json_decode($payment['history'],true) : array();
		$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>'declined','updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'));
		$state_history = array_merge($exist_history,$new_history);

		$values['status'] = 'declined';
		$result = $this->payment_m->update_payment($values, $row_id);

		$this->session->set_flashdata('error', lang('commerce:order:declined'));

		$this->load->model('location/kota_m');
		$this->load->model('users/user_m');

		$data['order'] = $this->order_m->get_order_by_many(array('default_commerce_order.id'=>$row_id));
		$data['order_product'] = $this->order_m->get_order_product($data['order']['id']);
		$data['order']['kota'] = $this->kota_m->get_kota_by_id($data['order']['id_kota_pengiriman']);
		$data['user'] = $this->user_m->get(array('id'=>$data['order']['id_customer']));

		$data['order_product'] = $this->order_m->get_order_product($data['order']['id']);
		$data['order']['kota'] = $this->kota_m->get_kota_by_id($data['order']['id_kota_pengiriman']);

		// check if all product is digital product
		// then remove field 'Pengiriman' and 'Alamat'
		$data['all_digital'] = 1;
		foreach ($data['order_product'] as $k=>$op) {
			if($op['is_digital_product']==0) {
				$data['all_digital'] = $op['is_digital_product'];
				continue;
			}
		}

		$pesanan_dikirim = array(
			'subject' => '[INVOICE #'.$data['order']['no_invoice'].'] Maaf, pembayaran anda kami tolak',
			'body' => $this->load->view('email_template/pembayaran_ditolak',$data,true)
			);

		send_email_api($data['user']->email,$pesanan_dikirim);
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        if($this->input->get('order_type')) {
			$type = $this->input->get('order_type');
		}
		redirect('admin/commerce/order/index/'.$type.$data['uri']);
    }

    public function affiliate_order($id)
    {
    	$order_entry = $this->order_m->get_order_by_id($id);
    	$email_data['order'] = $order_entry;
    	$email_data['order_product'] = $this->order_m->get_order_product($email_data['order']['id']);
		$email_data['order']['kota'] = $this->kota_m->get_kota_by_id($email_data['order']['id_kota_pengiriman']);

		// check if all product is digital product
		// then remove field 'Pengiriman' and 'Alamat'
		$email_data['all_digital'] = 1;
		foreach ($email_data['order_product'] as $k=>$op) {
			if($op['is_digital_product']==0) {
				$email_data['all_digital'] = $op['is_digital_product'];
				continue;
			}
		}

        $this->load->helper('commerce/commerce');

		// send notif to affiliate 
		if($order_entry['id_affiliate'] != NULL AND $order_entry['id_affiliate'] != '0') {
			$email_data['to_affiliate'] = 1;

			$this->load->view('email_template/affiliate_pesanan_baru',$email_data);
			// $tagihan_pembayaran = array(
			// 	'subject' => '[DuaKodiKartika.com] Terdapat pesanan baru melalui link affiliate anda #'.$email_data['order']['no_invoice'],
			// 	'body' => $this->load->view('email_template/affiliate_pesanan_baru',$email_data,true)
			// 	);

			// $affiliate_user = $this->ion_auth->get_user($order_entry['id_affiliate']);
			// send_email_api($affiliate_user->email,$tagihan_pembayaran);	
		}
    }
}