<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Commerce Module
 *
 * Modul untu me-manage simple commerce
 *
 */
class Admin_order extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'order';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'access_order_backend') AND $this->current_user->is_affiliate == 0){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('commerce');		
		$this->load->model('order_m');

		$this->load->helper('commerce');
		$this->load->helper('email/sendgrid');

		$this->load->library('variables/variables');
    }

    /**
	 * List all order
     *
     * @return	void
     */
    public function index($type = '')
    {
    	$type = ($this->input->get('type')) ? $this->input->get('type') : '';
    	if($type == '' or !in_array($type, array('own','affiliate'))) {
    		$type = 'own';
    	}
    	
    	if(!group_has_role('commerce','view_all_order') AND ($type == '' or !in_array($type, array('own','affiliate')))) {
    		redirect('admin/commerce/order/index/own');
    	}

    	if($this->current_user->is_affiliate=='1') {
    		$type = 'affiliate';
    	}
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_order') AND ! group_has_role('commerce', 'view_own_order') AND $this->current_user->is_affiliate == 0){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// if they insist to see affiliate order but they don't have permission, redirect them to their order
		if($type=='affiliate' and !group_has_role('commerce','view_affiliate_order') AND $this->current_user->is_affiliate == 0) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin/commerce/order/index/own');
		}

		$params = array();
		$permission_type = ($type != '') ? $type : 'own';
		if(! group_has_role('commerce', 'view_all_order')){
			if($permission_type == 'own') {
				if(group_has_role('commerce', 'view_own_order')) {
					$params['default_commerce_order.created_by'] = $this->current_user->id;
				}
			} elseif ($permission_type == 'affiliate') {
				if(group_has_role('commerce', 'view_own_order') OR $this->current_user->is_affiliate == 1) {
					$params['default_commerce_order.id_affiliate'] = $this->current_user->id;
				}
			}
		}

		// filter
		if($this->input->get('f-tanggal_order')) {
			$params['tanggal_order'] = date('Y-m-d',strtotime($this->input->get('f-tanggal_order')));
		}
		if($this->input->get('f-no_invoice')) {
			$params['no_invoice'] = $this->input->get('f-no_invoice');
		}
		if($this->input->get('f-nama')) {
			$params['display_name'] = $this->input->get('f-nama');
		}
		if($this->input->get('f-total_tagihan')) {
			$params['((biaya_order-ifnull(discount_nominal,0))+ifnull(biaya_pengiriman,0)+ifnull(kode_unik,0))'] = $this->input->get('f-total_tagihan');
		}
		if($this->input->get('f-voucher')) {
			$params['code'] = $this->input->get('f-voucher');
		}
		if($this->input->get('f-payment_method')) {
			$params['payment_method'] = $this->input->get('f-payment_method');
		}
		if($this->input->get('f-affiliate')) {
			$params['id_affiliate'] = $this->input->get('f-affiliate');
		}
		if($this->input->get('f-voucher_code')) {
			$params['code'] = $this->input->get('f-voucher_code');
		}
		if($this->input->get('product_type')) {
			$params['order_type'] = $this->input->get('product_type');
		}

		$params['status'] = 'pending';
		if(group_has_role('commerce','confirm_payment')) {
			$data['payment_status'] = 'confirm_payment';
		} else {
			$data['payment_status'] = 'completed';
		}
		if(!$this->input->get('payment_status')) {
			$_GET['payment_status'] = 'confirm_payment';
		}
		if($this->input->get('payment_status')) {
			if($this->input->get('payment_status')=='confirm_payment') {
				$data['payment_status'] = 'confirm_payment';
				$params['status_pesanan'] = 'waiting-payment';
				$params['payment_method'] = 'transfer';
				$params['status'] = 'pending';
			}elseif($this->input->get('payment_status')=='process') {
				$data['payment_status'] = 'process';
				$params['payment_status'] = 'process';
				unset($params['status']);
			}elseif(in_array($this->input->get('payment_status'),array('completed','expired','cancelled','declined'))) {
				$data['payment_status'] = $this->input->get('payment_status');
				unset($params['status']);
				$params['payment_status'] = $this->input->get('payment_status');
			}elseif($this->input->get('payment_status')=='all') {
				$data['payment_status'] = 'all';
				$params['payment_status'] = 'all';
				unset($params['status']);
			}
		}

		if($permission_type == 'affiliate') {
			$params['payment_method'] = $this->input->get('payment_method');
			// unset($params['status']);
			// unset($params['status_pesanan']);
		}

		if($data['payment_status']=='all') {
			unset($params['status_pesanan']);
			unset($params['status']);
		}

		// -------------------------------------
		// Pagination
		// -------------------------------------
		$uri_segment = ($type!='own' AND $type!='affiliate') ? 5 : 6;
		
		$pagination_config['base_url'] = base_url(). 'admin/commerce/order/index/'.$type;
		$pagination_config['uri_segment'] = $uri_segment;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->order_m->count_all_order($params,1);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string($uri_segment);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['order']['entries'] = $this->order_m->get_order($pagination_config,$params,1);
		$data['order']['total'] = $pagination_config['total_rows'];
		$data['order']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
		if(count($data['order']['entries'])==0 AND ($this->uri->segment($uri_segment) != NULL AND $this->uri->segment($uri_segment)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment($uri_segment), '', $data['uri']);
			if(strpos($data['uri'], '/cancel') !== false) {
				$data['uri'] == '';
			}
			if($this->uri->segment($uri_segment) != '' AND $this->uri->segment($uri_segment)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment($uri_segment)-$pagination_config['per_page'];
				redirect('admin/commerce/order/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/commerce/order/index'.$data['uri']);
				}
			}
		}

		$data['default_id_product'] = $this->variables->__get('default_id_product');

		$this->load->model('affiliate_m');
		$data['affiliate_lists'] = $this->affiliate_m->get_affiliate();

		if(!$this->input->get('order_type')) {
			$conjuction = ($_SERVER['QUERY_STRING']) ? '&' : '?';
			
			$data['uri'] .= $conjuction.'order_type='.$type;
		}
		
        $this->template->title(lang('commerce:order:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('commerce:order:plural'))
			->set('type',$type)
			->build('admin/order_index', $data);
    }

    /**
	 * download all order
     *
     * @return	void
     */
    public function download($type = '')
    {
    	if(!group_has_role('commerce','view_all_order') AND ($type == '' or !in_array($type, array('own','affiliate')))) {
    		redirect('admin/commerce/order/index/own');
    	}
    	if($type == '' or !in_array($type, array('own','affiliate'))) {
    		$type = 'own';
    	}
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_order') AND ! group_has_role('commerce', 'view_own_order')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// if they insist to see affiliate order but they don't have permission, redirect them to their order
		if($type=='affiliate' and !group_has_role('commerce','view_affiliate_order') AND $this->current_user->is_affiliate == 0) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin/commerce/order/index/own');
		}

		$params = array();
		$permission_type = ($type != '') ? $type : 'own';
		if(! group_has_role('commerce', 'view_all_order')){
			if($permission_type == 'own') {
				if(group_has_role('commerce', 'view_own_order')) {
					$params['default_commerce_order.created_by'] = $this->current_user->id;
				}
			} elseif ($permission_type == 'affiliate') {
				if(group_has_role('commerce', 'view_own_order') OR $this->current_user->is_affiliate == 1) {
					$params['default_commerce_order.id_affiliate'] = $this->current_user->id;
				}
			}
		}

		// filter
		if($this->input->get('f-tanggal_order')) {
			$params['tanggal_order'] = date('Y-m-d',strtotime($this->input->get('f-tanggal_order')));
		}
		if($this->input->get('f-no_invoice')) {
			$params['no_invoice'] = $this->input->get('f-no_invoice');
		}
		if($this->input->get('f-nama')) {
			$params['display_name'] = $this->input->get('f-nama');
		}
		if($this->input->get('f-total_tagihan')) {
			$params['((biaya_order-ifnull(discount_nominal,0))+ifnull(biaya_pengiriman,0)+ifnull(kode_unik,0))'] = $this->input->get('f-total_tagihan');
		}
		if($this->input->get('f-voucher')) {
			$params['code'] = $this->input->get('f-voucher');
		}
		if($this->input->get('f-payment_method')) {
			$params['payment_method'] = $this->input->get('f-payment_method');
		}
		if($this->input->get('f-affiliate')) {
			$params['id_affiliate'] = $this->input->get('f-affiliate');
		}
		if($this->input->get('f-voucher_code')) {
			$params['code'] = $this->input->get('f-voucher_code');
		}
		$params['order_type'] = 'membership';
		if($this->input->get('product_type')) {
			$params['order_type'] = $this->input->get('product_type');
		}

		$params['status'] = 'pending';
		if(group_has_role('commerce','confirm_payment')) {
			$data['payment_status'] = 'confirm_payment';
		} else {
			$data['payment_status'] = 'completed';
		}
		if(!$this->input->get('payment_status')) {
			$_GET['payment_status'] = 'confirm_payment';
		}
		if($this->input->get('payment_status')) {
			if($this->input->get('payment_status')=='confirm_payment') {
				$data['payment_status'] = 'confirm_payment';
				$params['status_pesanan'] = 'waiting-payment';
				$params['payment_method'] = 'transfer';
				$params['status'] = 'pending';
			}elseif($this->input->get('payment_status')=='process') {
				$data['payment_status'] = 'process';
				$params['payment_status'] = 'process';
				unset($params['status']);
			}elseif(in_array($this->input->get('payment_status'),array('completed','expired','cancelled','declined'))) {
				$data['payment_status'] = $this->input->get('payment_status');
				unset($params['status']);
				$params['payment_status'] = $this->input->get('payment_status');
			}
		}

		if($permission_type == 'affiliate') {
			$params['payment_method'] = $this->input->get('payment_method');
			// unset($params['status']);
			// unset($params['status_pesanan']);
		}

		if($data['payment_status']=='all') {
			unset($params['status_pesanan']);
			unset($params['status']);
		}

		// -------------------------------------
		// Get entries
		// -------------------------------------
		
        $order['entries'] = $this->order_m->get_order(NULL,$params,1);
        if(count($order['entries'])==0) {
	        $order_data = array(array(lang('commerce:order:no_entry')));
	    }
		foreach ($order['entries'] as $key => $order_entry) {
			$order_data[$key]['Tanggal Pesanan'] = (isset($order_entry['placed_on'])) ? date('Y-m-d H:i', strtotime($order_entry['placed_on'])) : date('Y-m-d H:i', strtotime($order_entry['created_on']));
			$order_data[$key]['No Invoice'] = (isset($order_entry['no_invoice'])) ? $order_entry['no_invoice'] : '-';
			$order_data[$key]['Nama Customer'] = user_displayname($order_entry['id_customer'],false);
			$order_data[$key]['Email Customer'] = user_email($order_entry['id_customer'],false);
			
			if(group_has_role('commerce','view_customer_mobile')) {
				$order_data[$key]['No Hp Customer'] = (isset($order_entry['no_handphone'])) ? $order_entry['no_handphone'] : '-';
			}
			$total_tagihan = ($order_entry['biaya_order']-$order_entry['discount_nominal']) + $order_entry['biaya_pengiriman']+$order_entry['kode_unik'];
			$order_data[$key]['Total Tagihan'] = $total_tagihan;
			$order_data[$key]['Status Pesanan'] = $order_entry['status_pesanan'];
			$order_data[$key]['Status Pembayaran'] = (isset($order_entry['status_pembayaran'])) ? $order_entry['status_pembayaran'] : '-';
			$order_data[$key]['Metode Pembayaran'] = (isset($order_entry['payment_method'])) ? $order_entry['payment_method'] : (isset($order_entry['no_invoice'])) ? 'transfer' : '-';
			$order_data[$key]['Voucher'] = (isset($order_entry['no_invoice'])) ? $order_entry['no_invoice'] : '-';

			if(group_has_role('commerce','view_all_order')) {
				$order_data[$key]['Affiliate'] = (isset($order_entry['id_affiliate']) AND $order_entry['id_affiliate'] != 0) ? user_displayname($order_entry['id_affiliate'],false) : '-';
			}			
		}
		download_csv($order_data, "data_order_" . date('Ymd'));
    }
	
	/**
     * Display one order
     *
     * @return  void
     */
    public function view($id = 0)
    {
    	$this->load->model('location/kota_m');
		$this->load->model('users/user_m');

        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_order') AND ! group_has_role('commerce', 'view_own_order') AND !group_has_role('commerce', 'view_affiliate_order') AND $this->current_user->is_affiliate == 0){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['order'] = $this->order_m->get_order_by_id($id);

        if(!$data['order']) {
        	$this->session->set_flashdata('error','Pesanan tidak ditemukan');
        	redirect('admin/commerce/order/index');
        }

        $data['order_product'] = $this->order_m->get_order_product($data['order']['id']);
        $data['order']['kota'] = $this->kota_m->get_kota_by_id($data['order']['id_kota_pengiriman']);
		$data['user'] = $this->user_m->get(array('id'=>$data['order']['id_customer']));
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_order')){
			$permission_type = ($this->input->get('type') != '') ? $this->input->get('type') : 'own';
			if($permission_type == 'own' and group_has_role('commerce', 'view_own_order')){
				if($data['order']['created_by'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			} elseif($permission_type == 'affiliate' and (group_has_role('commerce', 'view_affiliate_order') OR $this->current_user->is_affiliate == 1)){
				if($data['order']['id_affiliate'] != $this->current_user->id){
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		if($this->input->is_ajax_request()) {
			$this->template->set_layout(false);
		}
        $this->template->title(lang('commerce:order:view').' '.$data['order']['no_invoice'])
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_order') OR group_has_role('commerce', 'view_own_order')){
			$this->template->set_breadcrumb(lang('commerce:order:plural'), '/admin/commerce/order/index');
		}

		$this->template->set_breadcrumb($data['order']['no_invoice'])
			->build('admin/order_entry', $data);
    }
	
	/**
     * Create a new order entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'create_order')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_order('new')){	
				$this->session->set_flashdata('success', lang('commerce:order:submit_success'));				
				redirect('admin/commerce/order/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:order:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/commerce/order/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:order:new'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_order') OR group_has_role('commerce', 'view_own_order')){
			$this->template->set_breadcrumb(lang('commerce:order:plural'), '/admin/commerce/order/index');
		}

		$this->template->set_breadcrumb(lang('commerce:order:new'))
			->build('admin/order_form', $data);
    }
	
	/**
     * Edit a order entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the order to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'edit_all_order') AND ! group_has_role('commerce', 'edit_own_order')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('commerce', 'edit_all_order')){
			$entry = $this->order_m->get_order_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_order('edit', $id)){	
				$this->session->set_flashdata('success', lang('commerce:order:submit_success'));				
				redirect('admin/commerce/order/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:order:submit_failure');
			}
		}
		
		$data['fields'] = $this->order_m->get_order_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/commerce/order/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:order:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_order') OR group_has_role('commerce', 'view_own_order')){
			$this->template->set_breadcrumb(lang('commerce:order:plural'), '/admin/commerce/order/index')
			->set_breadcrumb(lang('commerce:order:view'), '/admin/commerce/order/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('commerce:order:plural'))
			->set_breadcrumb(lang('commerce:order:view'));
		}

		$this->template->set_breadcrumb(lang('commerce:order:edit'))
			->build('admin/order_form', $data);
    }
	
	/**
     * Delete a order entry
     * 
     * @param   int [$id] The id of order to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'delete_all_order') AND ! group_has_role('commerce', 'delete_own_order')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		$entry = $this->order_m->get_order_by_id($id);
		if(! group_has_role('commerce', 'delete_all_order')){
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		if(!$entry) {
        	$this->session->set_flashdata('error','Pesanan tidak ditemukan');
        	redirect('admin/commerce/order/index');
        }
        
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->order_m->delete_order_by_id($id);
        $this->session->set_flashdata('error', lang('commerce:order:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('admin/commerce/order/index'.$data['uri']);
    }
	
	/**
     * Insert or update order entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_order($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('commerce:order:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->order_m->insert_order($values);
				
			}
			else
			{
				$result = $this->order_m->update_order($values, $row_id);
			}
		}
		
		return $result;
	}

	public function update_resi($row_id)
	{
		$values = $this->input->post();
		// Set validation rules
		$this->form_validation->set_rules('no_resi_pengiriman', lang('commerce:no_resi_pengiriman'), 'required|max_length[100]');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			$data['order'] = $this->order_m->get_order_by_many(array('default_commerce_order.id'=>$row_id));
			$exist_history = (isset($data['order']['state_history'])) ? json_decode($data['order']['state_history'],true) : array();
			$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>'sent. No Resi '.$values['no_resi_pengiriman'],'updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'));
			$state_history = array_merge($exist_history,$new_history);

			$values['state_history'] = json_encode($state_history);

			$result = $this->order_m->update_order($values, array('id'=>$row_id));
			echo json_encode(array('status'=>'success','messages'=>'Resi berhasil dimasukkan'));
			$this->load->model('location/kota_m');
	        $this->load->model('users/user_m');

			$data['order_product'] = $this->order_m->get_order_product($data['order']['id']);
	        $data['order']['kota'] = $this->kota_m->get_kota_by_id($data['order']['id_kota_pengiriman']);
			$data['user'] = $this->user_m->get(array('id'=>$data['order']['id_customer']));

	        $pesanan_dikirim = array(
	        	'subject' => '[INVOICE #'.$data['order']['no_invoice'].'] Pesanan telah dikirim',
	        	'body' => 'Pesanan anda sudah kami kirim dengan No Resi '.$values['no_resi_pengiriman']
	        	);

	        send_email_api($data['user']->email,$pesanan_dikirim);
		} else {
			echo json_encode(array('status'=>'error','messages'=>validation_errors()));
		}
	}

	// --------------------------------------------------------------------------

	/**
     * update order status entry
     *
     * We're passing the
     * id of the entry, which will be updated
     *
     * @param   int [$id] The id of the order to the be updated.
     * @return	void
     */
    public function send_product($id = 0)
    {
		$data['uri'] = get_query_string(6);
        
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'edit_order_status')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin/commerce/order/index'.$data['uri']);
		}		
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------

		$values = NULL;
		$values['status_pesanan'] = 'completed';
		$update_order = $this->order_m->update_order($values, array('id' => $id));

		if($update_order !== false) {
			$this->load->model('location/kota_m');
	        $this->load->model('users/user_m');

			$data['order'] = $this->order_m->get_order_by_many(array('id'=>$id));
	        $data['order_product'] = $this->order_m->get_order_product($data['order']['id']);
	        $data['order']['kota'] = $this->kota_m->get_kota_by_id($data['order']['id_kota_pengiriman']);
			$data['user'] = $this->user_m->get(array('id'=>$data['order']['id_customer']));

	        $pesanan_dikirim = array(
	        	'subject' => '[INVOICE #'.$data['order']['no_invoice'].'] Pesanan telah dikirim',
	        	'body' => $this->load->view('email_template/pesanan_dikirim',$data,true)
	        	);

	        send_email_api($data['user']->email,$pesanan_dikirim);

			$this->session->set_flashdata('success', lang('commerce:order:submit_success'));
		} else {
			$this->session->set_flashdata('error', lang('commerce:order:submit_failure'));
		}
		
		redirect('admin/commerce/order/index'.$data['uri']);

    }
}