<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Commerce Module
 *
 * Modul untu me-manage simple commerce
 *
 */
class Commerce_rekening extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'rekening';

    public function __construct()
    {
        parent::__construct();
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('commerce');		
		$this->load->model('rekening_m');
    }

    /**
	 * List all rekening
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_rekening') AND ! group_has_role('commerce', 'view_own_rekening')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('');
		}

		// -------------------------------------
		// Filters
		// -------------------------------------

		$filters = NULL;
		if(! group_has_role('commerce', 'view_all_rekening') AND group_has_role('commerce', 'view_own_rekening')){
			$filters['created_by'] = $this->current_user->id;
		}

		if($this->input->get('f-id_affiliate')) {
			$filters['id_affiliate'] = $this->input->get('f-id_affiliate');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'commerce/rekening/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->rekening_m->count_all_rekening($filters);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$pagination_config['full_tag_open'] = '<div class=""><ul class="pagination no-margin">';
		$pagination_config['full_tag_close'] = '</ul></div>';
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(4);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['rekening']['entries'] = $this->rekening_m->get_rekening($pagination_config,$filters);
		$data['rekening']['total'] = $pagination_config['total_rows'];
		$data['rekening']['pagination'] = $this->pagination->create_links();

		$group_affiliate = $this->ion_auth->get_group_by_name('affiliate');
		$this->load->model('affiliate_m');
		$data['affiliate_lists'] = $this->affiliate_m->get_affiliate($group_affiliate->id);

		// -------------------------------------
        // Build the page. See views/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['rekening']['entries'])==0 AND ($this->uri->segment(4) != NULL AND $this->uri->segment(4)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(4), '', $data['uri']);
			if($this->uri->segment(4) != '' AND $this->uri->segment(4)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(4)-$pagination_config['per_page'];
				redirect('commerce/rekening/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('commerce/rekening/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('commerce:rekening:plural'))
        	->set_layout('member_area.html')
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('commerce:rekening:plural'))
			->build('rekening_index', $data);
    }
	
	/**
     * Display one rekening
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_rekening') AND ! group_has_role('commerce', 'view_own_rekening')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['rekening'] = $this->rekening_m->get_rekening_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_rekening')){
			if($data['rekening']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('');
			}
		}

		// -------------------------------------
        // Build the page. See views/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('commerce:rekening:view'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_rekening') OR group_has_role('commerce', 'view_own_rekening')){
			$this->template->set_breadcrumb(lang('commerce:rekening:plural'), '/commerce/rekening/index');
		}

		$this->template->set_breadcrumb(lang('commerce:rekening:view'))
			->set_layout('member_area.html')
			->build('rekening_entry', $data);
    }
	
	/**
     * Create a new rekening entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'create_rekening')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			// redirect('');
		}

		$data['uri'] = get_query_string(4);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_rekening('new')){	
				$this->session->set_flashdata('success', lang('commerce:rekening:submit_success'));				
				redirect('commerce/rekening/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:rekening:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'commerce/rekening/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:rekening:new'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_rekening') OR group_has_role('commerce', 'view_own_rekening')){
			$this->template->set_breadcrumb(lang('commerce:rekening:plural'), '/commerce/rekening/index');
		}

		$this->template->set_breadcrumb(lang('commerce:rekening:new'))
			->set_layout('member_area.html')
			->build('rekening_form', $data);
    }
	
	/**
     * Edit a rekening entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the rekening to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'edit_all_rekening') AND ! group_has_role('commerce', 'edit_own_rekening')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('');
		}
		
		// Check view all/own permission
		if(! group_has_role('commerce', 'edit_all_rekening')){
			$entry = $this->rekening_m->get_rekening_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('');
			}
		}

		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_rekening('edit', $id)){	
				$this->session->set_flashdata('success', lang('commerce:rekening:submit_success'));				
				redirect('commerce/rekening/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:rekening:submit_failure');
			}
		}
		
		$data['fields'] = $this->rekening_m->get_rekening_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'commerce/rekening/index/'.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:rekening:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_rekening') OR group_has_role('commerce', 'view_own_rekening')){
			$this->template->set_breadcrumb(lang('commerce:rekening:plural'), '/commerce/rekening/index');
		}else{
			$this->template->set_breadcrumb(lang('commerce:rekening:plural'))
			->set_breadcrumb(lang('commerce:rekening:view'));
		}

		$this->template->set_breadcrumb(lang('commerce:rekening:edit'))
			->set_layout('member_area.html')
			->build('rekening_form', $data);
    }
	
	/**
     * Delete a rekening entry
     * 
     * @param   int [$id] The id of rekening to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'delete_all_rekening') AND ! group_has_role('commerce', 'delete_own_rekening')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('');
		}
		// Check view all/own permission
		if(! group_has_role('commerce', 'delete_all_rekening')){
			$entry = $this->rekening_m->get_rekening_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->rekening_m->delete_rekening_by_id($id);
        $this->session->set_flashdata('error', lang('commerce:rekening:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('commerce/rekening/index'.$data['uri']);
    }
	
	/**
     * Insert or update rekening entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_rekening($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama_bank', lang('commerce:nama_bank'), 'required|max_length[100]');
		$this->form_validation->set_rules('no_rekening', lang('commerce:no_rekening'), 'required|max_length[100]');
		$this->form_validation->set_rules('atas_nama', lang('commerce:atas_nama'), 'required|max_length[100]');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			// if the user who logged in is affiliate
			// set id affiliate using id of current user
			$this->load->library('users/ion_auth');
            $default_group = $this->ion_auth->get_group_by_name('affiliate');
            if($this->current_user->group_id == $default_group->id) {
            	$values['id_affiliate'] = $this->current_user->id;
            }

            if(!isset($values['is_default'])) {
            	$values['is_default'] = 0;
            }

			if ($method == 'new')
			{
				$result = $this->rekening_m->insert_rekening($values);
				
			}
			else
			{
				$result = $this->rekening_m->update_rekening($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}