<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Commerce Module
 *
 * Modul untu me-manage simple commerce
 *
 */
class Commerce extends Public_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        // redirect('commerce/order/index');
        $this->load->library('variables/variables');

        $this->load->helper('commerce');
        $this->load->helper('email/sendgrid');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function elearning()
    {
        if(!is_logged_in()) {
            $log_data = array(
                'time' => date('Y-m-d H:i:s'),
                'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
                'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
                'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
                'function' => 'commerce/elearning',
                'url' => base_url(uri_string()),
                'get_data' => json_encode($_GET),
                'post_data' => json_encode($_POST),
                'messages' => 'User belum login'
                );
            log_message('error',json_encode($log_data));
            redirect('');
        }
        $group_id = $this->current_user->group_id;
        $this->load->model('groups/group_m');
        $group = $this->group_m->get_group_by_id($group_id);
        if($group->name!='customer') {
            redirect('admin');
        }

        $data['default_id_product'] = $this->variables->__get('default_id_product');

        // -------------------------------------
        // Build the page.
        // -------------------------------------

        $this->template->title('Akses E-Learning Platform')
            ->set_breadcrumb('Home', '/')
            ->set_layout('member_area.html')
            ->build('elearning_platform', $data);
    }

    public function become_affiliate()
    {
        if($_POST) {
            $validation = array(
                array(
                    'field' => 'agree',
                    'label' => 'Checkbox persetujuan',
                    'rules' => 'required|numeric|callback_check_group',
                    ),
                );
            $this->form_validation->set_message('required','%s harus di pilih');
            // Set the validation rules
            $this->form_validation->set_rules($validation);

            $result = false;
            // If the validation worked,
            if ($this->form_validation->run())
            {
                $default_id_product = $this->variables->__get('default_id_product');
                if(is_logged_in()) {
                    $member_group = $this->ion_auth->get_group_by_name('member');

                    $id_customer = (isset($this->current_user->id)) ? $this->current_user->id : 0;
                    $this->load->model('order_m');
                    $payment = $this->order_m->check_affiliate_order($id_customer);
                    $this->load->model('landing_page/konten_m');

                    if($payment) {
                        $this->load->library('users/ion_auth');
                        // $default_group = $this->ion_auth->get_group_by_name('affiliate');
                        $this->load->model('users/user_m');
                        if(!in_array($this->current_user->group, array('site_admin','admin','staf'))) {
                            $result = $this->user_m->become_affiliate($this->current_user->id);
                        }

                        if($result) {
                            // create landing page
                            $this->load->model('landing_page/konten_m');
                            $landing_page = $this->konten_m->get_konten();
                            foreach ($landing_page as $lp) {
                                $insert_data = array(
                                    'id_landing_page' => $lp['id'],
                                    'id_affiliate' => $id_customer,
                                    'uri_segment_string' => $id_customer
                                    );
                                $landing_page = $this->konten_m->insert_konten_affiliate($insert_data,$id_customer);
                            }

                            $data['landing_page']['uri_segment_string'] = $insert_data['uri_segment_string'];

                            // send notif success on becomeing affiliate
                            $menjadi_affiliate = array(
                                'subject' => 'Selamat dan terima kasih telah menjadi affiliate kami',
                                'body' => $this->load->view('email_template/menjadi_affiliate',$data,true)
                                );

                            send_email_api($this->current_user->email,$menjadi_affiliate);

                            exit(json_encode(array('status'=>'success')));
                        }
                    } else {
                        $params = array(
                            'id_customer'=>$id_customer,
                            'status_pesanan'=>array('waiting-payment','completed')
                            );
                        $order = $this->order_m->get_order_by_many($params);
                        if($order != null) {
                            exit(json_encode(array('status'=>'error','message'=>'Anda belum terdaftar sebagai Member Premium, mohon selesaikan pesanan Anda terlebih dahulu. <a href="'.base_url().'commerce/order/my_order">Lihat daftar pesanan saya</a>')));
                        } else {
                            exit(json_encode(array('status'=>'error','message'=>'Anda belum terdaftar sebagai Member Premium, silakan melakukan <a href="'.base_url().'commerce/cart/add?product='.$default_id_product.'">upgrade membership</a> terlebih dahulu')));
                        }
                    }
                }else {
                    exit(json_encode(array('status'=>'error','message'=>'Gagal menjadi affiliate. Silakan <a href="'.base_url().'login">login</a> atau <a href="'.base_url().'commerce/cart/add?product='.$default_id_product.'">daftar</a> terlebih dahulu')));
                }
            } else {
                $validation_errors = validation_errors();
                if($validation_errors == '') {
                    $validation_errors = 'Gagal menjadi affiliate. Checkbox harus di pilih.';
                }
                exit(json_encode(array('status'=>'error','message'=>$validation_errors)));
            }
        }

        $this->template->set_layout(false)
        ->build('menjadi_affiliate');
    }

    public function cek_pembelian() {
        $id_customer = (isset($this->current_user->id)) ? $this->current_user->id : 0;
        $this->load->model('order_m');
        $payment = $this->order_m->check_affiliate_order($id_customer);
        if($payment != null) {
            return true;
        }
        return false;
    }

    public function check_group() {
        if(is_logged_in()) {
            if($this->current_user->is_affiliate=='1') {
                $this->form_validation->set_message('check_group','Anda sudah menjadi affiliate. <a href="'.base_url().'admin/commerce/order/index/affiliate">Lihat halaman affiliate saya</a>');
                return false;
            } elseif ($this->current_user->group=='site_admin' OR $this->current_user->group=='staf') {
                $this->form_validation->set_message('check_group','Anda telah terdaftar sebagai '.$this->current_user->group.' dan tidak dapat menjadi affiliate.');
                return false;
            }
        }
        return true;
    }

    public function expired_order($key = '') {
        $time = date('Y-m-d H:i');

        if($key != 'K2ynwLI6827d5ZB2QtJsyqah3Q572LqS') {
            exit(json_encode(array('status'=>'failed','message'=>'Wrong Key!','time'=>$time)));
        }

        $this->load->model('order_m');

        $expired_order = $this->order_m->expired_order($time);

        if(isset($expired_order)) {
            if(count($expired_order)>0) {
                // send notif to the customer
                foreach ($expired_order as $key => $order) {
                    if(isset($order['no_invoice'])) {
                        $expired_ids[] = $order['id'];
                        $email_data['order'] = $order;
                        $invoice_expired = array(
                            'subject' => '[INVOICE #'.$email_data['order']['no_invoice'].'] Telah Expired',
                            'body' => $this->load->view('email_template/invoice_expired',$email_data,true)
                            );

                        send_email_api($order['email'],$invoice_expired);
                    }
                }
                exit(json_encode(array('status'=>'success','message'=>'Success Update Order','time'=>$time,'ids'=>json_encode($expired_ids))));
            } else {
                exit(json_encode(array('status'=>'success','message'=>'There is no expired order','time'=>$time)));
            }
        } else {
            exit(json_encode(array('status'=>'failed','message'=>'Unknown Error','time'=>$time)));
        }
    }

    public function email_blast()
    {
        $this->load->model('groups/group_m');
        $this->load->model('users/user_m');
        $group_member = $this->ion_auth->get_group_by_name('member');
        $group_affiliate = $this->ion_auth->get_group_by_name('affiliate');

        $users = (isset($group_member->id)) ? $this->user_m->get_many_by(array('group_id'=>$group_member->id)) :array();
        // $users_affiliate = (isset($group_affiliate->id)) ? $this->user_m->get_many_by(array('group_id'=>$group_affiliate->id)) : array();
        // $users = array_merge($users_member, $users_affiliate);
        
        foreach ($users as $user) {
            if($user->email != 'rendy@duakodikartika.com') {
                $recipients[] = $user->email;
            }
        }
        if(count($recipients)>500) {
            $batch_no = 0;
            foreach ($recipients as $key => $recipient) {
                $batch_recipients[$batch_no][] = $recipient;
                if($key!=0 AND $key % 500==0) {
                    $batch_no++;
                }
            }
        } else {
            $batch_recipients[0][] = $recipients;
        }

        foreach ($batch_recipients as $recipients) {
            $blast = array(
                'subject' => 'Tentang BelajarSOP.com',
                'body' => $this->load->view('email_template/blast',null,true)
                );
            send_email_api($recipients,$blast);
        }
    }

    public function error_checkout()
    {
        $this->template->set_layout('maintenance.html')
        ->build('error_checkout');
    }
}
