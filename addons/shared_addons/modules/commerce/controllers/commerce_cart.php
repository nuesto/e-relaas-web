<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Commerce Module
 *
 * Modul untu me-manage simple commerce
 *
 */
class Commerce_cart extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------

    protected $section = 'cart';

    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('commerce');

		$this->load->model('cart_m');
		$this->load->model('produk_m');
		$this->load->model('order_m');

		$this->load->library('variables/variables');
		$start_close_date = $this->variables->__get('start_close_date');
        $end_close_date = $this->variables->__get('end_close_date');

        date_default_timezone_set("Asia/Jakarta");

        if(strtotime($start_close_date) <= strtotime(date('Y-m-d H:i')) AND strtotime($end_close_date) >= strtotime(date('Y-m-d H:i'))) {
        	redirect('countdown');
        }

    }

    public function test_recal($id_order)
    {
    	$this->cart_m->recalculate_biaya_order($id_order);
    }

    /**
	 * List all cart
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'view_all_cart') AND ! group_has_role('commerce', 'view_own_cart')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}

        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'commerce/cart/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->cart_m->count_all_cart();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;

        // -------------------------------------
		// Get entries
		// -------------------------------------

        $data['cart']['entries'] = $this->cart_m->get_cart($pagination_config);
		$data['cart']['total'] = count($data['cart']['entries']);
		$data['cart']['pagination'] = $this->pagination->create_links();

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page.
		// -------------------------------------

        $this->template->title(lang('commerce:cart:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('commerce:cart:plural'))
			->build('cart_index', $data);
    }

	/**
     * Display one cart
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'view_all_cart') AND ! group_has_role('commerce', 'view_own_cart')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}

		// -------------------------------------
		// Get our entry.
		// -------------------------------------

        $data['cart'] = $this->cart_m->get_cart_by_id($id);

		// Check view all/own permission
		if(! group_has_role('commerce', 'view_all_cart')){
			if($data['cart']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page.
		// -------------------------------------

        $this->template->title(lang('commerce:cart:view'))
			->set_breadcrumb('Home', '/home');

		if(group_has_role('commerce', 'view_all_cart') OR group_has_role('commerce', 'view_own_cart')){
			$this->template->set_breadcrumb(lang('commerce:cart:plural'), '/commerce/cart/index');
		}

		$this->template->set_breadcrumb(lang('commerce:cart:view'))
			->build('cart_entry', $data);
    }

	/**
     * Create a new cart entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'create_cart')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}

		// -------------------------------------
		// Process POST input
		// -------------------------------------

		if($_POST){
			if($this->_update_cart('new')){
				$this->session->set_flashdata('success', lang('commerce:cart:submit_success'));
				redirect('commerce/cart/index');
			}else{
				$data['messages']['error'] = lang('commerce:cart:submit_failure');
			}
		}

		$data['mode'] = 'new';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'commerce/cart/index'.$data['uri'];

		// -------------------------------------
		// Build the form page.
		// -------------------------------------

        $this->template->title(lang('commerce:cart:view'))
			->set_breadcrumb('Home', '/home');

		if(group_has_role('commerce', 'view_all_cart') OR group_has_role('commerce', 'view_own_cart')){
			$this->template->set_breadcrumb(lang('commerce:cart:plural'), '/commerce/cart/index');
		}

		$this->template->set_breadcrumb(lang('commerce:cart:new'))
			->build('cart_form', $data);
    }

	/**
     * Edit a cart entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the cart to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'edit_all_cart') AND ! group_has_role('commerce', 'edit_own_cart')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('commerce', 'edit_all_cart')){
			$entry = $this->cart_m->get_cart_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		// -------------------------------------
		// Process POST input
		// -------------------------------------

		if($_POST){
			if($this->_update_cart('edit', $id)){
				$this->session->set_flashdata('success', lang('commerce:cart:submit_success'));
				redirect('commerce/cart/index');
			}else{
				$data['messages']['error'] = lang('commerce:cart:submit_failure');
			}
		}

		$data['fields'] = $this->cart_m->get_cart_by_id($id);
		$data['mode'] = 'edit';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'commerce/cart/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;

		// -------------------------------------
		// Build the form page.
		// -------------------------------------
       $this->template->title(lang('commerce:cart:edit'))
			->set_breadcrumb('Home', '/home');

		if(group_has_role('commerce', 'view_all_cart') OR group_has_role('commerce', 'view_own_cart')){
			$this->template->set_breadcrumb(lang('commerce:cart:plural'), '/commerce/cart/index')
			->set_breadcrumb(lang('commerce:cart:view'), '/commerce/cart/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('commerce:cart:plural'))
			->set_breadcrumb(lang('commerce:cart:view'));
		}

		$this->template->set_breadcrumb(lang('commerce:cart:edit'))
			->build('cart_form', $data);
    }

	/**
     * Delete a cart entry
     *
     * @param   int [$id] The id of cart to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'delete_all_cart') AND ! group_has_role('commerce', 'delete_own_cart')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('commerce', 'delete_all_cart')){
			$entry = $this->cart_m->get_cart_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		// -------------------------------------
		// Delete entry
		// -------------------------------------

        $this->cart_m->delete_cart_by_id($id);
		$this->session->set_flashdata('error', lang('commerce:cart:deleted'));

		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(5);
        redirect('commerce/cart/index'.$data['uri']);
    }

	/**
     * Insert or update cart entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_cart($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->load->helper(array('form', 'url'));

 		// -------------------------------------
		// Set Values
		// -------------------------------------

		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------

		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('commerce:cart:field_name'), 'required');

		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');

		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->cart_m->insert_cart($values);
			}
			else
			{
				$result = $this->cart_m->update_cart($values, $row_id);
			}
		}

		return $result;
	}

	// --------------------------------------------------------------------------

	public function add()
	{
		if(!$this->input->get('product') AND !$this->input->get('product_ext')) {
			$this->session->set_flashdata('error','Tidak ada produk yang akan ditambahkan ke cart');
			redirect('commerce/error_checkout');
		}

		if(!$this->input->get('product_type')) {
			$this->session->set_flashdata('error','parameter "product_type" tidak ditemukan');
			redirect('commerce/error_checkout');
		}

		if(!in_array($this->input->get('product_type'), array('membership','physical','donation'))) {
			$this->session->set_flashdata('error','Produk tipe tidak ditemukan');
			redirect('commerce/error_checkout');
		}

        if($this->input->get('product')) {
            $id_products = $this->input->get('product');
            $is_external = false;
        } else {
            $id_products = $this->input->get('product_ext');
            $is_external = true;
        }
		$ids = explode(',', $id_products);
		$prices_data = explode(',', $this->input->get('prices'));
		foreach ($prices_data as $key => $data) {
			$prices[$ids[$key]] = $data;
		}
		$products = $this->produk_m->get_produk_by_ids($ids, $is_external);
		if($products == NULL) {
			$this->session->set_flashdata('error','Produk tidak ditemukan');
			redirect('commerce/error_checkout');
		}

		if(!$this->session->userdata('cart_session')) {
			$this->session->set_userdata("cart_session", $this->session->userdata('session_id'));
		}

		// set id_affiliate
		$id_affiliate = NULL;
		// if there is session use it
		if($this->session->userdata('id_affiliate')) {
			$id_affiliate = $this->session->userdata('id_affiliate');
		}
		// if there is cookie use it
		if(get_cookie('id_affiliate',true)) {
			$id_affiliate = get_cookie('id_affiliate',true);
		}
		// if there is get parameter, use it instead
		if($this->input->get('affiliate')) {
			$id_affiliate = $this->input->get('affiliate');
		}

		foreach ($products as $key => $product) {
			$is_digital_product = $product['is_digital_product'];
			if($is_digital_product=='1') {
				$product_type = 'membership';
			} else {
				$product_type = 'physical';
			}
		}

		// get current order
		$not_corresponding = 0;
		foreach ($products as $key => $product) {
			$product_type = $product['type'];
			if($product_type != $this->input->get('product_type')) {
				unset($products[$key]);
				$not_corresponding ++;
			}
		}

		if($not_corresponding == count($products)+1) {
			$this->session->set_flashdata('error','Tidak dapat membuat order karena tidak ada tipe produk sesuai');
			redirect('commerce/error_checkout');
		}
		$current_order = $this->order_m->get_current_order(NULL,NULL,$product_type);

		// check if the order already have id_customer
		if(isset($current_order['id_customer'])) {
			// check if the affiliate change
			if($current_order['id_affiliate'] != $id_affiliate) {
				// make customer to choose weather to cancel the previous order and
				// make new order with new affiliate or to continue this one
				redirect('commerce/cart/cancel_or_continue?'.http_build_query($_GET));
			}
		}

		$order_values = array(
			'id_customer' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
			'id_session' => (isset($current_order['id_session'])) ? $current_order['id_session'] : $this->session->userdata('cart_session'),
			'status_pesanan' => 'in-cart',
			'id_affiliate' => $id_affiliate,
			'state_history' => json_encode(array(array('time'=>date('Y-m-d H:i:s'),'status'=>'in-cart','updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'))),
			'order_type' => $product_type
			);

		if($this->input->get('landing_page')) {
			$order_values['id_landing_page'] = $this->input->get('landing_page');
		}

		$err_message = NULL;
		foreach ($products as $key => $product) {
			// set harga and jumlah depent on product type
			$harga = 0;
			$jumlah = 0;
			if($product['type']=='membership') {
				$harga = $product['harga'];
				$jumlah = 1;
			}elseif ($product['type']=='donation') {
				if(!isset($prices[$product['id']]) OR $prices[$product['id']] == '' OR $prices[$product['id']] == '0') {
					$err_message[] = 'Harap masukkan jumlah donasi untuk <strong>'.ucwords($product['nama']).'</strong>';
					continue;
				}
				$harga = $prices[$product['id']];
				$jumlah = 1;
			}elseif($product['type']=='physical') {
				$harga = $product['harga'];
				$jumlah = 1;
			}
			$cart_values[] = array(
				'id_produk' => $product['id'],
				'harga' => $harga,
				'jumlah' => $jumlah,
				'type' => $product['type'],
				'id_unique' => $this->cart_m->generate_unique(),
				'is_digital_product' => $product['is_digital_product']
				);
		}

		if($err_message != null and count($err_message)>0) {
			$err_message = implode("<br>", $err_message);
			$this->session->set_flashdata('error',$err_message);
			redirect('commerce/error_checkout');
		}

		$add_cart = $this->cart_m->insert_cart($order_values,$cart_values,$product_type, $current_order);

		if($this->input->get('voucher')) {
			$voucher_code = $this->input->get('voucher');
			$order = $this->order_m->get_current_order(null);

			$check = $this->check_voucher_code($voucher_code, $order['id']);
			
			if($check['status']) {
				$row_id = $order['id'];
				$order = $this->order_m->get_order_by_id($row_id);
				$voucher_id = $check['voucher']['voucher_id'];
				$id_affiliate = $check['voucher']['id_affiliate'];
				$order_values = array(
						'id_affiliate' => $id_affiliate,
						'voucher_id' => $voucher_id,
						'voucher_applied_on' => $check['voucher']['voucher_applied_on'],
						'voucher_applied_data' => $check['voucher']['voucher_applied_data'],
						);
				if($check['voucher']['benefit_type']=='discount_nominal') {
					$order_values['discount_nominal'] = $this->input->post('benefit_value');
				} elseif($check['voucher']['benefit_type']=='discount_percentage') {
					$order_values['discount_nominal'] = $order['biaya_order'] * ($check['voucher']['benefit_value']/100);
				}
				$result = $this->order_m->update_order($order_values, array('id' => $row_id));
			} else {
				$this->session->set_flashdata('error',$check['message']);
			}
		}

		$next_step = (!$this->input->get('next_step')) ? 'commerce/order/customer_info' : $this->input->get('next_step');
		redirect($next_step);
	}

	public function update()
	{
		$id_customer = NULL;
		if(isset($this->current_user->id)) {
			$id_customer = $this->current_user->id;
		}
		$current_order = $this->order_m->get_current_order($id_customer);

		if(!$this->input->post('id_produk')) {
			show_404();
		}
		$ids = $this->input->post('id_produk');
		$quantities = $this->input->post('jumlah');
		$prices = $this->input->post('harga');
		// count eact item group by id
		$count_ids = array();
		foreach ($ids as $key => $value) {
			if(array_key_exists($value,$count_ids)) {
				$count_ids[$value]++;
			} else {
				$count_ids[$value] = 1;
			}
		}

		// set quantity and price
		foreach ($quantities as $key => $quantity) {
			if($count_ids[$ids[$key]] == 1) {
				$products_quantity[$ids[$key]] = $quantity;
			} else {
				$products_quantity[$ids[$key]][] = $quantity;
			}
		}
		if(isset($prices)) {
			foreach ($prices as $key => $price) {
				if($count_ids[$ids[$key]] == 1) {
					$products_price[$ids[$key]] = $price;
				} else {
					$products_price[$ids[$key]][] = $price;
				}
			}
		}

		// get products info
		$products = $this->produk_m->get_produk_by_ids($ids);
		if(!$this->session->userdata('cart_session')) {
			$this->session->set_userdata("cart_session", $this->session->userdata('session_id'));
		}

		$biaya_order = 0;
		foreach ($products as $key => $product) {
			// check if there is multi quantity and price for that product
			if(is_array($products_quantity[$product['id']]) AND is_array($products_price[$product['id']])) {
				foreach ($products_quantity[$product['id']] as $key => $value) {
					// set harga and jumlah depent on product type
					$harga = 0;
					$jumlah = 0;
					if($product['type']=='membership') {
						$harga = $product['harga'];
						$jumlah = 1;
					}elseif ($product['type']=='donation') {
						$harga = $products_price[$product['id']][$key];
						$jumlah = 1;
					}elseif($product['type']=='physical') {
						$harga = $product['harga'];
						$jumlah = $value;
					}

					$cart_values[] = array(
						'id_produk' => $product['id'],
						'harga' => $harga,
						'jumlah' => $jumlah,
						'type' => $product['type'],
						'id_unique' => $this->cart_m->generate_unique(),
						'is_digital_product' => $product['is_digital_product']
						);
				}
			} else {
				// set harga and jumlah depent on product type
				$harga = 0;
				$jumlah = 0;
				if($product['type']=='membership') {
					$harga = $product['harga'];
					$jumlah = 1;
				}elseif ($product['type']=='donation') {
					$harga = $products_price[$product['id']];
					$jumlah = 1;
				}elseif($product['type']=='physical') {
					$harga = $product['harga'];
					$jumlah = $products_quantity[$product['id']];
				}

				$cart_values[] = array(
					'id_produk' => $product['id'],
					'harga' => $harga,
					'jumlah' => $jumlah,
					'type' => $product['type'],
					'id_unique' => $this->cart_m->generate_unique(),
					'is_digital_product' => $product['is_digital_product']
					);
			}
		}

		$order_values = array();


		foreach ($this->input->post('id_produk') as $key => $product) {
			$config['cart'][$key] = array(
				'field' => 'id_produk['.$key.']',
				'label' => 'Produk',
				'rules' => 'required',
			);
			$config['cart'][$key] = array(
				'field' => 'harga['.$key.']',
				'label' => 'Harga Produk '.($key+1),
				'rules' => 'required|numeric|greater_than_equal_to[1]|integer',
			);
			$config['cart'][$key] = array(
				'field' => 'jumlah['.$key.']',
				'label' => 'Jumlah Produk '.($key+1),
				'rules' => 'required|numeric|greater_than_equal_to[1]|integer',
			);
		}

		$this->form_validation->set_rules($config['cart']);

		if($this->form_validation->run()) {
			$update_order = $this->order_m->update_order($order_values,array('id'=>$current_order['id']));

			$update_cart = $this->cart_m->update_cart($cart_values,$current_order['id']);
			$recalculate_biaya_order = $this->cart_m->recalculate_biaya_order($current_order['id']);
			$return_data = array(
				'sub_total'=>number_format($recalculate_biaya_order['biaya_order'],0,',','.')
				);
			if(isset($recalculate_biaya_order['discount_nominal'])) {
				$return_data['discount_nominal'] = number_format($recalculate_biaya_order['discount_nominal'],0,',','.');
			}
			$result = array_merge(array('status' => 'success','message' => 'Jumlah pembelian berhasil di ubah'),$return_data);
		} else {
			$result = array('status' => 'failed','message' => validation_errors());
		}
		die(json_encode($result));
	}

	public function finish() {
		$this->session->unset_userdata('cart_session');
	}

	public function check_voucher_code($code, $row_id)
	{
		$this->load->model('voucher_m');
		$order = $this->order_m->get_order_by_id($row_id);
		$voucher = $this->voucher_m->get_voucher_by_code($code);
		if($voucher) {
			// if($order['id_customer'] != NULL AND ($order['id_affiliate'] != NULL OR $order['id_affiliate'] != 0) AND $order['id_affiliate'] == $voucher['affiliate_id'] OR $order['id_affiliate'] == NULL OR $voucher['affiliate_id'] == NULL) {
				if($voucher['state_active']==1) {
					if($voucher['product_type']==$order['order_type']) {
						if($voucher['valid_unit'] == NULL OR (date('Y-m-d',strtotime($voucher['valid_unit'])) >= date('Y-m-d'))) {
							if($voucher['usage_limit_scope']=='limit_per_customer') {
								$id_customer = (isset($this->current_user->id)) ? $this->current_user->id : NULL;
								$params = array('id_customer'=>$id_customer,'code'=>$code);
								$voucher_usage = $this->voucher_m->get_voucher_usage($params);
								if($voucher_usage['usage_count'] < $voucher['usage_limit_count']) {
									$result['status'] = true;
								} else {
									$result['message'] = 'Voucher sudah mencapai batas penggunaan';
									$result['status'] = false;
								}
							} elseif($voucher['usage_limit_scope']=='limit_all_customer') {
								$params = array('code'=>$code);
								$voucher_usage = $this->voucher_m->get_voucher_usage($params);
								if($voucher_usage['usage_count'] < $voucher['usage_limit_count']) {
									$result['status'] = true;
								} else {
									$result['message'] = 'Voucher sudah mencapai batas penggunaan';
									$result['status'] = false;
								}
							} elseif($voucher['usage_limit_count']==null AND $voucher['usage_limit_count']==null) {
								$result['status'] = true;
							}
						} else {
							$result['message'] = 'Voucher sudah expired';
							$result['status'] = false;
						}
					} else {
						$result['message'] = 'Voucher tidak dapat digunakan pada order ini';
						$result['status'] = false;
					}
				} else {
					$result['message'] = 'Voucher tidak aktif';
					$result['status'] = false;
				}
			// } else {
			// 	$result['message'] = 'Tidak dapat menggunakan kode voucher milik affiliate ini karena Anda telah melakukan order melalui link affiliate lain';
			// 	$result['status'] = false;
			// }
		} else {
			$result['message'] = 'Voucher tidak ditemukan';
			$result['status'] = false;
		}

		if(isset($result['message'])) {
			$this->form_validation->set_message('check_voucher_code',$result['message']);
		}

		if($result['status']==true) {
			$result['voucher']['voucher_id'] = $voucher['id'];
			$result['voucher']['voucher_applied_on'] = date('Y-m-d H:i:s');
			$result['voucher']['voucher_applied_data'] = json_encode($voucher);
			$result['voucher']['benefit_type'] = $voucher['benefit_type'];
			$result['voucher']['benefit_value'] = $voucher['benefit_value'];
			$result['voucher']['id_affiliate'] = (isset($voucher['affiliate_id'])) ? $voucher['affiliate_id'] : $order['id_affiliate'];
		}
		return $result;
	}

	public function cancel_or_continue()
	{
		$current_order = $this->order_m->get_current_order();
		$query_string = $this->input->get();
		$landing_page = NULL;

		// set id_affiliate
		$id_affiliate = NULL;
		// if there is session use it
		if($this->session->userdata('id_affiliate')) {
			$id_affiliate = $this->session->userdata('id_affiliate');
		}
		// if there is cookie use it
		if(get_cookie('id_affiliate',true)) {
			$id_affiliate = get_cookie('id_affiliate',true);
		}
		// if there is get parameter, use it instead
		if($this->input->get('affiliate')) {
			$this->load->model('landing_page/konten_m');
			$landing_page = $this->konten_m->get_konten_by_uri($this->input->get('affiliate'));
			if($landing_page) {
				$id_affiliate = $landing_page['id_affiliate'];
			}
		}

		$old_affiliate = $current_order['id_affiliate'];
		$new_affiliate = $id_affiliate;

		$this->template->set_layout('checkout.html')
			->set('current_order',$current_order)
			->set('prev_affiliate',$old_affiliate)
			->set('curr_affiliate',$new_affiliate)
			->build('checkout/change_affiliate');
	}

	public function is_real_number($number)
	{
		dump($number);
		if(!is_int($number)) {
    		$this->form_validation->set_message('is_real_number','<strong>%s</strong> hanya menerima bilangan bulat');
			return false;
		}
		return true;
	}

}
