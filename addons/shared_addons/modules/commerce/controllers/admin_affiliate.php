<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Commerce Module
 *
 * Modul untu me-manage simple commerce
 *
 */
class Admin_affiliate extends Admin_Controller
{
    // -------------------------------------
    // This will set the active section tab
    // -------------------------------------
    
    protected $section = 'affiliate';

    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
        // Load everything we need
        // -------------------------------------

        $this->lang->load('commerce');      
        $this->load->model('affiliate_m');

        $this->lang->load('commerce');		
		$this->load->model('affiliate_m');

        date_default_timezone_set("Asia/Jakarta");
    }

    /**
     * List all affiliate
     *
     * @return  void
     */
    public function index()
    {
        // -------------------------------------
        // Check permission
        // -------------------------------------
        
        if(! group_has_role('commerce', 'view_all_affiliate') AND $this->current_user->is_affiliate == 1){
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }

        $data['uri'] = get_query_string(5);
        
        // -------------------------------------
        // Get entries
        // -------------------------------------
        $data['affiliate']['entries'] = $this->affiliate_m->get_affiliate();
        $data['affiliate']['total'] = count($data['affiliate']['entries']);
        // -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
        // -------------------------------------
        
        $this->template->title(lang('commerce:id_affiliate'))
            ->set_breadcrumb('Home', '/admin')
            ->set_breadcrumb(lang('commerce:id_affiliate'))
            ->build('admin/affiliate_index', $data);
    }

    public function commission()
    {
        // -------------------------------------
        // Check permission
        // -------------------------------------
        
        if(! group_has_role('commerce', 'view_all_affiliate')){
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }

        $data['range'] = $this->input->get('date-range-picker');
        $data['range_start'] = NULL;
        $data['range_end'] = NULL;
        if(isset($data['range']) AND $data['range'] != '') {
            $range = explode("-", $data['range']);
            if(count($range)==2) {
                $data['range_start'] = trim($range[0]);
                $data['range_end'] = trim($range[1]);
            }
        }

        $filters = NULL;
        if(isset($data['range_start']) AND isset($data['range_start'])) {
            $filters['range']['range_start'] = date('Y-m-d',strtotime($data['range_start']));
            $filters['range']['range_end'] = date('Y-m-d',strtotime($data['range_end']));
        }

        $data['commission']['entries'] = $this->affiliate_m->get_commission($filters);

        // get rekening affiliate
        $this->load->model('rekening_m');
        $rekening = $this->rekening_m->get_rekening();
        foreach ($rekening as $key => $value) {
            $rekening_affiliate[(isset($value['id_affiliate'])) ? $value['id_affiliate'] : '0'][] = $value;
        }

        foreach ($data['commission']['entries'] as $key => $commission_entry) {
            if(array_key_exists($commission_entry['user_id'], $rekening_affiliate)) {
                $data['commission']['entries'][$key]['rekening'] = $rekening_affiliate[$commission_entry['user_id']];
            }
        }

        $data['commission']['total'] = count($data['commission']['entries']);

        $data['uri'] = get_query_string(5);

        // -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
        // -------------------------------------
        
        $this->template->title(lang('commerce:affiliate_commission'))
            ->set_breadcrumb('Home', '/admin')
            ->set_breadcrumb(lang('cp:nav_Commerce'))
            ->set_breadcrumb(lang('commerce:affiliate_commission'))
            ->build('admin/affiliate_commission', $data);
    }

    public function download_commission()
    {
        // -------------------------------------
        // Check permission
        // -------------------------------------
        
        if(! group_has_role('commerce', 'view_affiliate_comission') AND $this->current_user->is_affiliate == 1){
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }

        $data['range'] = $this->input->get('date-range-picker');
        $data['range_start'] = NULL;
        $data['range_end'] = NULL;
        if(isset($data['range']) AND $data['range'] != '') {
            $range = explode("-", $data['range']);
            if(count($range)==2) {
                $data['range_start'] = trim($range[0]);
                $data['range_end'] = trim($range[1]);
            }
        }

        $filters = NULL;
        if(isset($data['range_start']) AND isset($data['range_start'])) {
            $filters['range']['range_start'] = date('Y-m-d',strtotime($data['range_start']));
            $filters['range']['range_end'] = date('Y-m-d',strtotime($data['range_end']));
        }

        $data['commission']['entries'] = $this->affiliate_m->get_commission($filters);

        foreach ($data['commission']['entries'] as $key => $commission_entry) {
            $commission_data[$key]['No'] = $key+1;
            $commission_data[$key]['Email'] = $commission_entry['email'];
            $commission_data[$key]['Nama Affiliate'] = ucwords($commission_entry['display_name']);
            $commission_data[$key]['No Rekening'] = (isset($commission_entry['nama_bank'])) ? ucwords($commission_entry['nama_bank']).' '.$commission_entry['no_rekening'].' a.n '.ucwords($commission_entry['atas_nama']) : 'Belum Memasukkan No Rekening';
            $commission_data[$key]['Jumlah Pesanan'] = (isset($commission_entry['jumlah_pesanan'])) ? $commission_entry['jumlah_pesanan'] : 0;
            $commission_data[$key]['Total Penjualan'] = number_format($commission_entry['total_penjualan'],0,'.',',');
            $commission_data[$key]['Komisi (25%)'] = number_format($commission_entry['komisi'],0,'.',',');
        }

        $this->load->helper('commerce');
        $this->load->helper('email/sendgrid');
        $download_time = (!isset($data['range']) AND $data['range'] != '') ? date('Ymd Hi') : $data['range'];
        $download_time = str_replace(" ", "-", $download_time);
        download_csv($commission_data, "data_komisi".$download_time);
    }

    public function statistics()
    {
        // -------------------------------------
        // Check permission
        // -------------------------------------
        
        if(! group_has_role('commerce', 'view_affiliate_statistics') AND ! group_has_role('commerce', 'view_own_statistics') AND $this->current_user->is_affiliate == 0){
        // if(! group_has_role('commerce', 'view_affiliate_statistics') AND ! group_has_role('commerce', 'view_own_statistics') ) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }

        $data['range'] = ($this->input->get('date-range-picker')) ? $this->input->get('date-range-picker') : date('m/d/Y',strtotime('7daysAgo')).' - '.date('m/d/Y',strtotime('today'));
        $data['range_start'] = NULL;
        $data['range_end'] = NULL;
        if(isset($data['range']) AND $data['range'] != '') {
            $range = explode("-", $data['range']);
            if(count($range)==2) {
                $data['range_start'] = trim($range[0]);
                $data['range_end'] = trim($range[1]);
            }
        }

        $filters = NULL;
        if(isset($data['range_start']) AND isset($data['range_start'])) {
            $filters['range']['range_start'] = date('Y-m-d',strtotime($data['range_start']));
            $filters['range']['range_end'] = date('Y-m-d',strtotime($data['range_end']));
        }

        if(group_has_role('commerce','view_own_statistics') OR $this->current_user->is_affiliate=='1') {
            $filters['id_affiliate'] = $this->current_user->id;
            $filters['group_by'] = ($this->input->get('group_by')) ? $this->input->get('group_by') : 'date';
        }

        $this->load->model('landing_page/konten_m');
        $data['landing_page'] = $this->konten_m->get_konten();

        $analytics_prefix = '~/';

        if($this->input->get('landing_page')) {
            $analytics_prefix = $this->input->get('landing_page');
            $landing_page = $this->konten_m->get_konten_by_uri($analytics_prefix);

            if($analytics_prefix=='/') {
                $analytics_prefix = '~/';
            } else {
                $analytics_prefix = '~/'.$analytics_prefix.'/';
            }

            $filters['id_landing_page'] = $landing_page['id'];
        }

        // chart
        $chart_data_result = $this->affiliate_m->get_chart_data($filters);

        // reformat data
        foreach ($chart_data_result as $key => $value) {
            if(!isset($filters['group_by']) OR $filters['group_by']=='date') {
                $chart_data[date('Y-m-d',strtotime($value['date']))] = $value;
            } elseif($filters['group_by']=='month') {
                $chart_data[date('Y-m',strtotime($value['date']))] = $value;
            }
        }

        // analytics for chart
        if(group_has_role('commerce','view_affiliate_statistics')) {
            $ga_data = $this->ga_api('ga:pageviews,ga:users','ga:date',NULL,'ga:pagePath='.$analytics_prefix);
        } else {
            $dimensions = 'ga:date';
            if(isset($filters['group_by'])) {
                if($filters['group_by']=='date') $dimensions = 'ga:date';
                elseif($filters['group_by']=='month') $dimensions = 'ga:month,ga:year';
                
            }
            $ga_data = $this->ga_api('ga:pageviews,ga:users',$dimensions,NULL,'ga:pagePath='.$analytics_prefix.'affiliate/'.$this->current_user->id);
        }

        // reformat ga data
        $analytics_data_rows = $ga_data->rows;
        foreach ($analytics_data_rows as $key => $value) {
            if(!isset($filters['group_by']) OR $filters['group_by']=='date') {
                $analytics_data[date('Y-m-d',strtotime($value[0]))][0] = date('Y-m-d',strtotime($value[0]));
                $analytics_data[date('Y-m-d',strtotime($value[0]))][1] = $value[1];
                $analytics_data[date('Y-m-d',strtotime($value[0]))][2] = $value[2];
            } elseif($filters['group_by']=='month') {
                $analytics_data[date('Y-m',strtotime($value[1].'-'.$value[0]))][0] = $value[1].'-'.$value[0];
                $analytics_data[date('Y-m',strtotime($value[1].'-'.$value[0]))][1] = $value[2];
                $analytics_data[date('Y-m',strtotime($value[1].'-'.$value[0]))][2] = $value[3];
            }
        }

        // form the data for chart
        if(group_has_role('commerce','view_affiliate_statistics')) {
            $startdate = date('Y-m-d',strtotime($data['range_start']));
            $enddate = date('Y-m-d',strtotime($data['range_end']));
            // dump($chart_data,$analytics_data);
            while ($startdate <= $enddate) {
                $graph_data[] = array(
                    'date' => $startdate,
                    'pageviews' => (isset($analytics_data[$startdate])) ? $analytics_data[$startdate][1] : 1,
                    'leads' => (isset($analytics_data[$startdate])) ? $analytics_data[$startdate][2] : 2,
                    'jumlah_semua_pesanan' => (isset($chart_data[$startdate])) ? $chart_data[$startdate]['all_order_quantity'] : 0,
                    'jumlah_pesanan' => (isset($chart_data[$startdate])) ? $chart_data[$startdate]['completed_order_quantity'] : 0,
                    'total_penjualan' => (isset($chart_data[$startdate])) ? $chart_data[$startdate]['total_penjualan'] : 0,
                    );
                $startdate = date('Y-m-d',strtotime($startdate.'+1 day'));
            }
        } elseif(group_has_role('commerce','view_own_statistics') OR $this->current_user->is_affiliate == 1) {
            if($filters['group_by']=='date') {
                $startdate = date('Y-m-d',strtotime($data['range_start']));
                $enddate = date('Y-m-d',strtotime($data['range_end']));

                while ($startdate <= $enddate) {
                    $graph_data[] = array(
                        'date' => $startdate,
                        'pageviews' => (isset($analytics_data[$startdate])) ? $analytics_data[$startdate][1] : 1,
                        'leads' => (isset($analytics_data[$startdate])) ? $analytics_data[$startdate][2] : 2,
                        'jumlah_semua_pesanan' => (isset($chart_data[$startdate])) ? $chart_data[$startdate]['all_order_quantity'] : 0,
                        'jumlah_pesanan' => (isset($chart_data[$startdate])) ? $chart_data[$startdate]['completed_order_quantity'] : 0,
                        'total_penjualan' => (isset($chart_data[$startdate])) ? $chart_data[$startdate]['total_penjualan'] : 0,
                        );
                    // dump($startdate,$chart_data,$analytics_data);
                    $startdate = date('Y-m-d',strtotime($startdate.'+1 day'));
                }

            }elseif($filters['group_by']=='month') {
                $startdate = date('Y-m',strtotime($data['range_start']));
                $enddate = date('Y-m',strtotime($data['range_end']));

                while ($startdate <= $enddate) {
                    $graph_data[] = array(
                        'date' => $startdate,
                        'pageviews' => (isset($analytics_data[$startdate])) ? $analytics_data[$startdate][1] : 1,
                        'leads' => (isset($analytics_data[$startdate])) ? $analytics_data[$startdate][2] : 2,
                        'jumlah_semua_pesanan' => (isset($chart_data[$startdate])) ? $chart_data[$startdate]['all_order_quantity'] : 0,
                        'jumlah_pesanan' => (isset($chart_data[$startdate])) ? $chart_data[$startdate]['completed_order_quantity'] : 0,
                        'total_penjualan' => (isset($chart_data[$startdate])) ? $chart_data[$startdate]['total_penjualan'] : 0,
                        );
                    // dump($startdate,$chart_data,$analytics_data);
                    $startdate = date('Y-m',strtotime($startdate.'+1 month'));
                }
            }
        }

        $data['graph_data'] = $graph_data;
        
        $commission_affiliate = array();
        if(group_has_role('commerce','view_affiliate_statistics')) {
            // affiliate
            $data['commission']['entries'] = $this->affiliate_m->get_statistics($filters);

            $analytics_data = $this->analytics($analytics_prefix.'affiliate/*');
            foreach ($data['commission']['entries'] as $key => $entry) {
                $commission_affiliate[$key] = $entry;
                // check permission
                if(group_has_role('commerce','view_affiliate_statistics')) {
                    // check key
                    if(array_key_exists($entry['user_id'], $analytics_data)) {
                        $commission_affiliate[$key] = array_merge($entry,$analytics_data[$entry['user_id']]);
                    }
                } elseif(group_has_role('commerce','view_own_statistics')) {
                    if($this->current_user->id != $entry['user_id']) {
                        unset($commission_affiliate[$key]);
                    }
                    if($this->current_user->id == $entry['user_id'] AND array_key_exists($entry['user_id'], $analytics_data)) {
                        $commission_affiliate[$key] = array_merge($entry,$analytics_data[$entry['user_id']]);
                    }
                    continue;
                }
            }
        } else {
            krsort($graph_data);
            $commission_affiliate = $graph_data;
        }

        // check permission
        if(group_has_role('commerce','view_affiliate_statistics')) {
            // non affiliate
            $commission_non_affiliate = $this->affiliate_m->get_statistics_non_affiliate($filters);
            if($analytics_prefix=='~/') {
                $analytics_prefix = '~^/$';
            }
            $analytics_data = $this->analytics($analytics_prefix);

            foreach ($commission_non_affiliate as $key => $entry) {
                if(isset($analytics_data[0])) {
                    $commission_non_affiliate[$key] = array_merge($entry,$analytics_data[0]);
                } else {
                    $commission_non_affiliate[$key] = $entry;
                }
            }


            $data['commission']['entries'] = array_merge($commission_non_affiliate,$commission_affiliate);
        } elseif(group_has_role('commerce','view_own_statistics') OR $this->current_user->is_affiliate == 1) {
            $data['commission']['entries'] = $commission_affiliate;
        }

        $data['commission']['total'] = count($data['commission']['entries']);

        $data['uri'] = get_query_string(5);

        // -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
        // -------------------------------------
        
        $this->template->title(lang('commerce:affiliate_statistics'))
            ->set_breadcrumb('Home', '/admin')
            ->set_breadcrumb(lang('cp:nav_Commerce'))
            ->set_breadcrumb(lang('commerce:affiliate_statistics'))
            ->build('admin/affiliate_statistics', $data);
    }

    public function download_statistics()
    {
        $data['range'] = ($this->input->get('date-range-picker')) ? $this->input->get('date-range-picker') : date('m/d/Y',strtotime('7daysAgo')).' - '.date('m/d/Y',strtotime('today'));
        $data['range_start'] = NULL;
        $data['range_end'] = NULL;
        if(isset($data['range']) AND $data['range'] != '') {
            $range = explode("-", $data['range']);
            if(count($range)==2) {
                $data['range_start'] = trim($range[0]);
                $data['range_end'] = trim($range[1]);
            }
        }

        $filters = NULL;
        if(isset($data['range_start']) AND isset($data['range_start'])) {
            $filters['range']['range_start'] = date('Y-m-d',strtotime($data['range_start']));
            $filters['range']['range_end'] = date('Y-m-d',strtotime($data['range_end']));
        }

        // affiliate
        $data['commission']['entries'] = $this->affiliate_m->get_statistics($filters);

        $analytics_data = $this->analytics($analytics_prefix.'affiliate/*');
        foreach ($data['commission']['entries'] as $key => $entry) {
            $commission_affiliate[$key] = $entry;
            // check permission
            if(group_has_role('commerce','view_affiliate_statistics')) {
                // check key
                if(array_key_exists($entry['user_id'], $analytics_data)) {
                    $commission_affiliate[$key] = array_merge($entry,$analytics_data[$entry['user_id']]);
                }
            } elseif(group_has_role('commerce','view_own_statistics') OR $this->current_user->is_affiliate == 1) {
                if($this->current_user->id == $entry['user_id'] AND array_key_exists($entry['user_id'], $analytics_data)) {
                    $commission_affiliate[$key] = array_merge($entry,$analytics_data[$entry['user_id']]);
                }
                continue;
            }
        }

        // check permission
        if(group_has_role('commerce','view_affiliate_statistics')) {
            // non affiliate
            $commission_non_affiliate = $this->affiliate_m->get_statistics_non_affiliate($filters);
            $analytics_data = $this->analytics('~^/$');
            foreach ($commission_non_affiliate as $key => $entry) {
                $commission_non_affiliate[$key] = array_merge($entry,$analytics_data[0]);
            }

            $data['commission']['entries'] = array_merge($commission_non_affiliate,$commission_affiliate);
        } elseif(group_has_role('commerce','view_own_statistics') OR $this->current_user->is_affiliate == 1) {
            $data['commission']['entries'] = $commission_affiliate;
        }

        $data['commission']['total'] = count($data['commission']['entries']);

        foreach ($data['commission']['entries'] as $key => $commission_entry) {
            $commission_data[$key]['No'] = $key+1;
            $commission_data[$key]['Nama Affiliate'] = (isset($commission_entry['display_name'])) ? ucwords($commission_entry['display_name']) : 'Tanpa Affiliate';
            $commission_data[$key]['Page Views'] = (isset($commission_entry['pageviews'])) ? number_format($commission_entry['pageviews'],0,'.',',') : 0;
            $commission_data[$key]['Leads'] = (isset($commission_entry['leads'])) ? number_format($commission_entry['leads'],0,'.',',') : 0;
            $commission_data[$key]['Jumlah Pesanan'] = (isset($commission_entry['jumlah_pesanan'])) ? $commission_entry['jumlah_pesanan'] : 0;
            $commission_data[$key]['Total Penjualan'] = number_format($commission_entry['total_penjualan'],0,'.',',');
        }

        $this->load->helper('commerce');
        $this->load->helper('email/sendgrid');
        $download_time = (!isset($data['range']) AND $data['range'] != '') ? date('Ymd Hi') : $data['range'];
        $download_time = str_replace(" ", "-", $download_time);
        download_csv($commission_data, "data_statistik".$download_time);
    }

    public function statistics_source()
    {
        // -------------------------------------
        // Check permission
        // -------------------------------------
        
        if(! group_has_role('commerce', 'view_affiliate_statistics') AND ! group_has_role('commerce', 'view_own_statistics') AND $this->current_user->is_affiliate == '0'){
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }

        $data['range'] = ($this->input->get('date-range-picker')) ? $this->input->get('date-range-picker') : date('m/d/Y',strtotime('7daysAgo')).' - '.date('m/d/Y',strtotime('today'));
        $data['range_start'] = NULL;
        $data['range_end'] = NULL;
        if(isset($data['range']) AND $data['range'] != '') {
            $range = explode("-", $data['range']);
            if(count($range)==2) {
                $data['range_start'] = trim($range[0]);
                $data['range_end'] = trim($range[1]);
            }
        }

        $this->load->model('landing_page/konten_m');
        $data['landing_page'] = $this->konten_m->get_konten();

        $analytics_prefix = '~/';

        if($this->input->get('landing_page')) {
            $analytics_prefix = $this->input->get('landing_page');
            $landing_page = $this->konten_m->get_konten_by_uri($analytics_prefix);

            if($analytics_prefix=='/') {
                $analytics_prefix = '~/';
            } else {
                $analytics_prefix = '~/'.$analytics_prefix.'/';
            }

            $filters['id_landing_page'] = $landing_page['id'];
        }

        if(group_has_role('commerce','view_affiliate_statistics')) {
            $ga_data = $this->ga_api('ga:sessions','ga:source','-ga:sessions','ga:pagePath='.$analytics_prefix);
        } elseif(group_has_role('commerce','view_own_statistics') OR $this->current_user->is_affiliate == '1') {
            $ga_data = $this->ga_api('ga:sessions','ga:source','-ga:sessions','ga:pagePath='.$analytics_prefix.'affiliate/'.$this->current_user->id);
        }

        $analytics_data= NULL;

        if(count($ga_data->rows)>0) {
            $analytics_data = $ga_data->rows;

            $sessions_total = 0;
            $percentage_total = 0;
            // calculate the total of session
            foreach ($analytics_data as $key => $value) {
                $sessions_total += $value[1];
            }

            // calculate percentage
            foreach ($analytics_data as $key => $value) {
                $percent = ($value[1]/$sessions_total) * 100;
                $analytics_data[$key][] = number_format($percent,2);
                $percentage_total += $percent;
            }
        }

        $data['analytics_data'] = $analytics_data;

        $data['uri'] = get_query_string(5);

        // -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
        // -------------------------------------
        
        $this->template->title(lang('commerce:affiliate_statistics'))
            ->set_breadcrumb('Home', '/admin')
            ->set_breadcrumb(lang('cp:nav_Commerce'))
            ->set_breadcrumb(lang('commerce:affiliate_statistics'))
            ->build('admin/affiliate_statistics_source', $data);
    }

    public function analytics($path)
    {
        $result = (Array)$this->ga($path);

        if(isset($result['error'])) {
            dump($result['error']['message']);
        }

        $row_data = $result['rows'];
        $analytics_result = array();
        
        if(isset($row_data)) {
            foreach ($row_data as $row) {
                $affiliate_id = substr($row['0'], 11);

                $analytics_result[$affiliate_id]['pagepath'] = $row['0'];
                $analytics_result[$affiliate_id]['leads'] = $row['1'];
                $analytics_result[$affiliate_id]['percentnewsessions'] = $row['2'];
                $analytics_result[$affiliate_id]['sessions'] = $row['3'];
                $analytics_result[$affiliate_id]['bouncerate'] = $row['4'];
                $analytics_result[$affiliate_id]['avgsessionduration'] = $row['5'];
                $analytics_result[$affiliate_id]['pageviews'] = $row['6'];
                $analytics_result[$affiliate_id]['pageviewspersession'] = $row['7'];
            }
        }

        return $analytics_result;
    }

    // --------------------------------------------------------------------------

    public function ga($path)
    {
        $range = explode("-", $this->input->get('date-range-picker'));
        $startdate = '7daysAgo';
        $enddate = 'today';
        if(count($range)==2) {
            $startdate = date('Y-m-d',strtotime($range[0]));
            $enddate = date('Y-m-d',strtotime($range[1]));
        }

        $this->load->library('google');
        
        $client_email = 'developer@sbdkk-141807.iam.gserviceaccount.com';
        $private_key = file_get_contents(base_url().$this->module_details['path'].'/libraries/SBDKK-6b266b8f8787.p12');
        $scopes = array('https://www.googleapis.com/auth/analytics','https://www.googleapis.com/auth/analytics.readonly');
        $credentials = new Google_Auth_AssertionCredentials(
            $client_email,
            $scopes,
            $private_key
            );

        $client = new Google_Client();
        $client->setAssertionCredentials($credentials);
        if ($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion();
        }
        $analytics = new Google_Service_Analytics($client);

        $profileId = '128653002';
        $result = $analytics->data_ga->get(
            'ga:' . $profileId,
            $startdate,
            $enddate,
            'ga:users,ga:percentNewSessions,ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviews,ga:pageviewsPerSession',
            array(
                'dimensions'=>'ga:pagePath',
                'sort'=>'ga:pagePath',
                'filters'=>'ga:pagePath='.$path)
            );
        return $result;
    }

    public function ga_api($metrics, $dimensions, $sort = NULL, $filters = NULL)
    {
        $range = explode("-", $this->input->get('date-range-picker'));
        $startdate = '7daysAgo';
        $enddate = 'today';
        if(count($range)==2) {
            $startdate = date('Y-m-d',strtotime($range[0]));
            $enddate = date('Y-m-d',strtotime($range[1]));
        }

        $this->load->library('google');
        
        $client_email = 'developer@sbdkk-141807.iam.gserviceaccount.com';
        $private_key = file_get_contents(base_url().$this->module_details['path'].'/libraries/SBDKK-6b266b8f8787.p12');
        $scopes = array('https://www.googleapis.com/auth/analytics','https://www.googleapis.com/auth/analytics.readonly');
        $credentials = new Google_Auth_AssertionCredentials(
            $client_email,
            $scopes,
            $private_key
            );

        $client = new Google_Client();
        $client->setAssertionCredentials($credentials);
        if ($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion();
        }
        $analytics = new Google_Service_Analytics($client);

        $profileId = '128653002';
        $result = $analytics->data_ga->get(
            'ga:' . $profileId,
            $startdate,
            $enddate,
            $metrics,
            array(
                'dimensions'=>$dimensions,
                'sort'=>$sort,
                'filters'=>$filters)
            );
        return $result;
    }
}