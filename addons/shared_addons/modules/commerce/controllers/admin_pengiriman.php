<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Commerce Module
 *
 * Modul untu me-manage simple commerce
 *
 */
class Admin_pengiriman extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'pengiriman';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'access_pengiriman_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('commerce');		
		$this->load->model('pengiriman_m');
    }

    /**
	 * List all pengiriman
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_pengiriman') AND ! group_has_role('commerce', 'view_own_pengiriman')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'admin/commerce/pengiriman/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->pengiriman_m->count_all_pengiriman();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['pengiriman']['entries'] = $this->pengiriman_m->get_pengiriman($pagination_config);
		$data['pengiriman']['total'] = $pagination_config['total_rows'];
		$data['pengiriman']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['pengiriman']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('admin/commerce/pengiriman/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/commerce/pengiriman/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('commerce:pengiriman:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('commerce:pengiriman:plural'))
			->build('admin/pengiriman_index', $data);
    }
	
	/**
     * Display one pengiriman
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_pengiriman') AND ! group_has_role('commerce', 'view_own_pengiriman')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['pengiriman'] = $this->pengiriman_m->get_pengiriman_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'view_all_pengiriman')){
			if($data['pengiriman']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('commerce:pengiriman:view'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_pengiriman') OR group_has_role('commerce', 'view_own_pengiriman')){
			$this->template->set_breadcrumb(lang('commerce:pengiriman:plural'), '/admin/commerce/pengiriman/index');
		}

		$this->template->set_breadcrumb(lang('commerce:pengiriman:view'))
			->build('admin/pengiriman_entry', $data);
    }
	
	/**
     * Create a new pengiriman entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'create_pengiriman')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_pengiriman('new')){	
				$this->session->set_flashdata('success', lang('commerce:pengiriman:submit_success'));				
				redirect('admin/commerce/pengiriman/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:pengiriman:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/commerce/pengiriman/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:pengiriman:new'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_pengiriman') OR group_has_role('commerce', 'view_own_pengiriman')){
			$this->template->set_breadcrumb(lang('commerce:pengiriman:plural'), '/admin/commerce/pengiriman/index');
		}

		$this->template->set_breadcrumb(lang('commerce:pengiriman:new'))
			->build('admin/pengiriman_form', $data);
    }
	
	/**
     * Edit a pengiriman entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the pengiriman to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'edit_all_pengiriman') AND ! group_has_role('commerce', 'edit_own_pengiriman')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('commerce', 'edit_all_pengiriman')){
			$entry = $this->pengiriman_m->get_pengiriman_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_pengiriman('edit', $id)){	
				$this->session->set_flashdata('success', lang('commerce:pengiriman:submit_success'));				
				redirect('admin/commerce/pengiriman/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('commerce:pengiriman:submit_failure');
			}
		}
		
		$data['fields'] = $this->pengiriman_m->get_pengiriman_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/commerce/pengiriman/index/'.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('commerce:pengiriman:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('commerce', 'view_all_pengiriman') OR group_has_role('commerce', 'view_own_pengiriman')){
			$this->template->set_breadcrumb(lang('commerce:pengiriman:plural'), '/admin/commerce/pengiriman/index');
		}else{
			$this->template->set_breadcrumb(lang('commerce:pengiriman:plural'))
			->set_breadcrumb(lang('commerce:pengiriman:view'));
		}

		$this->template->set_breadcrumb(lang('commerce:pengiriman:edit'))
			->build('admin/pengiriman_form', $data);
    }
	
	/**
     * Delete a pengiriman entry
     * 
     * @param   int [$id] The id of pengiriman to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('commerce', 'delete_all_pengiriman') AND ! group_has_role('commerce', 'delete_own_pengiriman')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('commerce', 'delete_all_pengiriman')){
			$entry = $this->pengiriman_m->get_pengiriman_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->pengiriman_m->delete_pengiriman_by_id($id);
        $this->session->set_flashdata('error', lang('commerce:pengiriman:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('admin/commerce/pengiriman/index'.$data['uri']);
    }
	
	/**
     * Insert or update pengiriman entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_pengiriman($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		if($this->input->post('is_active') == '') {
			$_POST['is_active'] = 0;
		}
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama_kurir', lang('commerce:nama_kurir'), 'required|max_length[100]');
		$this->form_validation->set_rules('kode_kurir', lang('commerce:kode_kurir'), 'required|max_length[50]');
		$this->form_validation->set_rules('is_active', lang('commerce:is_active'), 'required|max_length[100]');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->pengiriman_m->insert_pengiriman($values);
				
			}
			else
			{
				$result = $this->pengiriman_m->update_pengiriman($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}