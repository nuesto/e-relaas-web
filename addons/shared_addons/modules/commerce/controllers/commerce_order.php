<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Commerce Module
 *
 * Modul untu me-manage simple commerce
 *
 */
class Commerce_order extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------

    protected $section = 'order';

    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('commerce');

		$this->load->model('order_m');
		$this->load->model('produk_m');
		$this->load->model('cart_m');
		$this->load->model('rekening_m');
		$this->load->model('pengiriman_m');
		$this->load->model('payment_m');
		$this->load->model('voucher_m');
		$this->load->model('landing_page/konten_m');

		$this->load->model('location/provinsi_m');
		$this->load->model(array('location/kota_m','dompet/saldo_m','dompet/mutasi_m'));

		$this->load->helper('commerce');
		$this->load->helper('email/sendgrid');

		$this->load->library('variables/variables');

		$this->config->load('config');

		date_default_timezone_set("Asia/Jakarta");
    }

    /**
	 * List all order
     *
     * @return	void
     */
    public function index()
    {
    	redirect('commerce/order/my_order');
		// -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'view_all_order') AND ! group_has_role('commerce', 'view_own_order')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			$this->session->set_userdata('redirect_to', $this->uri->uri_string());
			redirect('login');
		}

		$params = array(
			'id_customer'=>$this->current_user->id,
			'no_invoice !='=>'',
			);

        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'commerce/order/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->order_m->count_all_order($params,1);
		$pagination_config['per_page'] = 5;
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;

        // -------------------------------------
		// Get entries
		// -------------------------------------


        $data['order']['entries'] = $this->order_m->get_order($pagination_config, $params,1);
		$data['order']['total'] = $pagination_config['total_rows'];
		$data['order']['pagination'] = $this->pagination->create_links();

		$data['uri'] = get_query_string(4);

		// -------------------------------------
		// Build the page.
		// -------------------------------------

		if(count($data['order']['entries'])==0 AND ($this->uri->segment(4) != NULL AND $this->uri->segment(4)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(4), '', $data['uri']);
			if($this->uri->segment(4) != '' AND $this->uri->segment(4)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(4)-$pagination_config['per_page'];
				redirect('commerce/order/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('commerce/order/index'.$data['uri']);
				}
			}
		}

		$this->template->title(lang('commerce:order:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('commerce:order:plural'))
			->build('order_index', $data);
    }

	/**
     * Display one order
     *
     * @return  void
     */
    public function view($id = 0)
    {
    	if(!is_logged_in()) {
    		redirect('login?login_required=1');
    	}

        // -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'view_all_order') AND ! group_has_role('commerce', 'view_own_order')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}

		// -------------------------------------
		// Get our entry.
		// -------------------------------------

        $data['order'] = $this->order_m->get_order_by_id($id);

		// Check view all/own permission
		if(! group_has_role('commerce', 'view_all_order')){
			if($data['order']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		$data['uri'] = get_query_string(4);

		// -------------------------------------
		// Build the page.
		// -------------------------------------

        $this->template->title(lang('commerce:order:view'))
			->set_breadcrumb('Home', '/home');

		if(group_has_role('commerce', 'view_all_order') OR group_has_role('commerce', 'view_own_order')){
			$this->template->set_breadcrumb(lang('commerce:order:plural'), '/commerce/order/index');
		}

		$this->template->set_breadcrumb(lang('commerce:order:view'))
			->build('order_entry', $data);
    }

	/**
     * Create a new order entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
    	$this->input->get('product') or show_404();
    	redirect('commerce/cart/add?product='.$this->input->get('product'));
    	// -------------------------------------
		// Process POST input
		// -------------------------------------

		if($_POST){
			if($this->_update_order('new')){
				$this->session->set_flashdata('success', lang('commerce:order:submit_success'));
				redirect('commerce/order/index');
			}else{
				$data['messages']['error'] = lang('commerce:order:submit_failure');
			}
		}

		$data['mode'] = 'new';
		$data['uri'] = get_query_string(4);
		$data['return'] = 'commerce/order/index'.$data['uri'];

		// Get products
		$ids = explode(",", $this->input->get('product'));
		$data['products'] = $this->produk_m->get_produk_by_ids($ids);

		// Get order
		if(isset($this->current_user->id)) {
			$params_order = array('id_customer'=>$this->current_user->id,'status_pesanan'=>'in-cart');
			$order = $this->order_m->get_order_by_many($params_order);
			$order_product = $this->order_m->get_order_product($order['id']);
			$data['order'] = $order;
			foreach ($order_product as $o) {
				$data['order']['item'][$o['id_produk']] = $o;
			}
		} else {
			$data['order'] = NULL;
		}

		// Get cities
		$data['provinsi'] = $this->provinsi_m->get_provinsi();

		// save current session
		$data_order = array(
			'id_session' => $this->session->userdata('session_id'),
			'status_pesanan' => 'in-cart',
			);

		$id_order = $this->order_m->insert_order($data_order);

		// -------------------------------------
		// Build the form page.
		// -------------------------------------

        $this->template->title(lang('commerce:order:view'))
			->set_breadcrumb('Home', '/home');

		if(group_has_role('commerce', 'view_all_order') OR group_has_role('commerce', 'view_own_order')){
			$this->template->set_breadcrumb(lang('commerce:order:plural'), '/commerce/order/index');
		}

		$this->template->set_breadcrumb(lang('commerce:order:new'))
			->build('order_form', $data);
    }

	/**
     * Edit a order entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the order to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'edit_all_order') AND ! group_has_role('commerce', 'edit_own_order')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('commerce', 'edit_all_order')){
			$entry = $this->order_m->get_order_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		// -------------------------------------
		// Process POST input
		// -------------------------------------

		if($_POST){
			if($this->_update_order('edit', $id)){
				$this->session->set_flashdata('success', lang('commerce:order:submit_success'));
				redirect('commerce/order/index');
			}else{
				$data['messages']['error'] = lang('commerce:order:submit_failure');
			}
		}

		$data['fields'] = $this->order_m->get_order_by_id($id);
		$data['mode'] = 'edit';
		$data['uri'] = get_query_string(4);
		$data['return'] = 'commerce/order/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;

		// -------------------------------------
		// Build the form page.
		// -------------------------------------
       $this->template->title(lang('commerce:order:edit'))
			->set_breadcrumb('Home', '/home');

		if(group_has_role('commerce', 'view_all_order') OR group_has_role('commerce', 'view_own_order')){
			$this->template->set_breadcrumb(lang('commerce:order:plural'), '/commerce/order/index')
			->set_breadcrumb(lang('commerce:order:view'), '/commerce/order/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('commerce:order:plural'))
			->set_breadcrumb(lang('commerce:order:view'));
		}

		$this->template->set_breadcrumb(lang('commerce:order:edit'))
			->build('order_form', $data);
    }

	/**
     * Delete a order entry
     *
     * @param   int [$id] The id of order to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'delete_all_order') AND ! group_has_role('commerce', 'delete_own_order')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('commerce', 'delete_all_order')){
			$entry = $this->order_m->get_order_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		// -------------------------------------
		// Delete entry
		// -------------------------------------

        $this->order_m->delete_order_by_id($id);
		$this->session->set_flashdata('error', lang('commerce:order:deleted'));

		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(4);
        redirect('commerce/order/index'.$data['uri']);
    }

    /**
     * Cancel a order entry
     *
     * @param   none
     * @return  void
     */
    public function cancel($row_id)
    {
    	if(!is_logged_in()) {
    		redirect('login?login_required=1');
    	}
		// -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'confirm_order')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}

		// -------------------------------------
		// Delete entry
		// -------------------------------------
		$order = $this->order_m->get_order_by_id($row_id);
		if(in_array($order['status_pesanan'], array('completed','expired','cancelled'))) {
			if($this->input->get('return')) {
				redirect($this->input->get('return'));
			}
			redirect('commerce/order/my_order');
		}

		$values['status_pesanan'] = 'cancelled';
		$result = $this->order_m->update_order($values, array('id' => $row_id));

		$this->session->set_flashdata('error', lang('commerce:order:canceled'));

		// -------------------------------------
		// Redirect
		// -------------------------------------
		if($this->input->get('return')) {
			redirect($this->input->get('return'));
		}
		$data['uri'] = get_query_string(4);
        redirect('commerce/order/my_order');
    }

    /**
     * Order is already arrived
     *
     * @param   none
     * @return  void
     */
    public function arrived()
    {
    	if(!is_logged_in()) {
    		redirect('login?login_required=1');
    	}
		// -------------------------------------
		// Check permission
		// -------------------------------------

		if(! group_has_role('commerce', 'confirm_order')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}

		// -------------------------------------
		// Delete entry
		// -------------------------------------
		$no_invoice = $this->input->get('no_invoice');

		$values['status_pesanan'] = 'produk telah diterima';
		$order = $this->order_m->get_order_by_many(array('no_invoice'=>strtoupper($no_invoice)));
		$row_id = $order['id'];
		$result = $this->order_m->update_order($values, array('id' => $row_id));

		$this->session->set_flashdata('error', lang('commerce:order:updated'));

		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(4);
        redirect('commerce/order/my_order'.$data['uri']);
    }

	/**
     * Insert or update order entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_order($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->load->helper(array('form', 'url'));

 		// -------------------------------------
		// Set Values
		// -------------------------------------
		// remove [dot] from jumlah pembayaran
		$_POST['jumlah_pembayaran'] = str_replace(".", "", $this->input->post('jumlah_pembayaran'));
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------

		// Set validation rules
		if($method == 'confirm') {
			$this->form_validation->set_rules('id_order', 'ID Order', 'trim|required|callback_check_order');
			$this->form_validation->set_rules('no_invoice', lang('commerce:no_invoice'), 'trim|required|max_length[20]|callback_check_expired');
			$this->form_validation->set_rules('bank_rekening_pengirim', lang('commerce:bank_rekening_pengirim'), 'trim|required|alpha_space|max_length[20]');
			$this->form_validation->set_rules('nama_rekening_pengirim', lang('commerce:nama_rekening_pengirim'), 'trim|required|max_length[30]');
			$this->form_validation->set_rules('no_rekening_pengirim', lang('commerce:no_rekening_pengirim'), 'trim|required|numeric|max_length[20]');
			$this->form_validation->set_rules('bank_rekening_tujuan', lang('commerce:bank_rekening_tujuan'), 'trim|required');
			$this->form_validation->set_rules('jumlah_pembayaran', lang('commerce:jumlah_pembayaran'), 'trim|required|numeric|max_length[10]');
			$this->form_validation->set_rules('tanggal_transfer', lang('commerce:tanggal_transfer'), 'trim|required|callback_check_date_format|callback_check_max_date['.date('Y-m-d').']');
			if(isset($_FILES['bukti_pembayaran'])) {
				$this->form_validation->set_rules('bukti_pembayaran',lang('commerce:bukti_pembayaran'),'callback_check_image');
			}
		}elseif($method == "confirm_dompet"){
			$this->form_validation->set_rules('no_invoice', lang('commerce:no_invoice'), 'trim|required|max_length[20]|callback_check_expired');
			$this->form_validation->set_rules('id_dompet', lang('commerce:no_invoice'), 'trim|required|max_length[20]');
			$this->form_validation->set_rules('total_tagihan', lang('commerce:jumlah_pembayaran'), 'trim|required|max_length[10]');
		}

		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');

		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = true;
			}
			elseif ($method == 'edit')
			{
				$result = $this->order_m->update_order($values, $row_id);
			}
			elseif ($method == 'confirm')
			{
				$order = $this->order_m->get_order_by_many(array('default_commerce_order.id'=>strtoupper($values['id_order'])));
				$id_order = $order['id'];

				$payment = $this->payment_m->get_payment_by_order($id_order);
				$exist_history = (isset($payment['history'])) ? json_decode($payment['history'],true) : array();
				$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>'pending','updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'));
				$state_history = array_merge($exist_history,$new_history);

				$values['status'] = 'pending';
				$values['paid_on'] = date('Y-m-d H:i');
				$values['id_bukti'] = (isset($_POST['id_bukti'])) ? $_POST['id_bukti'] : NULL;
				$values['amazon_filename'] = (isset($_POST['amazon_filename'])) ? $_POST['amazon_filename'] : NULL;
				$values['transfer_confirmed_by'] = $this->current_user->id;
				$values['transfer_confirmed_on'] = date('Y-m-d H:i:s');
				$values['history'] = json_encode($state_history);
				unset($values['total_tagihan']);
				unset($values['order']);
				unset($values['no_invoice']);
				if($payment) {
					$result = $this->payment_m->update_payment($values, $row_id);
				} else {
					$values['payment_method'] = 'transfer';
					$values['billed_on'] = $order['created_on'];

					$result = $this->payment_m->insert_payment($values);
				}

				$values = NULL;
				$values['status_pesanan'] = 'waiting-payment';
				$result = $this->order_m->update_order($values, array('id' => $id_order));
				if($result) {
					$this->notif_confirm($id_order);
				}
			}
			elseif ($method == 'confirm_dompet')
			{
				$payment = $this->payment_m->get_payment_by_order($values['id']);
				$exist_history = (isset($payment['history'])) ? json_decode($payment['history'],true) : array();
				$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>'pending','updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'));
				$state_history = array_merge($exist_history,$new_history);

				$values['status'] = 'paid';
				$values['paid_on'] = date('Y-m-d H:i');
				$values['history'] = json_encode($state_history);
				$values['jumlah_pembayaran'] = str_replace(",", "", $values['total_tagihan']);
				$values['is_otp_verified'] = 1;
				$values['otp_verified_on'] = date('Y-m-d H:i:s');
				$values['otp_verified_by'] = $this->current_user->id;
				unset($values['total_tagihan']);
				unset($values['order']);
				$no_invoice = $values['no_invoice'];
				unset($values['no_invoice']);
				unset($values['id_order']);
				$id_payment = $values['id'];

				$result = $this->payment_m->update_payment_by_id_payment($values, $id_payment);
				$dompet_user = $this->saldo_m->get_saldo_dompet($this->current_user->id);
				$new_dompet = array("saldo" => $dompet_user['saldo'] - $values['jumlah_pembayaran']);
				$this->saldo_m->update_saldo($new_dompet, $dompet_user['id']);

				$new_mutasi = array("tanggal" => date("Y-m-d H:i:s"),"tipe" => "penarikan", "created_on" => date("Y-m-d H:i:s"),"created_by" => $this->current_user->id ,"nominal_kredit" => $values['jumlah_pembayaran'], "id_dompet" => $dompet_user['id'], "keterangan" => "Pembayaran No. Invoice : ".$no_invoice);
				$this->mutasi_m->insert_mutasi($new_mutasi);

				$values = NULL;
				$values['status_pesanan'] = 'completed';
				$values['closed_on'] = date('Y-m-d H:i');
				$result = $this->order_m->update_order($values, array('id' => $payment['id_order']));
				if($result) {
					$this->notif_confirm($payment['id_order']);
				}
			}
		} else {
			if($method=='confirm') {
				if(isset($_POST['amazon_filename'])) {
					$this->delete_file($_POST['amazon_filename']);
				}
			}

			$log_data = array(
				'time' => date('Y-m-d H:i:s'),
				'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
				'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
				'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
				'function' => '_update_order',
				'url' => base_url(uri_string()),
				'get_data' => json_encode($_GET),
				'post_data' => json_encode($_POST),
				'id_order' => (isset($values['id_order'])) ? $values['id_order'] : NULL,
				'payment_method' => 'transfer',
				'messages' => validation_errors()
				);
			log_message('error',json_encode($log_data));
		}

		return $result;
	}

	// --------------------------------------------------------------------------

	public function check_expired($no_invoice)
	{
		$order = $this->order_m->get_order_by_many(array('no_invoice'=>strtoupper($no_invoice)));
		if($order['status_pesanan']=='expired') {
			$this->form_validation->set_message('check_expired','<strong>Pesanan Sudah Kadaluarsa</strong>');
			return false;
		}
		return true;
	}

	public function check_order($id_order)
	{
		$order = $this->order_m->get_order_by_many(array('default_commerce_order.id'=>$id_order));

		// check if order exist
		if($order === null) {
			$this->form_validation->set_message('check_order','<strong>Pesanan tidak ditemukan</strong>');
			return false;
		}

		// check order submitted value and order from db
		if($id_order != $order['id']) {
			$this->form_validation->set_message('check_order','<strong>Pesanan memiliki id order yang berbeda</strong>');
			return false;
		}

		// check order customer and current user
		if($this->current_user->id != $order['id_customer']) {
			$this->form_validation->set_message('check_order','<strong>Anda tidak memiliki pesanan ini</strong>');
			return false;
		}

		// check no_invoice
		if($this->input->post('no_invoice') != $order['no_invoice']) {
			$this->form_validation->set_message('check_order','<strong>No invoice yang di masukkan tidak sesuai dengan pesanan</strong>');
			return false;
		}

		// check payment method
		if($order['payment_method'] != 'transfer') {
			$this->form_validation->set_message('check_order','<strong>Konfirmasi hanya digunakan untuk metode pembayaran transfer</strong>');
			return false;
		}

		if($order['payment_method']=='transfer' AND ($order['status_pembayaran']=='pending' OR $order['status_pembayaran']=='paid')) {
			$this->form_validation->set_message('check_order','<strong>Pesanan sudah pernah dikonfirmasi oleh pembeli</strong>');
			return false;
		}

		// check status pembayaran
		if($order['payment_method']=='transfer' AND $order['status_pesanan']=='cancelled' AND $order['status_pembayaran']!='declined') {
			$this->form_validation->set_message('check_order','<strong>Pesanan sudah di batalkan oleh pembeli</strong>');
			return false;
		}

		if($order['payment_method']=='transfer' AND $order['status_pembayaran']=='declined') {
			$this->form_validation->set_message('check_order','<strong>Pembayaran dari pesanan ini telah ditolak oleh admin</strong>');
			return false;
		}

		if($order['payment_method']=='transfer' AND $order['status_pembayaran']!='unpaid') {
			$this->form_validation->set_message('check_order','<strong>Pesanan dalam status pembayaran yang tidak memungkinkan untuk dilakukan konfirmasi</strong>');
			return false;
		}

		return true;
	}

	public function confirm() {
		// -------------------------------------
		// Process POST input
		// -------------------------------------

		if($_POST){
			$payment = $this->payment_m->get_payment_by_order($this->input->post('order'));
			if(!isset($payment)) {
				if($this->_update_order('confirm',$this->input->post('id_order'))){
					$this->session->set_flashdata('success', lang('commerce:order:submit_success'));
					$results = array('status' => 'success','message' => '<div class="alert alert-success">Konfirmasi Berhasil</div>');
					echo json_encode($results);
					die();

					redirect('commerce/order/my_order');
				}else{
					$data['messages']['error'] = lang('commerce:order:submit_failure');

					if($this->input->is_ajax_request()) {
						$results = array('status' => 'failed','message' => '<div class="alert alert-danger">'.validation_errors().'</div>');
						echo json_encode($results);
						die();
					}
				}
			} else {
				$data['messages']['error'] = 'Anda sudah pernah melakukan konfirmasi pembayaran.';

				if($this->input->is_ajax_request()) {
					$results = array('status' => 'failed','message' => '<div class="alert alert-danger">'.$data['messages']['error'].'</div>');
					echo json_encode($results);
					die();
				}
			}
		}

		$data['mode'] = 'confirm';
		$data['uri'] = get_query_string(4);
		$data['return'] = 'commerce/order/my_order'.$data['uri'];

		// Get rekening
		$data['rekening'] = $this->rekening_m->get_rekening(NULL,array('rekening_utama'=>1));

		// -------------------------------------
		// Build the form page.
		// -------------------------------------

		$this->template->title(lang('commerce:konfirmasi_pembayaran'))
			->build('konfirmasi_pembayaran', $data);
	}

	public function confirm_dompet() {
		// -------------------------------------
		// Process POST input
		// -------------------------------------

		if($_POST){
			$payment = $this->payment_m->get_payment_by_id_payment($this->input->post('id'));
			if(isset($payment)) {
				if($this->_update_order('confirm_dompet',$this->input->post('id'))){
					$this->session->set_flashdata('success', lang('commerce:order:submit_success'));
					$results = array('status' => 'success','message' => '<div class="alert alert-success">Konfirmasi Berhasil</div>');
				}else{
					$data['messages']['error'] = lang('commerce:order:submit_failure');

					if($this->input->is_ajax_request()) {
						$results = array('status' => 'failed','message' => '<div class="alert alert-danger">'.validation_errors().'</div>');
					}
				}
			}
		}

		redirect('commerce/order/my_order');
	}


	public function get_order_by_invoice() {
		$no_invoice = $this->input->get('no_invoice');
		$order = $this->order_m->get_order_by_many(array('no_invoice'=>strtoupper($no_invoice),'id_customer'=>$this->current_user->id));
		$total_tagihan = $order['biaya_order'] + $order['biaya_pengiriman'] + $order['kode_unik'];
		$order['total_tagihan'] = number_format($total_tagihan,'0','.',',');
		exit(json_encode($order));
	}

	public function get_order_by_id() {
		$id_order = $this->input->get('id_order');
		$id_customer = NULL;
		if(isset($this->current_user->id)) {
			$id_customer = $this->current_user->id;
		}
		$order = $this->order_m->get_order_by_many(array('default_commerce_order.id'=>$id_order,'id_customer'=>$id_customer));
		if(!$order) {
			$log_data = array(
				'time' => date('Y-m-d H:i:s'),
				'user_id' => $id_customer,
				'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
				'function' => 'get_order_by_id',
				'url' => base_url(uri_string()),
				'get_data' => json_encode($_GET),
				'post_data' => json_encode($_POST),
				'id_order' => $id_order,
				'messages' => 'order tidak ditemukan'
				);
			log_message('error',json_encode($log_data));
		}
		$sub_total = $order['biaya_order']-$order['discount_nominal'];
		$sub_total = ($sub_total<0) ? 0 : $sub_total;
		$total_tagihan = $sub_total + $order['biaya_pengiriman'] + $order['kode_unik'];
		$order['total_tagihan'] = number_format($total_tagihan,'0','.',',');
		$biaya_order = (isset($order['biaya_order'])) ? $order['biaya_order'] : 0;
		$order['biaya_order'] = number_format($biaya_order,'0','.',',');
		$discount_nominal = (isset($order['discount_nominal'])) ? $order['discount_nominal'] : 0;
		$order['discount_nominal'] = number_format($discount_nominal,'0','.',',');
		exit(json_encode($order));
	}

	function cek_ongkir() {
		$courier = $this->input->get('courier');
		$destination = $this->input->get('destination');
		$weight = $this->input->get('weight');
		if($destination == null) {
			$user = (array)$this->ion_auth->profile();
			$destination = $user['id_kota'];
		}

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "origin=501&destination=".$destination."&weight=".$weight."&courier=".$courier,
			CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: 8e80f80147aee4cd4eae9cee8d548818"
				),
			));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			// $result = json_decode($response,true);
			exit($response);
		}
	}

	function rajaongkir_cities() {
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"content-type: application/x-www-form-urlencoded",
				"key: 8e80f80147aee4cd4eae9cee8d548818"
				),
			));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {


			$result = json_decode($response,true);
			$cities = $result['rajaongkir']['results'];
			foreach ($cities as $city) {
				$this->db->where('id',$city['province_id']);
				$res = $this->db->get('location_provinsi')->result_array();

				if(count($res)==0) {
					$provinsi = array(
						'id' => $city['province_id'],
						'kode' => $city['province_id'],
						'nama' => $city['province'],
						'slug' => $this->create_slug($city['province']),
						);
					$this->provinsi_m->insert_provinsi($provinsi);
				}

				$city = array(
						'id' => $city['city_id'],
						'id_provinsi' => $city['province_id'],
						'kode' => $city['city_id'],
						'nama' => $city['type'].' '.$city['city_name'],
						'slug' => $this->create_slug($city['type'].' '.$city['city_name']),
						);
					$this->kota_m->insert_kota($city);
			}
		}
	}

	function create_slug($string){
		$slug=preg_replace('/[^A-Za-z0-9-]+/', '-', strtolower($string));
		return $slug;
	}

	public function validate_form($step) {
		$config = array(
			'step1' => array(),
			'step2' => array(
				array(
					'field' => 'kota-pengiriman',
					'label' => 'Alamat Pengiriman Kota',
					'rules' => 'required',
				),
				array(
					'field' => 'kode-pos-pengiriman',
					'label' => 'Alamat Pengiriman Kode Pos',
					'rules' => 'numeric|max_length[7]',
				),
				array(
					'field' => 'alamat-pengiriman',
					'label' => 'Alamat Pengiriman Alamat Lengkap',
					'rules' => 'required|max_length[1000]',
				),
				array(
					'field' => 'nama-penerima',
					'label' => 'Nama Penerima',
					'rules' => 'required|max_length[100]',
				),
				array(
					'field' => 'nohp-penerima',
					'label' => 'No HP Penerima',
					'rules' => 'required|numeric|max_length[13]',
				),
				array(
					'field' => 'courier',
					'label' => 'Jasa Pengiriman Kurir',
					'rules' => 'required',
				),
				array(
					'field' => 'pengiriman',
					'label' => 'Jasa Pengiriman Paket',
					'rules' => 'required',
				),
			)
		);

		if($step == 1) {
			foreach ($this->input->post('id_produk') as $key => $product) {
				$config['step1'][$key] = array(
					'field' => 'id_produk['.$key.']',
					'label' => 'Produk',
					'rules' => 'required',
				);
				$config['step1'][$key] = array(
					'field' => 'quantity['.$key.']',
					'label' => 'Jumlah Produk '.($key+1),
					'rules' => 'required|numeric|greater_than_equal_to[1]',
				);
			}
		}

		$this->form_validation->set_rules($config['step'.$step]);

		if($this->form_validation->run()) {
			$user = ($this->current_user) ? $this->current_user : NULL;

			$data_order = array(
				'id_session' => $this->session->userdata('session_id'),
				'status_pesanan' => 'in-cart',
				'kurir_pengiriman' => $this->input->post('courier'),
				'paket_pengiriman' => $this->input->post('pengiriman'),
				'nama_penerima' => $this->input->post('nama-penerima'),
				'no_hp_penerima' => $this->input->post('nohp-penerima'),
				'id_kota_pengiriman' => $this->input->post('kota-pengiriman'),
				'berat_pengiriman' => $this->input->post('berat'),
				'tanggal_order' => date('Y-m-d H:i:s'),
				'biaya_order' => ($this->input->post('biaya') != '') ? $this->input->post('biaya') : 0,
				'biaya_pengiriman' => $this->input->post('biaya-kirim'),
				'alamat_pengiriman' => $this->input->post('alamat-pengiriman'),
				'id_affiliate' => $this->session->userdata('id_affiliate')
			);

			if($step==2) {
				$unique_number = $this->order_m->generate_unique();
				$data_order['no_invoice'] = $unique_number['no_invoice'];
				$data_order['kode_unik'] = $unique_number['kode_unik'];
			}

			$data_cart = NULL;
			if($this->input->post('id_produk')) {
				foreach ($this->input->post('id_produk') as $key => $product) {
					$data_cart[$key] = array(
						'id_produk' => $this->input->post('id_produk')[$key],
						'harga' => $this->input->post('harga')[$key],
						'jumlah' => $this->input->post('quantity')[$key]
						);
				}
			}

			$id_order = $this->order_m->insert_order($data_order,$data_cart);

			$result = array('status' => 'success');

		} else {
			$result = array('status' => 'failed','message' => validation_errors());
		}
		die(json_encode($result));
	}

	public function load_step($step) {
		// Get products
		$data['products'] = $this->produk_m->get_produk();

		// Get cities
		$data['provinsi'] = $this->provinsi_m->get_provinsi();

		// Get rekening
		$data['rekening'] = $this->rekening_m->get_rekening(NULL,array('rekening_utama'=>1));

		// Get order
		if(isset($this->current_user->id)) {
			$params_order = array('id_customer'=>$this->current_user->id,'id_session'=>$this->session->userdata('session_id'));
		} else {
			$params_order = array('id_session'=>$this->session->userdata('session_id'));
		}
		$data['order'] = $this->order_m->get_order_by_many($params_order);

		// Get paket
		$data['pengiriman'] = $this->pengiriman_m->get_pengiriman(NULL, array('is_active'=>1));

		// if load step 3 means that the order is finished and wait for transaction
		if($step==4) {
			$this->order_m->update_order(array('status_pesanan'=> 'waiting-payment'), array(
				'id_session'=>$this->session->userdata('session_id'),
				'status_pesanan'=>'in-cart'));
			$this->session->unset_userdata($this->session->userdata('session_id'));

			$data['order_product'] = $this->order_m->get_order_product($data['order']['id']);
        	$data['order']['kota'] = $this->kota_m->get_kota_by_id($data['order']['id_kota_pengiriman']);

        	$tagihan_pembayaran = array(
        		'subject' => '[INVOICE #'.$data['order']['no_invoice'].'] Tagihan dan Petunjuk Pembayaran',
        		'body' => $this->load->view('email_template/tagihan_pembayaran',$data,true)
        		);

        	send_email_api($this->current_user->email,$tagihan_pembayaran);
		}

		$this->template->set_layout(false)
			->build('order_form_step_'.($step), $data);
	}

	public function already_login() {
		if($this->ion_auth->logged_in()) {
			$is_login = array('status' => 'success','message' => (array)$this->ion_auth->profile());

			// $this->order_m->update_order(array('id_customer'=> $this->current_user->id), array('id_session'=>$this->session->userdata('session_id')));
		} else {
			$is_login = array('status' => 'failed');
		}
		die(json_encode($is_login));
	}

	public function check_image($image)
	{
		// $this->load->library('files/files');
		// $id_folder = Files::get_id_by_name('Public');
		// $image_id = null;
		// if($_FILES['bukti_pembayaran']['name']!=''){
		// 	$allowed_type = array('bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'png', 'tiff', 'tif', 'BMP', 'GIF', 'JPEG', 'JPG', 'PNG');
		// 	$data = Files::upload($id_folder, $_FILES['bukti_pembayaran']['name'], 'bukti_pembayaran',false,false,false,implode("|", $allowed_type));

		// 	if($data['status']===false) {
		// 		$this->form_validation->set_message('check_image','<strong>'.lang('commerce:bukti_pembayaran').'</strong> '.strip_tags($data['message']));
		// 		return false;
		// 	} else {
		// 		$image_id = $data['data']['id'];
		// 		$_POST['id_bukti'] = $image_id;
		// 		return true;
		// 	}
		// }
		// $_POST['id_bukti'] = NULL;
		// return true;

		$upload_status = true;

		$name = $_FILES['bukti_pembayaran']['name'];
		$size = $_FILES['bukti_pembayaran']['size'];
		$tmp = $_FILES['bukti_pembayaran']['tmp_name'];
		$ext = $this->getExtension($name);

		if(strlen($name) > 0)
		{

			$this->load->library('files/awslib');
			// Use the us-west-2 region and latest version of each client.
			$sharedConfig = [
			'region'  => 'ap-southeast-1',
			'version' => 'latest',
			'credentials' => [
			'key'    => 'AKIAJT3OY23NBEBGSNUQ',
			'secret' => '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'
			]
			];

			// Create an SDK class used to share configuration across clients.
			$sdk = new Aws\Sdk($sharedConfig);
			// Use an Aws\Sdk class to create the S3Client object.
			$s3Client = $sdk->createS3();

			$pyrobase_bucket = 'nuesto-pyrobase';

			//Here you can add valid file extensions.
			$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");

			// File format validation
			if(in_array($ext,$valid_formats))
			{
				// File size validation
				if($size<(2048*1024))
				{
					//Rename image name.
					$actual_image_name = time().".".$ext;
					$i = 1;
					$name = substr($name, 0, -4);
					$filename = substr($name, 0, 18).$i;
					$base_filename = $name;

					while($s3Client->doesObjectExist($pyrobase_bucket,'bukti_transfer/'.$filename.'.'.$ext)) {
						$filename = substr($base_filename, 0, 18).$i;

						++$i;
					}

					$key = 'bukti_transfer/'.$filename.'.'.$ext;
					if($s3Client->putObject(array('Bucket'=>$pyrobase_bucket,'Key'=>$key,'SourceFile'=>$tmp,'ACL'=>'public-read')) )
					{
						$image_id = $filename.'.'.$ext;
						$msg = "Upload berhasil";
						$upload_status = true;
					}
					else {
						$msg = "S3 Upload Fail.";
						$upload_status = false;
					}

				}
				else {
					$msg = "Ukuran gambar maksimum 2 MB";
					$upload_status = false;
				}

			}
			else {
				$msg = "Tipe file tidak sesuai, hanya menerima file gambar.";
				$upload_status = false;
			}

		}

		if($upload_status===false) {
			$this->form_validation->set_message('check_image','<strong>'.lang('commerce:bukti_pembayaran').'</strong> '.strip_tags($msg));
			return false;
		} else {
			$_POST['amazon_filename'] = (isset($image_id)) ? $image_id : NULL;
			return true;
		}
	}

	public function getExtension($str)
	{
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}

	public function delete_file($filename) {
		$this->load->library('files/awslib');
		$upload_status = false;
		// Use the us-west-2 region and latest version of each client.
		$sharedConfig = [
		'region'  => 'ap-southeast-1',
		'version' => 'latest',
		'credentials' => [
		'key'    => 'AKIAJT3OY23NBEBGSNUQ',
		'secret' => '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'
		]
		];

		// Create an SDK class used to share configuration across clients.
		$sdk = new Aws\Sdk($sharedConfig);
		// Use an Aws\Sdk class to create the S3Client object.
		$s3Client = $sdk->createS3();

		$pyrobase_bucket = 'nuesto-pyrobase';
		$response = null;
		$key = 'bukti_transfer/'.$filename;
		if($s3Client->doesObjectExist($pyrobase_bucket,$key)) {
			$response = $s3Client->deleteObject(array('Bucket' => $pyrobase_bucket,'Key' => $key));
		}
	}

	public function check_date_format($date) {
		$test_arr  = explode('-', $date);
		if (count($test_arr) == 3) {
			if (checkdate($test_arr[1], $test_arr[2], $test_arr[0])) {
        		return true;
			} else {
        		$this->form_validation->set_message('check_date_format','<strong>'.lang('commerce:tanggal_transfer').'</strong> format tanggal tidak sesuai. Format yyyy-mm-dd');
				return false;
			}
		} else {
    		$this->form_validation->set_message('check_date_format','<strong>'.lang('commerce:tanggal_transfer').'</strong> format tanggal tidak sesuai. Format yyyy-mm-dd');
			return false;
		}
	}

	public function check_max_date($date, $max_date) {
			if(strtotime($date) > strtotime($max_date)) {
				$this->form_validation->set_message('check_max_date','<strong>'.lang('commerce:tanggal_transfer').'</strong> harus kurang dari tanggal hari ini');
				return false;
			}
		return true;
	}


	// new function
	public function customer_info() {
		// get provinsi
		$data['provinsi'] = $this->provinsi_m->get_provinsi();

		// Get products
		$id_customer = NULL;
		if(isset($this->current_user->id)) {
			$id_customer = $this->current_user->id;
		}
		$current_order = $this->order_m->get_current_order($id_customer);

		if($this->input->get('order')) {
			$current_order = $this->order_m->get_order_by_many(array('default_commerce_order.id'=>$this->input->get('order')));
		}

		if(isset($current_order) AND $this->input->get('order')) {
			if($current_order['id'] != $this->input->get('order')) {
				$log_data = array(
					'time' => date('Y-m-d H:i:s'),
					'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
					'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
					'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
					'function' => 'customer_info',
					'url' => base_url(uri_string()),
					'get_data' => json_encode($_GET),
					'post_data' => json_encode($_POST),
					'id_order' => (isset($current_user['id'])) ? $current_user['id'] : NULL,
					'id_order_get' => $this->input->get('order'),
					'messages' => 'order sekarang berbeda dengan order dari parameter get'
					);
				log_message('error',json_encode($log_data));

				redirect('commerce/order/customer_info?order='.$current_order['id']);
			}
		}

		// Landing page
		$data['landing_page'] = NULL;
		if($current_order['id_affiliate']) {
			$data['landing_page'] = $this->konten_m->get_konten_by_uri($current_order['id_affiliate']);
		}

		// update id_customer if the order id customer is null but user already logged in
		if($current_order != NULL
			AND $current_order['id_customer'] == NULL
			AND isset($this->current_user->id)) {

			$this->order_m->update_order(
				array(
					'id_customer' => $this->current_user->id,
					'created_by' => $this->current_user->id
				),
				array('id' => $current_order['id'])
			);
		}

		// get this order's cart
		$data['carts'] = $this->cart_m->get_cart_by_order($current_order['id']);
		if(count($data['carts'])<1) {

			$log_data = array(
				'time' => date('Y-m-d H:i:s'),
				'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
				'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
				'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
				'function' => 'customer_info',
				'url' => base_url(uri_string()),
				'get_data' => json_encode($_GET),
				'post_data' => json_encode($_POST),
				'id_order' => (isset($current_order['id'])) ? $current_order['id'] : NULL,
				'messages' => 'tidak ada item dalam cart'
				);
			log_message('error',json_encode($log_data));

			$this->session->set_flashdata('error','Tidak ada item dalam cart');
			redirect('commerce/order/my_order');
		}

		//$data['default_id_product'] = $this->variables->__get('default_id_product');

		// get current order's products
		$data['current_products'] = NULL;
		if($current_order != NULL){
			$data['current_products'] = $this->order_m->get_order_product($current_order['id']);
		}

		// Get kurir
		$data['pengiriman'] = $this->pengiriman_m->get_pengiriman(NULL, array('is_active'=>1));

		// get current user's data
		$data['user'] = $this->current_user;

		// check in carts if all product is digital
		// then next step is payment
		// if not next step is shipping
		$all_digital = 1;
		foreach ($data['carts'] as $cart) {
			if($cart['type'] == 'physical') {
				$all_digital = 0;
				continue;
			}
		}
		$next_step = ($all_digital == 1) ? 'payment_method' : 'shipping';
		$next_step = 'payment_method';

    	$this->template->title(lang('commerce:order:view'));
		$this->template->set_layout('checkout.html');
		$this->template->set_breadcrumb(lang('commerce:order:new'))
			->set('current_order',$current_order)
			->set('all_digital',$all_digital)
			->set('current_step','customer_info')
			->set('prev_step','')
			->set('next_step',$next_step)
			->build('checkout/customer_info', $data);
	}

	public function customer_info_logged_in()
	{
		// Get products
		// check if current user have logged in
		if(isset($this->current_user->id)){
			// get current order by current user's id
			$current_order = $this->order_m->get_current_order($this->current_user->id);

			// if not found, then try to get by current session
			if($current_order == NULL){
				$current_order = $this->order_m->get_current_order();
			}
		// if current user is not logged in yet, get by session
		}else{
			$current_order = $this->order_m->get_current_order();
		}

		// update id_customer if the order id customer is null but user already logged in
		if($current_order != NULL
			AND $current_order['id_customer'] == NULL
			AND isset($this->current_user->id)) {

			$this->order_m->update_order(
				array(
					'id_customer' => $this->current_user->id,
					'created_by' => $this->current_user->id
				),
				array('id' => $current_order['id'])
			);
		}

		//$data['default_id_product'] = $this->variables->__get('default_id_product');

		// get current order's products
		$data['current_products'] = NULL;
		if($current_order != NULL){
			$data['current_products'] = $this->order_m->get_order_product($current_order['id']);
		}

		// Landing page
		$data['landing_page'] = NULL;
		if($current_order['id_affiliate']) {
			$data['landing_page'] = $this->konten_m->get_konten_by_id_affiliate($current_order['id_affiliate']);
		}

		$data['current_order'] = $current_order;

		// Get kurir
		$data['pengiriman'] = $this->pengiriman_m->get_pengiriman(NULL, array('is_active'=>1));

		// get current user's data
		$data['user'] = $this->current_user;

		$this->load->view('checkout/customer_info_logged_in',$data);
	}

	public function tes_shipping()
	{
		dump($this->input->post());
	}
	public function shipping()
	{
		if(!$this->session->userdata('cart_session')) {
			redirect('');
		}
		// get provinsi
		$data['provinsi'] = $this->provinsi_m->get_provinsi();

		// Get current order
		$current_order = $this->order_m->get_current_order($this->current_user->id);

		if(!$current_order OR $current_order['status_pesanan']=='waiting-payment') {
			redirect('');
		}

		$data['carts'] = $this->cart_m->get_cart_by_order($current_order['id']);

		// Get rekening
		$data['rekening'] = $this->rekening_m->get_rekening(NULL,array('rekening_utama'=>1));

		// Get kurir
		$data['pengiriman'] = $this->pengiriman_m->get_pengiriman(NULL, array('is_active'=>1));

		// get user info
		$user = (array)$this->ion_auth->profile();
		if(isset($user['id_kota']) AND $user['id_kota'] != 0) {
			$data['provinsi_kota'] = $this->kota_m->get_provinsi_by_kota($user['id_kota']);
			if(count($data['provinsi_kota'])==1) {
				$user['id_provinsi'] = $data['provinsi_kota'][0]['id'];
			}
			$data['kota'] = $this->kota_m->get_kota_by_provinsi($user['id_provinsi']);
		}
		$data['user'] = $user;

		// check in carts if all product is digital
		// then redirect to payment
		// if not continue as it is
		$all_digital = 1;
		foreach ($data['carts'] as $cart) {
			if($cart['type'] == 'physical') {
				$all_digital = 0;
				continue;
			}
		}
		if($all_digital == '1') {
			redirect('commerce/order/payment');
		}

		if($_POST) {
			if($this->input->post('kode-pos-pengiriman')=='-') {
				$_POST['kode-pos-pengiriman'] = '';
			}
			$rules = array(
				array(
					'field' => 'kota-pengiriman',
					'label' => 'Alamat Pengiriman Kota',
					'rules' => 'required',
				),
				array(
					'field' => 'kode-pos-pengiriman',
					'label' => 'Alamat Pengiriman Kode Pos',
					'rules' => 'trim|numeric|max_length[7]',
				),
				array(
					'field' => 'alamat-pengiriman',
					'label' => 'Alamat Pengiriman Alamat Lengkap',
					'rules' => 'trim|required|max_length[1000]',
				),
				array(
					'field' => 'nama-penerima',
					'label' => 'Nama Penerima',
					'rules' => 'trim|required|max_length[100]',
				),
				array(
					'field' => 'nohp-penerima',
					'label' => 'No HP Penerima',
					'rules' => 'trim|required|numeric|max_length[13]',
				),
			);

			$this->form_validation->set_rules($rules);

			if($this->form_validation->run()) {
				$values = $this->input->post();

				// $order_values = array(
				// 	'alamat_pengiriman' => $values['alamat-pengiriman'],
				// 	'nama_penerima' => $values['nama-penerima'],
				// 	'no_hp_penerima' => $values['nohp-penerima'],
				// 	'id_kota_pengiriman' => $values['kota-pengiriman'],
				// 	);

				$order_values = array(
                    'nama_penerima' => $values['nama-penerima'],
                    'no_hp_penerima' => $values['nohp-penerima'],
                    'alamat_pengiriman' => $values['alamat-pengiriman'],
                    'id_kota_pengiriman' => $values['kota-pengiriman'],
                    'kurir_pengiriman' => (isset($values['courier'])) ? $values['courier'] : NULL,
                    'paket_pengiriman' => (isset($values['pengiriman'])) ? $values['pengiriman'] : NULL,
                    'berat_pengiriman' => (isset($values['berat-pengiriman'])) ? $values['berat-pengiriman'] : NULL,
                    );

				$update_order = $this->order_m->update_order($order_values,array('id'=>$current_order['id']));

				$result = array('status'=>'success');

			} else {
				$result = array('status'=>'failed','message'=>validation_errors());
			}

			if($this->input->is_ajax_request()) {
				exit(json_encode($result));
			}
		}

		$next_step = ($all_digital == 1) ? 'payment_method' : 'shipping';
		$next_step = 'payment_method';

    	$this->template->title(lang('commerce:order:view'));
		$this->template->set_layout('checkout.html');
		$this->template->set_breadcrumb(lang('commerce:order:new'))
			->set('current_order',$current_order)
			->set('all_digital',$all_digital)
			->set('current_step','shipping')
			->set('prev_step','customer_info')
			->set('next_step',$next_step)
			->build('checkout/shipping', $data);
	}

	public function payment_method()
	{
		// get provinsi
		$data['provinsi'] = $this->provinsi_m->get_provinsi();

		// Get products
		$id_customer = NULL;
		if(isset($this->current_user->id)) {
			$id_customer = $this->current_user->id;
		}
		$current_order = $this->order_m->get_current_order($id_customer);

		if(!$current_order) {
			$log_data = array(
				'time' => date('Y-m-d H:i:s'),
				'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
				'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
				'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
				'function' => 'payment_method',
				'url' => base_url(uri_string()),
				'get_data' => json_encode($_GET),
				'post_data' => json_encode($_POST),
				'id_order' => (isset($current_user['id'])) ? $current_user['id'] : NULL,
				'messages' => 'order tidak ditemukan'
				);
			log_message('error',json_encode($log_data));

			$this->session->set_flashdata('error',$log_data['messages']);
			redirect('commerce/order/my_order');
		}

		if($current_order['status_pesanan']=='waiting-payment') {
			$log_data = array(
				'time' => date('Y-m-d H:i:s'),
				'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
				'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
				'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
				'function' => 'payment_method',
				'url' => base_url(uri_string()),
				'get_data' => json_encode($_GET),
				'post_data' => json_encode($_POST),
				'id_order' => (isset($current_user['id'])) ? $current_user['id'] : NULL,
				'step' => 'payment_method',
				'messages' => 'order sudah dalam status '.$current_order['status_pesanan']
				);
			log_message('error',json_encode($log_data));
			$this->session->set_flashdata('error',$log_data['messages']);
			redirect('commerce/order/my_order');
		}

		// Landing page
		$data['landing_page'] = NULL;
		if($current_order['id_affiliate']) {
			$data['landing_page'] = $this->konten_m->get_konten_by_uri($current_order['id_affiliate']);
		}

		// update id_customer if the order id customer is null but user already logged in
		if($current_order != NULL
			AND $current_order['id_customer'] == NULL
			AND isset($this->current_user->id)) {

			$this->order_m->update_order(
				array(
					'id_customer' => $this->current_user->id,
					'created_by' => $this->current_user->id
				),
				array('id' => $current_order['id'])
			);
		}

		// get this order's cart
		$data['carts'] = $this->cart_m->get_cart_by_order($current_order['id']);
		if(count($data['carts'])<1) {
			$log_data = array(
				'time' => date('Y-m-d H:i:s'),
				'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
				'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
				'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
				'function' => 'payment_method',
				'url' => base_url(uri_string()),
				'get_data' => json_encode($_GET),
				'post_data' => json_encode($_POST),
				'id_order' => (isset($current_user['id'])) ? $current_user['id'] : NULL,
				'step' => 'payment_method',
				'messages' => 'tidak ada item dalam cart'
				);
			log_message('error',json_encode($log_data));
			$this->session->set_flashdata('Tidak ada item dalam cart');
			redirect('commerce/order/my_order');
		}

		//$data['default_id_product'] = $this->variables->__get('default_id_product');

		// get current order's products
		$data['current_products'] = NULL;
		if($current_order != NULL){
			$data['current_products'] = $this->order_m->get_order_product($current_order['id']);
		}

		// Get rekening
		$data['rekening'] = $this->rekening_m->get_rekening(NULL,array('rekening_utama'=>1));

		// get current user's data
		$data['user'] = $this->current_user;

		// check in carts if all product is digital
		// then next step is payment
		// if not next step is shipping
		$all_digital = 1;
		foreach ($data['carts'] as $cart) {
			if($cart['type'] == 'physical') {
				$all_digital = 0;
				continue;
			}
		}
		$next_step = ($all_digital == 1) ? 'payment' : 'shipping';
		$next_step = 'payment_method';
		$prev_step = ($all_digital == 1) ? 'customer_info' : 'shipping';

		$next_step = 'payment';
		$prev_step = 'customer_info';

    	$this->template->title(lang('commerce:order:view'));
		$this->template->set_layout('checkout.html');
		$this->template->set_breadcrumb(lang('commerce:order:new'))
			->set('current_order',$current_order)
			->set('all_digital',$all_digital)
			->set('current_step','payment_method')
			->set('prev_step',$prev_step)
			->set('next_step',$next_step)
			->build('checkout/payment_method', $data);
	}

	public function payment_choice()
	{
		$choice = $this->input->post('payment_method');
		if($choice=='transfer') {
			$this->payment();
		} elseif($choice=='ipaymu') {
			$this->ipaymu_payment();
		} elseif($choice=='free') {
			$this->payment();
		}
	}

	public function payment() {
		// get provinsi
		$data['provinsi'] = $this->provinsi_m->get_provinsi();

		// Get current order
		$current_order = $this->order_m->get_current_order($this->current_user->id);
		if(!$current_order) {
			$log_data = array(
				'time' => date('Y-m-d H:i:s'),
				'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
				'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
				'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
				'function' => 'payment',
				'url' => base_url(uri_string()),
				'get_data' => json_encode($_GET),
				'post_data' => json_encode($_POST),
				'id_order' => (isset($current_user['id'])) ? $current_user['id'] : NULL,
				'messages' => 'order tidak ditemukan'
				);
			log_message('error',json_encode($log_data));

			$this->session->set_flashdata('error',$log_data['messages']);
			redirect('commerce/order/my_order');
		}

		if($current_order['status_pesanan']=='waiting-payment') {
			$log_data = array(
				'time' => date('Y-m-d H:i:s'),
				'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
				'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
				'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
				'function' => 'payment',
				'url' => base_url(uri_string()),
				'get_data' => json_encode($_GET),
				'post_data' => json_encode($_POST),
				'id_order' => (isset($current_user['id'])) ? $current_user['id'] : NULL,
				'messages' => 'order sudah dalam status '.$current_order['status_pesanan']
				);
			log_message('error',json_encode($log_data));
			$this->session->set_flashdata('error',$log_data['messages']);
			redirect('commerce/order/my_order');
		}

		// set no_invoice, status_pesanan, kode_unik
		$unique_number = $this->order_m->generate_unique();

		// if biaya_order - discount_nominal == 0
		// then doesn't have to set unique code and order status is telah melakukan pembayaran
		$kode_unik = $unique_number['kode_unik'];
		$status_pesanan = 'waiting-payment';

		$sub_total = $current_order['biaya_order']-$current_order['discount_nominal'];
		$sub_total = ($sub_total<0) ? 0 : $sub_total;
		if($sub_total == 0) {
			$kode_unik = 0;
		}

		$exist_history = (isset($current_order['state_history'])) ? json_decode($current_order['state_history'],true) : array();
		$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>'waiting-payment','updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'));
		$state_history = array_merge($exist_history,$new_history);

		$this->config->load('config.php');
		$expired_config = $this->config->item('expired');
		if($expired_config[$current_order['order_type']]['type'] != 'unlimit') {
			$expired_on = date('Y-m-d H:i', strtotime($expired_config[$current_order['order_type']]['value']));
		} else {
			$expired_on = NULL;
		}

		$order_values = array(
			'no_invoice' => $unique_number['no_invoice'],
			'kode_unik' => $kode_unik,
			'status_pesanan' => 'waiting-payment',
			'tanggal_order' => date('Y-m-d H:i'),
			'placed_on' => date('Y-m-d H:i'),
			'expired_on' => $expired_on,
			'amount_billed' => $sub_total+$current_order['biaya_pengiriman']+$current_order['kode_unik'],
			'created_by' => $this->current_user->id,
			'state_history' => json_encode($state_history)
			);

		$update_order = $this->order_m->update_order($order_values,array('id'=>$current_order['id']));

		$data['carts'] = $this->cart_m->get_cart_by_order($current_order['id']);

		// get new current order
		$current_order = $this->order_m->get_order_by_id($current_order['id']);

		// send invoice email to customer
		$this->send_invoice($current_order['id']);

		// if biaya_order - discount_nominal == 0
		// then doesn't have to set unique code and order status is telah melakukan pembayaran
		$status_pesanan = 'waiting-payment';
		$sub_total = $current_order['biaya_order']-$current_order['discount_nominal'];
		$sub_total = ($sub_total<0) ? 0 : $sub_total;
		if($sub_total == 0) {
			$status_pesanan = 'completed';

			$exist_history = (isset($current_order['state_history'])) ? json_decode($current_order['state_history'],true) : array();
			$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>'completed','updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'));
			$state_history = array_merge($exist_history,$new_history);

			$order_values = array(
			'status_pesanan' => $status_pesanan,
			'state_history' => json_encode($state_history),
			'amount_billed' => 0
			);

			if($order_values['status_pesanan']=='completed') {
				$order_values['closed_on'] = date('Y-m-d H:i:s');
			}

			// update order
			$update_order = $this->order_m->update_order($order_values,array('id'=>$current_order['id']));

			// if total invoice is 0 then treat it as it already confirmed by admin
			$default_group = $this->ion_auth->get_group_by_name('member');
            $customer_group = $this->ion_auth->get_group_by_name('customer');
            $user = $this->ion_auth->get_user_array($current_order['id_customer']);
            if($user['group_id'] == $customer_group->id) {
	        	$result = $this->ion_auth->update_user_group($current_order['id_customer'],$default_group->id);
	        	$this->load->model('users/user_m');
	        	$this->user_m->set_membership_time($current_order['id_customer']);
	        } else {
	        	$log_data = array(
	        		'time' => date('Y-m-d H:i:s'),
	        		'user_id' => $current_order['id_customer'],
	        		'display_name' => $user['display_name'],
	        		'function' => 'payment',
	        		'url' => base_url(uri_string()),
	        		'messages' => 'user group sudah bukan member basic'
	        		);
	        	log_message('error',json_encode($log_data));
	        }

			$payment_values = array(
				'id_order'=>$current_order['id'],
				'jumlah_pembayaran'=>0,
				'closed_on'=>date('Y-m-d H:i:s'),
				'payment_method'=>'free',
				'status'=>'paid',
				'billed_on'=>date('Y-m-d H:i:s'),
				'paid_on'=>date('Y-m-d H:i:s'),
				'keterangan'=>'',
				'history' => json_encode(array(array('time'=>date('Y-m-d H:i:s'),'status'=>'paid')))
				);
			$insert_payment = $this->payment_m->insert_payment($payment_values, $current_order['id']);

			// send notif
			$email_data['order'] = $current_order;
            $this->load->helper('commerce/commerce');
            $pembayaran_dikonfirmasi = array(
				'subject' => '[DuaKodiKartika.com] Pesanan #'.$email_data['order']['no_invoice'].' Telah Dikonfirmasi',
				'body' => $this->load->view('email_template/pembayaran_dikonfirmasi',$email_data,true)
				);

			$customer = $this->ion_auth->get_user($current_order['id_customer']);
			send_email_api($customer->email,$pembayaran_dikonfirmasi);

			// send notif to affiliate

			$email_data['order_product'] = $this->order_m->get_order_product($email_data['order']['id']);
			$email_data['order']['kota'] = $this->kota_m->get_kota_by_id($email_data['order']['id_kota_pengiriman']);

			// check if all product is digital product
			// then remove field 'Pengiriman' and 'Alamat'
			$email_data['all_digital'] = 1;
			foreach ($email_data['order_product'] as $k=>$op) {
				if($op['is_digital_product']==0) {
					$email_data['all_digital'] = $op['is_digital_product'];
					continue;
				}
			}

			if($current_order['id_affiliate'] != NULL AND $current_order['id_affiliate'] != '0') {
				$email_data['to_affiliate'] = 1;

				$tagihan_pembayaran = array(
					'subject' => '[DuaKodiKartika.com] Terdapat pesanan baru melalui link affiliate anda #'.$email_data['order']['no_invoice'],
					'body' => $this->load->view('email_template/affiliate_pesanan_baru',$email_data,true)
					);

				$affiliate_user = $this->ion_auth->get_user($current_order['id_affiliate']);
				send_email_api($affiliate_user->email,$tagihan_pembayaran);
			}
		} else {
			$payment_values['id_order'] = $current_order['id'];
			$payment_values['payment_method'] = 'transfer';
			$payment_values['status'] = 'unpaid';
			$payment_values['billed_on'] = date('Y-m-d H:i:s');
			$payment_values['jumlah_pembayaran'] = 0;
			$payment_values['keterangan'] = '';
			$payment_values['history'] = json_encode(array(array('time'=>date('Y-m-d H:i:s'),'status'=>'unpaid')));
			$result = $this->payment_m->insert_payment($payment_values);
		}

		// Get rekening
		$data['rekening'] = $this->rekening_m->get_rekening(NULL,array('rekening_utama'=>1));

		// unset cart_session
		$this->session->unset_userdata('cart_session');

		// check in carts if all product is digital
		// then redirect to payment
		// if not continue as it is
		$all_digital = 1;
		foreach ($data['carts'] as $cart) {
			if($cart['type'] == 'physical') {
				$all_digital = 0;
				continue;
			}
		}

		$data['amount_billed'] = $order_values['amount_billed'];

    	$this->template->title(lang('commerce:order:view'));
		$this->template->set_layout('checkout.html');
		$this->template->set_breadcrumb(lang('commerce:order:new'))
			->set('current_order',$current_order)
			->set('all_digital',$all_digital)
			->set('current_step','payment')
			->set('prev_step','')
			->set('next_step','')
			->build('checkout/payment', $data);
	}

	public function ipaymu_payment()
	{
		// Get current order
		$current_order = $this->order_m->get_current_order($this->current_user->id);
		if(!$current_order) {
			$log_data = array(
				'time' => date('Y-m-d H:i:s'),
				'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
				'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
				'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
				'function' => 'ipaymu_payment',
				'url' => base_url(uri_string()),
				'get_data' => json_encode($_GET),
				'post_data' => json_encode($_POST),
				'id_order' => (isset($current_user['id'])) ? $current_user['id'] : NULL,
				'messages' => 'order tidak ditemukan'
				);
			log_message('error',json_encode($log_data));

			$this->session->set_flashdata('error',$log_data['messages']);
			redirect('commerce/order/my_order');
		}

		if($current_order['status_pesanan']=='waiting-payment') {
			$log_data = array(
				'time' => date('Y-m-d H:i:s'),
				'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
				'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
				'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
				'function' => 'ipaymu_payment',
				'url' => base_url(uri_string()),
				'get_data' => json_encode($_GET),
				'post_data' => json_encode($_POST),
				'id_order' => (isset($current_user['id'])) ? $current_user['id'] : NULL,
				'messages' => 'order sudah dalam status '.$current_order['status_pesanan']
				);
			log_message('error',json_encode($log_data));
			$this->session->set_flashdata('error',$log_data['messages']);
			redirect('commerce/order/my_order');
		}

		// get this order's cart
		$carts = $this->cart_m->get_cart_by_order($current_order['id']);
		if(count($carts)<1) {
			$log_data = array(
				'time' => date('Y-m-d H:i:s'),
				'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
				'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
				'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
				'function' => 'ipaymu_payment',
				'url' => base_url(uri_string()),
				'get_data' => json_encode($_GET),
				'post_data' => json_encode($_POST),
				'id_order' => (isset($current_user['id'])) ? $current_user['id'] : NULL,
				'messages' => 'tidak ada item dalam cart'
				);
			log_message('error',json_encode($log_data));

			$this->session->set_flashdata('error','Tidak ada item dalam cart');
			redirect('commerce/order/my_order');
		}

		// set no_invoice, status_pesanan, kode_unik
		$unique_number = $this->order_m->generate_unique();

		// if biaya_order - discount_nominal == 0
		// then doesn't have to set unique code and order status is telah melakukan pembayaran
		$kode_unik = $unique_number['kode_unik'];
		$status_pesanan = 'waiting-payment';

		$sub_total = $current_order['biaya_order']-$current_order['discount_nominal'];
		$sub_total = ($sub_total<0) ? 0 : $sub_total;
		if($sub_total == 0) {
			$kode_unik = 0;
		}

		$ipaymu_data = $this->input->get();
		$ipaymu_code = array('-1'=>'sedang diproses','0'=>'pending','1'=>'berhasil','2'=>'batal','3'=>'refund');

		$exist_history = (isset($current_order['state_history'])) ? json_decode($current_order['state_history'],true) : array();
		$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>'waiting-payment','updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'));
		$state_history = array_merge($exist_history,$new_history);

		$this->config->load('config.php');
		$expired_config = $this->config->item('expired');
		if($expired_config[$current_order['order_type']]['type'] != 'unlimit') {
			$expired_on = date('Y-m-d H:i', strtotime($expired_config[$current_order['order_type']]['value']));
		} else {
			$expired_on = NULL;
		}

		$order_values = array(
			'no_invoice' => $unique_number['no_invoice'],
			'status_pesanan' => 'waiting-payment',
			'tanggal_order' => date('Y-m-d H:i'),
			'placed_on' => date('Y-m-d H:i'),
			'expired_on' => $expired_on,
			'amount_billed' => $sub_total+$current_order['biaya_pengiriman']+$current_order['kode_unik'],
			'state_history' => json_encode($state_history)
			);

		$update_order = $this->order_m->update_order($order_values,array('id'=>$current_order['id']));

		$quantity = 0;
		foreach ($carts as $cart) {
			$product_name[] = $cart['nama'];
			$product_comments[] = $cart['deskripsi'];
			$quantity += $cart['jumlah'];
		}

		$payment_values['id_order'] = $current_order['id'];
		$payment_values['payment_method'] = 'ipaymu';
		$payment_values['status'] = 'unpaid';
		$payment_values['billed_on'] = date('Y-m-d H:i:s');
		$payment_values['jumlah_pembayaran'] = 0;
		$payment_values['keterangan'] = '';
		$payment_values['history'] = json_encode(array(array('time'=>date('Y-m-d H:i:s'),'status'=>'unpaid')));
		$result = $this->payment_m->insert_payment($payment_values);

		$url = 'https://my.ipaymu.com/payment.htm';  // URL Payment iPaymu
		$params = array(   // Prepare Parameters
		            'key'      => 'ayUB41B2uNpAnMAZAR1lqMJmwZrx61', // API Key Merchant / Penjual
		            'action'   => 'payment',
		            'product'  => 'Transaksi Nomor '.$unique_number['no_invoice'],
		            'price'    => ($current_order['biaya_order']-$current_order['discount_nominal'])+$current_order['biaya_pengiriman']+$current_order['kode_unik'], // Total Harga
		            // 'price'    => '50000',
		            'quantity' => 1,
		            'comments' => implode(",", $product_comments), // Optional
		            'ureturn'  => base_url().'commerce/order/ipaymu_return?&q=return',
		            'unotify'  => base_url().'commerce/order/ipaymu_notify',
		            'ucancel'  => base_url().'commerce/order/my_order',

		            // /* Parameter untuk pembayaran lain menggunakan PayPal
		            //  * ----------------------------------------------- */
		            // 'invoice_number' => uniqid('INV-'), // Optional
		            // 'paypal_email'   => 'email_paypal_merchant',
		            // 'paypal_price'   => 1, // Total harga dalam kurs USD
		            // /* ----------------------------------------------- */

		            'format'   => 'json' // Format: xml / json. Default: xml
		            );

		$params_string = http_build_query($params);

		//open connection
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		//execute post
		$request = curl_exec($ch);

		if ( $request === false ) {
			echo 'Curl Error: ' . curl_error($ch);
		} else {

			$data['result'] = json_decode($request, true);

			if( isset($data['result']['url']) ) {
				// header('location: '. $result['url']);
				$ipaymu_values = array(
					'ipaymu_session_id' => $data['result']['sessionID'],
					'ipaymu_url' => $data['result']['url'],
					);
				$update_payment = $this->payment_m->update_payment($ipaymu_values, $current_order['id']);

			} else {
				//echo "Request Error ". $result['Status'] .": ". $result['Keterangan'];
			}
		}

		//close connection
		curl_close($ch);

		// check in carts if all product is digital
		// then redirect to payment
		// if not continue as it is
		$all_digital = 1;
		foreach ($carts as $cart) {
			if($cart['type'] == 'physical') {
				$all_digital = 0;
				continue;
			}
		}

		$biaya_kirim = (isset($current_order['biaya_pengiriman'])) ? $current_order['biaya_pengiriman'] : 0;
		$sub_total = $current_order['biaya_order']-$current_order['discount_nominal'];
		$sub_total = ($sub_total<0) ? 0 : $sub_total;
		$data['amount_billed'] = $order_values['amount_billed'];


		$this->template->title(lang('commerce:order:view'));
		$this->template->set_layout('checkout.html');
		$this->template->set_breadcrumb(lang('commerce:order:new'))
			->set('current_order',$current_order)
			->set('all_digital',$all_digital)
			->set('current_step','payment')
			->set('prev_step','')
			->set('next_step','')
			->build('checkout/payment', $data);
	}

	public function ipaymu_return()
	{
		if(!is_logged_in()) {
			$log_data = array(
                'time' => date('Y-m-d H:i:s'),
                'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
                'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
                'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
                'function' => 'ipaymu_return',
                'url' => base_url(uri_string()),
                'get_data' => json_encode($_GET),
                'post_data' => json_encode($_POST),
                'messages' => 'User belum login'
                );
            log_message('error',json_encode($log_data));

            $this->session->set_flashdata('error','Silahkan Login Terlebih Dahulu');
            redirect('login?redirect_to='.urlencode('commerce/order/ipaymu_return'.get_query_string(4)));
		}
		// Get current order
		$current_order = $this->order_m->get_current_order($this->current_user->id,'waiting-payment');

		// get this order's cart
		$carts = $this->cart_m->get_cart_by_order($current_order['id']);
		if(count($carts)<1) {
			$log_data = array(
				'time' => date('Y-m-d H:i:s'),
				'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
				'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
				'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
				'function' => 'ipaymu_return',
				'url' => base_url(uri_string()),
				'get_data' => json_encode($_GET),
				'post_data' => json_encode($_POST),
				'id_order' => (isset($current_user['id'])) ? $current_user['id'] : NULL,
				'messages' => 'tidak ada item dalam cart'
				);
			log_message('error',json_encode($log_data));

			$this->session->set_flashdata('error','Tidak ada item dalam cart');
			redirect('commerce/order/my_order');
		}

		$ipaymu_data = $this->input->get();
		$log_data = array(
			'time' => date('Y-m-d H:i:s'),
			'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : NULL,
			'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : NULL,
			'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : NULL,
			'function' => 'ipaymu_return',
			'url' => base_url(uri_string()),
			'get_data' => json_encode($_GET),
			'post_data' => json_encode($_POST),
			'id_order' => (isset($current_user['id'])) ? $current_user['id'] : NULL,
			'messages' => 'Data return ipaymu'
			);
		log_message('debug',json_encode($log_data));

		$ipaymu_code = array('-1'=>'sedang diproses','0'=>'pending','1'=>'berhasil','2'=>'batal','3'=>'refund');

		$exist_history = (isset($current_order['state_history'])) ? json_decode($current_order['state_history'],true) : array();
		$new_history = array(array(
			'time'=>date('Y-m-d H:i:s'),
			'status'=>($ipaymu_data['status']=='berhasil') ? 'completed' : 'waiting-payment',
			'updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'
			));
		$state_history = array_merge($exist_history,$new_history);

		$order_values = array(
			'status_pesanan' => ($ipaymu_data['status']=='berhasil') ? 'completed' : 'waiting-payment',
			'state_history' => json_encode($state_history)
			);

		if($order_values['status_pesanan']=='completed') {
			$order_values['closed_on'] = date('Y-m-d H:i:s');
		}

		$update_order = $this->order_m->update_order($order_values,array('id'=>$current_order['id']));

		$payment = $this->payment_m->get_payment_by_order($current_order['id']);
		$exist_history = (isset($payment['history'])) ? json_decode($payment['history'],true) : array();
		$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>($ipaymu_data['status']=='berhasil') ? 'paid' : 'pending','updated_by'=>(isset($this->current_user->display_name)) ? strtolower($this->current_user->display_name) : 'system'));
		$state_history = array_merge($exist_history,$new_history);

		$ipaymu_values = array(
			'status' => ($ipaymu_data['status']=='berhasil') ? 'paid' : 'pending',
			'ipaymu_trx_id' => $ipaymu_data['trx_id'],
			'ipaymu_data' => json_encode($ipaymu_data),
			'ipaymu_status_code' =>array_search($ipaymu_data['status'], $ipaymu_code),
			'ipaymu_status_name' =>$ipaymu_data['status'],
			'ipaymu_type' =>$ipaymu_data['tipe'],
			'ipaymu_status_last_updated_on' => date('Y-m-d H:i:s'),
			'history' => json_encode($state_history)
			);
		$result = $this->payment_m->update_payment($ipaymu_values, $current_order['id']);

		// if status berhasil then treat it as confirmed by admin
		if($ipaymu_data['status']=='berhasil') {
			$ipaymu_values = array(
				'paid_on' => date('Y-m-d H:i:s'),
				'closed_on' => date('Y-m-d H:i:s'),
				);
			$result = $this->payment_m->update_payment($ipaymu_values, $current_order['id']);

			// update user group
			if($order_entry['order_type']=='membership') {
				$default_group = $this->ion_auth->get_group_by_name('member');
		        $customer_group = $this->ion_auth->get_group_by_name('customer');
		        $user = $this->ion_auth->get_user_array($current_order['id_customer']);
		        if($user['group_id'] == $customer_group->id) {
		        	$update_user_group = $this->ion_auth->update_user_group($current_order['id_customer'],$default_group->id);
		        	$this->load->model('users/user_m');
	        		$this->user_m->set_membership_time($current_order['id_customer']);
		        } else {
		        	$log_data = array(
		        		'time' => date('Y-m-d H:i:s'),
		        		'user_id' => $email_data['order']['id_customer'],
		        		'display_name' => $user['display_name'],
		        		'function' => 'ipaymu_return',
		        		'url' => base_url(uri_string()),
		        		'messages' => 'user group sudah bukan member basic'
		        		);
		        	log_message('error',json_encode($log_data));
		        }
		    }

	        $email_data['order'] = $current_order;
	        $pembayaran_dikonfirmasi = array(
	        	'subject' => '[DuaKodiKartika.com] Pesanan #'.$email_data['order']['no_invoice'].' Telah Dikonfirmasi',
	        	'body' => $this->load->view('email_template/pembayaran_dikonfirmasi',$email_data,true)
	        	);

	        $customer = $this->ion_auth->get_user($current_order['id_customer']);
	        send_email_api($customer->email,$pembayaran_dikonfirmasi);

	        // send notif to affiliate
			$email_data['order_product'] = $this->order_m->get_order_product($email_data['order']['id']);
			$email_data['order']['kota'] = $this->kota_m->get_kota_by_id($email_data['order']['id_kota_pengiriman']);

			// check if all product is digital product
			// then remove field 'Pengiriman' and 'Alamat'
			$email_data['all_digital'] = 1;
			foreach ($email_data['order_product'] as $k=>$op) {
				if($op['is_digital_product']==0) {
					$email_data['all_digital'] = $op['is_digital_product'];
					continue;
				}
			}

			if($email_data['order']['id_affiliate'] != NULL AND $email_data['order']['id_affiliate'] != '0') {
				$email_data['to_affiliate'] = 1;

				$tagihan_pembayaran = array(
					'subject' => '[DuaKodiKartika.com] Terdapat pesanan baru melalui link affiliate anda #'.$email_data['order']['no_invoice'],
					'body' => $this->load->view('email_template/affiliate_pesanan_baru',$email_data,true)
					);

				$affiliate_user = $this->ion_auth->get_user($email_data['order']['id_affiliate']);
				send_email_api($affiliate_user->email,$tagihan_pembayaran);
			}
		}

		// check in carts if all product is digital
		// then redirect to payment
		// if not continue as it is
		$all_digital = 1;
		foreach ($carts as $cart) {
			if($cart['type'] == 'physical') {
				$all_digital = 0;
				continue;
			}
		}

		$data['ipaymu'] = $ipaymu_data;
		$_POST['payment_method'] = 'return_ipaymu';

		// unset cart_session
		$this->session->unset_userdata('cart_session');

		$this->template->title(lang('commerce:order:view'));
		$this->template->set_layout('checkout.html');
		$this->template->set_breadcrumb(lang('commerce:order:new'))
			->set('current_order',$current_order)
			->set('all_digital',$all_digital)
			->set('current_step','payment')
			->set('prev_step','')
			->set('next_step','')
			->build('checkout/payment', $data);
	}

	public function ipaymu_notify()
	{
		die('not used');
	}

	public function ipaymu_cancel()
	{
		echo 'halaman batal';
		dump($this->input->post());
		dump($this->input->get());
	}

	public function ipaymu_check_transaction()
	{
		$params = array(
			'ipaymu_status_code' => array('-1','0','1')
			);
		$ipaymu_payment_list = $this->payment_m->get_ipaymu_payment($params);

		foreach ($ipaymu_payment_list as $payment) {
			// check payment method
			if($payment['payment_method'] != 'ipaymu') {
				$log_data = array(
					'time' => date('Y-m-d H:i:s'),
					'user_id' => (isset($this->current_user->id)) ? $this->current_user->id : 'system',
					'user_displayname' => (isset($this->current_user->id)) ? $this->current_user->display_name : 'system',
					'session_id' => (isset($this->current_user->session_id)) ? $this->current_user->session_id : 'system',
					'function' => 'ipaymu_check_transaction',
					'url' => base_url(uri_string()),
					'get_data' => json_encode($_GET),
					'post_data' => json_encode($_POST),
					'id_order' => (isset($payment['id_order'])) ? $payment['id_order'] : NULL,
					'payment_method' => 'ipaymu',
					'messages' => 'metode pembayaran bukan ipaymu'
					);
				log_message('error',json_encode($log_data));

				continue;
			}

			if($payment['ipaymu_status_code'] != '1' OR ($payment['ipaymu_status_code']=='1' AND !isset($payment['paid_on']))) {
				$url = 'https://my.ipaymu.com/api/CekTransaksi.php';  // URL Payment iPaymu
				$params = array(   // Prepare Parameters
				            'key'	=> 'ayUB41B2uNpAnMAZAR1lqMJmwZrx61', // API Key Merchant / Penjual
				            'id'	=> $payment['ipaymu_trx_id'],
				            'format'=> 'json' // Format: xml / json. Default: xml
				            );

				$params_string = http_build_query($params);
				//open connection
				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL, $url.'?'.$params_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

				//execute post
				$request = curl_exec($ch);

				if ( $request === false ) {
					echo 'Curl Error: ' . curl_error($ch);
				} else {

					$result = json_decode($request, true);
					foreach ($result as $key => $value) {
						if($key!='Waktu') {
							$value = strtolower(str_replace(" ", "", $value));
						}
						$ipaymu_data[strtolower($key)] = $value;
					}
					$new_ipaymu_data = json_encode(array_merge(json_decode($payment['ipaymu_data'],true),$ipaymu_data));
					$order_values = NULL;
					$payment_values = array(
						'ipaymu_data' => $new_ipaymu_data,
						'ipaymu_status_code' => $result['Status'],
						'ipaymu_status_name' => strtolower($result['Keterangan']),
						'ipaymu_status_last_updated_on' => date('Y-m-d H:i:s'),
						);
					if($result['Status']=='1') {
						$exist_history = (isset($payment['history'])) ? json_decode($payment['history'],true) : array();
						$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>'paid','updated_by'=>'system'));
						$state_history = array_merge($exist_history,$new_history);

						$payment_values = array(
							'status' => 'paid',
							'tanggal_transfer' => date('Y-m-d H:i:s', strtotime($result['Waktu'])),
							'paid_on' => date('Y-m-d H:i:s', strtotime($result['Waktu'])),
							'closed_on' => date('Y-m-d H:i:s'),
							'jumlah_pembayaran' => $result['Nominal'],
							'ipaymu_status_code' => $result['Status'],
							'ipaymu_status_name' => strtolower($result['Keterangan']),
							'ipaymu_status_last_updated_on' => date('Y-m-d H:i:s'),
							'history' => json_encode($state_history)
							);

						$exist_history = (isset($current_order['state_history'])) ? json_decode($current_order['state_history'],true) : array();
						$new_history = array(array('time'=>date('Y-m-d H:i:s'),'status'=>'completed','updated_by'=>'system'
							));
						$state_history = array_merge($exist_history,$new_history);

						$order_values = array(
							'status_pesanan' => 'completed',
							'closed_on' => date('Y-m-d H:i:s'),
							'state_history' => json_encode($state_history)
							);

						$update_order = $this->order_m->update_order($order_values, array('id' => $payment['id_order']));

						if($update_order) {
							$email_data['order'] = $this->order_m->get_order_by_many(array('default_commerce_order.id'=>strtoupper($payment['id_order'])));

							// if status is 1 then treat it as it already confirmed by admin
							if($email_data['order']['order_type']=='membership') {
								$default_group = $this->ion_auth->get_group_by_name('member');
								$customer_group = $this->ion_auth->get_group_by_name('customer');
								$user = $this->ion_auth->get_user_array($email_data['order']['id_customer']);
								if($user['group_id'] == $customer_group->id) {
									$update_user_group = $this->ion_auth->update_user_group($email_data['order']['id_customer'],$default_group->id);
									$this->load->model('users/user_m');
	        						$this->user_m->set_membership_time($email_data['order']['id_customer']);
								} else {
									$log_data = array(
										'time' => date('Y-m-d H:i:s'),
										'user_id' => $email_data['order']['id_customer'],
										'display_name' => $user['display_name'],
										'function' => 'ipaymu_check_transaction',
										'url' => base_url(uri_string()),
										'messages' => 'user group sudah bukan member basic'
										);
									log_message('error',json_encode($log_data));
								}
							}

					        $pembayaran_dikonfirmasi = array(
					        	'subject' => '[DuaKodiKartika.com] Pesanan #'.$email_data['order']['no_invoice'].' Telah Dikonfirmasi',
					        	'body' => $this->load->view('email_template/pembayaran_dikonfirmasi',$email_data,true)
					        	);

					        $customer = $this->ion_auth->get_user($email_data['order']['id_customer']);
					        send_email_api($customer->email,$pembayaran_dikonfirmasi);

					        // send notif to affiliate
					        $email_data['order_product'] = $this->order_m->get_order_product($email_data['order']['id']);
							$email_data['order']['kota'] = $this->kota_m->get_kota_by_id($email_data['order']['id_kota_pengiriman']);

							// check if all product is digital product
							// then remove field 'Pengiriman' and 'Alamat'
							$email_data['all_digital'] = 1;
							foreach ($email_data['order_product'] as $k=>$op) {
								if($op['is_digital_product']==0) {
									$email_data['all_digital'] = $op['is_digital_product'];
									continue;
								}
							}

							if($email_data['order']['id_affiliate'] != NULL AND $email_data['order']['id_affiliate'] != '0') {
								$email_data['to_affiliate'] = 1;

								$tagihan_pembayaran = array(
									'subject' => '[DuaKodiKartika.com] Terdapat pesanan baru melalui link affiliate anda #'.$email_data['order']['no_invoice'],
									'body' => $this->load->view('email_template/affiliate_pesanan_baru',$email_data,true)
									);

								$affiliate_user = $this->ion_auth->get_user($email_data['order']['id_affiliate']);
								send_email_api($affiliate_user->email,$tagihan_pembayaran);
							}
						}
					}

					$update_payment = $this->payment_m->update_payment($payment_values,$payment['id_order']);
					dump($result,$order_values,$payment_values);
				}

			//close connection
			curl_close($ch);
			}
		}
	}

	public function set_affiliate()
	{
		if($_POST) {
			$rules = array(
				array(
					'field' => 'affiliate_code',
					'label' => 'Kode Affiliate',
					'rules' => 'trim|required|callback_check_affiliate_code',
				),
				);

			$this->form_validation->set_rules($rules);

			if($this->form_validation->run()) {
				$row_id = $this->input->post('id_order');
				$id_affiliate = $this->input->post('id_affiliate');
				$result = $this->order_m->update_order(array('id_affiliate'=>$id_affiliate), array('id' => $row_id));

				$result = array('status'=>'success','message'=>'Kode affiliate berhasil digunakan');

			} else {
				$result = array('status'=>'failed','message'=>validation_errors());
			}

			if($this->input->is_ajax_request()) {
				exit(json_encode($result));
			}
		} else {
			exit(json_encode(array('status'=>'failed','message'=>'Tidak ada data yang dikirim')));
		}
	}

	public function check_affiliate_code($code) {
		// get affiliate by uri code of landing page
		$landing_page = $this->konten_m->get_konten_by_uri($code);
		if(!isset($landing_page)) {
			$this->form_validation->set_message('check_affiliate_code','Kode affiliate tidak ditemukan');
			return false;
		}
		$_POST['id_affiliate'] = $landing_page['id_affiliate'];
		return true;
	}

	public function set_voucher()
	{
		if($_POST) {
			$rules = array(
				array(
					'field' => 'voucher_code',
					'label' => 'Kode Voucher',
					'rules' => 'trim|required|callback_check_voucher_code',
				),
				);

			$this->form_validation->set_rules($rules);

			if($this->form_validation->run()) {
				$row_id = $this->input->post('id_order');
				$order = $this->order_m->get_order_by_id($row_id);
				$voucher_id = $this->input->post('voucher_id');
				$id_affiliate = $this->input->post('id_affiliate');
				$order_values = array(
						'id_affiliate' => $id_affiliate,
						'voucher_id' => $voucher_id,
						'voucher_applied_on' => $this->input->post('voucher_applied_on'),
						'voucher_applied_data' => $this->input->post('voucher_applied_data'),
						);
				if($this->input->post('benefit_type')=='discount_nominal') {
					$order_values['discount_nominal'] = $this->input->post('benefit_value');
				} elseif($this->input->post('benefit_type')=='discount_percentage') {
					$order_values['discount_nominal'] = $order['biaya_order'] * ($this->input->post('benefit_value')/100);
				}
				$result = $this->order_m->update_order($order_values, array('id' => $row_id));

				$result = array('status'=>'success','message'=>'Voucher berhasil digunakan');
				if(isset($id_affiliate)) {
					$result['affiliate'] = array('name'=>user_displayname($id_affiliate,false),'code'=>$id_affiliate);
				}
			} else {
				$result = array('status'=>'failed','message'=>validation_errors());
			}

			if($this->input->is_ajax_request()) {
				exit(json_encode($result));
			}
		} else {
			exit(json_encode(array('status'=>'failed','message'=>'Tidak ada data yang dikirim')));
		}
	}

	public function check_voucher_code($code)
	{
		$row_id = $this->input->post('id_order');
		$order = $this->order_m->get_order_by_id($row_id);
		$voucher = $this->voucher_m->get_voucher_by_code($code);
		if($voucher) {
			// if($order['id_customer'] != NULL AND ($order['id_affiliate'] != NULL OR $order['id_affiliate'] != 0) AND $order['id_affiliate'] == $voucher['affiliate_id'] OR $order['id_affiliate'] == NULL OR $voucher['affiliate_id'] == NULL) {
				if($voucher['state_active']==1) {
					if($voucher['product_type']==$order['order_type']) {
						if($voucher['valid_unit'] == NULL OR (date('Y-m-d',strtotime($voucher['valid_unit'])) >= date('Y-m-d'))) {
							if($voucher['usage_limit_scope']=='limit_per_customer') {
								$params = array('id_customer'=>$this->current_user->id,'code'=>$code);
								$voucher_usage = $this->voucher_m->get_voucher_usage($params);
								if($voucher_usage['usage_count'] < $voucher['usage_limit_count']) {
									$result['status'] = true;
								} else {
									$result['message'] = 'Voucher sudah mencapai batas penggunaan';
									$result['status'] = false;
								}
							} elseif($voucher['usage_limit_scope']=='limit_all_customer') {
								$params = array('code'=>$code);
								$voucher_usage = $this->voucher_m->get_voucher_usage($params);
								if($voucher_usage['usage_count'] < $voucher['usage_limit_count']) {
									$result['status'] = true;
								} else {
									$result['message'] = 'Voucher sudah mencapai batas penggunaan';
									$result['status'] = false;
								}
							} elseif($voucher['usage_limit_count']==null AND $voucher['usage_limit_count']==null) {
								$result['status'] = true;
							}
						} else {
							$result['message'] = 'Voucher sudah expired';
							$result['status'] = false;
						}
					} else {
						$result['message'] = 'Voucher tidak dapat digunakan pada order ini';
						$result['status'] = false;
					}
				} else {
					$result['message'] = 'Voucher tidak aktif';
					$result['status'] = false;
				}
			// } else {
			// 	$result['message'] = 'Tidak dapat menggunakan kode voucher milik affiliate ini karena Anda telah melakukan order melalui link affiliate lain';
			// 	$result['status'] = false;
			// }
		} else {
			$result['message'] = 'Voucher tidak ditemukan';
			$result['status'] = false;
		}

		if(isset($result['message'])) {
			$this->form_validation->set_message('check_voucher_code',$result['message']);
		}

		if($result['status']==true) {
			$_POST['voucher_id'] = $voucher['id'];
			$_POST['voucher_applied_on'] = date('Y-m-d H:i:s');
			$_POST['voucher_applied_data'] = json_encode($voucher);
			$_POST['benefit_type'] = $voucher['benefit_type'];
			$_POST['benefit_value'] = $voucher['benefit_value'];
			$_POST['id_affiliate'] = (isset($voucher['affiliate_id'])) ? $voucher['affiliate_id'] : $order['id_affiliate'];
		}
		return $result['status'];
	}

	public function my_order($state_order = 'current')
    {
    	if(!is_logged_in()) {
    		redirect('login?login_required=1');
    	}
		// -------------------------------------
		// Check permission
		// -------------------------------------
		if(! group_has_role('commerce', 'view_all_order') AND ! group_has_role('commerce', 'view_own_order')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			$this->session->set_userdata('redirect_to', $this->uri->uri_string());
			redirect('login');
		}

		$params = array(
			'id_customer'=>$this->current_user->id,
			);

        // -------------------------------------
		// Pagination current order
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'commerce/order/my_order/'.$state_order;
		$pagination_config['uri_segment'] = 5;

		if($state_order == NULL or $state_order == 'current') {
			$pagination_config['total_rows'] = count($this->order_m->get_my_current_order());
		} elseif ($state_order == 'finished') {
			$pagination_config['total_rows'] = count($this->order_m->get_my_finished_order());
		} else {
			show_404();
		}
		$pagination_config['per_page'] = 5;
		$pagination_config['full_tag_open'] = '<ul class="pagination">';
		$pagination_config['full_tag_close'] = '</ul>';
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;

        // -------------------------------------
		// Get entries current order
		// -------------------------------------

		if($state_order == NULL or $state_order == 'current') {
			$data['orders']['entries'] = $this->order_m->get_my_current_order($pagination_config);
		} elseif ($state_order == 'finished') {
			$data['orders']['entries'] = $this->order_m->get_my_finished_order($pagination_config);
		} else {
			show_404();
		}

		$data['orders']['total'] = $pagination_config['total_rows'];
		$data['orders']['pagination'] = $this->pagination->create_links();

		foreach ($data['orders']['entries'] as $key => $order) {
			$cart = $this->cart_m->get_cart_by_order($order['id']);
			$data['orders']['entries'][$key]['cart'] = $cart;
		}

		// Get rekening
		$data['rekening'] = $this->rekening_m->get_rekening(NULL,array('rekening_utama'=>1));

		$data['uri'] = '';
		$data['state_order'] = $state_order;

		// -------------------------------------
		// Build the page.
		// -------------------------------------

		$this->template->title(lang('commerce:order:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('commerce:order:plural'))
			->set_layout('member_area.html')
			->build('order/order_index', $data);
    }

    public function send_invoice($id_order) {
    	// get new current order
		$current_order = $this->order_m->get_order_by_id($id_order);

		// Get rekening
		$email_data['rekening'] = $this->rekening_m->get_rekening(NULL,array('rekening_utama'=>1));

		// send invoice email to customer
		$email_data['order'] = $current_order;
		$email_data['order_product'] = $this->order_m->get_order_product($email_data['order']['id']);
		$email_data['order']['kota'] = $this->kota_m->get_kota_by_id($email_data['order']['id_kota_pengiriman']);

		// check if all product is digital product
		// then remove field 'Pengiriman' and 'Alamat'
		$email_data['all_digital'] = 1;
		foreach ($email_data['order_product'] as $k=>$op) {
			if($op['is_digital_product']==0) {
				$email_data['all_digital'] = $op['is_digital_product'];
				continue;
			}
		}
		$email_data['to_affiliate'] = 0;
		$tagihan_pembayaran = array(
			'subject' => '[INVOICE #'.$email_data['order']['no_invoice'].'] Tagihan dan Petunjuk Pembayaran',
			'body' => $this->load->view('email_template/tagihan_pembayaran',$email_data,true)
			);

		send_email_api($this->current_user->email,$tagihan_pembayaran);
    }

    public function notif_confirm($id_order)
    {
    	// get new current order
		$current_order = $this->order_m->get_order_by_id($id_order);

		// Get rekening
		$email_data['rekening'] = $this->rekening_m->get_rekening(NULL,array('rekening_utama'=>1));

		// send invoice email to customer
		$email_data['order'] = $current_order;
		$email_data['order_product'] = $this->order_m->get_order_product($email_data['order']['id']);
		$email_data['order']['kota'] = $this->kota_m->get_kota_by_id($email_data['order']['id_kota_pengiriman']);

		// check if all product is digital product
		// then remove field 'Pengiriman' and 'Alamat'
		$email_data['all_digital'] = 1;
		foreach ($email_data['order_product'] as $k=>$op) {
			if($op['is_digital_product']==0) {
				$email_data['all_digital'] = $op['is_digital_product'];
				continue;
			}
		}

		$this->load->model('groups/group_m');
		$this->load->model('users/user_m');
        $group_admin = $this->ion_auth->get_group_by_name('site_admin');
        $group_staf = $this->ion_auth->get_group_by_name('staf');

		$users_admin = (isset($group_admin->id)) ? $this->user_m->get_many_by(array('group_id'=>$group_admin->id)) :array();
		$users_staf = (isset($group_staf->id)) ? $this->user_m->get_many_by(array('group_id'=>$group_staf->id)) : array();
		$users = array_merge($users_admin, $users_staf);
		foreach ($users as $user) {
			if($user->email != 'rendy@duakodikartika.com') {
				$recipients[] = $user->email;
			}
		}

		$confirm_order = array(
			'subject' => '[INVOICE #'.$email_data['order']['no_invoice'].'] Telah Dibayar',
			'body' => $this->load->view('email_template/confirm_order',$email_data,true)
			);

		send_email_api($recipients,$confirm_order);
    }
}
