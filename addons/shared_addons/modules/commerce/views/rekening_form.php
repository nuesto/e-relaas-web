<div class="profile profile-edit">
<div class="margin-bottom-20">
	<h1><?php echo lang('commerce:rekening:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri,'class="sky-form"'); ?>
<dl class="dl-horizontal">
	<dt><?php echo lang('commerce:nama_bank'); ?></dt>
	<dd>
		<section>
			<label class="input">
				<?php 
					$value = NULL;
					if($this->input->post('nama_bank') != NULL){
						$value = $this->input->post('nama_bank');
					}elseif($this->input->get('f-nama_bank') != NULL){
						$value = $this->input->get('f-nama_bank');
					}elseif($mode == 'edit'){
						$value = $fields['nama_bank'];
					}
				?>
				<input placeholder="<?php echo lang('commerce:nama_bank'); ?>" name="nama_bank" type="text" value="<?php echo $value; ?>" class="" id="" />
				<b class="tooltip tooltip-bottom-right"><?php echo lang('commerce:nama_bank'); ?></b>
			</label>
		</section>
	</dd>

	<dt><?php echo lang('commerce:atas_nama'); ?></dt>
	<dd>
		<section>
			<label class="input">
				<?php 
					$value = NULL;
					if($this->input->post('atas_nama') != NULL){
						$value = $this->input->post('atas_nama');
					}elseif($this->input->get('f-atas_nama') != NULL){
						$value = $this->input->get('f-atas_nama');
					}elseif($mode == 'edit'){
						$value = $fields['atas_nama'];
					}
				?>
				<input placeholder="<?php echo lang('commerce:atas_nama'); ?>" name="atas_nama" type="text" value="<?php echo $value; ?>" class="" id="" />
				<b class="tooltip tooltip-bottom-right"><?php echo lang('commerce:atas_nama'); ?></b>
			</label>
		</section>
	</dd>
	<dt><?php echo lang('commerce:no_rekening'); ?></dt>
	<dd>
		<section>
			<label class="input">
				<?php 
					$value = NULL;
					if($this->input->post('no_rekening') != NULL){
						$value = $this->input->post('no_rekening');
					}elseif($this->input->get('f-no_rekening') != NULL){
						$value = $this->input->get('f-no_rekening');
					}elseif($mode == 'edit'){
						$value = $fields['no_rekening'];
					}
				?>
				<input placeholder="<?php echo lang('commerce:no_rekening'); ?>" name="no_rekening" type="text" value="<?php echo $value; ?>" class="" id="" />
				<b class="tooltip tooltip-bottom-right"><?php echo lang('commerce:no_rekening'); ?></b>
			</label>
		</section>
	</dd>
</dl>

<div class="">

	<a href="<?php echo site_url($return); ?>" class="btn-u btn-u-default"><?php echo lang('buttons:cancel'); ?></a>
	<button type="submit" class="btn-u"><span><?php echo lang('buttons:save'); ?></span></button>

</div>

<?php echo form_close();?>
</div>