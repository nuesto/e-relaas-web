<div class="page-header">
	<h1>
		<span><?php echo lang('commerce:order:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('commerce/order/index'.$uri); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('commerce:back') ?>
		</a>

		<?php if(group_has_role('commerce', 'edit_all_order')){ ?>
			<a href="<?php echo site_url('commerce/order/edit/'.$order['id'].$uri); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('commerce', 'edit_own_order')){ ?>
			<?php if($order->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('commerce/order/edit/'.$order['id'].$uri); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('commerce', 'delete_all_order')){ ?>
			<a href="<?php echo site_url('commerce/order/delete/'.$order['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('commerce', 'delete_own_order')){ ?>
			<?php if($order->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('commerce/order/delete/'.$order['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $order['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:id_order'); ?></div>
		<?php if(isset($order['id_order'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['id_order']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:no_invoice'); ?></div>
		<?php if(isset($order['no_invoice'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['no_invoice']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:id_customer'); ?></div>
		<?php if(isset($order['id_customer'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['id_customer']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:id_session'); ?></div>
		<?php if(isset($order['id_session'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['id_session']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:status_pesanan'); ?></div>
		<?php if(isset($order['status_pesanan'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['status_pesanan']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:id_affiliate'); ?></div>
		<?php if(isset($order['id_affiliate'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['id_affiliate']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:kode_unik'); ?></div>
		<?php if(isset($order['kode_unik'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['kode_unik']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:tanggal_order'); ?></div>
		<?php if(isset($order['tanggal_order'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['tanggal_order']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:biaya_order'); ?></div>
		<?php if(isset($order['biaya_order'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['biaya_order']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:biaya_pengiriman'); ?></div>
		<?php if(isset($order['biaya_pengiriman'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['biaya_pengiriman']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:alamat_pengiriman'); ?></div>
		<?php if(isset($order['alamat_pengiriman'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['alamat_pengiriman']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:kurir_pengiriman'); ?></div>
		<?php if(isset($order['kurir_pengiriman'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['kurir_pengiriman']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:paket_pengiriman'); ?></div>
		<?php if(isset($order['paket_pengiriman'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['paket_pengiriman']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:nama_penerima'); ?></div>
		<?php if(isset($order['nama_penerima'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['nama_penerima']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:no_hp_penerima'); ?></div>
		<?php if(isset($order['no_hp_penerima'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['no_hp_penerima']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:created'); ?></div>
		<?php if(isset($order['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($order['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:updated'); ?></div>
		<?php if(isset($order['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($order['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($order['created_by'], true); ?></div>
	</div>
</div>