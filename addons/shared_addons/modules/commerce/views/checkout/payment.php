<div class="container">
	<div class="row margin-bottom-30 commerce-tab">
		<?php
		$steps = array(
			'customer_info' => array(
				'tab_title' => 'Informasi Pembeli',
				'tab_description' => 'Data diri',
				'tab_icon' => 'fa-user',
				),
			'shipping_method' => array(
				'tab_title' => 'Metode Pembayaran',
				'tab_description' => 'Voucher dan metode pembayaran',
				'tab_icon' => 'fa-money',
				),
			'payment' => array(
				'tab_title' => 'Pembayaran',
				'tab_description' => 'Petunjuk pembayaran',
				'tab_icon' => 'fa-list-alt',
				),
			);
		?>
		<?php
		// if all products in cart is digital, then remove shipping from steps
		if($all_digital == 1) {
			unset($steps['shipping']);
		}

		$step_number = 1;
		$step_state = 'done';
		foreach ($steps as $key => $step) {
			if($current_step == 'current') {
				$step_state='disabled'; 
			}
			if($current_step == $key) {
				$step_state = 'current';
			}
		?>
		<div class="col-md-4 <?php echo $step_state; ?>">
			<a id="" href="#" style="margin-bottom:20px;" aria-controls=""><span class="current-info audible">step sekarang: </span>
				<span class="number"><?php echo $step_number; ?>.</span> 
				<div class="overflow-h">
					<h2><?php echo $step['tab_title']; ?></h2>
					<p><?php echo $step['tab_description']; ?></p>
					<i class="rounded-x fa <?php echo $step['tab_icon']; ?>"></i>
				</div>
			</a>
		</div>
		<?php $step_number++;} ?>
	</div>
	<hr>

	<div class="page-content">
		<?php
		$sub_total = $current_order['biaya_order']-$current_order['discount_nominal'];
		$sub_total = ($sub_total<0) ? 0 : $sub_total;
		$payment_method = $this->input->post('payment_method');
		if($payment_method == 'transfer') {
		?>
		<div class="top-poc-desc">
			<div class="row">
				<div class="left-text">
					<div class="col-md-6 md-margin-bottom-50">
						<h4 class="pull-left">Pembayaran Via Transfer</h4>
					</div>
				</div>

				<div class="right-text">
					<div class="col-md-6">
						<h4 class="hidden-xs pull-right">No Invoice <b><?php echo $current_order['no_invoice']; ?></b></h4>
						<h4 class="visible-xs">No Invoice <b><?php echo $current_order['no_invoice']; ?></b></h4>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php if(isset($current_order['expired_on'])) { ?>
				<p class="text-center">
					Segera lakukan pembayaran sebelum :
				</p>
				<div class="payment-due-date text-center box-grey-payment">
					<?php
					$expired_time = date('Y-m-d H:i',strtotime($current_order['expired_on']));
					$days = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
					$ex_day = date('w',strtotime($expired_time));
					$ex_date = date('d',strtotime($expired_time));
					$months = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
					$ex_month = date('n',strtotime($expired_time));
					$ex_year = date('Y',strtotime($expired_time));
					$ex_time = date('H:i',strtotime($expired_time));
					?>
					<?php echo 'Hari '.$days[$ex_day].', '.$ex_date.' '.$months[$ex_month-1].' '.$ex_year.' pukul '.$ex_time.' WIB'; ?>
				</div>
				<p></p>
				<?php } ?>
				<p class="text-center">
					Lakukan pembayaran sebesar : 
				</p>
				<div class="payment-value text-center">
					<?php
					$biaya_kirim = (isset($current_order['biaya_pengiriman'])) ? $current_order['biaya_pengiriman'] : 0;
					$sub_total = $current_order['biaya_order']-$current_order['discount_nominal'];
					$sub_total = ($sub_total<0) ? 0 : $sub_total;
					$total_biaya = $sub_total+$biaya_kirim+$current_order['kode_unik'];
					?>
					<?php if(strlen($total_biaya)>3) { ?>
					Rp <?php echo number_format(substr($total_biaya,0,-3),0,',','.'); ?>.<span><?php echo substr($total_biaya, -3); ?></span>
					<?php } else { ?>
					Rp <span><?php echo number_format($total_biaya,0,',','.'); ?></span>
					<?php } ?>
				</div>
				<p></p>
				<p class="text-center">
					<strong>tepat</strong> hingga <strong>3 digit terakhir</strong><br>
					<i>Perbedaan nilai transfer akan menghambat proses verifikasi.</i>
				</p>
			</div>
		</div>

		<div class="box-rekening">
			<div class="row">
				<div class="col-md-12">
					<p class="text-center">Pembayaran dapat dilakukan ke salah satu rekening berikut :</p>
					<div class="bank-list" style="margin-top:30px;">
						<?php
						foreach ($rekening as $key => $rek) { 
							$col = (count($rekening)==1 OR count($rekening)==3) ? 'col-md-4' : 'col-md-3';
							$offset = '';
							if($key==0) {
								if(count($rekening)==1) {
									$offset = 'col-md-offset-4';
								} elseif (count($rekening)==2) {
									$offset = 'col-md-offset-3';
								}
							}
						?>
						<div class="<?php echo $col; echo ' '.$offset; ?> text-center" style="margin-bottom:30px;">
							<img src="{{ url:site }}{{ theme:path }}/img/unify/<?php echo strtolower($rek['nama_bank']); ?>-logo.png" width="150"><br><br>
							
							<strong><?php echo $rek['nama_bank']; ?></strong><br>
							Nomor Rekening:<br>
							<strong><?php echo $rek['no_rekening']; ?></strong><br>
							a.n. <?php echo $rek['atas_nama']; ?>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="bottom-poc-desc">
					<p class="text-center">Jika sudah melakukan pembayaran, silahkan melakukan konfirmasi. Klik tombol konfirmasi dibawah ini :</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<a href="<?php echo base_url().'commerce/order/my_order'; ?>">
					<span class="btn btn-green col-sm-6 col-xs-12 pull-right">Lihat daftar pesanan saya</span>
				</a>			
			</div>
			<div class="col-sm-6">
				<button type="button" class="btn btn-confirmation col-sm-6 col-xs-12" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Mengirim...." id="btn-konfirmation">Konfirmasi Pembayaran</button>
				<div style="margin-bottom:50px;"></div>
			</div>
		</div>
		<?php } elseif($payment_method=='ipaymu') { ?>
		<div class="row">
			<div class="col-sm-12 text-center">
				<?php if( isset($result['url']) ) { ?>
				Anda akan dialihkan ke halaman pembayaran melalui iPaymu dalam <span id="timer">10</span> detik. . .<br>
				klik <a id="ipaymu_url" href="<?php echo $result['url']; ?>">link ini</a> jika Anda tidak dialihkan.
				<?php
				} else {
					echo "Request Error ". $result['Status'] .": ". $result['Keterangan'];
				}
				?>
			</div>
		</div>
		<script>
			var countdown_timer = setInterval(function() {
				var timer = parseInt($('#timer').text());
				if(timer-1==5) {
					window.location.href = $('#ipaymu_url').attr('href');
					if(timer-1==0) {
						clearInterval(countdown_timer);
					}
				} else {
					$('#timer').text(timer-1);
				}
			},1000);
		</script>
		<?php } elseif($payment_method=='return_ipaymu') { ?>
		<div class="row">
			<div class="col-sm-12 text-center">
				<?php if($ipaymu['status']=='berhasil') { ?>
					<h4>Pembayaran Berhasil</h4>
					<p>Terima kasih telah mendaftarkan diri di Sekolah Bisnis Dua Kodi Kartika</p>
				<?php } elseif($ipaymu['status']=='pending') { ?>
					<h4>Pembayaran Pending</h4>
					<p>Silahkan lakukan pembayaran sesuai petunjuk pembayaran yang diberikan oleh iPaymu melalui email iPaymu anda. Terima kasih.</p>
				<?php } ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 text-center">
				<?php if($ipaymu['status']=='berhasil') { ?>
				<a href="<?php echo base_url().'admin/gallery/video/index'; ?>">
					<span class="btn btn-green">Akses video pembelajaran</span>
				</a>
				<?php } ?>
				<a href="<?php echo base_url().'commerce/order/my_order'; ?>">
					<span class="btn btn-green">Lihat daftar pesanan saya</span>
				</a>			
			</div>
		</div>
		<?php } elseif($payment_method=='free') { ?>
		<div class="row">
			<div class="col-sm-12 text-center">
				<h4>Pembayaran Berhasil</h4>
				<p>Terima kasih telah mendaftarkan diri di Sekolah Bisnis Dua Kodi Kartika</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 text-center">
				<a href="<?php echo base_url().'admin/gallery/video/index'; ?>">
					<span class="btn btn-green">Akses video pembelajaran</span>
				</a>
				<a href="<?php echo base_url().'commerce/order/my_order'; ?>">
					<span class="btn btn-green">Lihat daftar pesanan saya</span>
				</a>			
			</div>
		</div>
		<?php } ?>
	</div>
</div>

<script>
	$('#btn-konfirmation').on("click", function() {
		var btn = $(this)
btn.button('loading')
setTimeout(function(){
btn.button('reset')
window.location.href = "<?php echo base_url().'commerce/order/my_order?id_order='.$current_order['id']; ?>";
},3000);
		});
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '<?php echo $this->session->userdata('facebook_pixel_id'); ?>');
fbq('track', 'Purchase', {value:"<?php echo $amount_billed; ?>",currency: 'Rp'});
<?php
if($this->session->userdata('facebook_pixel_id_affiliate') != '') {
	echo "fbq('init', '".$this->session->userdata('facebook_pixel_id_affiliate')."');
	fbq('track', 'Purchase', {value:\"".$amount_billed."\",currency: 'Rp'})";
}
?>
</script>
<!-- End Facebook Pixel Code -->