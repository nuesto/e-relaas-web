<style type="text/css">
	.buyer-info-form {
		min-height: 400px;
	}
	.show-password, .hide-password {
		cursor:pointer;
	}
	.button-container {
		margin-top: 20px;
	}
	.loader-container-top{
		text-align: center;
	}
	.loader-container-right{
		line-height: 50px;
		text-align: left;
	}
	.remove-item {
		position: absolute;
		top: 0px;
		right: 0px;
		background: #cfcfcf;
		border-radius: 50%;
		width: 25px;
		height: 25px;
		text-align: center;
		font-weight: bold;
		padding: 2px;
		cursor: pointer;
	}
	.media-body.id_produk {
		position: relative;
	}
</style>

<div class="container">
	<div class="row margin-bottom-10 commerce-tab">
		<?php
		$steps = array(
			'customer_info' => array(
				'tab_title' => 'Informasi Pembeli',
				'tab_description' => 'Data diri',
				'tab_icon' => 'fa-user',
				),
			'shipping_method' => array(
				'tab_title' => 'Metode Pembayaran',
				'tab_description' => 'Voucher dan metode pembayaran',
				'tab_icon' => 'fa-money',
				),
			'payment' => array(
				'tab_title' => 'Pembayaran',
				'tab_description' => 'Petunjuk pembayaran',
				'tab_icon' => 'fa-list-alt',
				),
			);
		?>
		<?php
		// if all products in cart is digital, then remove shipping from steps
		if($all_digital == 1) {
			unset($steps['shipping']);
		}

		$step_number = 1;
		$step_state = 'done';
		foreach ($steps as $key => $step) {
			if($current_step == $key) {
				$step_state = 'current';
			}
		?>
		<div class="col-md-4 <?php echo $step_state; ?>" style="margin-bottom:20px;">
			<a id="" href="#" aria-controls=""><span class="current-info audible">step sekarang: </span>
				<span class="number"><?php echo $step_number; ?>.</span>
				<div class="overflow-h">
					<h2><?php echo $step['tab_title']; ?></h2>
					<p><?php echo $step['tab_description']; ?></p>
					<i class="rounded-x fa <?php echo $step['tab_icon']; ?>"></i>
				</div>
			</a>
		</div>
		<?php $step_state='disabled'; $step_number++;} ?>
	</div>
	<hr>
	<div id="form-container" class="row tab-content">
		<div class="shopping-cart sky-form">
			<div class="col-md-8 md-margin-bottom-40">
				<h2 class="title-type">Informasi Pembeli</h2>
				{{ theme:partial name='notices' }}
				<?php
				if(!$this->current_user) {
				?>
				<div class="billing-info-inputs buyer-info-form">
					<div class="tab-v1">
						<ul class="nav nav-tabs">
							<li class="active"><a aria-expanded="true" href="#register" data-toggle="tab">Daftar</a></li>
							<li class="">
								<a aria-expanded="false" href="#login" data-toggle="tab">
									Sudah punya akun? <b>Login</b> disini
								</a>
							</li>
						</ul>
						<div class="tab-content">

							<!-- TAB #1 -->

							<!-- REGISTRATION FORM -->

							<div class="tab-pane fade in active" id="register">


								<form name="customer-info" id="form-customer-info" data-order="<?php echo $current_order['id']; ?>" class="shopping-cart sky-form" action="<?php echo site_url('users/register'); ?>" method="post">

									<div class="row">
										<div class="col-sm-12">
											<input id="name" type="text" placeholder="Nama Lengkap" name="display_name" class="form-control required">
										</div>
									</div>

									<div class="row">
										<div class="col-sm-12">
											<input id="email" type="text" placeholder="Email" name="email" class="form-control required email">
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<input id="password" type="password" placeholder="Password" name="password" class="form-control required" />
										</div>
										<div class="col-sm-6">
											<input id="repassword" type="password" placeholder="Retype Password" name="re-password" class="form-control required">
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<input id="no-hp" type="text" placeholder="No HP" name="no_handphone" class="form-control required">
										</div>

										<div class="col-sm-6">
											<label class="select gender">
												<select name="jenis_kelamin">
													<option value="" selected="" disabled="">Jenis Kelamin</option>
													<option value="l">Laki-laki</option>
													<option value="p">Perempuan</option>
												</select>
												<i></i>
											</label>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-12">
											<label class="textarea address">
												<textarea rows="3" class="form-control" placeholder="Alamat Lengkap" name="alamat"></textarea>
											</label>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<label class="select address">
												<select name="id_provinsi" id="provinsi">
													<option value="0" selected="">-- Pilih Provinsi --</option>
													<?php
													foreach ($provinsi as $prov) {
														echo '<option value="'.$prov['id'].'">'.$prov['nama'].'</option>';
													}
													?>
												</select>
												<i></i>
											</label>
										</div>

										<div class="col-sm-6">
											<label class="select address label-kota" style="">
												<select name="id_kota" id="kota">
													<option value="" selected="" disabled="">Kota</option>
												</select>
												<i></i>
											</label>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<input id="zip" type="text" placeholder="Kode POS" name="kode_pos" class="form-control required">
										</div>
									</div>

									<div class="row">
										<div class="col-xs-12 hidden-sm loader-container loader-container-top"></div>
									</div>

									<div class="row">
										<div class="col-xs-12 button-container loader-container-right">
											<input type="hidden" value=" " name="d0ntf1llth1s1n" />
											<input type="submit" class="btn btn-u btn-register col-xs-12 col-sm-4" id="btn-register" value="Buat Akun" />
											<div class="col-sm-4 visible-sm loader-container"></div>
										</div>
									</div>

								</form>

							</div>

							<!-- TAB #2 -->

							<div class="tab-pane fade sky-form" id="login">

								<!-- LOGIN FORM -->

								<div id="login-form-container">
									<form name="login" id="form-login" data-order="<?php echo $current_order['id']; ?>" action="<?php echo site_url('admin/login?redirect=commerce/order/customer_info?order='.$current_order['id']); ?>" method="post">

										<section>
											<label class="input login-input">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-user"></i></span>
													<input placeholder="Email Address" name="email" class="form-control" type="email">
												</div>
											</label>
										</section>

										<section>
											<label class="input login-input no-border-top">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-lock"></i></span>
													<input placeholder="Password" name="password" class="form-control" type="password">
													<span class="input-group-addon show-password">Show</span>
												</div>
											</label>
										</section>

										<section>
											<input type="submit" class="col-xs-4 btn btn-u btn-login" id="btn-login" value="Login" />
											<div class="col-xs-1 loader-container loader-container-right"></div>
										</section>


										<div class="row">
											<div class="col-xs-12">
												<a id="forgot-pass" href="#reset-pass-form-container">Lupa password</a>
											</div>
										</div>

									</form>
								</div>

								<!-- RESET PASS FORM -->

								<div id="reset-pass-form-container" style="display:none;">

									<form name="reset-pass" id="form-reset-pass" action="<?php echo site_url('users/reset_pass'); ?>" method="post">

										<h3></h3>

										<section>
											<label class="input login-input">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-user"></i></span>
													<input placeholder="Email Address" name="email" class="form-control" type="email">
												</div>
											</label>
										</section>

										<section>
											<input type="submit" class="col-xs-6 btn btn-u" id="btn-reset-pass" value="Reset Password" />
											<div class="col-xs-1 loader-container loader-container-right"></div>
										</section>


										<div class="row">
											<div class="col-xs-12">
												<a id="back-login" href="#back-login">Kembali ke form login</a>
											</div>
										</div>

									</form>

								</div>

							</div>
						</div>
					</div>
					<a href="<?php echo base_url();?>uploads/user_guide_daftar_duakodikartika.pdf" target="_blank">Download Panduan Pendaftaran</a>

					<!-- Facebook Pixel Code -->
					<script>
					!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
					n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
					n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
					t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
					document,'script','https://connect.facebook.net/en_US/fbevents.js');

					fbq('init', '<?php echo $this->session->userdata('facebook_pixel_id'); ?>');
					fbq('track', "InitiateCheckout");
					<?php
					if($this->session->userdata('facebook_pixel_id_affiliate') != '') {
						echo "fbq('init', '".$this->session->userdata('facebook_pixel_id_affiliate')."');fbq('track', 'InitiateCheckout')";
					}
					?>
					</script>
					<!-- End Facebook Pixel Code -->

				</div>
				<?php } else { ?>

				<?php $this->load->view('checkout/customer_info_logged_in'); ?>

				<?php } ?>
			</div>

			<div class="col-md-4" id="cart-items">
				<h2 class="title-type">Item Yang Dibeli</h2>
				<div class="billing-info-inputs checkbox-list">
					<ul class="media-list">
						<?php
						if(count($carts)>0) {
							foreach ($carts as $cart) {
						?>
						<li class="media product-item">
							<div class="media-left">
								<a href="#">
									<img src="<?php echo base_url().'uploads/default/files/'.$cart['filename']; ?>" alt="" style="width:80px;">
								</a>
							</div>
							<div class="media-body id_produk" data-idproduk="<?php echo $cart['id_produk']; ?>">
								<span class="product-name media-heading"><strong><?php echo $cart['nama']; ?></strong></span><br>
								<span class="color-orange"><strong>Rp <?php echo number_format($cart['harga'],'0',',','.'); ?></strong></span> /unit<br>
								<input type="hidden" name="price" class="price" value="<?php echo $cart['harga']; ?>">
								<?php
								$hide_quantity = '';
								if($cart['type']!='physical') {
									echo '<span>Jumlah : </span><span class="color-orange"><strong>'.$cart['jumlah'].'</strong> unit</span>';
									$hide_quantity = 'display:none';
								}
								?>
								<div class="product-quantity" style="<?php echo $hide_quantity; ?>">
									<button type="button" class="quantity-button" name="subtract" onclick="minus(this);" value="-">-</button>
									<input class="quantity-field" name="jumlah" data-initval="<?php echo $cart['jumlah']; ?>" value="<?php echo $cart['jumlah']; ?>" id="jumlah" type="text">
									<button type="button" class="quantity-button" name="add" onclick="plus(this);" value="+">+</button>
									<input type="hidden" name="berat" value="<?php echo $cart['berat']; ?>">
								</div>
								<div style="padding-top: 10px; font-size:12px;line-height:15px;">
								<?php if($current_order['order_type']=='membership') : ?>
								<?php echo (isset($cart['additional_info'])) ? $cart['additional_info'] : ''; ?>
								<?php endif; ?>
								</div>
								<?php if(count($carts)>1) { ?>
								<span class="remove-item" title="hapus item">X</span>
								<?php } ?>
							</div>
						</li>
						<?php }
						} ?>

					</ul>
					<div class="action-button" style="margin-top:15px;display:none">
						<!-- <button class="btn-u btn-u-sm btn-u-default btn-reset-quantity" type="button">Reset</button>
						<button class="btn-u btn-u-sm ladda-button btn-update-quantity" data-style="expand-right" type="button">Update Jumlah</button> -->
					</div>
				</div>
			</div>
			<div class="col-md-4 margin-top-20" style="">
				<h2 class="title-type">Affiliate</h2>
				<div class="billing-info-inputs checkbox-list">
					<ul class="list-inline">
						<li style="padding-left:5px;">
							<label for="id_affiliate" class="col-lg-12 control-label" style="font-size: 14px;font-weight: normal;text-align:left">Anda melakukan proses pemesanan <?php echo (isset($current_order['id_affiliate'])) ? 'dengan affiliate '.user_displayname($current_order['id_affiliate']).' - '.$current_order['id_affiliate'] : 'tanpa affiliate'; ?></label>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-4 margin-top-20">
				<h2 class="title-type">Rincian Biaya</h2>
				<div class="billing-info-inputs checkbox-list">
					<ul class="list-inline total-result">
						<li style="padding-left:5px;">
							<h5>Subtotal:</h5>
							<div class="total-result-in" id="sub-total">
								<span>Rp <?php echo number_format($current_order['biaya_order'],0,',','.'); ?></span>
							</div>
						</li>
						<?php if(array_key_exists('shipping', $steps)) { ?>
						<li>
							<h5>Pengiriman:</h5>
							<div class="total-result-in">
								<span class="text-right">- - - -</span>
							</div>
						</li>
						<?php } ?>
						<li>
							<h5>Discount:</h5>
							<div class="total-result-in" id="discount">
								<?php if(isset($current_order['discount_nominal'])) { ?>
									<span>Rp <?php echo number_format($current_order['discount_nominal'],0,',','.'); ?></span>
								<?php } else { ?>
								<span class="text-right">- - - -</span>
								<?php } ?>
							</div>
						</li>
						<li class="divider"></li>
						<li class="total-price">
							<h5>Total:</h5>
							<div class="total-result-in" id="total">
								<?php
								$sub_total = $current_order['biaya_order']-$current_order['discount_nominal'];
								$sub_total = ($sub_total<0) ? 0 : $sub_total;
								$total = $sub_total+$current_order['biaya_pengiriman']+$current_order['kode_unik']; ?>
								<span>Rp <?php echo number_format($total,0,',','.'); ?></span>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="http://malsup.github.com/jquery.form.js"></script>

<script type="text/javascript">

	$(document).ready(function() {

		// Registration form submit

		$('#form-customer-info').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(){
				// disable button
				$('#btn-register').attr("disabled", true);

				// display loader image
				$('.loader-container').prepend('<div class="loader"><img src="{{ url:site }}{{ theme:path }}/img/loading.gif"></div>');
			},
			success: function(retval){
				var order = $('#form-customer-info').data('order');

				// remove loader image
				$('#register .loader').remove();

				// enable button
				$('#btn-register').attr("disabled", false);

				// remove alert
				$('#register .alert').remove();

				// register succeed
				if(retval['status'] == 'success'){
					// remove alert
					$('#register .alert').remove();

					// add success notification
					$('#register').prepend('<div class="alert alert-success">Pendaftaran Berhasil</div>');

					// display loader image in the place of the form
					$('.buyer-info-form').html('<div class="loader"><img src="{{ url:site }}{{ theme:path }}/img/loading.gif"></div>');

					// scroll up
					$('html,body').animate({
						scrollTop: $('#form-container').offset().top
					}, 800);

					// redirect to this page
					window.location.href = '<?php echo base_url(); ?>commerce/order/customer_info?order='+order;

				// register fail
				}else{
					$('#register').prepend('<div class="alert alert-danger">'+retval['message']+'</div>');
					$('#btn-register').removeClass('disabled');
					$('html,body').animate({
						scrollTop: $('#form-container').offset().top
					}, 800);
				}

			}
		});

		// Login form submit

		$('#form-login').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(){
				// disable button
				$('#btn-login').attr("disabled", true);

				// display loader image
				$('#login .loader-container').prepend('<div class="loader"><img src="{{ url:site }}{{ theme:path }}/img/loading.gif"></div>');
			},
			success: function(retval){
				var order = $('#form-login').data('order');

				// remove loader image
				$('#login .loader').remove();

				// enable button
				$('#btn-login').attr("disabled", false);

				// remove alert
				$('#login .alert').remove();

				// register succeed
				if(retval['status'] == 'success'){
					// remove alert
					$('#login .alert').remove();

					// add success notification
					$('#login').prepend('<div class="alert alert-success">Login Berhasil</div>');

					// display loader image in the place of the form
					$('.buyer-info-form').html('<div class="loader"><img src="{{ url:site }}{{ theme:path }}/img/loading.gif"></div>');

					// scroll up
					$('html,body').animate({
						scrollTop: $('#form-container').offset().top
					}, 800);

					// redirect to this page
					window.location.href = '<?php echo base_url(); ?>commerce/order/customer_info?order='+order;

				// register fail
				}else{
					$('#login').prepend('<div class="alert alert-danger">'+retval['message']+'</div>');
					$('#btn-login').removeClass('disabled');
					$('html,body').animate({
						scrollTop: $('#form-container').offset().top
					}, 800);
				}

			}
		});

		// Reset password form submit

		$('#form-reset-pass').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(){
				// disable button
				$('#btn-reset-pass').attr("disabled", true);

				// display loader image
				$('#reset-pass-form-container .loader-container').prepend('<div class="loader"><img src="{{ url:site }}{{ theme:path }}/img/loading.gif"></div>');
			},
			success: function(retval){
				// remove loader image
				$('#reset-pass-form-container .loader').remove();

				// enable button
				$('#btn-reset-pass').attr("disabled", false);

				// remove alert
				$('#reset-pass-form-container .alert').remove();

				// register succeed
				if(retval['status'] == 'success'){
					// remove alert
					$('#reset-pass-form-container .alert').remove();

					// add success notification
					$('#reset-pass-form-container').prepend('<div class="alert alert-success">'+retval['message']+'</div>');

					// scroll up
					$('html,body').animate({
						scrollTop: $('#form-container').offset().top
					}, 800);

				// register fail
				}else{
					$('#reset-pass-form-container').prepend('<div class="alert alert-danger">'+retval['message']+'</div>');
					$('#btn-reset-pass').removeClass('disabled');
					$('html,body').animate({
						scrollTop: $('#form-container').offset().top
					}, 800);
				}

			}
		});

		// Use affiliate code
		$('#form-affiliate-code').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(){
				// remove alert
				$('#form-affiliate-code .alert').remove();

				// disable button
				$('#use-affiliate-code-btn').attr("disabled", true);

				// display loader image
				$('#use-affiliate-code-btn').before('<div class="loader"><img src="{{ url:site }}{{ theme:path }}/img/loading.gif"></div>');
			},
			success: function(retval){
				// enable button
				$('#use-affiliate-code-btn').removeAttr("disabled");

				// remove loader
				$('#form-affiliate-code .loader').remove();

				// display message
				if(retval['status']=='success') {
					$('#use-affiliate-code-btn').before('<div class="alert alert-success">'+retval['message']+'</div>');

					$('#form-affiliate-code input[name=affiliate_code]').attr('readonly',true);
					$('#use-affiliate-code-btn').hide();
					setTimeout(function() {
						$('#form-affiliate-code .alert').remove();
					},2000);
				} else {
					$('#use-affiliate-code-btn').before('<div class="alert alert-danger">'+retval['message']+'</div>');
				}
			}
		});

		// hide/unhide reset pass form

		$(this).on('click','#forgot-pass', function(e) {
			e.preventDefault();
			$('#login-form-container').hide();
			$('#reset-pass-form-container').fadeIn('slow');
		});

		// hide/unhide login form

		$(this).on('click','#back-login', function(e) {
			e.preventDefault();
			$('#reset-pass-form-container').hide();
			$('#login-form-container').fadeIn('slow');
		});

		$('#provinsi').change(function() {
			var id_provinsi = $(this).val();
			$('#kota').html('<option value="">-- Sedang Mengambil Data --</option>');
			$.getJSON('<?php echo base_url(); ?>location/kota/get_kota_by_provinsi/'+id_provinsi, function(result) {
				var choose = '-- Pilih Kota --';
				if(result.length==0) {
					choose = '-- Tidak Ada Kota --'
				}
				$('#kota').html('<option value="">'+choose+'</option>');
				$.each(result, function(key,value) {
					$('#kota').append('<option value="'+value.id+'">'+value.nama+'</option>');
				});
			});
		});

		$('.profile-info').on('click','.btn-signup', function() {
			$('.profile-info .buyer-info-form').html("<div class=\"loader\"><span class=\"fa fa-refresh fa-spin\"></span></div>");
			$.ajax({
				url:'step1_logged_in.html',
				success: function(rst) {
					login.state = true;
					if(login.state === true) {
						$('.profile-info .buyer-info-form .loader').remove();
						$('.profile-info .buyer-info-form').html(rst);
					}
				}
			});
		});



		Ladda.bind('.btn-update-quantity', {
			callback: function( instance ) {
				instance.start();
				$('#sub-total').html('- - - -');
				$('#discount').html('- - - -');
				$('#total').html('- - - -');

				var data = '';
				$('.product-item').each(function(k,elem) {
					$('.action-button .bg-danger').remove();

					var id_produk = $(this).find('.id_produk').data('idproduk');
					var jumlah = $(this).find('.id_produk[data-idproduk='+id_produk+'] .quantity-field').val();
					var harga = $(this).find('.id_produk[data-idproduk='+id_produk+'] .price').val();
					if(data != '') { begin = '&'; } else { begin = ''; };
					data += begin+'id_produk[]='+id_produk
					+'&jumlah[]='+jumlah
					+'&harga[]='+harga
				});

				if(data != '') {
					$.ajax({
						url : '<?php echo base_url(); ?>commerce/cart/update/',
						data : data,
						type : 'POST',
						dataType : 'JSON',
						success : function(rst) {
							if(rst.status=='success') {
								$('.action-button').prepend('<div class="bg-success" style="padding:5px; margin-bottom:10px;">'+rst.message+'</div>');
								setTimeout(function(){
									$('.action-button .bg-success').remove();
									$('.action-button').fadeOut();
								}, 1000);
								var total = parseInt(rst.sub_total.replace(/\./g,''));

								$('#sub-total').html('Rp '+rst.sub_total);
								if(rst.discount_nominal !== undefined) {
									$('#discount').html('Rp '+rst.discount_nominal);

									var sub_total = rst.sub_total;
									var discount = rst.discount_nominal;
									total = parseInt(sub_total.replace(/\./g,'')) - parseInt(discount.replace(/\./g,''));
								}
								$('#total').html('Rp <span>'+total.format(0,3,'.',',')+'</span>');
							} else {
								$('.action-button').prepend('<div class="bg-danger" style="padding:5px; margin-bottom:10px;">'+rst.message+'</div>');
							}

							instance.stop();
						}
					});
				}
				// setTimeout(function(){

				// }, 2000);
			}
		});


		function updateQuantity() {
			$('#sub-total').html('- - - -');
			$('#discount').html('- - - -');
			$('#total').html('- - - -');

			var data = '';
			$('.product-item').each(function(k,elem) {
				$('.action-button .bg-danger').remove();

				var id_produk = $(this).find('.id_produk').data('idproduk');
				var jumlah = $(this).find('.id_produk[data-idproduk='+id_produk+'] .quantity-field').val();
				var harga = $(this).find('.id_produk[data-idproduk='+id_produk+'] .price').val();
				if(data != '') { begin = '&'; } else { begin = ''; };
				data += begin+'id_produk[]='+id_produk
				+'&jumlah[]='+jumlah
				+'&harga[]='+harga
			});

			if(data != '') {
				$.ajax({
					url : '<?php echo base_url(); ?>commerce/cart/update/',
					data : data,
					type : 'POST',
					dataType : 'JSON',
					success : function(rst) {
						if(rst.status=='success') {
							$('.action-button').prepend('<div class="bg-success" style="padding:5px; margin-bottom:10px;">'+rst.message+'</div>');
							setTimeout(function(){
								$('.action-button .bg-success').remove();
								$('.action-button').fadeOut();
							}, 1000);
							var total = parseInt(rst.sub_total.replace(/\./g,''));

							$('#sub-total').html('Rp '+rst.sub_total);
							if(rst.discount_nominal !== undefined) {
								$('#discount').html('Rp '+rst.discount_nominal);

								var sub_total = rst.sub_total;
								var discount = rst.discount_nominal;
								total = parseInt(sub_total.replace(/\./g,'')) - parseInt(discount.replace(/\./g,''));
							}
							$('#total').html('Rp <span>'+total.format(0,3,'.',',')+'</span>');
						} else {
							$('.action-button').prepend('<div class="bg-danger" style="padding:5px; margin-bottom:10px;">'+rst.message+'</div>');
						}
					}
				});
			}
		}

		$(document).ready(function() {
			$('.quantity-button[name=add]').click(function() {
				updateQuantity();
			});
			$('.quantity-button[name=subtract]').click(function() {
				updateQuantity();
			});
		});

		$(document).on('click','.remove-item', function() {
			remove_item(this);
			updateQuantity();
		});

		$(document).on('click','.show-password', function() {
			$(this).text('Hide');
			$(this).removeClass('show-password').addClass('hide-password');
			$(this).closest('label').find('input').attr('type','text');
		});

		$(document).on('click','.hide-password', function() {
			$(this).text('Show');
			$(this).removeClass('hide-password').addClass('show-password');
			$(this).closest('label').find('input').attr('type','password');
		});

		$(document).ready(function() {
			setTimeout(function() {
				$('.alert').remove();
			}, 5000);
		});

	});

	/**
	* Number.prototype.format(n, x, s, c)
	*
	* @param integer n: length of decimal
	* @param integer x: length of whole part
	* @param mixed   s: sections delimiter
	* @param mixed   c: decimal delimiter
	*/
	Number.prototype.format = function(n, x, s, c) {
		var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
		num = this.toFixed(Math.max(0, ~~n));

		return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
	};

</script>
