<div class="container container-sm">
	<div class="headline-center margin-bottom-60">
		<h2>Peringatan</h2>
		<p>Maaf, kami medeteksi bahwa anda melakukan pembelian dari affiliate yang berbeda.<br>
			Silahkan pilih aksi yang anda lakukan pada pesanan sebelumnya
		</p>
		<div class="row">
		<div class="col-sm-6">
			<h4 class="text-center">Pesanan Sebelumnya</h4>
			<p>Affiliate : <?php echo (isset($prev_affiliate) AND $prev_affiliate) ? user_displayname($prev_affiliate,false) : '-'; ?></p>
		</div>
		<div class="col-sm-6">
			<h4 class="text-center">Pesanan Saat ini</h4>
			<p>Affiliate : <?php echo (isset($curr_affiliate) AND $curr_affiliate) ? user_displayname($curr_affiliate,false) : '-'; ?></p>
		</div>
		</div>
		<?php
		$query = array('product'=>$this->input->get('product'),'product_type'=>$this->input->get('product_type'),'affiliate'=>$curr_affiliate);
		$return_url = 'commerce/cart/add?'.http_build_query($query);
		echo anchor(base_url().'commerce/order/cancel/'.$current_order['id'].'?return='.urlencode($return_url),'Batalkan pesanan sebelumnya dan Buat order baru','class=" btn btn-u btn-lg btn-u-default"');
		$query = array('product'=>$this->input->get('product'),'product_type'=>$this->input->get('product_type'),'affiliate'=>$prev_affiliate);
		$return_url = 'commerce/order/customer_info';
		echo anchor(base_url().$return_url,'Lanjutkan pesanan sebelumnya','class=" btn btn-u btn-lg"');
		?>
	</div>
</div>