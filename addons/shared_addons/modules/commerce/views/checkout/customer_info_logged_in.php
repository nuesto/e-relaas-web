<style type="text/css">
	.section-after-login dt {
		text-align: inherit;
		border-right: 2px dotted;
	}
	.btn-step .btn {
		width: 100%;
	}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
	    background-color: #fff;
	    opacity: 1;
	}
</style>
<div class="billing-info-inputs buyer-info-form">
<section class="section-after-login">
	<dl class="dl-horizontal">
		<dt><strong>Nama Lengkap </strong></dt>
		<dd>
			<?php echo (isset($user->display_name) AND $user->display_name!='') ? $user->display_name : '-'; ?>
		</dd>

		<hr>
		<dt><strong>Email </strong></dt>
		<dd>
			<?php echo (isset($user->email) AND $user->email!='') ? $user->email : '-'; ?>
		</dd>

		<hr>
		<dt><strong>No Hp </strong></dt>
		<dd>
			<?php echo (isset($user->no_handphone) AND $user->no_handphone!='') ? $user->no_handphone : '-'; ?>
		</dd>

		<hr>
		<dt><strong>Alamat </strong></dt>
		<dd>
			<?php echo (isset($user->alamat) AND $user->alamat!='') ? $user->alamat : '-'; ?>
		</dd>
		<hr>

		<dt><strong>Kode POS </strong></dt>
		<dd>
			<?php echo (isset($user->kode_pos) AND $user->kode_pos!='') ? $user->kode_pos : '-'; ?>
		</dd>

	</dl>

	<?php
		// get current product ids
		$current_product_ids = array();
		if($current_products != NULL AND is_array($current_products)){
			foreach ($current_products as $idx => $current_product) {
				$current_product_ids[] = $current_product['id_produk'];
			}
		}

		// implode to comma separated
		$current_product_ids_str = implode(",", $current_product_ids);

		$query = array('product'=>$current_product_ids_str,'affiliate'=>$landing_page['uri_segment_string'],'landing_page'=>$current_order['id_landing_page'],'product_type'=>$current_order['order_type']);
		$return_url = 'commerce/cart/add?'.http_build_query($query);
	?>

	<p>Apakah anda ingin masuk dengan akun lain? klik <strong><a style="color:#c13f31;" href="<?php echo base_url().'logout?redirect='.urlencode($return_url); ?>">disini</a></strong></p>
	<p><a href="<?php echo base_url();?>uploads/user_guide_daftar_duakodikartika.pdf" target="_blank">Download Panduan Pendaftaran</a></p>
</section>
<?php if($all_digital == 1) { ?>
<div class="row margin-bottom-10">
	<div class="">
		<?php
		if($next_step != '' or $prev_step != '') {
			if($prev_step!=''){
				echo '<div class="col-sm-6 btn-step">';
				echo anchor(base_url().'commerce/order/'.$prev_step,'Kembali','class="btn btn-u btn-prev btn-u-default"');
				echo '</div>';
			}

			if($next_step!=''){
				$disable_next = (!isset($this->current_user->id)) ? 'disabled' : '';

				$label = 'Lanjutkan';
				if($prev_step != '' OR $next_step == 'payment') {
					$label = 'Beli Sekarang';
				}
				echo '<div class="col-sm-6 btn-step">';
				echo anchor(base_url().'commerce/order/'.$next_step,$label,'class="btn btn-u btn-next '.$disable_next.'"');
				echo '</div>';

			}
		}
		?>						
	</div>
</div>
<?php } ?>
</div>

<?php if($all_digital == 0) : ?>
<p style="margin-top:20px;">*Terdapat produk fisik dalam pesanan anda. silahkan isi data pengiriman di bawah ini.</p>
<h2 class="title-type">Informasi Pengiriman</h2>
<div class="billing-info-inputs buyer-info-form">
	<form class="shopping-cart sky-form" id="form-pengiriman" method="post" action="<?php echo base_url(); ?>commerce/order/tes_shipping">
		<div class="shipment-address margin-bottom-20">
			<p class="shipment-title">Alamat Pengiriman</p>
			<label class="checkbox">
				<input name="my-data" value="1" type="checkbox" id="my-data"><i></i>Gunakan informasi yang sama dengan pembeli
			</label>

			<div class="row">
				<div class="col-sm-6">
					<input type="text" name="nama-penerima" placeholder="Nama" class="form-control form-control" id="form-nama-penerima">
					<input type="text" name="no-hp-penerima" placeholder="No HP" class="form-control form-control" id="form-nohp-penerima">
				</div>
			</div>

			<label class="textarea address">
				<textarea rows="3" name="alamat_pengiriman" class="form-control" placeholder="Alamat Lengkap"></textarea>
			</label>
			<div class="row">
				<div class="col-sm-6">
					<label class="select address">
						<select class="form-control" name="provinsi-pembeli" id="provinsi">
							<option value="" selected="" disabled="">-- Pilih Provinsi --</option>
							<?php
							foreach ($provinsi as $prov) {
								echo '<option value="'.$prov['id'].'">'.$prov['nama'].'</option>';
							}
							?>
						</select>
						<i></i>
					</label>
				</div>
				<div class="col-sm-6">
					<label class="select address label-kota">
						<select class="form-control" id="kota-pengiriman" name="kota-pengiriman">
							<option value="" selected="">-- Pilih Kota --</option>

						</select>
						<i></i>
					</label>
				</div>
				<div class="col-sm-6">
					<input type="text" name="kode-pos-pengiriman" placeholder="Kode Pos" class="form-control form-control" id="form-kode-pos">
				</div>
			</div>
		</div>
		
		<div class="row margin-bottom-10">
			<div class="">
				<?php
				if($next_step != '' or $prev_step != '') {
					if($prev_step!=''){
						echo '<div class="col-sm-6 btn-step">';
						echo anchor(base_url().'commerce/order/'.$prev_step,'Kembali','class="btn btn-u btn-prev btn-u-default"');
						echo '</div>';
					}

					if($next_step!=''){
						$disable_next = (!isset($this->current_user->id)) ? 'disabled' : '';

						$label = 'Lanjutkan';
						if($prev_step != '' OR $next_step == 'payment') {
							$label = 'Beli Sekarang';
						}
						echo '<div class="col-sm-6 btn-step">';
						echo anchor(base_url().'commerce/order/'.$next_step,$label,'id="btn-form-pengiriman" class="btn btn-u btn-next '.$disable_next.'"');
						// echo form_submit('next_step',$label,'class="btn btn-u btn-next '.$disable_next.'" id="btn-form-pengiriman" style="min-height:auto;"');
						echo '</div>';

					}
				}
				?>						
			</div>
		</div>
	</form>

	<script>
		$(document).ready(function() {
			$('#my-data').change(function() {
				if($(this).is(':checked')) {
					$.ajax({
						url:'<?php echo base_url(); ?>users/already_login',
						type:'get',
						dataType:'json',
						success:function(rst) {
							if(rst.status=='success') {
								// set the data
								$('input[name=nama-penerima]').val(rst.message.display_name);
								$('input[name=no-hp-penerima]').val(rst.message.no_handphone);
								$('textarea[name=alamat_pengiriman]').val(rst.message.alamat);
								$('select[name=kota-pengiriman]').val(rst.message.id_kota);
								$('select[name=kota-pengiriman]').attr('data-idkota',rst.message.id_kota);
								get_provinsi_by_kota(rst.message.id_kota);
								$('input[name=kode-pos-pengiriman]').val(rst.message.kode_pos);

								// set to disabled
								$('input[name=nama-penerima]').attr('disabled',true);
								$('input[name=no-hp-penerima]').attr('disabled',true);
								$('textarea[name=alamat_pengiriman]').attr('disabled',true);
								$('select[name=kota-pengiriman]').attr('disabled',true);
								$('select[name=provinsi-pembeli]').attr('disabled',true);
								$('input[name=kode-pos-pengiriman]').attr('disabled',true);
							} else {
								$('#my-data').before('<div class="alert alert-danger">'+rst.message+'</div>');
							}
						},
						timeout:1000,
						error:function(rst) {
							alert('something went wrong when requesting data');
						}
					});
				} else {
					// set to enabled
					$('input[name=nama-penerima]').attr('disabled',false);
					$('input[name=no-hp-penerima]').attr('disabled',false);
					$('textarea[name=alamat_pengiriman]').attr('disabled',false);
					$('select[name=kota-pengiriman]').attr('disabled',false);
					$('select[name=provinsi-pembeli]').attr('disabled',false);
					$('input[name=kode-pos-pengiriman]').attr('disabled',false);
				}
			});

			function get_provinsi_by_kota(id_kota) {
				$.getJSON('<?php echo base_url(); ?>location/kota/get_provinsi_by_kota/'+id_kota, function(result) {
					$.each(result, function(key,value) {
						$('#provinsi').val(value.id);
					});
					$('#provinsi').trigger('change');
				});				
			}

			$('#provinsi').change(function() {
				var id_provinsi = $(this).val();
				$('#kota-pengiriman').html('<option value="">-- Sedang Mengambil Data --</option>');
				$.getJSON('<?php echo base_url(); ?>location/kota/get_kota_by_provinsi/'+id_provinsi, function(result) {
					var choose = '-- Pilih Kota --';
					if(result.length==0) {
						choose = '-- Tidak Ada Kota --'
					}
					$('#kota-pengiriman').html('<option value="">'+choose+'</option>');
					$.each(result, function(key,value) {
						var select_kota = '';
						if($('#my-data').is(':checked') && $('#kota-pengiriman').data('idkota') == value.id) {
							select_kota = 'selected="selected"';
						}
						$('#kota-pengiriman').append('<option value="'+value.id+'" '+select_kota+'>'+value.nama+'</option>');
					});

					$('#kota-pengiriman').trigger('change');
				});
			});

			$('#btn-form-pengiriman').click(function(e) {
				e.preventDefault();
				
				// remove alert
				$('#form-pengiriman .alert').remove();

				data = 'kota-pengiriman='+$('select[name=kota-pengiriman]').val()
				+'&kode-pos-pengiriman='+$('input[name=kode-pos-pengiriman]').val()
				+'&alamat-pengiriman='+$('textarea[name=alamat_pengiriman]').val()
				+'&nama-penerima='+$('input[name=nama-penerima]').val()
				+'&nohp-penerima='+$('input[name=no-hp-penerima]').val()
				+'&courier='+$('#kurir').val()
				+'&pengiriman='+$('select[name=paket-pengiriman]').val()
				+'&biaya-kirim='+$('select[name=paket-pengiriman]').find('option:selected').data('ongkir')
				+'&berat-pengiriman='+hitung_berat();

				if(data != '') {
					$.ajax({
						url : '<?php echo base_url(); ?>commerce/order/shipping/',
						data : data,
						type : 'POST',
						dataType : 'JSON',
						success : function(rst) {
							
							if(rst.status == 'success') {
								window.location.href = $('.btn-next').attr('href');
							} else {
								$('#form-pengiriman').prepend('<div class="alert alert-danger">'+rst.message+'</div>');
								$('html, body').animate({
									scrollTop: $("#form-pengiriman").offset().top
								}, 500);
							}
						}
					});
				}
			});
		
			if($('#kurir').val() != '') {
				$('#kurir').trigger('change');
			}
		
			$('#kurir').change(function() {
				var weight = hitung_berat();
				var destination = $('select[name=kota-pengiriman]').val();
				var courier = $('#kurir').val();

				if($('#alamat-saya').is(':checked')) {
					destination = $('#alamat-saya').data('idkota');
				}

				cek_ongkir(courier,destination,weight);
			});

			$('#kota-pengiriman').change(function() {
				var destination = $(this).val();
				var weight = hitung_berat();
				var courier = $('#kurir').val();

				// cek_ongkir(courier,destination,weight);
			});

			$('select[name=paket-pengiriman]').change(function() {
				var paket = $('select[name=paket-pengiriman]').val();
				var ongkir = $('select[name=paket-pengiriman]').find('option:selected').data('ongkir');
				$('.biaya-kirim').html('Rp '+ongkir.format(0,3,'.',','));
				$('input[name=biaya-kirim]').val(ongkir);
				var total = $('#total span').html();
				if(ongkir == '' || ongkir == undefined) {
					ongkir = 0;
				}

				// clear total text
				// remove currency
				total = total.replace('Rp ','');
				// remove formating
				total = total.replace(/\./g,'');
				var total_ongkir = parseInt(total)+parseInt(ongkir);
				$('#total').html('Rp <span>'+total_ongkir.format(0,3,'.',',')+'</span>');
			});

		});

		function hitung_berat() {
			var weight = 0;
			$('.product-item').each(function() {
				weight += parseInt($(this).find('input[name=berat]').val()) * parseInt($(this).find('.quantity-field').val());
			});
			if(weight > 0 && weight < 1000) {
				weight = 1000;
			}
			return weight;
		}

		function cek_ongkir(kurir,destination,weight) {
			console.log(kurir,destination,weight);
			
			if(kurir == '') {
				// alert('Silahkan pilih kurir penerima dahulu');
				return false;
			}
			if(destination == '' || destination == undefined) {
				// alert('Silahkan pilih kota penerima dahulu');
				return false;
			}
			if(weight<1000) {
				weight = 1000;
			}
			$('select[name=paket-pengiriman]').html('<option value="">-- Sedang ambil data --</option>');
			$('.biaya-kirim').html('- - - -');
			$.ajax({
				url : '<?php echo base_url(); ?>commerce/order/cek_ongkir/',
				data : 'courier='+kurir+'&destination='+destination+'&weight='+weight,
				type : 'GET',
				dataType : 'JSON',
				success : function(rst) {
					$('select[name=paket-pengiriman]').html('');
					var res = rst.rajaongkir.results;
					var val = '';
					$.each(res,function(i,r) {
						$.each(r.costs,function(k, f) {
							if(k<4) {
								var ongkir = f.cost[0].value;
								$('select[name=paket-pengiriman]').append('<option data-ongkir="'+ongkir+'" value="'+f.service+'">'+f.description+' ('+f.service+')</option>');
							}
						});
					});

					$('select[name=paket-pengiriman]').trigger('change');
				}
			});
		}
	</script>
</div>
<?php endif; ?>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '<?php echo $this->session->userdata('facebook_pixel_id'); ?>');
fbq('trackCustom', "AddCustomerInfo");
<?php
if($this->session->userdata('facebook_pixel_id_affiliate') != '') {
	echo "fbq('init', '".$this->session->userdata('facebook_pixel_id_affiliate')."');fbq('trackCustom', 'AddCustomerInfo')";
}
?>
</script>
<!-- End Facebook Pixel Code -->