<style type="text/css">
	.buyer-info-form {
		min-height: 300px;
	}
	.form-control[disabled] {
		background-color: #fff;
	}
	.remove-item {
		position: absolute;
		top: 0px;
		right: 0px;
		background: #cfcfcf;
		border-radius: 50%;
		width: 25px;
		height: 25px;
		text-align: center;
		font-weight: bold;
		padding: 2px;
		cursor: pointer;
	}
	.media-body.id_produk {
		position: relative;
	}
	.btn-step .btn {
		width: 100%;
	}
</style>
<div class="container">
	<div class="row margin-bottom-10 commerce-tab">
		<?php
		$steps = array(
			'customer_info' => array(
				'tab_title' => 'Informasi Pembeli',
				'tab_description' => 'Data diri',
				'tab_icon' => 'fa-user',
				),
			'shipping' => array(
				'tab_title' => 'Pengiriman',
				'tab_description' => 'Alamat dan metode pengiriman',
				'tab_icon' => 'fa-truck',
				),
			'shipping_method' => array(
				'tab_title' => 'Metode Pembayaran',
				'tab_description' => 'Voucher dan metode pembayaran',
				'tab_icon' => 'fa-money',
				),
			'payment' => array(
				'tab_title' => 'Pembayaran',
				'tab_description' => 'Petunjuk pembayaran',
				'tab_icon' => 'fa-list-alt',
				),
			);
		?>
		<?php
		// if all products in cart is digital, then remove shipping from steps
		if($all_digital == 1) {
			unset($steps['shipping']);
		}

		$step_number = 1;
		$step_state = 'done';
		foreach ($steps as $key => $step) {
			if($current_step == $key) {
				$step_state = 'current';
			}
		?>
		<div class="col-md-4 <?php echo $step_state; ?>">
			<a id="" href="#" aria-controls=""><span class="current-info audible">step sekarang: </span>
				<span class="number"><?php echo $step_number; ?>.</span> 
				<div class="overflow-h">
					<h2><?php echo $step['tab_title']; ?></h2>
					<p><?php echo $step['tab_description']; ?></p>
					<i class="rounded-x fa <?php echo $step['tab_icon']; ?>"></i>
				</div>
			</a>
		</div>
		<?php $step_state='disabled'; $step_number++;} ?>
	</div>
	<hr>
	<div class="row tab-content">
			<div class="col-md-8 md-margin-bottom-40">
				<h2 class="title-type">Pengiriman</h2>
				<form class="shopping-cart sky-form">
				<div id="shipping" class="billing-info-inputs">
					<div class="shipment-address margin-bottom-20">
						<p class="shipment-title">Alamat Pengiriman</p>
						<label class="checkbox">
							<?php
							$alamat = '';
							if(isset($user['alamat']) AND $user['alamat'] != '') {
								$alamat = $user['alamat'];
							}
							$provinsikota = '';
							if(isset($user['id_provinsi']) AND $user['id_provinsi'] != '') {
								$provinsikota = $user['id_provinsi'];
							}
							$idkota = '';
							if(isset($user['id_kota']) AND $user['id_kota'] != '') {
								$idkota = $user['id_kota'];
							}
							$kodepos = '-';
							if(isset($user['kode_pos']) AND $user['kode_pos'] != '') {
								$kodepos = $user['kode_pos'];
							}
							?>
							<input name="alamat-saya" value="1" type="checkbox" id="alamat-saya" data-alamat="<?php echo $alamat; ?>" data-provinsikota="<?php echo $provinsikota; ?>" data-idkota="<?php echo $idkota; ?>" data-kodepos="<?php echo $kodepos; ?>"><i></i>Gunakan alamat saya
						</label>
						<label class="textarea address">
							<textarea rows="3" name="alamat_pengiriman" class="form-control" placeholder="Alamat Lengkap"></textarea>
						</label>
						<div class="row">
							<div class="col-sm-6">
								<label class="select address">
									<select class="form-control" name="provinsi-pembeli" id="provinsi">
										<option value="" selected="" disabled="">-- Pilih Provinsi --</option>
										<?php
										foreach ($provinsi as $prov) {
											echo '<option value="'.$prov['id'].'">'.$prov['nama'].'</option>';
										}
										?>
									</select>
									<i></i>
								</label>
							</div>
							<div class="col-sm-6">
								<label class="select address label-kota">
									<select class="form-control" id="kota-pengiriman" name="kota-pengiriman">
										<option value="" selected="">Kota</option>

									</select>
									<i></i>
								</label>
							</div>
							<div class="col-sm-6">
								<input type="text" name="kode-pos-pengiriman" placeholder="Kode Pos" class="form-control form-control" id="form-kode-pos">
							</div>
						</div>
					</div>
					<div class="shipment-receiver margin-bottom-20">
						<p class="shipment-title">Penerima</p>
						<label class="checkbox">
							<?php
							$nama = '';
							if(isset($user['display_name']) AND $user['display_name'] != '') {
								$nama = $user['display_name'];
							}
							$nohandphone = '';
							if(isset($user['no_handphone']) AND $user['no_handphone'] != '') {
								$nohandphone = $user['no_handphone'];
							}
							?>
							<input name="penerima-saya" id="penerima-saya" value="1" type="checkbox" data-nama="<?php echo $nama; ?>" data-nohandphone="<?php echo $nohandphone; ?>"><i></i>Saya adalah penerima
						</label>
						<div class="row">
							<div class="col-sm-6">
								<input type="text" name="nama-penerima" placeholder="Nama" class="form-control form-control" id="form-nama-penerima">
								<input type="text" name="no-hp-penerima" placeholder="No HP" class="form-control form-control" id="form-nohp-penerima">
							</div>
						</div>
					</div>
					<div class="shipment-method margin-bottom-20">
						<p class="shipment-title">Jasa Pengiriman</p>
						<div class="row">
							<div class="col-sm-6">
								<label class="select address">
									<select class="form-control" id="kurir">
										<option value="" selected="">-- Pilih Kurir --</option>
										<?php
										foreach ($pengiriman as $kurir) {
											echo '<option value="'.$kurir['kode_kurir'].'">'.$kurir['nama_kurir'].'</option>';
										}
										?>
									</select>
									<i></i>
								</label>
								<label class="select address">
									<select class="form-control paket" name="paket-pengiriman">
										<option value="" selected="">-- Pilih Kurir Dahulu --</option>
									</select>
									<i></i>
								</label>
							</div>
						</div>
					</div>
				<div class="row margin-bottom-10">
					<div class="">
						<?php
						if($next_step != '' or $prev_step != '') {
							if($prev_step!=''){
								echo '<div class="col-sm-6 btn-step">';
								echo anchor(base_url().'commerce/order/'.$prev_step,'Kembali','class="btn btn-u btn-prev btn-u-default"');
								echo '</div>';
							}

							if($next_step!=''){
								$disable_next = (!isset($this->current_user->id)) ? 'disabled' : '';

								$label = 'Lanjutkan';
								if($prev_step != '' OR $next_step == 'payment') {
									$label = 'Beli Sekarang';
								}
								echo '<div class="col-sm-6 btn-step">';
								echo anchor(base_url().'commerce/order/'.$next_step,$label,'class="btn btn-u btn-next '.$disable_next.'"');
								echo '</div>';

							}
						}
						?>						
					</div>
				</div>
				</div>
				</form>
			</div>

			<div class="col-md-4" id="cart-items">
				<h2 class="title-type">Item Yang Dibeli</h2>
				<div class="billing-info-inputs checkbox-list">
					<ul class="media-list">
						<?php
						if(count($carts)>0) {
							foreach ($carts as $cart) {
						?>
						<li class="media product-item">
							<div class="media-left">
								<a href="#">
									<img src="<?php echo base_url().'uploads/default/files/'.$cart['filename']; ?>" alt="" style="width:80px;">
								</a>
							</div>
							<div class="media-body id_produk" data-idproduk="<?php echo $cart['id_produk']; ?>">
								<span class="product-name media-heading"><strong><?php echo $cart['nama']; ?></strong></span><br>
								<span class="color-orange"><strong>Rp <?php echo number_format($cart['harga'],'0',',','.'); ?></strong></span> /unit<br>
								<?php
								$hide_quantity = '';
								if($cart['is_digital_product']) {
									echo '<span>Jumlah : </span><span class="color-orange"><strong>'.$cart['jumlah'].'</strong> unit</span>';
									$hide_quantity = 'display:none';
								}
								?>
								<div class="product-quantity" style="<?php echo $hide_quantity; ?>">
									<button type="button" class="quantity-button" name="subtract" onclick="minus(this);" value="-">-</button>
									<input class="quantity-field" name="jumlah" data-initval="<?php echo $cart['jumlah']; ?>" value="<?php echo $cart['jumlah']; ?>" id="jumlah" type="text">
									<button type="button" class="quantity-button" name="add" onclick="plus(this);" value="+">+</button>
								</div>
								<div class="product-weight">
									<input data-idproduk="<?php echo $cart['id_produk']; ?>" type="hidden" name="berat" placeholder="" class="form-control weight" id="" style="" value="<?php echo $cart['berat']; ?>">
								</div>
								<?php if(count($carts)>1) { ?>
								<span class="remove-item" title="hapus item" onclick="remove_item(this);">X</span>
								<?php } ?>
							</div>
						</li>
						<?php }
						} ?>
						
					</ul>
					<div class="action-button" style="margin-top:15px;display:none">
						<button class="btn-u btn-u-sm btn-u-default btn-reset-quantity" type="button">Reset</button>
						<button class="btn-u btn-u-sm ladda-button btn-update-quantity" data-style="expand-right" type="button">Update Jumlah</button>
					</div>
				</div>
			</div>
			<div class="col-md-4 margin-top-20" style="display:none">
				<h2 class="title-type">Affiliate</h2>
				<div class="billing-info-inputs checkbox-list">
					<form class="form-horizontal" id="form-affiliate-code" method="post" action="<?php echo base_url(); ?>commerce/order/set_affiliate">
					<ul class="list-inline">
						<li style="padding-left:5px;">
							<div class="form-group" style="margin-bottom:0px;">
								<label for="id_affiliate" class="col-lg-6 control-label" style="font-size: 14px;font-weight: normal;text-align:left">Kode Affiliate</label>
								<div class="col-lg-6">
									<input type="hidden" name="id_order" value="<?php echo $current_order['id']; ?>">
									<?php
									$value = NULL;
									$readonly = NULL;
									if(isset($current_order['id_affiliate']) AND $current_order['id_affiliate']!=0) {
										$value = $current_order['id_affiliate'];
										$readonly = 'readonly="readonly"';
									}elseif($this->session->userdata('id_affiliate') AND $this->session->userdata('id_affiliate') != 0) {
										$value = $this->session->userdata('id_affiliate');
										$readonly = 'readonly="readonly"';
									} else {
									}
									?>
									<input class="form-control" id="id_affiliate" placeholder="" type="text" name="affiliate_code" value="<?php echo $value; ?>" <?php echo $readonly; ?>>
									
								</div>
							</div>
						</li>
					</ul>
					<?php if($current_order['id_affiliate'] == 0 AND $this->session->userdata('id_affiliate') == 0) { ?>
					<input type="submit" value="Gunakan" class="btn btn-u" id="use-affiliate-code-btn">
					<?php } ?>
					</form>
				</div>
			</div>
			<div class="col-md-4 margin-top-20">
				<h2 class="title-type">Rincian Biaya</h2>
				<div class="billing-info-inputs checkbox-list">
					<ul class="list-inline total-result">
						<li style="padding-left:5px;">
							<h5>Subtotal:</h5>
							<div class="total-result-in" id="sub-total">
								<span>Rp <?php echo number_format($current_order['biaya_order'],0,',','.'); ?></span>
							</div>
						</li>
						<?php if(array_key_exists('shipping', $steps)) { ?>
						<li>
							<h5>Pengiriman:</h5>
							<div class="total-result-in">
								<span class="text-right biaya-kirim" id="biaya-kirim">- - - -</span>
								<input type="hidden" name="biaya-kirim">
							</div>
						</li>
						<?php } ?>
						<li class="divider"></li>
						<li class="total-price">
							<h5>Total:</h5>
							<div class="total-result-in" id="total" data-biayaorder="<?php echo $current_order['biaya_order']; ?>">
								<span>Rp <?php echo number_format($current_order['biaya_order'],0,',','.'); ?></span>
							</div>
						</li>
					</ul>
				</div>
			</div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		if($('#provinsi').val() != '') {
			$('#provinsi').trigger('change');
		}

		// Use affiliate code
		$('#form-affiliate-code').ajaxForm({
			dataType: 'json',
			beforeSubmit: function(){
				// remove alert
				$('#form-affiliate-code .alert').remove();

				// disable button
				$('#use-affiliate-code-btn').attr("disabled", true);

				// display loader image
				$('#use-affiliate-code-btn').before('<div class="loader"><img src="{{ url:site }}{{ theme:path }}/img/loading.gif"></div>');
			},
			success: function(retval){
				// enable button
				$('#use-affiliate-code-btn').removeAttr("disabled");

				// remove loader
				$('#form-affiliate-code .loader').remove();

				// display message
				if(retval['status']=='success') {
					$('#use-affiliate-code-btn').before('<div class="alert alert-success">'+retval['message']+'</div>');

					$('#form-affiliate-code input[name=affiliate_code]').attr('readonly',true);
					$('#use-affiliate-code-btn').hide();
					setTimeout(function() {
						$('#form-affiliate-code .alert').remove();
					},2000);
				} else {
					$('#use-affiliate-code-btn').before('<div class="alert alert-danger">'+retval['message']+'</div>');
				}
			}
		});
	});

	$('#provinsi').change(function() {
		var id_provinsi = $(this).val();
		$('#kota-pengiriman').html('<option value="">-- Sedang Mengambil Data --</option>');
		$.getJSON('<?php echo base_url(); ?>location/kota/get_kota_by_provinsi/'+id_provinsi, function(result) {
			var choose = '-- Pilih Kota --';
			if(result.length==0) {
				choose = '-- Tidak Ada Kota --'
			}
			$('#kota-pengiriman').html('<option value="">'+choose+'</option>');
			var select_kota = '';
			$.each(result, function(key,value) {
				if($('#alamat-saya').is(':checked') && $('#alamat-saya').data('idkota') == value.id) {
					select_kota = 'selected="selected"';
				}
				$('#kota-pengiriman').append('<option value="'+value.id+'" '+select_kota+'>'+value.nama+'</option>');
			});
		});
	});

	Ladda.bind('.btn-update-quantity', { 
		callback: function( instance ) {
			instance.start();
			$('#sub-total').html('- - - -');
			$('#biaya-kirim').html('- - - -');
			$('#total').html('- - - -');

			var data = '';
			$('.product-item').each(function(k,elem) {
				$('.action-button .bg-danger').remove();

				var id_produk = $(this).find('.id_produk').data('idproduk');
				var jumlah = $(this).find('.id_produk[data-idproduk='+id_produk+'] .quantity-field').val();
				if(data != '') { begin = '&'; } else { begin = ''; };
				data += begin+'id_produk[]='+id_produk
				+'&jumlah[]='+jumlah

			});

			if(data != '') {
				$.ajax({
					url : '<?php echo base_url(); ?>commerce/cart/update/',
					data : data,
					type : 'POST',
					dataType : 'JSON',
					success : function(rst) {
						if(rst.status=='success') {
							$('.action-button').prepend('<div class="bg-success" style="padding:5px; margin-bottom:10px;">'+rst.message+'</div>');
							setTimeout(function(){ 
								$('.action-button .bg-success').remove();
								$('.action-button').fadeOut();
							}, 1000);
							$('#sub-total').html('Rp '+rst.sub_total);

							var sub_total = parseInt(rst.sub_total.replace(".",""));
							$('#total').attr('data-biayaorder',sub_total);
							$('select[name=paket-pengiriman]').trigger('change');
						} else {
							$('.action-button').prepend('<div class="bg-danger" style="padding:5px; margin-bottom:10px;">'+rst.message+'</div>');
						}

						instance.stop();
					}
				});
			}
			// setTimeout(function(){ 

			// }, 2000);
		}
	});

	$('.cart-items').on('click','.btn-update-quantity',function() {
		
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		if($('#kurir').val() != '') {
			$('#kurir').trigger('change');
		}
	
		$('#kurir').change(function() {
			var weight = hitung_berat();
			var destination = $('select[name=kota-pengiriman]').val();
			var courier = $('#kurir').val();

			if($('#alamat-saya').is(':checked')) {
				destination = $('#alamat-saya').data('idkota');
			}

			cek_ongkir(courier,destination,weight);
		});

		$('#alamat-saya').click(function() {
			if($(this).is(':checked')) {
				var id_kota = $(this).data('idkota');
				$.getJSON('<?php echo base_url(); ?>location/kota/get_provinsi_by_kota/'+id_kota, function(result) {

					$.each(result, function(key,value) {
						$('#provinsi').val(value.id);
						$('#provinsi').trigger('change');
					});

					$('#provinsi').attr('disabled', true);
					$('#kota-pengiriman').attr('disabled', true);
				});

				$('input[name=kode-pos-pengiriman]').val($(this).data('kodepos'));
				$('textarea[name=alamat_pengiriman]').val($(this).data('alamat'));
				$('input[name=kode-pos-pengiriman]').attr('disabled',true);
				$('textarea[name=alamat_pengiriman]').attr('disabled',true);
			} else {
				$('#provinsi-pengiriman').attr('disabled', false);
				$('#kota-pengiriman').attr('disabled', false);
				$('input[name=kode-pos-pengiriman]').attr('disabled',false);
				$('textarea[name=alamat_pengiriman]').attr('disabled',false);
			}
		});

		$('#penerima-saya').click(function() {
			if($(this).is(':checked')) {
				$('input[name=nama-penerima]').val($(this).data('nama'));
				$('input[name=no-hp-penerima]').val($(this).data('nohandphone'));

				$('input[name=nama-penerima]').attr('disabled',true);
				$('input[name=no-hp-penerima]').attr('disabled',true);
			} else {
				$('input[name=nama-penerima]').attr('disabled',false);
				$('input[name=no-hp-penerima]').attr('disabled',false);
			}
		});

		$('#kota-pengiriman').change(function() {
			var destination = $(this).val();
			var weight = hitung_berat();
			var courier = $('#kurir').val();

			cek_ongkir(courier,destination,weight);
		});

		$('select[name=paket-pengiriman]').change(function() {
			var paket = $(this).val();
			var ongkir = $(this).find('option:selected').data('ongkir');
			$('.biaya-kirim').html('Rp '+format_number(ongkir+''));
			$('input[name=biaya-kirim]').val(ongkir);
			var total = $('#total').attr('data-biayaorder');
			if(ongkir == '' || ongkir == undefined) {
				ongkir = 0;
			}
			var total_ongkir = parseInt(total)+parseInt(ongkir);
			$('#total').html('Rp '+format_number(total_ongkir+''));
		});

		function hitung_berat() {
			var weight = 0;
			$('.product-item').each(function() {
				weight += parseInt($(this).find('input[name=berat]').val()) * parseInt($(this).find('.quantity-field').val());
			});
			if(weight > 0 && weight < 1000) {
				weight = 1000;
			}
			return weight;
		}

		function cek_ongkir(kurir,destination,weight) {
			if(kurir == '') {
				// alert('Silahkan pilih kurir penerima dahulu');
				return false;
			}
			if(destination == '' || destination == undefined) {
				// alert('Silahkan pilih kota penerima dahulu');
				return false;
			}
			if(weight<1000) {
				weight = 1000;
			}
			$('select[name=paket-pengiriman]').html('<option value="">-- Sedang ambil data --</option>');
			$('.biaya-kirim').html('- - - -');
			$.ajax({
				url : '<?php echo base_url(); ?>commerce/order/cek_ongkir/',
				data : 'courier='+kurir+'&destination='+destination+'&weight='+weight,
				type : 'GET',
				dataType : 'JSON',
				success : function(rst) {
					$('select[name=paket-pengiriman]').html('');
					var res = rst.rajaongkir.results;
					var val = '';
					$.each(res,function(i,r) {
						$.each(r.costs,function(k, f) {
							if(k<4) {
								var ongkir = f.cost[0].value;
								$('select[name=paket-pengiriman]').append('<option data-ongkir="'+ongkir+'" value="'+f.service+'">'+f.description+' ('+f.service+')</option>');
							}
						});
					});

					$('select[name=paket-pengiriman]').trigger('change');
				}
			});
		}
	});

	function format_number (value) {
		return value
		.replace(/\D/g, "")
		.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		;
	}

	$(document).ready(function() {
		$('.btn-next').click(function(e) {
			e.preventDefault();
			data = 'kota-pengiriman='+$('select[name=kota-pengiriman]').val()
			+'&kode-pos-pengiriman='+$('input[name=kode-pos-pengiriman]').val()
			+'&alamat-pengiriman='+$('textarea[name=alamat_pengiriman]').val()
			+'&nama-penerima='+$('input[name=nama-penerima]').val()
			+'&nohp-penerima='+$('input[name=no-hp-penerima]').val()
			+'&courier='+$('#kurir').val()
			+'&pengiriman='+$('select[name=paket-pengiriman]').val()
			+'&biaya-kirim='+$('input[name=biaya-kirim]').val()
			+'&berat-pengiriman='+hitung_berat();

			if(data != '') {
				$.ajax({
					url : '<?php echo base_url(); ?>commerce/order/shipping/',
					data : data,
					type : 'POST',
					dataType : 'JSON',
					success : function(rst) {
						if(rst.status == 'success') {
							window.location.href = $('.btn-next').attr('href');
						} else {
							$('#shipping').prepend('<div class="alert alert-danger">'+rst.message+'</div>');
							$('html, body').animate({
								scrollTop: $(".tab-content").offset().top
							}, 500);
						}
					}
				});
			}
		});
	});
</script>