<p>Anda belum dapat mengakses Video Pembelajaran. Untuk mengaksesnya, mohon selesaikan proses pemesanan dan pembayaran terlebih dahulu. </p>

<p>Jika Anda sudah melakukan pembayaran, pastikan Anda telah melakukan konfirmasi [<a href="<?php echo base_url(); ?>commerce/order/my_order">di sini</a>]. </p>

<p>Jika invoice Anda sudah expired dan Anda ingin melakukan pembelian ulang, silahkan klik [<a href="<?php echo base_url().'commerce/cart/add?product='.$default_id_product.'&product_type=membership'; ?>">di sini</a>]. </p></p>

<p>Jika Anda telah melakukan konfirmasi, mohon kesediaannya untuk menunggu staf kami memverifikasi pembayaran Anda.</p>

<p>Terima kasih</p>