<div class="page-header">
	<h1>
		<span><?php echo lang('commerce:cart:'.$mode); ?></span>
	</h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_order"><?php echo lang('commerce:id_order'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_order') != NULL){
					$value = $this->input->post('id_order');
				}elseif($mode == 'edit'){
					$value = $fields['id_order'];
				}
			?>
			<input name="id_order" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_produk"><?php echo lang('commerce:id_produk'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_produk') != NULL){
					$value = $this->input->post('id_produk');
				}elseif($mode == 'edit'){
					$value = $fields['id_produk'];
				}
			?>
			<input name="id_produk" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="jumlah"><?php echo lang('commerce:jumlah'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('jumlah') != NULL){
					$value = $this->input->post('jumlah');
				}elseif($mode == 'edit'){
					$value = $fields['jumlah'];
				}
			?>
			<input name="jumlah" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="harga"><?php echo lang('commerce:harga'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('harga') != NULL){
					$value = $this->input->post('harga');
				}elseif($mode == 'edit'){
					$value = $fields['harga'];
				}
			?>
			<input name="harga" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>