<div class="page-header">
	<h1>
		<span><?php echo lang('commerce:payment:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('commerce/payment/index'.$uri); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('commerce:back') ?>
		</a>

		<?php if(group_has_role('commerce', 'edit_all_payment')){ ?>
			<a href="<?php echo site_url('commerce/payment/edit/'.$payment['id'].$uri); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('commerce', 'edit_own_payment')){ ?>
			<?php if($payment->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('commerce/payment/edit/'.$payment['id'].$uri); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('commerce', 'delete_all_payment')){ ?>
			<a href="<?php echo site_url('commerce/payment/delete/'.$payment['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('commerce', 'delete_own_payment')){ ?>
			<?php if($payment->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('commerce/payment/delete/'.$payment['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $payment['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:id'); ?></div>
		<?php if(isset($payment['id'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['id']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:status'); ?></div>
		<?php if(isset($payment['status'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['status']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:id_order'); ?></div>
		<?php if(isset($payment['id_order'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['id_order']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:tanggal_transfer'); ?></div>
		<?php if(isset($payment['tanggal_transfer'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['tanggal_transfer']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:nama_rekening_pengirim'); ?></div>
		<?php if(isset($payment['nama_rekening_pengirim'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['nama_rekening_pengirim']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:bank_rekening_pengirim'); ?></div>
		<?php if(isset($payment['bank_rekening_pengirim'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['bank_rekening_pengirim']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:bank_rekening_tujuan'); ?></div>
		<?php if(isset($payment['bank_rekening_tujuan'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['bank_rekening_tujuan']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:jumlah_pembayaran'); ?></div>
		<?php if(isset($payment['jumlah_pembayaran'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['jumlah_pembayaran']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:keterangan'); ?></div>
		<?php if(isset($payment['keterangan'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['keterangan']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:id_bukti'); ?></div>
		<?php if(isset($payment['id_bukti'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['id_bukti']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:created'); ?></div>
		<?php if(isset($payment['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($payment['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:updated'); ?></div>
		<?php if(isset($payment['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($payment['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($payment['created_by'], true); ?></div>
	</div>
</div>