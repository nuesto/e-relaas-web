<?php
if(!isset($this->current_user->id)) {
?>
<ul id="informasi-diri" class="nav nav-tabs" role="tablist">
	<li><a href="#register-form" role="tab" data-toggle="tab">Daftar</a></li>
	<li><a href="#login-form" role="tab" data-toggle="tab">Login</a></li>
</ul>

<div class="tab-content">
<div class="register-form tab-pane" id="register-form">
	<h3>Sudah memiliki akun? <a href="#login-form">Login</a> Jika belum silahkan mendaftar dengan mengisi form berikut : </h3>
	<div class="form-group">
		<label class="" for="form-email">Email</label>
		<input type="text" name="email" placeholder="" class="form-email form-control" id="form-email">
	</div>
	<div class="form-group">
		<label class="" for="form-password">Password</label>
		<input type="password" name="password" placeholder="" class="form-password form-control" id="form-password">
	</div>
	<div class="form-group">
		<label class="" for="form-password">Retype Password</label>
		<input type="password" name="repassword" placeholder="" class="form-password form-control" id="form-password">
	</div>
	<div class="form-group">
		<label class="" for="form-nama-lengkap">Nama Lengkap</label>
		<input type="text" name="nama-lengkap" placeholder="" class="form-control form-control" id="form-nama-lengkap">
	</div>
	<div class="form-group">
		<label class="" for="form-no-hp">No HP</label>
		<input type="text" name="no-hp" placeholder="" class="form-control" id="form-no-hp">
	</div>
	<div class="form-group">
		<label class="" for="form-jenis-kelamin">Jenis Kelamin</label>
		<label class="radio" style="margin-left:20px;">
			<input type="radio" name="jenis-kelamin" id="l" value="l"> Laki-laki
		</label>
		<label class="radio" style="margin-left:20px;">
			<input type="radio" name="jenis-kelamin" id="p" value="p"> Perempuan
		</label>
	</div>
	<div class="form-group">
		<label class="" for="form-no-hp">Alamat</label>
		<select class="form-control" id="provinsi-pembeli">
			<option>-- Pilih Provinsi --</option>
			<?php
			foreach ($provinsi as $prov) {
				echo '<option value="'.$prov['id'].'">'.$prov['nama'].'</option>';
			}
			?>
		</select>
		<select class="form-control" id="kota-pembeli" name="kota-pembeli">
			<option value="">-- Pilih Kota/Kabupaten --</option>
			
		</select>
		<input type="text" name="kode-pos" placeholder="Kode Pos" class="form-control form-control" id="kode-pos">
		<textarea name="alamat" class="form-control" placeholder="Alamat Lengkap"></textarea>
	</div>

	<div class="form-group">
		<span class="btn" id="btn-register">Daftar</span>
	</div>

	<script type="text/javascript">
		$('#provinsi-pembeli').change(function() {
			var id_provinsi = $(this).val();
			$('#kota-pembeli option:first').html('-- Sedang Mengambil Data --');
			$.getJSON('<?php echo base_url(); ?>location/kota/get_kota_by_provinsi/'+id_provinsi, function(result) {
				var choose = '-- Pilih Kota --';
				if(result.length==0) {
					choose = '-- Tidak Ada Kota --'
				}
				$('#kota-pembeli').html('<option value="">'+choose+'</option>');
				$.each(result, function(key,value) {
					$('#kota-pembeli').append('<option value="'+value.id+'">'+value.nama+'</option>');
				});
			});
		});

		$('#btn-login').click(function() {
			var email = $(this).closest('.login-form').find('input[name=email]').val();
			var password = $(this).closest('.login-form').find('input[name=password]').val();
			$.ajax({
				url : '<?php echo base_url(); ?>admin/login',
				data : 'email='+email+'&password='+password,
				type : 'POST',
				dataType : 'JSON',
				success : function(rst) {
					$('#btn-login').closest('fieldset').find('.error').remove();
					if(rst.status == 'success') {
						$('#btn-login').closest('fieldset').find('.btn-next').click();
					} else {
						$('#btn-login').closest('fieldset').find('.login-form h3').after('<div class="error">'+rst.message+'</div>');
					}
				}
			});
		});

		$('#btn-register').click(function() {
			if($('.loading-text').is(':visible')) {
				return false;
			}
			var email = $(this).closest('.register-form').find('input[name=email]').val();
			var username = $(this).closest('.register-form').find('input[name=email]').val();
			var password = $(this).closest('.register-form').find('input[name=password]').val();
			var repassword = $(this).closest('.register-form').find('input[name=repassword]').val();
			var namalengkap = $(this).closest('.register-form').find('input[name=nama-lengkap]').val();
			var nohp = $(this).closest('.register-form').find('input[name=no-hp]').val();
			var jeniskelamin = $(this).closest('.register-form').find('input[name=jenis-kelamin]:checked').val();
			if(jeniskelamin == undefined) {
				jeniskelamin = '';
			}
			var kotapembeli = $(this).closest('.register-form').find('select[name=kota-pembeli]').val();
			var kodepos = $(this).closest('.register-form').find('input[name=kode-pos]').val();
			var alamat = $(this).closest('.register-form').find('textarea[name=alamat]').val();

			$('#btn-register').after('<label class="loading-text">Sedang proses . . .</label>');

			$.ajax({
				url : '<?php echo base_url(); ?>users/register',
				data : 'email='+email
				+'&username='+username
				+'&password='+password
				+'&re-password='+repassword
				+'&display_name='+namalengkap
				+'&no_handphone='+nohp
				+'&jenis_kelamin='+jeniskelamin
				+'&kota_pembeli='+kotapembeli
				+'&kodepos='+kodepos
				+'&alamat='+alamat
				+'&d0ntf1llth1s1n= ',
				type : 'POST',
				dataType : 'JSON',
				success : function(rst) {
					$('#btn-register').closest('fieldset').find('.error').remove();
					$('#btn-register').closest('fieldset').find('.loading-text').remove();
					if(rst.status == 'success') {
						$('#btn-register').closest('fieldset').find('.btn-next').click();
					} else {
						$('#btn-register').closest('fieldset').find('.register-form h3').after('<div class="error">'+rst.message+'</div>');

						$('html,body').animate({
							scrollTop: $('.register-form').offset().top
						}, 800);
					}
				}
			});
		});
	</script>
</div>

<div class="login-form tab-pane" id="login-form">
	<h3></h3>
	<div class="form-group">
		<label class="" for="form-email">Email</label>
		<input type="text" name="email" placeholder="" class="form-email form-control" id="form-email">
	</div>
	<div class="form-group">
		<label class="" for="form-password">Password</label>
		<input type="password" name="password" placeholder="" class="form-password form-control" id="form-password">
	</div>
	<div class="form-group">
		<span class="btn" id="btn-login">Login</span>
	</div>
</div>

</div>
<?php } else { ?>
	<h4>Anda masuk sebagai : </h4>
	<div class="form-group">
		<label class="col-sm-3" for="form-nama-lengkap">Nama Lengkap</label>
		<?php echo $this->current_user->display_name; ?>
	</div>
	<div class="form-group">
		<label class="col-sm-3" for="form-email">Email</label>
		<?php echo $this->current_user->email; ?>
	</div>
	<p>Apakah anda ingin masuk dengan akun lain? klik <a href="<?php echo base_url(); ?>logout?redirect=commerce/order/create?product=2">disini</a></p>
	<script type="text/javascript">
		$(document).ready(function() {
			$('fieldset[data-step=1] .btn-next').show();
		});
	</script>

<?php } ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#informasi-diri a[href="#register-form"]').tab('show');

		$('#register-form a[href="#login-form"]').click(function(e) {
			e.preventDefault();
			$('#informasi-diri a[href="#login-form"]').tab('show');
		});
	})
</script>