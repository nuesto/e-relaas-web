<div class="page-header">
	<h1>
		<span><?php echo lang('commerce:payment:'.$mode); ?></span>
	</h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id"><?php echo lang('commerce:id'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id') != NULL){
					$value = $this->input->post('id');
				}elseif($mode == 'edit'){
					$value = $fields['id'];
				}
			?>
			<input name="id" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="status"><?php echo lang('commerce:status'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('status') != NULL){
					$value = $this->input->post('status');
				}elseif($mode == 'edit'){
					$value = $fields['status'];
				}
			?>
			<input name="status" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_order"><?php echo lang('commerce:id_order'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_order') != NULL){
					$value = $this->input->post('id_order');
				}elseif($mode == 'edit'){
					$value = $fields['id_order'];
				}
			?>
			<input name="id_order" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tanggal_transfer"><?php echo lang('commerce:tanggal_transfer'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('tanggal_transfer') != NULL){
					$value = $this->input->post('tanggal_transfer');
				}elseif($mode == 'edit'){
					$value = $fields['tanggal_transfer'];
				}
			?>
			<input name="tanggal_transfer" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama_rekening_pengirim"><?php echo lang('commerce:nama_rekening_pengirim'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama_rekening_pengirim') != NULL){
					$value = $this->input->post('nama_rekening_pengirim');
				}elseif($mode == 'edit'){
					$value = $fields['nama_rekening_pengirim'];
				}
			?>
			<input name="nama_rekening_pengirim" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="bank_rekening_pengirim"><?php echo lang('commerce:bank_rekening_pengirim'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('bank_rekening_pengirim') != NULL){
					$value = $this->input->post('bank_rekening_pengirim');
				}elseif($mode == 'edit'){
					$value = $fields['bank_rekening_pengirim'];
				}
			?>
			<input name="bank_rekening_pengirim" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="bank_rekening_tujuan"><?php echo lang('commerce:bank_rekening_tujuan'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('bank_rekening_tujuan') != NULL){
					$value = $this->input->post('bank_rekening_tujuan');
				}elseif($mode == 'edit'){
					$value = $fields['bank_rekening_tujuan'];
				}
			?>
			<input name="bank_rekening_tujuan" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="jumlah_pembayaran"><?php echo lang('commerce:jumlah_pembayaran'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('jumlah_pembayaran') != NULL){
					$value = $this->input->post('jumlah_pembayaran');
				}elseif($mode == 'edit'){
					$value = $fields['jumlah_pembayaran'];
				}
			?>
			<input name="jumlah_pembayaran" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="keterangan"><?php echo lang('commerce:keterangan'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('keterangan') != NULL){
					$value = $this->input->post('keterangan');
				}elseif($mode == 'edit'){
					$value = $fields['keterangan'];
				}
			?>
			<input name="keterangan" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_bukti"><?php echo lang('commerce:id_bukti'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_bukti') != NULL){
					$value = $this->input->post('id_bukti');
				}elseif($mode == 'edit'){
					$value = $fields['id_bukti'];
				}
			?>
			<input name="id_bukti" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>