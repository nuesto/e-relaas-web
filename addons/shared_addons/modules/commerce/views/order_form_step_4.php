<div class="container">
	<div class="row">
		<style type="text/css">
			.payment-due-date {
				background: rgb(212, 212, 212) none repeat scroll 0% 0%
				padding: 10px;
				width: 300px;
				margin: auto;
				overflow-x: auto;
				font-size: 15px;
				font-weight: bold;
				color: rgb(95, 93, 93);
			}
			.payment-value {
				padding: 10px;
				margin: auto;
				overflow-x: auto;
				font-size: 42px;
				font-weight: bold;
				color: rgb(95, 93, 93);
				overflow: hidden;
			}
			.bank-list {
				margin: auto;
			}
		</style>
		<div class="col-sm-12">
			<div class="row">
				<h3 class="pull-left">Pembayaran Via Transfer Bank</h3>
				<h3 class="pull-right">No Invoice <?php echo $order['no_invoice']; ?></h3>
			</div>
			<div class="row">
				<p>
					Transaksi ini dicatat dengan nomor
					<strong><?php echo $order['no_invoice']; ?></strong>
				</p>
				<p class="text-center">
					Segera lakukan pembayaran sebelum
					<div class="payment-due-date text-center">
						<?php
						$array_hari = array(1=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');
						$array_bulan = array(1=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus','September','Oktober', 'November','Desember');
						$hari = $array_hari[date('N',strtotime($order['tanggal_order']))];
						$tanggal = date('j',strtotime($order['tanggal_order']));
						$bulan = $array_bulan[date('n',strtotime($order['tanggal_order']))];
						$tahun = date('Y',strtotime($order['tanggal_order']));
						?>
						hari <?php echo $hari?>, <br>
						<?php echo $tanggal?> <?php echo $bulan?> <?php echo $tahun?> pukul <?php echo date('H:i',strtotime($order['tanggal_order']))?> WIB
					</div>
				</p>
				<p class="text-center">
					Lakukan pembayaran sebesar : 
					<div class="payment-value text-center">
						Rp 
						<?php 
						$total_biaya = $order['biaya_order'] + $order['biaya_pengiriman'] + $order['kode_unik'];
						echo number_format($total_biaya,0,',','.');
						?>
					</div>
				</p>
				<p class="text-center">
					<strong>tepat</strong> hingga <strong>3 digit terakhir</strong><br>
					<i>Perbedaan nilai transfer akan menghambat proses verifikasi.</i>
				</p>
			</div>
			<div class="row">
				<p class="text-center">Pembayaran dapat dilakukan ke salah satu rekening a/n PT Bukalapak.com berikut:</p>
				<div class="bank-list">
					<?php foreach ($rekening as $rek) { ?>
						<div class="col-sm-4 text-center">
							<strong><?php echo $rek['nama_bank']; ?></strong><br>
							Nomor Rekening:<br>
							<strong><?php echo $rek['no_rekening']; ?></strong><br>
							a.n. <?php echo $rek['atas_nama']; ?>
						</div>
					<?php } ?>
				</div>
			</div>
			<div class="row">
				<p class="text-center">Jika sudah melakukan pembayaran, silahkan melakukan konfirmasi. Klik <a href="<?php echo base_url().'commerce/order/confirm?no_invoice='.$order['no_invoice']; ?>">disini</a></p>
			</div>
		</div>
	</div>
</div>