<!-- <link rel="stylesheet" href="{{ url:site }}{{ theme:path }}/plugins/multi-step-wizard/css/form-elements.css"> -->
<link rel="stylesheet" href="{{ url:site }}{{ theme:path }}/plugins/multi-step-wizard/css/style.css">
<style type="text/css">
	.error {
		background: rgba(227, 29, 59, 0.82) none repeat scroll 0% 0%;
		color: #ffffff;
		padding: 0px 10px;
	}


</style>
<form role="form" action="" method="post" class="registration-form">

	<!-- <fieldset data-step="1">
		<div class="form-top">
			<div class="form-top-left">
				<h3>Step 1 / 6</h3>
				<p>Detil Pembelian : </p>
			</div>
			<div class="form-top-right">
				<i class="fa fa-user"></i>
			</div>
		</div>
		<div class="form-bottom">
			<div class="form-content">
			</div>
			<button type="button" class="btn btn-next">Next</button>
		</div>
	</fieldset> -->

	<fieldset data-step="1">
		<div class="form-top">
		
			<ul class="progress-indicator">
				<li class="completed"> <span class="bubble"></span> 1. Informasi Pembeli </li>
				<li> <span class="bubble"></span> 2. Pengiriman dan Pembayaran </li>
				<li> <span class="bubble"></span> 3. Ulasan Pembelian </li>
			</ul>
		</div>
		<div class="col-sm-7">
			<div class="form-bottom">
					<div class="form-content" style="">
					</div>
				<?php if(count($products)>0) { ?>
					<button type="button" style="display:none" class="btn btn-previous">Previous</button>
					<button type="button" style="display:none" class="btn btn-next">Next</button>
				<?php } ?>
			</div>
		</div>
		<div class="col-sm-5">
			<div class="form-bottom">
				<label><h4>Item yang dibeli : </h4></label>
				<?php if(count($products)>0) { ?>
				<div class="form-item-pembelian">
					<?php
					foreach ($products as $product) {
						?>
						<div class="product-item" data-idproduk="<?php echo $product['id']; ?>">
							<img class="" src="<?php echo base_url().'files/thumb/'.$product['id_file'].'/100/150/fit'; ?>">
							<span class="id_produk" data-idproduk="<?php echo $product['id']; ?>" style="display:none"><?php echo $product['id']; ?></span>
							<div class="form-group">
								<label class="" for="form-harga">Harga : <span data-idproduk="<?php echo $product['id']; ?>" id="harga" class="harga"><?php echo $product['harga']; ?></span></label>
							</div>
							<div class="form-group">
								<label class="" for="form-jumlah">Jumlah</label>
								<?php
								$quantity = (isset($order['item'][$product['id']]['jumlah'])) ? $order['item'][$product['id']]['jumlah'] : '1';
								$value = $quantity;
								?>
								<input type="text" data-init-quantity="<?php echo $value; ?>" data-idproduk="<?php echo $product['id']; ?>" name="quantity" placeholder="" class="form-control quantity" id="" style="" value="<?php echo $value; ?>">
								<?php
								$value = (isset($order['item'][$product['id']]['berat'])) ? $order['item'][$product['id']]['berat'] : $product['berat'];
								?>
								<input data-idproduk="<?php echo $product['id']; ?>" type="hidden" name="berat" placeholder="" class="form-control weight" id="" style="" value="<?php echo $value ?>">
							</div>
						</div>
					<?php } ?>
					</div>
				</div>
				<?php } else { ?>
					<div>Product yang anda beli tidak ditemukan</div>
				<?php } ?>
			</div>
		</div>
		<div class="col-sm-5 pull-right" style="margin-top:30px;">
			<div class="form-bottom">
				<label><h4>Rincian Biaya : </h4></label>
				<div class="form-item-pembelian">
						<div class="form-group">
							<label class="" for="form-biaya">Biaya : </label>
							<label class="biaya-item" for="form-biaya"></label>
							<input type="hidden" readonly="readonly" name="biaya" placeholder="" class=" form-control biaya-item-input">
						</div>
						<div class="form-group">
							<label class="" for="form-biaya">Biaya Pengiriman : </label>
							<label class="biaya-kirim" for="form-biaya"></label>
							<input type="hidden" readonly="readonly" name="biaya-kirim" placeholder="" class="form-control biaya-kirim-input">
						</div>
						<script type="text/javascript">
							$(document).ready(function() {
								hitung_biaya();
								$('.quantity').keyup(function() {
									$('input[name=quantity][data-idproduk='+$(this).data('idproduk')+']').val($(this).val());
									$('.cancel-update-order').show();
								});
							});
						</script>
					</div>
				</div>
			</div>
		</div>
	</fieldset>

	<fieldset data-step="2">
		<div class="form-top">
			<ul class="progress-indicator">
				<li class="completed"> <span class="bubble"></span> 1. Informasi Pembeli </li>
				<li class="completed"> <span class="bubble"></span> 2. Pengiriman dan Pembayaran </li>
				<li> <span class="bubble"></span> 3. Ulasan Pembelian </li>
			</ul>
		</div>
		<div class="col-sm-7">
			<div class="form-bottom">
				<div class="form-content" style="">
				</div>

				<button type="button" class="btn btn-previous">Previous</button>
				<button type="button" class="btn btn-next">Next</button>
			</div>
		</div>
		<div class="col-sm-5">
			<div class="form-bottom">
				<label><h4>Item yang dibeli : </h4></label>
				<div class="form-item-pembelian">
					<?php
					foreach ($products as $product) {
						?>
						<div class="product-item" data-idproduk="<?php echo $product['id']; ?>">
							<img class="" src="<?php echo base_url().'files/thumb/'.$product['id_file'].'/100/150/fit'; ?>">
							<span class="id_produk" data-idproduk="<?php echo $product['id']; ?>" style="display:none"><?php echo $product['id']; ?></span>
							<div class="form-group">
								<label class="" for="form-harga">Harga : <span data-idproduk="<?php echo $product['id']; ?>" id="harga" class="harga"><?php echo $product['harga']; ?></span></label>
							</div>
							<div class="form-group">
								<label class="" for="form-jumlah">Jumlah</label>
								<?php
								$quantity = (isset($order['item'][$product['id']]['jumlah'])) ? $order['item'][$product['id']]['jumlah'] : '1';
								$value = $quantity;
								?>
								<input data-init-quantity="<?php echo $value; ?>" data-idproduk="<?php echo $product['id']; ?>" type="text" name="quantity" placeholder="" class="form-control quantity" id="" style="" value="<?php echo $value; ?>">

								<?php
								$value = $quantity * $product['berat'];
								?>
								<input data-idproduk="<?php echo $product['id']; ?>" type="hidden" name="berat" placeholder="" class="form-control weight" id="" style="" value="<?php echo $value ?>">
							</div>
						</div>
					<?php } ?>
						<span class="btn update-order">Ubah Jumlah</span>
						<span class="btn cancel-update-order" style="display:none">Batal Ubah</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-5 pull-right" style="margin-top:30px;">
			<div class="form-bottom">
				<label><h4>Rincian Biaya : </h4></label>
				<div class="form-item-pembelian">
						<div class="form-group">
							<label class="" for="form-biaya">Biaya : </label>
							<label class="biaya-item" for="form-biaya"></label>
							<input type="hidden" readonly="readonly" name="biaya" placeholder="" class=" form-control biaya">
						</div>
						<div class="form-group">
							<label class="" for="form-biaya">Biaya Pengiriman : </label>
							<label class="biaya-kirim" for="form-biaya"></label>
							<input type="hidden" name="biaya-kirim" placeholder="" class="form-control biaya">
						</div>
					</div>
				</div>
			</div>
		</div>
	</fieldset>

	<fieldset data-step="3">
		<div class="form-top">
			<ul class="progress-indicator">
				<li class="completed"> <span class="bubble"></span> 1. Informasi Pembeli </li>
				<li class="completed"> <span class="bubble"></span> 2. Pengiriman dan Pembayaran </li>
				<li class="completed"> <span class="bubble"></span> 3. Instruksi Pembayaran </li>
			</ul>
		</div>
		<div class="form-bottom">
			<div class="form-content" style="">
			</div>

			<!-- <button type="button" class="btn btn-previous">Previous</button>
			<button type="button" class="btn btn-next">Next</button> -->
		</div>
	</fieldset>

</form>

<!-- Javascript -->
<script src="{{ url:site }}{{ theme:path }}/plugins/multi-step-wizard/js/jquery.backstretch.min.js"></script>
<script src="{{ url:site }}{{ theme:path }}/plugins/multi-step-wizard/js/retina-1.1.0.min.js"></script>
<!-- <script src="{{ url:site }}{{ theme:path }}/plugins/multi-step-wizard/js/scripts.js"></script> -->
<script type="text/javascript">
	var current_user = '';

	$(document).ready(function() {
		load_form($('fieldset[data-step=1] .btn-next'),2,function(rst) {
			$('.loading-text').remove();
			$('fieldset[data-step=1]').find('.form-content').html(rst);
		});
	});

	$('#top-navbar-1').on('shown.bs.collapse', function(){
    	$.backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function(){
    	$.backstretch("resize");
    });

    $('.registration-form fieldset:first-child').fadeIn('slow');
    
    $('.registration-form input[type="text"], .registration-form input[type="password"], .registration-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });

    // next step
    $('.registration-form .btn-next').on('click', function() {
    	var parent_fieldset = $(this).parents('fieldset');
    	var next_step = true;
    	var cur_step = parent_fieldset.data('step');

    	if(cur_step==1) {
    		check_user('next');
    	}else if(cur_step==2) {
    		if(check_change_in_quantity()) {
	    		return;
	    	}
    		validate_form(cur_step, function(rst) {
    			parent_fieldset.find('.error').remove();
    			if(rst.status=='success') {
    				parent_fieldset.fadeOut(400, function() {
    					$(this).next().fadeIn();

    					cur_step = $('fieldset:visible').data('step');
    					
    					load_form($('fieldset[data-step='+cur_step+'] .btn-next'),(cur_step+1),function(rst) {
    						$('.loading-text').remove();
    						$('fieldset[data-step='+cur_step+']').find('.form-content').html(rst);
    					});
    				});
    			} else {
    				parent_fieldset.find('.form-content').before('<div class="error">'+rst.message+'</div>');
    				$('html,body').animate({
    					scrollTop: $(parent_fieldset).offset().top
    				}, 800);
    				next_step = false;
    			}
    		});
    	}

    	// if( next_step ) {
    	// 	parent_fieldset.fadeOut(400, function() {
	    // 		$(this).next().fadeIn();
	    // 		check_user('next');
	    // 	});
    	// }
    });
    
    // previous step
    $('.registration-form .btn-previous').on('click', function() {
    	$(this).parents('fieldset').fadeOut(400, function() {
    		$(this).prev().fadeIn();
    		check_user('previous');
    		var cur_step = $('fieldset:visible').data('step');
    		load_form($('fieldset[data-step='+cur_step+'] .btn-next'),cur_step,function(rst) {
    						$('.loading-text').remove();
    						$('fieldset[data-step='+cur_step+']').find('.form-content').html(rst);
    					});
    	});
    });
    
    // submit
    $('.registration-form').on('submit', function(e) {
    	
    	// $(this).find('input[type="text"], input[type="password"], textarea').each(function() {
    	// 	if( $(this).val() == "" ) {
    	// 		e.preventDefault();
    	// 		$(this).addClass('input-error');
    	// 	}
    	// 	else {
    	// 		$(this).removeClass('input-error');
    	// 	}
    	// });
    	
    });

    $('.update-order').click(function() {
    	var biaya = 0;
    	var data = '';
    	$('fieldset:visible .product-item').each(function(k,elem) {
    		$('.form-item-pembelian .error').remove();

    		var id_produk = $(this).find('.id_produk').data('idproduk');
    		var quantity = $(this).find('.quantity[data-idproduk='+id_produk+']').val();
    		var harga = $(this).find('.harga[data-idproduk='+id_produk+']').html();
    		biaya += quantity * harga;
    		if(data != '') { begin = '&'; } else { begin = ''; };
    		data += begin+'id_produk[]='+id_produk
	    		+'&quantity[]='+quantity
	    		+'&harga[]='+harga;

    	});
    	data += '&biaya='+biaya;
		
		if(data != '') {
			$.ajax({
				url : '<?php echo base_url(); ?>commerce/order/validate_form/1',
				data : data,
				type : 'POST',
				dataType : 'JSON',
				success : function(rst) {
					if(rst.status=='failed') {
						$('.update-order').before('<div class="error">'+rst.message+'</div>');
					} else {
						hitung_biaya();
						$('.update-order').before('<div class="error"><p>Jumlah berhasil diperbaharui</p></div>');
						setTimeout(function() {
							$('.form-item-pembelian .error').fadeOut();
						},1000);

						$('fieldset .product-item').each(function(k,elem) {
							var id_produk = $(this).find('.id_produk').data('idproduk');
							var new_quantity = $(this).find('.quantity[data-idproduk='+id_produk+']').val();
							$(this).find('.quantity[data-idproduk='+id_produk+']').data('init-quantity',new_quantity);
						});

						$('.cancel-update-order').hide();
					}
				}
			});
		}
    	
    });

    $('.cancel-update-order').click(function() {
    	$('fieldset:visible .product-item').each(function(k,elem) {
    		$('.form-item-pembelian .error').remove();

    		var id_produk = $(this).find('.id_produk').data('idproduk');
    		var quantity_elem = $(this).find('.quantity[data-idproduk='+id_produk+']');
    		quantity_elem.val(quantity_elem.data('init-quantity'));
    		$('.cancel-update-order').hide();
    	});
    });

    function check_change_in_quantity() {
    	var changed = false;
    	$('fieldset:visible .form-item-pembelian input[name=quantity]').each(function() {
    		var init = $(this).data('init-quantity');
    		var cur = $(this).val();
    		if(init != cur) {
    			alert('Ada perubahan jumlah pesanan. Silahkan klik tombol "Ubah Jumlah" untuk memastikan jumlah pesanan anda.');
    			changed = true;
    			return false;
    		}
    	});
    	return changed;
    }

    function load_form(elem,step,callback) {
    	elem.closest('.form-bottom').find('.form-content').html('<div class="loading-text"><label>Sedang proses . . .</label></div>');

    	$.ajax({
    		url : '<?php echo base_url(); ?>commerce/order/load_step/'+step,
    		type : 'GET',
    		success : callback
    	});
    }

    function validate_form(step, callback) {
    	var data = '';
    	if(step == 1) {
    		data = 'quantity='+$('fieldset[data-step=1] input[name=quantity]').val()
    		+'&id_produk='+$('.id_produk').html()
    		+'&harga='+$('.harga').html()
    		+'&biaya='+$('fieldset:visible .biaya-item').html();
    	}else if(step == 2) {
    		data = 'kota-pengiriman='+$('select[name=kota-pengiriman]').val()
    		+'&kode-pos-pengiriman='+$('input[name=kode-pos-pengiriman]').val()
    		+'&alamat-pengiriman='+$('textarea[name=alamat_pengiriman]').val()
    		+'&nama-penerima='+$('input[name=nama-penerima]').val()
    		+'&nohp-penerima='+$('input[name=no-hp-penerima]').val()
    		+'&courier='+$('#kurir').val()
    		+'&pengiriman='+$('fieldset[data-step=2] select[name=paket-pengiriman]').val()
    		+'&biaya='+$('fieldset:visible .biaya-item').html()
    		+'&biaya-kirim='+$('fieldset:visible input[name=biaya-kirim]').val()
    		+'&berat='+hitung_berat()
    	}

    	if(data != '') {
	    	$.ajax({
	    			url : '<?php echo base_url(); ?>commerce/order/validate_form/'+step,
	    			data : data,
	    			type : 'POST',
	    			dataType : 'JSON',
	    			success : callback
	    		});
	    }
    }

    function hitung_biaya() {
    	var biaya = 0;
    	$('fieldset:visible .product-item').each(function() {
    		var harga = $(this).find('.harga').html();
    		var quantity = $(this).find('.quantity').val();
    		biaya += parseInt(harga) * parseInt(quantity);
    	});
    	
    	$('fieldset').find('.biaya-item').html(biaya);
    	$('fieldset').find('.biaya-item-input').val(biaya);

    	// if(!isNaN(quantity)) {
    	// 	elem.closest('fieldset').find('.biaya-item').html(parseInt(value_before) + (quantity*parseInt(harga)));
    	// 	elem.closest('fieldset').find('.biaya-item-input').val(parseInt(value_before) + (quantity*parseInt(harga)));
    	// } else {
    	// 	elem.closest('fieldset').find('.biaya-item').html(0);
    	// 	elem.closest('fieldset').find('.biaya-item-input').val(0);
    	// }
    }

    function check_user(step) {
    	validate_form(1,function(rst){
    		$('fieldset[data-step=1]').fadeOut(400, function() {
				$(this).next().fadeIn();
				load_form($('fieldset[data-step=2] .btn-next'),3,function(rst) {
					$('.loading-text').remove();
					$('fieldset[data-step=2]').find('.form-content').html(rst);
				});
			});
    	});
    	if($('fieldset[data-step=1]').css('display')=='block') {
    		$.ajax({
    			url : '<?php echo base_url(); ?>admin/already_login',
    			type : 'GET',
    			dataType : 'JSON',
    			success : function(rst) {
    				if(rst.status == 'success') {
    					current_user = rst.message;
    					if(step=='next') {
    						$('fieldset[data-step=1]').fadeOut(400, function() {
    							$(this).next().fadeIn();
    							load_form($('fieldset[data-step=2] .btn-next'),3,function(rst) {
    								$('.loading-text').remove();
    								$('fieldset[data-step=2]').find('.form-content').html(rst);
    							});
    						});
    					} else {
    						$('fieldset[data-step=1]').fadeOut(400, function() {
    							$(this).prev().fadeIn();
    							load_form($('fieldset[data-step=1] .btn-next'),2,function(rst) {
    								$('.loading-text').remove();
    								$('fieldset[data-step=1]').find('.form-content').html(rst);
    							});
    						});
    					}
    				}
    			}
    		})
    	}
    }
</script>