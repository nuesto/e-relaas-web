<div class="form-group">
	<label class="" for="form-no-hp">Alamat Pengiriman</label>
	<div class="checkbox">
		<label>
			<input id="alamat-saya" type="checkbox" value="alamat-saya">
			Sama dengan alamat saya
		</label>
	</div>
	<select class="form-control" id="provinsi-pengiriman">
		<option>-- Pilih Provinsi --</option>
		<?php
		foreach ($provinsi as $prov) {
			echo '<option value="'.$prov['id'].'">'.$prov['nama'].'</option>';
		}
		?>
	</select>
	<select class="form-control" id="kota-pengiriman" name="kota-pengiriman">
		<option value="">-- Pilih Kota/Kabupaten --</option>

	</select>
	<input type="hidden" name="id_kota" value="">
	<input type="text" name="kode-pos-pengiriman" placeholder="Kode Pos" class="form-control form-control" id="form-kode-pos">
	<textarea name="alamat_pengiriman" class="form-control" placeholder="Alamat Lengkap"></textarea>
</div>
<div class="form-group">
	<label class="" for="form-no-hp">Penerima</label>
	<div class="checkbox">
		<label>
			<input id="penerima-saya" type="checkbox" value="penerima-saya">
			Saya adalah penerima
		</label>
	</div>
	<input type="text" name="nama-penerima" placeholder="Nama" class="form-control form-control" id="form-nama-penerima">
	<input type="text" name="no-hp-penerima" placeholder="No HP" class="form-control form-control" id="form-nohp-penerima">
</div>
<div class="form-group">
	<label class="" for="form-no-hp">Jasa Pengiriman</label>
	<?php
	// $jasa_pengiriman = array(
	// 	array('kurir' => 'JNE', 'paket' => array('Reguler','OKE','YES')),
	// 	array('kurir' => 'POS Indonesia', 'paket' => array('Kilat Khusus','Kilat Biasa')),
	// 	);
		?>
		<select class="form-control" id="kurir">
			<option value="">-- Pilih Kurir --</option>
			<?php
			foreach ($pengiriman as $kurir) {
				echo '<option value="'.$kurir['kode_kurir'].'">'.$kurir['nama_kurir'].'</option>';
			}
			?>
		</select>
		<select class="form-control paket" name="paket-pengiriman">
			<option value=""></option>
		</select>
		
		<script type="text/javascript">
			$(document).ready(function() {
				var ongkir = '';

				$('#kurir').change(function() {
					var weight = hitung_berat();
					var destination = $('input[name=id_kota]').val();
					var courier = $('#kurir').val();

					cek_ongkir(courier,destination,weight);
				});

				$('select[name=paket-pengiriman]').change(function() {
					var paket = $(this).val();
					var ongkir = $(this).find('option:selected').data('ongkir');
					$('fieldset[data-step=2] .biaya-kirim').html(ongkir);
					$('fieldset[data-step=2] input[name=biaya-kirim]').val(ongkir);
				});

				if($('fieldset[data-step=4]').css('display')=='block') {
					$('#provinsi-pengiriman option:first').html('-- Sedang Mengambil Data --');
					$.getJSON('<?php echo base_url(); ?>location/provinsi/get_all_provinsi/', function(result) {
						var choose = '-- Pilih Provinsi --';
						if(result.length==0) {
							choose = '-- Tidak Ada Provinsi --'
						}
						$('#provinsi-pengiriman').html('<option value="">'+choose+'</option>');
						$.each(result, function(key,value) {
							$('#provinsi-pengiriman').append('<option value="'+value.id+'">'+value.nama+'</option>');
						});
					});
				}

				$('#provinsi-pengiriman').change(function() {
					var id_provinsi = $(this).val();
					$('#kota-pengiriman option:first').html('-- Sedang Mengambil Data --');
					$('input[name=id_kota]').val('');

					$.getJSON('<?php echo base_url(); ?>location/kota/get_kota_by_provinsi/'+id_provinsi, function(result) {
						var choose = '-- Pilih Kota --';
						if(result.length==0) {
							choose = '-- Tidak Ada Kota --'
						}
						$('#kota-pengiriman').html('<option value="">'+choose+'</option>');
						$.each(result, function(key,value) {
							if(current_user.id_kota != undefined && current_user.id_kota == value.id) {
								$('#kota-pengiriman').append('<option selected="selected" value="'+value.id+'">'+value.nama+'</option>');
							} else {
								$('#kota-pengiriman').append('<option value="'+value.id+'">'+value.nama+'</option>');
							}
						});
						if($('#kota-pengiriman option:selected').val() != '') {
							$('#kota-pengiriman').trigger('change');
						}

					});
				});

				$('#alamat-saya').click(function() {
					if($(this).is(':checked')) {
						var id_kota = current_user.id_kota;
						$.getJSON('<?php echo base_url(); ?>location/kota/get_provinsi_by_kota/'+id_kota, function(result) {

							$.each(result, function(key,value) {
								$('#provinsi-pengiriman').val(value.id);
								$('#provinsi-pengiriman').trigger('change');
							});

							$('#provinsi-pengiriman').attr('disabled', true);
							$('#kota-pengiriman').attr('disabled', true);
						});

						$('input[name=kode-pos-pengiriman]').val(current_user.code_pos);
						$('textarea[name=alamat_pengiriman]').val(current_user.alamat);
						$('input[name=kode-pos-pengiriman]').attr('disabled',true);
						$('textarea[name=alamat_pengiriman]').attr('disabled',true);
					} else {
						$('#provinsi-pengiriman').attr('disabled', false);
						$('#kota-pengiriman').attr('disabled', false);
						$('input[name=kode-pos-pengiriman]').attr('disabled',false);
						$('textarea[name=alamat_pengiriman]').attr('disabled',false);
					}
				});

				$('#penerima-saya').click(function() {
					if($(this).is(':checked')) {
						$('input[name=nama-penerima]').val(current_user.display_name);
						$('input[name=no-hp-penerima]').val(current_user.no_handphone);

						$('input[name=nama-penerima]').attr('disabled',true);
						$('input[name=no-hp-penerima]').attr('disabled',true);
					} else {
						$('input[name=nama-penerima]').attr('disabled',false);
						$('input[name=no-hp-penerima]').attr('disabled',false);
					}
				});

				$('#kota-pengiriman').change(function() {
					var destination = $('input[name=id_kota]').val($(this).val());
					var weight = hitung_berat();
					var courier = $('#kurir').val();

					cek_ongkir(courier,$(this).val(),weight);
				});

				$.ajax({
	    			url : '<?php echo base_url(); ?>admin/already_login',
	    			type : 'GET',
	    			dataType : 'JSON',
	    			success : function(rst) {
	    				if(rst.status == 'success') {
	    					current_user = rst.message;
	    				}
	    			}
	    		});
			});
			
			function hitung_berat() {
				var weight = 0;
				$('fieldset:visible .product-item').each(function() {
					weight += parseInt($(this).find('input[name=berat]').val()) * parseInt($(this).find('.quantity').val());
				});
				if(weight < 1000) {
					weight = 1000;
				}
				return weight;
			}

			function cek_ongkir(kurir,destination,weight) {
				if(destination == '') {
					current_user.id_kota;
				}
				if(weight<1000) {
					weight = 1000;
				}
				$('select[name=paket-pengiriman]').html('<option value="">-- Sedang ambil data --</option>');
				$.ajax({
	    			url : '<?php echo base_url(); ?>commerce/order/cek_ongkir/',
	    			data : 'courier='+kurir+'&destination='+destination+'&weight='+weight,
	    			type : 'GET',
	    			dataType : 'JSON',
	    			success : function(rst) {
	    				$('select[name=paket-pengiriman]').html('');
	    				var res = rst.rajaongkir.results;
	    				$.each(res,function(i,r) {
	    					$.each(r.costs,function(k, f) {
	    						if(k<4) {
	    							var ongkir = f.cost[0].value;
	    							$('select[name=paket-pengiriman]').append('<option data-ongkir="'+ongkir+'" value="'+f.service+'">'+f.description+' ('+f.service+')</option>');
	    						}
	    					});
	    				});

	    				$('select[name=paket-pengiriman]').trigger('change');
	    			}
	    		});
			}
		</script>
	</div>