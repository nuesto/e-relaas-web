<!DOCTYPE html>
<html>
<head>
	<title>Email</title>
</head>
<body>
Assalamu'alaikum warahamatullahi wabarakatuh...<br>
Salam Pertumbuhan....<br>
<br>
Dear Member SBDKK,<br>
Semoga selalu dalam semangat positif dan kondisi yang terus bertumbuh.<br>
<br>
Sahabat, beberapa bulan terakahir Saya bertemu dengan salah satu sosok hebat yang padat ilmu dan pengalaman. Beliau adalah Kang Lukman Sop, owner LSE Herba. Beliau sangat menguasai keilmuwan dalam membangun sistem di perusahaan.<br>
<br>
Pengalaman beliau selama di perusahaan Jepang membuatnya sangat memahami dan menjiwai akan arti sebuah sistem. Gaya pembahasan yang otentik dan alami, merupakan salah satu kekuatannya. Beliau juga sangat mengerti kasus-kasus yang sering terjadi.<br>
<br>
KeKe Busana pun telah merasakan manfaatnya. Dan puluhan workshop public telah beliau adakan. Hampir kesemua workshop yang beliau adakan penuh dan selalu menolak peserta.<br>
<br>
Beberapa pekan yang lalu, Saya menawarkan Kang Lukman untuk berkenan membangun platform pembelajaran bersama SBDKK. Keilmuwan beliau tentu sangat berharga dan harus terus menerus meluas.<br>
<br>
Belajar dari SBDKK, pembelajaran online ternyata dapat diterima dengan baik. 3500 lebih member SBDKK terus membersamai platform belajar ini.<br>
<br>
Bismillah, mengawali dengan Asma Allah, Kami hadirkan BelajarSOP.com, platform belajar bagaimana membangun Standard Operasional Prosedur (SOP) yang mapan dan tepat. <br>
<br>
InsyaAllah kami akan menyediakan 50 track episode untuk platform ini. Kami telah meletakkan 4 episode di platform dan akan menguploadnya setiap selasa dan kamis. Dimulai tanggal 15 desember hari kamis.<br>
<br>
Investasinya Rp 495.000,00<br>
Khusus member SBDKK, diskon 60% hingga tanggal 8 desember. <br>
Gunakan kupon SBDKK60OFF <br>
<br>
Dalam 12 jam kedepan, kami hanya melaunch belajarSOP.com hanya kepada member SBDKK. Maka segeralah lakukan pendaftaran dan pembayaran.<br>
<br>
Hubungi Teh Isti Wulandari di WA 081572214566 jika Anda mengalami kesulitan pendaftaran.<br>
<br>
Hatur nuhun...<br>
Terima Kasih...<br>
<br>
Semoga semangat belajar terus tumbuh, dan industri edukasi terus menerus melesat hebat. <br>
<br>
Kita harus berani melakukan terobosan, bahwa pelatihan kompetensi yang harusnya mahal, harus bisa diubah agar dapat dijangkau lebih murah.<br>
<br>
Pelajari lebih dalam produknya ke http://belajarSOP.com<br>
<br>
<br>
Rendy Saputra,<br>
Pengajar SBDKK<br>
</body>
</html>
