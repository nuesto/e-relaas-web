<html>
<title>[INVOICE #<?php echo $order['no_invoice']; ?>] Maaf, pembayaran anda kami tolak</title>
<body>
	<h3>[INVOICE #<?php echo $order['no_invoice']; ?>] Maaf, pembayaran anda kami tolak</h3>
	<p style="color:#000000">
	Hai, <?php echo user_displayname($order['id_customer'],false); ?><br>
	Mohon maaf, pembayaran atas pesanan dengan No Invoice #<?php echo $order['no_invoice']; ?> kami tolak. Segera hubungi CS kami jika anda sudah melakukan pembayaran dengan benar ataupun untuk melakukan konfirmasi ulang.
	</p>
	<p style="color:#000000">Kontak CS : <br>
	- (xxx) - xxx xxx
	- (yyy) - yyy yyy
	</p>

	<p style="color:#000000">Terima kasih</p>

	<p style="color:#000000">Rincian pembelian : </p>
	<table style="width:100%;border-collapse:collapse">
		<thead>
			<tr style="text-align:center; background-color:#e31d3b; color:#ffffff;">
				<td colspan="2">No Invoice #<?php echo $order['no_invoice']; ?></td>
			</tr>
		</thead>
		<tbody>
			<tr style="border-bottom:solid 1px">
				<td>Waktu Transaksi</td>
				<td><?php echo date('d-m-Y H:i',strtotime($order['tanggal_order'])); ?></td>
			</tr>
			<tr style="border-bottom:solid 1px">
				<td>Pembeli</td>
				<td><?php echo user_displayname($order['id_customer'],false); ?></td>
			</tr>
			<?php if($all_digital == 0) { ?>
			<tr style="border-bottom:solid 1px">
				<td>Pengiriman</td>
				<td><?php echo strtoupper($order['kurir_pengiriman']).' '.$order['paket_pengiriman'] ?></td>
			</tr>
			<?php } ?>
			<tr style="border-bottom:solid 1px">
				<td>Metode Pembayaran</td>
				<td>Transfer</td>
			</tr>
		</tbody>
	</table>
	<br>
	<table style="width:100%;border-collapse:collapse;">
		<thead>
			<tr><td colspan="3"></td></tr>
			<tr style="text-align:center; background-color:#e31d3b; color:#ffffff;">
				<th>Nama Barang</th>
				<th>Jumlah</th>
				<th>Harga</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($order_product as $op) { ?>
			<tr style="border-bottom:solid 1px">
				<td><strong><?php echo $op['nama']; ?></strong></td>
				<td style="text-align:center"><?php echo $op['jumlah']; ?></td>
				<td style="text-align:right"><?php echo 'Rp'.number_format($op['harga'],0,',','.'); ?></td>
			</tr>
			<?php } ?>
			<tr style="border-bottom:solid 1px">
				<td colspan="2">Total Biaya</td>
				<td style="text-align:right"><?php echo 'Rp'.number_format($order['biaya_order'],0,',','.'); ?></td>
			</tr>
			<?php if($all_digital == 0) { ?>
			<tr style="border-bottom:solid 1px">
				<td colspan="2">Biaya Kirim</td>
				<td style="text-align:right"><?php echo 'Rp'.number_format($order['biaya_pengiriman'],0,',','.'); ?></td>
			</tr>
			<?php } ?>
			<?php if($order['discount_nominal'] != '') { ?>
			<tr style="border-bottom:solid 1px;">
				<td style="border-right:solid 1px;" colspan="2">Discount</td>
				<td style="text-align:right"><?php echo 'Rp'.number_format($order['discount_nominal'],0,',','.'); ?></td>
			</tr>
			<?php } ?>
			<tr style="border-bottom:solid 1px">
				<td colspan="2">Kode Unik</td>
				<td style="text-align:right"><?php echo $order['kode_unik']; ?></td>
			</tr>
			<tr style="border-bottom:solid 1px">
				<td colspan="2">Total Pembayaran</td>
				<td style="text-align:right">
				<?php 
				$sub_total = $order['biaya_order']-$order['discount_nominal'];
				$sub_total = ($sub_total<0) ? 0 : $sub_total;
				$total = $sub_total + $order['biaya_pengiriman'] + $order['kode_unik'];
				echo 'Rp '.number_format($total,0,',','.'); 
				?>
				</td>
			</tr>
		</tbody>
	</table>
	
	<p style="color:#000000">
		<strong>Alamat Tujuan Pengiriman</strong>
		<?php echo $order['nama_penerima']; ?><br>
		<?php echo $order['alamat_pengiriman']; ?><br>
		<?php echo $order['kota']['nama']; ?><br>
		<?php echo $order['no_hp_penerima']; ?><br>
	</p>
</body>
</html>