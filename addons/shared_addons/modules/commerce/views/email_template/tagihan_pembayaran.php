<html>
<body>
	<h3>Tagihan dan Petunjuk Pembayaran [INVOICE #<?php echo $order['no_invoice']; ?>]</h3>
	<p style="color:#000000">
	Hai, <?php echo user_displayname($order['id_customer'],false); ?><br>
	Terima kasih sudah bertransaksi melalui <?php echo base_url(); ?>
	</p>

	<table style="width:100%;border-collapse:collapse">
		<tbody>
			<tr >
				<td>No Invoice</td>
				<td><?php echo $order['no_invoice']; ?></td>
			</tr>
			<tr >
				<td>Pembeli</td>
				<td><?php echo user_displayname($order['id_customer'],false); ?></td>
			</tr>
			<tr >
				<td>Total Pembayaran</td>
				<td>
				<?php 
				$sub_total = $order['biaya_order']-$order['discount_nominal'];
				$sub_total = ($sub_total<0) ? 0 : $sub_total;
				$total = $sub_total + $order['biaya_pengiriman'] + $order['kode_unik'];
				echo 'Rp '.number_format($total,0,',','.'); 
				?>
				</td>
			</tr>
		</tbody>
	</table>
	
	<p style="color:#000000">Silahkan melakukan pembayaran ke salah satu rekening berikut : </p>
	<?php foreach ($rekening as $rek) { ?>
		===================================<br>
		Bank : <strong><?php echo $rek['nama_bank']; ?></strong><br>
		Nomor Rekening: <strong><?php echo $rek['no_rekening']; ?></strong><br>
		a.n. : <strong><?php echo $rek['atas_nama']; ?></strong><br>
		===================================<br>
	<?php } ?>

	<?php
	if(isset($order['expired_on'])) {
		$expired_time = date('Y-m-d H:i',strtotime($order['expired_on']));
		$days = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
		$ex_day = date('w',strtotime($expired_time));
		$ex_date = date('d',strtotime($expired_time));
		$months = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$ex_month = date('n',strtotime($expired_time));
		$ex_year = date('Y',strtotime($expired_time));
		$ex_time = date('H:i',strtotime($expired_time));
	}
	?>


	<p style="color:#000000">Mohon bayar <strong>TEPAT</strong> hingga 3 digit terakhir.</p>
	<?php if(isset($order['expired_on'])) { ?>
	<p>Mohon LAKUKAN PEMBAYARAN sebelum <strong><?php echo 'Hari '.$days[$ex_day].', '.$ex_date.' '.$months[$ex_month-1].' '.$ex_year.' pukul '.$ex_time.' WIB'; ?></strong> setelah invoice ini terbit. Setelah itu, Invoice akan hangus dan jika Anda melakukan transfer setelah Invoice hangus, kami akan me-refund Uang Anda</p>
	<?php } ?>

	<p style="color:#000000">
		Setelah melakukan pembayaran, diharapkan untuk melakukan konfirmasi dengan mengunjungi link berikut <br>
		<a href="<?php echo base_url().'commerce/order/my_order?id_order='.$order['id']; ?>">Halaman konfirmasi</a><br>
		atau copy link berikut pada browser<br>
		<?php echo base_url().'commerce/order/my_order?id_order='.$order['id']; ?>
	</p>
	<p style="color:#000000">
		Untuk memantau status transaksi kamu, silahkan kunjungi halaman berikut : <br>
		<a href="<?php echo base_url().'commerce/order/my_order'; ?>">Halaman daftar pesanan</a>
	</p>

	<p style="color:#000000">Rincian pembelian : </p>
	<table style="width:100%;border-collapse:collapse">
		<thead>
			<tr style="text-align:center; background-color:#e31d3b; color:#ffffff;">
				<td colspan="2">No Invoice #<?php echo $order['no_invoice']; ?></td>
			</tr>
		</thead>
		<tbody>
			<tr style="border-bottom:solid 1px">
				<td>Waktu Pemesanan</td>
				<td><?php echo date('d-m-Y H:i',strtotime($order['tanggal_order'])); ?></td>
			</tr>
			<tr style="border-bottom:solid 1px">
				<td>Pembeli</td>
				<td><?php echo user_displayname($order['id_customer'],false); ?></td>
			</tr>
			<?php if($all_digital == 0) { ?>
			<tr style="border-bottom:solid 1px">
				<td>Pengiriman</td>
				<td><?php echo strtoupper($order['kurir_pengiriman']).' '.$order['paket_pengiriman'] ?></td>
			</tr>
			<?php } ?>
			<tr style="border-bottom:solid 1px">
				<td>Metode Pembayaran</td>
				<td>Transfer</td>
			</tr>
		</tbody>
	</table>
	<br>
	<table style="width:100%;border-collapse:collapse;">
		<thead>
			<tr><td colspan="3"></td></tr>
			<tr style="text-align:center; background-color:#e31d3b; color:#ffffff;">
				<th style="text-align:center;"><?php echo lang('commerce:nama'); ?></th>
				<th style="text-align:center;"><?php echo lang('commerce:jumlah'); ?></th>
				<th style="text-align:center;"><?php echo lang('commerce:harga'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($order_product as $op) { ?>
			<tr style="border-bottom:solid 1px">
				<td style="border-right:solid 1px;"><strong><?php echo $op['nama']; ?></strong></td>
				<td style="border-right:solid 1px;text-align:center"><?php echo $op['jumlah']; ?></td>
				<td style="text-align:right"><?php echo 'Rp'.number_format($op['harga'],0,',','.'); ?></td>
			</tr>
			<?php } ?>
			<tr style="border-bottom:solid 1px;">
				<td style="border-right:solid 1px;" colspan="2">Total Biaya</td>
				<td style="text-align:right"><?php echo 'Rp'.number_format($order['biaya_order'],0,',','.'); ?></td>
			</tr>
			<?php if($all_digital == 0) { ?>
			<tr style="border-bottom:solid 1px;">
				<td style="border-right:solid 1px;" colspan="2">Biaya Kirim</td>
				<td style="text-align:right"><?php echo 'Rp'.number_format($order['biaya_pengiriman'],0,',','.'); ?></td>
			</tr>
			<?php } ?>
			<?php if($order['discount_nominal'] != '') { ?>
			<tr style="border-bottom:solid 1px;">
				<td style="border-right:solid 1px;" colspan="2">Discount</td>
				<td style="text-align:right"><?php echo 'Rp'.number_format($order['discount_nominal'],0,',','.'); ?></td>
			</tr>
			<?php } ?>
			<tr style="border-bottom:solid 1px;">
				<td style="border-right:solid 1px;" colspan="2">Kode Unik</td>
				<td style="text-align:right"><?php echo $order['kode_unik']; ?></td>
			</tr>
			<tr style="border-bottom:solid 1px;">
				<td style="border-right:solid 1px;" colspan="2">Total Pembayaran</td>
				<td style="text-align:right">
				<?php 
				$sub_total = $order['biaya_order']-$order['discount_nominal'];
				$sub_total = ($sub_total<0) ? 0 : $sub_total;
				$total = $sub_total + $order['biaya_pengiriman'] + $order['kode_unik'];
				echo 'Rp '.number_format($total,0,',','.'); 
				?>
				</td>
			</tr>
		</tbody>
	</table>
	<?php if($all_digital == 0) { ?>
	<p style="color:#000000">
		<strong>Alamat Tujuan Pengiriman</strong>
		<?php echo $order['nama_penerima']; ?><br>
		<?php echo $order['alamat_pengiriman']; ?><br>
		<?php echo $order['kota']['nama']; ?><br>
		<?php echo $order['no_hp_penerima']; ?><br>
	</p>
	<?php } ?>
</body>
</html>