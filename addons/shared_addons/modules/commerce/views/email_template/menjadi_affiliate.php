<html>
<title>Terima kasih telah menjadi affiliate kami</title>
<body>
	<p><strong>Salam<br>
	Dear <?php echo ucwords($this->current_user->display_name); ?>, Sahabat Affiliate<br>
	</strong></p>

	<p>Terima kasih telah berkenan bergabung menjadi Affiliate dari Sekolah Bisnis <a href="<?php echo base_url(); ?>"><?php echo base_url(); ?></a></p>
	<p>Berikut mekanisme program Affiliate kami,</p>
	<ol>
		<li>Setiap member Sekolah Bisnis <a href="<?php echo base_url(); ?>"><?php echo base_url(); ?></a> berhak menjadi Affiliate.</li>
		<li>Setiap Affiliate akan mendapatkan link dan laman landing page tersendiri.</li>
		<li>Member <a href="<?php echo base_url(); ?>"><?php echo base_url(); ?></a> yang mendaftar dan bertransaksi melalui link dan laman Anda, akan didata sebagai member yang mendaftar melalui Anda.</li>
		<li>Untuk mereka yang mendaftar melalui laman Affiliate Anda, Anda berhak mendapatkan 25% dari nilai transaksi yang terjadi.</li>
		<li>Komisi Affiliate akan ditransfer setiap tanggal 10 setiap bulannya.</li>
	</ol>
	<hr>
	<p style="color:#000000">Berikut link sub-website anda : <strong><?php echo base_url().'affiliate/'.$landing_page['uri_segment_string']; ?></strong><br>
		*jika link tidak bisa di klik, silahkan copy-paste alamat tersebut ke kolom url pada browser anda
	</p>
	<p style="color:#000000">Untuk mengelola konten pada sub-website anda, anda dapat mengunjungi link berikut : <strong><?php echo base_url().'admin'; ?></strong><br>
		*jika link tidak bisa di klik, silahkan copy-paste alamat tersebut ke kolom url pada browser anda
	</p>
</body>