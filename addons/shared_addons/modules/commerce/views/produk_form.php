<div class="page-header">
	<h1>
		<span><?php echo lang('commerce:produk:'.$mode); ?></span>
	</h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id"><?php echo lang('commerce:id'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id') != NULL){
					$value = $this->input->post('id');
				}elseif($mode == 'edit'){
					$value = $fields['id'];
				}
			?>
			<input name="id" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama"><?php echo lang('commerce:nama'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama') != NULL){
					$value = $this->input->post('nama');
				}elseif($mode == 'edit'){
					$value = $fields['nama'];
				}
			?>
			<input name="nama" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="harga"><?php echo lang('commerce:harga'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('harga') != NULL){
					$value = $this->input->post('harga');
				}elseif($mode == 'edit'){
					$value = $fields['harga'];
				}
			?>
			<input name="harga" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="deskripsi"><?php echo lang('commerce:deskripsi'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('deskripsi') != NULL){
					$value = $this->input->post('deskripsi');
				}elseif($mode == 'edit'){
					$value = $fields['deskripsi'];
				}
			?>
			<input name="deskripsi" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_file"><?php echo lang('commerce:id_file'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_file') != NULL){
					$value = $this->input->post('id_file');
				}elseif($mode == 'edit'){
					$value = $fields['id_file'];
				}
			?>
			<input name="id_file" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#provinsi').change(function() {
			var id_provinsi = $(this).val();
			$('#kota option:first').html('-- Sedang Mengambil Data --');
			$.getJSON(BASE_URL+'admin/location/kecamatan/get_kota_by_provinsi/'+id_provinsi, function(result) {
				var choose = '-- Pilih Kota --';
				if(result.length==0) {
					choose = '-- Tidak Ada Kota --'
				}
				$('#kota').html('<option value="">'+choose+'</option>');
				$.each(result, function(key,value) {
					$('#kota').append('<option value="'+value.id+'">'+value.nama+'</option>');
				});
			});
		});
	});
</script>