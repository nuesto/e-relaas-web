<style>
.btn.btn-back {
    background: #1c2c85;
    color: #ffffff;
    border-color: #ffffff;
}
.btn.btn-back:hover {
    background: #ffffff;
    color:#1c2c85;
    border-color: #1c2c85;
}
.btn.btn-back.reverse {
    border-color: #1c2c85;
    color: #1c2c85;
    background: #ffffff;
}
.btn.btn-back.reverse:hover {
    background: #1c2c85;
    color:#ffffff;
    border-color: #ffffff;
}
</style>
<div class="container container-sm">
	<div class="headline-center margin-bottom-60">
        <div class="row">
		<div class="col-sm-6 col-sm-offset-3">
            <h2 class="text-center">Peringatan</h2>
            <p class="text-center">Maaf, kami medeteksi bahwa terjadi masalah saat mengakses halaman checkout.<br></p>
			{{ theme:partial name='notices' }}
            <div class="text-center">
                <a class="btn btn-back" href="<?php echo base_url(); ?>">Kembali ke halaman utama</a>
                <a class="btn btn-back reverse" href="<?php echo site_url('login'); ?>">Login</a>
            </div>
		</div>
		</div>
	</div>
</div>
