<link rel="stylesheet" href="{{ url:site }}{{ theme:path }}/plugins/jqueryui/jquery-ui.css">
<style type="text/css">
	.btn[type=submit] {
		width: auto;
		line-height: 36px;
		height: auto;
	}
	.form-group {
		margin-bottom: 0px;
	}
	.error {
		background: rgba(227, 29, 59, 0.82) none repeat scroll 0% 0%;
		color: #ffffff;
		padding: 10px;
		margin: 10px 0;
		font-size: 15px;
	}

</style>
<div class="col-sm-5">
	<div class="page-header">
		<h3>
		<span><?php echo lang('commerce:konfirmasi_pembayaran'); ?></span>
		</h3>
	</div>
	<?php echo form_open_multipart(uri_string().$uri); ?>
	{{ theme:partial name='notices' }}
	<div class="form-horizontal">
		<div class="form-group">
			<label class="col-sm-3 " for="no_invoice"><?php echo lang('commerce:no_invoice'); ?></label>

			<div class="col-sm-9">
				<?php 
				$value = NULL;
				if($this->input->post('no_invoice') != NULL){
					$value = $this->input->post('no_invoice');
				}elseif($this->input->get('no_invoice') != NULL){
					$value = $this->input->get('no_invoice');
				}
				?>
				<input name="no_invoice" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5 form-control" id="" />
				<input type="hidden" name="order" value="">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 " for="total_tagihan"><?php echo lang('commerce:total_tagihan'); ?></label>

			<div class="col-sm-9">
				<?php 
				$value = NULL;
				if($this->input->post('total_tagihan') != NULL){
					$value = $this->input->post('total_tagihan');
				}elseif($mode == 'edit'){
					$value = $fields['total_tagihan'];
				}
				?>
				<input name="total_tagihan" readonly="readonly" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5 form-control" id="" />
			</div>
		</div>

		<hr>

		<div class="form-group">
			<label class="col-sm-3 " for="bank_rekening_pengirim"><?php echo lang('commerce:bank_rekening_pengirim'); ?></label>

			<div class="col-sm-9">
				<?php 
				$value = NULL;
				if($this->input->post('bank_rekening_pengirim') != NULL){
					$value = $this->input->post('bank_rekening_pengirim');
				}elseif($mode == 'edit'){
					$value = $fields['bank_rekening_pengirim'];
				}
				?>
				<input name="bank_rekening_pengirim" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5 form-control" id="" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 " for="nama_rekening_pengirim"><?php echo lang('commerce:nama_rekening_pengirim'); ?></label>

			<div class="col-sm-9">
				<?php 
				$value = NULL;
				if($this->input->post('nama_rekening_pengirim') != NULL){
					$value = $this->input->post('nama_rekening_pengirim');
				}elseif($mode == 'edit'){
					$value = $fields['nama_rekening_pengirim'];
				}
				?>
				<input name="nama_rekening_pengirim" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5 form-control" id="" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 " for="no_rekening_pengirim"><?php echo lang('commerce:no_rekening_pengirim'); ?></label>

			<div class="col-sm-9">
				<?php 
				$value = NULL;
				if($this->input->post('no_rekening_pengirim') != NULL){
					$value = $this->input->post('no_rekening_pengirim');
				}elseif($mode == 'edit'){
					$value = $fields['no_rekening_pengirim'];
				}
				?>
				<input name="no_rekening_pengirim" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5 form-control" id="" />
			</div>
		</div>

		<hr>

		<div class="form-group">
			<label class="col-sm-3 " for="bank_rekening_tujuan"><?php echo lang('commerce:bank_rekening_tujuan'); ?></label>

			<div class="col-sm-9">
				<?php 
				$value = NULL;
				if($this->input->post('bank_rekening_tujuan') != NULL){
					$value = $this->input->post('bank_rekening_tujuan');
				}elseif($mode == 'edit'){
					$value = $fields['bank_rekening_tujuan'];
				}
				?>
				
				<select name="bank_rekening_tujuan" class="form-control">
					<option value="">-- Pilih Rekening Tujuan --</option>
				<?php
				foreach ($rekening as $rek) {
					$selected = ($rek['id'] == $value) ? 'selected' : '';
					echo '<option '.$selected.' value="'.$rek['id'].'">Bank '.$rek['nama_bank'].' Nomor '.$rek['no_rekening'].' a.n '.$rek['atas_nama'].'</option>';
				}
				?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 " for="jumlah_pembayaran"><?php echo lang('commerce:jumlah_pembayaran'); ?></label>

			<div class="col-sm-9">
				<?php 
				$value = NULL;
				if($this->input->post('jumlah_pembayaran') != NULL){
					$value = $this->input->post('jumlah_pembayaran');
				}elseif($mode == 'edit'){
					$value = $fields['jumlah_pembayaran'];
				}
				?>
				<input name="jumlah_pembayaran" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5 form-control" id="" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 " for="tanggal_transfer"><?php echo lang('commerce:tanggal_transfer'); ?><br>&nbsp;</label>

			<div class="col-sm-9">
				<?php 
				$value = NULL;
				if($this->input->post('tanggal_transfer') != NULL){
					$value = $this->input->post('tanggal_transfer');
				}elseif($mode == 'edit'){
					$value = $fields['tanggal_transfer'];
				}
				?>
				<input name="tanggal_transfer" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5 form-control" id="tanggal_transfer" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 " for="bukti_pembayaran"><?php echo lang('commerce:bukti_pembayaran'); ?></label>

			<div class="col-sm-9">
				<?php 
				$value = NULL;
				if($this->input->post('bukti_pembayaran') != NULL){
					$value = $this->input->post('bukti_pembayaran');
				}elseif($mode == 'edit'){
					$value = $fields['bukti_pembayaran'];
				}
				?>
				<input name="bukti_pembayaran" type="file" value="<?php echo $value; ?>" class="" id="" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 " for="keterangan"><?php echo lang('commerce:keterangan'); ?> (<i>optional</i>)</label>

			<div class="col-sm-9">
				<?php 
				$value = NULL;
				if($this->input->post('keterangan') != NULL){
					$value = $this->input->post('keterangan');
				}elseif($mode == 'edit'){
					$value = $fields['keterangan'];
				}
				?>
				<textarea name="keterangan" type="text" class="col-xs-10 col-sm-5 form-control" id=""><?php echo $value; ?></textarea>
			</div>
		</div>

	</div>

	<div class="clearfix form-actions">
		<div class="col-md-offset-3 col-md-10">
			<input type="submit" class="btn col-xs-12 payment-xs" value="<?php echo lang('buttons:save'); ?>"></input>
			<a href="<?php echo site_url($return); ?>" class="btn col-xs-12 payment-xs"><?php echo lang('buttons:cancel'); ?></a>
		</div>
	</div>

	<?php echo form_close();?>
</div>

<script src="{{ url:site }}{{ theme:path }}/plugins/jqueryui/jquery-ui.js"></script>
<script type="text/javascript">
	$(function() {
		$( "#tanggal_transfer" ).datepicker({
			dateFormat : 'yy-mm-dd',
			maxDate : '0'
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		get_order();
		$('input[name=no_invoice]').keyup(function() {
			get_order();
		});

		function get_order() {
			var no_invoice = $('input[name=no_invoice]').val();
			if(no_invoice != '') {
				$('input[name=total_tagihan]').val('Sedang ambil data....');
				$('input[name=order]').val('');
				$.ajax({
					url : '<?php echo base_url(); ?>commerce/order/get_order_by_invoice',
					data : 'no_invoice='+no_invoice,
					dataType : 'JSON',
					type : 'GET',
					success : function(rst) {
						if(rst.total_tagihan==0) {
							$('input[name=total_tagihan]').val('Order tidak ditemukan.');
							$('input[name=order]').val('');
						} else {
							$('input[name=total_tagihan]').val(rst.total_tagihan);
							$('input[name=order]').val(rst.id);
						}
					}
				});
			} else {
				$('input[name=total_tagihan]').val('');
				$('input[name=order]').val('');
			}
		}
	})
</script>