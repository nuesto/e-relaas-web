
<?php
foreach ($products as $product) {
	?>
	<div class="product-item">
		<img class="" src="<?php echo base_url().'files/thumb/'.$product['id_file'].'/100/150/fit'; ?>">
		<div class="form-group">
			<label class="" for="form-harga">Harga : <span class="harga"><?php echo $product['harga']; ?></span></label>
		</div>
		<div class="form-group">
			<label class="" for="form-jumlah">Jumlah</label>
			<input type="text" name="quantity" placeholder="" class="form-control quantity" id="" style="">
		</div>
		<div class="form-group">
			<label class="" for="form-biaya">Biaya</label>
			<input type="text" readonly="readonly" name="biaya" placeholder="" class=" form-control biaya">
		</div>
	</div>
	<?php } ?>
	<script type="text/javascript">
		$(document).ready(function() {
			hitung_biaya($('.quantity'));
			$('.quantity').keyup(function() {
				hitung_biaya($('.quantity'));
			});
		});
	</script>
</div>