<div id="modal-wizard" class="modal">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header" data-target="#modal-step-contents">
<ul class="wizard-steps">
<li data-target="#modal-step1" class="active">
<span class="step">1</span>
<span class="title">Validation states</span>
</li>

<li data-target="#modal-step2">
<span class="step">2</span>
<span class="title">Alerts</span>
</li>

<li data-target="#modal-step3">
<span class="step">3</span>
<span class="title">Payment Info</span>
</li>

<li data-target="#modal-step4">
<span class="step">4</span>
<span class="title">Other Info</span>
</li>
</ul>
</div>

<div class="modal-body step-content" id="modal-step-contents">
<div class="step-pane active" id="modal-step1">
<div class="center">
<h4 class="blue">Step 1</h4>
</div>
</div>

<div class="step-pane" id="modal-step2">
<div class="center">
<h4 class="blue">Step 2</h4>
</div>
</div>

<div class="step-pane" id="modal-step3">
<div class="center">
<h4 class="blue">Step 3</h4>
</div>
</div>

<div class="step-pane" id="modal-step4">
<div class="center">
<h4 class="blue">Step 4</h4>
</div>
</div>
</div>

<div class="modal-footer wizard-actions">
<button class="btn btn-sm btn-prev">
<i class="icon-arrow-left"></i>
Prev
</button>

<button class="btn btn-success btn-sm btn-next" data-last="Finish ">
Next
<i class="icon-arrow-right icon-on-right"></i>
</button>

<button class="btn btn-danger btn-sm pull-left" data-dismiss="modal">
<i class="icon-remove"></i>
Cancel
</button>
</div>
</div>
</div>
</div><!-- PAGE CONTENT ENDS -->