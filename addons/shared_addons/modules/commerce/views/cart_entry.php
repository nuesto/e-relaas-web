<div class="page-header">
	<h1>
		<span><?php echo lang('commerce:cart:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('commerce/cart/index'.$uri); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('commerce:back') ?>
		</a>

		<?php if(group_has_role('commerce', 'edit_all_cart')){ ?>
			<a href="<?php echo site_url('commerce/cart/edit/'.$cart['id'].$uri); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('commerce', 'edit_own_cart')){ ?>
			<?php if($cart->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('commerce/cart/edit/'.$cart['id'].$uri); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('commerce', 'delete_all_cart')){ ?>
			<a href="<?php echo site_url('commerce/cart/delete/'.$cart['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('commerce', 'delete_own_cart')){ ?>
			<?php if($cart->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('commerce/cart/delete/'.$cart['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $cart['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:id_order'); ?></div>
		<?php if(isset($cart['id_order'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $cart['id_order']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:id_produk'); ?></div>
		<?php if(isset($cart['id_produk'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $cart['id_produk']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:jumlah'); ?></div>
		<?php if(isset($cart['jumlah'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $cart['jumlah']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:harga'); ?></div>
		<?php if(isset($cart['harga'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $cart['harga']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:created'); ?></div>
		<?php if(isset($cart['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($cart['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:updated'); ?></div>
		<?php if(isset($cart['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($cart['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($cart['created_by'], true); ?></div>
	</div>
</div>