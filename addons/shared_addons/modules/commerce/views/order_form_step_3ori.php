
<?php
$list_bank = array(
	array(
		'name' => 'Mandiri',
		'no_rekening' => '123123',
		),
	array(
		'name' => 'BRI',
		'no_rekening' => '123123',
		),
	array(
		'name' => 'BNI',
		'no_rekening' => '123123',
		),
	);
	?>
	<div class="form-group">
		<label class="sr-only" for="form-alamat">Rekening</label>
		<select class="form-control" name="rekening-penerima">
			<option value="">-- Pilih Bank --</option>
			<?php
			foreach ($rekening as $bank) {
				echo '<option value="'.$bank['id'].'">'.$bank['nama_bank'].' / '.$bank['no_rekening'].' ('.$bank['atas_nama'].')</option>';
			}
			?>
		</select>
	</div>