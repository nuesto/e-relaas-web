<style type="text/css">
    .tos p {
        margin-bottom: 10px;
    }
    .tos ol {
        margin: 0px 15px;
    }
    .tos ol {
        margin-bottom: 10px;
    }
    .modal-footer .btn {
        margin-bottom: 0;
    }
    .btn:focus {
        color: #fff;
        border: none;
    }
    .btn-affiliate{
    background: #ad1129;
    color: #fff !important;
    border: none;
    margin: 15px 0;
    }
    @media (max-width:480px) {
        .btn-affiliate{
            width: 100%;
            margin: 0 -5px;
        }
    }
    .btn-close{
        margin-top: 15px;
    }
    @media (max-width:480px) {
        .btn-close{
            width: 100%;
        }
    }
    #affiliate-body .alert a {
        text-decoration: underline;
    }
</style>
<?php if(isset($this->current_user) AND $this->current_user->group_id==9) { ?>
<div class="modal-body">
    <h5>Anda sudah menjadi affiliate kami.</h5>
</div>
<div class="modal-footer">
    <button type="button" class="btn" data-dismiss="modal">Tutup</button>
</div>
<?php } else { ?>
<div class="modal-body">
    <div class="tos" style="">
        <p>Salam,</p>
        <p>Berikut mekanisme program Affiliate kami : </p>
        <ol>
            <li>Setiap member Sekolah Bisnis duakodikartika.com berhak menjadi Affiliate.</li>
            <li>Setiap Affiliate akan mendapatkan link dan laman landing page tersendiri.</li>
            <li>Member duakodikartika.com yang mendaftar dan bertransaksi melalui link dan laman Anda, akan didata sebagai member yang mendaftar melalui Anda.</li>
            <li>Untuk mereka yang mendaftar melalui laman Affiliate Anda, Anda berhak mendapatkan 25% dari nilai transaksi yang terjadi.</li>
            <li>Komisi Affiliate akan ditransfer setiap tanggal 10 setiap bulannya.</li>
        </ol>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" name="agree" value="1"> Saya setuju dengan mekanisme program Affiliate di atas.
        </label>
    </div>
    {{ theme:partial name='notices' }}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-affiliate" id="be_affiliate">Menjadi Affiliate</button>
    <button type="button" class="btn btn-close" data-dismiss="modal">Tutup</button>
</div>
<?php } ?>