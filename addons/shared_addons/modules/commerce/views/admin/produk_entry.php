<div class="page-header">
	<h1><?php echo lang('commerce:produk:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/commerce/produk/index'.$uri); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('commerce:back') ?>
		</a>

		<?php if(group_has_role('commerce', 'edit_all_produk')){ ?>
			<a href="<?php echo site_url('admin/commerce/produk/edit/'.$produk['id'].$uri); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('commerce', 'edit_own_produk')){ ?>
			<?php if($produk->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/commerce/produk/edit/'.$produk->id.$uri); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('commerce', 'delete_all_produk')){ ?>
			<a href="<?php echo site_url('admin/commerce/produk/delete/'.$produk['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('commerce', 'delete_own_produk')){ ?>
			<?php if($produk->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/commerce/produk/delete/'.$produk['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $produk['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:id'); ?></div>
		<?php if(isset($produk['id'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $produk['id']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:nama'); ?></div>
		<?php if(isset($produk['nama'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $produk['nama']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:harga'); ?></div>
		<?php if(isset($produk['harga'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $produk['harga']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:deskripsi'); ?></div>
		<?php if(isset($produk['deskripsi'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $produk['deskripsi']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:id_file'); ?></div>
		<?php if(isset($produk['id_file'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $produk['id_file']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:created'); ?></div>
		<?php if(isset($produk['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($produk['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:updated'); ?></div>
		<?php if(isset($produk['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($produk['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('commerce:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($produk['created_by'], true); ?></div>
	</div>
</div>