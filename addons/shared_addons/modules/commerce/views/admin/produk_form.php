<div class="page-header">
	<h1><?php echo lang('commerce:produk:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="type"><?php echo lang('commerce:type'); ?></label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('type') != NULL){
					$value = $this->input->post('type');
				}elseif($this->input->get('f-type') != NULL){
					$value = $this->input->get('f-type');
				}elseif($mode == 'edit'){
					$value = $fields['type'];
				}
			?>
			<select name="type" class="col-xs-10 col-sm-5" id="">
				<option value="">-- Pilih Tipe Produk --</option>
				<option <?php echo ($value=="membership") ? 'selected="selected"' : ''; ?> value="membership">Membership</option>
				<option <?php echo ($value=="donation") ? 'selected="selected"' : ''; ?> value="donation">Donasi / Top up</option>
				<option <?php echo ($value=="physical") ? 'selected="selected"' : ''; ?> value="physical">Fisik</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama"><?php echo lang('commerce:nama'); ?></label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('nama') != NULL){
					$value = $this->input->post('nama');
				}elseif($this->input->get('f-nama') != NULL){
					$value = $this->input->get('f-nama');
				}elseif($mode == 'edit'){
					$value = $fields['nama'];
				}
			?>
			<input name="nama" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="harga"><?php echo lang('commerce:harga'); ?></label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('harga') != NULL){
					$value = $this->input->post('harga');
				}elseif($this->input->get('f-harga') != NULL){
					$value = $this->input->get('f-harga');
				}elseif($mode == 'edit'){
					$value = $fields['harga'];
				}
			?>
			<input name="harga" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Rupiah</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="deskripsi"><?php echo lang('commerce:deskripsi'); ?></label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('deskripsi') != NULL){
					$value = $this->input->post('deskripsi');
				}elseif($this->input->get('f-deskripsi') != NULL){
					$value = $this->input->get('f-deskripsi');
				}elseif($mode == 'edit'){
					$value = $fields['deskripsi'];
				}
			?>
			<textarea name="deskripsi" class="col-xs-10 col-sm-5" rows="5"><?php echo $value; ?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="additional_info"><?php echo lang('commerce:additional_info'); ?></label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('additional_info') != NULL){
					$value = $this->input->post('additional_info');
				}elseif($this->input->get('f-additional_info') != NULL){
					$value = $this->input->get('f-additional_info');
				}elseif($mode == 'edit'){
					$value = $fields['additional_info'];
				}
			?>
			<textarea name="additional_info" class="col-xs-10 col-sm-5" rows="5"><?php echo $value; ?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_file"><?php echo lang('commerce:id_file'); ?></label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('id_file') != NULL){
					$value = $this->input->post('id_file');
				}elseif($this->input->get('f-id_file') != NULL){
					$value = $this->input->get('f-id_file');
				}elseif($mode == 'edit'){
					$value = $fields['id_file'];
				}
			?>
			<?php if(!empty($value)){ ?>
			<img src="<?php echo base_url().'uploads/default/files/'.$fields['filename']; ?>" style="display:block; width:200px; height:auto; margin-bottom:10px;" />
			<?php } ?>
			<input type="hidden" name="id_file" value="<?php echo $value; ?>">
			<input name="image" id="foto" type="file" />
			* jpeg, jpg, png
		</div>
	</div>

	<div class="pengiriman" style="display:none">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="berat"><?php echo lang('commerce:berat'); ?></label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('berat') != NULL){
					$value = $this->input->post('berat');
				}elseif($this->input->get('f-berat') != NULL){
					$value = $this->input->get('f-berat');
				}elseif($mode == 'edit'){
					$value = $fields['berat'];
				}
			?>
			<input name="berat" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Gram</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="kode"><?php echo lang('commerce:nama_provinsi'); ?></label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('id_provinsi') != NULL){
					$value = $this->input->post('id_provinsi');
				}elseif($this->input->get('f-provinsi') != NULL){
					$value = $this->input->get('f-provinsi');
				}elseif($mode == 'edit'){
					$value = $fields['id_provinsi'];
				}
			?>
			<select name="id_provinsi" class="col-xs-10 col-sm-5" id="provinsi" >
				<option value="">-- Pilih Provinsi --</option>
				<?php foreach ($provinsi as $prov) { ?>
					<option value="<?php echo $prov['id']; ?>" <?php echo ($value==$prov['id']) ? 'selected' : ''; ?>><?php echo $prov['nama']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="kode"><?php echo lang('commerce:nama_kota'); ?></label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('id_kota_pengirim') != NULL){
					$value = $this->input->post('id_kota_pengirim');
				}elseif($this->input->get('f-kota') != NULL){
					$value = $this->input->get('f-kota');
				}elseif($mode == 'edit'){
					$value = $fields['id_kota_pengirim'];
				}
			?>
			<select name="id_kota_pengirim" class="col-xs-10 col-sm-5" id="kota" >
				<option value="">-- Pilih Kota --</option>
				<?php if (count($kota)>0) { ?>
					<?php foreach ($kota as $kota) { ?>
						<option value="<?php echo $kota['id']; ?>" <?php echo ($value==$kota['id']) ? 'selected' : ''; ?>><?php echo $kota['nama']; ?></option>
					<?php } ?>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="pengiriman"><?php echo lang('commerce:nama_kurir'); ?></label>

		<div class="col-sm-10">
			<?php
				$value = array();
				if($this->input->post('pengiriman') != NULL){
					$value = $this->input->post('pengiriman');
				}elseif($this->input->get('f-pengiriman') != NULL){
					$value = $this->input->get('f-pengiriman');
				}elseif($mode == 'edit'){
					$value = explode(",", $fields['pengiriman']);
				}
			?>
			<?php foreach ($pengiriman as $kurir) { ?>
			<div class="checkbox" style="margin-left:20px">
				<label>
					<input type="checkbox" name="pengiriman[]" value="<?php echo $kurir['id']; ?>" <?php echo (in_array($kurir['id'], $value)) ? 'checked' : ''; ?> > <?php echo $kurir['nama_kurir']; ?>
				</label>
			</div>
			<?php } ?>
		</div>
	</div>

	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_external_product"><?php echo lang('commerce:id_external_product'); ?></label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('id_external_product') != NULL){
					$value = $this->input->post('id_external_product');
				}elseif($this->input->get('f-id_external_product') != NULL){
					$value = $this->input->get('f-id_external_product');
				}elseif($mode == 'edit'){
					$value = $fields['id_external_product'];
				}
			?>
			<input name="id_external_product" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#provinsi').change(function() {
			var id_provinsi = $(this).val();
			$('#kota option:first').html('-- Sedang Mengambil Data --');
			$.getJSON(BASE_URL+'location/kota/get_kota_by_provinsi/'+id_provinsi, function(result) {
				var choose = '-- Pilih Kota --';
				if(result.length==0) {
					choose = '-- Tidak Ada Kota --'
				}
				$('#kota').html('<option value="">'+choose+'</option>');
				$.each(result, function(key,value) {
					$('#kota').append('<option value="'+value.id+'">'+value.nama+'</option>');
				});
			});
		});

		$('select[name=type]').change(function() {
			if($(this).val()=='physical') {
				$('.pengiriman').show();
			} else {
				$('.pengiriman').hide();
			}
		});

		$('select[name=type]').trigger('change');
	});
</script>
