<style type="text/css">
@media (max-width:480px) {
	.entry-label{
		text-align: left;
	}
}

@media (max-width:767px) {
	.entry-label{
		text-align: left;
	}
}

</style>
<?php
$label_col = 2;
if($this->input->is_ajax_request()) {
	$label_col = 4;
}
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel2"><?php echo lang('commerce:payment:view'); ?></h4>
</div>
<div class="modal-body">

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:no_invoice'); ?></div>
		<?php if(isset($payment['no_invoice'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['no_invoice']; ?> (Order ID : <?php echo anchor(base_url().'admin/commerce/order/view/'.$payment['id_order'],$payment['id_order']); ?>)</div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:id_customer'); ?></div>
		<?php if(isset($payment['id_customer'])){ ?>
		<div class="entry-value col-sm-8"><?php echo user_displayname($payment['id_customer']); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	
	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:status'); ?></div>
		<?php if(isset($payment['status'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['status']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<br>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:last_updated'); ?></div>
		<?php if(isset($payment['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['updated_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:tanggal_order'); ?></div>
		<?php if(isset($payment['placed_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['placed_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:expired_on'); ?></div>
		<?php if(isset($payment['expired_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['expired_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	<br>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:payment_method'); ?></div>
		<?php if(isset($payment['payment_method'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['payment_method']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>">Jumlah Tertagih</div>
		<?php if(isset($payment['biaya_order'])){ ?>
		<div class="entry-value col-sm-8">
		<?php 
		$jumlah_tertagih = ($payment['biaya_order']-$payment['discount_nominal'])+$payment['biaya_pengiriman']+$payment['kode_unik'];
		echo number_format($jumlah_tertagih,'0',',','.') ; ?>
		</div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<hr>
	<?php if($payment['payment_method']=='ipaymu') { ?>
	<h3>Detil Transaksi iPaymu</h3>
	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:ipaymu_trx_id'); ?></div>
		<?php if(isset($payment['ipaymu_trx_id'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['ipaymu_trx_id']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:ipaymu_status_code'); ?></div>
		<?php if(isset($payment['ipaymu_status_code'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['ipaymu_status_code'].' - '.$payment['ipaymu_status_name']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:ipaymu_type'); ?></div>
		<?php if(isset($payment['ipaymu_type'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['ipaymu_type']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<?php } ?>
	<?php if($payment['payment_method']=='transfer') { ?>
	<h3>Detil Konfirmasi Transfer</h3>
	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:bank_rekening_pengirim'); ?></div>
		<?php if(isset($payment['bank_rekening_pengirim'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['bank_rekening_pengirim']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:no_rekening_pengirim'); ?></div>
		<?php if(isset($payment['no_rekening_pengirim'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['no_rekening_pengirim']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:nama_rekening_pengirim'); ?></div>
		<?php if(isset($payment['nama_rekening_pengirim'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['nama_rekening_pengirim']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	
	<br>
	
	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:bank_rekening_tujuan'); ?></div>
		<?php if(isset($payment['bank_rekening_tujuan'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['nama_bank']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:no_rekening'); ?></div>
		<?php if(isset($payment['no_rekening'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['no_rekening']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:atas_nama'); ?></div>
		<?php if(isset($payment['atas_nama'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['atas_nama']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	
	<br>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:jumlah_pembayaran'); ?></div>
		<?php if(isset($payment['jumlah_pembayaran'])){ ?>
		<div class="entry-value col-sm-8"><?php echo 'Rp '.number_format($payment['jumlah_pembayaran'],0,',','.'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:keterangan'); ?></div>
		<?php if(isset($payment['keterangan']) and $payment['keterangan'] != ''){ ?>
		<div class="entry-value col-sm-8"><?php echo $payment['keterangan']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:bukti_pembayaran'); ?></div>
		<?php if(isset($payment['id_bukti'])){ ?>
		<div class="entry-value col-sm-8">
			<a target="_blank" href="<?php echo base_url().'uploads/default/files/'.$payment['filename']; ?>"><img style="max-width:100px;" src="<?php echo base_url().'uploads/default/files/'.$payment['filename']; ?>"></a>
		</div>
		<?php }
		if(isset($payment['amazon_filename'])){ ?>
		<div class="entry-value col-sm-8">
			<a target="_blank" href="https://s3-ap-southeast-1.amazonaws.com/nuesto-pyrobase/bukti_transfer/<?php echo $payment['amazon_filename']; ?>"><img style="max-width:100px;" src="https://s3-ap-southeast-1.amazonaws.com/nuesto-pyrobase/bukti_transfer/<?php echo $payment['amazon_filename']; ?>"></a>
		</div>
		<?php }
		if(!isset($payment['id_bukti']) AND !isset($payment['amazon_filename'])) { ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	<?php } ?>

	<h3>History</h3>
	<?php
	$histories = json_decode($payment['history'],true);

	if(isset($histories)) {
	?>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Time</th>
					<th>Status</th>
					<th>Updated By</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($histories as $history) {
				?>
				<tr>
					<td><?php echo (isset($history['time'])) ? $history['time'] : '-'; ?></td>
					<td><?php echo (isset($history['status'])) ? $history['status'] : '-'; ?></td>
					<td><?php echo (isset($history['updated_by'])) ? $history['updated_by'] : '-'; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<?php } else { ?>
	<?php } ?>

	<?php if($payment['payment_method']=='transfer') { ?>
		<hr>

		<h3>Verifikasi</h3>
		<form action="<?php echo base_url().'admin/commerce/payment/confirm/'.$payment['id_order'].$uri; ?>" method="post">
			<div class="row">
				<div class="entry-value col-sm-12">
					<textarea name="keterangan" class="form-control" placeholder="Tulis catatan verifikasi (Optional)"></textarea>
				</div>
			</div>

			<div class="row">
				<div class="entry-value text-center">
					<input type="submit" class="btn btn-success" value="Verifikasi">
					<a href="<?php echo base_url().'admin/commerce/payment/cancel/'.$payment['id_order'].$uri; ?>"><span class="btn btn-danger">Tolak</span></a>
				</div>
			</div>
		</form>
	<?php } ?>
</div>
