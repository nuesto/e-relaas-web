<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/daterangepicker.css" />
<style type="text/css">
	.total_penjualan {
		text-align: right;
	}
</style>
<div class="page-header">
	<h1><?php echo lang('commerce:affiliate_statistics'); ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<div class="row" style="margin-bottom:20px">
	<div class="col-xs-12 col-sm-4">
		<?php echo form_open('', array('class' => 'form-horizontal', 'method' => 'get', 'style' => ''), array('f_module' => $module_details['slug'])) ?>
			<div class="input-group">
				<?php
				// repopulate query string
				if($this->input->get()) {
					foreach ($this->input->get() as $name => $value) {
						if($name != 'landing_page') {
							echo form_hidden($name,$value);
						}
					}
				}
				?>
				<select class="form-control" name="landing_page">
					<option value="">-- Pilih Landing Page</option>
					<?php foreach($landing_page as $lp) : ?>
						<option <?php echo ($lp['uri_segment_string']==$this->input->get('landing_page')) ? 'selected="selected"' : ''; ?> value="<?php echo $lp['uri_segment_string']; ?>"><?php echo $lp['title']; ?></option>>
					<?php endforeach; ?>
				</select>
				<span class="input-group-btn">
					<button type="submit" class="btn btn-info btn-sm" style="max-height:32px;">
						Tampilkan
						<i class="icon-search icon-on-right bigger-110"></i>
					</button>
				</span>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>

<ul class="nav nav-tabs" id="myTab">
	<li class="<?php echo ($this->uri->segment(4)=='statistics') ? 'active' : ''; ?>">
		<a href="<?php echo base_url().'admin/commerce/affiliate/statistics'.$uri; ?>">
			Sales Funnel
		</a>
	</li>

	<li class="<?php echo ($this->uri->segment(4)=='statistics_source') ? 'active' : ''; ?>">
		<a href="<?php echo base_url().'admin/commerce/affiliate/statistics_source'.$uri; ?>">
			Source
		</a>
	</li>
</ul>
<br>
<div class="row">
<?php echo form_open('', array('class' => 'col-sm-4 form-horizontal', 'method' => 'get', 'style' => 'padding: 0px 24px;'), array('f_module' => $module_details['slug'])) ?>
	<div class="form-group">
		<label>Range Tanggal:&nbsp;</label>
		<div class="input-group">
			<span class="input-group-addon">
				<i class="icon-calendar bigger-110"></i>
			</span>

			<input class="form-control" name="date-range-picker" id="id-date-range-picker" type="text" value="<?php echo $range; ?>" readonly="readonly">
		</div>
	</div>
	<?php if($this->current_user->is_affiliate=='1') { ?>
	<div class="form-group">
		<label>Tampilkan Per:&nbsp;</label>
		<select class="form-control" name="group_by">
			<option <?php echo ($this->input->get('group_by')=='date') ? 'selected' : ''; ?> value="date">Tanggal</option>
			<option <?php echo ($this->input->get('group_by')=='month') ? 'selected' : ''; ?> value="month">Bulan</option>
		</select>
	</div>
	<?php } ?>
	<div class="form-group">
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
			<i class="icon-filter"></i>
			<?php echo lang('global:filters'); ?>
		</button>
		
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-xs btn-clear" type="button">
			<i class="icon-remove"></i>
			<?php echo lang('buttons:clear'); ?>
		</button>
	</div>

<?php echo form_close() ?>
</div>
<div class="row">
	<div class="col-sm-6">
		<div id="container" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
		<ul>
			<?php foreach($graph_data as $graph_entry) : ?>
			<li class="chart-data" style="display:none">
				<span class="chart-date"><?php echo $graph_entry['date']; ?></span>
				<span class="chart-pageviews"><?php echo $graph_entry['pageviews']; ?></span>
				<span class="chart-leads"><?php echo $graph_entry['leads']; ?></span>
				<span class="chart-all_order_quantity"><?php echo $graph_entry['jumlah_semua_pesanan']; ?></span>
				<span class="chart-completed_order_quantity"><?php echo $graph_entry['jumlah_pesanan']; ?></span>
			</li>
			<?php endforeach; ?>
		</ul>
		
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				draw_chart();
			})
			function draw_chart() {
				var categories = [];
				$('.chart-date').each(function() {
					categories.push($(this).html());
				});
				var pageviews = [];
				$('.chart-pageviews').each(function() {
					pageviews.push(parseFloat($(this).html()));
				});
				var leads = [];
				$('.chart-leads').each(function() {
					leads.push(parseFloat($(this).html()));
				});
				var all_order_quantity = [];
				$('.chart-all_order_quantity').each(function() {
					all_order_quantity.push(parseFloat($(this).html()));
				});
				var completed_order_quantity = [];
				$('.chart-completed_order_quantity').each(function() {
					completed_order_quantity.push(parseFloat($(this).html()));
				});

				$('#container').highcharts({
					chart: {
						type: 'line'
					},
					title: false,
					xAxis: {
						categories: categories
					},
					plotOptions: {
						line: {
							dataLabels: {
								enabled: true
							},
							enableMouseTracking: false
						}
					},
					series: [{
						name: 'Page Views',
						data: pageviews
					}, {
						name: 'Leads',
						data: leads
					}, {
						name: 'All Order',
						data: all_order_quantity
					}, {
						name: 'Completed Order',
						data: completed_order_quantity
					}]
				});
			}
		</script>
	</div>
	<div class="col-sm-6">
		<div id="funnel" style="width:100%; height:200px; margin:auto; max-width: 400px"></div>
	</div>
</div>
<br>
<?php if ($commission['total'] > 0): ?>
	
	<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover" id="affilite-table">
		<thead>
			<tr>
				<th>No</th>
				<?php if(group_has_role('commerce','view_affiliate_statistics')) { ?>
				<th><?php echo lang('commerce:nama_affiliate'); ?></th>
				<?php } elseif(group_has_role('commerce','view_own_statistics') OR $this->current_user->is_affiliate=='1') { ?>
				<th><?php echo 'Tanggal/Bulan'; ?></th>
				<?php } ?>
				<th data-toggle="tooltip" data-placement="top" title="Jumlah Halaman Landing Page Dilihat"><?php echo 'Page Views'; ?></th>
				<th data-toggle="tooltip" data-placement="top" title="Jumlah Pengguna Yang Mengunjungi Landing Page"><?php echo 'Leads'; ?></th>
				<th data-toggle="tooltip" data-placement="top" title="Jumlah Semua Pesanan Melalui Landing Page"><?php echo lang('commerce:jumlah_semua_pesanan'); ?></th>
				<th data-toggle="tooltip" data-placement="top" title="Jumlah Pesanan Selesai Melalui Landing Page"><?php echo lang('commerce:jumlah_pesanan_selesai'); ?><br><span style="font-size:9px">*Berdasarkan waktu invoice dibuat</span></th>
				<th data-toggle="tooltip" data-placement="top" title="Jumlah Penjualan Melalui Landing Page"><?php echo lang('commerce:total_penjualan'); ?></th>
			</tr>
		</thead>
		<tfoot>
            <tr>
                <th colspan="2" style="text-align:right">Total:</th>
                <th id="total_pageviews"></th>
                <th id="total_leads"></th>
                <th id="total_semua_pesanan"></th>
                <th id="total_jumlah_pesanan"></th>
                <th id="total_penjualan" class="total_penjualan"></th>
            </tr>
        </tfoot>
		<tbody>
			<?php 
			$no = 1;
			$total_penjualan = 0;
			$total_komisi = 0;
			?>
			
			<?php foreach ($commission['entries'] as $commission_entry): ?>
			<tr>
				<td><?php echo $no; $no++; ?></td>
				<?php if(group_has_role('commerce','view_affiliate_statistics')) { ?>
				<td><?php echo (isset($commission_entry['display_name'])) ? ucwords($commission_entry['display_name']) : strtoupper('Tanpa Affiliate'); ?></td>
				<?php } elseif(group_has_role('commerce','view_own_statistics') OR $this->current_user->is_affiliate=='1') { ?>
					<th><?php echo $commission_entry['date']; ?></th>
				<?php } ?>
				<td><?php echo (isset($commission_entry['pageviews'])) ? number_format($commission_entry['pageviews'],0,'.',',') : 0; ?></td>
				<td><?php echo (isset($commission_entry['leads'])) ? number_format($commission_entry['leads'],0,'.',',') : 0; ?></td>
				<td><?php echo (isset($commission_entry['jumlah_semua_pesanan'])) ? number_format($commission_entry['jumlah_semua_pesanan'],0,'.',',') : 0; ?></td>
				<td><?php echo (isset($commission_entry['jumlah_pesanan'])) ? number_format($commission_entry['jumlah_pesanan'],0,'.',',') : 0; ?></td>
				<td class="total_penjualan">Rp <?php echo number_format($commission_entry['total_penjualan'],0,'.',','); ?></td>
			</tr>
			<?php 
			$total_penjualan += $commission_entry['total_penjualan'];
			endforeach; 
			?>
		</tbody>
	</table>
	</div>

	<div class="">
		<h4>Keterangan : </h4>
		<table>
			<tr>
				<td style="padding-right: 20px;">Page View</td>
				<td>: Jumlah Halaman Landing Page Dilihat</td>
			</tr>
			<tr>
				<td style="padding-right: 20px;">Leads</td>
				<td>: Jumlah Pengguna Yang Mengunjungi Landing Page</td>
			</tr>
			<tr>
				<td style="padding-right: 20px;">Jumlah Semua Pesanan</td>
				<td>: Jumlah Semua Pesanan Melalui Landing Page</td>
			</tr>
			<tr>
				<td style="padding-right: 20px;">Jumlah Pesanan Selesai</td>
				<td>: Jumlah Pesanan Selesai Melalui Landing Page</td>
			</tr>
			<tr>
				<td style="padding-right: 20px;">Total Penjualan</td>
				<td>: Jumlah Penjualan Melalui Landing Page (jumlah penjualan selesai x nilai tertagih ke pembeli)</td>
			</tr>
		</table>
	</div>
	
	<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/jquery.dataTables.bootstrap.js"></script>
	<script type="text/javascript">
	var intVal = function (i) {
		return typeof i === 'string' ?
		i.replace(/[\$,]/g, '') * 1 :
		typeof i === 'number' ?
		i : 0;
	};
	$(document).ready(function() {
		/**
		* Number.prototype.format(n, x, s, c)
		* 
		* @param integer n: length of decimal
		* @param integer x: length of whole part
		* @param mixed   s: sections delimiter
		* @param mixed   c: decimal delimiter
		*/
		Number.prototype.format = function(n, x, s, c) {
			var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
			num = this.toFixed(Math.max(0, ~~n));

			return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
		};

		var oTable1 = $('#affilite-table').on( 'init.dt', function () {
        		generate_funnel_chart();
    		}).dataTable({
			"fnFooterCallback": function ( nRow, aaData, iStart, iEnd, aiDisplay ) {

				var total_pageview = 0;
				var total_leads = 0;
				var total_semua_pesanan = 0;
				var total_jumlah_pesanan = 0;
				var penjualan = 0;
				var total_penjualan = 0;
				for ( var i=0 ; i<aaData.length ; i++ )
				{
					total_pageview += intVal(aaData[i][2])*1;
					total_leads += intVal(aaData[i][3])*1;
					total_semua_pesanan += intVal(aaData[i][4])*1;
					total_jumlah_pesanan += intVal(aaData[i][5])*1;

					// clean from format
					penjualan = aaData[i][6];
					total_penjualan += intVal(penjualan.substring(3)) * 1;
				}

				var nCells = nRow.getElementsByTagName('th');
				// console.log(nCells,nRow,total_pageview);
				nCells[1].innerHTML = total_pageview.format(0,3,',','.');
				nCells[2].innerHTML = total_leads.format(0,3,',','.');
				nCells[3].innerHTML = total_semua_pesanan.format(0,3,',','.');
				nCells[4].innerHTML = total_jumlah_pesanan.format(0,3,',','.');
				nCells[5].innerHTML = 'Rp ' + total_penjualan.format(0,3,',','.');
			}
		});
	});

	</script>
	
<?php else: ?>
	<div class="well"><?php echo lang('commerce:affiliate:no_entry'); ?></div>
<?php endif;?>
<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/date-time/moment.min.js"></script>
<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/date-time/daterangepicker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#id-date-range-picker').daterangepicker({
			"autoApply": true,
    		"autoUpdateInput" : true,
			"locale":{
				format:'YYYY-MM-DD'
			},
    		"maxDate": moment(),
		}).prev().on(ace.click_event, function(){
			$(this).next().focus();
		});
		
		$('.btn-clear').click(function() {
			$(this).closest('form').find('input').val('');
		});
	});
</script>

<script src="<?php echo base_url(); ?>system/cms/themes/ace/plugins/d3/d3.js"></script>
<script src="<?php echo base_url(); ?>system/cms/themes/ace/plugins/d3-funnel-master/d3-funnel.js"></script>
<script>
	function generate_funnel_chart() {
		const data = [
		['pageviews', intVal($('#total_pageviews').html())],
		['leads',    intVal($('#total_leads').html())],
		['all order', intVal($('#total_semua_pesanan').html())],
		['completed order', intVal($('#total_jumlah_pesanan').html())],
		];
		const options = {
			block: { dynamicHeight: false }
		};

		const chart = new D3Funnel('#funnel');
		chart.draw(data, options);
	}
</script>