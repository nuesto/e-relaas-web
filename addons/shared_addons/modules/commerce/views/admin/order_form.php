<div class="page-header">
	<h1><?php echo lang('commerce:order:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_order"><?php echo lang('commerce:id_order'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_order') != NULL){
					$value = $this->input->post('id_order');
				}elseif($this->input->get('f-id_order') != NULL){
					$value = $this->input->get('f-id_order');
				}elseif($mode == 'edit'){
					$value = $fields['id_order'];
				}
			?>
			<input name="id_order" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="no_invoice"><?php echo lang('commerce:no_invoice'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('no_invoice') != NULL){
					$value = $this->input->post('no_invoice');
				}elseif($this->input->get('f-no_invoice') != NULL){
					$value = $this->input->get('f-no_invoice');
				}elseif($mode == 'edit'){
					$value = $fields['no_invoice'];
				}
			?>
			<input name="no_invoice" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_customer"><?php echo lang('commerce:id_customer'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_customer') != NULL){
					$value = $this->input->post('id_customer');
				}elseif($this->input->get('f-id_customer') != NULL){
					$value = $this->input->get('f-id_customer');
				}elseif($mode == 'edit'){
					$value = $fields['id_customer'];
				}
			?>
			<input name="id_customer" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_session"><?php echo lang('commerce:id_session'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_session') != NULL){
					$value = $this->input->post('id_session');
				}elseif($this->input->get('f-id_session') != NULL){
					$value = $this->input->get('f-id_session');
				}elseif($mode == 'edit'){
					$value = $fields['id_session'];
				}
			?>
			<input name="id_session" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="status_pesanan"><?php echo lang('commerce:status_pesanan'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('status_pesanan') != NULL){
					$value = $this->input->post('status_pesanan');
				}elseif($this->input->get('f-status_pesanan') != NULL){
					$value = $this->input->get('f-status_pesanan');
				}elseif($mode == 'edit'){
					$value = $fields['status_pesanan'];
				}
			?>
			<input name="status_pesanan" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_affiliate"><?php echo lang('commerce:id_affiliate'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_affiliate') != NULL){
					$value = $this->input->post('id_affiliate');
				}elseif($this->input->get('f-id_affiliate') != NULL){
					$value = $this->input->get('f-id_affiliate');
				}elseif($mode == 'edit'){
					$value = $fields['id_affiliate'];
				}
			?>
			<input name="id_affiliate" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="kode_unik"><?php echo lang('commerce:kode_unik'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('kode_unik') != NULL){
					$value = $this->input->post('kode_unik');
				}elseif($this->input->get('f-kode_unik') != NULL){
					$value = $this->input->get('f-kode_unik');
				}elseif($mode == 'edit'){
					$value = $fields['kode_unik'];
				}
			?>
			<input name="kode_unik" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tanggal_order"><?php echo lang('commerce:tanggal_order'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('tanggal_order') != NULL){
					$value = $this->input->post('tanggal_order');
				}elseif($this->input->get('f-tanggal_order') != NULL){
					$value = $this->input->get('f-tanggal_order');
				}elseif($mode == 'edit'){
					$value = $fields['tanggal_order'];
				}
			?>
			<input name="tanggal_order" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="biaya_order"><?php echo lang('commerce:biaya_order'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('biaya_order') != NULL){
					$value = $this->input->post('biaya_order');
				}elseif($this->input->get('f-biaya_order') != NULL){
					$value = $this->input->get('f-biaya_order');
				}elseif($mode == 'edit'){
					$value = $fields['biaya_order'];
				}
			?>
			<input name="biaya_order" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="biaya_pengiriman"><?php echo lang('commerce:biaya_pengiriman'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('biaya_pengiriman') != NULL){
					$value = $this->input->post('biaya_pengiriman');
				}elseif($this->input->get('f-biaya_pengiriman') != NULL){
					$value = $this->input->get('f-biaya_pengiriman');
				}elseif($mode == 'edit'){
					$value = $fields['biaya_pengiriman'];
				}
			?>
			<input name="biaya_pengiriman" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="alamat_pengiriman"><?php echo lang('commerce:alamat_pengiriman'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('alamat_pengiriman') != NULL){
					$value = $this->input->post('alamat_pengiriman');
				}elseif($this->input->get('f-alamat_pengiriman') != NULL){
					$value = $this->input->get('f-alamat_pengiriman');
				}elseif($mode == 'edit'){
					$value = $fields['alamat_pengiriman'];
				}
			?>
			<input name="alamat_pengiriman" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="kurir_pengiriman"><?php echo lang('commerce:kurir_pengiriman'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('kurir_pengiriman') != NULL){
					$value = $this->input->post('kurir_pengiriman');
				}elseif($this->input->get('f-kurir_pengiriman') != NULL){
					$value = $this->input->get('f-kurir_pengiriman');
				}elseif($mode == 'edit'){
					$value = $fields['kurir_pengiriman'];
				}
			?>
			<input name="kurir_pengiriman" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="paket_pengiriman"><?php echo lang('commerce:paket_pengiriman'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('paket_pengiriman') != NULL){
					$value = $this->input->post('paket_pengiriman');
				}elseif($this->input->get('f-paket_pengiriman') != NULL){
					$value = $this->input->get('f-paket_pengiriman');
				}elseif($mode == 'edit'){
					$value = $fields['paket_pengiriman'];
				}
			?>
			<input name="paket_pengiriman" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama_penerima"><?php echo lang('commerce:nama_penerima'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama_penerima') != NULL){
					$value = $this->input->post('nama_penerima');
				}elseif($this->input->get('f-nama_penerima') != NULL){
					$value = $this->input->get('f-nama_penerima');
				}elseif($mode == 'edit'){
					$value = $fields['nama_penerima'];
				}
			?>
			<input name="nama_penerima" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="no_hp_penerima"><?php echo lang('commerce:no_hp_penerima'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('no_hp_penerima') != NULL){
					$value = $this->input->post('no_hp_penerima');
				}elseif($this->input->get('f-no_hp_penerima') != NULL){
					$value = $this->input->get('f-no_hp_penerima');
				}elseif($mode == 'edit'){
					$value = $fields['no_hp_penerima'];
				}
			?>
			<input name="no_hp_penerima" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>