<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/datepicker.css" />
<style type="text/css">
	#master-voucher .lbl::before {
		content: "\a0NO\a0\a0\a0\a0\a0\a0\a0\a0\a0\a0\a0YES";
	}
</style>
<div class="page-header">
	<h1><?php echo lang('commerce:voucher:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal" id="form-voucher">
	<?php if($this->current_user->is_affiliate == 1) { ?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="template_voucher">Template Voucher</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('template_voucher') != NULL){
					$value = $this->input->post('template_voucher');
				}elseif($this->input->get('f-template_voucher') != NULL){
					$value = $this->input->get('f-template_voucher');
				}
			?>
			<select class="col-xs-10 col-sm-5" id="template_voucher" name="template_voucher">
				<option value="">-- Pilih Template Voucher --</option>
			<?php foreach($template_voucher as $template) { ?>
				<option <?php echo ($value == $template['id']) ? 'selected="selected"' : ''; ?> value="<?php echo $template['id']; ?>"><?php echo $template['code']; ?></option>
			<?php } ?>
			</select>
		</div>
	</div>
	<?php } ?>
	<?php
	$hide_voucher_data = false;
	if($this->current_user->is_affiliate == 1 AND !$this->input->post('template_voucher')) {
		$hide_voucher_data = true;
	}
	?>
	<div id="voucher-data" style="<?php echo ($hide_voucher_data) ? 'display: none' : ''; ?>">
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="code">Kode</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('code') != NULL){
					$value = $this->input->post('code');
				}elseif($this->input->get('f-code') != NULL){
					$value = $this->input->get('f-code');
				}elseif($mode == 'edit'){
					$value = $fields['code'];
				}
			?>
			<div class="col-sm-5" style="padding: 0;">
				<div id="code-value" style="display: none;"><?php echo $value; ?></div>
				<div class="input-group" style="width: 100%" id="voucher-code">
					<input name="code" type="text" value="<?php echo $value; ?>" class="form-control" id="" style="text-transform:uppercase;" />
				</div>
				<?php if($mode == 'new') { ?>
				<div class="checkbox" style="margin-left:20px">
					<label>
						<?php
						$checked = NULL;
						if($this->input->post('is_multi')) {
							$checked = 'checked="checked"';
						}
						?>
						<input <?php echo $checked; ?> type="checkbox" name="is_multi" value="1"> Multi Voucher
					</label>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="valid_unit">Berlaku Hingga</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('valid_unit') != NULL){
					$value = date('d-m-Y',strtotime($this->input->post('valid_unit')));
				}elseif($this->input->get('f-valid_unit') != NULL){
					$value = date('d-m-Y',strtotime($this->input->get('f-valid_unit')));
				}elseif($mode == 'edit'){
					$value = (isset($fields['valid_unit'])) ? date('d-m-Y',strtotime($fields['valid_unit'])) : NULL;
				}
			?>
			<div class="col-sm-5" style="padding: 0;">
				<div class="input-group">
					<input name="valid_unit" class="form-control date-picker" id="berlaku-hingga" readonly="readonly" type="text" data-provide="datepicker" data-date-format="dd-mm-yyyy" value="<?php echo $value; ?>">
					<span class="input-group-addon">
						<i class="icon-calendar bigger-110"></i>
					</span>
				</div>
				<div class="checkbox" style="margin-left:20px">
					<label>
						<?php
						$checked = NULL;
						if($this->input->post('no_valid_unit')) {
							$checked = 'checked="checked"';
						}elseif($mode == 'edit'AND $fields['valid_unit'] == NULL) {
							$checked = 'checked="checked"';
						}
						?>
						<input <?php echo $checked; ?> type="checkbox" name="no_valid_unit" value="1"> tidak ada batas waktu
					</label>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="usage_limit_count">Batas Penggunaan</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('usage_limit_count') != NULL){
					$value = $this->input->post('usage_limit_count');
				}elseif($this->input->get('f-usage_limit_count') != NULL){
					$value = $this->input->get('f-usage_limit_count');
				}elseif($mode == 'edit'){
					$value = $fields['usage_limit_count'];
				}
			?>
			<input name="usage_limit_count" type="number" value="<?php echo $value; ?>" class="" id="" />

			<?php 
				$value = NULL;
				if($this->input->post('usage_limit_scope') != NULL){
					$value = $this->input->post('usage_limit_scope');
				}elseif($this->input->get('f-usage_limit_scope') != NULL){
					$value = $this->input->get('f-usage_limit_scope');
				}elseif($mode == 'edit'){
					$value = $fields['usage_limit_scope'];
				}
			?>
			<select class="" name="usage_limit_scope">
				<option <?php echo ($value == 'limit_per_customer') ? 'selected="selected"' : ''; ?> value="limit_per_customer">Per Customer</option>
				<option <?php echo ($value == 'limit_all_customer') ? 'selected="selected"' : ''; ?> value="limit_all_customer">Untuk Seluruh Customer</option>
			</select>
			<div class="checkbox" style="margin-left:20px">
				<label>
					<?php
						$checked = NULL;
						if($this->input->post('no_limit_usage')) {
							$checked = 'checked="checked"';
						}elseif($mode == 'edit'AND $fields['usage_limit_count'] == NULL) {
							$checked = 'checked="checked"';
						}
					?>
					<input <?php echo $checked; ?> type="checkbox" name="no_limit_usage" value="1"> tidak ada batas penggunaan
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="benefit_type">Tipe Benefit</label>

		<div class="col-sm-10">
			<?php 
				$value = 'discount_nominal';
				$disabled = NULL;
				if($this->input->post('benefit_type') != NULL){
					$value = $this->input->post('benefit_type');
				}elseif($this->input->get('f-benefit_type') != NULL){
					$value = $this->input->get('f-benefit_type');
				}elseif($mode == 'edit'){
					$value = $fields['benefit_type'];
				}
				$benefit_type = $value;

				if($this->current_user->is_affiliate == 1) {
					$value = $vdkk['benefit_type'];
					$disabled = 'disabled="disabled"';
					echo form_hidden('benefit_type',$vdkk['benefit_type']);
				}
			?>
			<select class="col-xs-10 col-sm-5" id="benefit_type" name="benefit_type" <?php echo $disabled; ?>>
				<option <?php echo ($value=='discount_nominal') ? 'selected="selected"' : ''; ?> value="discount_nominal">Potongan Harga (Rp)</option>
				<option <?php echo ($value=='discount_percentage') ? 'selected="selected"' : ''; ?> value="discount_percentage">Potongan Harga (%)</option>
			</select>
		</div>
	</div>


	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="benefit_value">Nilai Benefit</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				$disabled = NULL;
				if($this->input->post('benefit_value') != NULL){
					$value = $this->input->post('benefit_value');
				}elseif($this->input->get('f-benefit_value') != NULL){
					$value = $this->input->get('f-benefit_value');
				}elseif($mode == 'edit'){
					$value = $fields['benefit_value'];
				}

				if($this->current_user->is_affiliate == 1) {
					$value = $vdkk['benefit_value'];
					$disabled = 'readonly="readonly"';
					echo form_hidden('benefit_value',$vdkk['benefit_value']);
				}
			?>
			<div class="col-xs-10 col-sm-5" style="padding: 0;" id="benefit_value">
				<div class="input-group">
					<span class="input-group-addon">
					<?php
					if($this->current_user->is_affiliate == 1) {
						$prefix = ($vdkk['benefit_type']=='discount_nominal') ? 'Rp' : '%';
					}
					echo (isset($prefix)) ? $prefix : ($benefit_type=='discount_nominal') ? 'Rp' : '%';
					?>
					</span>
					<input class="form-control" id="" type="text" name="benefit_value" value="<?php echo $value; ?>" <?php echo $disabled; ?>>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="state_active">Status</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('state_active') != NULL){
					$value = $this->input->post('state_active');
				}elseif($this->input->get('f-state_active') != NULL){
					$value = $this->input->get('f-state_active');
				}elseif($mode == 'edit'){
					$value = $fields['state_active'];
				}
			?>
			<select class="col-xs-10 col-sm-5" id="" name="state_active">
				<option <?php echo ($value == '1') ? 'selected="selected"' : ''; ?> value="1">Aktif</option>
				<option <?php echo ($value == '0') ? 'selected="selected"' : ''; ?> value="0">Non Aktif</option>
			</select>
		</div>
	</div>

	<?php
	if($this->current_user->is_affiliate == 1) {
		echo form_hidden('affiliate_id',$this->current_user->id);
	} else {
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="affiliate_id">Affiliate</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('affiliate_id') != NULL){
					$value = $this->input->post('affiliate_id');
				}elseif($this->input->get('f-affiliate_id') != NULL){
					$value = $this->input->get('f-affiliate_id');
				}elseif($mode == 'edit'){
					$value = $fields['affiliate_id'];
				}
			?>
			<select class="col-xs-10 col-sm-5 chosen-select" id="" data-placeholder="Pilih Affiliate..." name="affiliate_id">
				<option value="">Tanpa Affiliate</option>
				<?php
				if(count($affiliate_lists)>0) {
					foreach ($affiliate_lists as $affiliate) {
				?>
				<option <?php echo ($value==$affiliate['id']) ? 'selected="selected"' : ''; ?> value="<?php echo $affiliate['id']; ?>"><?php echo $affiliate['display_name']; ?></option>
				<?php } 
				} ?>
			</select>
		</div>
	</div>
	<?php } ?>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="product_type">Berlaku pada Tipe Produk</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('product_type') != NULL){
					$value = $this->input->post('product_type');
				}elseif($this->input->get('f-product_type') != NULL){
					$value = $this->input->get('f-product_type');
				}elseif($mode == 'edit'){
					$value = $fields['product_type'];
				}

				$disabled = null;
				if($this->current_user->is_affiliate == 1) {
					$disabled = 'disabled="disabled"';
				}
			?>
			<select class="col-xs-10 col-sm-5" id="" name="product_type" <?php echo $disabled; ?>>
				<option value="">-- Pilih Tipe Produk --</option>
				<option <?php echo ($value=="membership") ? 'selected="selected"' : ''; ?> value="membership">Membership</option>
				<option <?php echo ($value=="donation") ? 'selected="selected"' : ''; ?> value="donation">Donasi / Top up</option>
				<option <?php echo ($value=="physical") ? 'selected="selected"' : ''; ?> value="physical">Fisik</option>
			</select>
		</div>
	</div>
	<?php if(group_has_role('commerce','create_template_voucher')) { ?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="is_affiliate_template">Jadi template voucher?</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('is_affiliate_template') != NULL){
					$value = $this->input->post('is_affiliate_template');
				}elseif($mode == 'edit'){
					$value = $fields['is_affiliate_template'];
				}
			?>
			<label id="master-voucher">
				<input name="is_affiliate_template" class="ace ace-switch ace-switch-7" type="checkbox" value="1" <?php echo ($value=='1') ? 'checked' : ''; ?>>
				<span class="lbl"></span>
			</label>
		</div>
	</div>
	<?php } ?>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>
<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/date-time/moment.min.js"></script>
<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/date-time/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		no_valid_unit();
		no_limit_usage();
		is_multi();

		$('.date-picker').datepicker({
			autoclose:true,
			startDate:'0d'
		}).next().on(ace.click_event, function(){
			$(this).prev().focus();
		});

		$(".chosen-select").chosen();

		$('#benefit_type').change(function() {
			var type = $(this).val();
			if(type == 'discount_nominal') {
				$('#benefit_value .input-group .input-group-addon').text('Rp');
			} else {
				$('#benefit_value .input-group .input-group-addon').text('%');
			}
		});

		$('input[name=no_valid_unit]').click(function() {
			no_valid_unit();
		});

		$('input[name=is_multi]').click(function() {
			is_multi();
		});

		$('input[name=no_limit_usage]').click(function() {
			if($(this).is(':checked')) {
				$('input[name=usage_limit_count]').val('');
				$('input[name=usage_limit_count]').attr('readonly',true);
				$('select[name=usage_limit_scope]').attr('disabled',true);
			} else {
				$('input[name=usage_limit_count]').removeAttr('readonly');
				$('select[name=usage_limit_scope]').removeAttr('disabled');
			}
		});
	});

	$(document).ready(function() {
		$('input[name=is_affiliate_template]').click(function() {
			$('input[name=no_limit_usage]').attr('checked',true);
			no_limit_usage();
		});

		$('#template_voucher').change(function() {
			var id_voucher = $(this).val();
			if(id_voucher != '') {
				$.ajax({
					url : '<?php echo base_url(); ?>admin/commerce/voucher/get_voucher/'+id_voucher,
					type : 'get',
					dataType : 'json',
					success : function(rst) {
						if(rst.error !== undefined) {
							$('#template_voucher').after('<div class="col-xs-10 col-sm-5" style="padding:5px;">'+rst.error+'</div>');
						} else {
							// set value
							if(rst.valid_unit==null) {
								$('input[name=no_valid_unit]').attr('checked',true);
								no_valid_unit();
							} else {
								$('input[name=valid_unit]').val(rst.valid_unit);
							}

							if(rst.usage_limit_count == null && rst.usage_limit_scope == null) {
								$('input[name=no_limit_usage]').attr('checked',true);
								no_limit_usage();
							} else {
								$('input[name=usage_limit_count]').val(rst.usage_limit_count);
								$('select[name=usage_limit_scope]').val(rst.usage_limit_scope);
								$('select[name=usage_limit_scope]').trigger('change');
							}
							
							$('select[name=benefit_type]').val(rst.benefit_type);
							$('select[name=benefit_type]').trigger('change');
							$('input[name=benefit_value]').val(rst.benefit_value);
							$('select[name=product_type]').val(rst.product_type);
							$('select[name=product_type]').trigger('change');
							$('select[name=state_active]').val(rst.state_active);
							$('select[name=state_active]').trigger('change');

							// disabled field
							$('input[name=valid_unit]').attr('disabled',true);
							$('input[name=no_valid_unit]').attr('disabled',true);
							// $('input[name=usage_limit_count]').attr('disabled',true);
							// $('select[name=usage_limit_scope]').attr('disabled',true);
							// $('input[name=no_limit_usage]').attr('disabled',true);
							$('select[name=benefit_type]').attr('disabled',true);
							$('input[name=benefit_value]').attr('disabled',true);
							$('select[name=product_type]').attr('disabled',true);

							// show the form
							$('#voucher-data').show();
						}
					},
					timeout : 10000,
					error : function(rst) {
						alert('Terjadi masalah saat ambil data voucher');
					}
				});
			}
		});
	});

	function no_valid_unit() {
		if($('input[name=no_valid_unit]').is(':checked')) {
			$('input[name=valid_unit]').val('');
			$('input[name=valid_unit]').attr('disabled',true);
		} else {
			$('input[name=valid_unit]').removeAttr('disabled');
		}
	}
	function no_limit_usage() {
		if($('input[name=no_limit_usage]').is(':checked')) {
			$('input[name=usage_limit_count]').val('');
			$('input[name=usage_limit_count]').attr('readonly',true);
			$('select[name=usage_limit_scope]').attr('disabled',true);
		} else {
			$('input[name=usage_limit_count]').removeAttr('readonly');
			$('select[name=usage_limit_scope]').removeAttr('disabled');
		}
	}
	function is_multi() {
		if($('input[name=is_multi]').is(':checked')) {
			$('#voucher-code').html('<textarea name="code" class="form-control" style="text-transform:uppercase;"></textarea>');
			$('#voucher-code textarea').html($('#code-value').html());
		} else {
			$('#voucher-code').html('<input name="code" type="text" value="" class="form-control" id="" style="text-transform:uppercase;" />');
			$('#voucher-code input').val($('#code-value').html());
		}
	}
</script>