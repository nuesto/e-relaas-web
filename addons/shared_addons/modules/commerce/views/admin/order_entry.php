<style type="text/css">
	#modal-konfirmasi .modal-content {
		padding: 15px;
		min-height: 56px;
	}
	#modal-konfirmasi .modal-content img.loading {
		position: absolute;
		left: 50%;
		transform: translate(-50%);
	}
</style>
<?php
$label_col = 2;
$hide_button = 0;
if($this->input->is_ajax_request()) {
	$label_col = 4;
	$hide_button = 1;
}
?>
<div class="page-header">
	<h1><?php echo lang('commerce:order:view').' '.$order['no_invoice']; ?></h1>
	
	<div class="btn-group content-toolbar" style="<?php echo ($hide_button) ? 'display: none;' : ''; ?>">
		
		<a href="<?php echo site_url('admin/commerce/order/index'.$uri); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('commerce:back') ?>
		</a>

		<?php if(group_has_role('commerce', 'delete_all_order')){ ?>
			<a href="<?php echo site_url('admin/commerce/order/delete/'.$order['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('commerce', 'delete_own_order')){ ?>
			<?php if($order->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/commerce/order/delete/'.$order['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
		<?php 
		if(group_has_role('commerce', 'confirm_payment') AND strpos($order['status_pembayaran'], 'pending') !== false AND $order['status_pesanan'] == 'waiting-payment'){
			echo '<span data-order="'.$order['id'].'" class="btn btn-sm btn-success edit_payment_status" data-toggle="modal" data-target="#modal-konfirmasi">'.lang('commerce:payment:edit_status').'</span>';
			
		}
		?>
	</div>
</div>

<div>
	<h3>Pesanan </h3>
	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:no_invoice'); ?></div>
		<?php if(isset($order['no_invoice'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['no_invoice']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:id_customer'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($order['id_customer'], false); ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:status_pesanan'); ?></div>
		<?php if(isset($order['status_pesanan'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['status_pesanan']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<br>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:last_updated'); ?></div>
		<?php if(isset($order['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['updated_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:tanggal_order'); ?></div>
		<?php if(isset($order['placed_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['placed_on']; ?></div>
		<?php }elseif(isset($order['tanggal_order'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['tanggal_order']; ?></div>
		<?php }elseif(isset($order['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['created_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>	

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:expired_on'); ?></div>
		<?php if(isset($order['expired_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['expired_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<br>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:payment_method'); ?></div>
		<?php if(isset($order['payment_method'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['payment_method']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:status_pembayaran'); ?></div>
		<?php if(isset($order['status_pembayaran'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['status_pembayaran']; ?>&nbsp;&nbsp;
		<?php 
		$as_modal = 'href="'.base_url().'admin/commerce/payment/view/'.$order['id'].'"';
		if(!$this->input->is_ajax_request()) {
			$as_modal = 'data-order="'.$order['id'].'" data-toggle="modal" data-target="#modal-konfirmasi"';
		} ?>
		<a <?php echo $as_modal; ?>>detil</a>
		</div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">belum melakukan pembayaran</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>">Jumlah Tertagih</div>
		<?php if(isset($order['biaya_order'])){ ?>
		<div class="entry-value col-sm-8">
		<?php 
		$jumlah_tertagih = ($order['biaya_order']-$order['discount_nominal'])+$order['biaya_pengiriman']+$order['kode_unik'];
		echo number_format($jumlah_tertagih,'0',',','.') ; ?>
		</div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:bukti_pembayaran'); ?></div>
		<?php if(isset($order['id_bukti'])){ ?>
		<div class="entry-value col-sm-8">
			<a target="_blank" href="<?php echo base_url().'uploads/default/files/'.$order['filename']; ?>"><img style="max-width:100px;" src="<?php echo base_url().'uploads/default/files/'.$order['filename']; ?>"></a>
		</div>
		<?php }
		if(isset($order['amazon_filename'])){ ?>
		<div class="entry-value col-sm-8">
			<a target="_blank" href="https://s3-ap-southeast-1.amazonaws.com/nuesto-pyrobase/bukti_transfer/<?php echo $order['amazon_filename']; ?>"><img style="max-width:100px;" src="https://s3-ap-southeast-1.amazonaws.com/nuesto-pyrobase/bukti_transfer/<?php echo $order['amazon_filename']; ?>"></a>
		</div>
		<?php }
		if(!isset($order['id_bukti']) AND !isset($order['amazon_filename'])) { ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	
	<hr>

	<h3>Rincian pembelian</h3>
	<?php
	// check if all the order product is digital then set is digital order = 1
	$is_digital_order = 1;
	foreach ($order_product as $op) {
		if($op['type'] == 'physical') {
			$is_digital_order = 0;
			continue;
		}
	}
	?>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr style="text-align:center;">
					<th style="text-align:center;"><?php echo lang('commerce:nama'); ?></th>
					<th style="text-align:center;width:100px"><?php echo lang('commerce:jumlah'); ?></th>
					<th style="text-align:center;width:150px"><?php echo lang('commerce:harga'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($order_product as $op) { ?>
				<tr style="border-bottom:solid 1px">
					<td style=""><strong><?php echo $op['nama']; ?></strong></td>
					<td style="text-align:center"><?php echo $op['jumlah']; ?></td>
					<td style="text-align:right"><?php echo 'Rp'.number_format($op['harga'],0,',','.'); ?></td>
				</tr>
				<?php } ?>
				
				<tr style="border-bottom:solid 1px;">
					<td style="" colspan="2">Total Biaya</td>
					<td style="text-align:right"><?php echo 'Rp'.number_format($order['biaya_order'],0,',','.'); ?></td>
				</tr>
				<?php if($is_digital_order == 0) { ?>
				<tr style="border-bottom:solid 1px;">
					<td style="" colspan="2">Biaya Kirim</td>
					<td style="text-align:right"><?php echo 'Rp'.number_format($order['biaya_pengiriman'],0,',','.'); ?></td>
				</tr>
				<?php } ?>
				<?php if($order['discount_nominal'] != '') { ?>
				<tr style="border-bottom:solid 1px;">
					<td style="" colspan="2">Discount</td>
					<td style="text-align:right"><?php echo 'Rp'.number_format($order['discount_nominal'],0,',','.'); ?></td>
				</tr>
				<?php } ?>
				<tr style="border-bottom:solid 1px;">
					<td style="" colspan="2">Kode Unik</td>
					<td style="text-align:right"><?php echo $order['kode_unik']; ?></td>
				</tr>
				<tr style="border-bottom:solid 1px;">
					<td style="" colspan="2">Total Pembayaran</td>
					<td style="text-align:right">
					<?php 
					$sub_total = $order['biaya_order']-$order['discount_nominal'];
					$sub_total = ($sub_total<0) ? 0 : $sub_total;
					$total = $sub_total + $order['biaya_pengiriman'] + $order['kode_unik'];
					echo 'Rp '.number_format($total,0,',','.'); 
					?>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<hr>

	<h3>Affiliate</h3>
	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:nama_affiliate'); ?></div>
		<?php if(isset($order['id_affiliate']) AND $order['id_affiliate']!=0){ ?>
		<div class="entry-value col-sm-8"><?php echo user_displayname($order['id_affiliate']); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:komisi'); ?></div>
		<?php if(isset($order['id_affiliate']) AND $order['id_affiliate']!=0){ ?>
		<div class="entry-value col-sm-8"><?php echo 'Rp '.number_format($total*0.25,0,',','.'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<hr>

	<h3>History</h3>
	<?php
	$histories = json_decode($order['state_history'],true);

	if(isset($histories)) {
	?>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Time</th>
					<th>Status</th>
					<th>Updated By</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($histories as $history) {
				?>
				<tr>
					<td><?php echo (isset($history['time'])) ? $history['time'] : '-'; ?></td>
					<td><?php echo (isset($history['status'])) ? $history['status'] : '-'; ?></td>
					<td><?php echo (isset($history['updated_by'])) ? $history['updated_by'] : '-'; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<?php } else { ?>
	<?php } ?>
	<?php if($is_digital_order == 0) { ?>
	<h3>Pengiriman</h3>
	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:nama_penerima'); ?></div>
		<?php if(isset($order['nama_penerima'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['nama_penerima']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:no_hp_penerima'); ?></div>
		<?php if(isset($order['no_hp_penerima'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['no_hp_penerima']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
	
	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:alamat_pengiriman'); ?></div>
		<?php if(isset($order['alamat_pengiriman'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $order['alamat_pengiriman'].', '.$order['kota']['nama']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-<?php echo $label_col; ?>"><?php echo lang('commerce:no_resi_pengiriman'); ?></div>
		<div class="entry-value col-sm-8"><?php echo form_open(site_url('admin/commerce/order/update_resi/'.$order['id']),'id="form-resi"'); echo form_input('no_resi_pengiriman',$order['no_resi_pengiriman'],'class="form-input"'); echo form_submit('','Simpan','class="btn btn-xs btn-success"'); echo form_close(); ?></div>
	</div>
	<?php } ?>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-konfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#modal-konfirmasi').on('hidden.bs.modal', function(e) {
			$('#modal-konfirmasi .modal-content').html('');
		});
		$('#modal-konfirmasi').on('shown.bs.modal', function(e) {
			$('#modal-konfirmasi .modal-content').html('<img class="loading" src="<?php echo base_url().'system/cms/themes/ace/img/loading_bar.gif'; ?>">');
			
			var order = $(e.relatedTarget).data('order');
			$.ajax({
				url : '<?php echo base_url(); ?>admin/commerce/payment/view/'+order+'<?php echo $uri; ?>',
				type : 'GET',
				success : function(rst) {
					$('#modal-konfirmasi .modal-content').html(rst);
				}
			});
		});

		$('#form-resi').submit(function(e) {
			e.preventDefault();
			var form = $(this);
			form.after('<p class="label">Loading</p>');
			form.find('input[type=submit]').attr('disabled',true);
			$.ajax({
				'url':form.attr('action'),
				'data' : form.serialize(),
				'type' : 'POST',
				'dataType' : 'json',
				'success' : function(rst) {
					form.closest('.entry-value').find('.label').remove();
					form.find('input[type=submit]').attr('disabled',false);
					if(rst.status=='success') {
						var lbl_class = 'success';
					} else if(rst.status=='error') {
						var lbl_class = 'danger';
					}
					form.after('<div class="label label-'+lbl_class+'">'+rst.messages+'</div>');
					setTimeout(function() {
						form.closest('.entry-value').find('.label').remove();
					},1500);
				},
				'timeout' : 10000,
				'error' : function(xhr) {
					form.closest('.entry-value').find('.label').remove();

					form.after('<p class="label">Terdapat kesalahan saat submit data</p>');
					setTimeout(function() {
						form.closest('.entry-value').find('.label').remove();
					},1500);
				}
			});
		});
	});
</script>