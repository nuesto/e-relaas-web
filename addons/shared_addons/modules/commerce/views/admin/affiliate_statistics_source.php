<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/daterangepicker.css" />
<style type="text/css">
	.total_penjualan {
		text-align: right;
	}
</style>
<div class="page-header">
	<h1><?php echo lang('commerce:affiliate_statistics'); ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<div class="row" style="margin-bottom:20px">
	<div class="col-xs-12 col-sm-4">
		<?php echo form_open('', array('class' => 'form-horizontal', 'method' => 'get', 'style' => ''), array('f_module' => $module_details['slug'])) ?>
			<div class="input-group">
				<?php
				// repopulate query string
				if($this->input->get()) {
					foreach ($this->input->get() as $name => $value) {
						if($name != 'landing_page') {
							echo form_hidden($name,$value);
						}
					}
				}
				?>
				<select class="form-control" name="landing_page">
					<option value="">-- Pilih Landing Page</option>
					<?php foreach($landing_page as $lp) : ?>
						<option <?php echo ($lp['uri_segment_string']==$this->input->get('landing_page')) ? 'selected="selected"' : ''; ?> value="<?php echo $lp['uri_segment_string']; ?>"><?php echo $lp['title']; ?></option>>
					<?php endforeach; ?>
				</select>
				<span class="input-group-btn">
					<button type="submit" class="btn btn-info btn-sm" style="max-height:32px;">
						Tampilkan
						<i class="icon-search icon-on-right bigger-110"></i>
					</button>
				</span>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>

<ul class="nav nav-tabs" id="myTab">
	<li class="<?php echo ($this->uri->segment(4)=='statistics') ? 'active' : ''; ?>">
		<a href="<?php echo base_url().'admin/commerce/affiliate/statistics'.$uri; ?>">
			Sales Funnel
		</a>
	</li>

	<li class="<?php echo ($this->uri->segment(4)=='statistics_source') ? 'active' : ''; ?>">
		<a href="<?php echo base_url().'admin/commerce/affiliate/statistics_source'.$uri; ?>">
			Source
		</a>
	</li>
</ul>
<br>
<div class="row">
<?php echo form_open('', array('class' => 'col-sm-4 form-horizontal', 'method' => 'get', 'style' => 'padding: 0px 24px;'), array('f_module' => $module_details['slug'])) ?>
	<div class="form-group">
		<label>Range Tanggal:&nbsp;</label>
		<div class="input-group">
			<span class="input-group-addon">
				<i class="icon-calendar bigger-110"></i>
			</span>

			<input class="form-control" name="date-range-picker" id="id-date-range-picker" type="text" value="<?php echo $range; ?>" readonly="readonly">
		</div>
	</div>
	<div class="form-group">
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
			<i class="icon-filter"></i>
			<?php echo lang('global:filters'); ?>
		</button>
		
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-xs btn-clear" type="button">
			<i class="icon-remove"></i>
			<?php echo lang('buttons:clear'); ?>
		</button>
	</div>

<?php echo form_close() ?>
</div>
<?php if($analytics_data != NUll) { ?>
	<div class="row">
		<div class="col-sm-8">
			<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover" id="affilite-table">
				<thead>
					<tr>
						<th>No</th>
						<th><?php echo 'Source'; ?></th>
						<th><?php echo 'Session'; ?></th>
						<th><?php echo '% Session'; ?></th>
					</tr>
				</thead>
				<tfoot>
		            <tr>
		                <th colspan="2" style="text-align:right">Total:</th>
		                <th id="total_session"></th>
		                <th id="total_percent_session"></th>
		            </tr>
		        </tfoot>
				<tbody>
					<?php 
					$no = 1;
					$total_penjualan = 0;
					$total_komisi = 0;
					?>
					
					<?php foreach ($analytics_data as $entry): ?>
					<tr>
						<td><?php echo $no; $no++; ?></td>
						<td><?php echo (isset($entry['0'])) ? $entry['0'] : strtoupper('-'); ?></td>
						<td><?php echo (isset($entry['1'])) ? $entry['1'] : 0; ?></td>
						<td><?php echo (isset($entry['2'])) ? $entry['2'] : 0; ?></td>
					</tr>
					<?php
					endforeach; 
					?>
				</tbody>
			</table>
			</div>
		</div>
		<div class="col-sm-4">
			<div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
			<?php foreach ($analytics_data as $entry): ?>
			<ul style="display:none;">
				<li class="source">
					<span class="source-name"><?php echo (isset($entry['0'])) ? $entry['0'] : strtoupper('-'); ?></span>
					<span class="source-value"><?php echo (isset($entry['1'])) ? $entry['1'] : 0; ?></span>
					<span class="source-percent"><?php echo (isset($entry['2'])) ? $entry['2'] : 0; ?></span>
				</li>
			</ul>
			<?php endforeach; ?>
		</div>
	</div>

	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script>
		function draw_chart() {
			var source,percentage;
			var sourcedata = [];
			$('.source').each(function(k,v) {
				source = $(this).find('.source-name').html();
				percentage = $(this).find('.source-percent').html();
				if(k<9) {
					sourcedata.push({name:source,y:parseFloat(percentage)});
				} else {
					if(sourcedata[9] === undefined) {
						sourcedata.push({name:'others',y:parseFloat(percentage)});
					} else {
						sourcedata[9].y = sourcedata[9].y + parseFloat(percentage);
					}
				}
			});

	        // Build the chart
			$('#container').highcharts({
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false,
					type: 'pie'
				},
				title: {
					text: 'Access Source'
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: true,
							format: '<b>{point.name}</b>: {point.percentage:.1f} %',
						},
						showInLegend: false
					}
				},
				series: [{
					name: 'Source',
					colorByPoint: true,
					data: sourcedata
				}]
			});
		}
	</script>
	
	<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/jquery.dataTables.bootstrap.js"></script>
	<script type="text/javascript">
	var intVal = function (i) {
		return typeof i === 'string' ?
		i.replace(/[\$,]/g, '') * 1 :
		typeof i === 'number' ?
		i : 0;
	};
	$(document).ready(function() {
		/**
		* Number.prototype.format(n, x, s, c)
		* 
		* @param integer n: length of decimal
		* @param integer x: length of whole part
		* @param mixed   s: sections delimiter
		* @param mixed   c: decimal delimiter
		*/
		Number.prototype.format = function(n, x, s, c) {
			var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
			num = this.toFixed(Math.max(0, ~~n));

			return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
		};

		var oTable1 = $('#affilite-table').on( 'init.dt', function () {
				draw_chart();
    		}).dataTable({
			"fnFooterCallback": function ( nRow, aaData, iStart, iEnd, aiDisplay ) {

				var total_session = 0;
				var total_percent_session = 0;
				
				for ( var i=0 ; i<aaData.length ; i++ )
				{
					total_session += intVal(aaData[i][2])*1;
					total_percent_session += intVal(aaData[i][3])*1;
					
					// clean from format
					// penjualan = aaData[i][6];
					// total_penjualan += intVal(penjualan.substring(3)) * 1;
				}

				var nCells = nRow.getElementsByTagName('th');
				// console.log(nCells,nRow,total_pageview);
				nCells[1].innerHTML = total_session.format(0,3,',','.');
				nCells[2].innerHTML = total_percent_session.format(0,3,',','.');
			}
		});
	});

	</script>
<?php } else { ?>
	
	<div class="well">Data Source Tidak Ditemukan</div>
	
<?php } ?>

<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/date-time/moment.min.js"></script>
<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/date-time/daterangepicker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#id-date-range-picker').daterangepicker({
			"autoApply": true,
    		"autoUpdateInput" : true,
			"locale":{
				format:'YYYY-MM-DD'
			},
    		"maxDate": moment(),
		}).prev().on(ace.click_event, function(){
			$(this).next().focus();
		});
		
		$('.btn-clear').click(function() {
			$(this).closest('form').find('input').val('');
		});
	});
</script>