<div class="page-header">
	<h1><?php echo lang('commerce:produk:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama_kurir"><?php echo lang('commerce:kode_kurir'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('kode_kurir') != NULL){
					$value = $this->input->post('kode_kurir');
				}elseif($this->input->get('f-kode_kurir') != NULL){
					$value = $this->input->get('f-kode_kurir');
				}elseif($mode == 'edit'){
					$value = $fields['kode_kurir'];
				}
			?>
			<input name="kode_kurir" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama_kurir"><?php echo lang('commerce:nama_kurir'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama_kurir') != NULL){
					$value = $this->input->post('nama_kurir');
				}elseif($this->input->get('f-nama_kurir') != NULL){
					$value = $this->input->get('f-nama_kurir');
				}elseif($mode == 'edit'){
					$value = $fields['nama_kurir'];
				}
			?>
			<input name="nama_kurir" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="is_active"><?php echo lang('commerce:is_active'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('is_active') != NULL){
					$value = $this->input->post('is_active');
				}elseif($this->input->get('f-is_active') != NULL){
					$value = $this->input->get('f-is_active');
				}elseif($mode == 'edit'){
					$value = $fields['is_active'];
				}
			?>
			
			<label>
				<input <?php echo ($value==1) ? 'checked="checked"' : ''; ?> name="is_active" class="ace ace-switch ace-switch-7" type="checkbox" value="1">
				<span class="lbl"></span>
			</label>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>