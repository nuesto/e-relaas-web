<div class="page-header">
	<h1><?php echo lang('commerce:rekening:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama_bank"><?php echo lang('commerce:nama_bank'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama_bank') != NULL){
					$value = $this->input->post('nama_bank');
				}elseif($this->input->get('f-nama_bank') != NULL){
					$value = $this->input->get('f-nama_bank');
				}elseif($mode == 'edit'){
					$value = $fields['nama_bank'];
				}
			?>
			<input name="nama_bank" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="no_rekening"><?php echo lang('commerce:no_rekening'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('no_rekening') != NULL){
					$value = $this->input->post('no_rekening');
				}elseif($this->input->get('f-no_rekening') != NULL){
					$value = $this->input->get('f-no_rekening');
				}elseif($mode == 'edit'){
					$value = $fields['no_rekening'];
				}
			?>
			<input name="no_rekening" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="atas_nama"><?php echo lang('commerce:atas_nama'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('atas_nama') != NULL){
					$value = $this->input->post('atas_nama');
				}elseif($this->input->get('f-atas_nama') != NULL){
					$value = $this->input->get('f-atas_nama');
				}elseif($mode == 'edit'){
					$value = $fields['atas_nama'];
				}
			?>
			<input name="atas_nama" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>