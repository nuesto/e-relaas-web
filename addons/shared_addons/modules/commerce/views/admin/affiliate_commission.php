<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/daterangepicker.css" />
<div class="page-header">
	<h1><?php echo lang('commerce:affiliate_commission'); ?></h1>
	
	<div class="btn-group content-toolbar">
		<a class="btn btn-info btn-sm" href="<?php echo base_url(); ?>admin/commerce/affiliate/download_commission<?php echo $uri; ?>">
			<i class="icon-download"></i>			
			<span class="no-text-shadow">Download CSV</span>
		</a>
	</div>
</div>

<?php echo form_open('', array('class' => 'form-inline', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
	<div class="form-group">
		<label>Range Tanggal:&nbsp;</label>
		<div class="input-group">
			<span class="input-group-addon">
				<i class="icon-calendar bigger-110"></i>
			</span>

			<input class="form-control" name="date-range-picker" id="id-date-range-picker" type="text" value="<?php echo $range; ?>" readonly="readonly">
		</div>
	</div>
	<div class="form-group">
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
			<i class="icon-filter"></i>
			<?php echo lang('global:filters'); ?>
		</button>
		
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-xs btn-clear" type="button">
			<i class="icon-remove"></i>
			<?php echo lang('buttons:clear'); ?>
		</button>
	</div>

<?php echo form_close() ?>

<?php if ($commission['total'] > 0): ?>
	<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover" id="affilite-table">
		<thead>
			<tr>
				<th>No</th>
				<th><?php echo lang('commerce:email_affiliate'); ?></th>
				<th><?php echo lang('commerce:nama_affiliate'); ?></th>
				<th><?php echo lang('commerce:no_rekening'); ?></th>
				<th><?php echo lang('commerce:jumlah_pesanan'); ?><br><span style="font-size:9px">*Berdasarkan waktu pesanan selesai</span></th>
				<th><?php echo lang('commerce:total_penjualan'); ?></th>
				<th><?php echo lang('commerce:komisi'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$no = 1;
			$total_penjualan = 0;
			$total_komisi = 0;
			?>
			
			<?php foreach ($commission['entries'] as $commission_entry): ?>
			<tr>
				<td><?php echo $no; $no++; ?></td>
				<td><?php echo $commission_entry['email']; ?></td>
				<td><?php echo ucwords($commission_entry['display_name']); ?></td>
				<!-- ucwords($commission_entry['nama_bank']).' '.$commission_entry['no_rekening'].' a.n '.ucwords($commission_entry['atas_nama']) -->
				<?php
				$rekening_list = array();
				if(isset($commission_entry['rekening'])) {
					$bullet = (count($commission_entry['rekening']) > 1) ? '- ' : '';
					foreach ($commission_entry['rekening'] as $rekening_entry) {
						$rekening_list[] = $bullet.ucwords($rekening_entry['nama_bank']).' '.$rekening_entry['no_rekening'].' a.n '.ucwords($rekening_entry['atas_nama']);
					}
				}

				?>
				<td><?php echo (isset($commission_entry['rekening'])) ? implode("<br>", $rekening_list) : 'Belum Memasukkan No Rekening'; ?></td>
				<td><?php echo (isset($commission_entry['jumlah_pesanan'])) ? $commission_entry['jumlah_pesanan'] : 0; ?></td>
				<td>Rp <?php echo number_format($commission_entry['total_penjualan'],0,'.',','); ?></td>
				<td>Rp <?php echo number_format($commission_entry['komisi'],0,'.',','); ?></td>
			</tr>
			<?php 
			$total_penjualan += $commission_entry['total_penjualan'];
			$total_komisi += $commission_entry['komisi'];
			endforeach; 
			?>
		</tbody>
	</table>
	</div>
	<div class="row">
		<div class="col-sm-12">
		<div class="pull-right text-right" style="width:150px;"><strong>Rp <?php echo number_format($total_penjualan,0,'.','.'); ?></strong></div>
		<div class="pull-right text-right"><strong>Total Penjualan Affiliate : </strong></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
		<div class="pull-right text-right" style="width:150px;"><strong>Rp <?php echo number_format($total_komisi,0,'.','.'); ?></strong></div>
		<div class="pull-right text-right"><strong>Total Komisi : </strong></div>
		</div>
	</div>
	<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/jquery.dataTables.bootstrap.js"></script>
	<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/date-time/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/date-time/daterangepicker.min.js"></script>
	<script type="text/javascript">
	jQuery(function($) {
		var oTable1 = $('#affilite-table').dataTable();
	});

	$(document).ready(function() {
		$('#id-date-range-picker').daterangepicker({
			"autoApply": true,
    		"autoUpdateInput" : true,
			"locale":{
				format:'YYYY-MM-DD'
			},
    		"maxDate": moment(),
		}).prev().on(ace.click_event, function(){
			$(this).next().focus();
		});
		
		$('.btn-clear').click(function() {
			$(this).closest('form').find('input').val('');
		});
	});
	</script>
	
<?php else: ?>
	<div class="well"><?php echo lang('commerce:affiliate:no_entry'); ?></div>
<?php endif;?>
