<style type="text/css">
	.tab-v1 nav {
		overflow-x: auto;
		overflow-y: hidden;
	}
	@media only screen and (max-width: 360px) {
		.tab-v1 nav .nav-tabs {
			width: 285px;
		}
	}
</style>
<div class="tab-v1">
	<nav>
		<ul class="nav nav-tabs">
			<li class="current <?php echo ($state_order=='current') ? 'active' : ''; ?>"><a href="<?php echo base_url().'commerce/order/my_order/current'; ?>">Pesanan Saat Ini</a></li>
			<li class="finished <?php echo ($state_order=='finished') ? 'active' : ''; ?>"><a href="<?php echo base_url().'commerce/order/my_order/finished'; ?>">Pesanan Selesai</a></li>
		</ul>
	</nav>
	
	<div class="tab-content">
		<div class="tab-pane fade in active" id="pesanan-sekarang">
			{{ theme:partial name='notices' }}
			<?php if(count($orders['entries'])>0) { ?>
			<p class="pull-right"><?php echo lang('commerce:showing').' '.count($orders['entries']).' '.lang('commerce:of').' '.$orders['total'] ?></p>
			<div class="col-sm-12">
				<?php foreach ($orders['entries'] as $order) { ?>
				<div class="row list-item">
					<div class="col-sm-12">
						<div class="row order-summary">
							<div class="col-sm-3">
								<span>TANGGAL TRANSAKSI</span><br>
								<span><strong>
								<?php echo (isset($order['placed_on'])) ? date('d M Y H:i', strtotime($order['placed_on'])) : date('d M Y H:i', strtotime($order['created_on'])); ?>
								</strong></span><br>
							</div>
							<div class="col-sm-3">
								<span>TOTAL PESANAN</span><br>
								<span><strong>
								<?php 
								$sub_total = $order['biaya_order']-$order['discount_nominal'];
								$sub_total = ($sub_total<0) ? 0 : $sub_total;
								$total = $sub_total + $order['biaya_pengiriman'] + $order['kode_unik'];
								echo 'Rp '.number_format($total,0,',','.'); 
								?>
								</strong></span>
							</div>
							<div class="col-sm-3">
								<span>STATUS PESANAN</span><br>
								<?php
								$status_pesanan_severity = array(
									'in-cart' => 'info',
									'waiting-payment' => 'warning',
									'expired' => 'default',
									'cancelled' => 'danger',
									'completed' => 'success',
									);

								$status_pembayaran_severity = array(
									'unpaid' => 'danger',
									'expired' => 'default',
									'cancelled' => 'default',
									'pending' => 'warning',
									'paid' => 'success',
									'declined' => 'default',
									);
								?>
								<span style="text-transform: capitalize;padding:5px;" class="<?php echo 'label-'.$status_pesanan_severity[strtolower($order['status_pesanan'])]; ?>"><strong><?php 
								$status_pesanan = ($order['payment_method']=='transfer'AND $order['status_pesanan']=='waiting-payment' AND $order['status_pembayaran']=='pending') ? 'waiting-approval' : $order['status_pesanan'];
								echo $status_pesanan; ?></strong></span><br>
								<?php $payment_method = array('transfer'=>'Transfer','free'=>'Gratis','ipaymu'=>'iPaymu')?>
								<span><?php echo (!isset($order['payment_method'])) ? 'Transfer' : ucwords($payment_method[$order['payment_method']]); ?></span>
							</div>
							<div class="col-sm-3">
								<span>NO INVOICE : <span><br><strong><?php echo $order['no_invoice']; ?></strong></span>
							</div>
							<?php if($order['status_pesanan']=='waiting-payment' AND isset($order['expired_on'])) { ?>
							<div class="col-sm-12">
								<div class="order-notice alert alert-warning clearfix">
									<div class="order-notice-icon">
										<span class="fa fa-exclamation-triangle  color-orange"></span>	
									</div>
									<div class="order-notice-message">
										<?php
										$expired_time = date('Y-m-d H:i',strtotime($order['expired_on']));
										$days = array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
										$ex_day = date('w',strtotime($expired_time));
										$ex_date = date('d',strtotime($expired_time));
										$months = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
										$ex_month = date('n',strtotime($expired_time));
										$ex_year = date('Y',strtotime($expired_time));
										$ex_time = date('H:i',strtotime($expired_time));
										?>
										Batas Akhir Pembayaran <br><strong class="color-orange"><?php echo 'Hari '.$days[$ex_day].', '.$ex_date.' '.$months[$ex_month-1].' '.$ex_year.'<br>pukul '.$ex_time.' WIB'; ?></strong>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<div class="row order-detail">
							<div class="col-sm-12">
								<div class="row">
									<div class="col-sm-6">
										<h6>PRODUK</h6>
										<ul class="media-list">
											<?php foreach ($order['cart'] as $cart) { ?>
											<li class="media">
												<div class="media-left">
													<a href="#">
														<img src="<?php echo base_url().'uploads/default/files/'.$cart['filename']; ?>" alt="" style="width:65px;">
													</a>
												</div>
												<div class="media-body">
													<span class="product-name media-heading"><?php echo $cart['nama']; ?></span><br>
													<span>Jumlah : <?php echo $cart['jumlah']; ?></span><br>
													<span class="color-orange"><strong>Rp <?php echo number_format($cart['harga'],0,',','.'); ?></strong></span> /unit
												</div>
											</li>
											<?php } ?>
										</ul>
									</div>
									<div class="col-sm-6">
										<h6>History</h6>
										<p>
											<?php if(isset($order['state_history']) AND count(json_decode($order['state_history']))) { ?>
											<ul style="padding-left: 10px;">
												<?php foreach (json_decode($order['state_history'],true) as $history) {
													echo '<li>'.date('d M Y H:i',strtotime($history['time'])).' '.$history['status'].'</li>';
												}?>
											</ul>
											<?php } else { echo '-'; }?>
										</p>
									</div>
								</div>
								<div class="setting-btn-koncel" style="padding:15px;">
									<div class="row action-button">
											<?php 
											$no_invoice = '?no_invoice=' . $order['no_invoice'];
											if(group_has_role('commerce', 'confirm_order') and $order['status_pesanan'] == 'waiting-payment' and ($order['status_pembayaran']==null OR $order['status_pembayaran']=='' OR $order['status_pembayaran']=='unpaid')){
												// echo anchor('commerce/order/confirm'.$no_invoice, lang('commerce:konfirmasi_pembayaran'), 'class="pull-right btn btn-xs btn-u view confirm-payment" data-noinvoice="'.$no_invoice.'"');
												if($order['payment_method']=='transfer' OR $order['payment_method']=='' OR $order['payment_method']==null) {

												echo '<span class="pull-right btn btn-confirm btn-xs btn-u view confirm-payment" data-noinvoice="'.$order['no_invoice'].'" data-idorder="'.$order['id'].'" data-toggle="modal" data-target="#confirm-modal">'.lang('commerce:konfirmasi_pembayaran').'</span>';
												}

												if(isset($order['payment_method']) AND $order['payment_method']=='ipaymu') {

												echo anchor($order['ipaymu_url'],'Bayar Sekarang','class="pull-right btn btn-xs btn-u"');
												}
											}

											if(in_array($order['status_pesanan'], array('in-cart','waiting-payment'))) {
												echo anchor('commerce/order/cancel/'.$order['id'], lang('commerce:batal_transaksi'), 'class="pull-right btn btn-xs btn-u btn-batal btn-u-default edit confirm"');
											}
											?>

											<?php 
											if($order['status_pesanan']=='produk telah dikirim'){
												echo anchor('commerce/order/arrived'.$no_invoice, lang('commerce:diterima'), 'class="pull-right btn btn-xs btn-u btn-batal edit confirm"');
											}
											?>
										
									</div>
							</div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>

				<div class="text-center">
					<?php echo $orders['pagination']; ?>
				</div>
			</div>
			<?php } else { ?>
				<div class="well"><?php echo lang('commerce:order:no_entry'); ?></div>
			<?php } ?>
		</div>
	</div>
</div>

<div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Konfirmasi Pembayaran</h4>
			</div>
			<?php echo form_open_multipart(base_url().'commerce/order/confirm'.$uri,array('class'=>'form-horizontal','id'=>'confirmation-form')); ?>
				<div class="modal-body">
					<div class="form-group">
						<label for="no_invoice" class="col-sm-4 control-label">No invoice</label>
						<div class="col-sm-6">
							<input name="id_order" type="hidden" class="form-control" id="id_order">
							<input name="no_invoice" type="text" class="form-control" id="no_invoice" readonly="readonly">
						</div>
					</div>

					<div class="form-group">
						<label for="total_tagihan" class="col-sm-4 control-label">Total Tagihan</label>
						<div class="col-sm-6">
							<div class="input-group">
								<span class="input-group-addon">Rp </span>
								<input name="total_tagihan" readonly="readonly" type="text" class="form-control" id="total_tagihan">
							</div>
						</div>
					</div>

					<hr>

					<div class="form-group">
						<label for="bank_rekening_pengirim" class="col-sm-4 control-label">Bank Rekening Pengirim</label>
						<div class="col-sm-6">
							<input name="bank_rekening_pengirim" type="text" class="form-control" id="bank_rekening_pengirim">
						</div>
					</div>

					<div class="form-group">
						<label for="nama_rekening_pengirim" class="col-sm-4 control-label">Nama Rekening Pengirim</label>
						<div class="col-sm-6">
							<input name="nama_rekening_pengirim" type="text" class="form-control" id="nama_rekening_pengirim">
						</div>
					</div>

					<div class="form-group">
						<label for="no_rekening_pengirim" class="col-sm-4 control-label">No Rekening Pengirim</label>
						<div class="col-sm-6">
							<input name="no_rekening_pengirim" type="text" class="form-control" id="no_rekening_pengirim">
						</div>
					</div>

					<hr>
					<div class="form-group">
						<label for="bank_rekening_tujuan" class="col-sm-4 control-label">Bank Rekening Tujuan</label>
						<div class="col-sm-6">
							<select class="form-control" name="bank_rekening_tujuan">
								<option value="">-- Pilih Rekening Tujuan --</option>
								<?php
								foreach ($rekening as $rek) {
									$selected = '';
									echo '<option '.$selected.' value="'.$rek['id'].'">Bank '.$rek['nama_bank'].', Nomor '.$rek['no_rekening'].' a.n '.$rek['atas_nama'].'</option>';
								}
								?>
							</select>

						</div>
					</div>

					<div class="form-group">
						<label for="jumlah_pembayaran" class="col-sm-4 control-label">Jumlah Pembayaran</label>
						<div class="col-sm-6">
							<div class="input-group">
								<span class="input-group-addon">Rp </span>
								<input name="jumlah_pembayaran" type="text" class="form-control number" id="priceformat">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="tanggal_transfer" class="col-sm-4 control-label">Tanggal Transfer</label>
						<div class="col-sm-6">
							<input type="text" name="tanggal_transfer" id="tanggal_transfer" class="form-control" readonly="readonly" style="background-color:#ffffff;">
						</div>
					</div>

					<div class="form-group">
						<label for="bukti_pembayaran" class="col-sm-4 control-label">Bukti Pembayaran (optional)</label>
						<div class="col-sm-6">
							<input name="bukti_pembayaran" type="file" id="bukti_pembayaran">
						</div>
					</div>

					<div class="form-group">
						<label for="keterangan" class="col-sm-4 control-label">Keterangan (optional)</label>
						<div class="col-sm-6">
							<textarea name="keterangan" class="form-control" rows="3"></textarea>
						</div>
					</div>

				</div>
				<div class="modal-footer transfer">
					<button type="button" class="btn btn-u-default" data-dismiss="modal">Batal</button>
					<input type="submit" class="btn btn-u" value="<?php echo lang('buttons:save'); ?>"></input>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="confirm-dompet-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Pembayaran dengan Dompet</h4>
			</div>
			<?php echo form_open_multipart(base_url().'commerce/order/confirm_dompet'.$uri,array('class'=>'form-horizontal','id'=>'confirm-dompet-form')); ?>

				<div class="modal-body">
					<div class="alert alert-danger order-notice code_not_match" style="display:none;">
		              <button data-dismiss="alert" class="close" type="button">
		                <i class="icon-remove"></i>
		              </button>
		              <div><strong>Kode OTP</strong> anda tidak cocok, silahkan coba lagi.</div>
		            </div>
		            <div class="alert alert-danger order-notice code_expired" style="display:none;">
		              <button data-dismiss="alert" class="close" type="button">
		                <i class="icon-remove"></i>
		              </button>
		              <div><strong>Kode OTP</strong> anda sudah expired. Silahkan request kode baru.</div>
		            </div>
					<div class="form-group">
						<label for="no_invoice" class="col-sm-4 control-label">No invoice</label>
						<div class="col-sm-6">
							<input name="id_order" type="hidden" class="form-control" id="id_order">
							<input name="no_invoice" type="text" class="form-control" id="no_invoice" readonly="readonly">
						</div>
					</div>

					<div class="form-group">
						<label for="total_tagihan" class="col-sm-4 control-label">Total Tagihan</label>
						<div class="col-sm-6">
							<div class="input-group">
								<span class="input-group-addon">Rp </span>
								<input name="total_tagihan" readonly="readonly" type="text" class="form-control" id="total_tagihan">
							</div>
						</div>
					</div>

					<hr>

					<div class="form-group">
						<label for="jumlah_pembayaran" class="col-sm-4 control-label">Saldo Anda</label>
						<div class="col-sm-6">
							<div class="input-group">
								<span class="input-group-addon">Rp </span>
								<input name="saldo" type="text" class="form-control number" id="priceformat">
								<input name="id_dompet" id="id_dompet" type="text" class="form-control number" id="priceformat">
								<input name="id" id="id" type="text" class="form-control number" id="priceformat">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="jumlah_pembayaran" class="col-sm-4 control-label">OTP* <span style="font-size: 9px;">(One Time Password)</span></label>
						<div class="col-sm-6">
							<input name="otp" id="otp" type="text" class="form-control" maxlength="6" style="width: 28%;display: inline;">
							<button name="otp_send" id="otp_send" type="button" name="" class="btn btn-primary btn-md" style="display: inline;margin-top: -4px;">Kirim OTP</button>
							<div style="font-size:11px">Telah mengirim OTP <label id="count_send">0</label> kali.</div>
                        	<div class="otp_send_success" style="display:none;font-size:11px">OTP telah terkirim ke <?php echo $this->current_user->email; ?></div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-u-default" data-dismiss="modal">Batal</button>
					<button name="request_button_submit" disabled="disabled" id="request_button_submit" type="submit" name="" class="btn btn-u" disabled="disabled"><?php echo lang('commerce:pay'); ?></button>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="{{ url:site }}{{ theme:path }}/plugins/jqueryui/jquery-ui.js"></script>
<script type="text/javascript">
	$(function() {
		$( "#tanggal_transfer" ).datepicker({
			dateFormat : 'yy-mm-dd',
			maxDate : '0'
		});
	});
</script>

{{ theme:js file="bootbox.min.js" }}
<script type="text/javascript">
	/****************************
	 *	CONFIRMATION DIALOG
	 ****************************/
	$(document).ready(function(){
		$(".confirm").on('click', function(event) {
			event.preventDefault();
			var link = $(this).attr('href');
			setTimeout(function() {
				$('.modal-footer .btn-primary').removeClass('btn-primary').addClass('btn-default');
			}, 100);
			
			bootbox.confirm("Anda yakin akan menjalankan aksi ini? Aksi ini tidak dapat dikembalikan.", function(result) {
				if(result) {
					window.location.href = link;
				}
			});
		});
	});
</script>

<script type="text/javascript">
	var counter = 15;
	var send_counter = 0;
	$('#confirm-modal').on('show.bs.modal',function(event) {
		// remove alert from before
		$(".alert:not(.order-notice)").remove();

		var id_order = $(event.relatedTarget).data('idorder');
		$('#confirm-modal input[name=id_order]').val(id_order);
		get_order();
	});

	$('#confirm-dompet-modal').on('show.bs.modal',function(event) {
		// remove alert from before
		$(".alert:not(.order-notice)").remove();
		$("input[name='saldo']").val("Sedang mengambil saldo ...");
		$("input[name='saldo']").attr("disabled",true);
		var id_order = $(event.relatedTarget).data('idorder');
		$('#confirm-modal input[name=id_order]').val(id_order);
		get_order();
		get_saldo();
	});

	$('input[name=no_invoice]').keyup(function() {
		get_order();
	});

	function get_saldo(){
		$.ajax({
			url : "<?php echo base_url(); ?>dompet/saldo/get_saldo_ajax",
			type : 'GET',
				timeout : 60000,
				success : function(result) {
					var tmp = $.parseJSON(result);
					console.log(result);
					if(tmp[0] == "success"){
						$("input[name='saldo']").val(tmp[1]['saldo']);
						$("input[name='id_dompet']").val(tmp[1]['id']);
						format_number();
					}else{

					}
					
					
				}
		})
	}

	function get_order() {
		var id_order = $('input[name=id_order]').val();
		if(id_order != '') {
			$('input[name=no_invoice]').val('Sedang ambil data....');
			$('input[name=total_tagihan]').val('Sedang ambil data....');
			$('input[name=order]').val('');
			$.ajax({
				url : '<?php echo base_url(); ?>commerce/order/get_order_by_id',
				data : 'id_order='+id_order,
				dataType : 'JSON',
				type : 'GET',
				timeout : 60000,
				success : function(rst) {
					if(rst.payment_method=='transfer' || rst.payment_method===null) {
						if(rst.total_tagihan==0) {
							$('input[name=total_tagihan]').val('Pesanan tidak ditemukan.');
							$('input[name=order]').val('');
						} else {
							if(rst.status_pesanan=='expired') {
								$('#confirm-modal .modal-body').html('Pesanan dengan No Invoice #'+rst.no_invoice+' telah expired');
								$('#confirm-modal .modal-footer input[type=submit]').remove();
							} else {
								$('input[name=no_invoice]').val(rst.no_invoice);
								$('input[name=total_tagihan]').val(rst.total_tagihan);
								$('input[name=order]').val(rst.id);
								$('input[name=id]').val(rst.id_payment);
								send_counter = (rst.resend_attempt) ? rst.resend_attempt : 0;

								$("#count_send").html(send_counter);
							}
						}
					}
				},
				error : function(xhr) {
					alert('Maaf ada masalah saat pengambilan data, "'+xhr.statusText+'"');
				}
			});
		} else {
			$('input[name=total_tagihan]').val('');
			$('input[name=order]').val('');
		}
	}

	$(document).ready(function() {
		$("#otp").on("keyup",function(){
	      if($(this).val().length != 6){
	        $("#request_button_submit").attr("disabled","disabled");
	      }else{
	        $("#request_button_submit").attr("disabled",false);
	      }
	    });

		$("#confirmation-form").submit(function(event){
			//disable the default form submission
			event.preventDefault();

			// remove alert from before
			$(".alert:not(.order-notice)").remove();

			// disable button and change the text
			$("#confirmation-form input[type=submit]").attr('disabled',true);
			var label = $("#confirmation-form input[type=submit]").val();
			$("#confirmation-form input[type=submit]").val('loading...');

			var url = $(this).attr('action');

			//grab all form data  
			var formData = new FormData($(this)[0]);

			$.ajax({
				url: url,
				type: 'POST',
				data: formData,
				async: false,
				cache: false,
				contentType: false,
				processData: false,
				dataType:'JSON',
				success: function (returndata) {
					$('#confirmation-form .modal-body').prepend(returndata.message);

					setTimeout(function() {
						$("#confirmation-form input[type=submit]").removeAttr('disabled');
						$("#confirmation-form input[type=submit]").val(label);

						window.location.hash = '#confirmation-form';
					},1000);

					if(returndata.status == 'success') {
						setTimeout(function() {
							window.location.href = "<?php echo base_url().'commerce/order/my_order'; ?>"
						},2000);
					}
				},
				error: function (xhr) {
					$('#confirmation-form .modal-body').prepend('<div class="alert alert-danger">Terjadi error saat request ke server. '+xhr.statusText+'</div>');
					setTimeout(function() {
						$("#confirmation-form input[type=submit]").removeAttr('disabled');
						$("#confirmation-form input[type=submit]").val(label);

						window.location.hash = '#confirmation-form';
					},1000);
				},
				timeout: 120000
			});

			return false;
		});

		$("#otp_send").on("click",function(event){
	      send_counter++;
	      $("#count_send").html(send_counter);
	      counter = 15;
	      $("#otp").attr("disabled",true);
	      $(this).attr("disabled",true);
	      $(this).html("Mengirim OTP ...");

	      event.preventDefault();
	        var form = $( this ),
	        formData = new FormData();
	        formData.append('id', $("#id").val());
	        $.ajax({
	          type: "POST",
	          url: "<?php echo base_url().'dompet/saldo/send_otp' ?>",
	          data: formData,
	          contentType: false,
	          processData: false,
	          success: function(result){
	          	console.log(result);
	            if(result == "sukses"){
	              callTimer();
	              $("#otp").attr("disabled",false);
	            }else{
	              console.log(result);
	            }
	          }
	        });
	    });
	});

	setTimeout(function() {
		$('.alert:not(.order-notice)').remove();
	},2000);


	$.urlParam = function(name){
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		if (results==null){
			return null;
		}else{
			return results[1] || 0;
		}
	}

	$(document).ready(function() {
		$("#request_button_submit").on("click",function(){
	      var form = $( this ),
	      formData = new FormData();
	      formData.append('otp', $("input[name='otp']").val());
	      formData.append('id', $("input[name='id']").val());
	      $.ajax({
	        type: "POST",
	        url: "<?php echo base_url().'dompet/saldo/is_otp_match_ajax' ?>",
	        data: formData,
	        contentType: false,
	        processData: false,
	        success: function(result){
	          console.log(result);
	          if(result == "match"){
	            $("#confirm-dompet-form").submit();
	            $(".code_not_match").hide();
	            $(".code_expired").hide();
	          }else if(result == "not match"){
	            console.log(result);
	            $(".code_not_match").show();
	            $(".code_expired").hide();
	          }else{
	            $(".code_not_match").hide();
	            $(".code_expired").show();
	          }
	        }
	      });
	      return false;
	    });

		if($.urlParam('id_order')!==null) {
			var id_order = $.urlParam('id_order');
			$('#confirm-modal').modal('show');
			$('#confirm-modal input[name=id_order]').val(id_order);
			get_order();
		}


		$('input.number').keyup(function(event) {

			// skip for arrow keys
			if(event.which >= 37 && event.which <= 40) return;

			// format number
			$(this).val(function(index, value) {
				return value
				.replace(/\D/g, "")
				.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
				;
			});
		});
	});

	function format_number(){
		if(event.which >= 37 && event.which <= 40) return;

		// format number
		$("input[name='saldo']").val(function(index, value) {
			return value
			.replace(/\D/g, "")
			.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
			;
		});
	}

	function callTimer(){
	    if(counter > 0){
	      $("#otp_send").html("Kirim OTP ("+counter +"s)");
	      counter--;
	      $("#otp_send").attr("disabled",true);
	      $(".otp_send_success").show();
	      setTimeout(callTimer,1000);
	    }else{
	      $("#otp_send").html("Kirim OTP").attr("disabled",false);
	      $(".otp_send_success").fadeOut();
	    }
  	}	

</script>
