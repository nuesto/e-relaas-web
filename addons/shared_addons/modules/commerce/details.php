<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Commerce extends Module
{
    public $version = '1.2.3';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Commerce',
			'id' => 'Commerce',
		);
		$info['description'] = array(
			'en' => 'Modul untu me-manage simple commerce',
			'id' => 'Modul untu me-manage simple commerce',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		$info['menu'] = 'Commerce';
		$info['roles'] = array(
			'access_order_backend', 'view_all_order', 'view_own_order', 'view_affiliate_order', 'edit_all_order', 'edit_own_order', 'edit_order_status', 'delete_all_order', 'delete_own_order', 'create_order', 'confirm_order',
			'access_payment_backend', 'view_all_payment', 'view_own_payment', 'view_affiliate_payment', 'edit_all_payment', 'edit_own_payment', 'edit_payment_status', 'delete_all_payment', 'delete_own_payment', 'create_payment', 'confirm_payment',
			'access_cart_backend', 'view_all_cart', 'view_own_cart', 'edit_all_cart', 'edit_own_cart', 'delete_all_cart', 'delete_own_cart', 'create_cart',
			'access_produk_backend', 'view_all_produk', 'view_own_produk', 'edit_all_produk', 'edit_own_produk', 'delete_all_produk', 'delete_own_produk', 'create_produk',
			'access_rekening_backend', 'view_all_rekening', 'view_own_rekening', 'edit_all_rekening', 'edit_own_rekening', 'delete_all_rekening', 'delete_own_rekening', 'create_rekening',
			'access_pengiriman_backend', 'view_all_pengiriman', 'view_own_pengiriman', 'edit_all_pengiriman', 'edit_own_pengiriman', 'delete_all_pengiriman', 'delete_own_pengiriman', 'create_pengiriman',
			'view_all_affiliate','view_affiliate_comission','view_affiliate_statistics','view_own_statistics',
			'access_voucher_backend', 'view_all_voucher', 'view_own_voucher', 'edit_all_voucher', 'edit_own_voucher', 'delete_all_voucher', 'delete_own_voucher', 'create_voucher', 'create_template_voucher',
			'view_customer_mobile',
		);

		if(group_has_role('commerce', 'access_produk_backend')){
			$info['sections']['produk']['name'] = 'commerce:produk:plural';
			$info['sections']['produk']['uri'] = array('urls'=>array('admin/commerce/produk/index','admin/commerce/produk/create','admin/commerce/produk/view%1','admin/commerce/produk/edit%1','admin/commerce/produk/view%2','admin/commerce/produk/edit%2'));

			if(group_has_role('commerce', 'create_produk')){
				$info['sections']['produk']['shortcuts']['create'] = array(
					'name' => 'commerce:produk:new',
					'uri' => 'admin/commerce/produk/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('commerce', 'access_order_backend') OR (isset($this->current_user->is_affiliate) AND $this->current_user->is_affiliate == 1)){
			$info['sections']['order']['name'] = 'commerce:order:plural';
			$info['sections']['order']['uri'] = array('urls'=>array('admin/commerce/order/index','commerce/order/create?product=2','admin/commerce/order/view%1','admin/commerce/order/edit%1','admin/commerce/order/view%2','admin/commerce/order/edit%2'));

			if(group_has_role('commerce', 'create_order')){
				$info['sections']['order']['shortcuts']['create'] = array(
					'name' => 'commerce:order:new',
					'uri' => 'commerce/order/create?product=2',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('commerce', 'access_rekening_backend') OR (isset($this->current_user->is_affiliate) AND $this->current_user->is_affiliate == 1)){
			$info['sections']['rekening']['name'] = 'commerce:rekening:plural';
			$info['sections']['rekening']['uri'] = array('urls'=>array('admin/commerce/rekening/index','admin/commerce/rekening/create','admin/commerce/rekening/view%1','admin/commerce/rekening/edit%1','admin/commerce/rekening/view%2','admin/commerce/rekening/edit%2'));

			if(group_has_role('commerce', 'create_rekening') OR (isset($this->current_user->is_affiliate) AND $this->current_user->is_affiliate == 1)){
				$info['sections']['rekening']['shortcuts']['create'] = array(
					'name' => 'commerce:rekening:new',
					'uri' => 'admin/commerce/rekening/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('commerce', 'access_pengiriman_backend')){
			$info['sections']['pengiriman']['name'] = 'commerce:pengiriman:plural';
			$info['sections']['pengiriman']['uri'] = array('urls'=>array('admin/commerce/pengiriman/index','admin/commerce/pengiriman/create','admin/commerce/pengiriman/view%1','admin/commerce/pengiriman/edit%1','admin/commerce/pengiriman/view%2','admin/commerce/pengiriman/edit%2'));

			if(group_has_role('commerce', 'create_pengiriman')){
				$info['sections']['pengiriman']['shortcuts']['create'] = array(
					'name' => 'commerce:pengiriman:new',
					'uri' => 'admin/commerce/pengiriman/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('commerce', 'access_voucher_backend') OR (isset($this->current_user->is_affiliate) AND $this->current_user->is_affiliate == 1)){
			$info['sections']['voucher']['name'] = 'commerce:voucher:plural';
			$info['sections']['voucher']['uri'] = array('urls'=>array('admin/commerce/voucher/index','admin/commerce/voucher/create','admin/commerce/voucher/view%1','admin/commerce/voucher/edit%1','admin/commerce/voucher/view%2','admin/commerce/voucher/edit%2'));

			if(group_has_role('commerce', 'create_voucher') OR (isset($this->current_user->is_affiliate) AND $this->current_user->is_affiliate == 1)){
				$info['sections']['voucher']['shortcuts']['create'] = array(
					'name' => 'commerce:voucher:new',
					'uri' => 'admin/commerce/voucher/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('commerce', 'view_affiliate_comission')){
			$info['sections']['affiliate_commission']['name'] = 'commerce:affiliate_commission';
			$info['sections']['affiliate_commission']['uri'] = array('urls'=>array('admin/commerce/affiliate/commission'));
		}

		if(group_has_role('commerce', 'view_all_affiliate')){
			$info['sections']['affiliate']['name'] = 'commerce:affiliate_link';
			$info['sections']['affiliate']['uri'] = array('urls'=>array('admin/commerce/affiliate/index'));
		}

		if(group_has_role('commerce', 'view_affiliate_statistics') OR group_has_role('commerce', 'view_own_statistics') OR (isset($this->current_user->is_affiliate) AND $this->current_user->is_affiliate == 1)){
		// if(group_has_role('commerce', 'view_affiliate_statistics') OR group_has_role('commerce', 'view_own_statistics')) {
			$info['sections']['affiliate_statistics']['name'] = 'commerce:affiliate_statistics';
			$info['sections']['affiliate_statistics']['uri'] = array('urls'=>array('admin/commerce/affiliate/statistics'));
		}

		return $info;
    }

	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items){
		
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

        // produk
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'harga' => array(
				'type' => 'INT',
			),
			'deskripsi' => array(
				'type' => 'TEXT',
			),
			'id_file' => array(
				'type' => 'CHAR',
				'constraint' => 15,
			),
			'berat' => array(
				'type' => 'INT',
				'constraint' => 15,
			),
			'id_kota_pengirim' => array(
				'type' => 'INT',
				'constraint' => 15,
			),
			'is_digital_product' => array(
				'type' => 'TINYINT',
				'default' => 0,
				),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('commerce_produk', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_commerce_produk(created_by)");

		// pengiriman
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
				),
			'kode_kurir' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				),
			'nama_kurir' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				),
			'is_active' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 0,
				),
			'created_on' => array(
				'type' => 'DATETIME',
				),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
				),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
				),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
				),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
				),
			);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('commerce_pengiriman', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_commerce_pengiriman(created_by)");

		// pengiriman
		$fields = array(
			'id_produk' => array(
				'type' => 'INT',
				'constraint' => 11,
				),
			'id_pengiriman' => array(
				'type' => 'INT',
				'constraint' => 11,
				),
			);
		$this->dbforge->add_field($fields);
		$this->dbforge->create_table('commerce_pengiriman_produk', TRUE);

		// rekening
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
				),
			'nama_bank' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				),
			'no_rekening' => array(
				'type' => 'VARCHAR',
				'constraint' => '20'
				),
			'atas_nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				),
			'created_on' => array(
				'type' => 'DATETIME',
				),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
				),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
				),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
				),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
				),
			);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('commerce_rekening', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_commerce_rekening(created_by)");

		// order
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'no_invoice' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => TRUE,
			),
			'id_customer' => array(
				'type' => 'SMALLINT',
				'constraint' => 5,
				'null' => TRUE,
			),
			'id_session' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => TRUE,
			),
			'status_pesanan' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,
			),
			'id_affiliate' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'kode_unik' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'tanggal_order' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'biaya_order' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'biaya_pengiriman' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'alamat_pengiriman' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'kurir_pengiriman' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,
			),
			'paket_pengiriman' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
				'null' => TRUE,
			),
			'nama_penerima' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,
			),
			'no_hp_penerima' => array(
				'type' => 'VARCHAR',
				'constraint' => 13,
				'null' => TRUE,
			),
			'id_kurir_pengiriman' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => true
			),
			'id_kota_pengiriman' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => true
			),
			'berat_pengiriman' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => true
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('commerce_order', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_commerce_order(created_by)");

		// cart
		$fields = array(
			'id_order' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'id_produk' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'jumlah' => array(
				'type' => 'INT',
			),
			'harga' => array(
				'type' => 'INT',
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id_order', 'id_produk', TRUE);
		$this->dbforge->create_table('commerce_cart', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_commerce_cart(created_by)");

		// payment
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'id_order' => array(
				'type' => 'INT',
			),
			'tanggal_transfer' => array(
				'type' => 'DATE',
			),
			'nama_rekening_pengirim' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
			),
			'bank_rekening_pengirim' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
			),
			'no_rekening_pengirim' => array(
				'type' => 'VARCHAR',
				'constraint' => 20,
			),
			'bank_rekening_tujuan' => array(
				'type' => 'VARCHAR',
				'constraint' => 45,
			),
			'jumlah_pembayaran' => array(
				'type' => 'INT',
			),
			'keterangan' => array(
				'type' => 'TEXT',
			),
			'id_bukti' => array(
				'type' => 'CHAR',
				'constraint' => 15,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('commerce_payment', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_commerce_payment(created_by)");

		$this->version = '1.1';

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('commerce_payment');
        $this->dbforge->drop_table('commerce_order');
        $this->dbforge->drop_table('commerce_cart');
        $this->dbforge->drop_table('commerce_pengiriman_produk');
        $this->dbforge->drop_table('commerce_pengiriman');
        $this->dbforge->drop_table('commerce_produk');
        $this->dbforge->drop_table('commerce_rekening');
        $this->dbforge->drop_table('commerce_voucher');
        return true;
    }

    public function upgrade($old_version)
    {
    	switch ($old_version) {
    		case '1.0':
    			case '1.0':
    			$new_field = array(
					'is_digital_product' => array(
						'type' => 'TINYINT',
						'default' => 0,
						),
    				);

    			$this->dbforge->add_column('commerce_produk',$new_field);
    			$this->version = '1.1';
    			break;

    		case '1.1':
    			$new_field = array(
    				'id_affiliate' => array(
    					'type' => 'INT',
    					'constraint' => '11',
    					'null' => TRUE,
    					),
    				);

    			$this->dbforge->add_column('commerce_rekening',$new_field);
    			$this->version = '1.1.1';
    			break;

    		case '1.1.1':
    			$field = array(
    				'id_bukti' => array(
    					'name' => 'id_bukti',
    					'type' => 'CHAR',
    					'constraint' => '15',
    					'null' => TRUE,
    					),
    				);

    			$this->dbforge->modify_column('commerce_payment',$field);
    			$this->version = '1.1.2';
    			break;

    		case '1.1.2':
    			if(!$this->db->field_exists('amazon_filename','commerce_payment')) {
	    			$new_field = array(
	    				'amazon_filename' => array(
	    					'type' => 'VARCHAR',
	    					'constraint' => '25',
	    					'null' => TRUE,
	    					),
	    				);

	    			$this->dbforge->add_column('commerce_payment',$new_field);
	    		}
    			$this->version = '1.1.3';
	    		break;

	    	case '1.1.3':
    			if(!$this->db->field_exists('closed_on','commerce_payment') AND !$this->db->field_exists('closed_by','commerce_payment')) {
	    			$new_field = array(
	    				'closed_on' => array(
	    					'type' => 'DATETIME',
	    					'null' => TRUE,
	    					),
	    				'closed_by' => array(
	    					'type' => 'INT',
	    					'constraint' => '11',
	    					'null' => TRUE,
	    					),
	    				);

	    			$this->dbforge->add_column('commerce_payment',$new_field);

	    		}
	    		$this->db->set('closed_on','updated_on',false);
	    		$this->db->set('closed_by','updated_by',false);
	    		$this->db->like('status','paid');
	    		$this->db->update('commerce_payment');

	    		$this->version = '1.1.4';
	    		break;

    		case '1.1.4':
    			// add column to commerce order
    			if(!$this->db->field_exists('voucher_id','commerce_order') AND !$this->db->field_exists('voucher_applied_on','commerce_order') AND !$this->db->field_exists('voucher_applied_data','commerce_order') AND !$this->db->field_exists('discount_nominal','commerce_order')) {
	    			$new_field = array(
	    				'voucher_id' => array(
	    					'type' => 'INT',
	    					'constraint' => '11',
	    					'null' => TRUE,
	    					'unsigned' => TRUE,
	    					),
	    				'voucher_applied_on' => array(
	    					'type' => 'DATETIME',
	    					'null' => TRUE,
	    					),
	    				'voucher_applied_data' => array(
	    					'type' => 'TEXT',
	    					'null' => TRUE,
	    					),
	    				'discount_nominal' => array(
	    					'type' => 'DECIMAL',
	    					'null' => TRUE,
	    					),
	    				);

	    			$this->dbforge->add_column('commerce_order',$new_field);
	    		}
	    		// voucher
	    		$this->db->query("CREATE TABLE IF NOT EXISTS ".$this->db->dbprefix("commerce_voucher")."(
	    			`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	    			`code` VARCHAR(45) NOT NULL,
	    			`valid_unit` DATETIME NULL DEFAULT NULL,
	    			`usage_limit_count` INT(11) UNSIGNED NULL DEFAULT NULL,
	    			`usage_limit_scope` VARCHAR(45) NULL DEFAULT NULL,
	    			`benefit_type` VARCHAR(45) NOT NULL,
	    			`benefit_value` DECIMAL UNSIGNED NOT NULL,
	    			`state_active` TINYINT(1) UNSIGNED NOT NULL,
	    			`affiliate_id` INT(11) UNSIGNED NULL DEFAULT NULL,
	    			`created_by` INT(11) UNSIGNED NOT NULL,
	    			`created_on` DATETIME NOT NULL,
	    			`last_edited_by` INT(11) UNSIGNED NULL DEFAULT NULL,
	    			`last_edited_on` DATETIME NULL DEFAULT NULL,
	    			PRIMARY KEY (`id`),
	    			UNIQUE INDEX `code_UNIQUE` (`code` ASC),
	    			INDEX `fk_voucher_affiliate_user_idx` (`affiliate_id` ASC),
	    			INDEX `fk_voucher_createdby_users_idx` (`created_by` ASC),
	    			INDEX `fk_voucher_lasteditedby_users_idx` (`last_edited_by` ASC),
	    			CONSTRAINT `fk_voucher_affiliate_user`
	    			FOREIGN KEY (`affiliate_id`)
	    			REFERENCES `default_users` (`id`)
	    			ON DELETE RESTRICT
	    			ON UPDATE RESTRICT,
	    			CONSTRAINT `fk_voucher_createdby_user`
	    			FOREIGN KEY (`created_by`)
	    			REFERENCES `default_users` (`id`)
	    			ON DELETE RESTRICT
	    			ON UPDATE RESTRICT,
	    			CONSTRAINT `fk_voucher_lasteditedby_users`
	    			FOREIGN KEY (`last_edited_by`)
	    			REFERENCES `default_users` (`id`)
	    			ON DELETE RESTRICT
	    			ON UPDATE RESTRICT)
	    			ENGINE = InnoDB");

	    		$this->version = '1.1.5';
	    		break;

	    	case '1.1.5':
    			$field = array(
    				'tanggal_transfer' => array(
    					'name' => 'tanggal_transfer',
    					'type' => 'DATE',
    					'null' => TRUE,
    					),
    				'nama_rekening_pengirim' => array(
    					'name' => 'nama_rekening_pengirim',
    					'type' => 'VARCHAR',
    					'constraint' => '100',
    					'null' => TRUE,
    					),
    				'bank_rekening_pengirim' => array(
    					'name' => 'bank_rekening_pengirim',
    					'type' => 'VARCHAR',
    					'constraint' => '45',
    					'null' => TRUE,
    					),
    				'no_rekening_pengirim' => array(
    					'name' => 'no_rekening_pengirim',
    					'type' => 'VARCHAR',
    					'constraint' => '20',
    					'null' => TRUE,
    					),
    				'bank_rekening_tujuan' => array(
    					'name' => 'bank_rekening_tujuan',
    					'type' => 'INT',
    					'constraint' => '11',
    					'null' => TRUE,
    					),
    				);

    			$this->dbforge->modify_column('commerce_payment',$field);

    			if(!$this->db->field_exists('payment_method','commerce_payment')) {
	    			$new_field = array(
	    				'payment_method' => array(
	    					'type' => 'VARCHAR',
	    					'constraint' => '50',
	    					'null' => TRUE,
	    					),
	    				);

	    			$this->dbforge->add_column('commerce_payment',$new_field);

	    		}

	    		$this->db->update('commerce_payment',array('payment_method'=>'transfer'));
    			$this->version = '1.1.6';
    			break;

    		case '1.1.6' :
    			$this->db->trans_start();
    			// rekening
    			// add column
    			$this->db->query("ALTER TABLE `default_commerce_rekening`
    				MODIFY COLUMN `id_affiliate`  int(11) NULL DEFAULT NULL AFTER `atas_nama`,
    				ADD COLUMN `is_default`  tinyint(1) UNSIGNED NOT NULL AFTER `id_affiliate`,
    				ADD COLUMN `owner_id`  int(11) UNSIGNED NULL AFTER `is_default`");

    			// clean data rekening
    			$this->db->query('DELETE FROM default_commerce_rekening WHERE created_by IS NOT NULL AND created_by NOT IN (SELECT id FROM default_users)');

    			// add foreign key
    			$this->db->query("ALTER TABLE `default_commerce_rekening` ADD CONSTRAINT `fk_rekening_createdby_users` FOREIGN KEY (`created_by`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_commerce_rekening` ADD CONSTRAINT `fk_rekening_updatedby_users` FOREIGN KEY (`updated_by`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_commerce_rekening` ADD CONSTRAINT `fk_rekening_owner_users` FOREIGN KEY (`owner_id`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			// pengiriman
    			// modify column
    			$this->db->query("ALTER TABLE `default_commerce_pengiriman_produk`
    				MODIFY COLUMN `id_produk`  int(11) UNSIGNED NOT NULL");

    			$this->db->query("ALTER TABLE `default_commerce_pengiriman_produk`
    				MODIFY COLUMN `id_pengiriman`  int(11) UNSIGNED NOT NULL");

    			// add foreign key
    			$this->db->query("ALTER TABLE `default_commerce_pengiriman` ADD CONSTRAINT `fk_pengiriman_updatedby_users` FOREIGN KEY (`updated_by`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_commerce_pengiriman_produk` ADD CONSTRAINT `fk_product_has_pengiriman` FOREIGN KEY (`id_produk`) REFERENCES `default_commerce_produk` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_commerce_pengiriman_produk` ADD CONSTRAINT `fk_pengiriman_usedby_product` FOREIGN KEY (`id_pengiriman`) REFERENCES `default_commerce_pengiriman` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			// produk
    			$this->db->query("ALTER TABLE `default_commerce_produk`
    				MODIFY COLUMN `id_kota_pengirim`  int(11) UNSIGNED NOT NULL");

    			// add foreign key
    			$this->db->query("ALTER TABLE `default_commerce_produk`
    				DROP INDEX `author_index`");

    			$this->db->query("ALTER TABLE `default_commerce_produk` ADD CONSTRAINT `fk_produk_foto_files` FOREIGN KEY (`id_file`) REFERENCES `default_files` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			//$this->db->query("INSERT INTO `default_location_kota` (`id`, `kode`, `nama`, `slug`, `id_provinsi`, `created_on`, `created_by`) VALUES ('0', '0', 'Pilih Kota', 'none', '0', now(), '1')");
    			//$this->db->query("UPDATE `default_location_kota` SET `id`=0 WHERE `kode`=0");

    			$this->db->query("ALTER TABLE `default_commerce_produk` ADD CONSTRAINT `fk_produk_pengirim_kota` FOREIGN KEY (`id_kota_pengirim`) REFERENCES `default_location_kota` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_commerce_produk` ADD CONSTRAINT `fk_produk_createdby_users` FOREIGN KEY (`created_by`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_commerce_produk` ADD CONSTRAINT `fk_produk_updatedby_users` FOREIGN KEY (`updated_by`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			// payment
    			// modify and add column
    			$this->db->query("ALTER TABLE `default_commerce_payment`
    				MODIFY COLUMN `id_order`  int(11) UNSIGNED NOT NULL,
    				MODIFY COLUMN `payment_method`  varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `id_order`,
    				MODIFY COLUMN `jumlah_pembayaran`  int(11) NOT NULL AFTER `payment_method`,
    				MODIFY COLUMN `created_on`  datetime NOT NULL AFTER `jumlah_pembayaran`,
    				MODIFY COLUMN `created_by`  int(11) UNSIGNED NULL DEFAULT NULL AFTER `created_on`,
    				MODIFY COLUMN `updated_on`  datetime NULL DEFAULT NULL AFTER `created_by`,
    				MODIFY COLUMN `updated_by`  int(11) UNSIGNED NULL DEFAULT NULL AFTER `updated_on`,
    				MODIFY COLUMN `ordering_count`  int(11) UNSIGNED NULL DEFAULT NULL AFTER `updated_by`,
    				MODIFY COLUMN `bank_rekening_tujuan`  int(11) UNSIGNED NULL DEFAULT NULL AFTER `bank_rekening_pengirim`,
    				MODIFY COLUMN `closed_by`  int(11) UNSIGNED NULL DEFAULT NULL AFTER `closed_on`,
    				ADD COLUMN `billed_on`  datetime NULL AFTER `payment_method`,
    				ADD COLUMN `paid_on`  datetime NULL AFTER `billed_on`,
    				ADD COLUMN `history`  text NULL AFTER `jumlah_pembayaran`,
    				ADD COLUMN `transfer_verification_status`  varchar(45) NULL AFTER `closed_by`,
    				ADD COLUMN `transfer_verification_note`  text NULL AFTER `transfer_verification_status`,
    				ADD COLUMN `transfer_verified_by`  int(11) UNSIGNED NULL AFTER `transfer_verification_note`,
    				ADD COLUMN `transfer_verified_on`  date NULL AFTER `transfer_verified_by`,
    				ADD COLUMN `transfer_confirmed_by`  int(11) UNSIGNED NULL AFTER `transfer_verified_on`,
    				ADD COLUMN `transfer_confirmed_on`  date NULL AFTER `transfer_confirmed_by`,
    				ADD COLUMN `ipaymu_session_id`  varchar(500) NULL AFTER `transfer_verification_note`,
    				ADD COLUMN `ipaymu_url`  varchar(500) NULL AFTER `ipaymu_session_id`,
    				ADD COLUMN `ipaymu_trx_id`  varchar(45) NULL AFTER `ipaymu_url`,
    				ADD COLUMN `ipaymu_data`  text NULL AFTER `ipaymu_trx_id`,
    				ADD COLUMN `ipaymu_type`  varchar(45) NULL AFTER `ipaymu_data`,
    				ADD COLUMN `ipaymu_status_code`  varchar(45) NULL AFTER `ipaymu_type`,
    				ADD COLUMN `ipaymu_status_name`  varchar(45) NULL AFTER `ipaymu_status_code`,
    				ADD COLUMN `ipaymu_status_last_updated_on`  datetime NULL AFTER `ipaymu_status_name`");

    			// add foreign key
    			// delete payment withour order
    			$this->db->query("DELETE FROM default_commerce_payment WHERE id_order NOT IN (SELECT id FROM default_commerce_order) ");

    			$this->db->query("DELETE FROM default_commerce_payment WHERE bank_rekening_tujuan NOT IN (SELECT id FROM default_commerce_rekening) ");

    			$this->db->query("ALTER TABLE `default_commerce_payment` ADD CONSTRAINT `fk_payment_id_order_order` FOREIGN KEY (`id_order`) REFERENCES `default_commerce_order` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_commerce_payment` ADD CONSTRAINT `fk_payment_last_updated_by_users` FOREIGN KEY (`updated_by`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_commerce_payment` ADD CONSTRAINT `fk_payment_transfer_rekening_tujuan_rekening` FOREIGN KEY (`bank_rekening_tujuan`) REFERENCES `default_commerce_rekening` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_commerce_payment` ADD CONSTRAINT `fk_payment_transfer_confirmed_by_users` FOREIGN KEY (`closed_by`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->query("ALTER TABLE `default_commerce_payment` ADD CONSTRAINT `fk_transfer_verified_by_users` FOREIGN KEY (`transfer_verified_by`) REFERENCES `default_users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			// order
    			// add and modify column
    			$this->db->query("ALTER TABLE `default_commerce_order`
    				ADD COLUMN `affiliate_commission_amout`  decimal NULL AFTER `discount_nominal`,
    				ADD COLUMN `affiliate_order_data`  text NULL AFTER `affiliate_commission_amout`,
    				ADD COLUMN `state_history`  TEXT NULL AFTER `affiliate_order_data`,
    				ADD COLUMN `placed_on`  datetime NULL AFTER `state_history`,
    				ADD COLUMN `expired_on`  datetime NULL AFTER `placed_on`,
    				ADD COLUMN `closed_on`  datetime NULL AFTER `expired_on`,
    				ADD COLUMN `amount_discount`  decimal NULL AFTER `closed_on`,
    				ADD COLUMN `amount_tax`  decimal NULL AFTER `amount_discount`,
    				ADD COLUMN `amount_handling_fee`  decimal NULL AFTER `amount_tax`,
    				ADD COLUMN `amount_billed`  decimal NULL AFTER `amount_handling_fee`");

    			// add index
    			// $this->db->query("ALTER TABLE `default_commerce_order`
    			// 	ADD UNIQUE INDEX `no_invoice_UNIQUE` (`no_invoice`) ,
    			// 	ADD UNIQUE INDEX `cart_session_id_UNIQUE` (`id_session`)");

    			// add foreign key
    			$this->db->query("ALTER TABLE `default_commerce_order` ADD CONSTRAINT `fk_order_voucherid_voucher` FOREIGN KEY (`voucher_id`) REFERENCES `default_commerce_voucher` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");

    			$this->db->trans_complete();

    			if ($this->db->trans_status() === FALSE)
    			{
    				return false;
    			}
    			$this->version = '1.1.7';
    			break;
    		case '1.1.7' :
    			// update baru
    			$this->db->query("UPDATE default_commerce_order
    			SET status_pesanan = 'in-cart'
    			WHERE
    			status_pesanan = 'baru'");

				// update telah melakukan pembayaran
    			$this->db->query("UPDATE default_commerce_order
    			SET status_pesanan = 'waiting-payment',
    			placed_on = tanggal_order,
    			expired_on = tanggal_order + INTERVAL 2 DAY
    			WHERE
    			status_pesanan like 'menunggu konfirmasi%'");

				// update telah melakukan pembayaran
    			$this->db->query("UPDATE default_commerce_order
    			SET status_pesanan = 'waiting-payment',
    			placed_on = tanggal_order,
    			expired_on = tanggal_order + INTERVAL 2 DAY
    			WHERE
    			status_pesanan like 'telah melakukan pembayaran%'");

				// update di batalkan
    			$this->db->query("UPDATE default_commerce_order
    			SET status_pesanan = 'cancelled',
    			placed_on = tanggal_order,
    			expired_on = tanggal_order + INTERVAL 2 DAY
    			WHERE
    			status_pesanan like 'dibatalkan pada %'");

				// update menunggu produk dikirim
    			$this->db->query("UPDATE default_commerce_order
    			SET status_pesanan = 'completed',
    			placed_on = tanggal_order,
    			expired_on = tanggal_order + INTERVAL 2 DAY,
    			closed_on = updated_on
    			WHERE
    			status_pesanan = 'menunggu produk dikirim'");

				// update menunggu produk dikirim
    			$this->db->query("UPDATE default_commerce_order
    			SET status_pesanan = 'completed',
    			placed_on = tanggal_order,
    			expired_on = tanggal_order + INTERVAL 2 DAY,
    			closed_on = updated_on
    			WHERE
    			status_pesanan = 'produk telah dikirim'");

				// update produk telah diterima
    			$this->db->query("UPDATE default_commerce_order
    			SET status_pesanan = 'completed',
    			placed_on = tanggal_order,
    			expired_on = tanggal_order + INTERVAL 2 DAY,
    			closed_on = updated_on
    			WHERE
    			status_pesanan = 'produk telah diterima'");

				// update order expired
    			$this->db->query("UPDATE default_commerce_order
    			SET
    			placed_on = tanggal_order,
    			expired_on = tanggal_order + INTERVAL 2 DAY
    			WHERE
    			status_pesanan = 'expired'");

				// update telah konfirmasi melakukan pembayaran
				$this->db->query("UPDATE default_commerce_payment
    			SET
    			`payment_method` = 'transfer'
    			WHERE
    			payment_method IS NULL and jumlah_pembayaran > 0");

    			$this->db->query("UPDATE default_commerce_payment
    			SET
    			`status` = 'pending',
    			billed_on = created_on,
    			paid_on = tanggal_transfer
    			WHERE
    			`status` like 'telah konfirmasi melakukan pembayaran%'");

				// update pembayaran telah dikonfirmasi
    			$this->db->query("UPDATE default_commerce_payment
    			SET
    			`status` = 'paid',
    			billed_on = created_on,
    			paid_on = tanggal_transfer,
    			transfer_confirmed_by = updated_by,
    			transfer_confirmed_on = updated_on,
    			closed_by = updated_by,
    			closed_on = updated_on
    			WHERE
    			`status` like 'pembayaran telah dikonfirmasi%'");

				// update pembayaran ditolak
    			$this->db->query("UPDATE default_commerce_payment
    			SET
    			`status` = 'declined',
    			billed_on = created_on,
    			paid_on = tanggal_transfer
    			WHERE
    			`status` like 'pembayaran ditolak pada%'");
    			$this->version = '1.1.8';
    			break;

    		case '1.1.8':
    			// update pembayaran ditolak
    			$this->db->query("UPDATE default_commerce_order
    			SET
    			`status_pesanan` = 'cancelled'
    			WHERE
    			`status_pesanan` like 'waiting-payment%' AND `id` IN (SELECT `id_order` FROM default_commerce_payment WHERE `status`='declined')");
    			$this->version = '1.1.9';
    			break;

    		case '1.1.9':
    			// update pembayaran ditolak
    		$this->db->query("UPDATE default_commerce_order
    			SET expired_on = '2016-09-05 23:59'
    			WHERE
    			status_pesanan = 'waiting-payment'");
    			$this->version = '1.1.10';
    			break;
    		case '1.1.10':
    			if(!$this->db->field_exists('order_type','commerce_order')) {
	    			$new_field = array(
	    				'order_type' => array(
	    					'type' => 'VARCHAR',
	    					'constraint' => '100',
	    					'null' => TRUE,
	    					),
	    				);

	    			$this->dbforge->add_column('commerce_order',$new_field);

	    		}

    			// set all current order type to membership
    			$this->db->query("UPDATE default_commerce_order SET order_type = 'membership'");

    			$this->version = '1.1.11';
    			break;

    		case '1.1.11':
	    		// product
	    		$this->db->query("ALTER TABLE `default_commerce_produk`
	    			MODIFY COLUMN `berat`  int(15) NULL AFTER `id_file`,
	    			MODIFY COLUMN `id_kota_pengirim`  int(11) UNSIGNED NULL AFTER `berat`,
	    			MODIFY COLUMN `is_digital_product`  tinyint(4) NULL DEFAULT 0 AFTER `id_kota_pengirim`,
	    			ADD COLUMN `type`  varchar(100) NULL AFTER `ordering_count`,
	    			ADD COLUMN `membership_period`  varchar(255) NULL AFTER `is_digital_product`,
	    			ADD COLUMN `membership_level`  varchar(255) NULL AFTER `membership_period`,
	    			ADD COLUMN `digital_id_file_asset`  char(15) NULL AFTER `membership_level`");

	    		// cart
	    		$this->db->query("ALTER TABLE `default_commerce_cart`
	    			ADD COLUMN `id_unique`  varchar(100) NULL AFTER `harga`");
    			$this->version = '1.1.12';
    			break;

    		case '1.1.12':
	    		// order
	    		$this->db->query("ALTER TABLE `default_commerce_order`
	    			MODIFY COLUMN `id_customer`  int(11) UNSIGNED NULL DEFAULT NULL,
	    			ADD UNIQUE INDEX `un_no_invoice` (`no_invoice`) USING BTREE");

	    		// clean data
	    		$this->db->query("DELETE
	    			FROM
	    			default_commerce_payment WHERE id_order IN (
	    			SELECT
	    			id
	    			FROM
	    			default_commerce_order
	    			WHERE
	    			id_customer NOT IN (SELECT id FROM default_users)
	    			AND id_customer IS NOT NULL
	    			)");

	    		$this->db->query("DELETE
	    			FROM
	    			default_commerce_cart
	    			WHERE
	    			id_order IN (
	    			SELECT
	    			id
	    			FROM
	    			default_commerce_order
	    			WHERE
	    			id_customer NOT IN (SELECT id FROM default_users)
	    			AND id_customer IS NOT NULL
	    			)");

	    		$this->db->query("DELETE FROM
	    			default_commerce_order
	    			WHERE
	    			id_customer NOT IN (SELECT id FROM default_users)
	    			AND id_customer IS NOT NULL");

	    		// add foreign key

	    		$this->db->query("ALTER TABLE `default_commerce_order` ADD CONSTRAINT `fk_customer` FOREIGN KEY (`id_customer`) REFERENCES `default_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE");

	    		// payment
	    		$this->db->query("ALTER TABLE `default_commerce_payment`
	    			ADD UNIQUE INDEX `un_order` (`id_order`) USING BTREE ,
	    			ADD UNIQUE INDEX `un_ipaymu_trx_id` (`ipaymu_trx_id`) USING BTREE");

    			$this->version = '1.1.13';
    			break;

    		case '1.1.13':
    			if(!$this->db->field_exists('otp','commerce_payment')) {
	    			$new_field = array(
						'id_dompet' => array(
							'type' => 'INT',
							'unsigned' => TRUE,
							'null' => TRUE,
							),
						'otp' => array(
							'type' => 'VARCHAR',
							'constraint' => 6,
							'null' => TRUE,
							),
						'is_opt_verified' => array(
							'type' => 'TINYINT',
							'constraint' => 1,
							'null' => TRUE,
							),
						'otp_verified_on' => array(
							'type' => 'DATETIME',
							'null' => TRUE,
							),
						'otp_verified_by' => array(
							'type' => 'INT',
							'constraint' => 11,
							'null' => TRUE,
							),
						'resend_attempt' => array(
							'type' => 'TINYINT',
							'constraint' => 1,
							'null' => TRUE,
							),
						'retry_attemp' => array(
							'type' => 'TINYINT',
							'constraint' => 1,
							'null' => TRUE,
							),
						'otp_expired_on' => array(
							'type' => 'DATETIME',
							'null' => TRUE,
							),
						'otp_created_on' => array(
							'type' => 'DATETIME',
							'null' => TRUE,
							),
	    				);

	    			$this->dbforge->add_column('commerce_payment',$new_field);

	    			$this->db->query("CREATE INDEX fk_default_mutasi_iddompet_dompet_saldo ON default_commerce_payment(id_dompet)");
					$this->db->query("CREATE INDEX fk_default_mutasi_createdby_users ON default_commerce_payment(otp_verified_by)");
				}

    			$this->version = '1.1.14';
    			break;
			case '1.1.14':
    			if(!$this->db->field_exists('is_affiliate_template','commerce_voucher')) {
	    			$new_field = array(
						'is_affiliate_template' => array(
							'type' => 'TINYINT',
							'unsigned' => TRUE,
							'null' => TRUE,
							),
						'product_type' => array(
							'type' => 'VARCHAR',
							'constraint' => 100,
							'null' => TRUE,
							),
	    				);

	    			$this->dbforge->add_column('commerce_voucher',$new_field);

				}
    			$this->db->update('commerce_voucher',array('product_type'=>'membership'));

    			$this->db->where_in('code',array('DKK60','DKK50'));
    			$this->db->update('commerce_voucher',array('is_affiliate_template'=>1));

    			$this->version = '1.1.15';
    			break;

    		case '1.1.15':
    			$new_field = array(
					'id_landing_page' => array(
						'type' => 'INT',
						'constraint' => 11,
						'unsigned' => true,
						'null' => true
						),
    				);

    			$this->dbforge->add_column('commerce_order',$new_field);

    			if ($this->db->table_exists('landing_page_konten')) {
	    			$this->db->where('uri_segment_string','/');
	    			$landing_page = $this->db->get('landing_page_konten')->row_array();

	    			$this->db->update('commerce_order',array('id_landing_page'=>$landing_page['id']));
	    		}

    			$this->version = '1.1.18';
    			break;

    		case '1.1.18':
    			// penyesuaian completed time
    			$this->db->query("UPDATE default_commerce_order AS dst, ( SELECT id, updated_on FROM default_commerce_order WHERE status_pesanan = 'completed' ) AS src SET dst.closed_on = src.updated_on WHERE src.id = dst.id");
    			$this->version = '1.1.19';
    			break;

    		case '1.1.19':
    			$new_field = array(
					'no_resi_pengiriman' => array(
						'type' => 'VARCHAR',
						'constraint' => 100,
						'null' => true
						),
    				);

    			$this->dbforge->add_column('commerce_order',$new_field);

    			$this->version = '1.1.20';
    			break;

            case '1.1.20':
                # code...
                $values =  array(
                'slug' => 'invoice_prefix',
                'title' => 'Invoicce Prefix',
                'description' => 'Prefix of invoice number',
                'type' => 'text',
                'options' => '',
                'default' => 'INV',
                'value' => '2KK',
                'is_required' => 0,
                'is_gui' => 1,
                'module' => 'commerce',
                'order' => 0
                );

                $this->db->insert('settings', $values);

                $this->version = '1.2.0';
                break;

            case '1.2.0':
    			$new_field = array(
					'additional_info' => array(
						'type' => 'TEXT',
						'null' => true
						),
    				);

    			$this->dbforge->add_column('commerce_produk',$new_field);

    			$this->version = '1.2.1';
    			break;

            case '1.2.1':
                // membership content, restrick level album
                $fields = array(
                    'id_membership' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        ),
                    'id_album' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        ),
                    );
                $this->dbforge->add_field($fields);
                $this->dbforge->create_table('commerce_membership_album', TRUE);

                $this->version = '1.2.2';
                break;

            case '1.2.2':
                // add field product external
                $fields = array(
                    'id_external_product' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'null' => true
                        ),
                    );
                $this->dbforge->add_column('commerce_produk', $fields);

                $this->version = '1.2.3';
                break;

    		default:
    			# code...
    			break;
    	}
    	return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}
