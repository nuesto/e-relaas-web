<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Expired
|--------------------------------------------------------------------------
|
|--------------------------------------------------------------------------*/
$config['expired']['membership']['type']	= 'fix'; // fix|range|unlimit
$config['expired']['physical']['type']	= 'unlimit';
$config['expired']['membership']['value']	= '2017-03-10 23:59';
$config['expired']['physical']['value']	= NULL;
$config['active_voucher_code'] = 'DKK25';
$config['active_voucher_nominal'] = '123750';
$config['active_voucher_percent'] = '25%';
