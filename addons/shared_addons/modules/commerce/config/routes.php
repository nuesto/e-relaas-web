<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['commerce/admin/order(:any)'] = 'admin_order$1';
$route['commerce/admin/payment(:any)'] = 'admin_payment$1';
$route['commerce/admin/cart(:any)'] = 'admin_cart$1';
$route['commerce/admin/produk(:any)'] = 'admin_produk$1';
$route['commerce/admin/rekening(:any)'] = 'admin_rekening$1';
$route['commerce/admin/pengiriman(:any)'] = 'admin_pengiriman$1';
$route['commerce/admin/affiliate(:any)'] = 'admin_affiliate$1';
$route['commerce/admin/voucher(:any)'] = 'admin_voucher$1';
$route['commerce/order(:any)'] = 'commerce_order$1';
$route['commerce/payment(:any)'] = 'commerce_payment$1';
$route['commerce/cart(:any)'] = 'commerce_cart$1';
$route['commerce/produk(:any)'] = 'commerce_produk$1';
$route['commerce/rekening(:any)'] = 'commerce_rekening$1';
