<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Outbox model
 *
 * @author Aditya Satrya
 */
class Outbox_m extends MY_Model {
	
	public function get_outbox($pagination_config = NULL)
	{
		$this->db->select('*');
		
		if($this->input->get('f-sms')){
			$this->db->like('sms', $this->input->get('f-sms'));
		}
		if($this->input->get('f-sender')){
			$this->db->like('sender', $this->input->get('f-sender'));
		}
		if($this->input->get('f-status')){
			$this->db->where('status', $this->input->get('f-status'));
		}

		$this->db->order_by('UpdatedInDB, id', 'DESC');
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_sms_gateway_api_outbox');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_outbox_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_sms_gateway_api_outbox');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_outbox_to_send(){
		$this->db->select('*');
		$this->db->from('default_sms_gateway_api_outbox');
		$this->db->where('status',NULL);
		$this->db->where("DATE_FORMAT(insertIntoDB, '%Y-%m-%d') = DATE_FORMAT(now(), '%Y-%m-%d')");
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function count_all_outbox()
	{
		return $this->db->count_all('sms_gateway_api_outbox');
	}
	
	public function delete_outbox_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_sms_gateway_api_outbox');
	}
	
	public function insert_outbox($values)
	{	
		return $this->db->insert('default_sms_gateway_api_outbox', $values);
	}
	
	public function update_outbox($values, $row_id)
	{
		$this->db->where('id', $row_id);
		return $this->db->update('default_sms_gateway_api_outbox', $values); 
	}
	
}