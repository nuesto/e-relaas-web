<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Sms_gateway_api extends Module
{
    public $version = '1.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'SMS Gateway API',
			'id' => 'SMS Gateway API',
		);
		$info['description'] = array(
			'en' => 'Module untuk mengelola SMS Gateway API',
			'id' => 'Module untuk mengelola SMS Gateway API',
		);
		$info['frontend'] = false;
		$info['backend'] = true;
		$info['menu'] = 'SMS Gateway API';
		$info['roles'] = array(
			'access_inbox_backend', 'view_all_inbox', 'view_own_inbox', 'edit_all_inbox', 'edit_own_inbox', 'delete_all_inbox', 'delete_own_inbox', 'create_inbox',
			'access_outbox_backend', 'view_all_outbox', 'view_own_outbox', 'edit_all_outbox', 'edit_own_outbox', 'delete_all_outbox', 'delete_own_outbox', 'create_outbox',
		);
		
		if(group_has_role('sms_gateway_api', 'access_inbox_backend')){
			$info['sections']['inbox']['name'] = 'sms_gateway_api:inbox:plural';
			$info['sections']['inbox']['uri'] = 'admin/sms_gateway_api/inbox/index';
			
			if(group_has_role('sms_gateway_api', 'create_inbox')){
				$info['sections']['inbox']['shortcuts']['create'] = array(
					'name' => 'sms_gateway_api:inbox:new',
					'uri' => 'admin/sms_gateway_api/inbox/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('sms_gateway_api', 'access_outbox_backend')){
			$info['sections']['outbox']['name'] = 'sms_gateway_api:outbox:plural';
			$info['sections']['outbox']['uri'] = 'admin/sms_gateway_api/outbox/index';
			
			if(group_has_role('sms_gateway_api', 'create_outbox')){
				$info['sections']['outbox']['shortcuts']['create'] = array(
					'name' => 'sms_gateway_api:outbox:new',
					'uri' => 'admin/sms_gateway_api/outbox/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items){
		
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// inbox
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'sms' => array(
				'type' => 'TEXT',
			),
			'sender' => array(
				'type' => 'VARCHAR',
				'constraint' => '12',
			),
			'waktu' => array(
				'type' => 'DATETIME',
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
				'null' => TRUE,
			),
			'ExecutedTime' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('sms_gateway_api_inbox', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_sms_gateway_api_inbox(sender, waktu)");


		// outbox
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'sms' => array(
				'type' => 'TEXT',
			),
			'destination' => array(
				'type' => 'VARCHAR',
				'constraint' => '12',
			),
			'InsertIntoDB' => array(
				'type' => 'DATETIME',
			),
			'UpdatedInDB' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
				'null' => TRUE,
			),
			'results' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('sms_gateway_api_outbox', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_sms_gateway_api_outbox(destination, InsertIntoDB)");

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('sms_gateway_api_inbox');
        $this->dbforge->drop_table('sms_gateway_api_outbox');
        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}