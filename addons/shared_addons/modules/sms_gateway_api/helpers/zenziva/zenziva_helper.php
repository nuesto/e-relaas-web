<?php
	function curl($url, $postfields){
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $postfields);
		curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		curl_setopt($curlHandle, CURLOPT_POST, 1);
		$results = curl_exec($curlHandle);
		return $results;
	}
	function sendsms($destination, $message){
		include "apiconfig.php";
		$url = $urlsend;
		$postfields = 'userkey='.$userkey.'&passkey='.$passkey.'&tipe=reguler&nohp='.$destination.'&pesan='.urlencode($message);
		$result = curl($url, $postfields);
		return $result;
	}

	function readinbox(){
		include "apiconfig.php";
		$url = $urlinbox;
		$postfields = 'userkey='.$userkey.'&passkey='.$passkey.'&status=all';
		$xml = simplexml_load_string(curl($url, $postfields));
		$json = json_encode($xml);
		$data = json_decode($json,TRUE)['message'];

		if(isset($data['status']) || isset($data['text'])){
			$results['status'] = 'error';
			$results['status_code'] = 400;
			$results['status_message'] = $data['text'];
		}else{
			foreach ($data as $key => $pesan) {
				$row['id'] = $pesan['id'];
				$row['sms'] = $pesan['isiPesan'];
				$row['sender'] = $pesan['dari'];
				$row['waktu'] = $pesan['tgl'].' '.$pesan['waktu'];
				$inbox[] = $row;
			}
			$results['status'] = 'success';
			$results['status_code'] = 200;
			$results['status_message'] = 'Berhasil mengambil data';
			$results['inbox'] = $inbox;
		}
		return $results;	
	}

	function readoutbox($i='', $n=''){
		include "apiconfig.php";
		$url = $urloutbox;
		$postfields = 'userkey='.$userkey.'&passkey='.$passkey.'&from='.date('Y-m-d').'&to='.date('Y-m-d');
		$xml = simplexml_load_string(curl($url, $postfields));
		$json = json_encode($xml);
		$data = json_decode($json,TRUE)['message'];
		if(isset($data['status']) || isset($data['text'])){
			$results['status'] = 'error';
			$results['status_code'] = 400;
			$results['status_message'] = $data['text'];
		}else{
			foreach ($data as $key => $pesan) {
				$row['id'] = $pesan['id'];
				$row['sms'] = $pesan['isiPesan'];
				$row['destination'] = $pesan['noTujuan'];
				$row['waktu'] = $pesan['tgl'].' '.$pesan['waktu'];
				$row['status'] = $pesan['status'];
				$outbox[] = $row;
			}
			$results['status'] = 'success';
			$results['status_code'] = 200;
			$results['status_message'] = 'Berhasil mengambil data';
			$results['outbox'] = $outbox;
		}
		return $results;
	}

?>