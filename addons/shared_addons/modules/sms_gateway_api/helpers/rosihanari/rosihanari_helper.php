<?php

function sendsms($destination, $message)
{
	include 'apiconfig.php';
	require_once('lib/nusoap.php');
	$client = new nusoap_client($urlsoap);
	$result = $client->call('sendsms', array('destination' => $destination, 'message' => $message, 'username' => $apiUsername, 'apikey' => $apiKey));
	return $result;
}

function cekkredit()
{
	include 'apiconfig.php';
	require_once('lib/nusoap.php');
	$client = new nusoap_client($urlsoap);
	$result = $client->call('cekkredit', array('username' => $apiUsername, 'apikey' => $apiKey));
	return $result;
}

function countinbox()
{
	include 'apiconfig.php';
	require_once('lib/nusoap.php');
	$client = new nusoap_client($urlsoap);
	$result = $client->call('countinbox', array('username' => $apiUsername, 'apikey' => $apiKey));
	return $result;
}

function countoutbox()
{
	include 'apiconfig.php';
	require_once('lib/nusoap.php');
	$client = new nusoap_client($urlsoap);
	$result = $client->call('countoutbox', array('username' => $apiUsername, 'apikey' => $apiKey));
	return $result;
}

function readinbox($i='', $n='')
{
	include 'apiconfig.php';
	require_once('lib/nusoap.php');
	$client = new nusoap_client($urlsoap);
	$data = $client->call('readinbox', array('username' => $apiUsername, 'apikey' => $apiKey, 'i' => $i, 'n' => $n));
	if(is_array($data) || $data == ""){
		$results['status'] = 'success';
		$results['status_code'] = 200;
		$results['status_message'] = 'Berhasil mengambil data';
		$results['inbox'] = (is_array($data)) ? $data : "Tidak ada sms";
	}else{
		$results['status'] = 'error';
		$results['status_code'] = 400;
		$results['status_message'] = $data;
	}
	return $results;
}

function readoutbox($i='', $n='')
{
	include 'apiconfig.php';
	require_once('lib/nusoap.php');
	$client = new nusoap_client($urlsoap);
	$data = $client->call('readoutbox', array('username' => $apiUsername, 'apikey' => $apiKey, 'i' => $i, 'n' => $n));           
	if(is_array($data)){
		$results['status'] = 'success';
		$results['status_code'] = 200;
		$results['status_message'] = 'Berhasil mengambil data';
		$results['outbox'] = $data;
	}else{
		$results['status'] = 'error';
		$results['status_code'] = 400;
		$results['status_message'] = $data;
	}
	return $results;

}

function readlastmsgoutbox($i='1', $n='1'){
	include 'apiconfig.php';
	require_once('lib/nusoap.php');
	$client = new nusoap_client($urlsoap);
	$result = $client->call('readoutbox', array('username' => $apiUsername, 'apikey' => $apiKey, 'i' => $i, 'n' => $n));           
	return $result;
}

function delinbox($id)
{
	include 'apiconfig.php';
	require_once('lib/nusoap.php');
	$client = new nusoap_client($urlsoap);
	$result = $client->call('delinbox', array('username' => $apiUsername, 'apikey' => $apiKey, 'id' => $id));
	return $result;
}

function deloutbox($id)
{
	include 'apiconfig.php';
	require_once('lib/nusoap.php');
	$client = new nusoap_client($urlsoap);
	$result = $client->call('deloutbox', array('username' => $apiUsername, 'apikey' => $apiKey, 'id' => $id));
	return $result;
}

?>