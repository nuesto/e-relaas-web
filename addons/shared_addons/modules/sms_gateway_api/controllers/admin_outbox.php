<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * SMS Gateway API Module
 *
 * Module untuk mengelola SMS Gateway API
 *
 */
class Admin_outbox extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'outbox';

  public function __construct()
  {
    parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'access_outbox_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$this->load->config('apiconfig', true);
    $this->apiKeyAccess = $this->config->item('apiKeyAccess', 'apiconfig');
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

    $this->lang->load('sms_gateway_api');		
		$this->load->model('outbox_m');
  }

    /**
	 * List all outbox
   *
   * @return	void
   */
  public function index()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'view_all_outbox') AND ! group_has_role('sms_gateway_api', 'view_own_outbox')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$surffix = '';
    if($_SERVER['QUERY_STRING']){
      $surffix = '?'.$_SERVER['QUERY_STRING'];
    }

    $get_outbox = $this->outbox_m->get_outbox(NULL);
		$pagination_config['base_url'] = base_url(). 'admin/sms_gateway_api/outbox/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['suffix'] = $surffix;
		$pagination_config['total_rows'] = count($get_outbox);
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
    // -------------------------------------
		// Get entries
		// -------------------------------------
		
    $data['outbox']['entries'] = $this->outbox_m->get_outbox($pagination_config);
		$data['outbox']['total'] = count($get_outbox);
		$data['outbox']['pagination'] = $this->pagination->create_links();

		$data['apiKeyAccess'] = $this->apiKeyAccess;

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('sms_gateway_api:outbox:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('sms_gateway_api:outbox:plural'))
			->build('admin/outbox_index', $data);
  }
	
	/**
   * Display one outbox
   *
   * @return  void
   */
  public function view($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'view_all_outbox') AND ! group_has_role('sms_gateway_api', 'view_own_outbox')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
    $data['outbox'] = $this->outbox_m->get_outbox_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'view_all_outbox')){
			if($data['outbox']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('sms_gateway_api:outbox:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('sms_gateway_api:outbox:plural'), '/admin/sms_gateway_api/outbox/index')
			->set_breadcrumb(lang('sms_gateway_api:outbox:view'))
			->build('admin/outbox_entry', $data);
  }
	
	/**
   * Create a new outbox entry
   *
   * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @return	void
   */
  public function create()
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'create_outbox')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_outbox('new')){	
				$this->session->set_flashdata('success', lang('sms_gateway_api:outbox:submit_success'));				
				redirect('admin/sms_gateway_api/outbox/index');
			}else{
				$data['messages']['error'] = lang('sms_gateway_api:outbox:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/sms_gateway_api/outbox/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('sms_gateway_api:outbox:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('sms_gateway_api:outbox:plural'), '/admin/sms_gateway_api/outbox/index')
			->set_breadcrumb(lang('sms_gateway_api:outbox:new'))
			->build('admin/outbox_form', $data);
  }
	
	/**
   * Edit a outbox entry
   *
   * We're passing the
   * id of the entry, which will allow entry_form to
   * repopulate the data from the database.
	 * We are building entry form manually using the fields API
   * and displaying the output in a custom view file.
   *
   * @param   int [$id] The id of the outbox to the be deleted.
   * @return	void
   */
  public function edit($id = 0)
  {
    // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'edit_all_outbox') AND ! group_has_role('sms_gateway_api', 'edit_own_outbox')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('sms_gateway_api', 'edit_all_outbox')){
			$entry = $this->outbox_m->get_outbox_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_outbox('edit', $id)){	
				$this->session->set_flashdata('success', lang('sms_gateway_api:outbox:submit_success'));				
				redirect('admin/sms_gateway_api/outbox/index');
			}else{
				$data['messages']['error'] = lang('sms_gateway_api:outbox:submit_failure');
			}
		}
		
		$data['fields'] = $this->outbox_m->get_outbox_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/sms_gateway_api/outbox/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
    $this->template->title(lang('sms_gateway_api:outbox:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('sms_gateway_api:outbox:plural'), '/admin/sms_gateway_api/outbox/index')
			->set_breadcrumb(lang('sms_gateway_api:outbox:view'), '/admin/sms_gateway_api/outbox/view/'.$id)
			->set_breadcrumb(lang('sms_gateway_api:outbox:edit'))
			->build('admin/outbox_form', $data);
  }
	
	/**
   * Delete a outbox entry
   * 
   * @param   int [$id] The id of outbox to be deleted
   * @return  void
   */
  public function delete($id = 0)
  {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'delete_all_outbox') AND ! group_has_role('sms_gateway_api', 'delete_own_outbox')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('sms_gateway_api', 'delete_all_outbox')){
			$entry = $this->outbox_m->get_outbox_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
    $this->outbox_m->delete_outbox_by_id($id);
    $this->session->set_flashdata('error', lang('sms_gateway_api:outbox:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
    redirect('admin/sms_gateway_api/outbox/index');
  }
	
	/**
   * Insert or update outbox entry in database
   *
   * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
   * @return	boolean
   */
	private function _update_outbox($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('sms_gateway_api:outbox:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->outbox_m->insert_outbox($values);
				
			}
			else
			{
				$result = $this->outbox_m->update_outbox($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}