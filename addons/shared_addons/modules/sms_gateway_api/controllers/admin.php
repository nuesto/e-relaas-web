<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * SMS Gateway API Module
 *
 * Module untuk mengelola SMS Gateway API
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('admin/sms_gateway_api/inbox/index');
    }

}