<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * SMS Gateway API Module
 *
 * Module untuk mengelola SMS Gateway API
 *
 */
class Admin_action extends Admin_Controller
{
	// -------------------------------------
  // This will set the active section tab
	// -------------------------------------
	
  protected $section = 'sms_gateway_api';

  public function __construct()
  {
    parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('sms_gateway_api', 'access_sms_gateway_api')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

    $this->lang->load('sms_gateway_api');
    $this->load->config('apiconfig', true);
    $this->apiKeyAccess = $this->config->item('apiKeyAccess', 'apiconfig');
    $provider = $this->config->item('provider', 'apiconfig');
    $this->load->helper($provider.'/'.$provider);
  }

  /**
 		* List all 
		*
		* @return	void
		*/
  public function index()
  {
		// $data['sms_inbox'] = '';
  	// 	$data['sms_outbox'] = '';

		// $http_response= $this->get_http_response();
		// if($http_response['available']){
		// 	$data['sms_inbox'] = readinbox();
		// 	$data['sms_outbox'] = readoutbox(3,5);
		// }
		$data['apiKeyAccess'] = $this->apiKeyAccess;  	

		// -------------------------------------
    // Build the page. See views/admin/index.php
    // for the view code.
		// -------------------------------------
		
    $this->template->title(lang('sms_gateway_api::singular'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('sms_gateway_api::singular'))
			->build('admin/manage_index', $data);
  }

  public function get_http_response(){
  	$apiUrl = $this->config->item('apiUrl', 'apiconfig');
  	$content = @file_get_contents($apiUrl);
  	if($content === false){
  		$result['msg'] = 'failed to open stream: HTTP request failed!';
  		$result['available'] = false;
  	}else{
  		$result['msg'] = $http_response_header[0];
  		$result['available'] = true;
  	}
 		return $result;
  }

	// --------------------------------------------------------------------------

}