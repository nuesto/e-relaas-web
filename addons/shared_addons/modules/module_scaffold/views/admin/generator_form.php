<script src="<?php echo base_url(); ?>system/cms/themes/ace/js/jquery/jquery.slugify.js"></script>

<div class="page-header">
	<h1>Tambah Module Generator</h1>
</div>

<?php 
	if($mode == 'new'){
		$return_url = 'admin/module_scaffold/generator/create';
	}else{
		$return_url = 'admin/module_scaffold/generator/edit/'.$entry_id;
	}
?>
<form action="<?php echo base_url().$return_url; ?>" class="form-horizontal" enctype="multipart/form-data" method="post" accept-charset="utf-8">

<div class="form_inputs">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="module_name">Module Name <span>*</span></label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('module_name') != NULL){
					$value = $this->input->post('module_name');
				}elseif($mode == 'edit'){
					$value = $fields['module_name'];
				}
			?>
			<input type="text" name="module_name" value="<?php echo $value; ?>" id="module_name" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="module_slug">Slug <span>*</span></label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('module_slug') != NULL){
					$value = $this->input->post('module_slug');
				}elseif($mode == 'edit'){
					$value = $fields['module_slug'];
				}
			?>
			<input type="text" name="module_slug" value="<?php echo $value; ?>" id="module_slug" class="form-control">

			<script>
				$('#module_name').slugify({ slug: '#module_slug', type: '_' });
			</script>					
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="module_description">Description <span>*</span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('module_description') != NULL){
					$value = $this->input->post('module_description');
				}elseif($mode == 'edit'){
					$value = $fields['module_description'];
				}
			?>
			<input type="text" name="module_description" value="<?php echo $value; ?>" id="module_description" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="module_has_frontend">Has Frontend <span>*</span></label>

		<div class="col-sm-1">
			<?php 
				$value = NULL;
				if($this->input->post('module_has_frontend') != NULL){
					$value = $this->input->post('module_has_frontend');
				}elseif($mode == 'edit'){
					$value = $fields['module_has_frontend'];
				}
			?>
			<select name="module_has_frontend" id="module_has_frontend" class="form-control">
				<option value=""></option>
				<option value="true" <?php if($value == 'true'){echo 'selected';} ?>>Yes</option>
				<option value="false" <?php if($value == 'false'){echo 'selected';} ?>>No</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="module_has_backend">Has Backend <span>*</span></label>

		<div class="col-sm-1">
			<?php 
				$value = NULL;
				if($this->input->post('module_has_backend') != NULL){
					$value = $this->input->post('module_has_backend');
				}elseif($mode == 'edit'){
					$value = $fields['module_has_backend'];
				}
			?>
			<select name="module_has_backend" id="module_has_backend" class="form-control">
				<option value=""></option>
				<option value="true" <?php if($value == 'true'){echo 'selected';} ?>>Yes</option>
				<option value="false" <?php if($value == 'false'){echo 'selected';} ?>>No</option>
			</select>
						
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="module_menu">Menu <span>*</span></label>

		<div class="col-sm-2">
			<?php 
				$value = NULL;
				if($this->input->post('module_menu') != NULL){
					$value = $this->input->post('module_menu');
				}elseif($mode == 'edit'){
					$value = $fields['module_menu'];
				}
			?>
			<input type="text" name="module_menu" value="<?php echo $value; ?>" id="module_menu" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="module_menu">Admin URI Segment <span>*</span></label>

		<div class="col-sm-2">
			<?php 
				$value = NULL;
				if($this->input->post('admin_uri_segment') != NULL){
					$value = $this->input->post('admin_uri_segment');
				}elseif($mode == 'edit'){
					$value = $fields['admin_uri_segment'];
				}
			?>
			<input type="text" name="admin_uri_segment" value="<?php echo $value; ?>" id="admin_uri_segment" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="module_streams">Tables and Columns *</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('module_fields') != NULL){
					$value = $this->input->post('module_fields');
				}elseif($mode == 'edit'){
					$value = $fields['module_fields'];
				}
			?>
			<textarea class="col-sm-12" rows="10" name="module_fields" class="item_input" placeholder="Describe in JSON Array format"><?php echo $value; ?></textarea>
		</div>
	</div>

</div>


<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span>Simpan</span></button>
		<a href="<?php echo base_url(); ?>admin/module_scaffold/generator/index" class="btn btn-danger">Batal</a>
	</div>
</div>

</form>