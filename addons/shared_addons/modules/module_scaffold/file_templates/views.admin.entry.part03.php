	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('%module_slug%:created'); ?></div>
		<?php if(isset($%section_slug%['created_on'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo format_date($%section_slug%['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('%module_slug%:updated'); ?></div>
		<?php if(isset($%section_slug%['updated_on'])){ ?>
		<div class="data-detail-value col-sm-10"><?php echo format_date($%section_slug%['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="data-detail-value col-sm-10">-</div>
		<?php } ?>
	</div>

	<div class="row data-detail">
		<div class="data-detail-label col-sm-2"><?php echo lang('%module_slug%:created_by'); ?></div>
		<div class="data-detail-value col-sm-10"><?php echo user_displayname($%section_slug%['created_by'], true); ?></div>
	</div>
</div>