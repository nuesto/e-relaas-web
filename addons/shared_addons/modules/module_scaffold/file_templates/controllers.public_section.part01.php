<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * %module_name% Module
 *
 * %module_description%
 *
 */
class %module_class%_%section_slug% extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = '%section_slug%';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('%module_slug%');
		
		$this->load->model('%section_slug%_m');
    }

    /**
	 * List all %section_name%
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'view_all_%section_slug%') AND ! group_has_role('%module_slug%', 'view_own_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). '%module_slug%/%section_slug%/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->%section_slug%_m->count_all_%section_slug%();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){

				$params[substr($key, 2)] = $value;
			}
		}
		
        $data['%section_slug%']['entries'] = $this->%section_slug%_m->get_%section_slug%($pagination_config);
		$data['%section_slug%']['total'] = count($data['%section_slug%']['entries']);
		$data['%section_slug%']['pagination'] = $this->pagination->create_links();

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('%module_slug%:%section_slug%:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'))
			->build('%section_slug%_index', $data);
    }
	
	/**
     * Display one %section_name%
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'view_all_%section_slug%') AND ! group_has_role('%module_slug%', 'view_own_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['%section_slug%'] = $this->%section_slug%_m->get_%section_slug%_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('%module_slug%', 'view_all_%section_slug%')){
			if($data['%section_slug%']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('%module_slug%:%section_slug%:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('%module_slug%', 'view_all_%section_slug%') OR group_has_role('%module_slug%', 'view_own_%section_slug%')){
			$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'), '/%module_slug%/%section_slug%/index');
		}

		$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:view'))
			->build('%section_slug%_entry', $data);
    }
	
	/**
     * Create a new %section_name% entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'create_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_%section_slug%('new')){	
				$this->session->set_flashdata('success', lang('%module_slug%:%section_slug%:submit_success'));				
				redirect('%module_slug%/%section_slug%/index');
			}else{
				$data['messages']['error'] = lang('%module_slug%:%section_slug%:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['uri'] = get_query_string(5);
		$data['return'] = '%module_slug%/%section_slug%/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('%module_slug%:%section_slug%:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('%module_slug%', 'view_all_%section_slug%') OR group_has_role('%module_slug%', 'view_own_%section_slug%')){
			$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'), '/%module_slug%/%section_slug%/index');
		}

		$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:new'))
			->build('%section_slug%_form', $data);
    }
	
	/**
     * Edit a %section_name% entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the %section_name% to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'edit_all_%section_slug%') AND ! group_has_role('%module_slug%', 'edit_own_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('%module_slug%', 'edit_all_%section_slug%')){
			$entry = $this->%section_slug%_m->get_%section_slug%_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_%section_slug%('edit', $id)){	
				$this->session->set_flashdata('success', lang('%module_slug%:%section_slug%:submit_success'));				
				redirect('%module_slug%/%section_slug%/index');
			}else{
				$data['messages']['error'] = lang('%module_slug%:%section_slug%:submit_failure');
			}
		}
		
		$data['fields'] = $this->%section_slug%_m->get_%section_slug%_by_id($id);
		$data['mode'] = 'edit';
		$data['uri'] = get_query_string(5);
		$data['return'] = '%module_slug%/%section_slug%/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
       $this->template->title(lang('%module_slug%:%section_slug%:edit'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('%module_slug%', 'view_all_%section_slug%') OR group_has_role('%module_slug%', 'view_own_%section_slug%')){
			$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'), '/%module_slug%/%section_slug%/index')
			->set_breadcrumb(lang('%module_slug%:%section_slug%:view'), '/%module_slug%/%section_slug%/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'))
			->set_breadcrumb(lang('%module_slug%:%section_slug%:view'));
		}

		$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:edit'))
			->build('%section_slug%_form', $data);
    }
	
	/**
     * Delete a %section_name% entry
     * 
     * @param   int [$id] The id of %section_name% to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'delete_all_%section_slug%') AND ! group_has_role('%module_slug%', 'delete_own_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('%module_slug%', 'delete_all_%section_slug%')){
			$entry = $this->%section_slug%_m->get_%section_slug%_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->%section_slug%_m->delete_%section_slug%_by_id($id);
		$this->session->set_flashdata('error', lang('%module_slug%:%section_slug%:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(5);
        redirect('%module_slug%/%section_slug%/index'.$data['uri']);
    }
	
	/**
     * Insert or update %section_name% entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_%section_slug%($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('%module_slug%:%section_slug%:field_name'), '');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->%section_slug%_m->insert_%section_slug%($values);
			}
			else
			{
				$result = $this->%section_slug%_m->update_%section_slug%($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}