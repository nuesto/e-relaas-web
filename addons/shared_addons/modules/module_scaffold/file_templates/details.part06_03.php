			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('%module_slug%_%section_slug%', TRUE);
		$this->db->query("CREATE INDEX author_%module_slug%_%section_slug%_idx ON ".$this->db->dbprefix("%module_slug%_%section_slug%")."(created_by)");

		// edit this query to add foreign key
		/*$this->db->query("CREATE INDEX fk_%module_slug%_%section_slug%_ref_idx ON ".$this->db->dbprefix("%module_slug%_%section_slug%")."(id_ref)");
		$this->db->query("ALTER TABLE ".$this->db->dbprefix("%module_slug%_%section_slug%")." 
			ADD CONSTRAINT `fk_%module_slug%_%section_slug%_ref` 
			FOREIGN KEY (`id_ref`) 
			REFERENCES ".$this->db->dbprefix("%module_slug%_ref")." (`id`) 
			ON DELETE RESTRICT 
			ON UPDATE RESTRICT");*/

