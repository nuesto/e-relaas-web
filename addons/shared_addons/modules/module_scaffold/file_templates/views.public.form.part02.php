	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="%field_slug%"><?php echo lang('%module_slug%:%section_slug%:%field_slug%'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('%field_slug%') != NULL){
					$value = $this->input->post('%field_slug%');
				}elseif($this->input->get('f-%field_slug%') != NULL AND $mode == 'new'){
					$value = $this->input->get('f-%field_slug%');
				}elseif($mode == 'edit'){
					$value = $fields['%field_slug%'];
				}
			?>
			<input name="%field_slug%" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

