<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * %module_name% Module
 *
 * %module_description%
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('%admin_uri_segment%/%module_slug%/%section_slug%/index');
    }

}