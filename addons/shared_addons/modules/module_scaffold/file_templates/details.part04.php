		if(group_has_role('%module_slug%', 'access_%section_slug%_backend')){
			$info['sections']['%section_slug%']['name'] = '%module_slug%:%section_slug%:plural';
			$info['sections']['%section_slug%']['uri'] = array('urls'=>array('%admin_uri_segment%/%module_slug%/%section_slug%/index','%admin_uri_segment%/%module_slug%/%section_slug%/create','%admin_uri_segment%/%module_slug%/%section_slug%/view%1','%admin_uri_segment%/%module_slug%/%section_slug%/edit%1','%admin_uri_segment%/%module_slug%/%section_slug%/view%2','%admin_uri_segment%/%module_slug%/%section_slug%/edit%2'));
			
			if(group_has_role('%module_slug%', 'create_%section_slug%')){
				$info['sections']['%section_slug%']['shortcuts']['create'] = array(
					'name' => '%module_slug%:%section_slug%:new',
					'uri' => '%admin_uri_segment%/%module_slug%/%section_slug%/create',
					'class' => 'add'
				);
			}
		}
		
