<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * %module_name% Module
 *
 * %module_description%
 *
 */
class Admin_%section_slug% extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = '%section_slug%';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'access_%section_slug%_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('%admin_uri_segment%');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('%module_slug%');		
		$this->load->model('%section_slug%_m');
    }

    /**
	 * List all %section_name%
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'view_all_%section_slug%') AND ! group_has_role('%module_slug%', 'view_own_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('%admin_uri_segment%');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). '%admin_uri_segment%/%module_slug%/%section_slug%/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->%section_slug%_m->count_all_%section_slug%();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		// handle filter get params
		$params = array();
		foreach ($_GET as $key => $value){
			if(strpos($key, "f-") === 0 
				AND $value != ''
				AND substr($key, 2) != ''){

				$params[substr($key, 2)] = $value;
			}
		}
		
        $data['%section_slug%']['entries'] = $this->%section_slug%_m->get_%section_slug%($pagination_config, $params);
		$data['%section_slug%']['total'] = count($data['%section_slug%']['entries']);
		$data['%section_slug%']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/%admin_uri_segment%/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['%section_slug%']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('%admin_uri_segment%/%module_slug%/%section_slug%/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('%admin_uri_segment%/%module_slug%/%section_slug%/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('%module_slug%:%section_slug%:plural'))
			->set_breadcrumb('Home', '/%admin_uri_segment%')
			->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'))
			->append_css('module::%module_slug%.css')
			->append_js('module::%module_slug%.js')
			->build('admin/%section_slug%_index', $data);
    }
	
	/**
     * Display one %section_name%
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'view_all_%section_slug%') AND ! group_has_role('%module_slug%', 'view_own_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('%admin_uri_segment%');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['%section_slug%'] = $this->%section_slug%_m->get_%section_slug%_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'view_all_%section_slug%')){
			if($data['%section_slug%']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('%admin_uri_segment%');
			}
		}

		// -------------------------------------
        // Build the page. See views/%admin_uri_segment%/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('%module_slug%:%section_slug%:view'))
			->set_breadcrumb('Home', '/%admin_uri_segment%');
			
		if(group_has_role('%module_slug%', 'view_all_%section_slug%') OR group_has_role('%module_slug%', 'view_own_%section_slug%')){
			$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'), '/%admin_uri_segment%/%module_slug%/%section_slug%/index');
		}

		$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:view'))
			->append_css('module::%module_slug%.css')
			->append_js('module::%module_slug%.js')
			->build('admin/%section_slug%_entry', $data);
    }
	
	/**
     * Create a new %section_name% entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'create_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('%admin_uri_segment%');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_%section_slug%('new')){	
				$this->session->set_flashdata('success', lang('%module_slug%:%section_slug%:submit_success'));				
				redirect('%admin_uri_segment%/%module_slug%/%section_slug%/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('%module_slug%:%section_slug%:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = '%admin_uri_segment%/%module_slug%/%section_slug%/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('%module_slug%:%section_slug%:new'))
			->set_breadcrumb('Home', '/%admin_uri_segment%');
			
		if(group_has_role('%module_slug%', 'view_all_%section_slug%') OR group_has_role('%module_slug%', 'view_own_%section_slug%')){
			$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'), '/%admin_uri_segment%/%module_slug%/%section_slug%/index');
		}

		$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:new'))
			->append_css('module::%module_slug%.css')
			->append_js('module::%module_slug%.js')
			->build('admin/%section_slug%_form', $data);
    }
	
	/**
     * Edit a %section_name% entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the %section_name% to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'edit_all_%section_slug%') AND ! group_has_role('%module_slug%', 'edit_own_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('%admin_uri_segment%');
		}
		
		// Check view all/own permission
		if(! group_has_role('%module_slug%', 'edit_all_%section_slug%')){
			$entry = $this->%section_slug%_m->get_%section_slug%_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('%admin_uri_segment%');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_%section_slug%('edit', $id)){	
				$this->session->set_flashdata('success', lang('%module_slug%:%section_slug%:submit_success'));				
				redirect('%admin_uri_segment%/%module_slug%/%section_slug%/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('%module_slug%:%section_slug%:submit_failure');
			}
		}
		
		$data['fields'] = $this->%section_slug%_m->get_%section_slug%_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = '%admin_uri_segment%/%module_slug%/%section_slug%/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('%module_slug%:%section_slug%:edit'))
			->set_breadcrumb('Home', '/%admin_uri_segment%');
			
		if(group_has_role('%module_slug%', 'view_all_%section_slug%') OR group_has_role('%module_slug%', 'view_own_%section_slug%')){
			$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'), '/%admin_uri_segment%/%module_slug%/%section_slug%/index')
			->set_breadcrumb(lang('%module_slug%:%section_slug%:view'), '/%admin_uri_segment%/%module_slug%/%section_slug%/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'))
			->set_breadcrumb(lang('%module_slug%:%section_slug%:view'));
		}

		$this->template->set_breadcrumb(lang('%module_slug%:%section_slug%:edit'))
			->append_css('module::%module_slug%.css')
			->append_js('module::%module_slug%.js')
			->build('admin/%section_slug%_form', $data);
    }
	
	/**
     * Delete a %section_name% entry
     * 
     * @param   int [$id] The id of %section_name% to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'delete_all_%section_slug%') AND ! group_has_role('%module_slug%', 'delete_own_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('%admin_uri_segment%');
		}
		// Check view all/own permission
		if(! group_has_role('%module_slug%', 'delete_all_%section_slug%')){
			$entry = $this->%section_slug%_m->get_%section_slug%_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('%admin_uri_segment%');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->%section_slug%_m->delete_%section_slug%_by_id($id);
        $this->session->set_flashdata('error', lang('%module_slug%:%section_slug%:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('%admin_uri_segment%/%module_slug%/%section_slug%/index'.$data['uri']);
    }
	
	/**
     * Insert or update %section_name% entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_%section_slug%($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('%module_slug%:%section_slug%:field_name'), '');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->%section_slug%_m->insert_%section_slug%($values);
				
			}
			else
			{
				$result = $this->%section_slug%_m->update_%section_slug%($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}