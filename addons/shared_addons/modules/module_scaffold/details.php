<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Module_scaffold extends Module
{
    public $version = '1.2.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Module Scaffolding',
			'id' => 'Module Scaffolding',
		);
		$info['description'] = array(
			'en' => 'Module to generate module codebase automatically.',
			'id' => 'Modul untuk menhasilkan codebase module secara otomatis.',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		$info['menu'] = 'addons';
		$info['roles'] = array(
			'access_generator_backend', 'manage_all_generator',
		);

		if(group_has_role('module_scaffold', 'access_generator_backend')){
			$info['sections']['generator']['name'] = 'module_scaffold:generator:plural';
			$info['sections']['generator']['uri'] = 'admin/module_scaffold/generator/index';

			if(group_has_role('module_scaffold', 'create_generator')){
				$info['sections']['generator']['shortcuts']['create'] = array(
					'name' => 'module_scaffold:generator:new',
					'uri' => 'admin/module_scaffold/generator/create',
					'class' => 'add'
				);
			}
		}

		return $info;
    }

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

        $this->db->query("CREATE TABLE ".$this->db->dbprefix("module_scaffold_generator")." (
            `id` int(9) NOT NULL,
            `created` datetime NOT NULL,
            `updated` datetime DEFAULT NULL,
            `ordering_count` int(11) DEFAULT NULL,
            `module_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `module_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `module_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `module_has_frontend` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `module_has_backend` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `module_menu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `admin_uri_segment` varchar(52) COLLATE utf8_unicode_ci DEFAULT 'admin',
            `module_fields` text COLLATE utf8_unicode_ci
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

        $this->db->query("ALTER TABLE ".$this->db->dbprefix("module_scaffold_generator")."
            ADD PRIMARY KEY (`id`);");

        $this->db->query("ALTER TABLE ".$this->db->dbprefix("module_scaffold_generator")."
            MODIFY `id` int(9) NOT NULL AUTO_INCREMENT;");

        $this->version = '1.1.0';

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
        $this->load->dbforge();
        $this->dbforge->drop_table('module_scaffold_generator');

        return true;
    }

    public function upgrade($old_version)
    {
        switch ($old_version) {
            case '1.1.0':
                $this->load->dbforge();
                $this->dbforge->drop_table('module_scaffold_generator');

                 $this->db->query("CREATE TABLE ".$this->db->dbprefix("module_scaffold_generator")." (
                    `id` int(9) NOT NULL,
                    `created` datetime NOT NULL,
                    `updated` datetime DEFAULT NULL,
                    `ordering_count` int(11) DEFAULT NULL,
                    `module_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                    `module_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                    `module_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                    `module_has_frontend` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                    `module_has_backend` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                    `module_menu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                    `admin_uri_segment` varchar(52) COLLATE utf8_unicode_ci DEFAULT 'admin',
                    `module_fields` text COLLATE utf8_unicode_ci
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

                $this->db->query("ALTER TABLE ".$this->db->dbprefix("module_scaffold_generator")."
                    ADD PRIMARY KEY (`id`);");

                $this->db->query("ALTER TABLE ".$this->db->dbprefix("module_scaffold_generator")."
                    MODIFY `id` int(9) NOT NULL AUTO_INCREMENT;");

                $this->version = '1.2.0';
                break;
        }

        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}
