<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Konten model
 *
 * @author Aditya Satrya
 */
class Generator_m extends MY_Model {
	
	public function get_generator($pagination_config = NULL,$filter = NULL)
	{
		$this->db->select('*');
		
		$query = $this->db->get('module_scaffold_generator');
		$result = $query->result_array();
		
        return $result;
	}

	public function get_generator_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('module_scaffold_generator');
		$result = $query->row_array();
		
		return $result;
	}

	public function insert_generator($values)
	{
		$values['created'] = date("Y-m-d H:i:s");
		$this->db->insert('module_scaffold_generator', $values);
		$insert_id = $this->db->insert_id();

		return $insert_id;
	}
	
	public function update_generator($values, $row_id)
	{
		$values['updated'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('module_scaffold_generator', $values); 
	}

	public function delete_generator_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('module_scaffold_generator');
	}
}