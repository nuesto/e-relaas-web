<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dashboard Mockup Module
 *
 * Prototype dashboard user interface
 *
 */
class Dashboard_mockup extends Public_Controller
{
	// -------------------------------------
	// This will set the active section tab
	// -------------------------------------

	protected $section = 'dashboard_mockup';

	public function __construct()
	{
		parent::__construct();
	}

	/**
	* Sample load data stats to widget
	*
	* @return  void
	*/
	public function stats()
	{
		$max_item = rand(5, 15);
		$data = array();
		for ($i=0; $i < $max_item; $i++) {
			$data['items'][$i] = rand(1, 15);
		}

		$data['total'] = rand(5, 99999);

		die(json_encode($data));
	}

	/**
	* Sample load view to widget
	*
	* @return  void
	*/
	public function view($view)
	{
		$data = array();

		echo $this->load->view('dashboard_mockup/'.$view, $data, true);
	}

}