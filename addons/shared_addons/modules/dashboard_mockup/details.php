<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Dashboard_mockup extends Module
{
	public $version = '1.1';

	public function info()
	{
		$info = array();
		$info['name'] = array(
			'en' => 'Dashboard Mockup',
			'id' => 'Dashboard Mockup',
		);
		$info['description'] = array(
			'en' => 'Module dashboard prototype.',
			'id' => 'Modul prototipe dasbor.',
		);
		$info['frontend'] = true;
		$info['backend'] = false;
		$info['roles'] = array();

		return $info;
	}

	/**
	 * Install
	 *
	 * This function will set up our streams
	 *
	 */
	public function install()
	{
		$this->load->library('widgets/widgets');

		$this->db->trans_begin();

		$status = true;

		// create area dashboard stats
		$area_dashboard_stats = $this->widgets->get_area('dashboard-stats');
		// get widget stats-box
		$widget_statbox = $this->widgets->get_widget('statbox');
		// get widget dashboard
		$widget_dashboard = $this->widgets->get_widget('dashboard');

		if ($area_dashboard_stats != NULL AND $widget_statbox != NULL)
		{
			// create instance
			$instances = array(
				array(
					'title' => 'Messages',
					'src_url' => 'dashboard_mockup/stats',
					'target_url' => 'admin/blog',
					'color' => 'bg-info',
					'icon_class' => 'fa fa-envelope',
					'widget_area_id' => $area_dashboard_stats->id,
					'widget_id' => $widget_statbox->id,
				),
				array(
					'title' => 'Orders',
					'src_url' => 'dashboard_mockup/stats',
					'target_url' => 'admin/blog',
					'color' => 'bg-success',
					'icon_class' => 'fa fa-money',
					'widget_area_id' => $area_dashboard_stats->id,
					'widget_id' => $widget_statbox->id,
				),
				array(
					'title' => 'Visitors/Day',
					'src_url' => 'dashboard_mockup/stats',
					'target_url' => 'admin/blog',
					'color' => 'bg-danger',
					'show_graphic' => '1',
					'graphic_options' => 'sparkHeight="56px" sparkBarWidth="6" sparkBarColor="#ffffff"',
					'widget_area_id' => $area_dashboard_stats->id,
					'widget_id' => $widget_statbox->id,
				),
				array(
					'title' => 'Total Profit',
					'src_url' => 'dashboard_mockup/stats',
					'target_url' => 'admin/blog',
					'color' => 'bg-warning',
					'show_graphic' => '1',
					'graphic_options' => 'sparkHeight="56px" sparkType="line" sparkLineWidth="0" sparkDefaultPixelsPerValue="7" sparkLineColor="#ffffff" sparkFillColor="#ffffff"',
					'widget_area_id' => $area_dashboard_stats->id,
					'widget_id' => $widget_statbox->id,
				),
			);

			foreach ($instances as $instance) {
				$title = $instance['title'];
				$widget_id = $instance['widget_id'];
				$widget_area_id = $instance['widget_area_id'];

				$this->widgets->add_instance($title, $widget_id, $widget_area_id, $instance);
			}
		}
		else
		{
			$status = false;
		}

		// create area dashboard left
		$area_dashboard_left = $this->widgets->get_area('dashboard-left');
		if ($area_dashboard_left != NULL AND $widget_dashboard != NULL)
		{
			// create instances
			$instances = array(
				array(
					'title' => 'Website Traffic',
					'view_uri' => 'dashboard_mockup/view/website_traffic',
					'color_scheme' => 'warning',
					'show_title' => '1',
					'has_container' => '1',
					'widget_area_id' => $area_dashboard_left->id,
					'widget_id' => $widget_dashboard->id,
				),
				array(
					'title' => 'Last Order',
					'view_uri' => 'dashboard_mockup/view/last_order',
					'color_scheme' => 'info',
					'show_title' => '1',
					'has_container' => '1',
					'widget_area_id' => $area_dashboard_left->id,
					'widget_id' => $widget_dashboard->id,
				),
			);

			foreach ($instances as $instance) {
				$title = $instance['title'];
				$widget_id = $instance['widget_id'];
				$widget_area_id = $instance['widget_area_id'];

				$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $instance);				
			}
		}
		else
		{
			$status = false;
		}

		// create area dashboard right
		$area_dashboard_right = $this->widgets->get_area('dashboard-right');
		if ($area_dashboard_right != NULL AND $widget_dashboard != NULL)
		{
			// create instance
			$instances = array(
				array(
					'title' => 'Today Status',
					'view_uri' => 'dashboard_mockup/view/today_status',
					'color_scheme' => 'danger',
					'show_title' => '1',
					'has_container' => '1',
					'widget_area_id' => $area_dashboard_right->id,
					'widget_id' => $widget_dashboard->id,
				),
				array(
					'title' => 'Browser Access',
					'view_uri' => 'dashboard_mockup/view/browser_access',
					'color_scheme' => 'success',
					'show_title' => '1',
					'has_container' => '1',
					'widget_area_id' => $area_dashboard_right->id,
					'widget_id' => $widget_dashboard->id,
				),
				array(
					'title' => 'File Upload',
					'view_uri' => 'dashboard_mockup/view/file_upload',
					'color_scheme' => 'info',
					'show_title' => '1',
					'has_container' => '1',
					'widget_area_id' => $area_dashboard_right->id,
					'widget_id' => $widget_dashboard->id,
				),
			);

			foreach ($instances as $instance) {
				$title = $instance['title'];
				$widget_id = $instance['widget_id'];
				$widget_area_id = $instance['widget_area_id'];

				$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $instance);	
			}
		}
		else
		{
			$status = false;
		}

		// create area dashboard middle left
		$area_dashboard_middle_left = $this->widgets->get_area('dashboard-middle-left');
		if ($area_dashboard_middle_left != NULL AND $widget_dashboard != NULL)
		{
			// create instance
			$instance = array(
				array(
					'title' => 'Visitor Location',
					'view_uri' => 'dashboard_mockup/view/visitor_location',
					'color_scheme' => 'warning',
					'show_title' => '1',
					'has_container' => '1',
					'widget_area_id' => $area_dashboard_middle_left->id,
					'widget_id' => $widget_dashboard->id,
				),
				);

			foreach ($instance as $itc) {
				$title = $itc['title'];
				$widget_id = $itc['widget_id'];
				$widget_area_id = $itc['widget_area_id'];

				$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
			}
		}
		else
		{
			$status = false;
		}

		// create area dashboard middle right
		$area_dashboard_middle_right = $this->widgets->get_area('dashboard-middle-right');
		if ($area_dashboard_middle_right != NULL AND $widget_dashboard != NULL)
		{
			// create instance
			$instances = array(
				array(
					'title' => 'Project Feed',
					'view_uri' => 'dashboard_mockup/view/project_feed',
					'color_scheme' => 'danger',
					'show_title' => '1',
					'has_container' => '1',
					'widget_area_id' => $area_dashboard_middle_right->id,
					'widget_id' => $widget_dashboard->id,
				),
			);

			foreach ($instances as $instance) {
				$title = $instance['title'];
				$widget_id = $instance['widget_id'];
				$widget_area_id = $instance['widget_area_id'];

				$result = $this->widgets->add_instance($title, $widget_id, $widget_area_id, $instance);				
			}
		}
		else
		{
			$status = false;
		}

		// create area dashboard bottom left
		$area_dashboard_bottom_left = $this->widgets->get_area('dashboard-bottom-left');
		if ($area_dashboard_bottom_left != NULL AND $widget_dashboard != NULL)
		{
			// create instance
			$instances = array(
				array(
					'title' => 'Curve Chart',
					'view_uri' => 'dashboard_mockup/view/curve_chart',
					'color_scheme' => 'success',
					'show_title' => '1',
					'has_container' => '1',
					'widget_area_id' => $area_dashboard_bottom_left->id,
					'widget_id' => $widget_dashboard->id,
				),
			);

			foreach ($instances as $instance) {
				$title = $instance['title'];
				$widget_id = $instance['widget_id'];
				$widget_area_id = $instance['widget_area_id'];

				$this->widgets->add_instance($title, $widget_id, $widget_area_id, $instance);
			}
		}
		else
		{
			$status = false;
		}

		// create area dashboard bottom right
		$area_dashboard_bottom_right = $this->widgets->get_area('dashboard-bottom-right');
		if ($area_dashboard_bottom_right != NULL AND $widget_dashboard != NULL)
		{
			// create instance
			$instance = array(
				array(
					'title' => 'Quick Post',
					'view_uri' => 'dashboard_mockup/view/quick_post',
					'color_scheme' => 'info',
					'show_title' => '1',
					'has_container' => '1',
					'widget_area_id' => $area_dashboard_bottom_right->id,
					'widget_id' => $widget_dashboard->id,
				),
			);

			foreach ($instance as $itc) {
				$title = $itc['title'];
				$widget_id = $itc['widget_id'];
				$widget_area_id = $itc['widget_area_id'];

				$this->widgets->add_instance($title, $widget_id, $widget_area_id, $itc);				
			}
		}
		else
		{
			$status = false;
		}
		
		if ($this->db->trans_status() === FALSE OR $status == FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return TRUE;
		}
	}

	/**
	 * Uninstall
	 *
	 * Uninstall our module - this should tear down
	 * all information associated with it.
	 */
	public function uninstall()
	{
		$this->load->library('widgets/widgets');
		$this->load->model('widgets/widget_m');

		$this->db->trans_begin();

		$status = true;

		// create area dashboard stats
		$area_dashboard_stats = $this->widgets->get_area('dashboard-stats');
		// get widget stats-box
		$widget_statbox = $this->widgets->get_widget('statbox');
		// get widget dashboard
		$widget_dashboard = $this->widgets->get_widget('dashboard');

		if ($area_dashboard_stats != NULL AND $widget_statbox != NULL)
		{
			// create instance
			$instances = array(
				array(
					'title' => 'Messages',
					'widget_area_id' => $area_dashboard_stats->id,
					'widget_id' => $widget_statbox->id,
				),
				array(
					'title' => 'Orders',
					'widget_area_id' => $area_dashboard_stats->id,
					'widget_id' => $widget_statbox->id,
				),
				array(
					'title' => 'Visitors/Day',
					'widget_area_id' => $area_dashboard_stats->id,
					'widget_id' => $widget_statbox->id,
				),
				array(
					'title' => 'Total Profit',
					'widget_area_id' => $area_dashboard_stats->id,
					'widget_id' => $widget_statbox->id,
				),
			);

			foreach ($instances as $instance) {
				$this->widget_m->delete_instance_by_params($instance);
			}
		}
		else
		{
			$status = false;
		}

		// create area dashboard left
		$area_dashboard_left = $this->widgets->get_area('dashboard-left');
		if ($area_dashboard_left != NULL AND $widget_dashboard != NULL)
		{
			// create instances
			$instances = array(
				array(
					'title' => 'Website Traffic',
					'widget_area_id' => $area_dashboard_left->id,
					'widget_id' => $widget_dashboard->id,
				),
				array(
					'title' => 'Last Order',
					'widget_area_id' => $area_dashboard_left->id,
					'widget_id' => $widget_dashboard->id,
				),
			);

			foreach ($instances as $instance) {
				$result = $this->widget_m->delete_instance_by_params($instance);
			}
		}
		else
		{
			$status = false;
		}

		// create area dashboard right
		$area_dashboard_right = $this->widgets->get_area('dashboard-right');
		if ($area_dashboard_right != NULL AND $widget_dashboard != NULL)
		{
			// create instance
			$instances = array(
				array(
					'title' => 'Today Status',
					'widget_area_id' => $area_dashboard_right->id,
					'widget_id' => $widget_dashboard->id,
				),
				array(
					'title' => 'Browser Access',
					'widget_area_id' => $area_dashboard_right->id,
					'widget_id' => $widget_dashboard->id,
				),
				array(
					'title' => 'File Upload',
					'widget_area_id' => $area_dashboard_right->id,
					'widget_id' => $widget_dashboard->id,
				),
			);

			foreach ($instances as $instance) {
				$result = $this->widget_m->delete_instance_by_params($instance);
			}
		}
		else
		{
			$status = false;
		}

		// create area dashboard middle left
		$area_dashboard_middle_left = $this->widgets->get_area('dashboard-middle-left');
		if ($area_dashboard_middle_left != NULL AND $widget_dashboard != NULL)
		{
			// create instance
			$instances = array(
				array(
					'title' => 'Visitor Location',
					'widget_area_id' => $area_dashboard_middle_left->id,
					'widget_id' => $widget_dashboard->id,
				),
				);

			foreach ($instances as $instance) {
				$result = $this->widget_m->delete_instance_by_params($instance);
			}
		}
		else
		{
			$status = false;
		}

		// create area dashboard middle right
		$area_dashboard_middle_right = $this->widgets->get_area('dashboard-middle-right');
		if ($area_dashboard_middle_right != NULL AND $widget_dashboard != NULL)
		{
			// create instance
			$instances = array(
				array(
					'title' => 'Project Feed',
					'widget_area_id' => $area_dashboard_middle_right->id,
					'widget_id' => $widget_dashboard->id,
				),
			);

			foreach ($instances as $instance) {
				$result = $this->widget_m->delete_instance_by_params($instance);
			}
		}
		else
		{
			$status = false;
		}

		// create area dashboard bottom left
		$area_dashboard_bottom_left = $this->widgets->get_area('dashboard-bottom-left');
		if ($area_dashboard_bottom_left != NULL AND $widget_dashboard != NULL)
		{
			// create instance
			$instances = array(
				array(
					'title' => 'Curve Chart',
					'widget_area_id' => $area_dashboard_bottom_left->id,
					'widget_id' => $widget_dashboard->id,
				),
			);

			foreach ($instances as $instance) {
				$this->widget_m->delete_instance_by_params($instance);
			}
		}
		else
		{
			$status = false;
		}

		// create area dashboard bottom right
		$area_dashboard_bottom_right = $this->widgets->get_area('dashboard-bottom-right');
		if ($area_dashboard_bottom_right != NULL AND $widget_dashboard != NULL)
		{
			// create instance
			$instances = array(
				array(
					'title' => 'Quick Post',
					'widget_area_id' => $area_dashboard_bottom_right->id,
					'widget_id' => $widget_dashboard->id,
				),
			);

			foreach ($instances as $instance) {
				$this->widget_m->delete_instance_by_params($instance);
			}
		}
		else
		{
			$status = false;
		}
		
		if ($this->db->trans_status() === FALSE OR $status == FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return TRUE;
		}
	}

	public function upgrade($old_version)
	{
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}