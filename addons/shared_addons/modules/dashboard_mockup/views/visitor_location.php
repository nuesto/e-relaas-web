<!-- start visitor location widget -->
<div class="widget-body">
  <div class="table-responsive clearfix">
		<table class="table table-hover no-margin">
			<thead>
				<tr>
					<th><span>Country</span></th>
					<th class="text-center"><span>Last Visit</span></th>
					<th class="text-center"><span>Status</span></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>USA</td>
					<td class="text-center">2013/08/08</td>
					<td class="text-center status green"><i class="fa fa-level-up"></i> 27%</td>
				</tr>
				<tr>
					<td>Russia</td>
					<td class="text-center">2013/08/08</td>
					<td class="text-center status red"><i class="fa fa-level-down"></i> 43%</td>
				</tr>
				<tr>
					<td>China</td>
					<td class="text-center">2013/08/08</td>
					<td class="text-center status green"><i class="fa fa-level-up"></i> 18%</td>
				</tr>
				<tr>
					<td>India</td>
					<td class="text-center">2013/08/08</td>
					<td class="text-center status green"><i class="fa fa-level-up"></i> 63%</td>
				</tr>
				<tr>
					<td>Australia</td>
					<td class="text-center">2013/08/08</td>
					<td class="text-center status red"><i class="fa fa-level-down"></i> 82%</td>
				</tr>
				<tr>
					<td>Canada</td>
					<td class="text-center">2013/08/08</td>
					<td class="text-center status red"><i class="fa fa-level-down"></i> 11%</td>
				</tr>
				<tr>
					<td>Argentina</td>
					<td class="text-center">2013/08/08</td>
					<td class="text-center status green"><i class="fa fa-level-up"></i> 74%</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<!-- end visitor location widget -->