<!-- start quick post widget -->
<div class="widget-body">
  <!-- Edit profile form (not working)-->
  <form class="padding form-horizontal">
	  <!-- Title -->
	  <div class="form-group">
		<label class="control-label col-lg-3" for="title">Title</label>
		<div class="col-lg-9">
		  <input type="text" class="form-control" id="title">
		</div>
	  </div>
	  <!-- Content -->
	  <div class="form-group">
		<label class="control-label col-lg-3" for="content">Content</label>
		<div class="col-lg-9">
		  <textarea class="form-control" id="content"></textarea>
		</div>
	  </div>
	  <!-- Cateogry -->
	  <div class="form-group">
		<label class="control-label col-lg-3">Category</label>
		<div class="col-lg-9">
			<select class="form-control">
			  <option value="">- Choose Cateogry -</option>
			  <option value="1">General</option>
			  <option value="2">News</option>
			  <option value="3">Media</option>
			  <option value="4">Funny</option>
			</select>
		</div>
	  </div>
	  <!-- Tags -->
	  <div class="form-group">
		<label class="control-label col-lg-3" for="tags">Tags</label>
		<div class="col-lg-9">
		  <input type="text" class="form-control" id="tags">
		</div>
	  </div>

	  <!-- Buttons -->
	  <div class="form-group">
		 <!-- Buttons -->
		 <div class="col-lg-offset-3 col-lg-9">
			<button type="submit" class="btn btn-success">Publish</button>
			<button type="submit" class="btn btn-danger">Save Draft</button>
			<button type="reset" class="btn btn-default">Reset</button>
		 </div>
	  </div>
  </form>
</div>
<!-- end quick post widget -->