<!-- start browser widget -->
<div class="widget-body no-padding">
  <table class="table table-bordered-inside no-margin">
  	<thead>
			<tr>
			  <th><center>#</center></th>
			  <th>Browsers</th>
			  <th>Visits</th>
			</tr>
  	</thead>
		<tbody>
			<tr>
			  <td class="text-center"><i class="browser-icon browser-icon-chrome"></i></td>
			  <td>Google Chrome</td>
			  <td>3,005</td>
			</tr>
			<tr>
			  <td class="text-center"><i class="browser-icon browser-icon-firefox"></i></td>
			  <td>Mozilla Firefox</td>
			  <td>2,505</td>
			</tr>
			<tr>
			  <td class="text-center"><i class="browser-icon browser-icon-ie"></i></td>
			  <td>Internet Explorer</td>
			  <td>1,405</td>
			</tr>
			<tr>
			  <td class="text-center"><i class="browser-icon browser-icon-opera"></i></td>
			  <td>Opera</td>
			  <td>4,005</td>
			</tr>
			<tr>
			  <td class="text-center"><i class="browser-icon browser-icon-safari"></i></td>
			  <td>Safari</td>
			  <td>505</td>
			</tr>
	  </tbody>
  </table>
</div>
<!-- end browser widget -->