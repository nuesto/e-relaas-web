<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mutasi model
 *
 * @author Aditya Satrya
 */
class Mutasi_m extends MY_Model {
	
	public function get_mutasi($pagination_config = NULL)
	{
		$this->db->select('*');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_dompet_mutasi');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_mutasi_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_dompet_mutasi');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_mutasi()
	{
		return $this->db->count_all('dompet_mutasi');
	}
	
	public function delete_mutasi_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_dompet_mutasi');
	}
	
	public function insert_mutasi($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_dompet_mutasi', $values);
	}
	
	public function update_mutasi($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_dompet_mutasi', $values); 
	}
	
	// Additional function
	public function get_mutasi_by_id_dompet($id, $filter = array())
	{
		$this->db->select('*');
		$this->db->where('id_dompet', $id);

		if(isset($filter['f-transaksi']) && $filter['f-transaksi'] != ""){
			$this->db->like('tipe', $filter['f-transaksi']);
		}

		$this->db->limit(25, 0);
		$query = $this->db->get('default_dompet_mutasi');
		$result = $query->result_array();
		
		return $result;
	}

}