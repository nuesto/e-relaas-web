<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Request model
 *
 * @author Aditya Satrya
 */
class Request_m extends MY_Model {
	
	public function get_request($pagination_config = NULL)
	{
		$this->db->select('*');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_dompet_request');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_request_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_dompet_request');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_request()
	{
		return $this->db->count_all('dompet_request');
	}
	
	public function delete_request_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_dompet_request');
	}
	
	public function insert_request($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_dompet_request', $values);
	}
	
	public function update_request($values, $row_id)
	{
		$values['last_updated_by'] = $this->current_user->id;
		$values['last_updated_on'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_dompet_request', $values); 
	}
	
}