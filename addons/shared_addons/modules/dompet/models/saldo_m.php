<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Saldo model
 *
 * @author Aditya Satrya
 */
class Saldo_m extends MY_Model {
	
	public function get_saldo($pagination_config = NULL)
	{
		$this->db->select('*,');
		$this->db->join("default_profiles p","default_dompet_saldo.id_owner = p.user_id");
		$this->db->join("default_users u","u.id = p.user_id");
		$this->db->join("default_groups g","g.id = u.group_id");
		
		if(isset($pagination_config['f-group']) && $pagination_config['f-group'] != ""){
			$this->db->like("g.name",$pagination_config['f-group']);
		}

		if(isset($pagination_config['f-id_owner']) && $pagination_config['f-id_owner'] != ""){
			$this->db->like("p.display_name",$pagination_config['f-id_owner']);
		}

		if(isset($pagination_config['f-saldo']) && $pagination_config['f-saldo'] != ""){
			$this->db->where("default_dompet_saldo.saldo",$pagination_config['f-saldo']);
		}

		if($pagination_config != ""){
			$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
			$this->db->limit($pagination_config['per_page'], $start);
		}
		
		$query = $this->db->get('default_dompet_saldo');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_saldo_dompet($id)
	{
		$this->db->select('*');
		$this->db->where('id_owner', $id);
		$query = $this->db->get('default_dompet_saldo');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_saldo_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_dompet_saldo');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_saldo()
	{
		return $this->db->count_all('dompet_saldo');
	}
	
	public function delete_saldo_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_dompet_saldo');
	}
	
	public function insert_saldo($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		$values['updated_by'] = $this->current_user->id;
		
		return $this->db->insert('default_dompet_saldo', $values);
	}
	
	public function update_saldo($values, $row_id)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		$values['updated_by'] = $this->current_user->id;
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_dompet_saldo', $values); 
	}
	
	public function get_my_request($id = "", $filter = array())
	{
		$this->db->select('*');
		if($id != ""){
			$this->db->where('id_dompet', $id);
		}else{
			if(isset($filter['f-status']) && $filter['f-status'] != ""){
				$this->db->where('status',$filter['f-status']);
			}

			if(isset($filter['f-transaksi']) && $filter['f-transaksi'] != ""){
				$this->db->where('tipe',$filter['f-transaksi']);
			}
		}

		$this->db->where("(tipe = 'topup' OR (tipe = 'penarikan' && is_otp_verified = 1))");
		
		$query = $this->db->get('default_dompet_request');
		$result = $query->result_array();
		return $result;
	}
	
	public function insert_request_tambah_saldo($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_dompet_request', $values);
	}
}