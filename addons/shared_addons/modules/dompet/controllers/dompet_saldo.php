<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dompet Module
 *
 * Dompet
 *
 */
class Dompet_saldo extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'saldo';
	
    public function __construct()
    {
    	date_default_timezone_set('Asia/Jakarta');
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('dompet');
		
		$this->load->model(array('saldo_m','mutasi_m','commerce/rekening_m','users/user_m','request_m','commerce/payment_m'));
    }

    /**
	 * List all saldo
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_saldo') AND ! group_has_role('dompet', 'view_own_saldo')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$filter = array();
		if($this->input->get()){
			$filter = $this->input->get();
		}
		$pagination_config['base_url'] = base_url(). 'dompet/saldo/index';
		$pagination_config['uri_segment'] = 4;

		$pagination_config['total_rows'] = $this->saldo_m->count_all_saldo();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
				
        $data['dompet']['entries'] = $this->saldo_m->get_saldo($pagination_config);
		$data['dompet']['total'] = count($data['dompet']['entries']);
		$data['dompet']['pagination'] = $this->pagination->create_links();
		$data['saldo'] = $this->saldo_m->get_saldo_dompet($this->current_user->id);
		$data['dompet']['mutasi'] = $this->mutasi_m->get_mutasi_by_id_dompet($data['saldo']['id'],$filter);
		$data['uri'] = get_query_string(5);
		$data['state'] = "index";

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('dompet:saldo:plural'))
        	->set_layout('member_area.html')
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('dompet:saldo:plural'))
			->build('my_dompet', $data);
    }

    public function get_saldo_ajax(){
    	if(is_logged_in()){
    		$dompet = $this->saldo_m->get_saldo_dompet($this->current_user->id);
    		echo json_encode(array("success",$dompet));
    	}else{
    		echo json_encode(array("error",""));
    	}
    }
	
	/**
     * Display one saldo
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_saldo') AND ! group_has_role('dompet', 'view_own_saldo')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['saldo'] = $this->saldo_m->get_saldo_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('dompet', 'view_all_saldo')){
			if($data['saldo']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('dompet:saldo:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('dompet', 'view_all_saldo') OR group_has_role('dompet', 'view_own_saldo')){
			$this->template->set_breadcrumb(lang('dompet:saldo:plural'), '/dompet/saldo/index');
		}

		$this->template->set_breadcrumb(lang('dompet:saldo:view'))
			->set_layout('member_area.html')
			->build('saldo_entry', $data);
    }
	
	/**
     * Create a new saldo entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'create_saldo')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_saldo('new')){	
				$this->session->set_flashdata('success', lang('dompet:saldo:submit_success'));				
				redirect('dompet/saldo/index');
			}else{
				$data['messages']['error'] = lang('dompet:saldo:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'dompet/saldo/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dompet:saldo:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('dompet', 'view_all_saldo') OR group_has_role('dompet', 'view_own_saldo')){
			$this->template->set_breadcrumb(lang('dompet:saldo:plural'), '/dompet/saldo/index');
		}

		$this->template->set_breadcrumb(lang('dompet:saldo:new'))
			->build('saldo_form', $data);
    }
	
	/**
     * Edit a saldo entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the saldo to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'edit_all_saldo') AND ! group_has_role('dompet', 'edit_own_saldo')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('dompet', 'edit_all_saldo')){
			$entry = $this->saldo_m->get_saldo_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_saldo('edit', $id)){	
				$this->session->set_flashdata('success', lang('dompet:saldo:submit_success'));				
				redirect('dompet/saldo/index');
			}else{
				$data['messages']['error'] = lang('dompet:saldo:submit_failure');
			}
		}
		
		$data['fields'] = $this->saldo_m->get_saldo_by_id($id);
		$data['mode'] = 'edit';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'dompet/saldo/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
       $this->template->title(lang('dompet:saldo:edit'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('dompet', 'view_all_saldo') OR group_has_role('dompet', 'view_own_saldo')){
			$this->template->set_breadcrumb(lang('dompet:saldo:plural'), '/dompet/saldo/index')
			->set_breadcrumb(lang('dompet:saldo:view'), '/dompet/saldo/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('dompet:saldo:plural'))
			->set_breadcrumb(lang('dompet:saldo:view'));
		}

		$this->template->set_breadcrumb(lang('dompet:saldo:edit'))
			->build('saldo_form', $data);
    }
	
	/**
     * Delete a saldo entry
     * 
     * @param   int [$id] The id of saldo to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'delete_all_saldo') AND ! group_has_role('dompet', 'delete_own_saldo')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('dompet', 'delete_all_saldo')){
			$entry = $this->saldo_m->get_saldo_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->saldo_m->delete_saldo_by_id($id);
		$this->session->set_flashdata('error', lang('dompet:saldo:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(5);
        redirect('dompet/saldo/index'.$data['uri']);
    }
	
	/**
     * Insert or update saldo entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_saldo($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('dompet:saldo:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->saldo_m->insert_saldo($values);
			}
			else
			{
				$result = $this->saldo_m->update_saldo($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	public function tambah_saldo(){
    	// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('payment', 'view_all_dompet') AND ! group_has_role('payment', 'view_own_dompet')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		

		if($_POST){
			if($this->_update_saldo_dompet('new')){	
				$this->session->set_flashdata('success', "Konfirmasi Penambahan Saldo berhasil dimasukkan");				
				redirect('dompet/saldo/tambah_saldo');
			}else{
				$data['messages']['error'] = lang('payment:dompet:submit_failure');
				// $this->session->set_flashdata('error', $data['messages']['error']);
			}
		}


        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'payment/dompet/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->saldo_m->count_all_saldo();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
		$data['rekening_saya'] = $this->rekening_m->get_rekening($pagination_config,$this->current_user->id);
		$data['rekening_admin'] = $this->rekening_m->get_rekening($pagination_config,1);
		$data['saldo'] = $this->saldo_m->get_saldo_dompet($this->current_user->id);
		$data['state'] = "tambah_saldo";
		$data['uri'] = get_query_string(5);

    	$this->template->title(lang('payment:dompet:plural'))
    		->set_layout('member_area.html')
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('payment:dompet:plural'))
			->build('tambah_saldo', $data);
    }

    public function tarik_saldo(){
    	// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('payment', 'view_all_dompet') AND ! group_has_role('payment', 'view_own_dompet')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		

		if($_POST){
			if($this->_update_saldo_dompet('tarik')){	
				$this->session->set_flashdata('success', "Request Penarikan Saldo berhasil dimasukkan");				
				redirect('dompet/saldo/tarik_saldo');
			}else{
				$data['messages']['error'] = lang('payment:dompet:submit_failure');
				// $this->session->set_flashdata('error', $data['messages']['error']);
			}
		}
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
		$data['rekening_saya'] = $this->rekening_m->get_rekening("",$this->current_user->id);
		
		$data['saldo'] = $this->saldo_m->get_saldo_dompet($this->current_user->id);
		$data['state'] = "tarik_saldo";
		$data['uri'] = get_query_string(5);

    	$this->template->title(lang('payment:dompet:plural'))
    		->set_layout('member_area.html')
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('payment:dompet:plural'))
			->build('tarik_saldo', $data);
    }


    public function request_saya(){
    	// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('payment', 'view_all_dompet') AND ! group_has_role('payment', 'view_own_dompet')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		

		if($_POST){
			if($this->_update_saldo_dompet('new')){	
				$this->session->set_flashdata('success', "Konfirmasi Penambahan Saldo berhasil dimasukkan");				
				redirect('payment/dompet/tambah_saldo');
			}else{
				$data['messages']['error'] = lang('payment:dompet:submit_failure');
			}
		}
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
		$data['saldo'] = $this->saldo_m->get_saldo_dompet($this->current_user->id);
		$data['request'] = $this->saldo_m->get_my_request($data['saldo']['id']);
		$data['uri'] = get_query_string(5);
		$data['state'] = "request_saya";
    	$this->template->title(lang('payment:dompet:plural'))
    		->set_layout('member_area.html')
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('payment:dompet:plural'))
			->build('request_saya', $data);
    }

    private function _update_saldo_dompet($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();
		$values['jumlah'] = str_replace(".", "", $values['jumlah']);
		$values['jumlah'] = str_replace(",", ".", $values['jumlah']);
		$_POST["jumlah"] = $values['jumlah'];
		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('jumlah', "Jumlah Transfer", 'required|numeric');
		
		if($method == "new"){
			$this->form_validation->set_rules('rekening_asal', "Rekengin Asal", 'required|numeric');
		}
		
		
		$this->form_validation->set_rules('rekening_tujuan', "Rekening Tujuan", 'required|numeric');
		$this->form_validation->set_rules('otp', "OTP", 'callback_is_otp_match');

		
		if($method == "new" && isset($_FILES['bukti_pembayaran']['name'])){
			$this->form_validation->set_rules('bukti_pembayaran', "Bukti Pembayaran", 'callback_cek_upload');
		}
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			$dompet_user = $this->saldo_m->get_saldo_dompet($this->current_user->id);
			if ($method == 'new')
			{
				$id_bukti = 0;
				if($_FILES['bukti_pembayaran']['name'] != ""){
					$data = Files::upload(1,$_FILES['bukti_pembayaran']['name'] , "bukti_pembayaran");
					$id_bukti = $data['data']['id'];
				}
				
				$new_transaksi_dompet = array("id_dompet"=>$dompet_user['id'],
					"tipe"=>"topup",
					"status"=>"pending",
					"jumlah"=>$values['jumlah'],
					"rekening_asal"=>$values['rekening_asal'],
					"rekening_tujuan"=>$values['rekening_tujuan'],
					);

				if($id_bukti != 0){
					$new_transaksi_dompet['id_request_bukti'] = $id_bukti;
				}
			}
			elseif ($method == 'tarik')
			{
				$new_transaksi_dompet = array("id_dompet"=>$dompet_user['id'],
					"tipe"=>"penarikan",
					"is_otp_verified"=>1,
					"otp_verified_on"=>date("Y-m-d H:i:s"),
					"otp_verified_by"=>$this->current_user->id,
					"status"=>"pending",
					"jumlah"=>$values['jumlah'],
					"rekening_asal" => $values['rekening_tujuan'],
					"rekening_tujuan" => $values['rekening_tujuan']
					);
			}
			$result = $this->saldo_m->insert_request_tambah_saldo($new_transaksi_dompet);
		}
		
		return $result;
	}

	public function is_otp_match(){
		$values = $this->input->post();
		if(isset($values['id'])){
			$request = $this->request_m->get_request_by_id($values['id']);
			if($request['otp'] == $values['otp']) {
	        	return true;
	        }else {
	        	$pesan='Kode OTP Anda tidak cocok, silahkan coba lagi';
	           	$this->form_validation->set_message('is_otp_match',  $pesan);
	            return false;
	        }
		}else{
			return true;
		}
	}

	public function cek_upload()
    {
    	$this->load->library('files/files');	
		$errors          = array();
		$maxsize         = 2097152;
		$validextensions = array("jpeg", "jpg","png");
		$temporary       = explode(".", $_FILES["bukti_pembayaran"]["name"]);
		$file_extension  = strtolower(end($temporary));
		$acceptable      = array('image/jpeg','image/jpg','image/gif','image/png');
        if ($_FILES["bukti_pembayaran"]["size"] == 0){
                $error[] = 'Anda belum mengupload';
        }else{
            if (!in_array($_FILES['bukti_pembayaran']['type'],$acceptable) || (!in_array($file_extension,$validextensions))){
            	$errors[] ='Tipe berkas ini tidak diperbolehkan'; 		
            }

            if(($_FILES['bukti_pembayaran']['size'] > $maxsize) || ($_FILES["bukti_pembayaran"]["size"] == 0)) {
            	$errors[] ='Ukuran file terlalu besar atau Anda belum upload, File maksimal 2 MB'; 
            } 
        }

        if(count($errors) === 0) {
        	return true;
        }else {
        	$pesan='';
        	foreach($errors as $error) {
        		$pesan .= $error.'<br>'; 
        	}
           	$this->form_validation->set_message('cek_upload',  $pesan);
            return false;
        }
    }

    public function generate_dompet_for_all_user(){
    	$all_dompet = $this->saldo_m->get_saldo();
    	$all_user = $this->user_m->get_all();
    	dump($all_dompet);

    	$all_dompet_user = array();
    	foreach ($all_dompet as $key => $value) {
    		array_push($all_dompet_user, $value['id_owner']);
    	}
    	
    	$all_user_id = array();
    	foreach ($all_user as $key => $value) {
    		// array_push($all_user_id, $value->id);
    		if(!in_array($value->id, $all_dompet_user)){
    			$values = array("id_owner" => $value->id,
    				"saldo" => 0,
    				);
    			$this->saldo_m->insert_saldo($values);
    		}
    	}
    }

    public function all_request(){
    	// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('payment', 'view_all_dompet')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('');
		}
		$filter = array();
		if($this->input->get()){
			$filter = $this->input->get();
		}		

		$data['saldo'] = $this->saldo_m->get_saldo_dompet($this->current_user->id);
		$data['all_rekening'] = $this->rekening_m->get_rekening("","");
		$data['rekening_id'] = array();
		foreach ($data['all_rekening'] as $key => $value) {
			array_push($data['rekening_id'], $value['id']);
		}

		$data['request'] = $this->saldo_m->get_my_request("",$filter);
		$data['uri'] = get_query_string(5);

    	$this->template->title(lang('payment:dompet:plural'))
    		->set_layout('member_area.html')
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('payment:dompet:plural'))
			->build('request', $data);
    }

    public function confirm_payment($id, $status){
    	// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('payment', 'view_all_dompet')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('');
		}
		$filter = array();
		if($this->input->get()){
			$filter = $this->input->get();
		}		

		$request = $this->request_m->get_request_by_id($id);
		$dompet = $this->saldo_m->get_saldo_by_id($request['id_dompet']);
		if($request['tipe'] == "topup"){
			if($status == 1){
				$new_saldo = array("saldo" => $dompet['saldo'] + $request['jumlah']);
				$this->saldo_m->update_saldo($new_saldo,$request['id_dompet']);
				$this->session->set_flashdata('success', "Transaksi tambah saldo telah berhasil diverifikasi");
				$new_inserted_mutasi = array("id_dompet" => $dompet['id'],
					'tanggal' => date("Y-m-d H:i:s"),
					"tipe" => "topup",
					"nominal_debet" => $request["jumlah"],
					"keterangan" => "Penambahan Saldo"
					);
				$this->mutasi_m->insert_mutasi($new_inserted_mutasi);
			}else{
				$this->saldo_m->update_saldo($new_saldo,$request['id_dompet']);
				$this->session->set_flashdata('error', "Transaksi tambah saldo ditolak");
			}
		}elseif($request['tipe'] == "penarikan"){
			if($status == 1){
				$new_inserted_mutasi = array("id_dompet" => $dompet['id'],
					'tanggal' => date("Y-m-d H:i:s"),
					"tipe" => "penarikan",
					"nominal_kredit" => $request["jumlah"],
					"keterangan" => "Penarikan Saldo"
					);
				$this->mutasi_m->insert_mutasi($new_inserted_mutasi);
				$new_saldo = array("saldo" => $dompet['saldo'] - $request['jumlah']);
				$this->saldo_m->update_saldo($new_saldo,$request['id_dompet']);
				$this->session->set_flashdata('success', "Transaksi tarik saldo telah berhasil diverifikasi");
			}
		}

		if($status == 1){
			$status = "diterima";
		}elseif($status == 2){
			$status = "ditolak";
		}

		$update_request = array("status" => $status);
		$this->request_m->update_request($update_request, $id);

		// $data['saldo'] = $this->dompet_m->get_saldo_dompet($this->current_user->id);
		// $data['all_rekening'] = $this->rekening_m->get_rekening("","");
		// $data['rekening_id'] = array();
		// exit();
		redirect('dompet/saldo/all_request');
    }

    public function send_otp(){
		require(FCPATH."system/cms/libraries/PHPMailer/PHPMailerAutoload.php");
    	$email_to = $this->current_user->email;

		$mail = new PHPMailer();

		$mail->IsSMTP();    // set mailer to use SMTP
		$mail->SMTPSecure = 'ssl';
		$mail->Host = $this->settings->get('mail_smtp_host');    // specify main and backup server
		$mail->SMTPAuth = true;    // turn on SMTP authentication
		$mail->Username = $this->settings->get('mail_smtp_user');    // SMTP username -- CHANGE --
		$mail->Password = $this->settings->get('mail_smtp_pass');    // SMTP password -- CHANGE --
		$mail->Port = $this->settings->get('mail_smtp_port');    // SMTP Port

		$mail->From = $this->settings->get('server_email');    //From Address -- CHANGE --
		$mail->FromName = "Rekadana";
		$mail->AddAddress($email_to, "Example");    //To Address -- CHANGE --
		$mail->AddReplyTo($this->settings->get('contact_email'), "Rekadana.com"); //Reply-To Address -- CHANGE --

		$mail->WordWrap = 50;    // set word wrap to 50 characters
		$mail->IsHTML(true);    // set email format to HTML
		
		$code = $this->generateRandomString(6);
		$time = new DateTime(date("Y-m-d H:i:s"));
		$time->add(new DateInterval('PT10M'));

		$stamp = $time->format('Y-m-d H:i:s');
		$request = $this->payment_m->get_payment_by_id($this->input->post("id"));
		$values = array("otp" => $code,
			"otp_created_on" => date("Y-m-d H:i:s"),
			"is_otp_verified" => 0,
			"resend_attempt" => intval($request['resend_attempt']) + 1,
			"otp_expired_on" => $stamp);
		$this->payment_m->update_payment_by_id_payment($values, $this->input->post("id"));
		

		$bd = "Anda akan menggunakan kode <b>".$code."</b> sebagai OTP untuk pembayaran menggunakan dompet. <br>Kode ini akan expired dalam <b>10menit</b>";
		$mail->Subject = "OTP Pembayaran Invoice";

		$mail->Body = $bd;
		if(!$mail->Send())
		{
			echo "Message could not be sent. <p>";
			echo "Mailer Error: " . $mail->ErrorInfo;
			echo "gagal";
		}else{
			echo "sukses";
		}
	}

	public function generateRandomString($length = 6) {
	    $characters = '0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return strtolower($randomString);
	}

	public function is_otp_match_ajax(){

		$values = $this->input->post();
		$payment = $this->payment_m->get_payment_by_id_payment($values['id']);
		if($payment['otp'] == $values['otp']) {
			if(date("Y-m-d H:i:s") > $payment['otp_expired_on']){
				echo "expired";
			}else{
				echo "match";
			}        
		}else {
			$values_ins = array("retry_attemp" => (intval($payment["retry_attemp"])) + 1);
			$result = $this->payment_m->update_payment($values_ins, $payment['id']);
        	echo "not match";
        }
	}

}