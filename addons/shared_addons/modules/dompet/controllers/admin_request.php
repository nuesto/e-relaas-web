<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dompet Module
 *
 * Dompet
 *
 */
class Admin_request extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'request';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'access_request_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('dompet');		
		$this->load->model('request_m');
    }

    /**
	 * List all request
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_request') AND ! group_has_role('dompet', 'view_own_request')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'admin/dompet/request/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->request_m->count_all_request();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['request']['entries'] = $this->request_m->get_request($pagination_config);
		$data['request']['total'] = $pagination_config['total_rows'];
		$data['request']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['request']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('admin/dompet/request/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/dompet/request/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('dompet:request:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('dompet:request:plural'))
			->build('admin/request_index', $data);
    }
	
	/**
     * Display one request
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_request') AND ! group_has_role('dompet', 'view_own_request')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['request'] = $this->request_m->get_request_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_request')){
			if($data['request']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('dompet:request:view'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('dompet', 'view_all_request') OR group_has_role('dompet', 'view_own_request')){
			$this->template->set_breadcrumb(lang('dompet:request:plural'), '/admin/dompet/request/index');
		}

		$this->template->set_breadcrumb(lang('dompet:request:view'))
			->build('admin/request_entry', $data);
    }
	
	/**
     * Create a new request entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'create_request')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_request('new')){	
				$this->session->set_flashdata('success', lang('dompet:request:submit_success'));				
				redirect('admin/dompet/request/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('dompet:request:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/dompet/request/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dompet:request:new'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('dompet', 'view_all_request') OR group_has_role('dompet', 'view_own_request')){
			$this->template->set_breadcrumb(lang('dompet:request:plural'), '/admin/dompet/request/index');
		}

		$this->template->set_breadcrumb(lang('dompet:request:new'))
			->build('admin/request_form', $data);
    }
	
	/**
     * Edit a request entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the request to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'edit_all_request') AND ! group_has_role('dompet', 'edit_own_request')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('dompet', 'edit_all_request')){
			$entry = $this->request_m->get_request_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_request('edit', $id)){	
				$this->session->set_flashdata('success', lang('dompet:request:submit_success'));				
				redirect('admin/dompet/request/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('dompet:request:submit_failure');
			}
		}
		
		$data['fields'] = $this->request_m->get_request_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/dompet/request/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dompet:request:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('dompet', 'view_all_request') OR group_has_role('dompet', 'view_own_request')){
			$this->template->set_breadcrumb(lang('dompet:request:plural'), '/admin/dompet/request/index')
			->set_breadcrumb(lang('dompet:request:view'), '/admin/dompet/request/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('dompet:request:plural'))
			->set_breadcrumb(lang('dompet:request:view'));
		}

		$this->template->set_breadcrumb(lang('dompet:request:edit'))
			->build('admin/request_form', $data);
    }
	
	/**
     * Delete a request entry
     * 
     * @param   int [$id] The id of request to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'delete_all_request') AND ! group_has_role('dompet', 'delete_own_request')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('dompet', 'delete_all_request')){
			$entry = $this->request_m->get_request_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->request_m->delete_request_by_id($id);
        $this->session->set_flashdata('error', lang('dompet:request:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('admin/dompet/request/index'.$data['uri']);
    }
	
	/**
     * Insert or update request entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_request($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('dompet:request:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->request_m->insert_request($values);
				
			}
			else
			{
				$result = $this->request_m->update_request($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}