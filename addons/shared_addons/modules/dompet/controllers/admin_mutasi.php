<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dompet Module
 *
 * Dompet
 *
 */
class Admin_mutasi extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'mutasi';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'access_mutasi_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('dompet');		
		$this->load->model('mutasi_m');
    }

    /**
	 * List all mutasi
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_mutasi') AND ! group_has_role('dompet', 'view_own_mutasi')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'admin/dompet/mutasi/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->mutasi_m->count_all_mutasi();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['mutasi']['entries'] = $this->mutasi_m->get_mutasi($pagination_config);
		$data['mutasi']['total'] = $pagination_config['total_rows'];
		$data['mutasi']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['mutasi']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('admin/dompet/mutasi/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/dompet/mutasi/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('dompet:mutasi:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('dompet:mutasi:plural'))
			->build('admin/mutasi_index', $data);
    }
	
	/**
     * Display one mutasi
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_mutasi') AND ! group_has_role('dompet', 'view_own_mutasi')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['mutasi'] = $this->mutasi_m->get_mutasi_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_mutasi')){
			if($data['mutasi']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('dompet:mutasi:view'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('dompet', 'view_all_mutasi') OR group_has_role('dompet', 'view_own_mutasi')){
			$this->template->set_breadcrumb(lang('dompet:mutasi:plural'), '/admin/dompet/mutasi/index');
		}

		$this->template->set_breadcrumb(lang('dompet:mutasi:view'))
			->build('admin/mutasi_entry', $data);
    }
	
	/**
     * Create a new mutasi entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'create_mutasi')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_mutasi('new')){	
				$this->session->set_flashdata('success', lang('dompet:mutasi:submit_success'));				
				redirect('admin/dompet/mutasi/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('dompet:mutasi:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/dompet/mutasi/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dompet:mutasi:new'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('dompet', 'view_all_mutasi') OR group_has_role('dompet', 'view_own_mutasi')){
			$this->template->set_breadcrumb(lang('dompet:mutasi:plural'), '/admin/dompet/mutasi/index');
		}

		$this->template->set_breadcrumb(lang('dompet:mutasi:new'))
			->build('admin/mutasi_form', $data);
    }
	
	/**
     * Edit a mutasi entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the mutasi to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'edit_all_mutasi') AND ! group_has_role('dompet', 'edit_own_mutasi')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('dompet', 'edit_all_mutasi')){
			$entry = $this->mutasi_m->get_mutasi_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_mutasi('edit', $id)){	
				$this->session->set_flashdata('success', lang('dompet:mutasi:submit_success'));				
				redirect('admin/dompet/mutasi/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('dompet:mutasi:submit_failure');
			}
		}
		
		$data['fields'] = $this->mutasi_m->get_mutasi_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/dompet/mutasi/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dompet:mutasi:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('dompet', 'view_all_mutasi') OR group_has_role('dompet', 'view_own_mutasi')){
			$this->template->set_breadcrumb(lang('dompet:mutasi:plural'), '/admin/dompet/mutasi/index')
			->set_breadcrumb(lang('dompet:mutasi:view'), '/admin/dompet/mutasi/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('dompet:mutasi:plural'))
			->set_breadcrumb(lang('dompet:mutasi:view'));
		}

		$this->template->set_breadcrumb(lang('dompet:mutasi:edit'))
			->build('admin/mutasi_form', $data);
    }
	
	/**
     * Delete a mutasi entry
     * 
     * @param   int [$id] The id of mutasi to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'delete_all_mutasi') AND ! group_has_role('dompet', 'delete_own_mutasi')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('dompet', 'delete_all_mutasi')){
			$entry = $this->mutasi_m->get_mutasi_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->mutasi_m->delete_mutasi_by_id($id);
        $this->session->set_flashdata('error', lang('dompet:mutasi:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('admin/dompet/mutasi/index'.$data['uri']);
    }
	
	/**
     * Insert or update mutasi entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_mutasi($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('dompet:mutasi:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->mutasi_m->insert_mutasi($values);
				
			}
			else
			{
				$result = $this->mutasi_m->update_mutasi($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}