<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dompet Module
 *
 * Dompet
 *
 */
class Dompet_request extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'request';
	
    public function __construct()
    {
    	date_default_timezone_set('Asia/Jakarta');
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('dompet');
		
		$this->load->model(array('request_m','saldo_m'));
    }

    /**
	 * List all request
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_request') AND ! group_has_role('dompet', 'view_own_request')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'dompet/request/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->request_m->count_all_request();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['request']['entries'] = $this->request_m->get_request($pagination_config);
		$data['request']['total'] = count($data['request']['entries']);
		$data['request']['pagination'] = $this->pagination->create_links();

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('dompet:request:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('dompet:request:plural'))
			->build('request_index', $data);
    }
	
	/**
     * Display one request
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_request') AND ! group_has_role('dompet', 'view_own_request')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['request'] = $this->request_m->get_request_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('dompet', 'view_all_request')){
			if($data['request']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		$data['uri'] = get_query_string(5);

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('dompet:request:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('dompet', 'view_all_request') OR group_has_role('dompet', 'view_own_request')){
			$this->template->set_breadcrumb(lang('dompet:request:plural'), '/dompet/request/index');
		}

		$this->template->set_breadcrumb(lang('dompet:request:view'))
			->build('request_entry', $data);
    }
	
	/**
     * Create a new request entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'create_request')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_request('new')){	
				$this->session->set_flashdata('success', lang('dompet:request:submit_success'));				
				redirect('dompet/request/index');
			}else{
				$data['messages']['error'] = lang('dompet:request:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'dompet/request/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dompet:request:view'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('dompet', 'view_all_request') OR group_has_role('dompet', 'view_own_request')){
			$this->template->set_breadcrumb(lang('dompet:request:plural'), '/dompet/request/index');
		}

		$this->template->set_breadcrumb(lang('dompet:request:new'))
			->build('request_form', $data);
    }

    public function create_ajax()
    {
		if(is_logged_in()){
			if($_POST){
				if($this->_update_request('new')){				
					echo json_encode(array("success",$this->db->insert_id()));
				}else{
					$data['messages']['error'] = lang('dompet:request:submit_failure');
					echo json_encode(array("error","failed to insert"));
				}
			}
		}else{
			echo json_encode(array("error","failed"));
		}
    }
	
	/**
     * Edit a request entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the request to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'edit_all_request') AND ! group_has_role('dompet', 'edit_own_request')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('dompet', 'edit_all_request')){
			$entry = $this->request_m->get_request_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_request('edit', $id)){	
				$this->session->set_flashdata('success', lang('dompet:request:submit_success'));				
				redirect('dompet/request/index');
			}else{
				$data['messages']['error'] = lang('dompet:request:submit_failure');
			}
		}
		
		$data['fields'] = $this->request_m->get_request_by_id($id);
		$data['mode'] = 'edit';
		$data['uri'] = get_query_string(5);
		$data['return'] = 'dompet/request/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
       $this->template->title(lang('dompet:request:edit'))
			->set_breadcrumb('Home', '/home');
			
		if(group_has_role('dompet', 'view_all_request') OR group_has_role('dompet', 'view_own_request')){
			$this->template->set_breadcrumb(lang('dompet:request:plural'), '/dompet/request/index')
			->set_breadcrumb(lang('dompet:request:view'), '/dompet/request/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('dompet:request:plural'))
			->set_breadcrumb(lang('dompet:request:view'));
		}

		$this->template->set_breadcrumb(lang('dompet:request:edit'))
			->build('request_form', $data);
    }
	
	/**
     * Delete a request entry
     * 
     * @param   int [$id] The id of request to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'delete_all_request') AND ! group_has_role('dompet', 'delete_own_request')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('dompet', 'delete_all_request')){
			$entry = $this->request_m->get_request_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->request_m->delete_request_by_id($id);
		$this->session->set_flashdata('error', lang('dompet:request:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(5);
        redirect('dompet/request/index'.$data['uri']);
    }
	
	/**
     * Insert or update request entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_request($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();
		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('jumlah', lang('dompet:jumlah'), 'required|numeric|greater_than[0]|max_length[20]');
		$this->form_validation->set_rules('rekening_tujuan', lang('dompet:rekening_tujuan'), 'required|numeric|greater_than[0]|max_length[5]');

		// if(isset($values['otp']) && $values['otp'] != ""){
		$this->form_validation->set_rules('otp', lang('dompet:otp'), 'callback_is_otp_match');
		// }
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;
		$dompet_user = $this->saldo_m->get_saldo_dompet($this->current_user->id);
		$values['id_dompet'] = $dompet_user['id'];
		$values['rekening_asal'] = $values['rekening_tujuan'];
		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->request_m->insert_request($values);
			}
			else
			{
				$result = $this->request_m->update_request($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	public function is_otp_match(){
		$values = $this->input->post();
		if(isset($values['id'])){
			$request = $this->request_m->get_request_by_id($values['id']);
			if($request['otp'] == $values['otp']) {
	        	return true;
	        }else {
	        	$pesan='Kode OTP Anda tidak cocok, silahkan coba lagi';
	           	$this->form_validation->set_message('cek_upload',  $pesan);
	            return false;
	        }
		}else{
			return true;
		}
	}


	public function is_otp_match_ajax(){
		$values = $this->input->post();
		$request = $this->request_m->get_request_by_id($values['id']);
		if($request['otp'] == $values['otp']) {
			if(date("Y-m-d H:i:s") > $request['otp_expired_on']){

				echo "expired";
			}else{
				echo "match";
			}        
		}else {
			$values_ins = array("retry_attemp" => (intval($request["retry_attemp"])) + 1);
			$result = $this->request_m->update_request($values_ins, $request['id']);
        	echo "not match";
        }
	}

	public function generateRandomString($length = 6) {
	    $characters = '0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return strtolower($randomString);
	}

	public function send_otp(){
		date_default_timezone_set('Asia/Jakarta');
		require(FCPATH."system/cms/libraries/PHPMailer/PHPMailerAutoload.php");
    	$email_to = $this->current_user->email;

		$mail = new PHPMailer();

		$mail->IsSMTP();    // set mailer to use SMTP
		$mail->SMTPSecure = 'ssl';
		$mail->Host = $this->settings->get('mail_smtp_host');    // specify main and backup server
		$mail->SMTPAuth = true;    // turn on SMTP authentication
		$mail->Username = $this->settings->get('mail_smtp_user');    // SMTP username -- CHANGE --
		$mail->Password = $this->settings->get('mail_smtp_pass');    // SMTP password -- CHANGE --
		$mail->Port = $this->settings->get('mail_smtp_port');    // SMTP Port

		$mail->From = $this->settings->get('server_email');    //From Address -- CHANGE --
		$mail->FromName = "Rekadana";
		$mail->AddAddress($email_to, "Example");    //To Address -- CHANGE --
		$mail->AddReplyTo($this->settings->get('contact_email'), "Rekadana.com"); //Reply-To Address -- CHANGE --

		$mail->WordWrap = 50;    // set word wrap to 50 characters
		$mail->IsHTML(true);    // set email format to HTML
		
		$code = $this->generateRandomString(6);
		$time = new DateTime(date("Y-m-d H:i:s"));
		$time->add(new DateInterval('PT15M'));

		$stamp = $time->format('Y-m-d H:i:s');
		$request = $this->request_m->get_request_by_id($this->input->post("id"));
		$values = array("otp" => $code,
			"otp_created_on" => date("Y-m-d H:i:s"),
			"is_otp_verified" => 0,
			"resend_attempt" => intval($request['resend_attempt']) + 1,
			"otp_expired_on" => $stamp);

		$this->request_m->update_request($values, $this->input->post("id"));
		

		$bd = "Anda akan menggunakan kode <b>".$code."</b> sebagai OTP untuk melanjutkan transaksi request penarikan saldo. <br>Kode ini akan expired dalam <b>10menit</b>";
		$mail->Subject = "OTP Penarikan Saldo";

		$mail->Body = $bd;
		if(!$mail->Send())
		{
			echo "Message could not be sent. <p>";
			echo "Mailer Error: " . $mail->ErrorInfo;
			echo "gagal";
		}else{
			echo "sukses";
		}
	}

	

}