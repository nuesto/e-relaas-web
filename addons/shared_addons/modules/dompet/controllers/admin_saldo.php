<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Dompet Module
 *
 * Dompet
 *
 */
class Admin_saldo extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'saldo';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'access_saldo_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('dompet');		
		$this->load->model('saldo_m');
    }

    /**
	 * List all saldo
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_saldo') AND ! group_has_role('dompet', 'view_own_saldo')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$pagination_config['base_url'] = base_url(). 'admin/dompet/saldo/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['reuse_query_string'] = true;
		$pagination_config['total_rows'] = $this->saldo_m->count_all_saldo();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		$data['uri'] = get_query_string(5);
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['saldo']['entries'] = $this->saldo_m->get_saldo($pagination_config);
		$data['saldo']['total'] = $pagination_config['total_rows'];
		$data['saldo']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------

		if(count($data['saldo']['entries'])==0 AND ($this->uri->segment(5) != NULL AND $this->uri->segment(5)%$pagination_config['per_page']==0)) {
			$data['uri'] = str_replace('/'.$this->uri->segment(5), '', $data['uri']);
			if($this->uri->segment(5) != '' AND $this->uri->segment(5)-$pagination_config['per_page']!=0) {
				$offset = $this->uri->segment(5)-$pagination_config['per_page'];
				redirect('admin/dompet/saldo/index/'.$offset.$data['uri']);
			} else {
				if($data['uri']!='') {
					redirect('admin/dompet/saldo/index'.$data['uri']);
				}
			}
		}
		
        $this->template->title(lang('dompet:saldo:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('dompet:saldo:plural'))
			->build('admin/saldo_index', $data);
    }
	
	/**
     * Display one saldo
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_saldo') AND ! group_has_role('dompet', 'view_own_saldo')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['saldo'] = $this->saldo_m->get_saldo_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'view_all_saldo')){
			if($data['saldo']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		$data['uri'] = get_query_string(6);
		
        $this->template->title(lang('dompet:saldo:view'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('dompet', 'view_all_saldo') OR group_has_role('dompet', 'view_own_saldo')){
			$this->template->set_breadcrumb(lang('dompet:saldo:plural'), '/admin/dompet/saldo/index');
		}

		$this->template->set_breadcrumb(lang('dompet:saldo:view'))
			->build('admin/saldo_entry', $data);
    }
	
	/**
     * Create a new saldo entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'create_saldo')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		$data['uri'] = get_query_string(5);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_saldo('new')){	
				$this->session->set_flashdata('success', lang('dompet:saldo:submit_success'));				
				redirect('admin/dompet/saldo/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('dompet:saldo:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/dompet/saldo/index'.$data['uri'];
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dompet:saldo:new'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('dompet', 'view_all_saldo') OR group_has_role('dompet', 'view_own_saldo')){
			$this->template->set_breadcrumb(lang('dompet:saldo:plural'), '/admin/dompet/saldo/index');
		}

		$this->template->set_breadcrumb(lang('dompet:saldo:new'))
			->build('admin/saldo_form', $data);
    }
	
	/**
     * Edit a saldo entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the saldo to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'edit_all_saldo') AND ! group_has_role('dompet', 'edit_own_saldo')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('dompet', 'edit_all_saldo')){
			$entry = $this->saldo_m->get_saldo_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$data['uri'] = get_query_string(6);
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_saldo('edit', $id)){	
				$this->session->set_flashdata('success', lang('dompet:saldo:submit_success'));				
				redirect('admin/dompet/saldo/index'.$data['uri']);
			}else{
				$data['messages']['error'] = lang('dompet:saldo:submit_failure');
			}
		}
		
		$data['fields'] = $this->saldo_m->get_saldo_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/dompet/saldo/view/'.$id.$data['uri'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('dompet:saldo:edit'))
			->set_breadcrumb('Home', '/admin');
			
		if(group_has_role('dompet', 'view_all_saldo') OR group_has_role('dompet', 'view_own_saldo')){
			$this->template->set_breadcrumb(lang('dompet:saldo:plural'), '/admin/dompet/saldo/index')
			->set_breadcrumb(lang('dompet:saldo:view'), '/admin/dompet/saldo/view/'.$id);
		}else{
			$this->template->set_breadcrumb(lang('dompet:saldo:plural'))
			->set_breadcrumb(lang('dompet:saldo:view'));
		}

		$this->template->set_breadcrumb(lang('dompet:saldo:edit'))
			->build('admin/saldo_form', $data);
    }
	
	/**
     * Delete a saldo entry
     * 
     * @param   int [$id] The id of saldo to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('dompet', 'delete_all_saldo') AND ! group_has_role('dompet', 'delete_own_saldo')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('dompet', 'delete_all_saldo')){
			$entry = $this->saldo_m->get_saldo_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->saldo_m->delete_saldo_by_id($id);
        $this->session->set_flashdata('error', lang('dompet:saldo:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		$data['uri'] = get_query_string(6);
        redirect('admin/dompet/saldo/index'.$data['uri']);
    }
	
	/**
     * Insert or update saldo entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_saldo($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('dompet:saldo:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->saldo_m->insert_saldo($values);
				
			}
			else
			{
				$result = $this->saldo_m->update_saldo($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}