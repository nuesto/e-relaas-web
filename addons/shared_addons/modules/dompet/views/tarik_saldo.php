<div class="">
	<style type="text/css">
		.hhh {
	        font-family: "Montserrat", sans-serif;
	        font-weight: 700;
	         margin: 1px !important; 
	        text-transform: uppercase;
	        display: inline;
	    }
	</style>
<div class="row">
  <div class="col-md-12" style="">
    <h2 class="h4">Saldo (<?php echo date('d M Y'); ?>) : <strong><?php echo number_format($saldo['saldo'],0,",","."); ?></strong></h2>
  </div>
</div>
<div class="row tabs-row shortcodeBlock m0" id="tabs">
    <!-- Nav tabs -->
    <div class="">
        <div class="col-md-12">
            <div class="tab-v1">
                <ul class="nav nav-tabs tab-disapear">
                  <li role="presentation" class="<?php echo ($state == 'index') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/index' ?>" aria-controls="tabs1" role="tab" >Mutasi</a></li>
                    <li role="presentation" class="<?php echo ($state == 'tambah_saldo') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/tambah_saldo' ?>" aria-controls="tabs1" role="tab">Tambah Saldo</a></li>
                    <li role="presentation" class="<?php echo ($state == 'tarik_saldo') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/tarik_saldo' ?>" aria-controls="tabs1" role="tab">Tarik Saldo</a></li>
                    <li role="presentation" class="<?php echo ($state == 'request_saya') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/request_saya' ?>" aria-controls="tabs1" role="tab">Request Saya</a></li>
              </ul>
            </div>
        </div>
        
    </div>
     <div class="col-xs-12">
         <div class="profile-edit">
        	<form action="" method="post" id="form_tarik_saldo" class="sky-form" enctype="multipart/form-data">
        		<h3 class="h4">Penarikan Saldo</h3>
                <hr class="nopad">
                <br>
        		{{ theme:partial name='notices' }}
            <div class="alert alert-danger client_validation" style="display:none;">
              <button data-dismiss="alert" class="close" type="button">
                <i class="icon-remove"></i>
              </button>
              <div><strong>Jumlah Transfer</strong> tidak boleh kosong dan berisi angka bulat positif.</div>
            </div>
            <div class="alert alert-danger code_not_match" style="display:none;">
              <button data-dismiss="alert" class="close" type="button">
                <i class="icon-remove"></i>
              </button>
              <div><strong>Kode OTP</strong> anda tidak cocok, silahkan coba lagi.</div>
            </div>
            <div class="alert alert-danger code_expired" style="display:none;">
              <button data-dismiss="alert" class="close" type="button">
                <i class="icon-remove"></i>
              </button>
              <div><strong>Kode OTP</strong> anda sudah expired. Silahkan request kode baru.</div>
            </div>
            <dl class="dl-horizontal">
              <dt>Jumlah * : </dt>
              <dd>
                  <section>
                      <input type="text" id="jumlah" name="jumlah" class="form-control moneymask" maxlength="15">
                  </section>
              </dd>
              <dt>Rekening Tujuan * : </dt>
              <dd>
                  <section>
                    <?php if(count($rekening_saya) > 0){ ?>
                      <select name="rekening_tujuan" id="rekening_tujuan" class="form-control">
                    <?php foreach ($rekening_saya as $rekening_entry): ?>
                        <option value="<?php echo $rekening_entry['id']; ?>"><?php echo $rekening_entry['nama_bank']." - ".$rekening_entry['no_rekening']; ?></option>
                    <?php endforeach; ?>
                    </select>
                    <?php }else{ ?>
                        Silahkan tambahkan rekening anda. <a href="<?php echo base_url().'payment/rekening/my_rekening'; ?>">Tambah Rekening</a>
                    <?php   } ?>
                  </section>
              </dd>
              <div class="otp" style="display:none;">
              <dt>OTP * : 
              <br>One Time Password
              </dt>
              <dd>
                  <section>
                    <div class="row">
                      <div class="col-md-7">
                        <input type="text" id="otp" name="otp" class="form-control moneymask" maxlength="6">
                        <input type="hidden" id="id" name="id" class="form-control moneymask" maxlength="6">
                        <div style="font-size:11px">Telah mengirim OTP <label id="count_send">0</label> kali.</div>
                        <div class="otp_send_success" style="display:none;font-size:11px">OTP telah terkirim ke <?php echo $this->current_user->email; ?></div>
                      </div>
                      <div class="col-md-5">
                         <input type="submit" name="otp_send" id="otp_send" class="btn btn-success btn-md" value="Kirim OTP">
                      </div>
                    </div>
                  </section>
              </dd>
              
              </div>
              <div class="row request_section">
                <div class="col-md-12 text-center">
                  <button name="request_button_submit" disabled="disabled" id="request_button_submit" type="submit" name="" class="btn btn-primary btn-md" style="display:none;">Request Penarikan</button>
                  <button name="request_button" id="request_button" type="submit" name="" class="btn btn-primary btn-md" >Request Penarikan</button>
                </div>
              </div>
              
      				
            </dl>
        	</form>
          </div>
    </div>
</div>
</div>

<script type="text/javascript">
  var counter = 0;
	function masking(){
		// VMasker(document.querySelector("#jumlah")).maskMoney({
  //         // Decimal precision -> "90"
  //         precision: 0,
  //         // Decimal separator -> ",90"
  //         separator: ',',
  //         // Number delimiter -> "12.345.678"
  //         delimiter: '.',
  //         // Money unit -> "R$ 12.345.678,90"
  //         unit: '',
  //         // Money unit -> "12.345.678,90 R$"
  //         suffixUnit: '',
  //         // Force type only number instead decimal,
  //         // masking decimals with ",00"
  //         // Zero cents -> "R$ 1.234.567.890,00"
  //         zeroCents: true
  //       });
	}
    var send_counter = 0;
  $(document).ready(function(){
    $("#otp").on("keyup",function(){
      if($(this).val().length != 6){
        $("#request_button_submit").attr("disabled","disabled");
      }else{
        $("#request_button_submit").attr("disabled",false);
      }
    });

		$("#jumlah").on("keyup",function(){
			masking();
		});
		
		masking();

		$("input[name='f-id_user']").val("<?php echo $this->input->get('f-id_user'); ?>");
		$("input[name='f-saldo']").val("<?php echo $this->input->get('f-saldo'); ?>");
		$("input[name='f-group']").val("<?php echo $this->input->get('f-group'); ?>");

    $("#request_button").on("click",function(){
      console.log(isInteger($("input[name='jumlah']").val()));
      if(!isInteger($("input[name='jumlah']").val()) || $("input[name='jumlah']").val() < 0 || $("input[name='jumlah']").val() == ""){
        $(".client_validation").show();
      }else{
        $("#request_button").html("Mengirimkan request ...");
        $("#request_button").attr("disabled",true);
        var form = $( this ),
          formData = new FormData();
          formData.append('jumlah', $("input[name='jumlah']").val());
          formData.append('tipe', "penarikan");
          formData.append('status', "pending");
          formData.append('rekening_tujuan', $('#rekening_tujuan').val());
          $.ajax({
            type: "POST",
            url: "<?php echo base_url().'dompet/request/create_ajax' ?>",
            data: formData,
            contentType: false,
            processData: false,
            success: function(result){
              var tmp = $.parseJSON(result);
              if(tmp[0] == "success"){
                $("input[name='jumlah']").attr('readonly','readonly');
                $("select[name='rekening_tujuan']").attr('readonly','readonly');
                $("#request_button").hide();
                $("#request_button_submit").show();
                $(".otp").fadeIn();
                $(".client_validation").hide();
                $("#id").val(tmp[1]);
              }else{
                console.log(result);
              }
            }
          });
      }
      return false;
    });

    $("#request_button_submit").on("click",function(){
      var form = $( this ),
      formData = new FormData();
      formData.append('otp', $("input[name='otp']").val());
      formData.append('id', $("input[name='id']").val());
      $.ajax({
        type: "POST",
        url: "<?php echo base_url().'dompet/request/is_otp_match_ajax' ?>",
        data: formData,
        contentType: false,
        processData: false,
        success: function(result){
          console.log(result);
          if(result == "match"){
            $("#form_tarik_saldo").submit();
            $(".code_not_match").hide();
            $(".code_expired").hide();
          }else if(result == "not match"){
            console.log(result);
            $(".code_not_match").show();
            $(".code_expired").hide();
          }else{
            $(".code_not_match").hide();
            $(".code_expired").show();
          }
        }
      });
      return false;
    });

    $("#otp_send").on("click",function(event){
      send_counter++;
      $("#count_send").html(send_counter);
      counter = 15;
      $("#otp").attr("disabled",true);
      $(this).attr("disabled",true);
      $(this).val("Mengirim OTP ...");

      event.preventDefault();
        var form = $( this ),
        formData = new FormData();
        formData.append('id', $("#id").val());
        $.ajax({
          type: "POST",
          url: "<?php echo base_url().'dompet/request/send_otp' ?>",
          data: formData,
          contentType: false,
          processData: false,
          success: function(result){
            if(result == "sukses"){
              callTimer();
              $("#otp").attr("disabled",false);
            }else{
              console.log(result);
            }
          }
        });
    });
	});

  function callTimer(){
    if(counter > 0){
      $("#otp_send").val("Kirim OTP ("+counter +"s)");
      counter--;
      $("#otp_send").attr("disabled",true);
      $(".otp_send_success").show();
      setTimeout(callTimer,1000);
    }else{
      $("#otp_send").val("Kirim OTP").attr("disabled",false);
      $(".otp_send_success").fadeOut();
    }
  }

  function isInteger(x) {
      return x % 1 === 0;
  }
</script>