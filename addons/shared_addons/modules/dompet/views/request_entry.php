<div class="page-header">
	<h1>
		<span><?php echo lang('dompet:request:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('dompet/request/index'.$uri); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('dompet:back') ?>
		</a>

		<?php if(group_has_role('dompet', 'edit_all_request')){ ?>
			<a href="<?php echo site_url('dompet/request/edit/'.$request['id'].$uri); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('dompet', 'edit_own_request')){ ?>
			<?php if($request->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('dompet/request/edit/'.$request['id'].$uri); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('dompet', 'delete_all_request')){ ?>
			<a href="<?php echo site_url('dompet/request/delete/'.$request['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('dompet', 'delete_own_request')){ ?>
			<?php if($request->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('dompet/request/delete/'.$request['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $request['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:id_dompet'); ?></div>
		<?php if(isset($request['id_dompet'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $request['id_dompet']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:tipe'); ?></div>
		<?php if(isset($request['tipe'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $request['tipe']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:status'); ?></div>
		<?php if(isset($request['status'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $request['status']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:jumlah'); ?></div>
		<?php if(isset($request['jumlah'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $request['jumlah']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:rekening_asal'); ?></div>
		<?php if(isset($request['rekening_asal'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $request['rekening_asal']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:rekening_tujuan'); ?></div>
		<?php if(isset($request['rekening_tujuan'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $request['rekening_tujuan']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:id_request_bukti'); ?></div>
		<?php if(isset($request['id_request_bukti'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $request['id_request_bukti']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:created_on'); ?></div>
		<?php if(isset($request['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $request['created_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:created_by'); ?></div>
		<?php if(isset($request['created_by'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $request['created_by']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:last_updated_on'); ?></div>
		<?php if(isset($request['last_updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $request['last_updated_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:last_updated_by'); ?></div>
		<?php if(isset($request['last_updated_by'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $request['last_updated_by']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:history'); ?></div>
		<?php if(isset($request['history'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $request['history']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:created'); ?></div>
		<?php if(isset($request['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($request['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:updated'); ?></div>
		<?php if(isset($request['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($request['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($request['created_by'], true); ?></div>
	</div>
</div>