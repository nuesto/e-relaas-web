<div class="">
	<style type="text/css">
		.hhh {
	        font-family: "Montserrat", sans-serif;
	        font-weight: 700;
	         margin: 1px !important; 
	        text-transform: uppercase;
	        display: inline;
	    }
        .label-danger {
            background-color: #d9534f;
        }
        .label-success {
            background-color: #5cb85c;
        }
        .label-warning {
            background-color: #f0ad4e;
        }	</style>
<div class="row">
  <div class="col-md-12" style="">
    <h2 class="h4">Saldo (<?php echo date('d M Y'); ?>) : <strong><?php echo number_format($saldo['saldo'],0,",","."); ?></strong></h2>
  </div>
</div>
<div class="row tabs-row shortcodeBlock m0" id="tabs">
    <!-- Nav tabs -->
    <div class="">
        <div class="col-md-12">
            <div class="tab-v1">
                <ul class="nav nav-tabs tab-disapear">
                    <li role="presentation" class="<?php echo ($state == 'index') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/index' ?>" aria-controls="tabs1" role="tab" >Mutasi</a></li>
                    <li role="presentation" class="<?php echo ($state == 'tambah_saldo') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/tambah_saldo' ?>" aria-controls="tabs1" role="tab">Tambah Saldo</a></li>
                    <li role="presentation" class="<?php echo ($state == 'tarik_saldo') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/tarik_saldo' ?>" aria-controls="tabs1" role="tab">Tarik Saldo</a></li>
                    <li role="presentation" class="<?php echo ($state == 'request_saya') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/request_saya' ?>" aria-controls="tabs1" role="tab">Request Saya</a></li>
                </ul>
            </div>
        </div>
        
    </div>
    <div class="col-xs-12">
        <p><?php if (count($request) > 0): ?>
        
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th style="text-align:center;">No</th>
                <th style="text-align:center;">Transaksi</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Jumlah</th>
                <th style="text-align:center;">Tanggal</th>
            </tr>
		<?php 
            $no = 1;
            foreach ($request as $rekening_entry): 
        ?>
            <tr>
                <td align="center"><?php echo $no; $no++; ?></td>
                <td><?php 
                    echo ucfirst($rekening_entry['tipe']);
                    //if($rekening_entry['transaksi'] == 1){
                    //     echo "Tambah Saldo";
                    // }else{
                    //     echo "Tarik Saldo";
                    //} ; ?></td>
                <td align="center"><?php if($rekening_entry['status'] == 0){ ?>
                        <label class="label label-warning label-md ">Verifikasi ...</label>
                    <?php }elseif($rekening_entry['status'] == 1){ ?>
                        <label class="label label-success label-md ">Transaksi Berhasil</label>
                    <?php }else{ ?>
                        <label class="label label-danger label-md ">Transaksi Ditolak</label>
                    <?php } ; ?></td>
                <td align="right"><?php echo number_format($rekening_entry['jumlah'],0,",","."); ?></td>
                <td align="center"><?php echo date('d F Y H:i:s', strtotime($rekening_entry['created_on'])); ?></td>
            </tr>
		<?php endforeach; ?>
        </table>
		<?php else: ?>
			<div class="well" style="margin-top: 30px;">Tidak Terdapat entry Request</div>
		<?php endif;?></p>
		</div>
    </div>
    <!-- <div class="tab-content hhh-tab-content">
			
	</div> -->
</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("input[name='f-id_user']").val("<?php echo $this->input->get('f-id_user'); ?>");
		$("input[name='f-saldo']").val("<?php echo $this->input->get('f-saldo'); ?>");
		$("input[name='f-group']").val("<?php echo $this->input->get('f-group'); ?>");
	});
</script>