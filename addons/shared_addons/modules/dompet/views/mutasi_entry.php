<div class="page-header">
	<h1>
		<span><?php echo lang('dompet:mutasi:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('dompet/mutasi/index'.$uri); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('dompet:back') ?>
		</a>

		<?php if(group_has_role('dompet', 'edit_all_mutasi')){ ?>
			<a href="<?php echo site_url('dompet/mutasi/edit/'.$mutasi['id'].$uri); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('dompet', 'edit_own_mutasi')){ ?>
			<?php if($mutasi->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('dompet/mutasi/edit/'.$mutasi['id'].$uri); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('dompet', 'delete_all_mutasi')){ ?>
			<a href="<?php echo site_url('dompet/mutasi/delete/'.$mutasi['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('dompet', 'delete_own_mutasi')){ ?>
			<?php if($mutasi->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('dompet/mutasi/delete/'.$mutasi['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $mutasi['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:id_dompet'); ?></div>
		<?php if(isset($mutasi['id_dompet'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $mutasi['id_dompet']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:tanggal'); ?></div>
		<?php if(isset($mutasi['tanggal'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $mutasi['tanggal']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:tipe'); ?></div>
		<?php if(isset($mutasi['tipe'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $mutasi['tipe']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:tanggal'); ?></div>
		<?php if(isset($mutasi['tanggal'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $mutasi['tanggal']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:nominal_debet'); ?></div>
		<?php if(isset($mutasi['nominal_debet'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $mutasi['nominal_debet']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:nominal_kredit'); ?></div>
		<?php if(isset($mutasi['nominal_kredit'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $mutasi['nominal_kredit']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:created_on'); ?></div>
		<?php if(isset($mutasi['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $mutasi['created_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:created_by'); ?></div>
		<?php if(isset($mutasi['created_by'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $mutasi['created_by']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:keterangan'); ?></div>
		<?php if(isset($mutasi['keterangan'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $mutasi['keterangan']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:history'); ?></div>
		<?php if(isset($mutasi['history'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $mutasi['history']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:created'); ?></div>
		<?php if(isset($mutasi['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($mutasi['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:updated'); ?></div>
		<?php if(isset($mutasi['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($mutasi['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($mutasi['created_by'], true); ?></div>
	</div>
</div>