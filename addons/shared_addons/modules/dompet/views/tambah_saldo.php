<div>
    <style type="text/css">
		.hhh {
	        font-family: "Montserrat", sans-serif;
	        font-weight: 700;
	         margin: 1px !important; 
	        text-transform: uppercase;
	        display: inline;
	    }
	</style>
<div class="row">
  <div class="col-md-12" style="">
    <h2 class="h4">Saldo (<?php echo date('d M Y'); ?>) : <strong><?php echo number_format($saldo['saldo'],0,",","."); ?></strong></h2>
  </div>
</div>
<div class="row tabs-row shortcodeBlock m0" id="tabs">
    <!-- Nav tabs -->
    <div class="">
        <div class="col-md-12">
            <div class="tab-v1">
                <ul class="nav nav-tabs tab-disapear">
                    <li role="presentation" class="<?php echo ($state == 'index') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/index' ?>" aria-controls="tabs1" role="tab" >Mutasi</a></li>
                    <li role="presentation" class="<?php echo ($state == 'tambah_saldo') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/tambah_saldo' ?>" aria-controls="tabs1" role="tab">Tambah Saldo</a></li>
                    <li role="presentation" class="<?php echo ($state == 'tarik_saldo') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/tarik_saldo' ?>" aria-controls="tabs1" role="tab">Tarik Saldo</a></li>
                    <li role="presentation" class="<?php echo ($state == 'request_saya') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/request_saya' ?>" aria-controls="tabs1" role="tab">Request Saya</a></li>
                </ul>
            </div>
        </div>
    </div>
        <div class="col-xs-12">
            <div class="profile-edit">
            <form action="" method="post" class="sky-form" enctype="multipart/form-data">
                <h3 class="h4">Konfirmasi Transfer</h3>
                <hr class="nopad">
                <br>
                <?php if (validation_errors()): ?>
                    <div class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button">
                            <i class="fa fa-remove"></i>
                        </button>
                        <?php echo validation_errors(); ?>
                    </div>
                <?php endif;?>
                <?php if ($this->session->flashdata('success')): ?>
                    <div class="alert alert-success">
                        <button data-dismiss="alert" class="close" type="button">
                            <i class="fa fa-remove"></i>
                        </button>
                        <h2 class="hhh h4"><?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif;?>
                <dl class="dl-horizontal">
                    <dt>Jumlah * : </dt>
                    <dd>
                        <section>
                            <input type="text" id="jumlah" name="jumlah" class="form-control moneymask" maxlength="15">
                        </section>
                    </dd>
                    <dt>Rekening Asal * : </dt>
                    <dd>
                        <section>
                            <?php if(count($rekening_saya) > 0){ ?>
                            <select name="rekening_asal" class="form-control">
                            <?php foreach ($rekening_saya as $rekening_entry): ?>
                                <option value="<?php echo $rekening_entry['id']; ?>"><?php echo $rekening_entry['nama_bank']." - ".$rekening_entry['no_rekening']; ?></option>
                            <?php endforeach; ?>
                            </select>
                            <?php }else{ ?>
                                Silahkan tambahkan rekening anda. <a href="<?php echo base_url().'payment/rekening/my_rekening'; ?>">Tambah Rekening</a>
                            <?php   } ?>
                        </section>
                    </dd>
                    <dt>Rekening Tujuan * : </dt>
                    <dd>
                        <section>
                            <select name="rekening_tujuan" class="form-control">
                            <?php foreach ($rekening_admin as $rekening_entry): ?>
                                <option value="<?php echo $rekening_entry['id']; ?>"><?php echo $rekening_entry['nama_bank']." - ".$rekening_entry['no_rekening']; ?></option>
                            <?php endforeach; ?>
                            </select>
                        </section>
                    </dd>
                    <dt>Bukti Pembayaran : </dt>
                    <dd>
                        <section>
                            <input type="file" name="bukti_pembayaran" class="form-control">
                        </section>
                    </dd>
                     <input type="submit" name="" class="btn btn-primary btn-md" value="Submit">
                </dl>
                   
            </form>
            </div>
            <p><?php if (count($rekening_admin) > 0): ?>
            <hr>
            <h3 class="h4">Rekening Rekadana </h3>
            <h4 class="h5">(*) Transfer ke rekening dibawah ini untuk tambah saldo dompet</h4>
            <div class="row" style="">
            <?php foreach ($rekening_admin as $rekening_entry): ?>
            <div class="col-md-3" style="background-color:background-color: rgba(128, 128, 128, 0.09);margin-right: 10px;">
                <h2 class="h4" style="background-color: #ececf9;padding: 5px;"><?php echo $rekening_entry['nama_bank']; ?><br>
                <?php echo $rekening_entry['no_rekening']; ?><br>
                <?php echo $rekening_entry['atas_nama']; ?></h2>
            </div>
            <?php endforeach; ?>
            <?php else: ?>
                <div class="well" style="margin-top: 30px;"><?php echo lang('payment:dompet:no_entry'); ?></div>
            <?php endif;?></p>
            </div>
        </div>
        	
    <!-- <div class="tab-content hhh-tab-content">
			
	</div> -->
</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
        VMasker(document.querySelector("#jumlah")).maskMoney({
          // Decimal precision -> "90"
          precision: 0,
          // Decimal separator -> ",90"
          separator: ',',
          // Number delimiter -> "12.345.678"
          delimiter: '.',
          // Money unit -> "R$ 12.345.678,90"
          unit: '',
          // Money unit -> "12.345.678,90 R$"
          suffixUnit: '',
          // Force type only number instead decimal,
          // masking decimals with ",00"
          // Zero cents -> "R$ 1.234.567.890,00"
          zeroCents: true
        });

		$("input[name='f-id_user']").val("<?php echo $this->input->get('f-id_user'); ?>");
		$("input[name='f-saldo']").val("<?php echo $this->input->get('f-saldo'); ?>");
		$("input[name='f-group']").val("<?php echo $this->input->get('f-group'); ?>");
	});
</script>