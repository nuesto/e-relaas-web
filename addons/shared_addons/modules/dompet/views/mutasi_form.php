<div class="page-header">
	<h1>
		<span><?php echo lang('dompet:mutasi:'.$mode); ?></span>
	</h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_dompet"><?php echo lang('dompet:id_dompet'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_dompet') != NULL){
					$value = $this->input->post('id_dompet');
				}elseif($mode == 'edit'){
					$value = $fields['id_dompet'];
				}
			?>
			<input name="id_dompet" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tanggal"><?php echo lang('dompet:tanggal'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('tanggal') != NULL){
					$value = $this->input->post('tanggal');
				}elseif($mode == 'edit'){
					$value = $fields['tanggal'];
				}
			?>
			<input name="tanggal" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tipe"><?php echo lang('dompet:tipe'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('tipe') != NULL){
					$value = $this->input->post('tipe');
				}elseif($mode == 'edit'){
					$value = $fields['tipe'];
				}
			?>
			<input name="tipe" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tanggal"><?php echo lang('dompet:tanggal'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('tanggal') != NULL){
					$value = $this->input->post('tanggal');
				}elseif($mode == 'edit'){
					$value = $fields['tanggal'];
				}
			?>
			<input name="tanggal" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nominal_debet"><?php echo lang('dompet:nominal_debet'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nominal_debet') != NULL){
					$value = $this->input->post('nominal_debet');
				}elseif($mode == 'edit'){
					$value = $fields['nominal_debet'];
				}
			?>
			<input name="nominal_debet" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nominal_kredit"><?php echo lang('dompet:nominal_kredit'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nominal_kredit') != NULL){
					$value = $this->input->post('nominal_kredit');
				}elseif($mode == 'edit'){
					$value = $fields['nominal_kredit'];
				}
			?>
			<input name="nominal_kredit" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="created_on"><?php echo lang('dompet:created_on'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('created_on') != NULL){
					$value = $this->input->post('created_on');
				}elseif($mode == 'edit'){
					$value = $fields['created_on'];
				}
			?>
			<input name="created_on" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="created_by"><?php echo lang('dompet:created_by'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('created_by') != NULL){
					$value = $this->input->post('created_by');
				}elseif($mode == 'edit'){
					$value = $fields['created_by'];
				}
			?>
			<input name="created_by" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="keterangan"><?php echo lang('dompet:keterangan'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('keterangan') != NULL){
					$value = $this->input->post('keterangan');
				}elseif($mode == 'edit'){
					$value = $fields['keterangan'];
				}
			?>
			<input name="keterangan" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="history"><?php echo lang('dompet:history'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('history') != NULL){
					$value = $this->input->post('history');
				}elseif($mode == 'edit'){
					$value = $fields['history'];
				}
			?>
			<input name="history" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>