<div class="page-header">	<h1>		<span><?php echo lang('dompet:mutasi:plural'); ?></span>	</h1>		<?php if(group_has_role('dompet', 'create_mutasi')){ ?>	<div class="btn-group content-toolbar">		<a class="btn btn-default btn-sm" href="<?php echo site_url('dompet/mutasi/create'.$uri); ?>">			<i class="icon-plus"></i>			<span class="no-text-shadow"><?php echo lang('dompet:mutasi:new'); ?></span>		</a>	</div>	<?php } ?></div><fieldset>	<legend><?php echo lang('global:filters') ?></legend>		<?php echo form_open('', array('class' => 'form-inline', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>		<div class="form-group">
			<label><?php echo lang('dompet:id_dompet'); ?>:&nbsp;</label>
			<input type="text" name="f-id_dompet" value="<?php echo $this->input->get('f-id_dompet'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('dompet:tanggal'); ?>:&nbsp;</label>
			<input type="text" name="f-tanggal" value="<?php echo $this->input->get('f-tanggal'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('dompet:tipe'); ?>:&nbsp;</label>
			<input type="text" name="f-tipe" value="<?php echo $this->input->get('f-tipe'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('dompet:tanggal'); ?>:&nbsp;</label>
			<input type="text" name="f-tanggal" value="<?php echo $this->input->get('f-tanggal'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('dompet:nominal_debet'); ?>:&nbsp;</label>
			<input type="text" name="f-nominal_debet" value="<?php echo $this->input->get('f-nominal_debet'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('dompet:nominal_kredit'); ?>:&nbsp;</label>
			<input type="text" name="f-nominal_kredit" value="<?php echo $this->input->get('f-nominal_kredit'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('dompet:created_on'); ?>:&nbsp;</label>
			<input type="text" name="f-created_on" value="<?php echo $this->input->get('f-created_on'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('dompet:created_by'); ?>:&nbsp;</label>
			<input type="text" name="f-created_by" value="<?php echo $this->input->get('f-created_by'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('dompet:keterangan'); ?>:&nbsp;</label>
			<input type="text" name="f-keterangan" value="<?php echo $this->input->get('f-keterangan'); ?>">
		</div>
		<div class="form-group">
			<label><?php echo lang('dompet:history'); ?>:&nbsp;</label>
			<input type="text" name="f-history" value="<?php echo $this->input->get('f-history'); ?>">
		</div>
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">			<i class="icon-ok"></i>			<?php echo lang('buttons:submit'); ?>		</button>				<button href="<?php echo current_url() . '#'; ?>" class="btn btn-danger btn-xs" type="reset">			<i class="icon-remove"></i>			<?php echo lang('buttons:clear'); ?>		</button>	<?php echo form_close() ?></fieldset><hr /><?php if ($mutasi['total'] > 0): ?>		<p class="pull-right"><?php echo lang('dompet:showing').' '.count($mutasi['entries']).' '.lang('dompet:of').' '.$mutasi['total'] ?></p>		<table class="table table-striped table-bordered table-hover">		<thead>			<tr>				<th>No</th>				<th><?php echo lang('dompet:id_dompet'); ?></th>				<th><?php echo lang('dompet:tanggal'); ?></th>				<th><?php echo lang('dompet:tipe'); ?></th>				<th><?php echo lang('dompet:tanggal'); ?></th>				<th><?php echo lang('dompet:nominal_debet'); ?></th>				<th><?php echo lang('dompet:nominal_kredit'); ?></th>				<th><?php echo lang('dompet:created_on'); ?></th>				<th><?php echo lang('dompet:created_by'); ?></th>				<th><?php echo lang('dompet:keterangan'); ?></th>				<th><?php echo lang('dompet:history'); ?></th>				<th><?php echo lang('dompet:created'); ?></th>				<th><?php echo lang('dompet:updated'); ?></th>				<th><?php echo lang('dompet:created_by'); ?></th>				<th></th>			</tr>		</thead>		<tbody>			<?php 			$cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);			if($cur_page != 0){				$item_per_page = $pagination_config['per_page'];				$no = (($cur_page -1) * $item_per_page) + 1;			}else{				$no = 1;			}			?>						<?php foreach ($mutasi['entries'] as $mutasi_entry): ?>			<tr>				<td><?php echo $no; $no++; ?></td>				<td><?php echo $mutasi_entry['id_dompet']; ?></td>				<td><?php echo $mutasi_entry['tanggal']; ?></td>				<td><?php echo $mutasi_entry['tipe']; ?></td>				<td><?php echo $mutasi_entry['tanggal']; ?></td>				<td><?php echo $mutasi_entry['nominal_debet']; ?></td>				<td><?php echo $mutasi_entry['nominal_kredit']; ?></td>				<td><?php echo $mutasi_entry['created_on']; ?></td>				<td><?php echo $mutasi_entry['created_by']; ?></td>				<td><?php echo $mutasi_entry['keterangan']; ?></td>				<td><?php echo $mutasi_entry['history']; ?></td>							<?php if($mutasi_entry['created_on']){ ?>				<td><?php echo format_date($mutasi_entry['created_on'], 'd-m-Y G:i'); ?></td>				<?php }else{ ?>				<td>-</td>				<?php } ?>								<?php if($mutasi_entry['updated_on']){ ?>				<td><?php echo format_date($mutasi_entry['updated_on'], 'd-m-Y G:i'); ?></td>				<?php }else{ ?>				<td>-</td>				<?php } ?>								<td><?php echo user_displayname($mutasi_entry['created_by'], true); ?></td>				<td class="actions">				<?php 				if(group_has_role('dompet', 'view_all_mutasi')){					echo anchor('dompet/mutasi/view/' . $mutasi_entry['id'].$uri, lang('global:view'), 'class="btn btn-xs btn-info view"');				}elseif(group_has_role('dompet', 'view_own_mutasi')){					if($mutasi_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('dompet/mutasi/view/' . $mutasi_entry['id'].$uri, lang('global:view'), 'class="btn btn-xs btn-info view"');					}				}				?>				<?php 				if(group_has_role('dompet', 'edit_all_mutasi')){					echo anchor('dompet/mutasi/edit/' . $mutasi_entry['id'].$uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');				}elseif(group_has_role('dompet', 'edit_own_mutasi')){					if($mutasi_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('dompet/mutasi/edit/' . $mutasi_entry['id'].$uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');					}				}				?>				<?php 				if(group_has_role('dompet', 'delete_all_mutasi')){					echo anchor('dompet/mutasi/delete/' . $mutasi_entry['id'].$uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));				}elseif(group_has_role('dompet', 'delete_own_mutasi')){					if($mutasi_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('dompet/mutasi/delete/' . $mutasi_entry['id'].$uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));					}				}				?>				</td>			</tr>			<?php endforeach; ?>		</tbody>	</table>		<?php echo $mutasi['pagination']; ?>	<?php else: ?>	<div class="well"><?php echo lang('dompet:mutasi:no_entry'); ?></div><?php endif;?>