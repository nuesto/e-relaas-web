<div class="page-header">
	<h1>
		<span><?php echo lang('dompet:saldo:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('dompet/saldo/index'.$uri); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('dompet:back') ?>
		</a>

		<?php if(group_has_role('dompet', 'edit_all_saldo')){ ?>
			<a href="<?php echo site_url('dompet/saldo/edit/'.$saldo['id'].$uri); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('dompet', 'edit_own_saldo')){ ?>
			<?php if($saldo->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('dompet/saldo/edit/'.$saldo['id'].$uri); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('dompet', 'delete_all_saldo')){ ?>
			<a href="<?php echo site_url('dompet/saldo/delete/'.$saldo['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('dompet', 'delete_own_saldo')){ ?>
			<?php if($saldo->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('dompet/saldo/delete/'.$saldo['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $saldo['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:saldo'); ?></div>
		<?php if(isset($saldo['saldo'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $saldo['saldo']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:id_owner'); ?></div>
		<?php if(isset($saldo['id_owner'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $saldo['id_owner']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:created_on'); ?></div>
		<?php if(isset($saldo['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $saldo['created_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:created_by'); ?></div>
		<?php if(isset($saldo['created_by'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $saldo['created_by']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:updated_on'); ?></div>
		<?php if(isset($saldo['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $saldo['updated_on']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:updated_by'); ?></div>
		<?php if(isset($saldo['updated_by'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $saldo['updated_by']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:ordering_count'); ?></div>
		<?php if(isset($saldo['ordering_count'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $saldo['ordering_count']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:created'); ?></div>
		<?php if(isset($saldo['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($saldo['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:updated'); ?></div>
		<?php if(isset($saldo['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($saldo['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('dompet:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($saldo['created_by'], true); ?></div>
	</div>
</div>