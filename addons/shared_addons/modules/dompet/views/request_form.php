<div class="page-header">
	<h1>
		<span><?php echo lang('dompet:request:'.$mode); ?></span>
	</h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_dompet"><?php echo lang('dompet:id_dompet'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_dompet') != NULL){
					$value = $this->input->post('id_dompet');
				}elseif($mode == 'edit'){
					$value = $fields['id_dompet'];
				}
			?>
			<input name="id_dompet" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tipe"><?php echo lang('dompet:tipe'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('tipe') != NULL){
					$value = $this->input->post('tipe');
				}elseif($mode == 'edit'){
					$value = $fields['tipe'];
				}
			?>
			<input name="tipe" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="status"><?php echo lang('dompet:status'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('status') != NULL){
					$value = $this->input->post('status');
				}elseif($mode == 'edit'){
					$value = $fields['status'];
				}
			?>
			<input name="status" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="jumlah"><?php echo lang('dompet:jumlah'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('jumlah') != NULL){
					$value = $this->input->post('jumlah');
				}elseif($mode == 'edit'){
					$value = $fields['jumlah'];
				}
			?>
			<input name="jumlah" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="rekening_asal"><?php echo lang('dompet:rekening_asal'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('rekening_asal') != NULL){
					$value = $this->input->post('rekening_asal');
				}elseif($mode == 'edit'){
					$value = $fields['rekening_asal'];
				}
			?>
			<input name="rekening_asal" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="rekening_tujuan"><?php echo lang('dompet:rekening_tujuan'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('rekening_tujuan') != NULL){
					$value = $this->input->post('rekening_tujuan');
				}elseif($mode == 'edit'){
					$value = $fields['rekening_tujuan'];
				}
			?>
			<input name="rekening_tujuan" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_request_bukti"><?php echo lang('dompet:id_request_bukti'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_request_bukti') != NULL){
					$value = $this->input->post('id_request_bukti');
				}elseif($mode == 'edit'){
					$value = $fields['id_request_bukti'];
				}
			?>
			<input name="id_request_bukti" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="created_on"><?php echo lang('dompet:created_on'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('created_on') != NULL){
					$value = $this->input->post('created_on');
				}elseif($mode == 'edit'){
					$value = $fields['created_on'];
				}
			?>
			<input name="created_on" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="created_by"><?php echo lang('dompet:created_by'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('created_by') != NULL){
					$value = $this->input->post('created_by');
				}elseif($mode == 'edit'){
					$value = $fields['created_by'];
				}
			?>
			<input name="created_by" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="last_updated_on"><?php echo lang('dompet:last_updated_on'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('last_updated_on') != NULL){
					$value = $this->input->post('last_updated_on');
				}elseif($mode == 'edit'){
					$value = $fields['last_updated_on'];
				}
			?>
			<input name="last_updated_on" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="last_updated_by"><?php echo lang('dompet:last_updated_by'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('last_updated_by') != NULL){
					$value = $this->input->post('last_updated_by');
				}elseif($mode == 'edit'){
					$value = $fields['last_updated_by'];
				}
			?>
			<input name="last_updated_by" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="history"><?php echo lang('dompet:history'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('history') != NULL){
					$value = $this->input->post('history');
				}elseif($mode == 'edit'){
					$value = $fields['history'];
				}
			?>
			<input name="history" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>