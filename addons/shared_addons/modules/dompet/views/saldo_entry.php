<div class="page-header">
	<h1>
		<span><?php echo lang('payment:dompet:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('dompet/saldo/index'.$uri); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('payment:back') ?>
		</a>

		<?php if(group_has_role('payment', 'edit_all_dompet')){ ?>
			<a href="<?php echo site_url('dompet/saldo/edit/'.$dompet['id'].$uri); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('payment', 'edit_own_dompet')){ ?>
			<?php if($dompet->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('dompet/saldo/edit/'.$dompet['id'].$uri); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('payment', 'delete_all_dompet')){ ?>
			<a href="<?php echo site_url('dompet/saldo/delete/'.$dompet['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('payment', 'delete_own_dompet')){ ?>
			<?php if($dompet->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('dompet/saldo/delete/'.$dompet['id'].$uri); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $dompet['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('payment:saldo'); ?></div>
		<?php if(isset($dompet['saldo'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $dompet['saldo']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('payment:id_user'); ?></div>
		<?php if(isset($dompet['id_user'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $dompet['id_user']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('payment:created'); ?></div>
		<?php if(isset($dompet['created_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($dompet['created_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('payment:updated'); ?></div>
		<?php if(isset($dompet['updated_on'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($dompet['updated_on'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('payment:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($dompet['created_by'], true); ?></div>
	</div>
</div>