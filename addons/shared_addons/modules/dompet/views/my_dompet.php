<div class="">
<style type="text/css">
	.hhh {
        font-family: "Montserrat", sans-serif;
        font-weight: 700;
         margin: 1px !important; 
        text-transform: uppercase;
    }
</style>
<div class="row">
  <div class="col-md-12" style="">
    <h2 class="h4">Saldo (<?php echo date('d M Y'); ?>) : <strong><?php echo number_format($saldo['saldo'],0,",","."); ?></strong></h2>
  </div>
</div>
<div class="row tabs-row shortcodeBlock m0" id="tabs">
    <div class="">
    	<div class="col-md-12">
    		<div class="tab-v1">
				<ul class="nav nav-tabs tab-disapear">
			    	<li role="presentation" class="<?php echo ($state == 'index') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/index' ?>" aria-controls="tabs1" role="tab" >Mutasi</a></li>
			        <li role="presentation" class="<?php echo ($state == 'tambah_saldo') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/tambah_saldo' ?>" aria-controls="tabs1" role="tab">Tambah Saldo</a></li>
			        <li role="presentation" class="<?php echo ($state == 'tarik_saldo') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/tarik_saldo' ?>" aria-controls="tabs1" role="tab">Tarik Saldo</a></li>
			        <li role="presentation" class="<?php echo ($state == 'request_saya') ? 'active' : ''; ?>"><a href="<?php echo base_url().'dompet/saldo/request_saya' ?>" aria-controls="tabs1" role="tab">Request Saya</a></li>
				</ul>
			</div>
    	</div>
    </div>
    <div class="col-xs-12">
        <div class="profile-edit">
    		<fieldset>
                <?php echo form_open('', array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
	        	<div class="row" style="padding-bottom: 10px;">
		            <div class="col-md-2">
		                <label>Transaksi :&nbsp;</label>
		            </div>
		            <div class="col-md-10">
		                <select name="f-transaksi" class="form-control" style="width:25%">
		                    <option value="">-- Semua --</option>
		                    <option value="topup">TopUp</option>
		                    <option value="penarikan">Penarikan Saldo</option>
		                </select>
		            </div>
		        </div>
		    </fieldset>
	     	<button href="<?php echo current_url() . '#'; ?>" class="btn btn-success btn-sm" type="submit">
                <i class="icon-ok"></i>
                <?php echo lang('buttons:filter'); ?>
            </button>
            
            <button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary dark btn-sm" type="reset">
                <i class="icon-remove"></i>
                <?php echo lang('buttons:clear'); ?>
            </button>
		</div>
	</div>
<?php if ($dompet['total'] > 0): ?>
	<div class="col-xs-12" style="margin-top:20px"> 
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th style="text-align:center">No</th>
				<th style="text-align:center">Keterangan</th>
				<th style="text-align:center">Tanggal</th>
				<th style="text-align:center">Jumlah</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
			if($cur_page != 0){
				$item_per_page = $pagination_config['per_page'];
				$no = (($cur_page -1) * $item_per_page) + 1;
			}else{
				$no = 1;
			}
			?>
			
			<?php foreach ($dompet['mutasi'] as $dompet_entry): ?>
			<tr>
				<td align="center"><?php echo $no; $no++; ?></td>
				<td><?php echo $dompet_entry['keterangan']; ?></td>
				<td><?php echo date("d F Y H:i",strtotime($dompet_entry['tanggal'])); ?></td>
				<td align="right"><?php 
				if($dompet_entry['tipe'] == "topup"){
					echo number_format($dompet_entry['nominal_debet'],0,",",".");
				}elseif ($dompet_entry['tipe'] == "penarikan") {
					echo number_format($dompet_entry['nominal_kredit'],0,",",".");
				}
				 ?></td>
				
			</tr>
			<?php endforeach; ?>
		
		</tbody>
	</table>
	
	<?php echo $dompet['pagination']; ?>
	</div>
<?php else: ?>
	<div class="col-xs-12" style="margin-top:20px">
		<div class="well" style="margin-top: 0px;"><?php echo lang('dompet:no_history'); ?>
		</div>
	</div>
<?php endif;?>
</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("input[name='f-id_user']").val("<?php echo $this->input->get('f-id_user'); ?>");
		$("input[name='f-saldo']").val("<?php echo $this->input->get('f-saldo'); ?>");
		$("input[name='f-group']").val("<?php echo $this->input->get('f-group'); ?>");
		$("select[name='f-transaksi']").val("<?php echo $this->input->get('f-transaksi'); ?>");
	});
</script>