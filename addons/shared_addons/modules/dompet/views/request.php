<div class="">
	<style type="text/css">
		.hhh {
	        font-family: "Montserrat", sans-serif;
	        font-weight: 700;
	         margin: 1px !important; 
	        text-transform: uppercase;
	        display: inline;
	    }
        .label-danger {
            background-color: #d9534f;
        }
        .label-success {
            background-color: #5cb85c;
        }
        .label-warning {
            background-color: #f0ad4e;
        }
	</style>
	<div class="row sectionTitle text-center">
	<h3>
		<span>Semua Request</span>
	</h3>
	</div>
	<hr class="nopad">
    <br>

<div class="row tabs-row shortcodeBlock m0" id="tabs">
    <!-- Nav tabs -->
    <div class="col-xs-12">
        <div class="profile-edit">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="fa fa-remove"></i>
                    </button>
                    <h2 class=" h5"><?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif;?>
            <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close" type="button">
                        <i class="fa fa-remove"></i>
                    </button>
                    <h2 class=" h5"><?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php endif;?>
            <fieldset>
                <?php echo form_open('', array('class' => 'form-horizontal', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
                    <div class="row" style="padding-bottom: 10px;">
                        <div class="col-md-2">
                            <label>Transaksi :&nbsp;</label>
                        </div>
                        <div class="col-md-10">
                            <select name="f-transaksi" class="form-control" style="width:25%">
                                <option value="">-- Semua --</option>
                                <option value="topup">Tambah Saldo</option>
                                <option value="penarikan">Tarik Saldo</option>
                            </select>
                        </div>
                    </div>
                    <div class="row" style="padding-bottom: 10px;">
                        <div class="col-md-2">
                            <label>Status :&nbsp;</label>
                        </div>
                        <div class="col-md-10">
                            <select name="f-status" class="form-control" style="width:25%">
                                <option value="">-- Semua --</option>
                                <option value="pending">Pending</option>
                                <option value="diterima">Verified</option>
                                <option value="ditolak">Rejected</option>
                            </select>
                        </div>
                    </div>
                    <button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-sm" type="submit">
                        <i class="icon-ok"></i>
                        <?php echo lang('buttons:filter'); ?>
                    </button>
                    
                    <button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary dark btn-sm" type="reset">
                        <i class="icon-remove"></i>
                        <?php echo lang('buttons:clear'); ?>
                    </button>

                <?php echo form_close() ?>
            </fieldset>
        </div>
        	<hr>
            <p><?php if (count($request) > 0): ?>
            
            <table class="table table-striped table-bordered table-hover">
                <tr>
                    <th style="text-align:center;">No</th>
                    <th style="text-align:center;">Transaksi</th>
                    <th style="text-align:center;">Jumlah</th>
                    <th style="text-align:center;">Tanggal</th>
                    <th style="text-align:center;">Keterangan</th>
                    <th style="text-align:center;">Aksi / Status</th>
                </tr>
			<?php 
                $no = 1;
                foreach ($request as $request_entry): 
            ?>
                <tr>
                    <td align="center"><?php echo $no; $no++; ?></td>
                    <td><?php
                        echo ucfirst($request_entry['tipe']); 
                        // if($request_entry['transaksi'] == 1){
                        //     echo "Tambah Saldo";
                        // }else{
                        //     echo "Tarik Saldo";
                        //} ; ?></td>
                    <td align="right"><?php echo number_format($request_entry['jumlah'],0,",","."); ?></td>
                    <td align="center"><?php echo date('d F Y H:i:s', strtotime($request_entry['created_on'])); ?></td>
                    <td><?php 
                        $detail_rekening_asal = "";
                        $detail_rekening_tujuan = "";
                        $id_rekening_asal = array_search($request_entry['rekening_asal'], $rekening_id);
                        $id_rekening_tujuan = array_search($request_entry['rekening_tujuan'], $rekening_id);

                        if((int)$request_entry['rekening_asal'] > 0){
                            $detail_rekening_asal = $all_rekening[$id_rekening_asal]['nama_bank']." - ".$all_rekening[$id_rekening_asal]['no_rekening']." (".$all_rekening[$id_rekening_asal]['atas_nama'].")";
                        }
                        if((int)$request_entry['rekening_tujuan'] > 0){
                            $detail_rekening_tujuan = $all_rekening[$id_rekening_tujuan]['nama_bank']." - ".$all_rekening[$id_rekening_tujuan]['no_rekening']." (".$all_rekening[$id_rekening_asal]['atas_nama'].")";
                        }
                        if($request_entry['tipe'] == "topup"){ 
                            echo "Telah transfer dari dari : <br>".$detail_rekening_asal." ke ".$detail_rekening_tujuan;
                        }elseif($request_entry['tipe'] == "penarikan"){
                            echo "Permintaan pencairan dana ke <br>".$detail_rekening_tujuan;
                        }
                        ?>
                    <td>
                    <?php if($request_entry['status'] == "diterima"){  ?>
                        <label class="label label-success label-md ">Transaksi Berhasil</label>
                        <br>
                    <?php }elseif($request_entry['status'] == "ditolak"){ ?>
                        <label class="label label-danger label-md ">Transaksi Ditolak</label>
                        <br>
                    <?php } ; ?>
                    
                    <?php
                        if($request_entry['status'] == "pending" && $request_entry['tipe'] == "topup"){
                            ?>
                            <h2 class="hhh h4" style="display:inline;"><a href="<?php echo base_url().'dompet/saldo/confirm_payment/'.$request_entry['id'].'/1'; ?>" class="confirm btn btn-success btn-sm">Diterima</a></h2>
                            <h2 class="hhh h4" style="display:inline;"><a href="<?php echo base_url().'dompet/saldo/confirm_payment/'.$request_entry['id'].'/2'; ?>"  class="confirm btn btn-danger btn-sm">Ditolak</a></h2>
                        <?php }elseif($request_entry['status'] == "pending" && $request_entry['tipe'] == "penarikan"){ ?>
                            <h2 class="hhh h4" style="display:inline;"><a href="<?php echo base_url().'dompet/saldo/confirm_payment/'.$request_entry['id'].'/1'; ?>" class="confirm btn btn-success btn-sm">Terkirim</a></h2>
                        <?php }
                    ?>
                    </td>
                </tr>
			<?php endforeach; ?>
            </table>
			<?php else: ?>
				<div class="well" style="margin-top: 30px;">Tidak Terdapat entry Request</div>
			<?php endif;?></p>
			</div>
    </div>
    <!-- <div class="tab-content hhh-tab-content">
			
	</div> -->
</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
        $('a.confirm').on('click', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            bootbox.confirm('Apakah Anda yakin akan melakukan aksi ini?', function (result) {
                if (result) {
                    window.location.href = url;
                }
            });
        });

		$("select[name='f-status']").val("<?php echo $this->input->get('f-status'); ?>");
		$("select[name='f-transaksi']").val("<?php echo $this->input->get('f-transaksi'); ?>");
		$("input[name='f-group']").val("<?php echo $this->input->get('f-group'); ?>");
	});
</script>