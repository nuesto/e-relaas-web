<div class="page-header">
	<h1><?php echo lang('dompet:saldo:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string().$uri); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="saldo"><?php echo lang('dompet:saldo'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('saldo') != NULL){
					$value = $this->input->post('saldo');
				}elseif($this->input->get('f-saldo') != NULL){
					$value = $this->input->get('f-saldo');
				}elseif($mode == 'edit'){
					$value = $fields['saldo'];
				}
			?>
			<input name="saldo" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_owner"><?php echo lang('dompet:id_owner'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_owner') != NULL){
					$value = $this->input->post('id_owner');
				}elseif($this->input->get('f-id_owner') != NULL){
					$value = $this->input->get('f-id_owner');
				}elseif($mode == 'edit'){
					$value = $fields['id_owner'];
				}
			?>
			<input name="id_owner" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="created_on"><?php echo lang('dompet:created_on'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('created_on') != NULL){
					$value = $this->input->post('created_on');
				}elseif($this->input->get('f-created_on') != NULL){
					$value = $this->input->get('f-created_on');
				}elseif($mode == 'edit'){
					$value = $fields['created_on'];
				}
			?>
			<input name="created_on" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="created_by"><?php echo lang('dompet:created_by'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('created_by') != NULL){
					$value = $this->input->post('created_by');
				}elseif($this->input->get('f-created_by') != NULL){
					$value = $this->input->get('f-created_by');
				}elseif($mode == 'edit'){
					$value = $fields['created_by'];
				}
			?>
			<input name="created_by" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="updated_on"><?php echo lang('dompet:updated_on'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('updated_on') != NULL){
					$value = $this->input->post('updated_on');
				}elseif($this->input->get('f-updated_on') != NULL){
					$value = $this->input->get('f-updated_on');
				}elseif($mode == 'edit'){
					$value = $fields['updated_on'];
				}
			?>
			<input name="updated_on" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="updated_by"><?php echo lang('dompet:updated_by'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('updated_by') != NULL){
					$value = $this->input->post('updated_by');
				}elseif($this->input->get('f-updated_by') != NULL){
					$value = $this->input->get('f-updated_by');
				}elseif($mode == 'edit'){
					$value = $fields['updated_by'];
				}
			?>
			<input name="updated_by" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="ordering_count"><?php echo lang('dompet:ordering_count'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('ordering_count') != NULL){
					$value = $this->input->post('ordering_count');
				}elseif($this->input->get('f-ordering_count') != NULL){
					$value = $this->input->get('f-ordering_count');
				}elseif($mode == 'edit'){
					$value = $fields['ordering_count'];
				}
			?>
			<input name="ordering_count" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>