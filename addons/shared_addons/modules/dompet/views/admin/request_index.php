<div class="page-header">	<h1><?php echo lang('dompet:request:plural'); ?></h1>		<?php file_partial('shortcuts'); ?></div><?php echo form_open('', array('class' => 'form-inline', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>	<div class="form-group">
		<label><?php echo lang('dompet:id_dompet'); ?>:&nbsp;</label>
		<input type="text" name="f-id_dompet" value="<?php echo $this->input->get('f-id_dompet'); ?>">
	</div>
	<div class="form-group">
		<label><?php echo lang('dompet:tipe'); ?>:&nbsp;</label>
		<input type="text" name="f-tipe" value="<?php echo $this->input->get('f-tipe'); ?>">
	</div>
	<div class="form-group">
		<label><?php echo lang('dompet:status'); ?>:&nbsp;</label>
		<input type="text" name="f-status" value="<?php echo $this->input->get('f-status'); ?>">
	</div>
	<div class="form-group">
		<label><?php echo lang('dompet:jumlah'); ?>:&nbsp;</label>
		<input type="text" name="f-jumlah" value="<?php echo $this->input->get('f-jumlah'); ?>">
	</div>
	<div class="form-group">
		<label><?php echo lang('dompet:rekening_asal'); ?>:&nbsp;</label>
		<input type="text" name="f-rekening_asal" value="<?php echo $this->input->get('f-rekening_asal'); ?>">
	</div>
	<div class="form-group">
		<label><?php echo lang('dompet:rekening_tujuan'); ?>:&nbsp;</label>
		<input type="text" name="f-rekening_tujuan" value="<?php echo $this->input->get('f-rekening_tujuan'); ?>">
	</div>
	<div class="form-group">
		<label><?php echo lang('dompet:id_request_bukti'); ?>:&nbsp;</label>
		<input type="text" name="f-id_request_bukti" value="<?php echo $this->input->get('f-id_request_bukti'); ?>">
	</div>
	<div class="form-group">
		<label><?php echo lang('dompet:created_on'); ?>:&nbsp;</label>
		<input type="text" name="f-created_on" value="<?php echo $this->input->get('f-created_on'); ?>">
	</div>
	<div class="form-group">
		<label><?php echo lang('dompet:created_by'); ?>:&nbsp;</label>
		<input type="text" name="f-created_by" value="<?php echo $this->input->get('f-created_by'); ?>">
	</div>
	<div class="form-group">
		<label><?php echo lang('dompet:last_updated_on'); ?>:&nbsp;</label>
		<input type="text" name="f-last_updated_on" value="<?php echo $this->input->get('f-last_updated_on'); ?>">
	</div>
	<div class="form-group">
		<label><?php echo lang('dompet:last_updated_by'); ?>:&nbsp;</label>
		<input type="text" name="f-last_updated_by" value="<?php echo $this->input->get('f-last_updated_by'); ?>">
	</div>
	<div class="form-group">
		<label><?php echo lang('dompet:history'); ?>:&nbsp;</label>
		<input type="text" name="f-history" value="<?php echo $this->input->get('f-history'); ?>">
	</div>
	<div class="form-group">		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">			<i class="icon-filter"></i>			<?php echo lang('global:filters'); ?>		</button>				<button href="<?php echo current_url() . '#'; ?>" class="btn btn-xs" type="reset">			<i class="icon-remove"></i>			<?php echo lang('buttons:clear'); ?>		</button>	</div><?php echo form_close() ?><?php if ($request['total'] > 0): ?>		<p class="pull-right"><?php echo lang('dompet:showing').' '.count($request['entries']).' '.lang('dompet:of').' '.$request['total'] ?></p>		<table class="table table-striped table-bordered table-hover">		<thead>			<tr>				<th>No</th>				<th><?php echo lang('dompet:id_dompet'); ?></th>				<th><?php echo lang('dompet:tipe'); ?></th>				<th><?php echo lang('dompet:status'); ?></th>				<th><?php echo lang('dompet:jumlah'); ?></th>				<th><?php echo lang('dompet:rekening_asal'); ?></th>				<th><?php echo lang('dompet:rekening_tujuan'); ?></th>				<th><?php echo lang('dompet:id_request_bukti'); ?></th>				<th><?php echo lang('dompet:created_on'); ?></th>				<th><?php echo lang('dompet:created_by'); ?></th>				<th><?php echo lang('dompet:last_updated_on'); ?></th>				<th><?php echo lang('dompet:last_updated_by'); ?></th>				<th><?php echo lang('dompet:history'); ?></th>				<th><?php echo lang('dompet:created'); ?></th>				<th><?php echo lang('dompet:updated'); ?></th>				<th><?php echo lang('dompet:created_by'); ?></th>				<th></th>			</tr>		</thead>		<tbody>			<?php 			$cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);			if($cur_page != 0){				if(isset($pagination_config['use_page_numbers']) AND $pagination_config['use_page_numbers'] === true) {					$item_per_page = $pagination_config['per_page'];					$no = (($cur_page -1) * $item_per_page) + 1;				} else {					$no = $cur_page+1;				}			}else{				$no = 1;			}			?>						<?php foreach ($request['entries'] as $request_entry): ?>			<tr>				<td><?php echo $no; $no++; ?></td>				<td><?php echo $request_entry['id_dompet']; ?></td>				<td><?php echo $request_entry['tipe']; ?></td>				<td><?php echo $request_entry['status']; ?></td>				<td><?php echo $request_entry['jumlah']; ?></td>				<td><?php echo $request_entry['rekening_asal']; ?></td>				<td><?php echo $request_entry['rekening_tujuan']; ?></td>				<td><?php echo $request_entry['id_request_bukti']; ?></td>				<td><?php echo $request_entry['created_on']; ?></td>				<td><?php echo $request_entry['created_by']; ?></td>				<td><?php echo $request_entry['last_updated_on']; ?></td>				<td><?php echo $request_entry['last_updated_by']; ?></td>				<td><?php echo $request_entry['history']; ?></td>							<?php if($request_entry['created_on']){ ?>				<td><?php echo format_date($request_entry['created_on'], 'd-m-Y G:i'); ?></td>				<?php }else{ ?>				<td>-</td>				<?php } ?>								<?php if($request_entry['updated_on']){ ?>				<td><?php echo format_date($request_entry['updated_on'], 'd-m-Y G:i'); ?></td>				<?php }else{ ?>				<td>-</td>				<?php } ?>								<td><?php echo user_displayname($request_entry['created_by'], true); ?></td>				<td class="actions">				<?php 				if(group_has_role('dompet', 'view_all_request')){					echo anchor('admin/dompet/request/view/' . $request_entry['id'].$uri, lang('global:view'), 'class="btn btn-xs btn-info view"');				}elseif(group_has_role('dompet', 'view_own_request')){					if($request_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('admin/dompet/request/view/' . $request_entry['id'].$uri, lang('global:view'), 'class="btn btn-xs btn-info view"');					}				}				?>				<?php 				if(group_has_role('dompet', 'edit_all_request')){					echo anchor('admin/dompet/request/edit/' . $request_entry['id'].$uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');				}elseif(group_has_role('dompet', 'edit_own_request')){					if($request_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('admin/dompet/request/edit/' . $request_entry['id'].$uri, lang('global:edit'), 'class="btn btn-xs btn-info edit"');					}				}				?>				<?php 				if(group_has_role('dompet', 'delete_all_request')){					echo anchor('admin/dompet/request/delete/' . $request_entry['id'].$uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));				}elseif(group_has_role('dompet', 'delete_own_request')){					if($request_entry['created_by']['user_id'] == $this->current_user->id){						echo anchor('admin/dompet/request/delete/' . $request_entry['id'].$uri, lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));					}				}				?>				</td>			</tr>			<?php endforeach; ?>		</tbody>	</table>		<?php echo $request['pagination']; ?>	<?php else: ?>	<div class="well"><?php echo lang('dompet:request:no_entry'); ?></div><?php endif;?>