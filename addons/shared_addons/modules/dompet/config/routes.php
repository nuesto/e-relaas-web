<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['dompet/admin/saldo(:any)'] = 'admin_saldo$1';
$route['dompet/admin/request(:any)'] = 'admin_request$1';
$route['dompet/admin/mutasi(:any)'] = 'admin_mutasi$1';
$route['dompet/saldo(:any)'] = 'dompet_saldo$1';
$route['dompet/request(:any)'] = 'dompet_request$1';
$route['dompet/mutasi(:any)'] = 'dompet_mutasi$1';
