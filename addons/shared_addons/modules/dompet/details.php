<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Dompet extends Module
{
    public $version = '1.1.2';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Dompet',
			'id' => 'Dompet',
		);
		$info['description'] = array(
			'en' => 'Dompet',
			'id' => 'Dompet',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		$info['menu'] = 'Dompet';
		$info['roles'] = array(
			'access_saldo_backend', 'view_all_saldo', 'view_own_saldo', 'edit_all_saldo', 'edit_own_saldo', 'delete_all_saldo', 'delete_own_saldo', 'create_saldo',
			'access_request_backend', 'view_all_request', 'view_own_request', 'edit_all_request', 'edit_own_request', 'delete_all_request', 'delete_own_request', 'create_request',
			'access_mutasi_backend', 'view_all_mutasi', 'view_own_mutasi', 'edit_all_mutasi', 'edit_own_mutasi', 'delete_all_mutasi', 'delete_own_mutasi', 'create_mutasi',
		);
		
		if(group_has_role('dompet', 'access_saldo_backend')){
			$info['sections']['saldo']['name'] = 'dompet:saldo:plural';
			$info['sections']['saldo']['uri'] = array('urls'=>array('admin/dompet/saldo/index','admin/dompet/saldo/create','admin/dompet/saldo/view%1','admin/dompet/saldo/edit%1','admin/dompet/saldo/view%2','admin/dompet/saldo/edit%2'));
			
			if(group_has_role('dompet', 'create_saldo')){
				$info['sections']['saldo']['shortcuts']['create'] = array(
					'name' => 'dompet:saldo:new',
					'uri' => 'admin/dompet/saldo/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('dompet', 'access_request_backend')){
			$info['sections']['request']['name'] = 'dompet:request:plural';
			$info['sections']['request']['uri'] = array('urls'=>array('admin/dompet/request/index','admin/dompet/request/create','admin/dompet/request/view%1','admin/dompet/request/edit%1','admin/dompet/request/view%2','admin/dompet/request/edit%2'));
			
			if(group_has_role('dompet', 'create_request')){
				$info['sections']['request']['shortcuts']['create'] = array(
					'name' => 'dompet:request:new',
					'uri' => 'admin/dompet/request/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('dompet', 'access_mutasi_backend')){
			$info['sections']['mutasi']['name'] = 'dompet:mutasi:plural';
			$info['sections']['mutasi']['uri'] = array('urls'=>array('admin/dompet/mutasi/index','admin/dompet/mutasi/create','admin/dompet/mutasi/view%1','admin/dompet/mutasi/edit%1','admin/dompet/mutasi/view%2','admin/dompet/mutasi/edit%2'));
			
			if(group_has_role('dompet', 'create_mutasi')){
				$info['sections']['mutasi']['shortcuts']['create'] = array(
					'name' => 'dompet:mutasi:new',
					'uri' => 'admin/dompet/mutasi/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items){
		
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// saldo
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'saldo' => array(
				'type' => 'DECIMAL',
			),
			'id_owner' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'default' => 0,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('dompet_saldo', TRUE);
		//$this->db->query("CREATE INDEX fk_dompet_saldo_id_owner ON default_dompet_saldo(id_owner)");
		//$this->db->query("CREATE INDEX fk_dompet_saldo_created_by ON default_dompet_saldo(created_by)");
		//$this->db->query("CREATE INDEX fk_dompet_saldo_updated_by ON default_dompet_saldo(updated_by)");


		// request
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'id_dompet' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
			),
			'tipe' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
			),
			'status' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
			),
			'jumlah' => array(
				'type' => 'DECIMAL',
				'unsigned' => TRUE,
			),
			'rekening_asal' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
			),
			'rekening_tujuan' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
			),
			'id_request_bukti' => array(
				'type' => 'CHAR',
				'constraint' => '15',
				'null' => TRUE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
			),
			'last_updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'last_updated_by' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'history' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('dompet_request', TRUE);
		$this->db->query("CREATE INDEX fk_dompet_request_iddompet_dompet_saldo ON default_dompet_request(id_dompet)");
		$this->db->query("CREATE INDEX fk_dompet_request_rekeningasal_rekening ON default_dompet_request(rekening_asal)");
		$this->db->query("CREATE INDEX fk_dompet_request_rekeningtujuan_rekening ON default_dompet_request(rekening_tujuan)");
		$this->db->query("CREATE INDEX fk_dompet_request_createdby_users ON default_dompet_request(created_by)");
		$this->db->query("CREATE INDEX fk_dompet_request_verifiedby_users ON default_dompet_request(last_updated_by)");


		// mutasi
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'id_dompet' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
			),
			'tanggal' => array(
				'type' => 'DATETIME',
			),
			'tipe' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
			),
			'nominal_debet' => array(
				'type' => 'DECIMAL',
				'unsigned' => TRUE,
				'default' => 0,
			),
			'nominal_kredit' => array(
				'type' => 'DECIMAL',
				'unsigned' => TRUE,
				'default' => 0,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
			),
			'keterangan' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'history' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('dompet_mutasi', TRUE);
		//$this->db->query("CREATE INDEX fk_default_mutasi_iddompet_dompet_saldo ON default_dompet_mutasi(id_dompet)");
		$this->db->query("CREATE INDEX fk_default_mutasi_createdby_users ON default_dompet_mutasi(created_by)");

		if(!$this->db->field_exists('otp','commerce_payment')) {
			$new_field = array(
				'id_dompet' => array(
					'type' => 'INT',
					'unsigned' => TRUE,
					'null' => TRUE,
					),
				'otp' => array(
					'type' => 'VARCHAR',
					'constraint' => 6,
					'null' => TRUE,
					),
				'is_opt_verified' => array(
					'type' => 'TINYINT',
					'constraint' => 1,
					'null' => TRUE,
					),
				'otp_verified_on' => array(
					'type' => 'DATETIME',
					'null' => TRUE,
					),
				'otp_verified_by' => array(
					'type' => 'INT',
					'constraint' => 11,
					'null' => TRUE,
					'unsigned' => TRUE,
					),
				'resend_attempt' => array(
					'type' => 'TINYINT',
					'constraint' => 1,
					'null' => TRUE,
					),
				'retry_attemp' => array(
					'type' => 'TINYINT',
					'constraint' => 1,
					'null' => TRUE,
					),
				'otp_expired_on' => array(
					'type' => 'DATETIME',
					'null' => TRUE,
					),
				'otp_created_on' => array(
					'type' => 'DATETIME',
					'null' => TRUE,
					),
				);

			$this->dbforge->add_column('commerce_payment',$new_field);
		}

		$new_field = array(
			'otp' => array(
				'type' => 'VARCHAR',
				'constraint' => 6,
				'null' => TRUE,
				),
			'is_opt_verified' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'null' => TRUE,
				),
			'otp_verified_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
				),
			'otp_verified_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,
				'unsigned' => TRUE,
				),
			'resend_attempt' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'null' => TRUE,
				),
			'retry_attemp' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'null' => TRUE,
				),
			'otp_expired_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
				),
			'otp_created_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
				),
			);

		$this->dbforge->add_column('dompet_request',$new_field);

		//$this->db->query("CREATE INDEX fk_default_mutasi_iddompet_dompet_saldo ON default_commerce_payment(id_dompet)");
		//$this->db->query("CREATE INDEX fk_default_mutasi_createdby_users ON default_commerce_payment(otp_verified_by)");

		$this->db->query("ALTER TABLE default_dompet_saldo ADD CONSTRAINT fk_dompet_saldo_idowner_users FOREIGN KEY(id_owner) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
		$this->db->query("ALTER TABLE default_dompet_saldo ADD CONSTRAINT fk_dompet_saldo_createdby_users FOREIGN KEY(created_by) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
		$this->db->query("ALTER TABLE default_dompet_saldo ADD CONSTRAINT fk_dompet_saldo_updatedby_users FOREIGN KEY(updated_by) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
		$this->db->query("ALTER TABLE default_dompet_mutasi ADD CONSTRAINT fk_dompet_mutasi_iddompet_dompet_saldo FOREIGN KEY(id_dompet) REFERENCES default_dompet_saldo(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
		$this->db->query("ALTER TABLE default_dompet_mutasi ADD CONSTRAINT fk_dompet_mutasi_createdby_users FOREIGN KEY(created_by) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
		$this->db->query("ALTER TABLE default_dompet_request ADD CONSTRAINT fk_dompet_request_iddompet_dompet_saldo FOREIGN KEY(id_dompet) REFERENCES default_dompet_saldo(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
		//$this->db->query("ALTER TABLE default_dompet_request ADD CONSTRAINT fk_dompet_request_rekeningasal_rekening FOREIGN KEY(rekening_asal) REFERENCES default_commerce_rekening(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
		//$this->db->query("ALTER TABLE default_dompet_request ADD CONSTRAINT fk_dompet_request_rekeningtujuan_rekening FOREIGN KEY(rekening_tujuan) REFERENCES default_commerce_rekening(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
		$this->db->query("ALTER TABLE default_dompet_request ADD CONSTRAINT fk_dompet_request_createdby_users FOREIGN KEY(created_by) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
		$this->db->query("ALTER TABLE default_dompet_request ADD CONSTRAINT fk_dompet_request_verifiedby_users FOREIGN KEY(last_updated_by) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
		//$this->db->query("ALTER TABLE default_commerce_payment ADD CONSTRAINT fk_payment_id_dompet_saldo FOREIGN KEY(id_dompet) REFERENCES default_dompet_saldo(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
		//$this->db->query("ALTER TABLE default_commerce_payment ADD CONSTRAINT fk_payment_otp_verified_by_users FOREIGN KEY(otp_verified_by) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");

		return true;


    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
		$this->db->query("SET FOREIGN_KEY_CHECKS=0;");
        $this->dbforge->drop_table('dompet_saldo');
        $this->dbforge->drop_table('dompet_request');
        $this->dbforge->drop_table('dompet_mutasi');
        $this->db->query("SET FOREIGN_KEY_CHECKS=1;");
        return true;
    }

    public function upgrade($old_version)
    {
    	switch ($old_version) {
    		case '1.0':
    			if(!$this->db->field_exists('otp','commerce_payment')) {
	    			$new_field = array(
						'id_dompet' => array(
							'type' => 'INT',
							'unsigned' => TRUE,
							'null' => TRUE,
							),
						'otp' => array(
							'type' => 'VARCHAR',
							'constraint' => 6,
							'null' => TRUE,
							),
						'is_opt_verified' => array(
							'type' => 'TINYINT',
							'constraint' => 1,
							'null' => TRUE,
							),
						'otp_verified_on' => array(
							'type' => 'DATETIME',
							'null' => TRUE,
							),
						'otp_verified_by' => array(
							'type' => 'INT',
							'constraint' => 11,
							'null' => TRUE,
							),
						'resend_attempt' => array(
							'type' => 'TINYINT',
							'constraint' => 1,
							'null' => TRUE,
							),
						'retry_attemp' => array(
							'type' => 'TINYINT',
							'constraint' => 1,
							'null' => TRUE,
							),
						'otp_expired_on' => array(
							'type' => 'DATETIME',
							'null' => TRUE,
							),
						'otp_created_on' => array(
							'type' => 'DATETIME',
							'null' => TRUE,
							),
	    				);

	    			$this->dbforge->add_column('commerce_payment',$new_field);

	    			//$this->db->query("CREATE INDEX fk_default_mutasi_iddompet_dompet_saldo ON default_commerce_payment(id_dompet)");
					$this->db->query("CREATE INDEX fk_default_mutasi_createdby_users ON default_commerce_payment(otp_verified_by)");
				}

    			$this->version = '1.1';
    			break;
    		case '1.1' : 
    			// Adding all foreign key
    			// dompet_saldo
    			$is_exists = $this->db->query("SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = DATABASE() AND
                   CONSTRAINT_NAME   = 'fk_dompet_saldo_idowner_users' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'");
				
				if(count($is_exists) == 0){
    				$this->db->query("ALTER TABLE default_dompet_saldo ADD CONSTRAINT fk_dompet_saldo_idowner_users FOREIGN KEY(id_owner) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
    			}
					
    			
    			$is_exists = $this->db->query("SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = DATABASE() AND
                   CONSTRAINT_NAME   = 'fk_dompet_saldo_createdby_users' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'");
				
				if(count($is_exists) == 0){
    				$this->db->query("ALTER TABLE default_dompet_saldo ADD CONSTRAINT fk_dompet_saldo_createdby_users FOREIGN KEY(created_by) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
    			}
    			$is_exists = $this->db->query("SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = DATABASE() AND
                   CONSTRAINT_NAME   = 'fk_dompet_saldo_updatedby_users' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'");
				
				if(count($is_exists) == 0){
    				$this->db->query("ALTER TABLE default_dompet_saldo ADD CONSTRAINT fk_dompet_saldo_updatedby_users FOREIGN KEY(updated_by) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
    			}
    			// dompet_mutasi
    			
    			$is_exists = $this->db->query("SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = DATABASE() AND
                   CONSTRAINT_NAME   = 'fk_dompet_mutasi_iddompet_dompet_saldo' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'");
				
				if(count($is_exists) == 0){
    				$this->db->query("ALTER TABLE default_dompet_mutasi ADD CONSTRAINT fk_dompet_mutasi_iddompet_dompet_saldo FOREIGN KEY(id_dompet) REFERENCES default_dompet_saldo(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
    			}
    			$is_exists = $this->db->query("SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = DATABASE() AND
                   CONSTRAINT_NAME   = 'fk_dompet_mutasi_createdby_users' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'");
				
				if(count($is_exists) == 0){
    				$this->db->query("ALTER TABLE default_dompet_mutasi ADD CONSTRAINT fk_dompet_mutasi_createdby_users FOREIGN KEY(created_by) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
    			}
    			// dompet request
    			
    			$is_exists = $this->db->query("SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = DATABASE() AND
                   CONSTRAINT_NAME   = 'fk_dompet_request_iddompet_dompet_saldo' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'");
				
				if(count($is_exists) == 0){
    				$this->db->query("ALTER TABLE default_dompet_request ADD CONSTRAINT fk_dompet_request_iddompet_dompet_saldo FOREIGN KEY(id_dompet) REFERENCES default_dompet_saldo(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
    			}
    			$is_exists = $this->db->query("SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = DATABASE() AND
                   CONSTRAINT_NAME   = 'fk_dompet_request_rekeningasal_rekening' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'");
				
				if(count($is_exists) == 0){
    				$this->db->query("ALTER TABLE default_dompet_request ADD CONSTRAINT fk_dompet_request_rekeningasal_rekening FOREIGN KEY(rekening_asal) REFERENCES default_commerce_rekening(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
    			}
    			$is_exists = $this->db->query("SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = DATABASE() AND
                   CONSTRAINT_NAME   = 'fk_dompet_request_rekeningtujuan_rekening' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'");
				
				if(count($is_exists) == 0){
    				$this->db->query("ALTER TABLE default_dompet_request ADD CONSTRAINT fk_dompet_request_rekeningtujuan_rekening FOREIGN KEY(rekening_tujuan) REFERENCES default_commerce_rekening(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
    			}
    			$is_exists = $this->db->query("SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = DATABASE() AND
                   CONSTRAINT_NAME   = 'fk_dompet_request_createdby_users' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'");
				
				if(count($is_exists) == 0){
    				$this->db->query("ALTER TABLE default_dompet_request ADD CONSTRAINT fk_dompet_request_createdby_users FOREIGN KEY(created_by) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
    			}
    			$is_exists = $this->db->query("SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = DATABASE() AND
                   CONSTRAINT_NAME   = 'fk_dompet_request_verifiedby_users' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'");
				
				if(count($is_exists) == 0){
    				$this->db->query("ALTER TABLE default_dompet_request ADD CONSTRAINT fk_dompet_request_verifiedby_users FOREIGN KEY(last_updated_by) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
    			}
    			// commerce payment
    			
    			$is_exists = $this->db->query("SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = DATABASE() AND
                   CONSTRAINT_NAME   = 'fk_payment_id_dompet_saldo' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'");
				
				if(count($is_exists) == 0){
    				$this->db->query("ALTER TABLE default_commerce_payment ADD CONSTRAINT fk_payment_id_dompet_saldo FOREIGN KEY(id_dompet) REFERENCES default_dompet_saldo(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
    			}
    			$is_exists = $this->db->query("SELECT * FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = DATABASE() AND
                   CONSTRAINT_NAME   = 'fk_payment_otp_verified_by_users' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'");
				
				if(count($is_exists) == 0){
    				$this->db->query("ALTER TABLE default_commerce_payment ADD CONSTRAINT fk_payment_otp_verified_by_users FOREIGN KEY(otp_verified_by) REFERENCES default_users(id) ON UPDATE RESTRICT ON DELETE RESTRICT");
    			}
    			$this->version = '1.1.1';
    			break;
    		case '1.1.1' :
    			$new_field = array(
					'otp' => array(
						'type' => 'VARCHAR',
						'constraint' => 6,
						'null' => TRUE,
						),
					'is_otp_verified' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
						'null' => TRUE,
						),
					'otp_verified_on' => array(
						'type' => 'DATETIME',
						'null' => TRUE,
						),
					'otp_verified_by' => array(
						'type' => 'INT',
						'constraint' => 11,
						'null' => TRUE,
						'unsigned' => TRUE,
						),
					'resend_attempt' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
						'null' => TRUE,
						),
					'retry_attemp' => array(
						'type' => 'TINYINT',
						'constraint' => 1,
						'null' => TRUE,
						),
					'otp_expired_on' => array(
						'type' => 'DATETIME',
						'null' => TRUE,
						),
					'otp_created_on' => array(
						'type' => 'DATETIME',
						'null' => TRUE,
						),
					);

				$this->dbforge->add_column('dompet_request',$new_field);
				$this->version = '1.1.2';
    			break;
    		default:
    			# code...
    			break;
    	}

        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}