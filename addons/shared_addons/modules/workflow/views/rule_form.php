<div class="page-header">
	<h1>
		<span><?php echo lang('workflow:rule:'.$mode); ?></span>
	</h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_domain_state"><?php echo lang('workflow:id_domain_state'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_domain_state') != NULL){
					$value = $this->input->post('id_domain_state');
				}elseif($mode == 'edit'){
					$value = $fields['id_domain_state'];
				}
			?>
			<input name="id_domain_state" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_target_state"><?php echo lang('workflow:id_target_state'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_target_state') != NULL){
					$value = $this->input->post('id_target_state');
				}elseif($mode == 'edit'){
					$value = $fields['id_target_state'];
				}
			?>
			<input name="id_target_state" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="grant_roles"><?php echo lang('workflow:grant_roles'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('grant_roles') != NULL){
					$value = $this->input->post('grant_roles');
				}elseif($mode == 'edit'){
					$value = $fields['grant_roles'];
				}
			?>
			<input name="grant_roles" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>