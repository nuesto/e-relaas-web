<div class="page-header">
	<h1><?php echo lang('workflow:workflow:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="name"><?php echo lang('workflow:name'); ?><span style="color:red;"> * </span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('name') != NULL){
					$value = $this->input->post('name');
				}elseif($mode == 'edit'){
					$value = $fields['name'];
				}
			?>
			<input name="name" id="name" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="slug"><?php echo lang('workflow:slug'); ?><span style="color:red;"> * </span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('slug') != NULL){
					$value = $this->input->post('slug');
				}elseif($mode == 'edit'){
					$value = $fields['slug'];
				}
			?>
			<input name="slug" id="slug" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			<script type="text/javascript">
                $('#name').slugify({slug: '#slug', type: '-'});
            </script>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="description"><?php echo lang('workflow:description'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('description') != NULL){
					$value = $this->input->post('description');
				}elseif($mode == 'edit'){
					$value = $fields['description'];
				}
			?>
			<textarea name="description" class="col-xs-10 col-sm-5" rows="7" id=""><?php echo $value; ?></textarea>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>