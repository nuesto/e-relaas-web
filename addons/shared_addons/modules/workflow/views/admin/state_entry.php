<div class="page-header">
	<h1><?php echo lang('workflow:state:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/workflow/state/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('workflow:back') ?>
		</a>

		<?php if(group_has_role('workflow', 'edit_all_state')){ ?>
			<a href="<?php echo site_url('admin/workflow/state/edit/'.$state['id']); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('workflow', 'edit_own_state')){ ?>
			<?php if($state->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/workflow/state/edit/'.$state->id); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('workflow', 'delete_all_state')){ ?>
			<a href="<?php echo site_url('admin/workflow/state/delete/'.$state['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('workflow', 'delete_own_state')){ ?>
			<?php if($state->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/workflow/state/delete/'.$state['id']); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $state['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:id_workflow'); ?></div>
		<?php if(isset($state['id_workflow'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $state['id_workflow']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:name'); ?></div>
		<?php if(isset($state['name'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $state['name']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:slug'); ?></div>
		<?php if(isset($state['slug'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $state['slug']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:is_initial'); ?></div>
		<?php if(isset($state['is_initial'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $state['is_initial']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:is_final'); ?></div>
		<?php if(isset($state['is_final'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $state['is_final']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:is_content_editable'); ?></div>
		<?php if(isset($state['is_content_editable'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $state['is_content_editable']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:is_content_deletable'); ?></div>
		<?php if(isset($state['is_content_deletable'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $state['is_content_deletable']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:severity'); ?></div>
		<?php if(isset($state['severity'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $state['severity']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:ordering_count'); ?></div>
		<?php if(isset($state['ordering_count'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $state['ordering_count']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:created'); ?></div>
		<?php if(isset($state['created'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($state['created'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:updated'); ?></div>
		<?php if(isset($state['updated'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($state['updated'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($state['created_by'], true); ?></div>
	</div>
</div>