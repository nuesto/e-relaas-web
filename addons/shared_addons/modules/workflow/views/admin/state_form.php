<div class="page-header">
	<h1><?php echo lang('workflow:state:'.$mode). ' ' .lang('workflow:workflow:singular'); ?> "<?php echo $workflow['name'] ?>"</h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="name"><?php echo lang('workflow:status_name'); ?><span style="color:red;"> * </span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('name') != NULL){
					$value = $this->input->post('name');
				}elseif($mode == 'edit'){
					$value = $fields['name'];
				}
			?>
			<input name="id_workflow" type="hidden" value="<?php echo $workflow['id']; ?>" />
			<input name="name" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="name" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="slug"><?php echo lang('workflow:slug'); ?><span style="color:red;"> * </span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('slug') != NULL){
					$value = $this->input->post('slug');
				}elseif($mode == 'edit'){
					$value = $fields['slug'];
				}
			?>
			<input name="slug" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="slug" />
			<script type="text/javascript">
                $('#name').slugify({slug: '#slug', type: '-'});
            </script>
		</div>
	</div>


	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="is_initial"></label>

		<div class="col-sm-10">
			<div class="checkbox" style="padding-top:0px;">
				<?php 
					$value = NULL;
					if($this->input->post('is_initial') != NULL){
						$value = $this->input->post('is_initial');
					}elseif($mode == 'edit'){
						$value = $fields['is_initial'];
					}
				?>
				<label>
					<input class="ace" name="is_initial" type="checkbox" value="1" id="is_initial" <?php echo $value ? 'checked="checked"' : ''; ?> />
					<span class="lbl"> <?php echo lang('workflow:is_initial'); ?></span>
				</label>
			</div>
			<div class="checkbox">
				<?php 
					$value = NULL;
					if($this->input->post('is_final') != NULL){
						$value = $this->input->post('is_final');
					}elseif($mode == 'edit'){
						$value = $fields['is_final'];
					}
				?>
				<label>
					<input class="ace" name="is_final" type="checkbox" value="1" id="is_final" <?php echo $value ? 'checked="checked"' : ''; ?> />
					<span class="lbl"> <?php echo lang('workflow:is_final'); ?></span>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="is_final"></label>

		<div class="col-sm-10">
			<div class="checkbox">
				<?php 
					$value = NULL;
					if($this->input->post('is_content_editable') != NULL){
						$value = $this->input->post('is_content_editable');
					}elseif($mode == 'edit'){
						$value = $fields['is_content_editable'];
					}
				?>
				<label>
					<input class="ace" name="is_content_editable" type="checkbox" value="1" id="is_content_editable" <?php echo $value ? 'checked="checked"' : ''; ?> />
					<span class="lbl"> <?php echo lang('workflow:is_content_editable'); ?></span>
				</label>
			</div>
			<div class="checkbox">
				<?php 
					$value = NULL;
					if($this->input->post('is_content_deletable') != NULL){
						$value = $this->input->post('is_content_deletable');
					}elseif($mode == 'edit'){
						$value = $fields['is_content_deletable'];
					}
				?>
				<label>
					<input class="ace" name="is_content_deletable" type="checkbox" value="1" id="is_content_deletable" <?php echo $value ? 'checked="checked"' : ''; ?> />
					<span class="lbl"> <?php echo lang('workflow:is_content_deletable'); ?></span>
				</label>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="severity"><?php echo lang('workflow:severity'); ?><span style="color:red;"> * </span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('severity') != NULL){
					$value = $this->input->post('severity');
				}elseif($mode == 'edit'){
					$value = $fields['severity'];
				}
				
				$severities = array('normal','success','warning','danger','disabled');
            ?>
            <select name="severity" id="severity">
                <?php foreach ($severities as $severity) { ?>
                    <option value="<?php echo $severity ?>" <?php echo $value == $severity ? 'selected="selected"' : ''; ?>><?php echo lang('workflow:'.$severity); ?></option>
                <?php } ?>
            </select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="ordering_count"><?php echo lang('workflow:ordering_count'); ?><span style="color:red;"> * </span></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('ordering_count') != NULL){
					$value = $this->input->post('ordering_count');
				}elseif($mode == 'edit'){
					$value = $fields['ordering_count'];
				}
			?>
			<select id="ordering_count" name="ordering_count">
                <?php for ($i = 0; $i <= 10; $i++) : ?>
                    <option <?php echo $value == $i ? 'selected="selected"' : ''; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option> 
                <?php endfor; ?>
            </select>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>