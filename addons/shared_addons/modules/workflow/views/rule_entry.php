<div class="page-header">
	<h1>
		<span><?php echo lang('workflow:rule:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('workflow/rule/index'); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('workflow:back') ?>
		</a>

		<?php if(group_has_role('workflow', 'edit_all_rule')){ ?>
			<a href="<?php echo site_url('workflow/rule/edit/'.$rule['id']); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('workflow', 'edit_own_rule')){ ?>
			<?php if($rule->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('workflow/rule/edit/'.$rule['id']); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('workflow', 'delete_all_rule')){ ?>
			<a href="<?php echo site_url('workflow/rule/delete/'.$rule['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('workflow', 'delete_own_rule')){ ?>
			<?php if($rule->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('workflow/rule/delete/'.$rule['id']); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $rule['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:id_domain_state'); ?></div>
		<?php if(isset($rule['id_domain_state'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $rule['id_domain_state']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:id_target_state'); ?></div>
		<?php if(isset($rule['id_target_state'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $rule['id_target_state']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:grant_roles'); ?></div>
		<?php if(isset($rule['grant_roles'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $rule['grant_roles']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:created'); ?></div>
		<?php if(isset($rule['created'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($rule['created'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:updated'); ?></div>
		<?php if(isset($rule['updated'])){ ?>
		<div class="entry-value col-sm-8"><?php echo format_date($rule['updated'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('workflow:created_by'); ?></div>
		<div class="entry-value col-sm-8"><?php echo user_displayname($rule['created_by'], true); ?></div>
	</div>
</div>