<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Rule model
 *
 * @author Aditya Satrya
 */
class Rule_m extends MY_Model {
	
	public function get_rule_matrix($id_workflow = 0) {
        $this->db->select('*');
        $this->db->from('default_workflow_rule r');
        $this->db->join('default_workflow_state s','s.id = r.id_domain_state','left');
        $this->db->where('s.id_workflow',$id_workflow);
        $query = $this->db->get();
        $transitions = $query->result_array();
        
        $result = array();
        foreach ($transitions as $transition) {
            $result[$transition['id_domain_state']][$transition['id_target_state']] = $transition['grant_roles'];
        }
        return $result;
    }
	
	public function get_rule_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_workflow_rule');
		$result = $query->row_array();
		
		return $result;
	}
	
    public function get_rule_by($params=NULL)
    {
        if($params) {
            if(!is_array($params)) {
                $this->db->where($params, NULL, false);
            } else {
                foreach ($params as $key => $value) {
                    $this->db->where($key,$value);
                }
            }
        }

        $result = $this->db->get('default_workflow_rule');
        return $result->result_array();
    }

	public function count_all_rule()
	{
		return $this->db->count_all('workflow_rule');
	}
	
	public function delete_rule_by_id($id_state = NULL, $id_domain_state = NULL, $id_target_state = NULL) {
        if(isset($id_state)){
            $this->db->query("DELETE FROM default_workflow_rule WHERE id_domain_state = '$id_state' OR id_target_state = '$id_state'");
        } else {
            $this->db->where('id_domain_state', $id_domain_state);
            $this->db->where('id_target_state', $id_target_state);
            $this->db->delete('default_workflow_rule');
        }
    }
	
	public function insert_rule($values) {
        return $this->db->insert('default_workflow_rule', $values);
    }

    public function update_rule($values, $id_domain_state, $id_target_state) {
        $this->db->where('id_domain_state', $id_domain_state);
        $this->db->where('id_target_state', $id_target_state);
        return $this->db->update('default_workflow_rule', $values);
    }
	
}