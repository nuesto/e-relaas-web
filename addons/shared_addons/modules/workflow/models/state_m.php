<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * State model
 *
 * @author Aditya Satrya
 */
class State_m extends MY_Model {
	
	public function get_state($id_workflow)
	{
		$this->db->select('*');
		$this->db->where('id_workflow',$id_workflow);
		$this->db->order_by('ordering_count','ASC');

		$query = $this->db->get('default_workflow_state');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_state_by_id($id)
	{
		$this->db->select('s.*, w.name AS workflow_name');
		$this->db->from('default_workflow_state s');
		$this->db->join('default_workflow_workflow w', 'w.id = s.id_workflow');
		$this->db->where('s.id', $id);
		$query = $this->db->get();
		$result = $query->row_array();

		return $result;
	}

	public function get_state_by_slug($slug)
	{
		$this->db->select('s.*, w.name AS workflow_name');
		$this->db->from('default_workflow_state s');
		$this->db->join('default_workflow_workflow w', 'w.id = s.id_workflow');
		$this->db->where('s.slug', $slug);
		$query = $this->db->get();
		$result = $query->row_array();

		return $result;
	}

	public function get_state_by_workflow_slug($slug)
	{
		$this->db->select('s.*, w.name AS workflow_name');
		$this->db->from('default_workflow_state s');
		$this->db->join('default_workflow_workflow w', 'w.id = s.id_workflow');
		$this->db->where('w.slug', $slug);
		$query = $this->db->get();
		$result = $query->result_array();

		return $result;
	}

	public function get_state_by($params = NULL)
	{
		$this->db->select('s.*, w.name AS workflow_name');
		$this->db->from('default_workflow_state s');
		$this->db->join('default_workflow_workflow w', 'w.id = s.id_workflow');

		if($params) {
			if(!is_array($params)) {
				$this->db->where($params, NULL, false);
			} else {
				foreach ($params as $key => $value) {
					$this->db->where($key,$value);
				}
			}
		}

		$query = $this->db->get();
		$result = $query->result_array();

		return $result;
	}
	
	public function count_all_state()
	{
		return $this->db->count_all('workflow_state');
	}
	
	public function delete_state_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_workflow_state');
	}
	
	public function insert_state($values)
	{
		return $this->db->insert('default_workflow_state', $values);
	}
	
	public function update_state($values, $row_id)
	{
		$values['is_initial'] = isset($values['is_initial']) ? $values['is_initial'] : 0;
        $values['is_final'] = isset($values['is_final']) ? $values['is_final'] : 0;
        $values['is_content_editable'] = isset($values['is_content_editable']) ? $values['is_content_editable'] : 0;
        $values['is_content_deletable'] = isset($values['is_content_deletable']) ? $values['is_content_deletable'] : 0;

		$this->db->where('id', $row_id);
		return $this->db->update('default_workflow_state', $values); 
	}

	public function update_ordering_count($id,$i){
        return $this->db->query("UPDATE default_workflow_state SET ordering_count = '$i' WHERE id = '$id'");
    }
	
}