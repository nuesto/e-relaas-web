<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['workflow/admin/workflow(:any)'] = 'admin_workflow$1';
$route['workflow/admin/state(:any)'] = 'admin_state$1';
$route['workflow/admin/rule(:any)'] = 'admin_rule$1';
$route['workflow/workflow(:any)'] = 'workflow_workflow$1';
$route['workflow/state(:any)'] = 'workflow_state$1';
$route['workflow/rule(:any)'] = 'workflow_rule$1';
