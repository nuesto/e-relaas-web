<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Workflow Module
 *
 * Modul untuk mengelola workflow
 *
 */
class Workflow_workflow extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'workflow';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('workflow');
		
		$this->load->model('workflow_m');
    }

    /**
	 * List all workflow
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'view_all_workflow') AND ! group_has_role('workflow', 'view_own_workflow')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'workflow/workflow/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->workflow_m->count_all_workflow();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['workflow']['entries'] = $this->workflow_m->get_workflow($pagination_config);
		$data['workflow']['total'] = count($data['workflow']['entries']);
		$data['workflow']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('workflow:workflow:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('workflow:workflow:plural'))
			->build('workflow_index', $data);
    }
	
	/**
     * Display one workflow
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'view_all_workflow') AND ! group_has_role('workflow', 'view_own_workflow')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['workflow'] = $this->workflow_m->get_workflow_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('workflow', 'view_all_workflow')){
			if($data['workflow']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('workflow:workflow:view'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('workflow:workflow:plural'), '/workflow/workflow/index')
			->set_breadcrumb(lang('workflow:workflow:view'))
			->build('workflow_entry', $data);
    }
	
	/**
     * Create a new workflow entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'create_workflow')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_workflow('new')){	
				$this->session->set_flashdata('success', lang('workflow:workflow:submit_success'));				
				redirect('workflow/workflow/index');
			}else{
				$data['messages']['error'] = lang('workflow:workflow:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'workflow/workflow/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('workflow:workflow:new'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('workflow:workflow:plural'), '/workflow/workflow/index')
			->set_breadcrumb(lang('workflow:workflow:new'))
			->build('workflow_form', $data);
    }
	
	/**
     * Edit a workflow entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the workflow to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'edit_all_workflow') AND ! group_has_role('workflow', 'edit_own_workflow')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'edit_all_workflow')){
			$entry = $this->workflow_m->get_workflow_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_workflow('edit', $id)){	
				$this->session->set_flashdata('success', lang('workflow:workflow:submit_success'));				
				redirect('workflow/workflow/index');
			}else{
				$data['messages']['error'] = lang('workflow:workflow:submit_failure');
			}
		}
		
		$data['fields'] = $this->workflow_m->get_workflow_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'workflow/workflow/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
        $this->template->title(lang('workflow:workflow:edit'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('workflow:workflow:plural'), '/workflow/workflow/index')
			->set_breadcrumb(lang('workflow:workflow:view'), '/workflow/workflow/view/'.$id)
			->set_breadcrumb(lang('workflow:workflow:edit'))
			->build('workflow_form', $data);
    }
	
	/**
     * Delete a workflow entry
     * 
     * @param   int [$id] The id of workflow to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'delete_all_workflow') AND ! group_has_role('workflow', 'delete_own_workflow')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'delete_all_workflow')){
			$entry = $this->workflow_m->get_workflow_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->workflow_m->delete_workflow_by_id($id);
		$this->session->set_flashdata('error', lang('workflow:workflow:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('workflow/workflow/index');
    }
	
	/**
     * Insert or update workflow entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_workflow($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('workflow:workflow:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->workflow_m->insert_workflow($values);
			}
			else
			{
				$result = $this->workflow_m->update_workflow($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}