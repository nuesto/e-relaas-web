<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Workflow Module
 *
 * Modul untuk mengelola workflow
 *
 */
class Workflow_state extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'state';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('workflow');
		
		$this->load->model('state_m');
    }

    /**
	 * List all state
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'view_all_state') AND ! group_has_role('workflow', 'view_own_state')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'workflow/state/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->state_m->count_all_state();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['state']['entries'] = $this->state_m->get_state($pagination_config);
		$data['state']['total'] = count($data['state']['entries']);
		$data['state']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('workflow:state:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('workflow:state:plural'))
			->build('state_index', $data);
    }
	
	/**
     * Display one state
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'view_all_state') AND ! group_has_role('workflow', 'view_own_state')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['state'] = $this->state_m->get_state_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('workflow', 'view_all_state')){
			if($data['state']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('workflow:state:view'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('workflow:state:plural'), '/workflow/state/index')
			->set_breadcrumb(lang('workflow:state:view'))
			->build('state_entry', $data);
    }
	
	/**
     * Create a new state entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'create_state')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_state('new')){	
				$this->session->set_flashdata('success', lang('workflow:state:submit_success'));				
				redirect('workflow/state/index');
			}else{
				$data['messages']['error'] = lang('workflow:state:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'workflow/state/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('workflow:state:new'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('workflow:state:plural'), '/workflow/state/index')
			->set_breadcrumb(lang('workflow:state:new'))
			->build('state_form', $data);
    }
	
	/**
     * Edit a state entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the state to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'edit_all_state') AND ! group_has_role('workflow', 'edit_own_state')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'edit_all_state')){
			$entry = $this->state_m->get_state_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_state('edit', $id)){	
				$this->session->set_flashdata('success', lang('workflow:state:submit_success'));				
				redirect('workflow/state/index');
			}else{
				$data['messages']['error'] = lang('workflow:state:submit_failure');
			}
		}
		
		$data['fields'] = $this->state_m->get_state_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'workflow/state/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
        $this->template->title(lang('workflow:state:edit'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('workflow:state:plural'), '/workflow/state/index')
			->set_breadcrumb(lang('workflow:state:view'), '/workflow/state/view/'.$id)
			->set_breadcrumb(lang('workflow:state:edit'))
			->build('state_form', $data);
    }
	
	/**
     * Delete a state entry
     * 
     * @param   int [$id] The id of state to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'delete_all_state') AND ! group_has_role('workflow', 'delete_own_state')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'delete_all_state')){
			$entry = $this->state_m->get_state_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->state_m->delete_state_by_id($id);
		$this->session->set_flashdata('error', lang('workflow:state:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('workflow/state/index');
    }
	
	/**
     * Insert or update state entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_state($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('workflow:state:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->state_m->insert_state($values);
			}
			else
			{
				$result = $this->state_m->update_state($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}