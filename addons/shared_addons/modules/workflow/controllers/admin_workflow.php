<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Workflow Module
 *
 * Modul untuk mengelola workflow
 *
 */
class Admin_workflow extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'workflow';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('workflow', 'access_workflow_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('workflow');		
		$this->load->model('workflow_m');
    }

    /**
	 * List all workflow
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'admin/workflow/workflow/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['total_rows'] = $this->workflow_m->count_all_workflow();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['workflow']['entries'] = $this->workflow_m->get_workflow($pagination_config);
		$data['workflow']['total'] = count($data['workflow']['entries']);
		$data['workflow']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('workflow:workflow:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('workflow:workflow:singular'), '/admin/workflow/workflow/index')
			->set_breadcrumb(lang('workflow:workflow:plural'))
			->build('admin/workflow_index', $data);
    }
	
	/**
     * Display one workflow
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['workflow'] = $this->workflow_m->get_workflow_by_id($id);

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('workflow:workflow:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('workflow:workflow:plural'), '/admin/workflow/workflow/index')
			->set_breadcrumb(lang('workflow:workflow:view'))
			->build('admin/workflow_entry', $data);
    }
	
	/**
     * Create a new workflow entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_workflow('new')){	
				$this->session->set_flashdata('success', lang('workflow:workflow:submit_success'));				
				redirect('admin/workflow/workflow/index');
			}else{
				$data['messages']['error'] = lang('workflow:workflow:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/workflow/workflow/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('workflow:workflow:new'))
        	->append_js('jquery/jquery.slugify.js')
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('workflow:workflow:singular'), '/admin/workflow/workflow/index')
			->set_breadcrumb(lang('workflow:workflow:new'))
			->build('admin/workflow_form', $data);
    }
	
	/**
     * Edit a workflow entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the workflow to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_workflow('edit', $id)){	
				$this->session->set_flashdata('success', lang('workflow:workflow:submit_success'));				
				redirect('admin/workflow/workflow/index');
			}else{
				$data['messages']['error'] = lang('workflow:workflow:submit_failure');
			}
		}
		
		$data['fields'] = $this->workflow_m->get_workflow_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/workflow/workflow/index/';
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('workflow:workflow:edit'))
        	->append_js('jquery/jquery.slugify.js')
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('workflow:workflow:singular'), '/admin/workflow/workflow/index')
			->set_breadcrumb(lang('workflow:workflow:edit'))
			->build('admin/workflow_form', $data);
    }
	
	/**
     * Delete a workflow entry
     * 
     * @param   int [$id] The id of workflow to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if (!group_has_role('workflow', 'manage_workflow')) {
            $this->session->set_flashdata('error', lang('cp:access_denied'));
            redirect('admin');
        }

		// -------------------------------------
		// Delete entry
		// -------------------------------------

		$this->load->model('state_m');
		$cek = $this->state_m->get_state($id);

        if(count($cek) > 0){
            $message = lang('workflow:workflow:status_still_exist');
        } else {

            $message =  lang('workflow:workflow:deleted');
            $this->workflow_m->delete_workflow_by_id($id);
        }
        $this->session->set_flashdata('error', $message);
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/workflow/workflow/index');
    }
	
	/**
     * Insert or update workflow entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_workflow($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('name', lang('workflow:name'), 'required');
		$this->form_validation->set_rules('slug', lang('workflow:slug'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->workflow_m->insert_workflow($values);
				
			}
			else
			{
				$result = $this->workflow_m->update_workflow($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}