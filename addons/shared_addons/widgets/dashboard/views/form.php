<div class="form-group">
  <label>URI View</label>
   <?php echo form_input('view_uri', set_value('view_uri', isset($options['view_uri']) ? $options['view_uri'] : ''), 'class="form-control"') ?>
</div>

<div class="form-group">
  <label>Color Scheme</label>
  <?php echo form_dropdown('color_scheme', $opt_color_scheme, isset($options['color_scheme']) ? $options['color_scheme'] : '', 'class="form-control"') ?>
</div>