<style type="text/css">
	.loader {
		padding: 20px;
		text-align: center;
	}
</style>
<div class="nav-container">
	<nav class="nav-fixed fixed scrolled">
		<div class="nav-bar">
			<div class="module left">
				<a href="<?php echo base_url(); ?>">
					{{ theme:image file="unify/logo1-default.png" class="logo logo-dark" }}
				</a>
				{{ theme:image file="unify/demo.jpg" class="logo logo-dark" style="display:none" }}
			</div>
			<div class="visible-sm visible-xs">
				<div class="module widget-handle mobile-toggle right visible-sm visible-xs">
					<i class="ti-menu"></i>
				</div>
				<?php if(!is_logged_in()) { ?>
					<a href="<?php echo base_url().'login'?>" class="login-xs right pull-right">Login</a>
				<?php } else { ?>
					<a href="<?php echo base_url().'users/view'?>" class="login-xs right pull-right">Member Area</a>
					<?php } ?>
				</div>

				<div class="module-group right">
					<div class="module left">
						<ul class="menu">
							<li><a class="menu_item apa" href="#apa" data-slug="apa">Praesent sed dapibus</a></li>
							<li><a class="menu_item nilai" href="#nilai" data-slug="nilai">Nam pretium</a></li>
							<?php if(!is_logged_in()) { ?>
								<li><a href="<?php echo base_url().'login?redirect_to=commerce/elearning'?>" class="btn btn-red red-hide">Login</a></li>
							<?php } else { ?>
								<li><a href="<?php echo base_url().'users/view'?>" class="btn btn-red red-hide">Member Area</a></li>
							<?php } ?>
						</ul>
					</div>
				</div>

			</div>
		</nav>
	</div>

	<div class="main-container">
		<div class="top-before-masterhead clearfix"></div>
		<section>
			<div class="container">
				<div class="row v-align-children">
					<div class="col-md-6 col-sm-6 text-center mb-xs-24">
						<div class="embed-video-container embed-responsive embed-responsive-16by9">
							<!--<div id="player" style="margin-top:30px;"></div>-->
							<iframe width="560" height="315" class="embed-responsive-item" src="https://www.youtube.com/embed/wy-9gEJt6Mc?controls=1&showinfo=0&rel=0" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-md-offset-1 text-masterhead">

						<h4>Lorem Ipsum</h4>
						<h3>Neque porro quisquam</h3>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec urna facilisis, tristique tortor vestibulum, scelerisque nulla. Sed dapibus turpis id felis facilisis accumsan. Donec ac turpis aliquam eros pretium imperdiet.
						</p>

						<p>Praesent vitae massa quis lectus venenatis maximus a nec nunc. Quisque sollicitudin pulvinar libero ac gravida. Duis aliquet velit sit amet enim vestibulum congue. Nullam a commodo neque. Donec sit amet mauris placerat, porttitor eros eu, ullamcorper arcu. </p>

						<div class="btn-daftar-top">
							<a href="#" class="btn btn-daftar-top col-sm-5 col-xs-12">
								<?php if(is_logged_in()) { ?>
									Pesan
								<?php } else { ?>
									Daftar
									<?php } ?>
							</a>
							<a class="menu_item latar-belakang btn btn-red col-sm-5 col-xs-12" href="#nilai" data-slug="latar-belakang">Pelajari</a>
						</div>

					</div>
				</div>

			</div>

		</section>
		<section id="latar-belakang" class="bg-primary" data-slug="nilai">
			<div class="container">
				<div class="row" id="latar-belakang">
					<div class="col-md-offset-1 text-right text-left-xs col-sm-5 col-md-5">
						<h1 class="uppercase mb24 bold italic">Lorem Ipsum</h1>

						<hr class="visible-xs">
					</div>
					<div class="col-md-5 col-sm-7">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec urna facilisis, tristique tortor vestibulum, scelerisque nulla. Sed dapibus turpis id felis facilisis accumsan. Donec ac turpis aliquam eros pretium imperdiet.
						</p>

						<p>
							Praesent vitae massa quis lectus venenatis maximus a nec nunc. Quisque sollicitudin pulvinar libero ac gravida. Duis aliquet velit sit amet enim vestibulum congue. Nullam a commodo neque. Donec sit amet mauris placerat, porttitor eros eu, ullamcorper arcu. 
						</p>

						<p>
							Sed quis finibus metus. Maecenas pretium dapibus augue. Suspendisse luctus est nec nibh vulputate rutrum. Duis suscipit sapien ac eros vulputate, vel bibendum nisl feugiat. Nulla porta laoreet mi, ut tincidunt turpis finibus vitae.
						</p>

					</div>
				</div>
			</div>
		</section>

		<section class="bg-dark">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 text-center">
						<h3 class="uppercase color-primary mb40 mb-xs-24 bold">Praesent sed dapibus mauris</h3>
						<p class="lead">Praesent sed dapibus mauris, sit amet malesuada lorem. Aenean orci mi, vehicula non metus in, eleifend auctor sapien. Duis ullamcorper, elit sit amet hendrerit porttitor, odio ipsum tincidunt velit,
						</p>
						<div class="box-contactus-ldp">
							<h4>Contact Us :</h4>
							<div class="row">
								<div class="col-sm-12">
									<i class="fa fa-mobile-phone fa-2x"></i>&nbsp;
									<p>No HP/WA : 081X-XXXX-XXXX</p>
								</div>
							</div>
						</div>
						<div class="button-red-bottom">
							<a href="#" class="btn btn-signup">
								<?php if(is_logged_in()) { ?>
									Pesan
								<?php } else { ?>
									Daftar
								<?php } ?>
							</a>
						</div>
					</div>
				</div>

			</div>

		</section>
	</div>

	{{ template:partial file="new_footer" }}

	<div class="modal fade" id="affiliate-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Menjadi Affiliate</h4>
				</div>
				<div id="affiliate-body">

				</div>
				<script type="text/javascript">
					$(function(){
						$('#affiliate-modal').on('show.bs.modal', function (e) {
							$('#affiliate-body').html('<div class="loader"><img src="{{ url:site }}{{ theme:path }}/img/loading.gif"></div>');
							$.ajax({
								url : '<?php echo base_url(); ?>commerce/become_affiliate',
								type : 'GET',
								success : function(rst) {
									$('#affiliate-body').html(rst);
								}
							});
						});
					});

					function check_user(redirect_to) {
						$.ajax({
							url : '<?php echo base_url(); ?>users/already_login',
							type : 'GET',
							dataType : 'JSON',
							success : function(rst) {
								if(rst.status == 'success') {
									$('#affiliate-body').load('<?php echo base_url(); ?>'+redirect_to);
								} else {
									$('#affiliate-body').load('<?php echo base_url(); ?>login?redirect='+redirect_to);
								}
							}
						});
					}

					$(document).ready(function() {

						$(document).on('click','#be_affiliate',function() {
							$('#affiliate-body .alert').remove();
							$('#affiliate-body .modal-body').append('<div class="loader"><img src="{{ url:site }}{{ theme:path }}/img/loading.gif"></div>');
							if($('input[name=agree]').is(':checked')) {
								var agree = 1;
							} else {
								var agree = '';
							}
							$.ajax({
								url : '<?php echo base_url(); ?>commerce/become_affiliate',
								data : 'agree='+agree,
								type : 'POST',
								dataType : 'JSON',
								success : function (rst) {
									$('#affiliate-body .modal-body .loader').remove();
									if(rst.status=='success') {
										$('#affiliate-body').html('<div class="modal-body">Selamat, anda berhasil mendaftar sebagai affiliate kami. Silahkan cek email anda untuk informasi lebih lanjut.</div>');
									} else {
										$('.modal-body').append('<div class="alert alert-danger">'+rst.message+'</div>');
									}
								}
							})
						});
					});
				</script>
			</div>
		</div>
	</div>