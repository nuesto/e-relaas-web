<div class="tab-v1">
	<ul class="nav nav-justified nav-tabs">
		<li class="<?php echo ($referer == '') ? 'active' : ''; ?>"><a data-toggle="tab" href="#profile">Edit Profile</a></li>
		<li class="<?php echo ($referer == 'change_password') ? 'active' : ''; ?>"><a data-toggle="tab" href="#passwordTab">Change Password</a></li>
	</ul>
	<div class="tab-content">
		<div id="profile" class="profile-edit tab-pane fade <?php echo ($referer == '') ? 'in active' : ''; ?>">
			<h2 class="heading-md">Edit your profile</h2>
			<br />
			<?php if($referer == '') { ?>
			{{ theme:partial name='notices' }}
			<?php } ?>
			<form method="post" class="sky-form" id="sky-form4" action="<?php echo base_url().'users/update'; ?>">
				<dl class="dl-horizontal">
					<dt>Nama Lengkap</dt>
					<dd>
						<section>
							<label class="input">
								<?php
								$value = $user_profile->display_name;
								if(array_key_exists('display_name', $post_data)) {
									$value = $post_data['display_name'];
								}
								?>
								<input type="text" placeholder="Nama Lengkap" name="display_name" value="<?php echo $value; ?>">
								<b class="tooltip tooltip-bottom-right">Nama Lengkap</b>
							</label>
						</section>
					</dd>

					<dt>Email</dt>
					<dd>
						<section>
							<label class="input">
								<?php
								$value = $user_profile->email;
								if(array_key_exists('email', $post_data)) {
									$value = $post_data['email'];
								}
								?>
								<input class="disabled" type="email" disabled="" placeholder="Email" name="email" value="<?php echo $value; ?>">
								<b class="tooltip tooltip-bottom-right">Email</b>
							</label>
						</section>
					</dd>
					
				</dl>

				<a href="<?php echo base_url().'users/view'; ?>"><button type="button" class="btn-u btn-u-default" type="reset">Cancel</button></a>
				<button class="btn-u" type="submit">Save Changes</button>
			</form>

		</div>

		<div id="passwordTab" class="profile-edit tab-pane fade <?php echo ($referer == 'change_password') ? 'in active' : ''; ?>">
			<h2 class="heading-md">Pengaturan Keamanan</h2>
			<?php if($referer == 'change_password') { ?>
			{{ theme:partial name='notices' }}
			<?php } ?>
			<br />
			<form method="post" class="sky-form" id="sky-form4" action="<?php echo base_url().'users/change_password'; ?>">
				<dl class="dl-horizontal">
					<dt><?php echo lang('global:email'); ?></dt>
					<dd>
						<section>
							<label class="input">
								<i class="icon-append fa fa-envelope"></i>
								<input type="email" placeholder="Email address" name="email">
								<b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
							</label>
						</section>
					</dd>
					<dt><?php echo lang('user:old_password_label'); ?></dt>
					<dd>
						<section>
							<label class="input">
								<i class="icon-append fa fa-lock"></i>
								<input type="password" id="password" name="old_password" placeholder="<?php echo lang('user:old_password_label'); ?>">
								<b class="tooltip tooltip-bottom-right">Don't forget your password</b>
							</label>
						</section>
					</dd>
					<dt><?php echo lang('user:new_password_label'); ?></dt>
					<dd>
						<section>
							<label class="input">
								<i class="icon-append fa fa-lock"></i>
								<input type="password" id="password" name="new_password" placeholder="<?php echo lang('user:new_password_label'); ?>">
								<b class="tooltip tooltip-bottom-right">Don't forget your password</b>
							</label>
						</section>
					</dd>
					<dt><?php echo lang('global:re-password'); ?></dt>
					<dd>
						<section>
							<label class="input">
								<i class="icon-append fa fa-lock"></i>
								<input type="password" name="re_password" placeholder="<?php echo lang('global:re-password'); ?>">
								<b class="tooltip tooltip-bottom-right">Don't forget your password</b>
							</label>
						</section>
					</dd>
				</dl>

				<button type="button" class="btn-u btn-u-default">Cancel</button>
				<button class="btn-u" type="submit">Save Changes</button>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#provinsi').change(function() {
		var id_provinsi = $(this).val();
		$('#kota').html('<option value="">-- Sedang Mengambil Data --</option>');
		$.getJSON('<?php echo base_url(); ?>location/kota/get_kota_by_provinsi/'+id_provinsi, function(result) {
			var choose = '-- Pilih Kota --';
			if(result.length==0) {
				choose = '-- Tidak Ada Kota --'
			}
			$('#kota').html('<option value="">'+choose+'</option>');
			$.each(result, function(key,value) {
				$('#kota').append('<option value="'+value.id+'">'+value.nama+'</option>');
			});
		});
	});

	setTimeout(function() {
		$('.alert-success').fadeOut('slow');
	},2000);
</script>