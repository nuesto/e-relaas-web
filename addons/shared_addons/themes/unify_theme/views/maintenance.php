<style type="text/css">
	.loader {
		padding: 20px;
		text-align: center;
	}
	.btn-close-jwl{
		margin-top: -24px;
	}
	@media (max-width:767px) {
		.btn-close-jwl{
			width: 100%;
		}
	}
	.btn-jwl{
		background: #ad1129;
		color: #fff !important;
		border: none;
		margin-right: -5px;
	}
	@media (max-width:767px) {
		.btn-jwl{
			width: 100%;
		}
		.top-ku{
			margin-left: 0 !important;
		}

	}
</style>
<div class="nav-container">
	<nav class="nav-fixed fixed scrolled">
		<div class="nav-bar">
			<div class="module left">
				<a href="<?php echo base_url(); ?>">
					{{ theme:image file="unify/logo-duakodikartika-com.png" class="logo logo-dark" }}
				</a>
			</div>
			<div class="visible-sm visible-xs">
				<div class="module widget-handle mobile-toggle right visible-sm visible-xs">
					<i class="ti-menu"></i>
				</div>
				<?php if(!is_logged_in()) { ?>
					<a href="<?php echo base_url().'login'?>" class="login-xs right pull-right">Login</a>
					<?php } else { ?>
						<a href="<?php echo base_url().'users/view'?>" class="login-xs right pull-right">Member Area</a>
						<?php } ?>
					</div>

					<div class="module-group right">
						<div class="module left">
							<ul class="menu">
								<li><a class="menu_item apa" href="#apa" data-slug="apa">Apa yang Dipelajari</a></li>
								<li><a class="menu_item nilai" href="#nilai" data-slug="nilai">Nilai Investasi</a></li>
								<?php if(!is_logged_in()) { ?>
									<li><a href="<?php echo base_url().'login'?>" class="btn btn-red red-hide">Login</a></li>
									<?php } else { ?>
										<li><a href="<?php echo base_url().'users/view'?>" class="btn btn-red red-hide">Member Area</a></li>
										<?php } ?>
									</ul>
								</div>
							</div>

						</div>
					</nav>
				</div>

				<div class="main-container">
					<div class="top-before-masterhead clearfix" style="margin-top:-20px !important;"></div>
					<section>
						<div class="container">
							<div class="row v-align-children">
								<div class="col-md-12 col-sm-12 text-masterhead">
									<center>
										<h4 style="margin-bottom:10px;">Assalamu'alaikum...</h4>
										<p>
											Terima Kasih telah mengunjungi laman sekolah bisnis<br>DuaKodiKartika.com. 
										</p>
										<p>
											Kami sedang break transisi masa pendaftaran dari Harga Diskon 70%<br>ke harga Diskon 60% pada tanggal 6 dan 7 agustus 2016. 											
										</p>
										<p>
											Dalam 2 hari ini, web ini hanya dapat melayani akses member premium dan<br>
											dashboard Affiliate.
										</p>
										<p>
											Kepada Anda yang berniat untuk mendaftar, kami akan kembali<br>
											layanan pendaftaran pada Senin, 8 Agustus jam 10.00 WIB.
										</p>
										<p>
											Kami persilakan Anda untuk mengisi guest list terlebih dahulu, agar<br> kami dapat memfollow up Anda saat layanan kami sudah siap.
										</p>
										<p>
											Biaya pendaftaran untuk masa registrasi 8 Agustus 2016 sd 5<br>
											September 2016 adalah Rp 198.000,00 .
										</p>
										<p>
											Terima Kasih
										</p>
										<p>
											Manajemen, <br>
											Sekolah Bisnis DuaKodiKartika
										</p>
									</center>
									<div class="row">
										<div class="col-sm-12">
											<div class="text-center">
												<div class="countdown mb40" data-date="<?php echo date('Y/m/d H:i',strtotime($end_close_date)); ?>" style="color:#000;"></div>
											</div>
										</div>
									</div>

									<div class="btn-masterhead-setting" style="max-width:500px; margin: 0 auto;">
										<div class="btn-daftar-top top-ku" style="margin-left:60px;">
											<a href="#" class="btn btn-daftar-top col-sm-5 col-xs-12" data-toggle="modal" data-target="#myModal">
												Join Waiting List
											</a>
											<a class="menu_item latar-belakang btn btn-red col-sm-5 col-xs-12" href="#nilai" data-slug="latar-belakang">Pelajari</a>
										</div>
									</div>
								</div>
							</div>

						</div>

					</section><section id="latar-belakang" class="bg-primary" data-slug="nilai">
					<div class="container">
						<div class="row" id="latar-belakang">
							<div class="col-md-offset-1 text-right text-left-xs col-sm-5 col-md-5">
								<h1 class="uppercase mb24 bold italic">LATAR BELAKANG</h1>

								<hr class="visible-xs">
							</div>
							<div class="col-md-5 col-sm-7">
								<p>
									Pada awal 2015, Saya meluncurkan buku Dua Kodi Kartika. Sebuah buku yang berisi pelajaran bisnis yang Saya dapatkan dari didikan pemilik KeKe Busana : Ibu Ika Kartika. 
								</p>

								<p>
									Sejak meluncurnya buku tersebut, Saya mulai sering berbagi ke komunitas-komunitas bisnis yang ada. Namun karena sifatnya yang insidental dan tidak reguler, pengajaran bisnis pun tidak dapat menyentuh seluruh aspek dan cenderung bersifat umum.
								</p>

								<p>
									Dari permasalahan tersebut, Saya dan KeKe kemudian meluncurkan Sekolah Bisnis duakodikartika.com. kami berharap, dengan platform edukasi ini, para pebisnis akan mendapatkan pembelajaran yang komprehensif tentang bagaimana mengelola dan membangun bisnis.
								</p>

							</div>
						</div>
					</div>

				</div>

			</section>

			<section class="bg-secondary">
				<div class="container">

					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<h1 class="thin">Apa itu Sekolah Bisnis DuaKodiKartika.com ?</h1>

							<p class="lead">Sekolah Bisnis duakodikartika.com adalah sekolah bisnis berbasis online dengan media video sebagai konten pembelajaran utama. 
								<br>Mulai 1 Agustus 2016, kami mengupload video pembelajaran sebanyak 3 episode setiap pekannya. Video tersebut adalah video eksklusif yang hanya dapar dinikmati oleh member dari Sekolah Bisnis. 
								<br>
								<br>Total Video yang kami siapkan adalah 99 episode atau setara dengan pembelajaran selama 33 pekan sejak tanggal 1 Agustus 2016. 
								<br>Selain Video, kami juga menyediakan ruang diskusi dua arah di FB Group tertutup dan juga kanal group Telegram.
							</p>

						</div>

						<div class="col-md-6 col-sm-6 col-xs-12 mb-xs-0">
							<div class="right-text-one">
								<div class="row">
									<div class="col-sm-12 mb-20">
										<h1 class="large color-primary mb0 top-xs">Online</h1>
										<h5 class="color-primary mb-xs-0" style="margin-bottom:50px;">Sekolah bisnis berbasis online</h5>
									</div>
									<div class="col-sm-12 mb-20">
										<h1 class="large color-primary mb0">99 Video</h1>
										<h5 class="color-primary mb60 mb-xs-0" style="margin-bottom:50px;">3 episode per pekan selama 33 pekan</h5>
									</div>
									<div class="col-sm-12">
										<h1 class="large color-primary mb0">Eksklusif</h1>
										<h5 class="color-primary mb0">ruang diskusi 2 arah khusus bagi member</h5>
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>

			</section><section id="apa" data-slug="apa">
			<div class="container">

				<div class="box-howit-work">
					<h2>APA SAJA YANG DIPELAJARI?</h2>
					<p>
						Topik yang akan dipelajari dalam Sekolah Bisnis duakodikartika.com adalah hampir semua hal tentang bisnis kecuali finansial.
						Untuk pembelajaran detail tentang finansial, kami telah bekerjasama dengan Billionaire Coach di www.MelekFinansial.co
						Sekolah Bisnis duakodikartika.com menitik beratkan pada topik (Topik-topik dan tema pembalajaran akan terus berkembang dan tidak terpaku.) :
					</p>
				</div>

				<div class="box-item">
					<div class="row">
						<div class="col-sm-3">
							<div class="feature feature-1 boxeds">
								<div class="text-center">
									<i class="ti-layout-grid2 icon"></i>
									<h4>Substansi Dasar Bisnis</h4>
								</div>

							</div>

						</div>
						<div class="col-sm-3">
							<div class="feature feature-1 boxeds">
								<div class="text-center">
									<i class="ti-star icon"></i>
									<h4>Business and Personal Mastery</h4>
								</div>

							</div>

						</div>
						<div class="col-sm-3">
							<div class="feature feature-1 boxeds">
								<div class="text-center">
									<i class="ti-briefcase icon"></i>
									<h4>Manajemen Organisasi Bisnis</h4>
								</div>

							</div>

						</div><div class="col-sm-3">
						<div class="feature feature-1 boxeds">
							<div class="text-center">
								<i class="ti-stats-up icon"></i>
								<h4>Tools atau alat-alat pertumbuhan</h4>
							</div>

						</div>

					</div><div class="col-sm-3">
					<div class="feature feature-1 boxeds">
						<div class="text-center">
							<i class="ti-user icon"></i>
							<h4>People Management</h4>
						</div>

					</div>

				</div>

				<div class="col-sm-3">
					<div class="feature feature-1 boxeds">
						<div class="text-center">
							<i class="ti-target icon"></i>
							<h4>Marketing and Brand</h4>
						</div>

					</div>

				</div>
				<div class="col-sm-3">
					<div class="feature feature-1 boxeds">
						<div class="text-center">
							<i class="ti-settings icon"></i>
							<h4>Business Operation</h4>
						</div>

					</div>
				</div>
			</div>
		</div>

	</div>

</section><section class="bg-secondary">
<div class="container">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 text-center">
			<h3 class="uppercase color-primary mb40 mb-xs-24 bold">KEUNTUNGAN TAMBAHAN</h3>
			<p class="lead">
				Selain mendapatkan layanan edukasi, setiap Member Sekolah duakodikartika.com berhak menjadi Affiliate dari program sekolah bisnis ini.
			</p>
			<p class="text-center" style="margin-top:40px;"><span class="btn btn-affilate" data-toggle="modal" data-target="#affiliate-modal">DAFTAR MENJADI AFFILIATE</span></p>
		</div>
	</div>

</div>

</section>
<section id="nilai" data-slug="nilai">
	<div class="container">
		<div class="row v-align-childrens">
			<div class="col-md-7 col-sm-6">
				<h3 class="nim">NILAI INVESTASI MEMBERSHIP</h3>
				<p class="lead mb20">
					Untuk masa membership dua tahun dan berhak atas 99 video pembelajaran bisnis.
					Bagi sahabat yang mendaftarkan diri sebelum 6 September 2016, sahabat berkesempatan untuk mendapatkan diskon sebesar 60%.
				</p>


			</div>
			<div class="col-md-4 col-sm-6">
				<div class="pricing-table pt-1 text-center emphasis">
					<h5 class="uppercase">Nilai Investasi Investasi Untuk 2 Tahun</h5>
					<span class="prices"><s>Rp 495.000&nbsp;</s></span>
					<span class="prices-new">Rp 198.000&nbsp;</span>
					<p class="lead">untuk pendaftaran sebelum 6 September 2016.</p>
					<a class="btn btn-white btn-lg" href="#" data-toggle="modal" data-target="#myModal">
						Join Waiting List
					</a>

				</div>

			</div>
		</div>

	</div>
</section>


<!-- Section Buku & Tiket-->

<section class="bg-secondary">
	<div class="container">
		<div class="row mb64 mb-xs-24">
			<div class="col-sm-12 col-md-10 col-md-offset-1 text-center">
				<h3>Miliki produk kami yang lain :</h3>
			</div>
		</div>

		<div class="row">
			<?php if($default_buku != NULL) { ?>
				<div class="col-sm-6 text-center">
					<div class="feature">
						<div class="text-center">
							<center>
								<div class="box-img-resize">
									<img src="<?php echo base_url().'uploads/default/files/'.$default_buku['filename']; ?>" class="image-product mb40 mb-xs-24 inline-block">
								</div>
							</center>
							<div class="btn-action">
								<?php if($default_buku['id']==0) { ?>
									<a href="<?php echo base_url().'commerce/cart/add?product='.$default_buku['id']; ?>"  class="btn btn-affilate uppercase" >Beli Buku</a>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<?php if($default_ticket != NULL) { ?>
						<div class="col-sm-6 text-center">
							<div class="feature">
								<div class="text-center">
									<center>
										<div class="box-img-resize">
											<img src="<?php echo base_url().'uploads/default/files/'.$default_ticket['filename']; ?>" class="image-product mb40 mb-xs-24 inline-block">
										</div>
									</center>
									<div class="btn-action">
										<?php if($default_buku['id']==0) { ?>
											<a href="<?php echo base_url().'commerce/cart/add?product='.$default_ticket['id']; ?>"  class="btn btn-affilate uppercase">Pesan Tiket</a>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</section>

				<!-- End Section buku & Tiket -->


				<section class="bg-dark">
					<div class="container">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1 text-center">
								<h3 class="uppercase color-primary mb40 mb-xs-24 bold">MEKANISME PENDAFTARAN</h3>
								<p class="lead">Ikuti alur pendaftaran online yang ada, lakukan transaksi senilai jumlah yang ditagihkan di invoice dan dapatkan usernama-password membership Sekolah Bisnis duakodikartika.com.<br>Selamat bergabung</p>
								<div class="box-contactus-ldp">
									<h4>Contact Us :</h4>
									<div class="row">
										<div class="col-sm-12">
											<i class="fa fa-mobile-phone fa-2x"></i>&nbsp;
											<p>No HP/WA : 081572214566</p>
										</div>
									</div>
								</div>
								<div class="button-red-bottom">
									<a href="" class="btn btn-signup" data-toggle="modal" data-target="#myModal">
										Join Waiting List
									</a>
								</div>
							</div>
						</div>

					</div>

				</section>
			</div>

			{{ template:partial file="new_footer" }}

			<div class="modal fade" id="affiliate-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Menjadi Affiliate</h4>
						</div>
						<div id="affiliate-body">

						</div>
						<script type="text/javascript">
							$(function(){
								$('#affiliate-modal').on('show.bs.modal', function (e) {
									$('#affiliate-body').html('<div class="loader"><img src="{{ url:site }}{{ theme:path }}/img/loading.gif"></div>');
									$.ajax({
										url : '<?php echo base_url(); ?>commerce/become_affiliate',
										type : 'GET',
										success : function(rst) {
											$('#affiliate-body').html(rst);
										}
									});
								});
							});

							function check_user(redirect_to) {
								$.ajax({
									url : '<?php echo base_url(); ?>users/already_login',
									type : 'GET',
									dataType : 'JSON',
									success : function(rst) {
										if(rst.status == 'success') {
											$('#affiliate-body').load('<?php echo base_url(); ?>'+redirect_to);
										} else {
											$('#affiliate-body').load('<?php echo base_url(); ?>login?redirect='+redirect_to);
										}
									}
								});
							}

							$(document).ready(function() {

								$(document).on('click','#be_affiliate',function() {
									$('#affiliate-body .alert').remove();
									$('#affiliate-body .modal-body').append('<div class="loader"><img src="{{ url:site }}{{ theme:path }}/img/loading.gif"></div>');
									if($('input[name=agree]').is(':checked')) {
										var agree = 1;
									} else {
										var agree = '';
									}
									$.ajax({
										url : '<?php echo base_url(); ?>commerce/become_affiliate',
										data : 'agree='+agree,
										type : 'POST',
										dataType : 'JSON',
										success : function (rst) {
											$('#affiliate-body .modal-body .loader').remove();
											if(rst.status=='success') {
												$('#affiliate-body').html('<div class="modal-body">Selamat, anda berhasil mendaftar sebagai affiliate kami. Silahkan cek email anda untuk informasi lebih lanjut.</div>');
											} else {
												$('.modal-body').append('<div class="alert alert-danger">'+rst.message+'</div>');
											}
										}
									})
								});
							});
						</script>
					</div>
				</div>
			</div>



			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<form id="form-join-leads" class="sky-form" method="post" action="<?php echo base_url().'landing_page/leads/join'; ?>">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title text-center" id="myModalLabel">Untuk Join Waiting List, cukup Anda Masukkan Nama Lengkap dan Email Aktif Anda dibawah ini:</h4>
							</div>
							<div class="modal-body">
								<div id="input-form">
									<div class="form-group">
										<label for="email">Email Aktif Anda</label>
										<input name="email" type="email" class="form-control" id="email">
									</div>

									<div class="form-group">
										<label for="full_name">Nama Lengkap Anda</label>
										<input name="full_name" type="text" class="form-control" style="background-color:#fff; border:1px solid #ddd; height:40px;">
									</div>

									<div class="form-group">
										<label for="mobile_phone">No Hp</label>
										<input name="mobile_phone" type="text" class="form-control" style="background-color:#fff; border:1px solid #ddd; height:40px;">
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-daftar-top" id="btn-join-leads" style="width:auto;height:auto;line-height: 36px;">Join Waiting List</button>
								<button type="button" class="btn btn-default btn-close-jwl" data-dismiss="modal">Close</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<script src="http://malsup.github.com/jquery.form.js"></script> 
			<script type="text/javascript">
				$(document).ready(function() {
					$('#form-join-leads').ajaxForm({
						dataType: 'json',
						beforeSubmit: function(){
				// remove alert
				$('#form-join-leads .alert').remove();

				// disable button
				$('#btn-join-leads').attr("disabled", true);

				// display loader image
				$('#form-join-leads .modal-body').prepend('<div class="loader"><img src="{{ url:site }}{{ theme:path }}/img/loading.gif"></div>');

				// hide form
				$('#form-join-leads #input-form').hide();
			},
			success: function(retval){
				// enable button
				$('#btn-join-leads').removeAttr("disabled");

				// remove loader image
				$('#form-join-leads .modal-body .loader').remove();

				// display result
				if(retval.status=='failed') {
					$('#form-join-leads .modal-body').prepend('<div class="alert alert-danger">'+retval.messages+'</div>');
					$('#form-join-leads #input-form').show();
				} else {
					$('#form-join-leads .modal-body').prepend('<div class="alert alert-success">'+retval.messages+'</div>');
					$('#form-join-leads #input-form').hide();
					$('#form-join-leads button[type=submit]').attr('disabled',true);
				}
			}
		});
				});
			</script>
