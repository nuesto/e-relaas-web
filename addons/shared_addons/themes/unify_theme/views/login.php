<style type="text/css">
	.show-password, .hide-password {
		cursor:pointer;
	}
	.loader-container-right {
		line-height: 30px;
	}
	#register {
		margin-left: -30px;
		padding-top: 8px;
		display: inline-block;
	}
</style>

<script src="http://malsup.github.com/jquery.form.js"></script> 

<div style="padding:5%;" id="form-container" class="row">

	<!-- LOGIN PASSWORD FORM -->

	<form name="login" id="form-login" method="post" action="<?php echo site_url('admin/login'); ?>">
		<div class="login-form sky-form" id="login-form">
			<?php if($this->session->flashdata('reset_password') != '') {
				echo $this->session->flashdata('reset_password');
				} ?>
			<?php if($this->template->error_string != '') {
				echo '<div class="alert alert-danger">'.$this->template->error_string.'</div>';
				} ?>
			<?php if($this->input->get('login_required') == '1') {
				echo '<div class="alert alert-danger">Silahkan Login Terlebih Dahulu</div>';
				} ?>
			<h3></h3>
			<div class="form-group">
				<label class="" for="form-email">Email</label>
				<input type="text" name="email" placeholder="" class="form-email form-control" id="form-email">
			</div>
			<div class="form-group">
				<label class="" for="form-password">Password</label>
				<label class="input">
					<i class="icon-append show-password" style="width:50px;">Show</i>
					<input type="password" name="password" placeholder="" class="form-password form-control" id="form-password">
				</label>
			</div>
			<div class="form-group">
				<input type="submit" class="col-xs-4 btn btn-u btn-login" id="btn-login" value="Login" />
				<a id="register" href="#">Belum memiliki akun? Daftar disini</a>
				<div class="col-xs-1 loader-container loader-container-right"></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<a id="forgot-pass" href="#reset-pass-form-container">Lupa password</a>
				</div>
			</div>
		</div>
	</form>

	<!-- RESET PASSWORD FORM -->

	<form action="<?php echo base_url(); ?>users/reset_pass" method="post" accept-charset="utf-8" id="form-reset-pass" style="display:none;">
		<div class="login-form sky-form" id="login-form">
			<h3></h3>

			<div class="form-group">
				<label class="" for="form-email">Masukkan Email Anda</label>
				<input type="text" name="email" placeholder="" class="form-email form-control" id="form-email">
			</div>

			<div class="form-group">
				<input type="submit" class="col-xs-6 btn btn-u" id="btn-forgot-pass" value="Reset Password" />
				<div class="col-xs-1 loader-container loader-container-right"></div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<a id="back-login" href="#back-login">Kembali ke Form Login</a>
				</div>
			</div>

		</div>
	</form>

	<!-- REGISTER FORM -->

	<form name="register" id="form-register" method="post" action="<?php echo site_url('register'); ?>" style="display: none">
		<div class="login-form sky-form" id="register-form">
			<?php if($this->session->flashdata('reset_password') != '') {
				echo $this->session->flashdata('reset_password');
				} ?>
			<?php if($this->template->error_string != '') {
				echo '<div class="alert alert-danger">'.$this->template->error_string.'</div>';
				} ?>
			<?php if($this->input->get('login_required') == '1') {
				echo '<div class="alert alert-danger">Silahkan Login Terlebih Dahulu</div>';
				} ?>
			<h3></h3>
			<div class="form-group">
				<label class="" for="form-displayname">Nama Lengkap</label>
				<?php
				$value = NULL;
				if($this->input->post('display_name')) {
					$value = $this->input->post('display_name');
				}
				?>
				<input type="text" name="display_name" placeholder="" class="form-displayname form-control" id="form-displayname" value="<?php echo $value; ?>">
			</div>

			<div class="form-group">
				<label class="" for="form-email">Email</label>
				<?php
				$value = NULL;
				if($this->input->post('email')) {
					$value = $this->input->post('email');
				}
				?>
				<input type="text" name="email" placeholder="" class="form-email form-control" id="form-email" value="<?php echo $value; ?>">

				<?php echo form_input('d0ntf1llth1s1n', ' ', 'class="default-form" style="display:none"') ?>
			</div>
			
			<div class="form-group">
				<label class="" for="form-password">Password</label>
				<label class="input">
					<i class="icon-append show-password" style="width:50px;">Show</i>
					<input type="password" name="password" placeholder="" class="form-password form-control" id="form-password">
				</label>
			</div>

			<div class="form-group">
				<label class="" for="form-password">Retype Password</label>
				<label class="input">
					<i class="icon-append show-password" style="width:50px;">Show</i>
					<input type="password" name="re-password" placeholder="" class="form-password form-control" id="form-password">
				</label>
			</div>
			
			<div class="form-group">
				<input type="submit" class="col-xs-4 btn btn-u btn-login" id="btn-register" value="Register" />
				<div class="col-xs-1 loader-container loader-container-right"></div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<a id="back-login" href="#back-login">Kembali ke Form Login</a>
				</div>
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		if(window.location.href.indexOf("register") > -1) {
			$('#form-login').hide();
			$('#form-reset-pass').hide();
			$('#form-register').fadeIn('slow');
		}
	});

	$(document).on('click','#forgot-pass', function(e) {
		e.preventDefault();
		$('#form-login').hide();
		$('#form-reset-pass').fadeIn('slow');
	});

	$(document).on('click','#register', function(e) {
		e.preventDefault();
		$('#form-login').hide();
		$('#form-reset-pass').hide();
		$('#form-register').fadeIn('slow');
	});

	$(document).on('click','#back-login', function(e) {
		e.preventDefault();
		$('#form-reset-pass').hide();
		$('#form-register').hide();
		$('#form-login').fadeIn('slow');
	});

	// Login form submit

	$('#form-login').ajaxForm({
		dataType: 'json',
		beforeSubmit: function(){
			// disable button
			$('#btn-login').attr("disabled", true);

			// display loader image
			$('#form-login .loader-container').prepend('<div class="loader"><i style="font-size: 30px; vertical-align: top;" class="fa fa-refresh fa-spin"></i></div>');
		},
		success: function(retval){
			// remove loader image
			$('#form-login .loader').remove();

			// enable button
			$('#btn-login').attr("disabled", false);

			// remove alert
			$('#form-login .alert').remove();

			// register succeed
			if(retval['status'] == 'success'){
				// remove alert
				$('#form-login .alert').remove();

				// add success notification
				$('#form-login').prepend('<div class="alert alert-success">Login Berhasil</div>');

				// display loader image in the place of the form
				$('.buyer-info-form').html('<div class="loader"><i style="font-size: 30px; vertical-align: top;" class="fa fa-refresh fa-spin"></i></div>');

				// scroll up
				$('html,body').animate({
					scrollTop: $('#form-container').offset().top
				}, 800);

				// redirect to this page
				window.location.href = '<?php echo site_url($this->input->get('redirect_to')); ?>';

			// register fail
			}else{
				$('#form-login').prepend('<div class="alert alert-danger">'+retval['message']+'</div>');
				$('#btn-login').removeClass('disabled');
				$('html,body').animate({
					scrollTop: $('#form-container').offset().top
				}, 800);
			}
			
		}
	});

	// Reset password form submit

	$('#form-reset-pass').ajaxForm({
		dataType: 'json',
		beforeSubmit: function(){
			// disable button
			$('#btn-reset-pass').attr("disabled", true);

			// display loader image
			$('#form-reset-pass .loader-container').prepend('<div class="loader"><i style="font-size: 30px; vertical-align: top;" class="fa fa-refresh fa-spin"></i></div>');
		},
		success: function(retval){
			// remove loader image
			$('#form-reset-pass .loader').remove();

			// enable button
			$('#btn-reset-pass').attr("disabled", false);

			// remove alert
			$('#form-reset-pass .alert').remove();

			// register succeed
			if(retval['status'] == 'success'){
				// remove alert
				$('#form-reset-pass .alert').remove();

				// add success notification
				$('#form-reset-pass').prepend('<div class="alert alert-success">'+retval['message']+'</div>');

				// scroll up
				$('html,body').animate({
					scrollTop: $('#form-container').offset().top
				}, 800);

			// register fail
			}else{
				$('#form-reset-pass').prepend('<div class="alert alert-danger">'+retval['message']+'</div>');
				$('#btn-reset-pass').removeClass('disabled');
				$('html,body').animate({
					scrollTop: $('#form-container').offset().top
				}, 800);
			}
			
		}
	});

	$(document).on('click','.show-password', function() {
		$(this).text('Hide');
		$(this).removeClass('show-password').addClass('hide-password');
		$(this).closest('label').find('input').attr('type','text');
	});

	$(document).on('click','.hide-password', function() {
		$(this).text('Show');
		$(this).removeClass('hide-password').addClass('show-password');
		$(this).closest('label').find('input').attr('type','password');
	});
</script>