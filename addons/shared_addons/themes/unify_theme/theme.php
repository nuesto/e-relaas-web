<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_Unify_theme extends Theme {

    public $name = 'Unify Theme';
    public $author = 'Nuesto Technology';
    public $author_website = 'http://www.nuesto.id';
    public $website = '';
    public $description = 'Responsive template based on bootstrap';
    public $version = '1.0.0';
    public $options = array();
}

