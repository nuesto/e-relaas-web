<div class="form-group">
	<label>Address</label><br />
	<?php echo form_input('address', $options['address']); ?>
</div>
<div class="form-group">
	<label>Width</label><br />
	<?php echo form_input('width', ($options['width'] != '' ? $options['width'] : '100%')); ?>
</div>
<div class="form-group">
	<label>Height</label><br />
	<?php echo form_input('height', ($options['height'] != '' ? $options['height'] : '400px')); ?>
</div>
<div class="form-group">
	<label>Zoom Level</label><br />
	<?php echo form_input('zoom', ($options['zoom'] != '' ? $options['zoom'] : '16')); ?>
</div>
<div class="form-group">
	<label>Description (optional)</label><br />
	<?php echo form_textarea('description', $options['description'], 'style="width: 100%"'); ?>
</div>