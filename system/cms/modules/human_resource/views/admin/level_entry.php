<div class="page-header">
	<h1><?php echo lang('human_resource:level:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/human_resource/level/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('human_resource:back') ?>
		</a>

		<?php if(group_has_role('human_resource', 'edit_all_level')){ ?>
			<a href="<?php echo site_url('admin/human_resource/level/edit/'.$level['id']); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php } ?>

		<?php if(group_has_role('human_resource', 'delete_all_level')){ ?>
			<a href="<?php echo site_url('admin/human_resource/level/delete/'.$level['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $level['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:nama_level'); ?></div>
		<?php if(isset($level['nama_level'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $level['nama_level']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:ordering_count'); ?></div>
		<?php if(isset($level['ordering_count'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $level['ordering_count']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
</div>