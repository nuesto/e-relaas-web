<div class="page-header">
	<h1><?php echo lang('human_resource:metode_rekrutmen:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/human_resource/metode_rekrutmen/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('human_resource:back') ?>
		</a>

		<?php if(group_has_role('human_resource', 'edit_all_metode_rekrutmen')){ ?>
			<a href="<?php echo site_url('admin/human_resource/metode_rekrutmen/edit/'.$metode_rekrutmen['id']); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php } ?>

		<?php if(group_has_role('human_resource', 'delete_all_metode_rekrutmen')){ ?>
			<a href="<?php echo site_url('admin/human_resource/metode_rekrutmen/delete/'.$metode_rekrutmen['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="row">
		<div class="entry-label col-sm-2">ID</div>
		<div class="entry-value col-sm-8"><?php echo $metode_rekrutmen['id']; ?></div>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:nama_metode'); ?></div>
		<?php if(isset($metode_rekrutmen['nama_metode'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $metode_rekrutmen['nama_metode']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="entry-label col-sm-2"><?php echo lang('human_resource:ordering_count'); ?></div>
		<?php if(isset($metode_rekrutmen['ordering_count'])){ ?>
		<div class="entry-value col-sm-8"><?php echo $metode_rekrutmen['ordering_count']; ?></div>
		<?php }else{ ?>
		<div class="entry-value col-sm-8">-</div>
		<?php } ?>
	</div>
</div>