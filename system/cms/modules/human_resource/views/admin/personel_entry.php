<style type="text/css">
	h3 {
		padding: 0px;
		margin-top: 30px;
		font-size: 24px;
		font-weight: lighter;
		color: #2679B5;
	}
</style>
<div class="page-header">
	<h1><strong><?php echo $personel['nama_lengkap']; ?></strong></h1>
	
	<div class="btn-group content-toolbar">
		<?php if(! group_has_role('human_resource', 'view_all_personel') AND ! group_has_role('human_resource', 'view_own_unit_personel') AND ! group_has_role('human_resource', 'view_own_personel') AND group_has_role('human_resource', 'view_own_user_personel')){ ?>
		<?php } else { ?>
		<a href="<?php echo site_url('admin/human_resource/personel/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('human_resource:personel:back') ?>
		</a>
		<?php } ?>

		<?php if(group_has_role('human_resource', 'edit_all_personel')){ ?>
			<a href="<?php echo site_url('admin/human_resource/personel/edit/'.$personel['id']); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('human_resource:personel:edit') ?>
			</a>
		<?php }elseif(group_has_role('human_resource', 'edit_own_personel') AND $personel['created_by'] == $this->current_user->id){ ?>
			<a href="<?php echo site_url('admin/human_resource/personel/edit/'.$personel['id']); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('human_resource:personel:edit') ?>
			</a>
		<?php }elseif(group_has_role('human_resource', 'edit_own_user_personel') AND $personel['id_user'] == $this->current_user->id){ ?>
			<a href="<?php echo site_url('admin/human_resource/personel/edit/'.$personel['id']); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('human_resource:personel:edit') ?>
			</a>
		<?php } ?>

		<?php if(group_has_role('human_resource', 'delete_all_personel')){ ?>
			<a href="<?php echo site_url('admin/human_resource/personel/delete/'.$personel['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('human_resource', 'delete_own_personel')){ ?>
			<?php if($personel['created_by'] == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/human_resource/personel/delete/'.$personel['id']); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="tabbable tab-link">
		<ul class="nav nav-tabs" id="myTab">
			<li class="<?php echo ($this->uri->segment(4)=='view') ? 'active' : ''; ?>">
				<a href="<?php echo base_url(); ?>admin/human_resource/personel/view/<?php echo $personel['id']; ?>">Detail Personel</a>
			</li>

			<li class="<?php echo ($this->uri->segment(4)=='personel_history') ? 'active' : ''; ?>">
				<a href="<?php echo base_url(); ?>admin/human_resource/personel/personel_history/<?php echo $personel['id']; ?>">History & Catatan</a>
			</li>
		</ul>
	</div>

	
	<div id="detail_personel" class="tab-pane <?php echo (!isset($_GET['display']) OR $_GET['display']!='history') ? 'active' : ''; ?>">
		<h3><?php echo lang('human_resource:personel:data'); ?></h3><hr>
		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:nama_lengkap'); ?></div>
			<?php if(isset($personel['nama_lengkap'])){ ?>
			<div class="entry-value col-sm-8"><?php echo $personel['nama_lengkap']; ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:jenis_kelamin'); ?></div>
			<?php if(isset($personel['jenis_kelamin'])){ ?>
			<div class="entry-value col-sm-8"><?php echo ($personel['jenis_kelamin']=='L') ? 'Laki-laki' : 'Perempuan'; ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:tanggal_lahir'); ?></div>
			<?php if($personel['tanggal_lahir']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo format_date($personel['tanggal_lahir'], 'd-M-Y'); ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<h3><?php echo lang('human_resource:personel:contact'); ?></h3><hr>
		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:handphone'); ?></div>
			<?php if($personel['handphone']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo $personel['handphone']; ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:email'); ?></div>
			<?php if($personel['email']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo $personel['email']; ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<h3><?php echo lang('human_resource:personel:employee'); ?></h3><hr>
		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:nomor_induk'); ?></div>
			<?php if($personel['nomor_induk']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo $personel['nomor_induk']; ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:id_status_pekerja'); ?></div>
			<?php if($personel['id_status_pekerja']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo $personel['nama_status']; ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:id_level'); ?></div>
			<?php if($personel['id_level']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo $personel['nama_level']; ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:tahun_mulai_bekerja'); ?></div>
			<?php if($personel['tahun_mulai_bekerja']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo $personel['tahun_mulai_bekerja']; ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:tahun_mulai_profesional'); ?></div>
			<?php if($personel['tahun_mulai_profesional']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo $personel['tahun_mulai_profesional']; ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:id_metode_rekrutmen'); ?></div>
			<?php if($personel['id_metode_rekrutmen']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo $personel['nama_metode']; ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:id_supervisor'); ?></div>
			<?php if($personel['id_supervisor']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo anchor('admin/human_resource/personel/view/'.$personel['id_supervisor'], $personel['supervisor_nama_lengkap']); ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:id_organization_unit'); ?></div>
			<?php if($personel['id_organization_unit']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo anchor('admin/organization/units/view/'.$personel['id_organization_unit'], $personel['unit_name']); ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<h3><?php echo lang('human_resource:personel:account'); ?></h3><hr>
		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:id_user'); ?></div>
			<?php if($personel['id_user']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo user_displayname($personel['id_user'], true); ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<h3><?php echo lang('human_resource:personel:info'); ?></h3><hr>
		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:created_on'); ?></div>
			<?php if($personel['created_on']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo format_date($personel['created_on'], 'd-m-Y G:i'); ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:updated_on'); ?></div>
			<?php if($personel['updated_on']!=''){ ?>
			<div class="entry-value col-sm-8"><?php echo format_date($personel['updated_on'], 'd-m-Y G:i'); ?></div>
			<?php }else{ ?>
			<div class="entry-value col-sm-8">-</div>
			<?php } ?>
		</div>

		<div class="row">
			<div class="entry-label col-sm-2"><?php echo lang('human_resource:created_by'); ?></div>
			<div class="entry-value col-sm-8"><?php echo user_displayname($personel['created_by'], true); ?></div>
		</div>
	</div>	
	
</div>