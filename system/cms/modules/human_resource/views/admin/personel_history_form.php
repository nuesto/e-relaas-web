<div class="page-header">
	<h1><?php echo lang('human_resource:personel_history:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>

<div class="form-horizontal">
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="description"><?php echo lang('human_resource:description'); ?> <span style="color:red">*</span></label>

		<div class="col-sm-5">
			<?php 
				$value = NULL;
				if($this->input->post('description') != NULL){
					$value = $this->input->post('description');
				}elseif($mode == 'edit'){
					$value = $fields['description'];
				}
			?>
			<textarea rows="6" name="description" class="form-control" id=""><?php echo $value; ?></textarea>
		</div>
	</div>

</div>

<div class="clearfix">
	<div class="col-sm-offset-2 col-sm-9">
		<button type="submit" class="btn btn-sm btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-sm btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>