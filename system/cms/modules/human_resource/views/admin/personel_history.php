<style type="text/css">
	h3 {
		padding: 0px;
		margin: 0px 8px 0px 0px;
		font-size: 24px;
		font-weight: lighter;
		color: #2679B5;
	}
</style>
<div class="page-header">
	<h1><strong><?php echo $personel['nama_lengkap']; ?></strong></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/human_resource/personel/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('human_resource:personel:back') ?>
		</a>

		<?php if(group_has_role('human_resource', 'edit_all_personel')){ ?>
			<a href="<?php echo site_url('admin/human_resource/personel/edit/'.$personel['id']); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('human_resource:personel:edit') ?>
			</a>
		<?php }elseif(group_has_role('human_resource', 'edit_own_personel') AND $personel['created_by'] == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/human_resource/personel/edit/'.$personel['id']); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('human_resource:personel:edit') ?>
				</a>
		<?php }elseif(group_has_role('human_resource', 'edit_own_user_personel') AND $personel['id_user'] == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/human_resource/personel/edit/'.$personel['id']); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('human_resource:personel:edit') ?>
				</a>
		<?php } ?>

		<?php if(group_has_role('human_resource', 'delete_all_personel')){ ?>
			<a href="<?php echo site_url('admin/human_resource/personel/delete/'.$personel['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('human_resource', 'delete_own_personel')){ ?>
			<?php if($personel['created_by'] == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/human_resource/personel/delete/'.$personel['id']); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="tabbable tab-link">
		<ul class="nav nav-tabs" id="myTab">
			<li class="<?php echo ($this->uri->segment(4)=='view') ? 'active' : ''; ?>">
				<a href="<?php echo base_url(); ?>admin/human_resource/personel/view/<?php echo $personel['id']; ?>">Detail Personel</a>
			</li>

			<li class="<?php echo ($this->uri->segment(4)=='personel_history') ? 'active' : ''; ?>">
				<a href="<?php echo base_url(); ?>admin/human_resource/personel/personel_history/<?php echo $personel['id']; ?>">History & Catatan</a>
			</li>
		</ul>
	</div>
	
	<div class="">
		<div id="history">
			<?php if ($personel_history['total'] > 0): ?>
		
			<form>
			<div class="form-group">
				<select name="type">
					<option value="">- All -</option>
					<?php foreach ($personel_history['type'] as $type) {
						
						$selected = ($this->input->get('type') AND $this->input->get('type')==$type['type']) ? 'selected' : '';
							
						echo '<option '.$selected.'>'.$type['type'].'</option>';
					}?>
				</select>
				<input type="hidden" name="display" value="history">
				<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
					<i class="icon-filter"></i>
					<?php echo lang('global:filters'); ?>
				</button>

				<?php if(group_has_role('human_resource', 'edit_all_personel')){ ?>
					<a href="<?php echo site_url('admin/human_resource/personel_history/create/'.$personel['id']); ?>" class="btn btn-xs btn-yellow pull-right">
						<i class="icon-plus"></i>
						<?php echo lang('human_resource:personel_history:new') ?>
					</a>
				<?php }elseif(group_has_role('human_resource', 'edit_own_personel') AND $personel['created_by'] == $this->current_user->id){ ?>
					<a href="<?php echo site_url('admin/human_resource/personel_history/create/'.$personel['id']); ?>" class="btn btn-xs btn-yellow pull-right">
						<i class="icon-plus"></i>
						<?php echo lang('human_resource:personel_history:new') ?>
					</a>
				<?php }elseif(group_has_role('human_resource', 'edit_own_user_personel') AND $personel['id_user'] == $this->current_user->id){ ?>
					<a href="<?php echo site_url('admin/human_resource/personel_history/create/'.$personel['id']); ?>" class="btn btn-xs btn-yellow pull-right">
						<i class="icon-plus"></i>
						<?php echo lang('human_resource:personel_history:new') ?>
					</a>
				<?php } ?>
			</div>
			</form>

			<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>No</th>
						<th style="width:140px"><?php echo lang('human_resource:created_on'); ?></th>
						<th><?php echo lang('human_resource:type'); ?></th>
						<th><?php echo lang('human_resource:description'); ?></th>
						<th><?php echo lang('human_resource:created_by'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$cur_page = (int) $this->uri->segment($pagination_config['uri_segment']);
					if($cur_page != 0){
						$no = $cur_page + 1;
					}else{
						$no = 1;
					}
					?>
					
					<?php foreach ($personel_history['entries'] as $personel_history_entry): ?>
					<tr>
						<td><?php echo $no; $no++; ?></td>
						<td><?php echo format_date($personel_history_entry['created_on'], 'd-M-Y h:i'); ?></td>
						<td><?php echo ($personel_history_entry['type']!='') ? $personel_history_entry['type'] : '-'; ?></td>
						<td><?php echo ($personel_history_entry['description']!='') ? $personel_history_entry['description'] : '-'; ?></td>
						<td><?php echo ($personel_history_entry['created_by']!='') ? user_displayname($personel_history_entry['created_by'], true) : '-'; ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			
			<div class="clearfix">
			<?php echo $personel_history['pagination']; ?>
			</div>
				
			<?php else: ?>
				<div class="well"><?php echo lang('human_resource:personel_history:no_entry'); ?></div>
			<?php endif;?>
		</div>
	</div>
</div>