<div class="page-header">
	<h1>
		<span><?php echo lang('human_resource:personel:'.$mode); ?></span>
	</h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nama_lengkap"><?php echo lang('human_resource:nama_lengkap'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama_lengkap') != NULL){
					$value = $this->input->post('nama_lengkap');
				}elseif($mode == 'edit'){
					$value = $fields['nama_lengkap'];
				}
			?>
			<input name="nama_lengkap" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="jenis_kelamin"><?php echo lang('human_resource:jenis_kelamin'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('jenis_kelamin') != NULL){
					$value = $this->input->post('jenis_kelamin');
				}elseif($mode == 'edit'){
					$value = $fields['jenis_kelamin'];
				}
			?>
			<input name="jenis_kelamin" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="nomor_induk"><?php echo lang('human_resource:nomor_induk'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('nomor_induk') != NULL){
					$value = $this->input->post('nomor_induk');
				}elseif($mode == 'edit'){
					$value = $fields['nomor_induk'];
				}
			?>
			<input name="nomor_induk" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tanggal_lahir"><?php echo lang('human_resource:tanggal_lahir'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('tanggal_lahir') != NULL){
					$value = $this->input->post('tanggal_lahir');
				}elseif($mode == 'edit'){
					$value = $fields['tanggal_lahir'];
				}
			?>
			<input name="tanggal_lahir" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_status_pekerja"><?php echo lang('human_resource:id_status_pekerja'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_status_pekerja') != NULL){
					$value = $this->input->post('id_status_pekerja');
				}elseif($mode == 'edit'){
					$value = $fields['id_status_pekerja'];
				}
			?>
			<input name="id_status_pekerja" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_level"><?php echo lang('human_resource:id_level'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_level') != NULL){
					$value = $this->input->post('id_level');
				}elseif($mode == 'edit'){
					$value = $fields['id_level'];
				}
			?>
			<input name="id_level" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tahun_mulai_bekerja"><?php echo lang('human_resource:tahun_mulai_bekerja'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('tahun_mulai_bekerja') != NULL){
					$value = $this->input->post('tahun_mulai_bekerja');
				}elseif($mode == 'edit'){
					$value = $fields['tahun_mulai_bekerja'];
				}
			?>
			<input name="tahun_mulai_bekerja" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="tahun_mulai_profesional"><?php echo lang('human_resource:tahun_mulai_profesional'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('tahun_mulai_profesional') != NULL){
					$value = $this->input->post('tahun_mulai_profesional');
				}elseif($mode == 'edit'){
					$value = $fields['tahun_mulai_profesional'];
				}
			?>
			<input name="tahun_mulai_profesional" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_metode_rekrutmen"><?php echo lang('human_resource:id_metode_rekrutmen'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_metode_rekrutmen') != NULL){
					$value = $this->input->post('id_metode_rekrutmen');
				}elseif($mode == 'edit'){
					$value = $fields['id_metode_rekrutmen'];
				}
			?>
			<input name="id_metode_rekrutmen" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_supervisor"><?php echo lang('human_resource:id_supervisor'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_supervisor') != NULL){
					$value = $this->input->post('id_supervisor');
				}elseif($mode == 'edit'){
					$value = $fields['id_supervisor'];
				}
			?>
			<input name="id_supervisor" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="handphone"><?php echo lang('human_resource:handphone'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('handphone') != NULL){
					$value = $this->input->post('handphone');
				}elseif($mode == 'edit'){
					$value = $fields['handphone'];
				}
			?>
			<input name="handphone" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="email"><?php echo lang('human_resource:email'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('email') != NULL){
					$value = $this->input->post('email');
				}elseif($mode == 'edit'){
					$value = $fields['email'];
				}
			?>
			<input name="email" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_organization_unit"><?php echo lang('human_resource:id_organization_unit'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_organization_unit') != NULL){
					$value = $this->input->post('id_organization_unit');
				}elseif($mode == 'edit'){
					$value = $fields['id_organization_unit'];
				}
			?>
			<input name="id_organization_unit" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="id_user"><?php echo lang('human_resource:id_user'); ?></label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_user') != NULL){
					$value = $this->input->post('id_user');
				}elseif($mode == 'edit'){
					$value = $fields['id_user'];
				}
			?>
			<input name="id_user" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>