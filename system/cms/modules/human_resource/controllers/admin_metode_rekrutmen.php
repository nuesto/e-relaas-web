<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Human Resource Module
 *
 * Manage human resource
 *
 */
class Admin_metode_rekrutmen extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'metode_rekrutmen';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'access_metode_rekrutmen_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('human_resource');		
		$this->load->model('metode_rekrutmen_m');
    }

    /**
	 * List all metode_rekrutmen
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_metode_rekrutmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['metode_rekrutmen']['entries'] = $this->metode_rekrutmen_m->get_metode_rekrutmen();
		$data['metode_rekrutmen']['total'] = count($data['metode_rekrutmen']['entries']);
		
		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:metode_rekrutmen:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:plural'))
			->build('admin/metode_rekrutmen_index', $data);
    }
	
	/**
     * Display one metode_rekrutmen
     *
     * @return  void
     */
    public function view($id = 0)
    {
    	// there is not need to view
    	redirect('admin/human_resource/metode_rekrutmen/index');

        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_metode_rekrutmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['metode_rekrutmen'] = $this->metode_rekrutmen_m->get_metode_rekrutmen_by_id($id);
		
		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:metode_rekrutmen:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:plural'), '/admin/human_resource/metode_rekrutmen/index')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:view'))
			->build('admin/metode_rekrutmen_entry', $data);
    }
	
	/**
     * Create a new metode_rekrutmen entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'create_metode_rekrutmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_metode_rekrutmen('new')){	
				$this->session->set_flashdata('success', lang('human_resource:metode_rekrutmen:submit_success'));				
				redirect('admin/human_resource/metode_rekrutmen/index');
			}else{
				$data['messages']['error'] = lang('human_resource:metode_rekrutmen:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/human_resource/metode_rekrutmen/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:metode_rekrutmen:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:plural'), '/admin/human_resource/metode_rekrutmen/index')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:new'))
			->build('admin/metode_rekrutmen_form', $data);
    }
	
	/**
     * Edit a metode_rekrutmen entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the metode_rekrutmen to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'edit_all_metode_rekrutmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_metode_rekrutmen('edit', $id)){	
				$this->session->set_flashdata('success', lang('human_resource:metode_rekrutmen:submit_success'));				
				redirect('admin/human_resource/metode_rekrutmen/index');
			}else{
				$data['messages']['error'] = lang('human_resource:metode_rekrutmen:submit_failure');
			}
		}
		
		$data['fields'] = $this->metode_rekrutmen_m->get_metode_rekrutmen_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/human_resource/metode_rekrutmen/index/';
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:metode_rekrutmen:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:plural'), '/admin/human_resource/metode_rekrutmen/index')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:edit'))
			->build('admin/metode_rekrutmen_form', $data);
    }
	
	/**
     * Delete a metode_rekrutmen entry
     * 
     * @param   int [$id] The id of metode_rekrutmen to be deleted
     * @return  void
     */
    public function delete($id = 0, $order = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'delete_all_metode_rekrutmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
				
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->metode_rekrutmen_m->delete_metode_rekrutmen_by_id($id, $order);
        $this->session->set_flashdata('error', lang('human_resource:metode_rekrutmen:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/human_resource/metode_rekrutmen/index');
    }
	
	/**
     * Insert or update metode_rekrutmen entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_metode_rekrutmen($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama_metode', lang('human_resource:nama_metode'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->metode_rekrutmen_m->insert_metode_rekrutmen($values);
				
			}
			else
			{
				$result = $this->metode_rekrutmen_m->update_metode_rekrutmen($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	public function move($direction = '', $id = 0, $order = 0) {
		$this->load->helper('human_resource');
		move('metode_rekrutmen', $direction, $id, $order);
	}
}