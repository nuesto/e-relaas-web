<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Human Resource Module
 *
 * Manage human resource
 *
 */
class Human_resource extends Public_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('human_resource/personel/index');
    }

}