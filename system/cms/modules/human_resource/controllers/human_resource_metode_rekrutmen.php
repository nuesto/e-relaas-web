<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Human Resource Module
 *
 * Manage human resource
 *
 */
class Human_resource_metode_rekrutmen extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'metode_rekrutmen';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('human_resource');
		
		$this->load->model('metode_rekrutmen_m');
    }

    /**
	 * List all metode_rekrutmen
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_metode_rekrutmen') AND ! group_has_role('human_resource', 'view_own_metode_rekrutmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'human_resource/metode_rekrutmen/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->metode_rekrutmen_m->count_all_metode_rekrutmen();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['metode_rekrutmen']['entries'] = $this->metode_rekrutmen_m->get_metode_rekrutmen($pagination_config);
		$data['metode_rekrutmen']['total'] = count($data['metode_rekrutmen']['entries']);
		$data['metode_rekrutmen']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('human_resource:metode_rekrutmen:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:plural'))
			->build('metode_rekrutmen_index', $data);
    }
	
	/**
     * Display one metode_rekrutmen
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_metode_rekrutmen') AND ! group_has_role('human_resource', 'view_own_metode_rekrutmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['metode_rekrutmen'] = $this->metode_rekrutmen_m->get_metode_rekrutmen_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('human_resource', 'view_all_metode_rekrutmen')){
			if($data['metode_rekrutmen']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:metode_rekrutmen:view'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:plural'), '/human_resource/metode_rekrutmen/index')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:view'))
			->build('metode_rekrutmen_entry', $data);
    }
	
	/**
     * Create a new metode_rekrutmen entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'create_metode_rekrutmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_metode_rekrutmen('new')){	
				$this->session->set_flashdata('success', lang('human_resource:metode_rekrutmen:submit_success'));				
				redirect('human_resource/metode_rekrutmen/index');
			}else{
				$data['messages']['error'] = lang('human_resource:metode_rekrutmen:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'human_resource/metode_rekrutmen/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:metode_rekrutmen:new'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:plural'), '/human_resource/metode_rekrutmen/index')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:new'))
			->build('metode_rekrutmen_form', $data);
    }
	
	/**
     * Edit a metode_rekrutmen entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the metode_rekrutmen to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'edit_all_metode_rekrutmen') AND ! group_has_role('human_resource', 'edit_own_metode_rekrutmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('human_resource', 'edit_all_metode_rekrutmen')){
			$entry = $this->metode_rekrutmen_m->get_metode_rekrutmen_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_metode_rekrutmen('edit', $id)){	
				$this->session->set_flashdata('success', lang('human_resource:metode_rekrutmen:submit_success'));				
				redirect('human_resource/metode_rekrutmen/index');
			}else{
				$data['messages']['error'] = lang('human_resource:metode_rekrutmen:submit_failure');
			}
		}
		
		$data['fields'] = $this->metode_rekrutmen_m->get_metode_rekrutmen_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'human_resource/metode_rekrutmen/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
        $this->template->title(lang('human_resource:metode_rekrutmen:edit'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:plural'), '/human_resource/metode_rekrutmen/index')
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:view'), '/human_resource/metode_rekrutmen/view/'.$id)
			->set_breadcrumb(lang('human_resource:metode_rekrutmen:edit'))
			->build('metode_rekrutmen_form', $data);
    }
	
	/**
     * Delete a metode_rekrutmen entry
     * 
     * @param   int [$id] The id of metode_rekrutmen to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'delete_all_metode_rekrutmen') AND ! group_has_role('human_resource', 'delete_own_metode_rekrutmen')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('human_resource', 'delete_all_metode_rekrutmen')){
			$entry = $this->metode_rekrutmen_m->get_metode_rekrutmen_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->metode_rekrutmen_m->delete_metode_rekrutmen_by_id($id);
		$this->session->set_flashdata('error', lang('human_resource:metode_rekrutmen:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('human_resource/metode_rekrutmen/index');
    }
	
	/**
     * Insert or update metode_rekrutmen entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_metode_rekrutmen($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('human_resource:metode_rekrutmen:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->metode_rekrutmen_m->insert_metode_rekrutmen($values);
			}
			else
			{
				$result = $this->metode_rekrutmen_m->update_metode_rekrutmen($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}