<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Human Resource Module
 *
 * Manage human resource
 *
 */
class Admin_level extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'level';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'access_level_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('human_resource');		
		$this->load->model('level_m');
    }

    /**
	 * List all level
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_level')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['level']['entries'] = $this->level_m->get_level();
        $data['level']['total'] = count($data['level']['entries']); 

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:level:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:level:plural'))
			->build('admin/level_index', $data);
    }
	
	/**
     * Display one level
     *
     * @return  void
     */
    public function view($id = 0)
    {
    	// there is not need to view
    	redirect('admin/human_resource/level/index');

        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_level')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['level'] = $this->level_m->get_level_by_id($id);
		
		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:level:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:level:plural'), '/admin/human_resource/level/index')
			->set_breadcrumb(lang('human_resource:level:view'))
			->build('admin/level_entry', $data);
    }
	
	/**
     * Create a new level entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'create_level')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_level('new')){	
				$this->session->set_flashdata('success', lang('human_resource:level:submit_success'));				
				redirect('admin/human_resource/level/index');
			}else{
				$data['messages']['error'] = lang('human_resource:level:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/human_resource/level/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:level:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:level:plural'), '/admin/human_resource/level/index')
			->set_breadcrumb(lang('human_resource:level:new'))
			->build('admin/level_form', $data);
    }
	
	/**
     * Edit a level entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the level to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'edit_all_level')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_level('edit', $id)){	
				$this->session->set_flashdata('success', lang('human_resource:level:submit_success'));				
				redirect('admin/human_resource/level/index');
			}else{
				$data['messages']['error'] = lang('human_resource:level:submit_failure');
			}
		}
		
		$data['fields'] = $this->level_m->get_level_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/human_resource/level/index';
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:level:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:level:plural'), '/admin/human_resource/level/index')
			->set_breadcrumb(lang('human_resource:level:view'), '/admin/human_resource/level/view/'.$id)
			->set_breadcrumb(lang('human_resource:level:edit'))
			->build('admin/level_form', $data);
    }
	
	/**
     * Delete a level entry
     * 
     * @param   int [$id] The id of level to be deleted
     * @return  void
     */
    public function delete($id = 0, $order = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'delete_all_level')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->level_m->delete_level_by_id($id, $order);
        $this->session->set_flashdata('error', lang('human_resource:level:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/human_resource/level/index');
    }
	
	/**
     * Insert or update level entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_level($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama_level', lang('human_resource:nama_level'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->level_m->insert_level($values);
				
			}
			else
			{
				$result = $this->level_m->update_level($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

	public function move($direction = '', $id = 0, $order = 0) {
		$this->load->helper('human_resource');
		move('level', $direction, $id, $order);
	}

}