<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Human Resource Module
 *
 * Manage human resource
 *
 */
class Admin_personel_history extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'personel_history';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'access_personel_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('human_resource');		
		$this->load->model('personel_history_m');
    }
	
	/**
     * Create a new level entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'edit_all_personel') AND ! group_has_role('human_resource', 'edit_own_unit_personel') AND ! group_has_role('human_resource', 'edit_own_personel')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_personel_history('new', $id)){	
				$this->session->set_flashdata('success', lang('human_resource:personel_history:submit_success'));				
				redirect('admin/human_resource/personel/view/'.$id.'?display=history');
			}else{
				$data['messages']['error'] = lang('human_resource:personel_history:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/human_resource/personel/view/'.$id.'?display=history';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:personel_history:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('cp:nav_Human_Resource'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:personel:plural'), '/admin/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:personel_history:new'))
			->build('admin/personel_history_form', $data);
    }
	
	/**
     * Insert or update level entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_personel_history($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();
		$values['type'] = 'add-catatan';
		$values['id_personel'] = $row_id;

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('description', lang('human_resource:description'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->personel_history_m->insert_personel_history($values);
				
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}