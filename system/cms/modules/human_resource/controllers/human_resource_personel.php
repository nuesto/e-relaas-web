<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Human Resource Module
 *
 * Manage human resource
 *
 */
class Human_resource_personel extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'personel';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('human_resource');
		
		$this->load->model('personel_m');
    }

    /**
	 * List all personel
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_personel') AND ! group_has_role('human_resource', 'view_own_personel')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'human_resource/personel/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->personel_m->count_all_personel();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['personel']['entries'] = $this->personel_m->get_personel($pagination_config);
		$data['personel']['total'] = count($data['personel']['entries']);
		$data['personel']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('human_resource:personel:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('human_resource:personel:plural'))
			->build('personel_index', $data);
    }
	
	/**
     * Display one personel
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'view_all_personel') AND ! group_has_role('human_resource', 'view_own_personel')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['personel'] = $this->personel_m->get_personel_by_id($id);
		
		// Check view all/own permission
		if(! group_has_role('human_resource', 'view_all_personel')){
			if($data['personel']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:personel:view'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('human_resource:personel:plural'), '/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:personel:view'))
			->build('personel_entry', $data);
    }
	
	/**
     * Create a new personel entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'create_personel')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_personel('new')){	
				$this->session->set_flashdata('success', lang('human_resource:personel:submit_success'));				
				redirect('human_resource/personel/index');
			}else{
				$data['messages']['error'] = lang('human_resource:personel:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'human_resource/personel/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('human_resource:personel:new'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('human_resource:personel:plural'), '/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:personel:new'))
			->build('personel_form', $data);
    }
	
	/**
     * Edit a personel entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the personel to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'edit_all_personel') AND ! group_has_role('human_resource', 'edit_own_personel')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('human_resource', 'edit_all_personel')){
			$entry = $this->personel_m->get_personel_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_personel('edit', $id)){	
				$this->session->set_flashdata('success', lang('human_resource:personel:submit_success'));				
				redirect('human_resource/personel/index');
			}else{
				$data['messages']['error'] = lang('human_resource:personel:submit_failure');
			}
		}
		
		$data['fields'] = $this->personel_m->get_personel_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'human_resource/personel/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
        $this->template->title(lang('human_resource:personel:edit'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('human_resource:personel:plural'), '/human_resource/personel/index')
			->set_breadcrumb(lang('human_resource:personel:view'), '/human_resource/personel/view/'.$id)
			->set_breadcrumb(lang('human_resource:personel:edit'))
			->build('personel_form', $data);
    }
	
	/**
     * Delete a personel entry
     * 
     * @param   int [$id] The id of personel to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('human_resource', 'delete_all_personel') AND ! group_has_role('human_resource', 'delete_own_personel')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('login');
		}
		// Check view all/own permission
		if(! group_has_role('human_resource', 'delete_all_personel')){
			$entry = $this->personel_m->get_personel_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('login');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->personel_m->delete_personel_by_id($id);
		$this->session->set_flashdata('error', lang('human_resource:personel:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('human_resource/personel/index');
    }
	
	/**
     * Insert or update personel entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_personel($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('human_resource:personel:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->personel_m->insert_personel($values);
			}
			else
			{
				$result = $this->personel_m->update_personel($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}