<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['human_resource/admin/personel(:any)'] = 'admin_personel$1';
$route['human_resource/admin/status_pekerja(:any)'] = 'admin_status_pekerja$1';
$route['human_resource/admin/level(:any)'] = 'admin_level$1';
$route['human_resource/admin/metode_rekrutmen(:any)'] = 'admin_metode_rekrutmen$1';
$route['human_resource/personel(:any)'] = 'human_resource_personel$1';
$route['human_resource/status_pekerja(:any)'] = 'human_resource_status_pekerja$1';
$route['human_resource/level(:any)'] = 'human_resource_level$1';
$route['human_resource/metode_rekrutmen(:any)'] = 'human_resource_metode_rekrutmen$1';
