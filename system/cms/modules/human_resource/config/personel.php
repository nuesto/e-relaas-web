<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Is Complete Personel
|--------------------------------------------------------------------------
|
| Default value of is_complete. 1 => Complete, NULL/0 => Not Complete
|
|--------------------------------------------------------------------------*/
$config['default_is_complete']	= NULL;