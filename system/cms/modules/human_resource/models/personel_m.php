<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Personel model
 *
 * @author Aditya Satrya
 */
class Personel_m extends MY_Model {
	
	public function get_personel($pagination_config = NULL, $params = NULL, $filter = NULL, $sort = NULL, $skip_personel = NULL)
	{
		$this->db->select('default_human_resource_personel.*, unit_name, display_name as user_display_name');
		$this->db->select('supervisor.nama_lengkap as supervisor_nama_lengkap');
		$this->db->select('supervisor.handphone as supervisor_handphone');
		$this->db->select('supervisor.email as supervisor_email');
		$this->db->select('supervisor.supervisor_level as supervisor_level');
		$this->db->select('default_human_resource_metode_rekrutmen.nama_metode');
		$this->db->select('default_human_resource_status_pekerja.nama_status');
		$this->db->select('default_human_resource_level.nama_level');
		
		$this->db->select('default_human_resource_jabatan.posisi jabatan');
		$this->db->select('default_human_resource_jabatan.deskripsi deskripsi_jabatan');

		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		
		if(isset($params)) {
			$this->db->like($params);
		}

		if(isset($filter) AND $filter != '') {
			$this->db->where($filter, NULL, false);
		}

		if(isset($sort) AND $sort != '') {
			$this->db->order_by($sort, NULL, false);
		}

		if(isset($skip_personel) AND is_array($skip_personel)) {
			$this->db->where('default_human_resource_personel.id NOT IN ('.implode(",", $skip_personel).')', NULL, false);
		}

		$query = $this->db->join('default_human_resource_status_pekerja', 'default_human_resource_personel.id_status_pekerja=default_human_resource_status_pekerja.id', 'left');
		$query = $this->db->join('default_human_resource_level', 'default_human_resource_personel.id_level=default_human_resource_level.id', 'left');
		$query = $this->db->join('default_human_resource_metode_rekrutmen', 'default_human_resource_personel.id_metode_rekrutmen=default_human_resource_metode_rekrutmen.id', 'left');

		$this->db->join('(select `default_human_resource_personel`.*, `nama_level` `supervisor_level` from `default_human_resource_personel` left join `default_human_resource_level` on `default_human_resource_personel`.`id_level`=`default_human_resource_level`.`id`) supervisor', 'default_human_resource_personel.id_supervisor = supervisor.id', 'left');
		$this->db->join('default_organization_units', 'default_human_resource_personel.id_organization_unit=default_organization_units.id', 'left');
		$this->db->join('default_users', 'default_human_resource_personel.id_user=default_users.id', 'left');
		$this->db->join('default_profiles', 'default_users.id=default_profiles.user_id', 'left');

		$this->db->join('default_human_resource_jabatan','default_human_resource_personel.id=default_human_resource_jabatan.id_personel and CONCAT(tahun_selesai,"-",LPAD(bulan_selesai,2,0),"-31")>DATE_FORMAT(now(), "%Y-%m-%d") and CONCAT(tahun_mulai,"-",CONCAT(LPAD(bulan_mulai,2,0)),"-01")<DATE_FORMAT(now(), "%Y-%m-%d")','left');
		$this->db->order_by('default_human_resource_level.ordering_count, default_human_resource_personel.nama_lengkap');
		
		$query = $this->db->get('default_human_resource_personel');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_personel_by_id($id)
	{
		$this->db->select('default_human_resource_personel.*, unit_name, display_name as user_display_name');
		$this->db->select('supervisor.nama_lengkap as supervisor_nama_lengkap');
		$this->db->select('supervisor.handphone as supervisor_handphone');
		$this->db->select('supervisor.email as supervisor_email');
		$this->db->select('supervisor.supervisor_level as supervisor_level');
		$this->db->select('default_human_resource_metode_rekrutmen.nama_metode');
		$this->db->select('default_human_resource_status_pekerja.nama_status');
		$this->db->select('default_human_resource_level.nama_level');
		
		$this->db->select('default_human_resource_jabatan.posisi jabatan');
		$this->db->select('default_human_resource_jabatan.deskripsi deskripsi_jabatan');

		$this->db->where('default_human_resource_personel.id', $id);

		$query = $this->db->join('default_human_resource_status_pekerja', 'default_human_resource_personel.id_status_pekerja=default_human_resource_status_pekerja.id', 'left');
		$query = $this->db->join('default_human_resource_level', 'default_human_resource_personel.id_level=default_human_resource_level.id', 'left');
		$query = $this->db->join('default_human_resource_metode_rekrutmen', 'default_human_resource_personel.id_metode_rekrutmen=default_human_resource_metode_rekrutmen.id', 'left');

		$this->db->join('(select `default_human_resource_personel`.*, `nama_level` `supervisor_level` from `default_human_resource_personel` left join `default_human_resource_level` on `default_human_resource_personel`.`id_level`=`default_human_resource_level`.`id`) supervisor', 'default_human_resource_personel.id_supervisor = supervisor.id', 'left');
		$this->db->join('default_organization_units', 'default_human_resource_personel.id_organization_unit=default_organization_units.id', 'left');
		$this->db->join('default_users', 'default_human_resource_personel.id_user=default_users.id', 'left');
		$this->db->join('default_profiles', 'default_users.id=default_profiles.user_id', 'left');
		$this->db->join('default_human_resource_jabatan','default_human_resource_personel.id=default_human_resource_jabatan.id_personel and CONCAT(tahun_selesai,"-",LPAD(bulan_selesai,2,0),"-31")>DATE_FORMAT(now(), "%Y-%m-%d") and CONCAT(tahun_mulai,"-",CONCAT(LPAD(bulan_mulai,2,0)),"-01")<DATE_FORMAT(now(), "%Y-%m-%d")','left');

		$query = $this->db->get('default_human_resource_personel');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_personel_by_account($id)
	{
		$this->db->select('default_human_resource_personel.*, unit_name, display_name as user_display_name');
		$this->db->select('supervisor.nama_lengkap as supervisor_nama_lengkap');
		$this->db->select('supervisor.handphone as supervisor_handphone');
		$this->db->select('supervisor.email as supervisor_email');
		$this->db->select('supervisor.supervisor_level as supervisor_level');
		$this->db->select('default_human_resource_metode_rekrutmen.nama_metode');
		$this->db->select('default_human_resource_status_pekerja.nama_status');
		$this->db->select('default_human_resource_level.nama_level');
		
		$this->db->select('default_human_resource_jabatan.posisi jabatan');
		$this->db->select('default_human_resource_jabatan.deskripsi deskripsi_jabatan');

		$this->db->where('default_human_resource_personel.id_user', $id);

		$query = $this->db->join('default_human_resource_status_pekerja', 'default_human_resource_personel.id_status_pekerja=default_human_resource_status_pekerja.id', 'left');
		$query = $this->db->join('default_human_resource_level', 'default_human_resource_personel.id_level=default_human_resource_level.id', 'left');
		$query = $this->db->join('default_human_resource_metode_rekrutmen', 'default_human_resource_personel.id_metode_rekrutmen=default_human_resource_metode_rekrutmen.id', 'left');

		$this->db->join('(select `default_human_resource_personel`.*, `nama_level` `supervisor_level` from `default_human_resource_personel` left join `default_human_resource_level` on `default_human_resource_personel`.`id_level`=`default_human_resource_level`.`id`) supervisor', 'default_human_resource_personel.id_supervisor = supervisor.id', 'left');
		$this->db->join('default_organization_units', 'default_human_resource_personel.id_organization_unit=default_organization_units.id', 'left');
		$this->db->join('default_users', 'default_human_resource_personel.id_user=default_users.id', 'left');
		$this->db->join('default_profiles', 'default_users.id=default_profiles.user_id', 'left');
		$this->db->join('default_human_resource_jabatan','default_human_resource_personel.id=default_human_resource_jabatan.id_personel and CONCAT(tahun_selesai,"-",LPAD(bulan_selesai,2,0),"-31")>DATE_FORMAT(now(), "%Y-%m-%d") and CONCAT(tahun_mulai,"-",CONCAT(LPAD(bulan_mulai,2,0)),"-01")<DATE_FORMAT(now(), "%Y-%m-%d")','left');

		$query = $this->db->get('default_human_resource_personel');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_personel()
	{
		return $this->db->count_all('human_resource_personel');
	}

	public function get_personel_history($id, $pagination_config = NULL, $filter = NULL) {
		$this->db->select('*');

		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		$this->db->where('id_personel', $id);

		if(isset($filter)) {
			$this->db->where($filter);
		}

		$this->db->order_by('created_on','desc');

		$query = $this->db->get('default_human_resource_personel_history');
		$result = $query->result_array();
		
        return $result;
	}

	public function count_all_personel_history($id, $filter = NULL)
	{
		$this->db->where('id_personel', $id);
		if(isset($filter)) {
			$this->db->where($filter);
		}
		return $this->db->count_all_results('default_human_resource_personel_history');
	}

	public function get_personel_history_type()
	{
		$this->db->distinct();
		$this->db->select('type');

		$res = $this->db->get('default_human_resource_personel_history');

		return $res->result_array();
	}
	
	public function delete_personel_by_id($id)
	{
		$this->db->where('id_personel', $id);
		$this->db->delete('default_human_resource_jabatan');

		$this->db->where('id_personel', $id);
		$this->db->delete('default_human_resource_personel_history');
		
		$this->db->where('id', $id);
		$this->db->delete('default_human_resource_personel');
	}
	
	public function insert_personel($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;

		$jabatan = (isset($values['jabatan'])) ? $values['jabatan'] : array();
		unset($values['jabatan']);
		unset($values['provinsi_office']);
		unset($values['kota_office']);
		unset($values['kecamatan_office']);
		unset($values['provinsi_home']);
		unset($values['kota_home']);
		unset($values['kecamatan_home']);

		$this->db->trans_start();
		$this->db->insert('default_human_resource_personel', $values);

		$id_personel = $this->db->insert_id();

		foreach ($jabatan as $value) {
			$value['id_personel'] = $id_personel;
			$this->db->insert('default_human_resource_jabatan', $value);
		}

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	public function update_personel($values, $row_id, $history)
	{
		$values['updated_on'] = date("Y-m-d H:i:s");
		
		$jabatan = (isset($values['jabatan'])) ? $values['jabatan'] : array();
		unset($values['jabatan']);
		unset($values['provinsi_office']);
		unset($values['kota_office']);
		unset($values['kecamatan_office']);
		unset($values['provinsi_home']);
		unset($values['kota_home']);
		unset($values['kecamatan_home']);

		$this->db->trans_start();

		$this->db->where('id', $row_id);
		$this->db->update('default_human_resource_personel', $values);

		$this->db->where('id_personel', $row_id);
		$this->db->delete('default_human_resource_jabatan');

		foreach ($jabatan as $value) {
			$value['id_personel'] = $row_id;
			$this->db->insert('default_human_resource_jabatan', $value);
		}

		$this->db->trans_complete();

		foreach ($history as $h) {
			$data_history = array(
				'id_personel'=>$row_id,
				'type'=>$h['type'],
				'description'=>$h['description'],
				'created_on'=>date("Y-m-d H:i:s"),
				'created_by'=>$this->current_user->id
				);

			$this->db->insert('default_human_resource_personel_history', $data_history);
		}

		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	public function get_organization_unit_by_id($id_organization_unit = 0) {
		$this->db->where('id', $id_organization_unit);

		return $this->db->get('default_organization_units')->row_array();
	}

	public function get_user_by_id($id_user = 0) {
		$this->db->where('default_users.id', $id_user);
		$this->db->join('default_profiles', 'default_users.id=default_profiles.user_id');
		return $this->db->get('default_users')->row_array();
	}

	public function get_tahun_mulai_bekerja() {
		$this->db->distinct();
		$this->db->select('tahun_mulai_bekerja');

		return $this->db->get('default_human_resource_personel')->result_array();
	}

	public function get_tahun_mulai_profesional() {
		$this->db->distinct();
		$this->db->select('tahun_mulai_profesional');

		return $this->db->get('default_human_resource_personel')->result_array();
	}
	
	public function get_personel_account($params = NULL, $exception_id = '', $all = FALSE) {
		$this->db->select('default_users.username, default_users.group_id, default_groups.name group_name, default_groups.description group_description, default_groups.sort_order, default_profiles.*');
		$this->db->from('default_users');
		$this->db->join('default_profiles', 'default_users.id=default_profiles.user_id', 'left');
		$this->db->join('default_groups', 'default_users.group_id=default_groups.id', 'left');

		$this->db->where('default_groups.enable_personel_link', '1');

		if(isset($params['display_name'])) {
			$this->db->where('display_name', $params['display_name']);
		}

		if(isset($params['custom'])) {
			$this->db->where($params['custom'],NULL,FALSE);
		}

		if($all === FALSE) {
			$skip_personel = 'user_id NOT IN (SELECT IFNULL(`id_user`,\'\') FROM `default_human_resource_personel`';
			if($exception_id!='') {
				$skip_personel .= 'where `id_user` != '.$exception_id.')';
			} else {
				$skip_personel .= ')';
			}
			$this->db->where($skip_personel);
		}

		$this->db->order_by('sort_order, display_name');


		return $this->db->get()->result_array();
	}

	public function get_jabatan_by_personel($id = '') {
		$this->db->where('id_personel',$id);

		return $this->db->get('default_human_resource_jabatan')->result_array();
	}
}