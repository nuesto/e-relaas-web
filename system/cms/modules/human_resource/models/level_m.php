<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Level model
 *
 * @author Aditya Satrya
 */
class Level_m extends MY_Model {
	
	public function get_level($pagination_config = NULL)
	{
		$this->db->select('*');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$this->db->order_by('ordering_count');
		$query = $this->db->get('default_human_resource_level');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_level_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$this->db->order_by('ordering_count');
		$query = $this->db->get('default_human_resource_level');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_level()
	{
		return $this->db->count_all('human_resource_level');
	}
	
	public function delete_level_by_id($id, $order)
	{
		$this->db->where('id', $id);
		$res = $this->db->delete('default_human_resource_level');
		
		if($res) {
			$this->db->query('update default_human_resource_level set ordering_count = ordering_count-1 where ordering_count>'.$order);
		}
	}
	
	public function insert_level($values)
	{
		$this->db->select_max('ordering_count');
		$max = $this->db->get('default_human_resource_level')->row();
		
		$values['ordering_count'] = $max->ordering_count+1;
		return $this->db->insert('default_human_resource_level', $values);
	}
	
	public function update_level($values, $row_id)
	{
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_human_resource_level', $values); 
	}
	
	public function move_level($id, $order, $direction) {
		if($direction=='up') {
			$new_order = $order--;
		} elseif ($direction=='down') {
			$new_order = $order++;
		}

		$this->db->where('ordering_count', $order);
		$this->db->update('default_human_resource_level', array('ordering_count'=>$new_order));

		$this->db->where('id', $id);
		$this->db->update('default_human_resource_level', array('ordering_count'=>$order));

		return true;
	}
}