<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Personel model
 *
 * @author Aditya Satrya
 */
class Personel_history_m extends MY_Model {
	
	public function insert_personel_history($values)
	{
		$values['created_on'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_human_resource_personel_history', $values);
	}
}