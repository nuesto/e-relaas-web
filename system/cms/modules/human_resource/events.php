<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Sample Events Class
 *
 * @package     PyroCMS
 * @subpackage  Sample Module
 * @category    Users
 * @author      M. Sidi Mustaqbal
 */
class Events_Users
{
    protected $ci;

    public function __construct()
    {
        $this->ci =& get_instance();

        // register the public_controller event when this file is autoloaded
        Events::register('user_updated', array($this, 'user_update'));
	}

	public function user_update($id = '0')
	{
		$this->ci->load->model('users/user_m');
		$this->ci->load->library('organization/Organization');

		$users = $this->ci->user_m->get(array('id'=>$id));
		$memberships = $this->ci->organization->get_membership_by_user($id);

		$id_organization_unit = (isset($memberships['entries'][0]['membership_unit']['id'])) ? $memberships['entries'][0]['membership_unit']['id'] : NULL;

		$data_personel = array (
			'nama_lengkap' => $users->display_name,
			'email' => $users->email,
			'id_organization_unit' => $id_organization_unit,
			);

		$this->ci->db->where('id_user',$id);
		return $this->ci->db->update('human_resource_personel',$data_personel);
	}

}
