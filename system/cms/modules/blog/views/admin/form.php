<?php Asset::js('ckeditor/ckeditor.js', 'wysiwyg'); ?>
<?php Asset::js('ckeditor/adapters/jquery.js', 'wysiwyg'); ?>
<?php echo Asset::render_js('wysiwyg') ?>

<script type="text/javascript">

	var instance;

	function update_instance()
	{
		instance = CKEDITOR.currentInstance;
	}

	(function($) {
		$(function(){

			pyro.init_ckeditor = function(){
				<?php echo $this->parser->parse_string(Settings::get('ckeditor_config'), $this, TRUE); ?>
			};
			pyro.init_ckeditor();

		});
	})(jQuery);
</script>

<div class="page-header">
<?php if ($this->method == 'create'): ?>
	<h1><?php echo lang('blog:create_title') ?></h1>
<?php else: ?>
	<h1><?php echo sprintf(lang('blog:edit_title'), $post->title) ?></h1>
<?php endif ?>
</div>

<?php echo form_open_multipart('', array('class' => 'form-horizontal')); ?>

<div class="row">

<div class="col-sm-8">
<?php
$supported_lang = get_supported_lang();
foreach ($supported_lang as $sl) {
	$tmp_lang = explode("=", $sl);
	$lang_list[$tmp_lang[0]] = $tmp_lang[1];
}
$public_lang = explode(",", $this->settings->get('site_public_lang'));
array_unshift($public_lang, $this->settings->get('site_lang'));
$public_lang = array_unique($public_lang);
foreach ($public_lang as $pl) {
	$available_lang[] = array('code'=>$pl,'name'=>$lang_list[$pl]);
}

echo form_hidden('available_lang',implode(",", $public_lang));
?>
<?php
foreach ($available_lang as $key => $al) { 
	$lang_code = '';
	if(count($available_lang)>1) {
		echo '<h2>'.$al['name'].'</h2>';
		$lang_code = '_'.$al['code'];
	}
	if(isset($post->translation[$al['code']])) {
		$trans_detail = json_decode($post->translation[$al['code']]->translation);
		
		if($this->input->post('title'.$lang_code)) {
			$v = $this->input->post('title'.$lang_code);
		} elseif (isset($trans_detail->{'title_'.$al['code']})) {
			$v = $trans_detail->{'title_'.$al['code']};
		} else {
			$v = '';
		}
		$post->title = $v;

		if($this->input->post('slug'.$lang_code)) {
			$v = $this->input->post('slug'.$lang_code);
		} elseif (isset($trans_detail->{'slug_'.$al['code']})) {
			$v = $trans_detail->{'slug_'.$al['code']};
		} else {
			$v = '';
		}
		$post->slug = $v;

		if($this->input->post('body'.$lang_code)) {
			$v = $this->input->post('body'.$lang_code);
		} elseif (isset($trans_detail->{'body_'.$al['code']})) {
			$v = $trans_detail->{'body_'.$al['code']};
		} else {
			$v = '';
		}
		$post->body = $v;

		if($this->input->post('intro'.$lang_code)) {
			$v = $this->input->post('intro'.$lang_code);
		} elseif (isset($trans_detail->{'intro_'.$al['code']})) {
			$v = $trans_detail->{'intro_'.$al['code']};
		} else {
			$v = '';
		}
		$post->intro = $v;
	} else {
		$post->title = ($this->input->post('title'.$lang_code)) ? $this->input->post('title'.$lang_code) : '';
		$post->slug = ($this->input->post('slug'.$lang_code)) ? $this->input->post('slug'.$lang_code) : '';
		$post->body = ($this->input->post('body'.$lang_code)) ? $this->input->post('body'.$lang_code) : '';
		$post->intro = ($this->input->post('intro'.$lang_code)) ? $this->input->post('intro'.$lang_code) : '';
	}
	?>
<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:title') ?>*</label>

	<div class="col-sm-10">

		<input type="text" class="form-control" placeholder="<?php echo lang('global:title') ?>" name="title<?php echo $lang_code; ?>" id="title<?php echo $lang_code; ?>" value="<?php echo htmlspecialchars_decode($post->title); ?>" maxlength="100" />

	</div>

</div>

<div class="form-group">

	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:slug') ?>*</label>

	<div class="col-sm-10">

		<input type="text" class="form-control" placeholder="<?php echo lang('global:slug'); ?>" id="slug<?php echo $lang_code; ?>" name="slug<?php echo $lang_code; ?>" value="<?php echo $post->slug; ?>" maxlength="100" />

		<script>
			$('#title<?php echo $lang_code; ?>').slugify({ slug: '#slug<?php echo $lang_code; ?>', type: '-' });
		</script>

	</div>
</div>

<div class="form-group">

	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('blog:content_label') ?>*</label>

	<div class="col-sm-10">

		<?php echo form_dropdown('type', array(
			'html' => 'html',
			'markdown' => 'markdown',
			'wysiwyg-simple' => 'wysiwyg-simple',
			'wysiwyg-advanced' => 'wysiwyg-advanced',
		), $post->type, 'style="margin-bottom:10px" id="type'.$lang_code.'"') ?>
		<script>
			$("#type<?php echo $lang_code; ?>").change(function() {
				var textarea = $('#body<?php echo $lang_code; ?>');
				// Destroy existing WYSIWYG instance
				textarea.removeClass();
				var instance = CKEDITOR.instances[textarea.attr('id')];
				instance && instance.destroy();
				// Set up the new instance
				textarea.addClass($(this).val());
				textarea.addClass('form-control');
				pyro.init_ckeditor();
			});
		</script>

		<br />

		<?php echo form_textarea(array('id' => 'body'.$lang_code, 'name' => 'body'.$lang_code, 'value' => htmlspecialchars_decode($post->body), 'rows' => 30, 'class' => $post->type.' form-control')) ?>

		<?php echo form_hidden('preview_hash', $post->preview_hash)?>

	</div>

</div>

<div class="form-group">

	<label for="intro" class="col-sm-2 control-label no-padding-right">Snippet </label>
	
	<div class="col-sm-10">
	
		<textarea name="intro<?php echo $lang_code; ?>" cols="40" rows="10" class="wysiwyg-simple form-control" id="intro<?php echo $lang_code; ?>" ><?php echo htmlspecialchars_decode($post->intro); ?></textarea>
	</div>
</div>

<?php } ?>
<!-- End of multilang content -->
</div>

<style type="text/css">
	.form-group {
		margin: 0 0 15px 0 !important;
	}

	.form-group:last-child {
		margin-bottom: 10px !important;
	}

	.tags {
		display: block;
		width: auto;
	}

	.widget-main {
		padding: 10px 15px;
	}
</style>

<div class="col-sm-4">
<?php
if(count($available_lang)>1) {
	echo '<h2>General</h2>';
}
?>
<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo lang('blog:publishing_options_title'); ?></h4>
	</div>

	<div class="widget-body">
		<div class="widget-main">

			<div class="form-group">
				<label>Image</label>
				<input name="image" value="<?php echo (isset($post->image)) ? $post->image : 'dummy' ; ?>" type="hidden">
				<?php
				if(isset($post->image) AND $post->image != 'dummy') {
					echo '<img class="img img-responsive" src="'.base_url().'files/thumb/'.$post->image.'">';
				}
				?>
				<input name="file_image" value="" type="file">
			</div>
		</div>
	</div>
</div>

<hr />
<?php if($this->config->item('with_organization')) { ?>
<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo lang('blog:publishing_options_title'); ?></h4>
	</div>

	<div class="widget-body">
		<div class="widget-main">

			<div class="form-group">
				<label><?php echo lang('blog:organization_label') ?></label>
				<select class="form-control" name="id_unit">
					<?php 
					if($this->config->item('allow_empty_organization')) :
						if(group_has_role('blog','create_post') OR group_has_role('blog','create_own_unit_post')) : 
					?>
					<option value=""><?php echo lang('blog:choose_organization'); ?></option>
					<?php
						endif;
					endif;
					?>

					<?php foreach ($organization as $org) { ?>
						<option value="<?php echo $org['id']; ?>" <?php echo (isset($post->id_unit) and $post->id_unit==$org['id']) ? 'selected' : ''; ?>>
							<?php
							for ($i=0; $i < $org['type_level']; $i++) { 
								echo '&nbsp;&nbsp;&nbsp;';
							}
							echo $org['unit_name']; 
							?>
						</option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
</div>

<hr />
<?php } ?>
<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo lang('blog:publishing_options_title'); ?></h4>
	</div>

	<div class="widget-body">
		<div class="widget-main">

			<div class="form-group">
				<label><?php echo lang('blog:status_label') ?></label>
				<?php
				$status = array('draft' => lang('blog:draft_label'));
				// check the owner of post
				if(!group_has_role('blog','publish_all_post')) {
					if (group_has_role('blog','publish_own_unit_post')) {
						$status['live'] = lang('blog:live_label');
						if(isset($post->id_unit) AND !$this->organization->is_member($this->current_user->id,$post->id_unit)) {
							unset($status['live']);
						}
					}elseif(group_has_role('blog','publish_own_post')) {
						$status['live'] = lang('blog:live_label');
						if(isset($post->created_by) AND !$post->created_by == $this->current_user->id) {
							unset($status['live']);
						}
					}
				} elseif(group_has_role('blog','publish_all_post')) {
					$status['live'] = lang('blog:live_label');
				}
				?>
				<?php echo form_dropdown('status', $status, $post->status, 'class="form-control"') ?>

			</div>

			<div class="form-group">
				<label><?php echo lang('blog:date_label') ?></label>
				<div class="row">
					<div class="col-md-7">
						<div class="input-group">
							<?php echo form_input('created_on', date("d-m-Y", $post->created_on), 'class="form-control" id="created_on_date"') ?>
							<span class="input-group-addon"><i class="icon-calendar blue"></i></span>
						</div>
					</div>

					<div class="col-md-5">
						<div class="input-group bootstrap-timepicker">
							<?php echo form_input('created_on_time', date("H:i", $post->created_on), 'class="form-control" id="created_on_time"') ?>
							<span class="input-group-addon"><i class="icon-time blue"></i></span>
						</div>
					</div>

					<script>
						$(document).ready(function(){
							$('#created_on_date').datepicker({
								autoclose: true,
								format: 'dd-mm-yyyy'
							});
							$('#created_on_time').timepicker({
								minuteStep: 30,
								showSeconds: false,
								showMeridian: false
							});
						});
					</script>
				</div>
			</div>


			<?php if ( ! module_enabled('comments')): ?>
				<?php echo form_hidden('comments_enabled', 'no'); ?>
			<?php else: ?>


			<div class="form-group">
				<label><?php echo lang('blog:comments_enabled_label');?></label>
				<?php echo form_dropdown('comments_enabled', array(
					'no' => lang('global:no'),
					'1 day' => lang('global:duration:1-day'),
					'1 week' => lang('global:duration:1-week'),
					'2 weeks' => lang('global:duration:2-weeks'),
					'1 month' => lang('global:duration:1-month'),
					'3 months' => lang('global:duration:3-months'),
					'always' => lang('global:duration:always'),
				), $post->comments_enabled ? $post->comments_enabled : '3 months', 'class="form-control"') ?>
			</div>

			<?php endif; ?>

		</div>
	</div>

</div>

<hr />

<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo lang('blog:taxonomy_title'); ?></h4>
	</div>

	<div class="widget-body">
		<div class="widget-main">

			<div class="form-group">
				<label style="display:block"><?php echo lang('blog:category_label') ?>
				<?php if(group_has_role('blog','manage_categories')) : ?>
				<?php echo anchor('admin/blog/categories/create', lang('blog:new_category_label'), 'class="pull-right" target="_blank"') ?>
				<?php endif; ?>
				</label>

				<?php echo form_dropdown('category_id', array(lang('blog:no_category_select_label')) + $categories, @$post->category_id, 'class="form-control"') ?>

			</div>

			<?php if ( ! module_enabled('keywords')): ?>
				<?php echo form_hidden('keywords'); ?>
			<?php else: ?>

			<div class="form-group">
				<label><?php echo lang('global:keywords') ?></label>
				<div>
				<?php echo form_textarea(array('id' => 'keywords', 'name' => 'keywords', 'value' => $post->keywords, 'rows' => 2, 'class' => 'form-control')); ?>
				</div>
				<script>
					$(document).ready(function(){
						var tag_input = $('#keywords');
						// make tag input field
						tag_input.tag(
						  {
							placeholder:tag_input.attr('placeholder'),
							//enable typeahead by specifying the source array
							source: function(query, process){
								return $.getJSON('<?php echo base_url(); ?>admin/keywords/autocomplete', { query: query }, function (data) {
									return process(data);
								});
							}
						  }
						);
					});
				</script>

			</div>

			<?php endif; ?>

		</div>
	</div>

</div>

</div>

</div>

<div class="row">

<div class="col-sm-12">

<input type="hidden" name="row_edit_id" value="<?php if ($this->method != 'create'): echo $post->id; endif; ?>" />

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" value="save" name="btnAction" type="submit">
			<i class="icon-save bigger-110"></i>
			<?php echo lang('buttons:save'); ?>
		</button>

		<button class="btn btn-info" value="save_exit" name="btnAction" type="submit">
			<i class="icon-save bigger-110"></i>
			<?php echo lang('buttons:save_exit'); ?>
		</button>

		<a class="btn" href="<?php echo base_url().'admin/blog/index' ?>">
			<i class="icon-remove bigger-110"></i>
			<?php echo lang('buttons:cancel'); ?>
		</a>
	</div>
</div>

</div>

</div>

<?php echo form_close() ?>