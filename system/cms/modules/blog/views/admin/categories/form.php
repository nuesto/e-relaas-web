<div class="page-header">
	<?php if ($this->controller == 'admin_categories' && $this->method === 'edit'): ?>
	<h1><?php echo sprintf(lang('cat:edit_title'), $category->title);?></h1>
	<?php else: ?>
	<h1><?php echo lang('cat:create_title');?></h1>
	<?php endif ?>
</div>

<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"') ?>

<?php
$supported_lang = get_supported_lang();
foreach ($supported_lang as $sl) {
	$tmp_lang = explode("=", $sl);
	$lang_list[$tmp_lang[0]] = $tmp_lang[1];
}
$public_lang = explode(",", $this->settings->get('site_public_lang'));
array_unshift($public_lang, $this->settings->get('site_lang'));
$public_lang = array_unique($public_lang);
foreach ($public_lang as $pl) {
	$available_lang[] = array('code'=>$pl,'name'=>$lang_list[$pl]);
}

echo form_hidden('available_lang',implode(",", $public_lang));
?>
<?php
foreach ($available_lang as $key => $al) { 
	$lang_code = '';
	if(count($available_lang)>1) {
		echo '<h2>'.$al['name'].'</h2>';
		$lang_code = '_'.$al['code'];
	}
	if(isset($category->translation[$al['code']])) {
		$trans_detail = json_decode($category->translation[$al['code']]->translation);
		
		if($this->input->post('title'.$lang_code)) {
			$v = $this->input->post('title'.$lang_code);
		} elseif (isset($trans_detail->{'title_'.$al['code']})) {
			$v = $trans_detail->{'title_'.$al['code']};
		} else {
			$v = '';
		}
		$category->title = $v;

		if($this->input->post('slug'.$lang_code)) {
			$v = $this->input->post('slug'.$lang_code);
		} elseif (isset($trans_detail->{'slug_'.$al['code']})) {
			$v = $trans_detail->{'slug_'.$al['code']};
		} else {
			$v = '';
		}
		$category->slug = $v;
	} else {
		$category->title = ($this->input->post('title'.$lang_code)) ? $this->input->post('title'.$lang_code) : '';
		$category->slug = ($this->input->post('slug'.$lang_code)) ? $this->input->post('slug'.$lang_code) : '';
	}
	?>
<div class="form-group">

	<label class="col-sm-2 control-label no-padding-right" for="title"><?php echo lang('global:title');?> <span>*</span></label>
	
	<div class="col-sm-10">
		<?php echo  form_input('title'.$lang_code, $category->title, 'id="title'.$lang_code.'"') ?>
	</div>
	
</div>

<div class="form-group">

	<label class="col-sm-2 control-label no-padding-right" for="slug"><?php echo lang('global:slug') ?> <span>*</span></label>
	
	<div class="col-sm-10">
		<?php echo  form_input('slug'.$lang_code, $category->slug, 'id="slug'.$lang_code.'"') ?>
		<script>
			$('#title<?php echo $lang_code?>').slugify({ slug: '#slug<?php echo $lang_code?>', type: '-' });
		</script>
	</div>
	
</div>
<?php } ?>
<?php
if(count($available_lang)>1) {
	echo '<h2>General</h2>';
}
?>
<div class="form-group">
	<?php echo  form_hidden('id', $category->id) ?>
	<label class="col-sm-2 control-label no-padding-right" for="slug"><?php echo 'Parent Category'; ?> <span>*</span></label>
	
	<div class="col-sm-10">
		<select name="parent_id">
			<option value="">-- Choose Parent Category --</option>
		<?php foreach($available_categories as $ac) {
			
			$selected = NULL;
			$disabled = NULL;
			
			if ($this->controller == 'admin_categories' && $this->method === 'edit'): 
				if($category->id==$ac->id || $category->id==$ac->parent_id) {
					$disabled = 'disabled';
				}

				if(isset($category->parent_id) AND $category->parent_id==$ac->id) {
					$selected = 'selected';
				}
			endif;

			echo '<option value="'.$ac->id.'" '.$selected.' '.$disabled.'>'.$ac->title.'</option>';
		} ?>
		</select>
	</div>
	
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button class="btn btn-info" value="save" name="btnAction" type="submit">
			<i class="icon-save bigger-110"></i>
			<?php echo lang('buttons:save'); ?>
		</button>
		
		<a class="btn" href="<?php echo base_url(). 'admin/blog/categories/index' ?>">
			<i class="icon-remove bigger-110"></i>
			<?php echo lang('buttons:cancel'); ?>
		</a>
	</div>
</div>

<?php echo form_close() ?>