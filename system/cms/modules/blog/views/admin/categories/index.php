<?php

?>
<div class="page-header">
	<h1><?php echo lang('cat:list_title') ?></h1>
	
	<div class="btn-group content-toolbar">
		<a class="btn btn-yellow btn-sm" href="<?php echo base_url(); ?>admin/blog/categories/create">
			<i class="icon-plus"></i>
			<span class="no-text-shadow"><?php echo lang('cat:create_title'); ?></span>
		</a>
	</div>
	<?php file_partial('shortcuts'); ?>
</div>
	
<?php if ($categories): ?>

	<?php echo form_open('admin/blog/categories/delete',array('id'=>'form_action_categories')) ?>

	<table id="category_list" class="table table-striped table-bordered table-hover">
		<thead>
		<tr>
			<th class="center" width="20"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all')) ?></th>
			<th><span style="margin-left:20px;"><?php echo lang('cat:category_label') ?></span></th>
			<th><?php echo lang('global:slug') ?></th>
			<th width="120"></th>
		</tr>
		</thead>
		<tbody>
			<?php foreach ($categories as $category): ?>
			<tr data-tt-id="<?php echo $category->id; ?>" <?php echo (isset($category->parent_id)) ? 'data-tt-parent-id="'.$category->parent_id.'"' : ''; ?>>
				<td class="center"><?php echo form_checkbox('action_to[]', $category->id) ?></td>
				<td><?php echo trim($category->title) ?></td>
				<td><?php echo $category->slug ?></td>
				<td class="align-center buttons buttons-small">
					<?php echo anchor('admin/blog/categories/edit/'.$category->id, lang('global:edit'), 'class="btn btn-xs btn-blue"') ?>
					<?php echo anchor('admin/blog/categories/delete/'.$category->id, lang('global:delete'), 'class="confirm btn btn-xs btn-danger"') ;?>
				</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	<?php $this->load->view('admin/partials/pagination') ?>

	<div class="table_action_buttons">
		<button disabled="" type="submit" name="btnAction" value="delete" class="btn btn-sm btn-danger confirm-delete">
			<span>Hapus</span>
		</button>
	</div>

	<?php echo form_close() ?>

<?php else: ?>
	<div class="well"><?php echo lang('cat:no_categories') ?></div>
<?php endif ?>

<script type="text/javascript">
$('#category_list').treetable(
	{expandable : true, column : 1});


	$(document).ready(function(){
		$(".confirm-delete").on(ace.click_event, function(event) {
			event.preventDefault();
			bootbox.confirm("Anda yakin akan menjalankan aksi ini? Aksi ini tidak dapat dikembalikan.", function(result) {
				console.log(result);
				if(result) {
					var form = $('#form_action_categories');
					form.append('<input type="hidden" name="btnAction" value="delete">');
					form.unbind('submit');
					form.submit();
				}
			});
		});
	});

</script>