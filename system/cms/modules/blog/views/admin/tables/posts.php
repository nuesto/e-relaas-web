	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th class="center" width="20"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all')) ?></th>
				<th><?php echo lang('blog:post_label') ?></th>
				<th><?php echo lang('blog:category_label') ?></th>
				<th><?php echo lang('blog:date_label') ?></th>
				<?php if($this->config->item('with_organization')) { ?>
				<?php if(group_has_role('blog','view_all_post')) { ?>
				<th><?php echo lang('blog:organization_label') ?></th>
				<?php } ?>
				<?php } ?>
				<th><?php echo lang('blog:written_by_label') ?></th>
				<th><?php echo lang('blog:status_label') ?></th>
				<th width="180"><?php echo lang('global:actions') ?></th>
			</tr>
		</thead>

		<tbody>			
			<?php foreach ($blog as $post) : ?>
			<tr>
				<td class="center"><?php echo form_checkbox('action_to[]', $post->id) ?></td>
				<td><?php echo $post->title ?></td>
				<td><?php echo ($post->category_title) ? $post->category_title : '-'; ?></td>
				<td><?php echo format_date($post->created_on) ?></td>
				
				<?php if($this->config->item('with_organization')) { ?>
				<?php if(group_has_role('blog','view_all_post')) { ?>
				<td><?php echo ($post->unit_name) ? $post->unit_name : '-'; ?></td>
				<?php } ?>
				<?php } ?>

				<td>
				<?php if (isset($post->display_name)): ?>
					<?php echo anchor('user/'.$post->username, $post->display_name, 'target="_blank"') ?>
				<?php else: ?>
					<?php echo lang('blog:author_unknown') ?>
				<?php endif ?>
				</td>
				<td><?php echo lang('blog:'.$post->status.'_label') ?></td>
				<td style="padding-top:10px;">
					<?php if($post->status=='live') : ?>
						<a href="<?php echo site_url('blog/'.date('Y/m', $post->created_on).'/'.$post->slug) ?>" title="<?php echo lang('global:view')?>" class="btn btn-xs btn-info" target="_blank"><?php echo lang('global:view')?></a>
					<?php else: ?>
						<a href="<?php echo site_url('blog/preview/' . $post->preview_hash) ?>" title="<?php echo lang('global:preview')?>" class="btn btn-xs btn-info"target="_blank"><?php echo lang('global:preview')?></a>
					<?php endif ?>

					<?php
					if(group_has_role('blog', 'edit_all_post')){
						echo anchor('admin/blog/edit/' . $post->id, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
					}elseif (group_has_role('blog','edit_own_unit_post')) {
						if($this->organization->is_member($this->current_user->id,$post->id_unit)) {
							echo anchor('admin/blog/edit/' . $post->id, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
						}
					}elseif(group_has_role('blog', 'edit_own_post')){
						if($post->created_by == $this->current_user->id){
							echo anchor('admin/blog/edit/' . $post->id, lang('global:edit'), 'class="btn btn-xs btn-info edit"');
						}
					}
					?>

					<?php
					if(group_has_role('blog', 'delete_all_post')){
						echo anchor('admin/blog/delete/' . $post->id, lang('global:delete'), 'class="btn btn-xs btn-danger confirm"');
					}elseif (group_has_role('blog','edit_own_unit_post')) {
						if($this->organization->is_member($this->current_user->id,$post->id_unit)) {
							echo anchor('admin/blog/delete/' . $post->id, lang('global:delete'), 'class="btn btn-xs btn-danger confirm"');
						}
					}elseif(group_has_role('blog', 'delete_own_post')){
						if($post->created_by == $this->current_user->id){
							echo anchor('admin/blog/delete/' . $post->id, lang('global:delete'), 'class="btn btn-xs btn-danger confirm"');
						}
					}
					?>
				</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	<?php $this->load->view('admin/partials/pagination') ?>

	<div class="table_action_buttons">
		<?php if(group_has_role('blog','delete_all_post') OR group_has_role('blog','delete_own_post')) { ?>
		<button type="submit" name="btnAction" value="delete" class="btn btn-sm btn-danger confirm-delete">
			<span>Hapus</span>
		</button>
		<?php } ?>
		
		<?php if(group_has_role('blog','publish_all_post') OR group_has_role('blog','publish_own_unit_post') OR group_has_role('blog','publish_own_post')) { ?>
		<button type="submit" name="btnAction" value="publish" class="btn btn-sm btn-info">
			<span>Publikasikan</span>
		</button>
		<?php } ?>
		<?php //$this->load->view('admin/partials/buttons', array('buttons' => array('delete', 'publish'))) ?>
	</div>
	<script>
	$(document).ready(function(){
		$(".confirm-delete").on(ace.click_event, function(event) {
			event.preventDefault();
			bootbox.confirm("Anda yakin akan menjalankan aksi ini? Aksi ini tidak dapat dikembalikan.", function(result) {
				if(result) {
					var form = $('#form_action_post');
					form.append('<input type="hidden" name="btnAction" value="delete">');
					form.unbind('submit');
					form.submit();
				}
			});
		});
	});
	</script>