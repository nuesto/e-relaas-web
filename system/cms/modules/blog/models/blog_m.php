<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Blog\Models
 */
class Blog_m extends MY_Model
{
	protected $_table = 'blog';

	public function get_all()
	{
		$this->db
			->select('blog.*, blog_categories.title AS category_title, blog_categories.slug AS category_slug')
			->select('users.username, profiles.display_name')
			->select('organization_units.unit_name,organization_units.unit_slug')
			->join('blog_categories', 'blog.category_id = blog_categories.id', 'left')
			->join('profiles', 'profiles.user_id = blog.author_id', 'left')
			->join('users', 'blog.author_id = users.id', 'left')
			->join('organization_units', 'blog.id_unit = organization_units.id', 'left')
			->join('blog_translation','blog.id=blog_translation.id_blog','left')
			->order_by('created_on', 'DESC');

		return $this->db->get('blog')->result();
	}

	public function get($id)
	{
		return $this->db
			->select('blog.*, users.username, profiles.display_name')
			->select('organization_units.unit_name,organization_units.unit_slug')
			->join('profiles', 'profiles.user_id = blog.author_id', 'left')
			->join('users', 'blog.author_id = users.id', 'left')
			->join('organization_units', 'blog.id_unit = organization_units.id', 'left')
			->join('blog_translation','blog.id=blog_translation.id_blog','left')
			->where('blog.id', $id)
			->get('blog')
			->row();
	}

	public function get_by($key = null, $value = null)
	{
		$this->db
			->select('blog.*, users.username, profiles.display_name')
			->select('organization_units.unit_name,organization_units.unit_slug')
			->join('profiles', 'profiles.user_id = blog.author_id', 'left')
			->join('users', 'blog.author_id = users.id', 'left')
			->join('organization_units', 'blog.id_unit = organization_units.id', 'left')
			->join('blog_translation','blog.id=blog_translation.id_blog','left');

		if (is_array($key))
		{
			$this->db->where($key);
		}
		elseif (is_string($key) AND $value == null)
		{
			$this->db->where($key,null,false);
		}
		else
		{
			$this->db->where($key, $value);
		}

		return $this->db->get($this->_table)->row();
	}

	public function get_many_by($params = array())
	{
		if ( ! empty($params['category']))
		{
			if (is_numeric($params['category']))
			{
				$this->db->where('blog_categories.id', $params['category']);
			}
			else
			{
				$this->db->where('blog_categories.slug', $params['category']);
			}
		}

		if ( ! empty($params['month']))
		{
			$this->db->where('MONTH(FROM_UNIXTIME('.$this->db->dbprefix('blog').'.created_on))', $params['month']);
		}

		if ( ! empty($params['year']))
		{
			$this->db->where('YEAR(FROM_UNIXTIME('.$this->db->dbprefix('blog').'.created_on))', $params['year']);
		}

		if ( ! empty($params['keywords']))
		{
			$this->db
				->like('blog.title', trim($params['keywords']))
				->or_like('profiles.display_name', trim($params['keywords']));
		}

		// Is a status set?
		if ( ! empty($params['status']))
		{
			// If it's all, then show whatever the status
			if ($params['status'] != 'all')
			{
				// Otherwise, show only the specific status
				$this->db->where('status', $params['status']);
			}
		}

		// Nothing mentioned, show live only (general frontend stuff)
		else
		{
			$this->db->where('status', 'live');
		}

		// By default, dont show future posts
		if ( ! isset($params['show_future']) || (isset($params['show_future']) && $params['show_future'] == false))
		{
			$this->db->where('blog.created_on <=', now());
		}

		// Limit the results based on created by
		if ( ! empty($params['created_by'])) {
			$this->db->where('blog.created_by',$params['created_by']);
		}

		if ( ! empty($params['id_unit'])) {
			if(is_array($params['id_unit'])) {
				$this->db->where_in('blog.id_unit',$params['id_unit']);
			} else {
				$this->db->where('blog.id_unit',$params['id_unit']);
			}
		}

		// Limit the results based on 1 number or 2 (2nd is offset)
		if (isset($params['limit']) && is_array($params['limit']))
		{
			$this->db->limit($params['limit'][0], $params['limit'][1]);
		}
		elseif (isset($params['limit']))
		{
			$this->db->limit($params['limit']);
		}

		// Exclude some data
		if ( ! empty($params['exclude']))
		{
			$this->db->where($params['exclude']['field'].' NOT IN ('. $params['exclude']['value'].')', null, false);
		}

		// Custom where
		if ( ! empty($params['custom']))
		{
			$this->db->where($params['custom'], null, false);
		}

		return $this->get_all();
	}

	public function count_tagged_by($tag, $params)
	{
		return $this->select('*')
			->from('blog')
			->join('keywords_applied', 'keywords_applied.hash = blog.keywords')
			->join('keywords', 'keywords.id = keywords_applied.keyword_id')
			->where('keywords.name', str_replace('-', ' ', $tag))
			->where($params)
			->count_all_results();
	}

	public function get_tagged_by($tag, $params)
	{
		return $this->db->select('blog.*, blog.title title, blog.slug slug, blog_categories.title category_title, blog_categories.slug category_slug, profiles.display_name')
			->from('blog')
			->join('keywords_applied', 'keywords_applied.hash = blog.keywords')
			->join('keywords', 'keywords.id = keywords_applied.keyword_id')
			->join('blog_categories', 'blog_categories.id = blog.category_id', 'left')
			->join('profiles', 'profiles.user_id = blog.author_id', 'left')
			->where('keywords.name', str_replace('-', ' ', $tag))
			->where($params)
			->get()
			->result();
	}

	public function count_by($params = array())
	{
		$this->db->join('blog_categories', 'blog.category_id = blog_categories.id', 'left')
		// we need the display name joined so we can get an accurate count when searching
			->join('profiles', 'profiles.user_id = blog.author_id');

		if ( ! empty($params['category']))
		{
			if (is_numeric($params['category']))
			{
				$this->db->where('blog_categories.id', $params['category']);
			}
			else
			{
				$this->db->where('blog_categories.slug', $params['category']);
			}
		}

		if ( ! empty($params['month']))
		{
			$this->db->where('MONTH(FROM_UNIXTIME('.$this->db->dbprefix('blog').'.created_on))', $params['month']);
		}

		if ( ! empty($params['year']))
		{
			$this->db->where('YEAR(FROM_UNIXTIME('.$this->db->dbprefix('blog').'.created_on))', $params['year']);
		}

		if ( ! empty($params['keywords']))
		{
			$this->db
				->like('blog.title', trim($params['keywords']))
				->or_like('profiles.display_name', trim($params['keywords']));
		}

		// Is a status set?
		if ( ! empty($params['status']))
		{
			// If it's all, then show whatever the status
			if ($params['status'] != 'all')
			{
				// Otherwise, show only the specific status
				$this->db->where('status', $params['status']);
			}
		}

		// Nothing mentioned, show live only (general frontend stuff)
		else
		{
			$this->db->where('status', 'live');
		}

		return $this->db->count_all_results('blog');
	}

	/**
	 * Insert new entry
	 * @param array $extra The blog entry.
	 * 
	 * @return id
	 */
	public function insert_entry($input)
	{
		$this->db->trans_start();

		// They are trying to put this live
		if ($this->input->post('status') == 'live')
		{
			if(!group_has_role('blog','publish_all_post') AND !group_has_role('blog','publish_own_post')) {
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin/blog/index');
			}

			$hash = "";
		}
		else
		{
			$hash = $this->_preview_hash();
		}

		if ($this->input->post('created_on'))
		{
			$created_on = strtotime(sprintf('%s %s', $this->input->post('created_on'), $this->input->post('created_on_time')));
		}
		else
		{
			$created_on = now();
		}

		// set title, slug, and body
		if(count(explode(",",$input['available_lang']))>1) { 
			$input['title'] = $input['title_'.$this->settings->get('site_lang')];
			$input['slug'] = $input['slug_'.$this->settings->get('site_lang')];
			$input['body'] = $input['body_'.$this->settings->get('site_lang')];
			$input['intro'] = $input['intro_'.$this->settings->get('site_lang')];
		}

		$blog = array(
			'title'            => $input['title'],
			'slug'             => $input['slug'],
			'category_id'      => ($input['category_id'] != '0') ? $input['category_id'] : NULL,
			'keywords'         => Keywords::process($input['keywords']),
			'body'             => $input['body'],
			'intro'             => $input['intro'],
			'image'             => $input['image'],
			'status'           => $input['status'],
			'created_on'       => $created_on,
			'created'		   => date('Y-m-d H:i:s',$created_on),
			'comments_enabled' => $input['comments_enabled'],
			'author_id'        => $this->current_user->id,
			'type'             => $input['type'],
			'parsed'           => ($input['type'] == 'markdown') ? parse_markdown($input['body']) : '',
			'preview_hash'     => $hash,
			'id_unit'		   => ($input['id_unit'] != '') ? $input['id_unit'] : null,
			);
		
		$this->db->insert('blog',$blog);

		$id = $this->db->insert_id();
		// did it pass validation?
		if ( ! $id) return false;

		// save the translation
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		// for default lang
		$translation_content = array(
					'title_'.$this->settings->get('site_lang') => $input['title'],
					'slug_'.$this->settings->get('site_lang') => $input['slug'],
					'body_'.$this->settings->get('site_lang') => $input['body'],
					'intro_'.$this->settings->get('site_lang') => $input['intro'],
				);
		$translation_lang = array(
			'id_blog' => $id,
			'lang_code' => $this->settings->get('site_lang'),
			'lang_name' => $lang_list[$this->settings->get('site_lang')],
			'translation' => json_encode($translation_content)
			);
		$this->db->insert('blog_translation',$translation_lang);

		// for add lang
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		array_splice($public_lang, array_search($this->settings->get('site_lang'), $public_lang),1);
		
		$translation_lang = array();
		foreach ($public_lang as $pl) {
			$translation_content = array(
						'title_'.$pl => $input['title_'.$pl],
						'slug_'.$pl => $input['slug_'.$pl],
						'body_'.$pl => $input['body_'.$pl],
						'intro_'.$pl => $input['intro_'.$pl],
					);
			$translation_lang = array(
				'id_blog' => $id,
				'lang_code' => $pl,
				'lang_name' => $lang_list[$pl],
				'translation' => json_encode($translation_content)
				);
			$this->db->insert('blog_translation',$translation_lang);
		}

		$this->db->trans_complete();

		return ($this->db->trans_status() === false) ? false : $id;
	}

	/**
	 * Insert new entry
	 * @param array $extra The blog entry.
	 * 
	 * @return id
	 */
	public function update_entry($id, $input, $post = null)
	{
		$this->db->trans_start();

		// If we have keywords before the update, we'll want to remove them from keywords_applied
		$old_keywords_hash = (trim($post->keywords) != '') ? $post->keywords : null;

		$post->keywords = Keywords::get_string($post->keywords);

		// They are trying to put this live
		$hash = $this->input->post('preview_hash');

		if ($this->input->post('created_on'))
		{
			$created_on = strtotime(sprintf('%s %s', $this->input->post('created_on'), $this->input->post('created_on_time')));
		}
		else
		{
			$created_on = now();
		}

		$author_id = empty($post->display_name) ? $this->current_user->id : $post->author_id;

		$blog = array(
			'title'            => $this->input->post('title'),
			'slug'             => $this->input->post('slug'),
			'category_id'      => ($this->input->post('category_id') != '0') ? $this->input->post('category_id') : NULL,
			'keywords'         => Keywords::process($this->input->post('keywords'), $old_keywords_hash),
			'body'             => $this->input->post('body'),
			'image'             => $this->input->post('image'),
			'status'           => $this->input->post('status'),
			'created_on'       => $created_on,
			'updated_on'       => $created_on,
			'created'		   => date('Y-m-d H:i:s', $created_on),
			'updated'		   => date('Y-m-d H:i:s', $created_on),
			'comments_enabled' => $this->input->post('comments_enabled'),
			'author_id'        => $author_id,
			'type'             => $this->input->post('type'),
			'parsed'           => ($this->input->post('type') == 'markdown') ? parse_markdown($this->input->post('body')) : '',
			'preview_hash'     => $hash,
			'id_unit'		   => ($this->input->post('id_unit') != '') ? $this->input->post('id_unit') : null,
		);

		// set title, slug, and body
		if(count(explode(",",$input['available_lang']))>1) { 
			$blog['title'] = $input['title_'.$this->settings->get('site_lang')];
			$blog['slug'] = $input['slug_'.$this->settings->get('site_lang')];
			$blog['body'] = $input['body_'.$this->settings->get('site_lang')];
			$blog['intro'] = $input['intro_'.$this->settings->get('site_lang')];
		}

		$this->db->where('id',$id);
		$result = $this->db->update('blog',$blog);

		if ( ! $result) return false;

		// save the translation
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		// for default lang
		$translation_content = array(
					'title_'.$this->settings->get('site_lang') => $blog['title'],
					'slug_'.$this->settings->get('site_lang') => $blog['slug'],
					'body_'.$this->settings->get('site_lang') => $blog['body'],
					'intro_'.$this->settings->get('site_lang') => $blog['intro'],
				);
		$translation_lang = array(
			'id_blog' => $id,
			'lang_code' => $this->settings->get('site_lang'),
			'lang_name' => $lang_list[$this->settings->get('site_lang')],
			'translation' => json_encode($translation_content)
			);
		// check whether there is data for default_lang
		$this->db->where('id_blog',$id);
		$this->db->where('lang_code',$this->settings->get('site_lang'));
		$translation = $this->db->get('blog_translation')->result();
		if(count($translation)<1) {
			// if there is not, then insert it
			$this->db->insert('blog_translation',$translation_lang);
		} else {
			$this->db->where('id_blog',$id);
			$this->db->where('lang_code',$this->settings->get('site_lang'));
			$this->db->update('blog_translation',$translation_lang);
		}

		// for add lang
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		array_splice($public_lang, array_search($this->settings->get('site_lang'), $public_lang),1);
		
		$translation_lang = array();
		foreach ($public_lang as $pl) {
			$translation_content = array(
						'title_'.$pl => $input['title_'.$pl],
						'slug_'.$pl => $input['slug_'.$pl],
						'body_'.$pl => $input['body_'.$pl],
						'intro_'.$pl => $input['intro_'.$pl],
					);
			$translation_lang = array(
				'id_blog' => $id,
				'lang_code' => $pl,
				'lang_name' => $lang_list[$pl],
				'translation' => json_encode($translation_content)
				);

			$this->db->where('id_blog',$id);
			$this->db->where('lang_code',$pl);
			$translation = $this->db->get('blog_translation')->result();
			if(count($translation)<1) {
				$this->db->insert('blog_translation',$translation_lang);
			} else {
				$this->db->where('id_blog',$id);
				$this->db->where('lang_code',$pl);
				$this->db->update('blog_translation',$translation_lang);
			}
		}

		$this->db->trans_complete();

		return (bool)$this->db->trans_status();
	}

	public function update($id, $input, $skip_validation = false)
	{
		$input['updated_on'] = now();
		if ($input['status'] == "live" and $input['preview_hash'] != '') {
			$input['preview_hash'] = '';
		}

		return parent::update($id, $input);
	}

	public function delete($id) {
		// delete translation
		$this->db->where('id_blog',$id);
		$this->db->delete('blog_translation');
		
		return parent::delete($id);
	}

	public function publish($id = 0)
	{
		return parent::update($id, array('status' => 'live', 'preview_hash' => ''));
	}

	// -- Archive ---------------------------------------------

	public function get_archive_months()
	{
		$this->db->select('UNIX_TIMESTAMP(DATE_FORMAT(FROM_UNIXTIME(t1.created_on), "%Y-%m-02")) AS `date`', false);
		$this->db->from('blog t1');
		$this->db->distinct();
		$this->db->select('(SELECT count(id) FROM '.$this->db->dbprefix('blog').' t2
							WHERE MONTH(FROM_UNIXTIME(t1.created_on)) = MONTH(FROM_UNIXTIME(t2.created_on))
								AND YEAR(FROM_UNIXTIME(t1.created_on)) = YEAR(FROM_UNIXTIME(t2.created_on))
								AND status = "live"
								AND created_on <= '.now().'
							) as post_count');

		$this->db->where('status', 'live');
		$this->db->where('created_on <=', now());
		$this->db->having('post_count >', 0);
		$this->db->order_by('t1.created_on DESC');
		$query = $this->db->get();

		return $query->result();
	}

	public function check_exists($field, $value = '', $id = 0)
	{
		if (is_array($field))
		{
			$params = $field;
			$id = $value;
		}
		else
		{
			$params[$field] = $value;
		}
		$params['id !='] = (int)$id;

		return parent::count_by($params) == 0;
	}

	/**
	 * Searches blog posts based on supplied data array
	 *
	 * @param $data array
	 *
	 * @return array
	 */
	public function search($data = array())
	{
		if (array_key_exists('category_id', $data))
		{
			$this->db->where('category_id', $data['category_id']);
		}

		if (array_key_exists('status', $data))
		{
			$this->db->where('status', $data['status']);
		}

		if (array_key_exists('keywords', $data))
		{
			$matches = array();
			if (strstr($data['keywords'], '%'))
			{
				preg_match_all('/%.*?%/i', $data['keywords'], $matches);
			}

			if ( ! empty($matches[0]))
			{
				foreach ($matches[0] as $match)
				{
					$phrases[] = str_replace('%', '', $match);
				}
			}
			else
			{
				$temp_phrases = explode(' ', $data['keywords']);
				foreach ($temp_phrases as $phrase)
				{
					$phrases[] = str_replace('%', '', $phrase);
				}
			}

			$counter = 0;
			foreach ($phrases as $phrase)
			{
				if ($counter == 0)
				{
					$this->db->like('blog.title', $phrase);
				}
				else
				{
					$this->db->or_like('blog.title', $phrase);
				}

				$this->db->or_like('blog.body', $phrase);
				$this->db->or_like('blog.intro', $phrase);
				$this->db->or_like('profiles.display_name', $phrase);
				$counter++;
			}
		}

		return $this->get_all();
	}

	/**
	 * Generate a preview hash
	 *
	 * @return bool
	 */
	private function _preview_hash()
	{
		return md5(microtime() + mt_rand(0, 1000));
	}

	/**
	 * Get the page translation
	 * @param int $id The page id.
	 * 
	 * @return object
	 */
	public function get_post_translation($id, $lang_code = NULL)
	{
		if($lang_code) {

			$this->db->where('blog_translation.lang_code', $lang_code);
		}
		return $this->db
			->select('blog_translation.*')
			->where('id_blog', $id)
			->join('blog_translation','blog.id=blog_translation.id_blog','left')
			->get('blog')
			->result();
	}

}