<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Categories model
 *
 * @author  Phil Sturgeon
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Blog\Models
 */
class Blog_categories_m extends MY_Model
{
	public function get_all()
	{
		$this->db
			->select('blog_categories.*, blog_categories_categories.category_id parent_id')

			->order_by('blog_categories.title');

		$this->db->join('blog_categories_categories', 'blog_categories.id=blog_categories_categories.row_id','left');
		$this->db->join('blog_categories_translation', 'blog_categories.id=blog_categories_translation.id_category','left');

		return $this->db->get('blog_categories')->result();
	}

	public function get($id)
	{
		return $this->db
			->select('blog_categories.*, blog_categories_categories.category_id as parent_id')
			->join('blog_categories_categories', 'blog_categories.id=blog_categories_categories.row_id','left')
			->where('blog_categories.id', $id)
			->get('blog_categories')
			->row();
	}

	/**
	 * Insert a new category into the database
	 *
	 * @param array $input The data to insert
	 * @param bool  $skip_validation
	 *
	 * @return string
	 */
	public function insert($input = array(), $skip_validation = false)
	{
		$this->db->trans_start();

		// set title, slug, and body
		if(count(explode(",",$input['available_lang']))>1) {
			$input['title'] = $input['title_'.$this->settings->get('site_lang')];
			$input['slug'] = $input['slug_'.$this->settings->get('site_lang')];
		}

		parent::insert(array(
			'title' => $input['title'],
			'slug' => $input['slug'],
		));

		$id = $this->db->insert_id();

		if($input['parent_id']!='') {
			$this->db->insert('blog_categories_categories', array('row_id'=>$id, 'category_id'=>$input['parent_id']));
		}

		// save the translation
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		// for default lang
		$translation_content = array(
					'title_'.$this->settings->get('site_lang') => $input['title'],
					'slug_'.$this->settings->get('site_lang') => $input['slug'],
				);
		$translation_lang = array(
			'id_category' => $id,
			'lang_code' => $this->settings->get('site_lang'),
			'lang_name' => $lang_list[$this->settings->get('site_lang')],
			'translation' => json_encode($translation_content)
			);
		$this->db->insert('blog_categories_translation',$translation_lang);

		// for add lang
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		array_splice($public_lang, array_search($this->settings->get('site_lang'), $public_lang),1);

		$translation_lang = array();
		foreach ($public_lang as $pl) {
			$translation_content = array(
						'title_'.$pl => $input['title_'.$pl],
						'slug_'.$pl => $input['slug_'.$pl],
					);
			$translation_lang = array(
				'id_category' => $id,
				'lang_code' => $pl,
				'lang_name' => $lang_list[$pl],
				'translation' => json_encode($translation_content)
				);
			$this->db->insert('blog_categories_translation',$translation_lang);
		}

		$this->db->trans_complete();

		return ($this->db->trans_status() === false) ? false : $input['title'];
	}

	/**
	 * Update an existing category
	 *
	 * @param int   $id    The ID of the category
	 * @param array $input The data to update
	 * @param bool  $skip_validation
	 *
	 * @return bool
	 */
	public function update($id, $input, $skip_validation = false)
	{
		$this->db->trans_start();

		$this->db->where('row_id',$id);
		$this->db->delete('blog_categories_categories');

		if($input['parent_id']!='') {

			$this->db->insert('blog_categories_categories', array('row_id'=>$id, 'category_id'=>$input['parent_id']));
		}

		// set title, slug, and body
		if(count(explode(",",$input['available_lang']))>1) {
			$input['title'] = $input['title_'.$this->settings->get('site_lang')];
			$input['slug'] = $input['slug_'.$this->settings->get('site_lang')];
		}

		$result = parent::update($id, array(
			'title' => $input['title'],
			'slug' => $input['slug'],
		));

		if ( ! $result) return false;

		// save the translation
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		// for default lang
		$translation_content = array(
					'title_'.$this->settings->get('site_lang') => $input['title'],
					'slug_'.$this->settings->get('site_lang') => $input['slug'],
				);
		$translation_lang = array(
			'id_category' => $id,
			'lang_code' => $this->settings->get('site_lang'),
			'lang_name' => $lang_list[$this->settings->get('site_lang')],
			'translation' => json_encode($translation_content)
			);
		// check whether there is data for default_lang
		$this->db->where('id_category',$id);
		$this->db->where('lang_code',$this->settings->get('site_lang'));
		$translation = $this->db->get('blog_categories_translation')->result();
		if(count($translation)<1) {
			// if there is not, then insert it
			$this->db->insert('blog_categories_translation',$translation_lang);
		} else {
			$this->db->where('id_category',$id);
			$this->db->where('lang_code',$this->settings->get('site_lang'));
			$this->db->update('blog_categories_translation',$translation_lang);
		}

		// for add lang
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		array_splice($public_lang, array_search($this->settings->get('site_lang'), $public_lang),1);

		$translation_lang = array();
		foreach ($public_lang as $pl) {
			$translation_content = array(
						'title_'.$pl => $input['title_'.$pl],
						'slug_'.$pl => $input['slug_'.$pl],
					);
			$translation_lang = array(
				'id_category' => $id,
				'lang_code' => $pl,
				'lang_name' => $lang_list[$pl],
				'translation' => json_encode($translation_content)
				);

			$this->db->where('id_category',$id);
			$this->db->where('lang_code',$pl);
			$translation = $this->db->get('blog_categories_translation')->result();
			if(count($translation)<1) {
				$this->db->insert('blog_categories_translation',$translation_lang);
			} else {
				$this->db->where('id_category',$id);
				$this->db->where('lang_code',$pl);
				$this->db->update('blog_categories_translation',$translation_lang);
			}
		}

		$this->db->trans_complete();

		return (bool)$this->db->trans_status();
	}

	/**
	 * Callback method for validating the title
	 *
	 * @param string $title The title to validate
	 * @param int    $id    The id to check
	 *
	 * @return mixed
	 */
	public function check_title($title = '', $id = 0)
	{
		return (bool)$this->db->where('title', $title)
			->where('id != ', $id)
			->from('blog_categories')
			->count_all_results();
	}

	/**
	 * Callback method for validating the slug
	 *
	 * @param string $slug The slug to validate
	 * @param int    $id   The id to check
	 *
	 * @return bool
	 */
	public function check_slug($slug = '', $id = 0)
	{
		return (bool)$this->db->where('slug', $slug)
			->where('id != ', $id)
			->from('blog_categories')
			->count_all_results();
	}

	/**
	 * Insert a new category into the database via ajax
	 *
	 * @param array $input The data to insert
	 *
	 * @return int
	 */
	public function insert_ajax($input = array())
	{
		return parent::insert(array(
			'title' => $input['title'],
			//is something wrong with convert_accented_characters?
			//'slug'=>url_title(strtolower(convert_accented_characters($input['title'])))
			'slug' => url_title(strtolower($input['title']))
		));
	}

	// ----------------------------------------------------------------------------------------

	public function delete($id = 0) {
		$params['parent'] = $id;
		$children = $this->get_categories_by($params);

		// delete direct child of category, make them as parent
		$this->db->trans_start();
		foreach ($children as $child) {
			$this->db->where('category_id', $id);
			$this->db->delete('blog_categories_categories');
		}

		// delete category
		$this->db->where('id', $id);
		$this->db->delete('blog_categories');

		// delete translation
		$this->db->where('id_category',$id);
		$this->db->delete('blog_categories_translation');
		$this->db->trans_complete();

		return $this->db->trans_status();
	}

	public function get_categories_by($params = array()) {
		if(! empty($params['parent'])) {
			$this->db->where('blog_categories_categories.category_id', $params['parent']);
		}

		if(! empty($params['except'])) {
			$this->db->where($params['except']['field'].' NOT IN '.$params['except']['value'], null, false);
		}

		if(! empty($params['slug'])) {
			$this->db->where("(".$this->db->dbprefix('blog_categories').".slug like '%".$params['slug']."%' OR ". $this->db->dbprefix('blog_categories_translation').".translation like '%\"slug_%\":\"".$params['slug']."\"%')", null, false);
		}

		if(! empty($params['lang'])) {
			$this->db->where("blog_categories_translation.lang_code", $params['lang']);
		}

		// Limit the results based on 1 number or 2 (2nd is offset)
		if (isset($params['limit']) && is_array($params['limit']))
		{
			$this->db->limit($params['limit'][0], $params['limit'][1]);
		}
		elseif (isset($params['limit']))
		{
			$this->db->limit($params['limit']);
		}

		return $this->get_all();
	}



	public function generate_categories_tree($category_tree, $categories, $level = 0, $id_now = NULL, $lang = NULL) {
		foreach ($categories as $category) {

			$prefix = '';
			for($i = 0; $i < $level; $i++){
				$prefix .= '&nbsp;&nbsp;&nbsp;&nbsp;';
			}

			$category->title = $prefix.$category->title;

			$category_tree[] = $category;
			$params['parent'] = $category->id;
			if(! empty($id_now)) {
				$params['except'] = array('field'=>$this->db->dbprefix('blog_categories_categories').'.category_id','value'=>'('.$id_now.')');
			}
			if(! empty($lang)) {
				$params['lang'] = $lang;
			}
			$children = $this->get_categories_by($params);

			if(count($children) == 0){
				continue;
			}else{
				$category_tree = $this->generate_categories_tree($category_tree, $children, $level+1, $id_now);
			}
		}

		return $category_tree;
	}

	/**
	 * Get the page translation
	 * @param int $id The page id.
	 *
	 * @return object
	 */
	public function get_category_translation($id, $lang_code = NULL)
	{
		if($lang_code) {

			$this->db->where('blog_categories_translation.lang_code', $lang_code);
		}
		return $this->db
			->select('blog_categories_translation.*')
			->where('blog_categories.id', $id)
			->join('blog_categories_translation','blog_categories.id=blog_categories_translation.id_category','left')
			->get('blog_categories')
			->result();
	}
}
