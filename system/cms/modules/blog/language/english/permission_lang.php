<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Blog Permissions
$lang['blog:role_put_live']		= 'Put articles live';
$lang['blog:role_edit_live']	= 'Edit live articles';
$lang['blog:role_delete_live'] 	= 'Delete live articles';

$lang['blog:role_create_post']			= 'Create Articles';
$lang['blog:role_create_own_unit_post']			= 'Create Articles Own Unit';
$lang['blog:role_view_all_post']		= 'View All Articles';
$lang['blog:role_view_own_post']		= 'View Own Articles';
$lang['blog:role_view_own_unit_post']	= 'View Own Unit Articles';
$lang['blog:role_edit_all_post']		= 'Edit All Articles';
$lang['blog:role_edit_own_post']		= 'Edit Own Articles';
$lang['blog:role_edit_own_unit_post']	= 'Edit Own Unit Articles';
$lang['blog:role_delete_all_post']		= 'Delete All Articles';
$lang['blog:role_delete_own_post']		= 'Delete Own Articles';
$lang['blog:role_delete_own_unit_post']	= 'Delete Own Unit Articles';
$lang['blog:role_manage_categories']	= 'Manage Categories';
$lang['blog:role_publish_all_post']		= 'Publish All Articles';
$lang['blog:role_publish_own_post']		= 'Publish Own Articles';
$lang['blog:role_publish_own_unit_post']	= 'Publish Own Unit Articles';