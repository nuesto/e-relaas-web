<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Blog Permissions
$lang['blog:role_put_live']		= 'Publikasikan artikel';
$lang['blog:role_edit_live']	= 'Edit artikel terpublikasi';
$lang['blog:role_delete_live'] 	= 'Hapus artikel terpublikasi';

$lang['blog:role_create_post']			= 'Tambah Artikel';
$lang['blog:role_create_own_unit_post']	= 'Tambah Artikel Unit Sendiri';
$lang['blog:role_view_all_post']		= 'Lihat Semua Artikel';
$lang['blog:role_view_own_post']		= 'Lihat Artikel Sendiri';
$lang['blog:role_view_own_unit_post']	= 'Lihat Artikel Unit Sendiri';
$lang['blog:role_edit_all_post']		= 'Edit Semua Artikel';
$lang['blog:role_edit_own_post']		= 'Edit Artikel Sendiri';
$lang['blog:role_edit_own_unit_post']	= 'Edit Artikel Unit Sendiri';
$lang['blog:role_delete_all_post']		= 'Delete Semua Artikel';
$lang['blog:role_delete_own_post']		= 'Delete Artikel Sendiri';
$lang['blog:role_delete_own_unit_post']	= 'Delete Artikel Unit Sendiri';
$lang['blog:role_manage_categories']	= 'Kelola Kategori';
$lang['blog:role_publish_all_post']		= 'Publikasikan Semua Artikel';
$lang['blog:role_publish_own_post']		= 'Publikasikan Artikel Sendiri';
$lang['blog:role_publish_own_unit_post']	= 'Publikasikan Artikel Unit Sendiri';