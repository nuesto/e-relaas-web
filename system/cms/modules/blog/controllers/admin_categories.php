<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Admin Page Layouts controller for the Pages module
 *
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Blog\Controllers
 */
class Admin_Categories extends Admin_Controller
{

	/** @var int The current active section */
	protected $section = 'categories';

	/** @var array The validation rules */
	protected $validation_rules = array(
		array(
			'field' => 'title',
			'label' => 'lang:global:title',
			'rules' => 'trim|required|max_length[100]|callback__check_title'
		),
		array(
			'field' => 'slug',
			'label' => 'lang:global:slug',
			'rules' => 'trim|required|max_length[100]|callback__check_slug'
		),
		array(
			'field' => 'id',
			'rules' => 'trim|numeric'
		),
	);

	/**
	 * Every time this controller is called should:
	 * - load the blog_categories model.
	 * - load the categories and blog language files.
	 * - load the form_validation and set the rules for it.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('blog_categories_m');
		$this->lang->load('categories');
		$this->lang->load('blog');

		// Load the validation library along with the rules
		$this->load->library('form_validation');

		if(!group_has_role('blog','manage_categories')) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin/blog/index');
		}
	}

	/**
	 * Index method, lists all categories
	 */
	public function index()
	{
		$this->pyrocache->delete_all('module_m');

		// Create pagination links
		// $total_rows = $this->blog_categories_m->count_all();
		// $pagination = create_pagination('admin/blog/categories/index', $total_rows, Settings::get('records_per_page'), 5);

		// Using this data, get the relevant results
		// $categories = $this->blog_categories_m
		// 						->order_by('title')
		// 						->limit($pagination['limit'])
		// 						->offset($pagination['offset'])
		// 						->get_all();
		$params['except'] = array(
			'field' => $this->db->dbprefix('blog_categories').'.id',
			'value' => '(SELECT IFNULL(row_id,\'\') FROM '.$this->db->dbprefix('blog_categories_categories').')'
			);
		$params['lang'] = Settings::get('site_lang');
		$category_list = $this->blog_categories_m->get_categories_by($params);

		$available_categories = $this->blog_categories_m->generate_categories_tree(array(), $category_list, NULL, NULL, $params['lang']);

		$this->template->append_css('module::treetable/jquery.treetable.css');
		$this->template->append_css('module::blog.css');
		$this->template->append_js('module::treetable/jquery.treetable.js');

		$this->template
			->title($this->module_details['name'], lang('cat:list_title'))
			->set('categories', $available_categories)
			->build('admin/categories/index');
	}

	/**
	 * Create method, creates a new category
	 */
	public function create()
	{
		$category = new stdClass;

		// set validation rules
		$this->_set_validation_rules();

		// Validate the data
		if ($this->form_validation->run())
		{
			if ($id = $this->blog_categories_m->insert($this->input->post()))
			{
				// Fire an event. A new blog category has been created.
				Events::trigger('blog_category_created', $id);

				$this->session->set_flashdata('success', sprintf(lang('cat:add_success'), $this->input->post('title')));
			}
			else
			{
				$this->session->set_flashdata('error', lang('cat:add_error'));
			}

			redirect('admin/blog/categories/index');
		}

		$category = new stdClass();
		$params['except'] = array(
			'field' => $this->db->dbprefix('blog_categories').'.id',
			'value' => '(SELECT IFNULL(row_id,\'\') FROM '.$this->db->dbprefix('blog_categories_categories').')'
			);

		$params['lang'] = $_SESSION['lang_code'];
		$category_list = $this->blog_categories_m->get_categories_by($params);

		$available_categories = $this->blog_categories_m->generate_categories_tree(array(), $category_list, NULL, NULL, $params['lang']);

		// Loop through each validation rule
		foreach ($this->validation_rules as $rule)
		{
			$category->{$rule['field']} = set_value($rule['field']);
		}

		$this->template
			->title($this->module_details['name'], lang('cat:create_title'))
			->set('category', $category)
			->set('available_categories', $available_categories)
			->set('mode', 'create')
			->append_js('jquery/jquery.slugify.js')
			->build('admin/categories/form');
	}

	/**
	 * Edit method, edits an existing category
	 *
	 * @param int $id The ID of the category to edit
	 */
	public function edit($id = 0)
	{
		// Get the category
		$category = $this->blog_categories_m->get($id);

		$params['except'] = array(
			'field' => $this->db->dbprefix('blog_categories').'.id',
			'value' => '(SELECT IFNULL(row_id,\'\') FROM '.$this->db->dbprefix('blog_categories_categories').')'
			);

		$params['lang'] = $_SESSION['lang_code'];

		$category_list = $this->blog_categories_m->get_categories_by($params);

		$available_categories = $this->blog_categories_m->generate_categories_tree(array(), $category_list, NULL, $id, $params['lang']);;

		// ID specified?
		$category or redirect('admin/blog/categories/index');

		$this->form_validation->set_rules('id', 'ID', 'trim|required|numeric');
		// set validation rules
		$this->_set_validation_rules();

		// Validate the results
		if ($this->form_validation->run())
		{
			$this->blog_categories_m->update($id, $this->input->post())
				? $this->session->set_flashdata('success', sprintf(lang('cat:edit_success'), $this->input->post('title')))
				: $this->session->set_flashdata('error', lang('cat:edit_error'));

			// Fire an event. A blog category is being updated.
			Events::trigger('blog_category_updated', $id);

			redirect('admin/blog/categories/index');
		}

		// Loop through each rule
		foreach ($this->validation_rules as $rule)
		{
			if ($this->input->post($rule['field']) !== null)
			{
				$category->{$rule['field']} = $this->input->post($rule['field']);
			}
		}

		// get the translation of that post
		$translation = $this->blog_categories_m->get_category_translation($id);
		$tmp_translation = array();
		foreach ($translation as $trans) {
			$tmp_translation[$trans->lang_code] = $trans;
		}
		$category->translation = $tmp_translation;

		$this->template
			->title($this->module_details['name'], sprintf(lang('cat:edit_title'), $category->title))
			->set('category', $category)
			->set('available_categories', $available_categories)
			->set('mode', 'edit')
			->append_js('jquery/jquery.slugify.js')
			->build('admin/categories/form');
	}

	/**
	 * Delete method, deletes an existing category (obvious isn't it?)
	 *
	 * @param int $id The ID of the category to edit
	 */
	public function delete($id = 0)
	{
		$id_array = (!empty($id)) ? array($id) : $this->input->post('action_to');

		// Delete multiple
		if (!empty($id_array))
		{
			$deleted = 0;
			$to_delete = 0;
			$deleted_ids = array();
			foreach ($id_array as $id)
			{
				if ($this->blog_categories_m->delete($id))
				{
					$deleted++;
					$deleted_ids[] = $id;
				}
				else
				{
					$this->session->set_flashdata('error', sprintf(lang('cat:mass_delete_error'), $id));
				}
				$to_delete++;
			}

			if ($deleted > 0)
			{
				$this->session->set_flashdata('success', sprintf(lang('cat:mass_delete_success'), $deleted, $to_delete));
			}

			// Fire an event. One or more categories have been deleted.
			Events::trigger('blog_category_deleted', $deleted_ids);
		}
		else
		{
			$this->session->set_flashdata('error', lang('cat:no_select_error'));
		}

		redirect('admin/blog/categories/index');
	}

	/**
	 * Callback method that checks the title of the category
	 *
	 * @param string $title The title to check
	 *
	 * @return bool
	 */
	public function _check_title($title = '')
	{
		if ($this->blog_categories_m->check_title($title, $this->input->post('id')))
		{
			$this->form_validation->set_message('_check_title', sprintf(lang('cat:already_exist_error'), $title));

			return false;
		}

		return true;
	}

	/**
	 * Callback method that checks the slug of the category
	 *
	 * @param string $slug The slug to check
	 *
	 * @return bool
	 */
	public function _check_slug($slug = '')
	{
		if ($this->blog_categories_m->check_slug($slug, $this->input->post('id')))
		{
			$this->form_validation->set_message('_check_slug', sprintf(lang('cat:already_exist_error'), $slug));

			return false;
		}

		return true;
	}

	/**
	 * Create method, creates a new category via ajax
	 */
	public function create_ajax()
	{
		$category = new stdClass();

		// Loop through each validation rule
		foreach ($this->validation_rules as $rule)
		{
			$category->{$rule['field']} = set_value($rule['field']);
		}

		$data = array(
			'mode' => 'create',
			'category' => $category,
		);

		if ($this->form_validation->run())
		{
			$id = $this->blog_categories_m->insert_ajax($this->input->post());

			if ($id > 0)
			{
				$message = sprintf(lang('cat:add_success'), $this->input->post('title', true));
			}
			else
			{
				$message = lang('cat:add_error');
			}

			return $this->template->build_json(array(
				'message' => $message,
				'title' => $this->input->post('title'),
				'category_id' => $id,
				'status' => 'ok'
			));
		}
		else
		{
			// Render the view
			$form = $this->load->view('admin/categories/form', $data, true);

			if ($errors = validation_errors())
			{
				return $this->template->build_json(array(
					'message' => $errors,
					'status' => 'error',
					'form' => $form
				));
			}

			echo $form;
		}
	}

	private function _set_validation_rules() {
		// language
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		array_unshift($public_lang, $this->settings->get('site_lang'));
		$public_lang = array_unique($public_lang);
		foreach ($public_lang as $pl) {
			$available_lang[] = array('code'=>$pl,'name'=>$lang_list[$pl]);
		}

		$this->load->library('Form_validation');
		$blog_validation = array();

		if(count($available_lang) > 1) {
			unset($this->validation_rules[0]);
			unset($this->validation_rules[1]);

			// for additional lang
			foreach ($available_lang as $al) {
				$blog_validation[] = array(
					'field' => 'title_'.$al['code'],
					'label' => lang('global:title').' '.$al['name'],
					'rules' => 'required|trim'
					);
				$blog_validation[] = array(
					'field' => 'slug_'.$al['code'],
					'label' => lang('global:slug').' '.$al['name'],
					'rules' => 'required|trim'
					);
			}
		}
		$rules = array_merge($this->validation_rules, $blog_validation);
		
		// Set the validation rules based on the compiled validation.
		$this->form_validation->set_rules($rules);
	}
}
