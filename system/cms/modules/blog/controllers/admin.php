<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 *
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Blog\Controllers
 */
class Admin extends Admin_Controller
{
	/** @var string The current active section */
	protected $section = 'posts';

	/** @var array The validation rules */
	protected $validation_rules = array(
		'title' => array(
			'field' => 'title',
			'label' => 'lang:global:title',
			'rules' => 'trim|htmlspecialchars|required|max_length[200]|callback__check_title'
		),
		'slug' => array(
			'field' => 'slug',
			'label' => 'lang:global:slug',
			'rules' => 'trim|required|alpha_dot_dash|max_length[200]|callback__check_slug'
		),
		array(
			'field' => 'category_id',
			'label' => 'lang:blog:category_label',
			'rules' => 'trim|numeric'
		),
		array(
			'field' => 'keywords',
			'label' => 'lang:global:keywords',
			'rules' => 'trim'
		),
		array(
			'field' => 'body',
			'label' => 'lang:blog:content_label',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'type',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'status',
			'label' => 'lang:blog:status_label',
			'rules' => 'trim|alpha'
		),
		array(
			'field' => 'created_on',
			'label' => 'lang:blog:date_label',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'created_on_time',
			'label' => 'lang:blog:created_time',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'comments_enabled',
			'label' => 'lang:blog:comments_enabled_label',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'preview_hash',
			'label' => '',
			'rules' => 'trim'
		)
	);

	/**
	 * Every time this controller controller is called should:
	 * - load the blog and blog_categories models
	 * - load the keywords and form validation libraries
	 * - set the hours, minutes and categories template variables.
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model(array('blog_m', 'blog_categories_m'));
		$this->lang->load(array('blog', 'categories'));

		$this->load->library(array('keywords/keywords', 'form_validation'));

		$this->load->model('organization/units_m');
		$this->load->library('organization/organization');

		$_categories = array();
		$params = array(
			'except' => array(
				'field' => $this->db->dbprefix('blog_categories').'.id',
				'value' => '(SELECT IFNULL(row_id,\'\') FROM '.$this->db->dbprefix('blog_categories_categories').')'
				)
			);
		if ($category_list = $this->blog_categories_m->order_by('title')->get_categories_by($params))
		{
			$categories = $this->blog_categories_m->generate_categories_tree(array(), $category_list);
			foreach ($categories as $category)
			{
				$_categories[$category->id] = $category->title;
			}
		}

		// Date ranges for select boxes
		$this->template->set('categories', $_categories);

		$this->config->load('blog');
	}

	/**
	 * Show all created blog posts
	 */
	public function index()
	{
		// Checl permission
		if(!group_has_role('blog','view_all_post') AND !group_has_role('blog','view_own_post') AND !group_has_role('blog','view_own_unit_post')) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		//set the base/default where clause
		$base_where = array('show_future' => true, 'status' => 'all');

		//add get values to base_where if f_module is posted
		if ($this->input->get('f_organization'))
		{
			$base_where['id_unit'] = $this->input->get('f_organization');
		}

		if ($this->input->get('f_category'))
		{
			$base_where['category'] = $this->input->get('f_category');
		}

		if ($this->input->get('f_status'))
		{
			$base_where['status'] = $this->input->get('f_status');
		}

		if ($this->input->get('f_keywords'))
		{
			$base_where['keywords'] = $this->input->get('f_keywords');
		}

		if(!group_has_role('blog','view_all_post')) {
			if (group_has_role('blog','view_own_unit_post')) {
				$membership = $this->organization->get_membership_by_user($this->current_user->id);
				$units = array();
				foreach ($membership['entries'] as $membership_unit) {
					$units[] = $membership_unit['membership_unit']['id'];
				}

				$units = (count($units)>0) ? $units : array('-');
				$base_where['id_unit'] = $units;
			}elseif(group_has_role('blog','view_own_post')) {
				$base_where['created_by'] = $this->current_user->id;
			}
		}

		// Create pagination links
		$total_rows = $this->blog_m->count_by($base_where);
		$pagination = create_pagination('admin/blog/index'.$_SERVER['QUERY_STRING'], $total_rows);

		// Get only blog with site language translation
		$base_where['custom'] = "(default_blog_translation.lang_code is null or default_blog_translation.lang_code='".$this->settings->get('site_lang')."')";

		// Using this data, get the relevant results
		$blog = $this->blog_m
			->limit($pagination['limit'], $pagination['offset'])
			->get_many_by($base_where);

		// Get organization according to permission
		$root_units = $this->units_m->get_units_by_level(0);
		$organization = $this->organization->build_serial_tree(array(), $root_units);

		//do we need to unset the layout because the request is ajax?
		$this->input->is_ajax_request() and $this->template->set_layout(false);

		$this->template
			->title($this->module_details['name'])
			->set_partial('filters', 'admin/partials/filters')
			->set('pagination', $pagination)
			->set('blog', $blog)
			->set('organization', $organization);

		$this->input->is_ajax_request()
			? $this->template->build('admin/tables/posts')
			: $this->template->build('admin/index');

	}

	/**
	 * Create new post
	 */
	public function create()
	{
		// Check permission
		if(!group_has_role('blog','create_post') AND !group_has_role('blog','create_own_unit_post')) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin/blog/index');
		}

		$post = new stdClass();

		// Get the blog stream.
		// $this->load->driver('Streams');
		// $stream = $this->streams->streams->get_stream('blog', 'blogs');
		// $stream_fields = $this->streams_m->get_stream_fields($stream->id, $stream->stream_namespace);

		// Get the validation for our custom blog fields.
		// $blog_validation = $this->streams->streams->validation_array($stream->stream_slug, $stream->stream_namespace, 'new');

		// // Combine our validation rules.
		// $rules = array_merge($this->validation_rules, $blog_validation);

		// Set our validation rules
		// $this->form_validation->set_rules($rules);
		$this->_set_validation_rules();

		// check file image
		$this->form_validation->set_rules('file_image','Image','callback_check_image');

		if(!$this->config->item('allow_empty_organization')) {
			$this->form_validation->set_rules('id_unit',lang('blog:organization_label'),'required');
		}

		if ($this->form_validation->run())
		{
			// Insert a new blog entry.
			$input = $this->input->post();


			if(!group_has_role('blog','create_post') AND !group_has_role('blog','create_own_unit_post')) {
				unset($extra['id_unit']);
			}

			if ($id = $this->blog_m->insert_entry($input))
			{
				$this->pyrocache->delete_all('blog_m');
				$title = ($this->input->post('title')) ? $this->input->post('title') : $this->input->post('title_'.$this->settings->get('site_lang'));
				$this->session->set_flashdata('success', sprintf($this->lang->line('blog:post_add_success'), $title));

				// Blog article has been updated, may not be anything to do with publishing though
				Events::trigger('post_created', $id);

				// They are trying to put this live
				if ($this->input->post('status') == 'live')
				{
					// Fire an event, we're posting a new blog!
					Events::trigger('post_published', $id);
				}
			}
			else
			{
				$this->session->set_flashdata('error', lang('blog:post_add_error'));
			}

			// Redirect back to the form or main page
			if ($this->input->post('btnAction') == 'save_exit') {
				redirect('admin/blog/index');
			} else {
				// check all or own permission
				if(group_has_role('blog','edit_all_post') OR group_has_role('blog','edit_own_unit_post') OR group_has_role('blog','edit_own_post')) {
					redirect('admin/blog/edit/'.$id);
				} else {
					redirect('admin/blog/index');
				}
			}
		}
		else
		{
			// Go through all the known fields and get the post values
			$post = new stdClass;
			foreach ($this->validation_rules as $key => $field)
			{
				$post->$field['field'] = set_value($field['field']);
			}

			if ($this->input->post('created_on'))
			{
				$created_on = strtotime(sprintf('%s %s', $this->input->post('created_on'), $this->input->post('created_on_time')));
			}
			else
			{
				$created_on = now();
			}
			$post->created_on = $created_on;

			// if it's a fresh new article lets show them the advanced editor
			$post->type or $post->type = 'wysiwyg-advanced';
		}

		// Set Values
		// $values = $this->fields->set_values($stream_fields, null, 'new');

		// Run stream field events
		// $this->fields->run_field_events($stream_fields, array(), $values);

		// Get organization according to permission
		$root_units = $this->units_m->get_units_by_level(0);
		$organization = $this->organization->build_serial_tree(array(), $root_units);

		if(!group_has_role('blog','create_post')) {
			if(group_has_role('blog','create_own_unit_post')) {
				foreach ($organization as $key => $value) {
					if(!$this->organization->is_member($this->current_user->id,$value['id'])) {
						unset($organization[$key]);
					}
				}
			}
		}

		$this->template
			->title($this->module_details['name'], lang('blog:create_title'))
			->append_js('jquery/jquery.slugify.js')
			->append_js('date-time/bootstrap-datepicker.js')
			->append_js('date-time/bootstrap-timepicker.js')
			->append_js('bootstrap-tag.min.js')
			->append_css('datepicker.css')
			->append_css('bootstrap-timepicker.css')
			->set('post', $post)
			->set('organization', $organization)
			->build('admin/form');
	}

	/**
	 * Edit blog post
	 *
	 * @param int $id The ID of the blog post to edit
	 */
	public function edit($id = 0)
	{
		$id or redirect('admin/blog/index');

		// check all or own permission
		if(!group_has_role('blog','edit_all_post') AND !group_has_role('blog','edit_own_unit_post') AND !group_has_role('blog','edit_own_post')) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin/blog/index');
		}

		$post = $this->blog_m->get($id);

		// return to index if post not found
		$post or redirect('admin/blog/index');

		// check the owner of post
		if(!group_has_role('blog','edit_all_post')) {
			if (group_has_role('blog','edit_own_unit_post')) {
				if(!$this->organization->is_member($this->current_user->id,$post->id_unit)) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin/blog/index');
				}
			}elseif(group_has_role('blog','edit_own_post')) {
				if($post->created_by != $this->current_user->id) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin/blog/index');
				}
			}
		}

		// They are trying to put this live
		if ($post->status != 'live' and $this->input->post('status') == 'live')
		{
			role_or_die('blog', 'publish_all_post');
		}

		// Load up streams
		// $this->load->driver('Streams');
		// $stream = $this->streams->streams->get_stream('blog', 'blogs');
		// $stream_fields = $this->streams_m->get_stream_fields($stream->id, $stream->stream_namespace);

		// // Get the validation for our custom blog fields.
		// $blog_validation = $this->streams->streams->validation_array($stream->stream_slug, $stream->stream_namespace, 'new');

		// $blog_validation = array_merge($this->validation_rules, array(
		// 	'title' => array(
		// 		'field' => 'title',
		// 		'label' => 'lang:global:title',
		// 		'rules' => 'trim|htmlspecialchars|required|max_length[100]|callback__check_title['.$id.']'
		// 	),
		// 	'slug' => array(
		// 		'field' => 'slug',
		// 		'label' => 'lang:global:slug',
		// 		'rules' => 'trim|required|alpha_dot_dash|max_length[100]|callback__check_slug['.$id.']'
		// 	),
		// ));

		// // Merge and set our validation rules
		// $this->form_validation->set_rules(array_merge($this->validation_rules, $blog_validation));

		$this->_set_validation_rules($id);

		// check file image
		$this->form_validation->set_rules('file_image','Image','callback_check_image');

		$hash = $this->input->post('preview_hash');

		if ($this->input->post('status') == 'draft' and $this->input->post('preview_hash') == '')
		{
			$hash = $this->_preview_hash();
		}
		//it is going to be published we don't need the hash
		elseif ($this->input->post('status') == 'live')
		{
			$hash = '';
		}

		if(!$this->config->item('allow_empty_organization')) {
			$this->form_validation->set_rules('id_unit',lang('blog:organization_label'),'required');
		}

		if ($this->form_validation->run())
		{
			$input = $this->input->post();

			if(!group_has_role('blog','create_post') AND !group_has_role('blog','create_own_unit_post')) {
				unset($input['id_unit']);
			}

			if ($this->blog_m->update_entry($id, $input, $post))
			{
				$title = ($this->input->post('title')) ? $this->input->post('title') : $this->input->post('title_'.$this->settings->get('site_lang'));
				$this->session->set_flashdata(array('success' => sprintf(lang('blog:edit_success'), $title)));

				// Blog article has been updated, may not be anything to do with publishing though
				Events::trigger('post_updated', $id);

				// They are trying to put this live
				if ($post->status != 'live' and $this->input->post('status') == 'live')
				{
					// Fire an event, we're posting a new blog!
					Events::trigger('post_published', $id);
				}
			}
			else
			{
				$this->session->set_flashdata('error', lang('blog:edit_error'));
			}

			// Redirect back to the form or main page
			($this->input->post('btnAction') == 'save_exit') ? redirect('admin/blog/index') : redirect('admin/blog/edit/'.$id);
		}

		// Go through all the known fields and get the post values
		foreach ($this->validation_rules as $key => $field)
		{
			if (isset($_POST[$field['field']]))
			{
				$post->$field['field'] = set_value($field['field']);
			}
		}

		if ($this->input->post('created_on'))
		{
			$created_on = strtotime(sprintf('%s %s', $this->input->post('created_on'), $this->input->post('created_on_time')));
		}
		else
		{
			$created_on = now();
		}

		$post->created_on = $created_on;

		// get the translation of that post
		$translation = $this->blog_m->get_post_translation($id);
		$tmp_translation = array();
		foreach ($translation as $trans) {
			$tmp_translation[$trans->lang_code] = $trans;
		}
		$post->translation = $tmp_translation;

		// // Set Values
		// $values = $this->fields->set_values($stream_fields, $post, 'edit');

		// // Run stream field events
		// $this->fields->run_field_events($stream_fields, array(), $values);

		// Get organization according to permission
		$root_units = $this->units_m->get_units_by_level(0);
		$organization = $this->organization->build_serial_tree(array(), $root_units);

		if(!group_has_role('blog','create_post')) {
			if(group_has_role('blog','create_own_unit_post')) {
				foreach ($organization as $key => $value) {
					if(!$this->organization->is_member($this->current_user->id,$value['id'])) {
						unset($organization[$key]);
					}
				}
			} else {
				$unit = (Array)$this->units_m->get_units_by_id($post->id_unit);
				$organization = array($unit);
			}
		}

		$this->template
			->title($this->module_details['name'], sprintf(lang('blog:edit_title'), $post->title))
			->append_js('jquery/jquery.slugify.js')
			->append_js('date-time/bootstrap-datepicker.js')
			->append_js('date-time/bootstrap-timepicker.js')
			->append_js('bootstrap-tag.min.js')
			->append_css('datepicker.css')
			->append_css('bootstrap-timepicker.css')
			// ->set('stream_fields', $this->streams->fields->get_stream_fields($stream->stream_slug, $stream->stream_namespace, $values, $post->id))
			->set('post', $post)
			->set('organization', $organization)
			->build('admin/form');
	}

	/**
	 * Preview blog post
	 *
	 * @param int $id The ID of the blog post to preview
	 */
	public function preview($id = 0)
	{
		$post = $this->blog_m->get($id);

		$this->template
			->set_layout('modal', 'admin')
			->set('post', $post)
			->build('admin/preview');
	}

	/**
	 * Helper method to determine what to do with selected items from form post
	 */
	public function action()
	{
		switch ($this->input->post('btnAction'))
		{
			case 'publish':
				$this->publish();
				break;

			case 'delete':
				$this->delete();
				break;

			default:
				redirect('admin/blog/index');
				break;
		}
	}

	/**
	 * Publish blog post
	 *
	 * @param int $id the ID of the blog post to make public
	 */
	public function publish($id = 0)
	{
		// check all or own permission
		if(!group_has_role('blog','publish_all_post') AND !group_has_role('blog','publish_own_unit_post') AND !group_has_role('blog','publish_own_post')) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin/blog/index');
		}

		// Publish one
		$ids = ($id) ? array($id) : $this->input->post('action_to');

		if ( ! empty($ids))
		{
			// Go through the array of slugs to publish
			$post_titles = array();
			foreach ($ids as $id)
			{
				// Get the current page so we can grab the id too
				if ($post = $this->blog_m->get($id))
				{
					// check the owner of post
					if(!group_has_role('blog','publish_all_post')) {
						if (group_has_role('blog','publish_own_unit_post')) {
							if(!$this->organization->is_member($this->current_user->id,$post->id_unit)) {
								$this->session->set_flashdata('error', lang('cp:access_denied'));
								redirect('admin/blog/index');
							}
						}elseif(group_has_role('blog','publish_own_post')) {
							if($post->created_by != $this->current_user->id) {
								goto skip;
							}
						}
					}

					$this->blog_m->publish($id);

					// Wipe cache for this model, the content has changed
					$this->pyrocache->delete('blog_m');
					$post_titles[] = $post->title;
				}
				skip :
			}
		}

		// Some posts have been published
		if ( ! empty($post_titles))
		{
			// Only publishing one post
			if (count($post_titles) == 1)
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('blog:publish_success'), $post_titles[0]));
			}
			// Publishing multiple posts
			else
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('blog:mass_publish_success'), implode('", "', $post_titles)));
			}
		}
		// For some reason, none of them were published
		else
		{
			$this->session->set_flashdata('notice', $this->lang->line('blog:publish_error'));
		}

		redirect('admin/blog/index');
	}

	/**
	 * Delete blog post
	 *
	 * @param int $id The ID of the blog post to delete
	 */
	public function delete($id = 0)
	{
		// check all or own permission
		if(!group_has_role('blog','delete_all_post') AND !group_has_role('blog','delete_own_unit_post') AND !group_has_role('blog','delete_own_post')) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin/blog/index');
		}

		$this->load->model('comments/comment_m');

		// Delete one
		$ids = ($id) ? array($id) : $this->input->post('action_to');

		// Go through the array of slugs to delete
		if ( ! empty($ids))
		{
			$post_titles = array();
			$deleted_ids = array();
			foreach ($ids as $id)
			{
				// Get the current page so we can grab the id too
				if ($post = $this->blog_m->get($id))
				{
					// check the owner of post
					if(!group_has_role('blog','delete_all_post')) {
						if (group_has_role('blog','delete_own_unit_post')) {
							if(!$this->organization->is_member($this->current_user->id,$post->id_unit)) {
								$this->session->set_flashdata('error', lang('cp:access_denied'));
								redirect('admin/blog/index');
							}
						}elseif(group_has_role('blog','delete_own_post')) {
							if($post->created_by != $this->current_user->id) {
								goto skip;
							}
						}
					}

					if ($this->blog_m->delete($id))
					{
						$this->comment_m->where('module', 'blog')->delete_by('entry_id', $id);

						// Wipe cache for this model, the content has changed
						$this->pyrocache->delete('blog_m');
						$post_titles[] = $post->title;
						$deleted_ids[] = $id;
					}
				}
				skip :
			}

			// Fire an event. We've deleted one or more blog posts.
			Events::trigger('post_deleted', $deleted_ids);
		}

		// Some pages have been deleted
		if ( ! empty($post_titles))
		{
			// Only deleting one page
			if (count($post_titles) == 1)
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('blog:delete_success'), $post_titles[0]));
			}
			// Deleting multiple pages
			else
			{
				$this->session->set_flashdata('success', sprintf($this->lang->line('blog:mass_delete_success'), implode('", "', $post_titles)));
			}
		}
		// For some reason, none of them were deleted
		else
		{
			$this->session->set_flashdata('notice', lang('blog:delete_error'));
		}

		redirect('admin/blog/index');
	}

	/**
	 * Generate a preview hash
	 *
	 * @return bool
	 */
	private function _preview_hash()
	{
		return md5(microtime() + mt_rand(0, 1000));
	}

	/**
	 * Callback method that checks the title of an post
	 *
	 * @param string $title The Title to check
	 * @param string $id
	 *
	 * @return bool
	 */
	public function _check_title($title, $id = null)
	{
		$this->form_validation->set_message('_check_title', sprintf(lang('blog:already_exist_error'), lang('global:title')));

		return $this->blog_m->check_exists('title', $title, $id);
	}

	/**
	 * Callback method that checks the slug of an post
	 *
	 * @param string $slug The Slug to check
	 * @param null   $id
	 *
	 * @return bool
	 */
	public function _check_slug($slug, $id = null)
	{
		$this->form_validation->set_message('_check_slug', sprintf(lang('blog:already_exist_error'), lang('global:slug')));

		return $this->blog_m->check_exists('slug', $slug, $id);
	}

	private function _set_validation_rules($id = 0) {
		// language
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		array_unshift($public_lang, $this->settings->get('site_lang'));
		$public_lang = array_unique($public_lang);
		foreach ($public_lang as $pl) {
			$available_lang[] = array('code'=>$pl,'name'=>$lang_list[$pl]);
		}

		$this->load->library('Form_validation');
		$blog_validation = array();

		if(count($available_lang) > 1) {
			unset($this->validation_rules['title']);
			unset($this->validation_rules['slug']);
			unset($this->validation_rules[2]);

			// for additional lang
			foreach ($available_lang as $al) {
				$blog_validation[] = array(
					'field' => 'title_'.$al['code'],
					'label' => lang('global:title').' '.$al['name'],
					'rules' => 'required'
					);
				$blog_validation[] = array(
					'field' => 'slug_'.$al['code'],
					'label' => lang('global:slug').' '.$al['name'],
					'rules' => 'required'
					);
				$blog_validation[] = array(
					'field' => 'body_'.$al['code'],
					'label' => lang('blog:content_label').' '.$al['name'],
					'rules' => 'required'
					);
				$blog_validation[] = array(
					'field' => 'intro_'.$al['code'],
					'label' => 'Snippet '.$al['name'],
					'rules' => ''
					);
			}
		}

		if($id != 0) {
			$this->validation_rules['title']['rules'] = 'trim|htmlspecialchars|required|max_length[100]|callback__check_title['.$id.']';
			$this->validation_rules['slug']['rules'] = 'trim|required|alpha_dot_dash|max_length[100]|callback__check_slug['.$id.']';
		}
		
		$rules = array_merge($this->validation_rules, $blog_validation);
		
		// Set the validation rules based on the compiled validation.
		$this->form_validation->set_rules($rules);
	}

	public function check_image($image) {
		$this->load->library('files/files');
		$id_folder = Files::get_id_by_name('Public');
		$image_id = null;
		if($_FILES['file_image']['name']!=''){
			$allowed_type = array('bmp', 'gif', 'jpeg', 'jpg', 'jpe', 'png', 'tiff', 'tif', 'BMP', 'GIF', 'JPEG', 'JPG', 'PNG');
			$data = Files::upload($id_folder, $_FILES['file_image']['name'], 'file_image',false,false,false,implode("|", $allowed_type));

			if($data['status']===false) {
				$this->form_validation->set_message('check_image','<strong>Image</strong> '.$data['message']);
				return false;
			} else {
				$image_id = $data['data']['id'];
				$_POST['image'] = $image_id;
				return true;
			}
		} else {
			// if($this->input->post('image')=='dummy') {
			// 	$this->form_validation->set_message('check_image','<strong>Image</strong> wajib diisi.');
			// 	return false;
			// }
		}
	}
}
