<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Blog module
 *
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Blog
 */
class Module_Blog extends Module
{
	public $version = '2.2.3';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Blog',
				'ar' => 'المدوّنة',
				'br' => 'Blog',
				'pt' => 'Blog',
				'el' => 'Ιστολόγιο',
                            'fa' => 'بلاگ',
				'he' => 'בלוג',
				'id' => 'Blog',
				'lt' => 'Blogas',
				'pl' => 'Blog',
				'ru' => 'Блог',
				'tw' => '文章',
				'cn' => '文章',
				'hu' => 'Blog',
				'fi' => 'Blogi',
				'th' => 'บล็อก',
				'se' => 'Blogg',
			),
			'description' => array(
				'en' => 'Post blog entries.',
				'ar' => 'أنشر المقالات على مدوّنتك.',
				'br' => 'Escrever publicações de blog',
				'pt' => 'Escrever e editar publicações no blog',
				'cs' => 'Publikujte nové články a příspěvky na blog.', #update translation
				'da' => 'Skriv blogindlæg',
				'de' => 'Veröffentliche neue Artikel und Blog-Einträge', #update translation
				'sl' => 'Objavite blog prispevke',
				'fi' => 'Kirjoita blogi artikkeleita.',
				'el' => 'Δημιουργήστε άρθρα και εγγραφές στο ιστολόγιο σας.',
				'es' => 'Escribe entradas para los artículos y blog (web log).', #update translation
                                'fa' => 'مقالات منتشر شده در بلاگ',
				'fr' => 'Poster des articles d\'actualités.',
				'he' => 'ניהול בלוג',
				'id' => 'Post entri blog',
				'it' => 'Pubblica notizie e post per il blog.', #update translation
				'lt' => 'Rašykite naujienas bei blog\'o įrašus.',
				'nl' => 'Post nieuwsartikelen en blogs op uw site.',
				'pl' => 'Dodawaj nowe wpisy na blogu',
				'ru' => 'Управление записями блога.',
				'tw' => '發表新聞訊息、部落格等文章。',
				'cn' => '发表新闻讯息、部落格等文章。',
				'th' => 'โพสต์รายการบล็อก',
				'hu' => 'Blog bejegyzések létrehozása.',
				'se' => 'Inlägg i bloggen.',
			),
			'frontend' => true,
			'backend' => true,
			'skip_xss' => true,
			'menu' => 'content',

			'roles' => array(
				'create_post','create_own_unit_post',
				'view_all_post','view_own_post','view_own_unit_post',
				'edit_all_post','edit_own_post','edit_own_unit_post',
				'delete_all_post','delete_own_post','delete_own_unit_post',
				'manage_categories','publish_all_post','publish_own_post','publish_own_unit_post',
			),

			'sections' => array(
				'posts' => array(
					'name' => 'blog:posts_title',
					'uri' => array('urls'=>array('admin/blog/index','admin/blog/create','admin/blog/view%1','admin/blog/edit%1','admin/blog/view%2','admin/blog/edit%2')),
					'shortcuts' => array(
						// array(
						// 	'name' => 'blog:create_title',
						// 	'uri' => 'admin/blog/create',
						// 	'class' => 'add',
						// ),
					),
				),
			),
		);

		if(group_has_role('blog','manage_categories')) {
			$info['sections']['categories'] = array(
					'name' => 'cat:list_title',
					'uri' => array('urls'=>array('admin/blog/categories/index','admin/blog/categories/create','admin/blog/categories/view%1','admin/blog/categories/edit%1','admin/blog/categories/view%2','admin/blog/categories/edit%2')),
					'shortcuts' => array(
						// array(
						// 	'name' => 'cat:create_title',
						// 	'uri' => 'admin/blog/categories/create',
						// 	'class' => 'add',
						// ),
					),
				);
		}

		// if (function_exists('group_has_role'))
		// {
		// 	if(group_has_role('blog', 'admin_blog_fields'))
		// 	{
		// 		$info['sections']['fields'] = array(
		// 					'name' 	=> 'global:custom_fields',
		// 					'uri' 	=> 'admin/blog/fields',
		// 						'shortcuts' => array(
		// 							'create' => array(
		// 								'name' 	=> 'streams:add_field',
		// 								'uri' 	=> 'admin/blog/fields/create',
		// 								'class' => 'add'
		// 								)
		// 							)
		// 					);
		// 	}
		// }

		return $info;
	}

	public function admin_menu(&$menu_items)
	{
		
	}

	public function install()
	{
		$this->dbforge->drop_table('blog_categories');

		$this->load->driver('Streams');
		$this->streams->utilities->remove_namespace('blogs');

		// Just in case.
		$this->dbforge->drop_table('blog');

		if ($this->db->table_exists('data_streams'))
		{
			$this->db->where('stream_namespace', 'blogs')->delete('data_streams');
		}

		// Create the blog categories table.
		$this->install_tables(array(
			'blog_categories' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false, 'unique' => true, 'key' => true),
				'title' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false, 'unique' => true),
			),
		));

		// Create the blog sub categories table.
		$fields = array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE),
				'row_id' => array('type' => 'INT', 'constraint' => 11),
				'category_id' => array('type' => 'INT', 'constraint' => 11),
			);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('blog_categories_categories', TRUE);

		$this->streams->streams->add_stream(
			'lang:blog:blog_title',
			'blog',
			'blogs',
			null,
			null
		);

		// Add the intro field.
		// This can be later removed by an admin.
		$intro_field = array(
			'name'		=> 'lang:blog:intro_label',
			'slug'		=> 'intro',
			'namespace' => 'blogs',
			'type'		=> 'wysiwyg',
			'assign'	=> 'blog',
			'extra'		=> array('editor_type' => 'simple', 'allow_tags' => 'y'),
			'required'	=> true
		);
		$this->streams->fields->add_field($intro_field);

		// Ad the rest of the blog fields the normal way.
		$blog_fields = array(
				'title' => array('type' => 'VARCHAR', 'constraint' => 200, 'null' => false, 'unique' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 200, 'null' => false),
				'category_id' => array('type' => 'INT', 'constraint' => 11, 'key' => true),
				'body' => array('type' => 'TEXT'),
				'parsed' => array('type' => 'TEXT'),
				'keywords' => array('type' => 'VARCHAR', 'constraint' => 32, 'default' => ''),
				'author_id' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'created_on' => array('type' => 'INT', 'constraint' => 11),
				'updated_on' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'comments_enabled' => array('type' => 'ENUM', 'constraint' => array('no','1 day','1 week','2 weeks','1 month', '3 months', 'always'), 'default' => '3 months'),
				'status' => array('type' => 'ENUM', 'constraint' => array('draft', 'live'), 'default' => 'draft'),
				'type' => array('type' => 'SET', 'constraint' => array('html', 'markdown', 'wysiwyg-advanced', 'wysiwyg-simple')),
				'preview_hash' => array('type' => 'CHAR', 'constraint' => 32, 'default' => ''),
		);
		return $this->dbforge->add_column('blog', $blog_fields);
	}

	public function uninstall()
	{
		// This is a core module, lets keep it around.
		return false;
	}

	public function upgrade($old_version)
	{
		switch ($old_version) {
			case '2.0.0':
				// Create the blog sub categories table.
				$fields = array(
						'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE),
						'row_id' => array('type' => 'INT', 'constraint' => 11),
						'category_id' => array('type' => 'INT', 'constraint' => 11),
					);

				$this->dbforge->add_field($fields);
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->create_table('blog_categories_categories', TRUE);

				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '2.0.1';
				break;

			case '2.0.1':
				$new_fields = array(
					'id_unit' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE),
					);
				$this->dbforge->add_column('blog',$new_fields);

				// add table translation for pages
				if(!$this->db->table_exists('blog_translation')) {
					$fields = array(
						'id_blog' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
						),
						'lang_code' => array(
							'type' => 'VARCHAR',
							'constraint' => 10,
							'null' => FALSE,
						),
						'lang_name' => array(
							'type' => 'VARCHAR',
							'constraint' => 100,
							'null' => FALSE,
						),
						'translation' => array(
							'type' => 'TEXT',
							'null' => TRUE,
						),
					);
					$this->dbforge->add_field($fields);
					$this->dbforge->create_table('blog_translation', TRUE);
					$this->db->query("CREATE INDEX `blog_trans_idx` ON ".$this->db->dbprefix('blog_translation')."(`id_blog`)");
				}

				// add table translation for pages field
				if(!$this->db->table_exists('blog_categories_translation')) {
					$fields = array(
						'id_category' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
						),
						'lang_code' => array(
							'type' => 'VARCHAR',
							'constraint' => 10,
							'null' => FALSE,
						),
						'lang_name' => array(
							'type' => 'VARCHAR',
							'constraint' => 100,
							'null' => FALSE,
						),
						'translation' => array(
							'type' => 'TEXT',
							'null' => TRUE,
						),
					);
					$this->dbforge->add_field($fields);
					$this->dbforge->create_table('blog_categories_translation', TRUE);
					$this->db->query(
						"CREATE INDEX `blog_categories_trans_idx` ON ". $this->db->dbprefix('blog_categories_translation')."(`id_category`)"
					);
				}

				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '2.0.2';
				break;

			case '2.0.2':
				// add table translation for pages
				if(!$this->db->table_exists('blog_translation')) {
					$fields = array(
						'id_blog' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
						),
						'lang_code' => array(
							'type' => 'VARCHAR',
							'constraint' => 10,
							'null' => FALSE,
						),
						'lang_name' => array(
							'type' => 'VARCHAR',
							'constraint' => 100,
							'null' => FALSE,
						),
						'translation' => array(
							'type' => 'TEXT',
							'null' => TRUE,
						),
					);
					$this->dbforge->add_field($fields);
					$this->dbforge->create_table('blog_translation', TRUE);
					$this->db->query("CREATE INDEX `blog_trans_idx` ON ".$this->db->dbprefix('blog_translation')."(`id_blog`)");
				}

				// add table translation for pages field
				if(!$this->db->table_exists('blog_categories_translation')) {
					$fields = array(
						'id_category' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
						),
						'lang_code' => array(
							'type' => 'VARCHAR',
							'constraint' => 10,
							'null' => FALSE,
						),
						'lang_name' => array(
							'type' => 'VARCHAR',
							'constraint' => 100,
							'null' => FALSE,
						),
						'translation' => array(
							'type' => 'TEXT',
							'null' => TRUE,
						),
					);
					$this->dbforge->add_field($fields);
					$this->dbforge->create_table('blog_categories_translation', TRUE);
					$this->db->query("CREATE INDEX `blog_categories_trans_idx` ON ".$this->db->dbprefix('blog_categories_translation')."(`id_category`)");
				}

				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '2.1.2';
				break;

            case '2.1.2':
                // convert all tables to InnoDB engine
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('blog')." ENGINE=InnoDB");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('blog_categories')." ENGINE=InnoDB");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('blog_categories_categories')." ENGINE=InnoDB");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('blog_categories_translation')." ENGINE=InnoDB");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('blog_translation')." ENGINE=InnoDB");

				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '2.2.0';
				break;

			case '2.2.0':
				// ------------------------------------------
				// CREATE FK FOR `blog`.`created_by`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				$existing_idx = $this->db->query("SHOW INDEX FROM ".$this->db->dbprefix('blog')." WHERE KEY_NAME = 'blog_created_by_idx'")->result();

				if(count($existing_idx) == 0){
					$this->db->query("CREATE INDEX `blog_created_by_idx` ON ".$this->db->dbprefix('blog')."(`created_by`)");
				}

				// Step 2 - delete all child rows that have no match FK reference in parent row
				$this->db->select('id');
				$existing_user_ids = $this->db->get('users')->result();

				$existing_user_ids_arr = array();
				foreach ($existing_user_ids as $existing_user_id) {
					$existing_user_ids_arr[] = (int)$existing_user_id->id;
				}

				if(count($existing_user_ids_arr) > 0){
					$this->db->where_not_in('created_by', $existing_user_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('blog');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog')." MODIFY `created_by` INT(11) UNSIGNED NULL DEFAULT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog')." ADD FOREIGN KEY(`created_by`) REFERENCES ".$this->db->dbprefix('users')."(`id`)");

				// ------------------------------------------
				// CREATE FK FOR `blog`.`category_id`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				$existing_idx = $this->db->query("SHOW INDEX FROM ".$this->db->dbprefix('blog')." WHERE KEY_NAME = 'blog_category_idx'")->result();

				if(count($existing_idx) == 0){
					$this->db->query("CREATE INDEX `blog_category_idx` ON ".$this->db->dbprefix('blog')."(`category_id`)");
				}

				// Step 2 - delete all child rows that have no match FK reference in parent row
				$this->db->select('id');
				$existing_category_ids = $this->db->get('blog_categories')->result();

				$existing_category_ids_arr = array();
				foreach ($existing_category_ids as $existing_category_id) {
					$existing_category_ids_arr[] = (int)$existing_category_id->id;
				}

				if(count($existing_category_ids_arr) > 0){
					$this->db->where_not_in('category_id', $existing_category_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('blog');

				// Step 3 - equalize child's column data type to parent's
				// no need

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog')." ADD FOREIGN KEY(`category_id`) REFERENCES ".$this->db->dbprefix('blog_categories')."(`id`)");

				// ------------------------------------------
				// CREATE FK FOR `blog`.`author_id`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				$existing_idx = $this->db->query("SHOW INDEX FROM ".$this->db->dbprefix('blog')." WHERE KEY_NAME = 'blog_author_idx'")->result();

				if(count($existing_idx) == 0){
					$this->db->query("CREATE INDEX `blog_author_idx` ON ".$this->db->dbprefix('blog')."(`author_id`)");
				}

				// Step 2 - delete all child rows that have no match FK reference in parent row
				if(count($existing_user_ids_arr) > 0){
					$this->db->where_not_in('author_id', $existing_user_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('blog');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog')." MODIFY `author_id` INT(11) UNSIGNED NULL DEFAULT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog')." ADD FOREIGN KEY(`author_id`) REFERENCES ".$this->db->dbprefix('users')."(`id`)");

				// ------------------------------------------
				// CREATE FK FOR `blog`.`id_unit`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				$existing_idx = $this->db->query("SHOW INDEX FROM ".$this->db->dbprefix('blog')." WHERE KEY_NAME = 'blog_unit_idx'")->result();

				if(count($existing_idx) == 0){
					$this->db->query("CREATE INDEX `blog_unit_idx` ON ".$this->db->dbprefix('blog')."(`id_unit`)");
				}

				// Step 2 - delete all child rows that have no match FK reference in parent row
				$this->db->select('id');
				$existing_unit_ids = $this->db->get('organization_units')->result();

				$existing_unit_ids_arr = array();
				foreach ($existing_unit_ids as $existing_unit_id) {
					$existing_unit_ids_arr[] = (int)$existing_unit_id->id;
				}

				if(count($existing_unit_ids_arr) > 0){
					$this->db->where_not_in('id_unit', $existing_unit_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->where('id_unit IS NOT NULL', NULL, false); // `id_unit` is allowed to have NULL value
				$this->db->delete('blog');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog')." MODIFY `id_unit` INT(11) UNSIGNED NULL DEFAULT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog')." ADD FOREIGN KEY(`id_unit`) REFERENCES ".$this->db->dbprefix('organization_units')."(`id`)");

				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '2.2.1';
				break;

			case '2.2.1':
				// ------------------------------------------
				// CREATE FK FOR `blog_categories_categories`.`row_id`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				$existing_idx = $this->db->query("SHOW INDEX FROM ".$this->db->dbprefix('blog_categories_categories')." WHERE KEY_NAME = 'blog_categories_categories_child_idx'")->result();

				if(count($existing_idx) == 0){
					$this->db->query("CREATE INDEX `blog_categories_categories_child_idx` ON ".$this->db->dbprefix('blog_categories_categories')."(`row_id`)");
				}

				// Step 2 - delete all child rows that have no match FK reference in parent row
				$this->db->select('id');
				$existing_category_ids = $this->db->get('blog_categories')->result();

				$existing_category_ids_arr = array();
				foreach ($existing_category_ids as $existing_category_id) {
					$existing_category_ids_arr[] = (int)$existing_category_id->id;
				}

				if(count($existing_category_ids_arr) > 0){
					$this->db->where_not_in('row_id', $existing_category_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('blog_categories_categories');

				// Step 3 - equalize child's column data type to parent's
				// no need

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog_categories_categories')." ADD FOREIGN KEY(`row_id`) REFERENCES ".$this->db->dbprefix('blog_categories')."(`id`) ON DELETE CASCADE ON UPDATE RESTRICT");

				// ------------------------------------------
				// CREATE FK FOR `blog_categories_categories`.`category_id`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				$existing_idx = $this->db->query("SHOW INDEX FROM ".$this->db->dbprefix('blog_categories_categories')." WHERE KEY_NAME = 'blog_categories_categories_parent_idx'")->result();

				if(count($existing_idx) == 0){
					$this->db->query("CREATE INDEX `blog_categories_categories_parent_idx` ON ".$this->db->dbprefix('blog_categories_categories')."(`category_id`)");
				}

				// Step 2 - delete all child rows that have no match FK reference in parent row
				if(count($existing_category_ids_arr) > 0){
					$this->db->where_not_in('category_id', $existing_category_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('blog_categories_categories');

				// Step 3 - equalize child's column data type to parent's
				// no need

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog_categories_categories')." ADD FOREIGN KEY(`category_id`) REFERENCES ".$this->db->dbprefix('blog_categories')."(`id`)");

				// ------------------------------------------
				// CREATE FK FOR `blog_categories_translation`.`id_category`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row
				if(count($existing_category_ids_arr) > 0){
					$this->db->where_not_in('id_category', $existing_category_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('blog_categories_translation');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog_categories_translation')." MODIFY `id_category` INT(11) NULL DEFAULT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog_categories_translation')." ADD FOREIGN KEY(`id_category`) REFERENCES ".$this->db->dbprefix('blog_categories')."(`id`)");

				// ------------------------------------------
				// CREATE FK FOR `blog_translation`.`id_blog`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row
				$this->db->select('id');
				$existing_blog_ids = $this->db->get('blog')->result();

				$existing_blog_ids_arr = array();
				foreach ($existing_blog_ids as $existing_blog_id) {
					$existing_blog_ids_arr[] = (int)$existing_blog_id->id;
				}

				if(count($existing_blog_ids_arr) > 0){
					$this->db->where_not_in('id_blog', $existing_blog_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('blog_translation');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog_translation')." MODIFY `id_blog` INT(9) NULL DEFAULT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog_translation')." ADD FOREIGN KEY(`id_blog`) REFERENCES ".$this->db->dbprefix('blog')."(`id`)");

				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '2.2.2';
				break;
			case '2.2.2':
				// remove existing foreign key
				$constraint = $this->db->query("SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = '".$this->db->database."' AND REFERENCED_TABLE_NAME = '".$this->db->dbprefix('blog_categories')."' AND TABLE_NAME = '".$this->db->dbprefix('blog')."'");
				
				$blog_fk = $constraint->result_array();

				foreach ($blog_fk as $fk) {
					if(isset($fk['CONSTRAINT_NAME'])) {
					$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog')." DROP FOREIGN KEY `".$fk['CONSTRAINT_NAME']."`");
					}
				}

				// modify column

				$fields = array(
					'category_id' => array(
						'type' => 'INT', 
						'constraint' => 11, 
						'null' => true),
					);
				$this->dbforge->modify_column('blog', $fields);

				// add foreign key
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('blog')." ADD FOREIGN KEY(`category_id`) REFERENCES ".$this->db->dbprefix('blog_categories')."(`id`)");	

				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '2.2.3';
				break;
		}
		return true;
	}
}
