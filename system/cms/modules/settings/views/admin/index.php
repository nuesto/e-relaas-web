<div class="page-header">
	<h1><?php echo $module_details['name'] ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<?php if ($setting_sections): ?>
	
	<?php echo form_open('admin/settings/edit', 'class="form-horizontal"');?>
	
	<div class="tabbable">

		<ul class="nav nav-tabs">
			<?php 
			$count = 0;
			foreach ($setting_sections as $section_slug => $section_name): 
			?>
			<li <?php if($count == 0){ ?>class="active"<?php $count++;} //if ?>>
				<a href="#<?php echo $section_slug ?>" title="<?php printf(lang('settings:section_title'), $section_name) ?>" data-toggle="tab">
					<span><?php echo $section_name ?></span>
				</a>
			</li>
			<?php endforeach ?>
		</ul>
		
		<div class="tab-content">

		<?php 
		$count = 0;
		foreach ($setting_sections as $section_slug => $section_name): 
		?>
		
		<div id="<?php echo $section_slug ?>" class="tab-pane <?php if($count == 0){ ?>in active<?php $count++;} //if ?>">
		
		<?php $section_count = 1; foreach ($settings[$section_slug] as $setting): ?>
		
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right">
				<?php echo $setting->title ?>
			</label>

			<div class="col-sm-9">
				<?php echo $setting->form_control ?>
				<?php if($setting->description): echo '<br /><small>'.$setting->description.'</small>'; endif ?>
			</div>
			<span class="move-handle"></span>
		</div>
		
		<?php endforeach ?>
		
		</div>
		
		<?php endforeach ?>
		
		</div>

	</div>
	
	<div class="space-24"></div>

	<div class="buttons padding-top">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save') )) ?>
	</div>
	
	<?php echo form_close() ?>
	
<?php else: ?>
	<div class="well"><?php echo lang('settings:no_settings');?></div>
<?php endif ?>