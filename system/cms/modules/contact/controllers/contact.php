<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Contact controller (frontend)
 *
 * @package		PyroCMS\Core\Modules\Contact\Controllers
 * @author		PyroCMS Dev Team
 * @copyright   Copyright (c) 2012, PyroCMS LLC
 */
class Contact extends Public_Controller
{
	/**
	 * Constructor method
	 * 
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->template
			->title(lang('contact_title'))
			->set_layout('contact.html')
			->build('form');
	}
}
