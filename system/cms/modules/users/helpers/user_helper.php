<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Phil Sturgeon
 */
// ------------------------------------------------------------------------

/**
 * Checks to see if a user is logged in or not.
 * 
 * @access public
 * @return bool
 */
function is_logged_in()
{
    return (isset(get_instance()->current_user->id)) ? true : false; 
}

// ------------------------------------------------------------------------

/**
 * Checks if a group has access to module or role
 * 
 * @access public
 * @param string $module sameple: pages
 * @param string $role sample: put_live
 * @return bool
 */
function group_has_role($module, $role, $users = NULL)
{
	if($users != NULL){
		ci()->current_user = $users;
	}

	if (empty(ci()->current_user))
	{
		return false;
	}

	if (ci()->current_user->group == 'admin')
	{
		return true;
	}

	$permissions = ci()->permission_m->get_group(ci()->current_user->group_id);
	
	if (empty($permissions[$module]) or empty($permissions[$module][$role]))
	{
		return false;
	}

	return true;
}

// ------------------------------------------------------------------------

/**
 * Checks if a group has access to module or role
 *
 * @access public
 * @param string $module sameple: pages
 * @param string $role sample: put_live
 * @return bool
 */
function group_has_access($module, $action, $stream_slug, $entry_object)
{
	if(group_has_role($module, $action . '_all_' . $stream_slug)){
		return TRUE;
	}elseif(group_has_role($module, $action . '_own_' . $stream_slug)){
		if ($entry_object->created_by_user_id == ci()->current_user->id){
			return TRUE;
		}
	}

	return FALSE;
}

// ------------------------------------------------------------------------

/**
 * Checks if role has access to module or returns error 
 * 
 * @access public
 * @param string $module sample: pages
 * @param string $role sample: edit_live
 * @param string $redirect_to (default: 'admin') Url to redirect to if no access
 * @param string $message (default: '') Message to display if no access
 * @return mixed
 */
function role_or_die($module, $role, $redirect_to = 'admin', $message = '')
{
	ci()->lang->load('admin');

	if (ci()->input->is_ajax_request() and ! group_has_role($module, $role))
	{
		echo json_encode(array('error' => ($message ? $message : lang('cp:access_denied')) ));
		return false;
	}
	elseif ( ! group_has_role($module, $role))
	{
		ci()->session->set_flashdata('error', ($message ? $message : lang('cp:access_denied')) );
		redirect($redirect_to);
	}
	return true;
}

// ------------------------------------------------------------------------

/**
 * Return a users display name based on settings
 *
 * @param int $user the users id
 * @param string $linked if true a link to the profile page is returned, 
 *                       if false it returns just the display name.
 * @return  string
 */
function user_displayname($user, $linked = true, $admin_link = false)
{
    ci()->lang->load('users/user');

    // User is numeric and user hasn't been pulled yet isn't set.
    if (is_numeric($user))
    {
        $user = ci()->ion_auth->get_user($user);
    }

    if(!isset($user)) {
    	return lang('user:not_found');
    }

    $user = (array) $user;
    $name = empty($user['display_name']) ? $user['username'] : $user['display_name'];

    // Static var used for cache
    if ( ! isset($_users))
    {
        static $_users = array();
    }

    // check if it exists
    if (isset($_users[$user['id']]))
    {
        if( ! empty( $_users[$user['id']]['profile_link'] ) and $linked)
        {
        	return $_users[$user['id']]['profile_link'];
        }
        else
        {
            return $name;
        }
    }

    // Set cached variable.
    if (ci()->settings->enable_profiles and $linked)
    {
    	if($admin_link){
			$_users[$user['id']]['profile_link'] = '<a href="'.site_url('admin/users/view/'.$user['id']).'">'.$name.'</a>';
		}else{
			$_users[$user['id']]['profile_link'] = anchor('admin/users/view/'.$user['id'], $name);
		}
        return $_users[$user['id']]['profile_link'];
    }

    // Not cached, Not linked. get_user caches the result so no need to cache non linked
    return $name;
}

/**
 * Return a users email based on settings
 *
 * @param int $user the users id
 * @param string $linked if true a link to the profile page is returned, 
 *                       if false it returns just the email.
 * @return  string
 */
function user_email($user, $linked = true, $admin_link = false)
{
    ci()->lang->load('users/user');

    // User is numeric and user hasn't been pulled yet isn't set.
    if (is_numeric($user))
    {
        $user = ci()->ion_auth->get_user($user);
    }

    if(!isset($user)) {
    	return lang('user:not_found');
    }

    $user = (array) $user;
    $email = empty($user['email']) ? $user['username'] : $user['email'];

    // Static var used for cache
    if ( ! isset($_users))
    {
        static $_users = array();
    }

    // check if it exists
    if (isset($_users[$user['id']]))
    {
        if( ! empty( $_users[$user['id']]['profile_link'] ) and $linked)
        {
        	return $_users[$user['id']]['profile_link'];
        }
        else
        {
            return $email;
        }
    }

    // Set cached variable.
    if (ci()->settings->enable_profiles and $linked)
    {
    	if($admin_link){
			$_users[$user['id']]['profile_link'] = '<a href="'.site_url('admin/users/view/'.$user['id']).'">'.$email.'</a>';
		}else{
			$_users[$user['id']]['profile_link'] = anchor('admin/users/view/'.$user['id'], $email);
		}
        return $_users[$user['id']]['profile_link'];
    }

    // Not cached, Not linked. get_user caches the result so no need to cache non linked
    return $email;
}

function user_groups($user_id)
{
	ci()->load->model('users/user_m');

	$user = ci()->user_m->get(array('id' => $user_id));
	echo $user->group_description;
}

function user_has_group($group_short_name)
{
	ci()->load->model('users/user_m');

	$user = ci()->user_m->get(array('id' => ci()->current_user->id));

	if($user->group_name == $group_short_name){
		return TRUE;
	}else{
		return FALSE;
	}
}

// simple download excel

function download_user($data) {
	// filename for download
	$filename = "data_users_" . date('Ymd') . ".xls";

	header("Content-Disposition: attachment; filename=\"$filename\"");
	header("Content-Type: application/vnd.ms-excel");

	$flag = false;
	foreach($data as $row) {
		if(!$flag) {
      		// display field/column names as first row
			echo implode("\t", array_keys($row)) . "\r\n";
			$flag = true;
		}
		array_walk($row, 'cleanData');
		echo implode("\t", array_values($row)) . "\r\n";
	}
	exit;
}

function cleanData(&$str)
{
		// escape tab characters
	$str = preg_replace("/\t/", "\\t", $str);

		// escape new lines
	$str = preg_replace("/\r?\n/", "\\n", $str);

		// convert 't' and 'f' to boolean values
	if($str == 't') $str = 'TRUE';
	if($str == 'f') $str = 'FALSE';

		// force certain number/date formats to be imported as strings
	if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
		$str = "'$str";
	}

		// escape fields that include double quotes
	if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}

function api_has_role($api)
{
	ci()->load->model('users/user_m');
	$user = ci()->user_m->get_user_by_api($api['user'], $api['key']);
	if(is_array($user)){
		ci()->current_user = $user;
		return true;
	}
	return false;
}


function api_get_user($api){
	$user = ci()->user_m->get_user_by_api($api['user'], $api['key']);
	$user = (object) $user;
	return $user;
}
/* End of file users/helpers/user_helper.php */