<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * The User model.
 *
 * @author PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Users\Models
 */
class User_m extends MY_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->profile_table = $this->db->dbprefix('profiles');
	}

	// --------------------------------------------------------------------------

	/**
	 * Get a specified (single) user
	 *
	 * @param array $params
	 *
	 * @return object
	 */
	public function get($params)
	{
		if (isset($params['id']))
		{
			$this->db->where('users.id', $params['id']);
		}

		if (isset($params['email']))
		{
			$this->db->where('LOWER('.$this->db->dbprefix('users.email').')', strtolower($params['email']));
		}

		if (isset($params['role']))
		{
			$this->db->where('users.group_id', $params['role']);
		}

		if (isset($params['id_facebook']))
		{
			$this->db->or_where('users.id_facebook', $params['id_facebook']);
		}

		if (isset($params['id_google']))
		{
			$this->db->or_where('users.id_google', $params['id_google']);
		}

		if (isset($params['id_twitter']))
		{
			$this->db->or_where('users.id_twitter', $params['id_twitter']);
		}

		$this->db
			->select($this->profile_table.'.*')
			->select('users.*')
			->select('groups.name AS group_name')
			->select('groups.description AS group_description')
			->select('cra_profile.user_id AS cra_user_id')
			->select('cra_profile.display_name AS cra_display_name')
			->limit(1)
			->join('profiles', 'profiles.user_id = users.id', 'left')
			->join('groups', 'users.group_id = groups.id', 'left')
			->join('profiles as cra_profile', 'profiles.id_cra = cra_profile.user_id', 'left');

		return $this->db->get('users')->row();
	}

	// --------------------------------------------------------------------------

	/**
	 * Get recent users
	 *
	 * @param     int  $limit defaults to 10
	 *
	 * @return     object
	 */
	public function get_recent($limit = 10)
	{
		$this->db->order_by('users.created_on', 'desc');
		$this->db->limit($limit);
		return $this->get_all();
	}

	// --------------------------------------------------------------------------

	/**
	 * Get all user objects
	 *
	 * @return object
	 */
	public function get_all($params = NULL)
	{
		$this->db
			->select($this->profile_table.'.*, groups.description as group_name, users.*')
			->select('organization_units.id AS unit_id')
			->select('organization_units.unit_name AS unit_name')
			->select('organization_units.unit_slug AS unit_slug')
			->select('organization_units.unit_sort_order AS unit_sort_order')
			->select('organization_units.unit_abbrevation AS unit_abbrevation')
			->select('organization_units.unit_description AS unit_description')
			->select('organization_types.id AS unit_type_id')
			->select('organization_types.type_name AS unit_type_name')
			->select('organization_types.type_description AS unit_type_description')
			->select('organization_types.type_slug AS unit_type_slug')
			->select('organization_types.type_level AS unit_type_level')
			->select('organization_types.ordering_count AS unit_type_ordering_count')
			->select('cra_profile.user_id as cra_user_id')
			->select('cra_profile.display_name as cra_display_name')

			->join('groups', 'groups.id = users.group_id','left')
			->join('profiles', 'profiles.user_id = users.id', 'left')
			->join('organization_memberships', 'organization_memberships.membership_user = users.id', 'left')
			->join('organization_units', 'organization_memberships.membership_unit = organization_units.id', 'left')
			->join('organization_types', 'organization_units.unit_type = organization_types.id', 'left')
			->join('profiles as cra_profile', 'profiles.id_cra = cra_profile.user_id', 'left')

			->order_by('groups.sort_order, '.$this->profile_table.'.display_name', 'asc');

		if (isset($params['display_name']))
		{
			$this->db->like('profiles.display_name', $params['display_name']);
		}

		$res = parent::get_all();
		return $res;
	}

	// --------------------------------------------------------------------------

	/**
	 * Create a new user
	 *
	 * @param array $input
	 *
	 * @return int|true
	 */
	public function add($input = array())
	{
		$this->load->helper('date');

		return parent::insert(array(
			'email' => $input->email,
			'password' => $input->password,
			'salt' => $input->salt,
			'role' => empty($input->role) ? 'user' : $input->role,
			'active' => 0,
			'lang' => $this->config->item('default_language'),
			'activation_code' => $input->activation_code,
			'created_on' => now(),
			'last_login' => now(),
			'ip' => $this->input->ip_address()
		));
	}

	// --------------------------------------------------------------------------

	/**
	 * Update the last login time
	 *
	 * @param int $id
	 */
	public function update_last_login($id)
	{
		$this->db->update('users', array('last_login' => now()), array('id' => $id));
	}

	// --------------------------------------------------------------------------

	/**
	 * Activate a newly created user
	 *
	 * @param int $id
	 *
	 * @return bool
	 */
	public function activate($id)
	{
		return parent::update($id, array('active' => 1, 'activation_code' => ''));
	}

	// --------------------------------------------------------------------------

	/**
	 * Count by
	 *
	 * @param array $params
	 *
	 * @return int
	 */
	public function count_by($params = array())
	{
		$this->db->from($this->_table)
			->join('profiles', 'users.id = profiles.user_id', 'left');

		if ( ! empty($params['active']))
		{
			$params['active'] = $params['active'] === 2 ? 0 : $params['active'];
			$this->db->where('users.active', $params['active']);
		}

		if ( ! empty($params['group_id']))
		{
			$this->db->where('group_id', $params['group_id']);
		}

		if ( ! empty($params['id_cra']))
		{
			$this->db->where('id_cra', $params['id_cra']);
		}

		if ( ! empty($params['name']))
		{
			$this->db
				->like('users.username', trim($params['name']))
				->or_like('users.email', trim($params['name']))
				->or_like('profiles.display_name', trim($params['name']));
		}

		return $this->db->count_all_results();
	}

	// --------------------------------------------------------------------------

	/**
	 * Get by many
	 *
	 * @param array $params
	 *
	 * @return object
	 */
	public function get_many_by($params = array())
	{
		if ( ! empty($params['active']))
		{
			$params['active'] = $params['active'] === 2 ? 0 : $params['active'];
			$this->db->where('active', $params['active']);
		}

		if ( ! empty($params['group_id']))
		{
			$this->db->where('group_id IN ('.$params['group_id'].')', NULL, FALSE);
		}

		if ( ! empty($params['unit_id']))
		{
			$this->db->where('default_organization_units.id IN ('.$params['unit_id'].')', NULL, FALSE);
		}

		if ( ! empty($params['name']))
		{
			$this->db
				->where('(default_users.username like "%'.trim($params['name']).'%" OR default_users.email like "%'.trim($params['name']).'%" OR default_profiles.display_name like "%'.trim($params['name']).'%")', NULL, FALSE);
		}

		if ( ! empty($params['exact_name']))
		{
			$this->db
				->where('(default_users.username = "'.trim($params['exact_name']).'" OR default_users.email = "'.trim($params['exact_name']).'" OR default_profiles.display_name = "'.trim($params['exact_name']).'")', NULL, FALSE);
		}

		if ( ! empty($params['is_affiliate']))
		{
			$params['is_affiliate'] = ($params['is_affiliate']=='yes') ? 1 : 0;
			$this->db->where('default_profiles.is_affiliate', $params['is_affiliate']);
		}

		if ( ! empty($params['id_cra']))
		{
			$this->db->where('profiles.id_cra', $params['id_cra']);
		}

		if ( ! empty($params['user_id']))
		{
			$this->db->where('profiles.user_id', $params['user_id']);
		}

		$retval = $this->get_all();
		return $retval;
	}

	public function get_user_by_api($api_user, $api_key){
		$this->db->select('default_users.*, default_groups.name as group');
		$this->db->join('default_groups','default_groups.id = default_users.group_id');
		$res = $this->db->where('username', $api_user)->where('api_key', $api_key)->get('default_users');
		return $res->row_array();
	}


	/**
	 * Set Memberships User
	 *
	 * @param array $data_membership
	 *
	 * @return boolean
	 */

	public function set_membership_user($data_membership) {
		$data_membership['created'] = date("Y-m-d H:i:s");;
		return $this->db->insert('organization_memberships', $data_membership);
	}

	public function get_recent_group($date, $group) {
		$this->db->where('groups.name', $group);
		$this->db->where("FROM_UNIXTIME(created_on,'%Y-%m-%d')",$date);
		return $this->get_all();
	}

	public function become_affiliate($id_user)
	{
		$this->db->where('user_id',$id_user);
		return $this->db->update('profiles',array('is_affiliate'=>1));
	}
}