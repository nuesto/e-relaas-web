<p>Halo <?php echo $user->display_name; ?>,</p>

<p>Sepertinya anda meminta untuk mengatur ulang password. Silahkan klik pada tautan berikut untuk menyelesaikan proses atur ulang password : <?php echo BASE_URL.'users/reset_pass/'.$user->forgotten_password_code; ?></p>

<p>Jika anda tidak meminta untuk mengatur ulang password silahkan abaikan pesan ini. Tidak ada tindakan lebih lanjut yang diperlukan.</p>	