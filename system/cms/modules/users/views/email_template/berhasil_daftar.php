<p>Halo Sahabat DuaKodiKartika.com,</p>

<p>Terima kasih atas kesediaan Sahabat bergabung menjadi member DuaKodiKartika.com</p>

<p>Sahabat bisa login dengan menggunakan akun<br>
Email: <?php echo $email; ?><br>
Password: (tidak ditampilkan)</p>

<p>Saat ini Sahabat telah menjadi Member namum belum dapat mengakses konten video pembelajaran DuaKodiKartika.com. Sahabat dapat mengakses konten pembelajaran tersebut dengan melanjutkan proses pemesanan dan pembayaran.</p>

<p>Untuk melakukan pemesanan silahkan klik link berikut : <a href="<?php echo base_url().'commerce/cart/add?product='.$this->variables->__get('default_id_product'); ?>">Beli Sekarang</a></p>

<p>Salam hangat,<br>
DuaKodiKartika.com</p>