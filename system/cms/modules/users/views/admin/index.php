<div class="page-header">
	<h1><?php echo lang('user:list_title') ?></h1>

	<?php if (group_has_role('users', 'create_users') AND group_has_role('users', 'import_users')) : ?>
	<div class="btn-group content-toolbar">
		<a class="btn btn-yellow btn-sm" href="<?php echo site_url('admin/users/import_user').'?'.$_SERVER['QUERY_STRING']; ?>">
			<i class="icon-plus"></i>
			<span class="no-text-shadow"><?php echo lang('buttons:import'); ?></span>
		</a>
	</div>
	<?php endif; ?>

	<?php if (group_has_role('users', 'create_users')) : ?>
	<div class="btn-group content-toolbar">
		<a class="btn btn-yellow btn-sm" href="<?php echo site_url('admin/users/create').'?'.$_SERVER['QUERY_STRING']; ?>">
			<i class="icon-plus"></i>
			<span class="no-text-shadow"><?php echo lang('user:add_title'); ?></span>
		</a>
	</div>
	<?php endif; ?>
</div>

<div class="tabbable tab-link">
	<ul class="nav nav-tabs">
		<?php
		$class = '';
		if($this->input->get('f_group') == NULL){
			$class = 'active';
		}
		?>
		<li class="<?php echo $class; ?>">
			<a href="<?php echo site_url('admin/users/index'); ?>"><?php echo lang('user:all_group_label'); ?></a>
		</li>

		<?php $group_count = 0; foreach($groups_select as $id => $group){
			$active_group_id = $this->input->get('f_group');

			$class = '';
			if($active_group_id == $id) $class = 'active';
			$this->db->distinct();
			$this->db->select('users.id, organization_memberships.membership_user');
			$this->db->where('group_id', $id);

			if(! group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users') AND isset($unit_lists)) {
				$this->db->where('membership_unit IN ('.implode(',', $unit_lists).')', NULL, FALSE);
			}


			if($this->current_user->group != 'admin' && group_has_role('users','view_own_account_crm')){
				$this->db->where('profiles.id_cra', $cra_user_id);
			}

			$this->db->from('users');
			$this->db->join(
				'profiles', $this->db->dbprefix('users').'.id='.$this->db->dbprefix('profiles').'.user_id',
				'LEFT');
			$this->db->join(
				'organization_memberships', $this->db->dbprefix('users').'.id='.$this->db->dbprefix('organization_memberships').'.membership_user',
				'LEFT');
			$res = $this->db->get()->result_array();
			$count = count($res);
			$group_count++;
			?>

			<?php if ($group_count >5 ) {
				if ($group_count == 6) { ?>
				<li class="dropdown <?php if ($this->input->get('f_group')) echo 'active'; ?>">
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">
						<?php echo lang('user:other_group_label'); ?> &nbsp;
						<i class="icon-caret-down bigger-110 width-auto"></i>
					</a>

					<ul class="dropdown-menu dropdown-info">

						<li class="<?php echo $class; ?>">
							<a href="<?php echo site_url('admin/users/index').'?f_group='.$id; ?>"><?php echo $group; ?> <span class="badge badge-inverse"><?php echo $count; ?></span></a>
						</li>


				<?php } else { ?>
						<li class="<?php echo $class; ?>">
							<a href="<?php echo site_url('admin/users/index').'?f_group='.$id; ?>"><?php echo $group; ?> <span class="badge badge-inverse"><?php echo $count; ?></span></a>
						</li>
				<?php } ?>

			<?php } else { ?>
			<li class="<?php echo $class; ?>">
				<a href="<?php echo site_url('admin/users/index').'?f_group='.$id; ?>"><?php echo $group; ?> <span class="badge badge-inverse"><?php echo $count; ?></span></a>
			</li>
			<?php } ?>
		<?php } ?>
		<?php if ($group_count>5) echo '</ul>
		</li>'; ?>
	</ul>
</div>

<?php template_partial('filters') ?>

<?php echo form_open('admin/users/action?'.$_SERVER['QUERY_STRING'],array('id'=>'form_action_user')) ?>

	<div id="filter-stage">
		<?php template_partial('tables/users') ?>
	</div>

	<div class="table_action_buttons">
		<button disabled="" type="submit" name="btnAction" value="activate" class="btn btn-sm btn-info">
			<span>Aktifkan</span>
		</button>

		<button disabled="" type="submit" name="btnAction" value="delete" class="btn btn-sm btn-danger confirm-delete">
			<span>Hapus</span>
		</button>
		<?php /* $this->load->view('admin/partials/buttons', array('buttons' => array('activate', 'delete') )) */ ?>
	</div>

<?php echo form_close() ?>

<script>
	$(document).ready(function(){
		$(".confirm-delete").on(ace.click_event, function(event) {
			event.preventDefault();
			bootbox.confirm("Anda yakin akan menjalankan aksi ini? Aksi ini tidak dapat dikembalikan.", function(result) {
				if(result) {
					var form = $('#form_action_user');
					form.append('<input type="hidden" name="btnAction" value="delete">');
					form.unbind('submit');
					form.submit();
				}
			});
		});
	});
</script>
