<?php if (!empty($users)): ?>
	<table border="0" class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th with="30" class="align-center"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
				<th><?php echo lang('user:name_label');?></th>
				<th><?php echo lang('user:username');?></th>
				<th class="collapse"><?php echo lang('user:email_label');?></th>

				<?php if($this->input->get('f_group') == NULL){ ?>
				<th><?php echo lang('user:group_label');?></th>
				<?php } ?>

				<?php if($this->input->get('f_group') == $id_crm_group){ ?>
				<th><?php echo lang('user:cra_label');?></th>
				<?php } ?>

				<th class="collapse"><?php echo lang('user:active') ?></th>
				<th class="collapse"><?php echo lang('user:joined_label');?></th>
				<th class="collapse"><?php echo lang('user:last_visit_label');?></th>
				<th width="200"></th>
			</tr>
		</thead>
		
		<tbody>
			<?php $link_profiles = Settings::get('enable_profiles') ?>
			<?php foreach ($users as $member): ?>
				<tr>
					<td class="align-center"><?php echo form_checkbox('action_to[]', $member->id) ?></td>
					<td>
					<?php 
					if($member->display_name == NULL){
						$display_name = lang('user:not_completed_yet');
					}else{
						$display_name = $member->display_name;
					}

					if ($link_profiles){

						if(group_has_role('users', 'view_all_account') OR group_has_role('users', 'view_all_profile')){

							echo anchor('admin/users/view/' . $member->id . '?' . $_SERVER['QUERY_STRING'], $display_name, array());
						}elseif((group_has_role('users', 'view_own_account') OR group_has_role('users', 'view_own_profile')) AND $member->id == $this->current_user->id){
							echo anchor('admin/users/view/' . $member->id . '?' . $_SERVER['QUERY_STRING'], $display_name, array());
						} else {
							echo $display_name;
						}
					}else{
						echo $display_name;
					}

					if($member->active == 0){
						echo ' ('.lang('user:inactive_title').')';
					}
					?>
					</td>
					<td><?php echo $member->username; ?></td>
					<td class="collapse"><?php echo mailto($member->email) ?></td>

					<?php if($this->input->get('f_group') == NULL){ ?>
					<td><?php echo ($member->group_name) ? $member->group_name : '-'; ?></td>
					<?php } ?>

					<?php if($this->input->get('f_group') == $id_crm_group){ ?>
					<td><?php echo ($member->cra_display_name) ? $member->cra_display_name : '-'; ?></td>
					<?php } ?>

					<td class="collapse"><?php echo $member->active ? lang('global:yes') : lang('global:no')  ?></td>
					<td class="collapse"><?php echo format_date($member->created_on) ?></td>
					<td class="collapse"><?php echo ($member->last_login > 0 ? format_date($member->last_login) : lang('user:never_label')) ?></td>
					<td class="actions">
						<?php 
						
						?>
						
						<?php 
						if(group_has_role('users', 'edit_all_account') OR group_has_role('users', 'edit_all_profile')){
							echo anchor('admin/users/edit/' . $member->id . '?' . $_SERVER['QUERY_STRING'], lang('global:edit'), array('class'=>'btn btn-xs btn-info edit'));
						}elseif((group_has_role('users', 'edit_own_account') OR group_has_role('users', 'edit_own_profile')) AND $member->id == $this->current_user->id){
							echo anchor('admin/users/edit/' . $member->id . '?' . $_SERVER['QUERY_STRING'], lang('global:edit'), array('class'=>'btn btn-xs btn-info edit'));
						}
						?>
						
						<?php 
						if(group_has_role('users', 'delete_users')){
							echo anchor('admin/users/delete/' . $member->id . '?' . $_SERVER['QUERY_STRING'], lang('global:delete'), array('class'=>'confirm btn btn-xs btn-danger delete'));
						}
						?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	
	<?php $this->load->view('admin/partials/pagination') ?>
<?php else : ?>
	<div class="well"><?php echo lang('user:no_registred');?></div>
<?php endif ?>