<?php echo form_open('',  array('class' => 'form-inline', 'method' => 'get')) ?>
	<?php echo form_hidden('f_module', $module_details['slug']) ?>	

	<div class="form-group">
		<label><?php echo lang('user:active', 'f_active') ?></label>
		<?php echo form_dropdown('f_active', array(0 => lang('global:select-all'), 1 => lang('global:yes'), 2 => lang('global:no') ), $this->input->get('f_active')) ?>
	</div>

	<?php
	$active_group_id = $this->input->get('f_group');
	echo form_hidden('f_group', $active_group_id);
	?>
	
	<div class="form-group">
		<label for="f_keywords"><?php echo lang('global:keywords') ?></label>
		<?php echo form_input('f_keywords', $this->input->get('f_keywords')) ?></li>
	</div>

	<?php if($active_group_id == $id_crm_group){ ?>
	<div class="form-group">
		<label><?php echo lang('user:cra_label') ?></label>
		<?php 
		$options = array('' => '');
		foreach ($cra_users as $key => $value) {
			$options[$value->id] = $value->display_name;
		}
		if($this->input->get('f_id_cra') != NULL){
			$option_id_cra = $this->input->get('f_id_cra');
		}else{
			$option_id_cra = '';
		}
		echo form_dropdown('f_id_cra', $options, $option_id_cra);
		?>
	</div>		
	<?php } ?>
	
	<div class="form-group">
		<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
			<i class="icon-filter"></i>
			<?php echo lang('global:filters'); ?>
		</button>
		
		<a href="<?php echo site_url('admin/users').'?f_group='.$active_group_id; ?>" class="btn btn-xs">
			<i class="icon-remove"></i>
			<?php echo lang('buttons:clear'); ?>
		</a>
	</div>
	
<?php echo form_close() ?>