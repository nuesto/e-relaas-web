<div class="page-header">
	<h1><?php echo lang('user:import_title') ?></h1>
</div>

<?php echo form_open_multipart(uri_string().'?'.$_SERVER['QUERY_STRING'], 'class="form-horizontal" autocomplete="off"') ?>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:group_id') ?> <span>*</span></label>
	<div class="col-sm-10">
		<select name="group_name" class="col-md-3">
			<option value=""><?php echo lang('user:choose_group'); ?></option>
			<?php 
				foreach ($group as $grp) {
					if($grp->name != 'site_admin'){
						echo '<option value="'.$grp->name.'">'.$grp->description.'</option>';
					}
				}
			?>
		</select>
	</div>
</div>

<div class="well description description-cra" style="display: none">
	<b><?php echo lang('user:notif1'); ?>
	<br>1. <?php echo lang('user:notif2'); ?> ','
	<br>2. <?php echo lang('user:notif3'); ?> '\n'
	
	<hr>
	
	<?php echo lang('user:set_csv_1'); ?>
	<br>1. <?php echo lang('user:set_csv_2'); ?>
	<br>2. <?php echo lang('user:set_csv_3'); ?>
	<br>3. <?php echo lang('user:set_csv_4'); ?>
	<br>4. <?php echo lang('user:set_csv_5'); ?>
	<br>5. <?php echo lang('user:set_csv_6'); ?>
	</b>
	<br>

	<hr>

	<div class="form-group">
		<div class="col-sm-10">
			<a href="<?php echo site_url().'admin/users/download_contoh/cra'; ?>" class="btn btn-sm btn-info"><i class="icon-cloud-download"></i> Download Contoh File CSV</a>
		</div>
	</div>
</div>

<div class="well description description-crm" style="display: none">
	<b><?php echo lang('user:notif1'); ?>
	<br>1. <?php echo lang('user:notif2'); ?> ','
	<br>2. <?php echo lang('user:notif3'); ?> '\n'
	
	<hr>
	
	<?php echo lang('user:set_csv_1'); ?>
	<br>1. <?php echo lang('user:set_csv_2'); ?>
	<br>2. <?php echo lang('user:set_csv_3'); ?>
	<br>3. <?php echo lang('user:set_csv_4'); ?>
	<br>4. <?php echo lang('user:set_csv_5'); ?>
	<br>5. <?php echo lang('user:set_csv_6'); ?>
	<br>6. <?php echo lang('user:csv_field_cra'); ?>
	</b>
	<br>

	<hr>

	<div class="form-group">
		<div class="col-sm-10">
			<a href="<?php echo site_url().'admin/users/download_contoh/crm'; ?>" class="btn btn-sm btn-info"><i class="icon-cloud-download"></i> Download Contoh File CSV</a>
		</div>
	</div>
</div>

<fieldset>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:select_file') ?> <span>*</span></label>
		<div class="col-sm-10">
			<input type="file" name="file">
			<!--<?php echo form_upload('file','', 'name="file"') ?>-->
		</div>
	</div>

	<?php if (count($units) > 1) : ?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:membership_unit_label') ?></label>
		<div class="col-sm-10">
			<select name="membership_unit" class="col-md-3">
				<option value=""><?php echo lang('user:choose_membership_unit'); ?></option>
			<?php 
				foreach ($units as $unit) {
					echo '<option value="'.$unit['membership_unit']['id'].'">'.$unit['membership_unit']['unit_name'].'</option>';
				}
			?>
			</select>
		</div>
	</div>
	<?php endif; ?>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:activate_label') ?></label>
		<div class="col-sm-10">
			<select name="active" class="col-md-3">
				<option value='0'>Inactive</option>
				<option value='1'>Active</option>
				<option value='2'>Send Activation Email</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:set_header') ?> </label>
		<div class="col-sm-10">
			<?php echo form_checkbox('header', 'yes', TRUE); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
		<div class="col-md-offset-3 col-md-9">
			<button class="btn btn-sm btn-success" value="submit" name="btnAction" type="submit">
				<span><?php echo lang('buttons:import'); ?></span>
			</button>
		</div>
	</div>
</fieldset>
	
<?php echo form_close() ?>

<script type="text/javascript">
	$("[name='group_name']").change(function(){
		var id_group = $("[name='group_name']").val();

		$('.description').hide();
		$('.description-' + id_group).show();
	});
</script>
	