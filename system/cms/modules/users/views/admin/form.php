<style type="text/css">
	#picture {
		position: relative;
		display: inline-block;
	}
	#delete-pic {
		position: absolute;
		top: -5px;
		right: -9px;
		font: 20px bold;
		width: 25px;
		height: 25px;
		background: #E1DADA;
		text-align: center;
		line-height: 26px;
		border-radius: 50%;
		color: #000000;
		cursor: pointer;
	}
</style>
<div class="page-header">
	<?php if ($this->method === 'create'): ?>
		<h1><?php echo lang('user:add_title') ?></h1>
	<?php else: ?>
		<h1><?php $title = sprintf(lang('user:edit_title'), $member->old_username);
		if($forced=='forced') {
			$title = 'Lengkapi data profil pengguna';
		}
		echo $title; ?></h1>
	<?php endif ?>
</div>

<?php if ($this->method === 'create'): ?>
	<?php echo form_open_multipart(uri_string().'?'.$_SERVER['QUERY_STRING'], 'class="form-horizontal" autocomplete="off"') ?>
<?php else: ?>
	<?php echo form_open_multipart(uri_string().'?'.$_SERVER['QUERY_STRING'], 'class="form-horizontal"') ?>
	<?php echo form_hidden('row_edit_id', isset($member->row_edit_id) ? $member->row_edit_id : $member->profile_id); ?>
<?php endif ?>

<?php
$is_create_mode = FALSE;
$is_own_user = FALSE;

// check if in edit mode?
if($this->method == 'create'){
	$is_create_mode = TRUE;
}else{
	// check if user being edited is current user?
	$member_id = isset($member->id) ? $member->id : $member->row_edit_id;
	if($this->current_user->id == $member_id){
		$is_own_user = TRUE;
	}
}
?>
	
<!-- Content tab -->
<?php 
if($is_create_mode
	OR group_has_role('users', 'edit_all_account') 
	OR (group_has_role('users', 'edit_own_account') AND $is_own_user)){ 
?>

<fieldset>
	<h3 class="header smaller lighter blue col-sm-12"><?php echo lang('user_account_data_label'); ?></h3>
	
	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_email') 
		OR (group_has_role('users', 'edit_own_email') AND $is_own_user)){ 
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:email') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('email', $member->email, 'id="email"') ?>
		</div>
	</div>
	<?php }else{ 
		echo form_hidden('email', $member->email);
	} ?>
	
	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_username') 
		OR (group_has_role('users', 'edit_own_username') AND $is_own_user)){ 
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:username') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('username', $member->username, 'id="username"') ?>
		</div>
	</div>
	<?php }else{ 
		echo '<input type="hidden" name="username" id="username" value="'.$member->username.'">';
	} ?>
	
	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_group') 
		OR (group_has_role('users', 'edit_own_group') AND $is_own_user)){ 
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:group_label') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php 
			if($member->group_id != NULL AND $member->group_id != ''){
				$group_id = $member->group_id;
			}else{
				$group_id = $this->input->get('f_group');
			}

			if(count($groups_select)>1) {
				echo form_dropdown('group_id', array('' => lang('global:select-pick')) + $groups_select, $group_id, 'id="group_id"');
			} else {
				echo '<input type="hidden" name="group_id" id="group_id" value="'.key($groups_select).'">';
				echo '<label>'.$groups_select[key($groups_select)].'</label>';
			}
			?>
		</div>
	</div>
	<?php }else{ 
		echo '<input type="hidden" name="group_id" id="group_id" value="'.$member->group_id.'">';
	} ?>
	
	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_status') 
		OR (group_has_role('users', 'edit_own_status') AND $is_own_user)){ 
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:activate_label') ?></label>
		<div class="col-sm-10">
			<?php $options = array(0 => lang('user:do_not_activate'), 1 => lang('user:active'), 2 => lang('user:send_activation_email')) ?>
			<?php echo form_dropdown('active', $options, $member->active, 'id="active"') ?>
		</div>
	</div>
	<?php }else{ 
		echo '<input type="hidden" name="active" id="active" value="'.$member->active.'">';
	} ?>
	
	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_password') 
		OR (group_has_role('users', 'edit_own_password') AND $is_own_user)){ 
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right">
			<?php echo lang('global:password') ?>
			<?php if ($this->method == 'create'): ?> <span>*</span><?php endif ?>
		</label>
		<div class="col-sm-10">
			<?php echo form_password('password', '', 'id="password" autocomplete="off"') ?>
		</div>
	</div>
	<?php }else{ 
		echo '<input type="hidden" name="password" id="password" value="">';
	} ?>

</fieldset>	

<?php }else{ 

echo '<input type="hidden" name="email" id="email" value="'.$member->email.'">';
echo '<input type="hidden" name="username" id="username" value="'.$member->username.'">';
if($member->group_id != NULL AND $member->group_id != ''){
	$group_id = $member->group_id;
}else{
	$group_id = $this->input->get('f_group');
}
echo '<input type="hidden" name="group_id" id="group_id" value="'.$group_id.'">';
echo '<input type="hidden" name="active" id="active" value="'.$member->active.'">';
echo form_hidden('password', '');

} //if ?>

<?php 
if($is_create_mode
	OR group_has_role('users', 'edit_all_profile') 
	OR (group_has_role('users', 'edit_own_profile') AND $is_own_user)){ 
?>

<fieldset>
	<h3 class="header smaller lighter blue col-sm-12"><?php echo lang('profile_user_details_label'); ?></h3>

	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_picture') 
		OR (group_has_role('users', 'edit_own_picture') AND $is_own_user)){ 
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:picture_id'); ?> </label>
		<div class="col-sm-10">
			<?php if (isset($member->picture_id)) : ?>
				<span id="picture">
					<span id="delete-pic">&#10006;</span>
					<img src="<?php echo base_url(); ?>files/thumb/<?php echo $member->picture_id; ?>/200/250/fit" />
				</span>
			<?php endif; ?>
			<input type="file" name="picture_id">
		</div>
	</div>
	<?php }else{ 
		$picture_id = (isset($member->picture_id)) ? $member->picture_id : '';
		echo form_hidden('picture_id',  $picture_id);
	} ?>

	<?php 
	if($is_create_mode
		OR group_has_role('users', 'edit_all_displayname') 
		OR (group_has_role('users', 'edit_own_displayname') AND $is_own_user)){ 
	?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('profile_display_name') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('display_name', $display_name, 'id="display_name"') ?>
		</div>
	</div>
	<?php }else{ 
		echo form_hidden('display_name',  $member->display_name);
	} ?>

	<?php } ?>


	<?php 
		$value = NULL;
		if($this->input->post('no_hp') != NULL){
			$value = $this->input->post('no_hp');
		}elseif($mode == 'edit'){
			$value = $fields['no_hp'];
		}
	?>

	<div class="form-group" id="form-group-no-hp" <?php echo ($group_id != $id_cra_group) ? 'style="display:none"': ''; ?>>
		<label class="col-sm-2 control-label no-padding-right" for="provinsi"><?php echo lang('user:no_hp'); ?>*<span></span></label>

		<div class="col-sm-6">
			<input type="text" value="<?php echo $value ?>" name="no_hp" id="no_hp" class="col-xs-10 col-sm-5">
		</div>
	</div>

	<div class="form-group" id="form-group-cra-users">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:cra_label') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php 
			$options = array('' => '');
			foreach ($cra_users as $key => $value) {
				$options[$value->id] = $value->display_name;
			}
			if(isset($member->id_cra)){
				$option_id_cra = $member->id_cra;
			}elseif($this->input->get('f_id_cra') != NULL){
				$option_id_cra = $this->input->get('f_id_cra');
			}else{
				$option_id_cra = '';
			}
			echo form_dropdown('id_cra', $options, $option_id_cra, 'id="id_cra"');
			?>
		</div>
	</div>

</fieldset>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-sm btn-info" value="save_exit" name="btnAction" type="submit">
			<span><?php echo lang('buttons:save_exit'); ?></span>
		</button>
		
		<?php if ($this->method == 'create'){ ?>
		
		<a class="btn btn-sm btn-info cancel" href="<?php echo base_url(); ?>admin/users?<?php echo $_SERVER['QUERY_STRING']; ?>"><?php echo lang('buttons:cancel'); ?></a>
		
		<?php }else{ ?>
		
		<a class="btn btn-sm btn-info cancel" href="<?php echo base_url(); ?>admin/users/view/<?php echo isset($member->id) ? $member->id : $member->row_edit_id; ?>?<?php echo $_SERVER['QUERY_STRING']; ?>"><?php echo lang('buttons:cancel'); ?></a>
		
		<?php } ?>
	</div>
</div>

<?php echo form_close() ?>

<script type="text/javascript">
	jQuery(function($) {
		$(".chosen-select").chosen();
		$('#chosen-multiple-style').on('click', function(e){
			var target = $(e.target).find('input[type=radio]');
			var which = parseInt(target.val());
			if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
			 else $('#form-field-select-4').removeClass('tag-input-style');
		});
	});

	check_available_group($('#group_id').val());
	function check_available_group(group_id) {
		$('.unit_list').each(function(key,value) {
			var unitType = $(this).data('unitType').toString().split(',');
			// alert(group_id);
			if(group_id!='' && $.inArray(group_id, unitType) < 0 && group_id != 1) {
				$(this).find('input').attr('disabled',true);
			} else {
				$(this).find('input').removeAttr('disabled');
			}
		});
	}

	$(document).ready(function() {
		$('#group_id').change(function() {
			var id_group = $(this).val();
			if(id_group == 2){
				$("#form-group-no-hp").show();
			}else{
				$("#form-group-no-hp").hide();
			}
			console.log(id_group);
			check_available_group($(this).val());
		});

		$('#btnClearOrganization').click(function() {
			clear_organization();
		});

		show_hide_crm_dropdown();
		$('#group_id').change(function() {
			show_hide_crm_dropdown();	
		});
	});

	function show_hide_crm_dropdown(){
		$('#form-group-cra-users').hide();
		if($('#group_id').val() == <?php echo $id_crm_group; ?>){
			$('#form-group-cra-users').show();
		}
	}

	function clear_organization() {
		$('.unit_list').each(function(key,value) {
			$(this).find('input').removeAttr('checked');
		});
	}

	$(document).ready(function() {
		$('#delete-pic').click(function() {
			$('#picture').after('<input type="hidden" name="delete_pic" value="1">');
			$('#picture').remove();
		});
	});
</script>