<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['users:role_manage_users'] = 'Pengaturan pengguna';
$lang['users:role_manage_own_unit_users'] = 'Pengaturan pengguna dalam unit yang sama';
$lang['users:role_manage_user_profile_fields'] = 'Mengatur fields pada profil pengguna';
$lang['users:role_create_users'] = 'Menambahkan pengguna';
$lang['users:role_delete_users'] = 'Menghapus pengguna';
$lang['users:role_import_users'] = 'Import pengguna';

// accounts
$lang['users:role_view_all_account'] = 'Melihat semua data akun';
$lang['users:role_view_own_account'] = 'Melihat data akun sendiri';
$lang['users:role_view_own_account_crm'] = 'Melihat data akun semua anggota tim';
$lang['users:role_edit_all_account'] = 'Mengedit semua data akun';
$lang['users:role_edit_own_account'] = 'Mengedit data akun sendiri';
$lang['users:role_edit_all_email'] = 'Mengedit semua email';
$lang['users:role_edit_all_username'] = 'Mengedit semua username';
$lang['users:role_edit_all_group'] = 'Mengedit semua grup';
$lang['users:role_edit_all_status'] = 'Mengedit semua status';
$lang['users:role_edit_all_password'] = 'Mengedit semua password';
$lang['users:role_edit_own_email'] = 'Mengedit email sendiri';
$lang['users:role_edit_own_username'] = 'Mengedit username sendiri';
$lang['users:role_edit_own_group'] = 'Mengedit grup sendiri';
$lang['users:role_edit_own_status'] = 'Mengedit status sendiri';
$lang['users:role_edit_own_password'] = 'Mengedit password sendiri';

// profiles
$lang['users:role_view_all_profile'] = 'Melihat profil semua pengguna';
$lang['users:role_view_own_profile'] = 'Melihat profil sendiri';
$lang['users:role_edit_all_profile'] = 'Mengedit profil semua pengguna';
$lang['users:role_edit_own_profile'] = 'Mengedit profil sendiri';
$lang['users:role_edit_all_displayname'] = 'Mengedit display name semua pengguna';
$lang['users:role_edit_own_displayname'] = 'Mengedit display name sendiri';
$lang['users:role_edit_all_picture'] = 'Mengedit profil picture semua pengguna';
$lang['users:role_edit_own_picture'] = 'Mengedit profil picture sendiri';

// organization
$lang['users:role_edit_all_organization_unit'] = 'Mengedit organisasi semua pengguna';
$lang['users:role_edit_own_organization_unit'] = 'Mengedit organisasi sendiri';