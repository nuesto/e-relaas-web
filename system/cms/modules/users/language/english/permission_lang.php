<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['users:role_manage_users'] = 'Manage users';
$lang['users:role_manage_own_unit_users'] = 'Manage users in the same unit';
$lang['users:role_manage_user_profile_fields'] = 'Manage user profile fields';
$lang['users:role_create_users'] = 'Create users';
$lang['users:role_delete_users'] = 'Delete users';
$lang['users:role_import_users'] = 'Import users';

// accounts
$lang['users:role_view_all_account'] = 'View all account data';
$lang['users:role_view_own_account'] = 'View own account data';
$lang['users:role_edit_all_account'] = 'Edit all account data';
$lang['users:role_edit_own_account'] = 'Edit own account data';
$lang['users:role_edit_all_email'] = 'Edit all email';
$lang['users:role_edit_all_username'] = 'Edit all username';
$lang['users:role_edit_all_group'] = 'Edit all grup';
$lang['users:role_edit_all_status'] = 'Edit all status';
$lang['users:role_edit_all_password'] = 'Edit all password';
$lang['users:role_edit_own_email'] = 'Edit own email';
$lang['users:role_edit_own_username'] = 'Edit own username';
$lang['users:role_edit_own_group'] = 'Edit own group';
$lang['users:role_edit_own_status'] = 'Edit own status';
$lang['users:role_edit_own_password'] = 'Edit own password';

// profile
$lang['users:role_view_all_profile'] = 'View all profile';
$lang['users:role_view_own_profile'] = 'View own profile';
$lang['users:role_edit_all_profile'] = 'Edit all profile';
$lang['users:role_edit_own_profile'] = 'Edit own profile';
$lang['users:role_edit_all_displayname'] = 'Edit all display name';
$lang['users:role_edit_own_displayname'] = 'Edit own display name';
$lang['users:role_edit_all_picture'] = 'Edit all profile picture';
$lang['users:role_edit_own_picture'] = 'Edit own profile picture';

// organization
$lang['users:role_edit_all_organization_unit'] = 'Edit all user\'s organization';
$lang['users:role_edit_own_organization_unit'] = 'Edit own organization';