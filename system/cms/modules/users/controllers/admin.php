<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Admin controller for the users module
 *
 * @author		 PyroCMS Dev Team
 * @package	 PyroCMS\Core\Modules\Users\Controllers
 */
class Admin extends Admin_Controller
{

	protected $section = 'users';

	/**
	 * Validation for basic profile
	 * data. The rest of the validation is
	 * built by streams.
	 *
	 * @var array
	 */
	private $validation_rules = array(
		'email' => array(
			'field' => 'email',
			'label' => 'lang:global:email',
			'rules' => 'required|max_length[60]|valid_email'
		),
		'password' => array(
			'field' => 'password',
			'label' => 'lang:global:password',
			'rules' => 'min_length[6]|max_length[20]'
		),
		'username' => array(
			'field' => 'username',
			'label' => 'lang:user_username',
			'rules' => 'max_length[20]'
		),
		array(
			'field' => 'group_id',
			'label' => 'lang:user_group_label',
			'rules' => 'required|callback__group_check'
		),
		array(
			'field' => 'active',
			'label' => 'lang:user_active_label',
			'rules' => ''
		),
		array(
			'field' => 'display_name',
			'label' => 'lang:profile_display_name',
			'rules' => 'required|max_length[50]'
		)
	);

	/**
	 * Constructor method
	 */
	public function __construct()
	{
		parent::__construct();

		// Load the required classes
		$this->load->model('user_m');
		$this->load->model('groups/group_m');
		$this->load->helper('user');
		$this->load->library('form_validation');
		$this->lang->load('user');

		// Load classes from module Organization
		$this->load->model('organization/units_m');
		$this->load->model('organization/memberships_m');
		$this->load->model('human_resource/personel_m');
		$this->load->model('groups/group_m');
		$this->load->model('users/user_m');
		$this->lang->load('organization/organization');
		$this->load->library('organization/Organization');
		$this->config->load('organization');
		
		if ($this->current_user->group != 'admin') 
		{

			if (!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
				$memberships = $this->organization->get_membership_by_user($this->current_user->id);
				$data['membership_units'] = $memberships['entries'];

				$unit_lists = array();

				foreach ($memberships['entries'] as $key => $value) {
					$unit_lists[] = $value['membership_unit']['id'];
				}

				if(count($unit_lists)>0) {
					$res_available_groups = $this->group_m->get_available_groups_by_units($unit_lists);
					$available_groups = array();
					foreach ($res_available_groups as $value) {
						$available_groups = array_merge($available_groups, json_decode($value['available_groups']));
					}
					$available_groups = implode(",", $available_groups);

					$this->template->groups = $this->group_m->where_not_in('name', 'admin')->get_all(array('available_groups' => $available_groups));
				} else {
					$this->template->groups = $this->group_m->where_not_in('name', 'admin')->get_all();
				}
			} else {
				$this->template->groups = $this->group_m->where_not_in('name', 'admin')->get_all();
			}

			if(group_has_role('users','view_own_account_crm')){
				$this->template->groups = $this->group_m->where_in('name', 'crm')->get_all();
			}
		}
		else 
		{
			$this->template->groups = $this->group_m->get_all();
		}
		
		$this->template->groups_select = array_for_select($this->template->groups, 'id', 'description');
	}

	/**
	 * List all users
	 */
	public function index()
	{
		if(! group_has_role('users', 'manage_users') AND ! group_has_role('users', 'manage_own_unit_users')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		$base_where = array('active' => 0);

		// ---------------------------
		// User Filters
		// ---------------------------

		// Determine active param
		$base_where['active'] = $this->input->get('f_module') ? (int)$this->input->get('f_active') : $base_where['active'];

		// Determine organization unit param
		$data['membership_units'] = array();
		if(!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			$memberships = $this->organization->get_membership_by_user($this->current_user->id);
			$data['membership_units'] = $memberships['entries'];

			foreach ($memberships['entries'] as $key => $value) {
				$data['unit_lists'][] = $value['membership_unit']['id'];
			}

			if (!isset($_GET['f_unit']) AND isset($data['unit_lists'])) {
				$_GET['f_unit'] = implode(',', $data['unit_lists']);
			}
		}

		$base_where = $this->input->get('f_unit') ? $base_where + array('unit_id' => $this->input->get('f_unit')) : $base_where;

		// Determine group param
		$base_where = $this->input->get('f_group') ? $base_where + array('group_id' => (int)$this->input->get('f_group')) : $base_where;

		// Keyphrase param
		$base_where = $this->input->get('f_keywords') ? $base_where + array('name' => $this->input->get('f_keywords')) : $base_where;

		$base_where = $this->input->get('f_is_affiliate') ? $base_where + array('is_affiliate' => $this->input->get('f_is_affiliate')) : $base_where;

		$base_where = $this->input->get('f_id_cra') ? $base_where + array('id_cra' => $this->input->get('f_id_cra')) : $base_where;


		$cra_user_id = null;
		if(group_has_role('users','view_own_account_crm')){
			// Get CRA User
			$cra_user_id = $this->user_m->get(array('id' => $this->current_user->id))->cra_user_id;
			$base_where = $base_where + array('id_cra' => $cra_user_id);
		}

		// Create pagination links
		$pagination = create_pagination('admin/users/index', $this->user_m->count_by($base_where));

		//Skip admin
		$skip_admin = ( $this->current_user->group != 'admin' ) ? 'admin' : '';

		// Using this data, get the relevant results
		$this->db->order_by('active', 'desc')
			->limit($pagination['limit'], $pagination['offset']);

		if($skip_admin!='') {
			$this->db->where_not_in('groups.name', $skip_admin);
		}

		$users = $this->user_m->get_many_by($base_where);
		
		$user2 = array();
		$id_user = array();
		
		foreach ($users as $value) {
			$user2[$value->id] = (array)$value;

			$id_user[] = $value->id;
		}

		$memberships_list = $this->memberships_m->get_memberships_by_multi_user($id_user);
		$unit_list = array();

		foreach ($memberships_list as $value) {
			$unit_list[$value['membership_user']][] = $value;
		}

		foreach ($user2 as $key => $value) {
			if(array_key_exists($key, $unit_list)) {
				$user2[$key]['units'] = $unit_list[$key];
			} else {
				$user2[$key]['units'] = array();
			}

			$user2[$key] = (object)$user2[$key];
		}

		// if f_group == affiliate get landing page url
		$group = $this->group_m->get($this->input->get('f_group'));
		if(isset($group) and $group->name == 'affiliate') {
			$this->load->model('landing_page/konten_m');
			foreach ($user2 as $key => $affiliate) {
				$landing_page = $this->konten_m->get_konten_by_id_affiliate($affiliate->id);
				$user2[$key]->landing_page_url = base_url().'affiliate/'.$landing_page['uri_segment_string'];
			}
		}
		
		// Unset the layout if we have an ajax request
		if ($this->input->is_ajax_request())
		{
			$this->template->set_layout(false);
		}

		// set session page that user view
		$this->session->set_userdata('page',$pagination['current_page']);

		// pupulate list of users which group = CRA
		$cra_group = $this->group_m->get_group_by_slug('cra');
		$id_cra_group = $cra_group->id;
		$cra_users = $this->user_m->get_many_by(array('group_id' => $id_cra_group));

		$crm_group = $this->group_m->get_group_by_slug('crm');
		$id_crm_group = $crm_group->id;


		// Render the view
		$this->template
			->title($this->module_details['name'])
			->set('pagination', $pagination)
			->set('users', $user2)
			->set('group',$group)
			->set('id_crm_group', $id_crm_group)
			->set('cra_users', $cra_users)
			->set('cra_user_id', $cra_user_id)
			->set_partial('filters', 'admin/partials/filters')
			->set_partial('tables/users', 'admin/tables/users');

		$this->input->is_ajax_request() ? $this->template->build('admin/tables/users', $data) : $this->template->build('admin/index', $data);
	}
	
	/**
	 * View a user profile based on the username
	 *
	 * @param string $username The Username or ID of the user
	 */
	public function view($id = NULL)
	{
		// If $id is NULL then make it current user id
		if($id == NULL){
			$id = $this->current_user->id;
		}
		$user = $this->user_m->get(array('id' => $id));

		// No user? Show a 404 error
		$user or show_404();

		// check permission
		if(!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);
			if(count($memberships_current_user['entries'])!=0) {
				
				if (count($memberships_current_user['entries']) < 1) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
				
				$unit_current_user = $memberships_current_user['entries'];

				foreach ($unit_current_user as $key => $value) {
					$unit_lists[] = $value['membership_unit']['id'];
				}

				$memberships_user = $this->organization->get_membership_by_user($id);
				
				if (count($memberships_user['entries']) < 1) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}

				$unit_user = $memberships_user['entries'];

				foreach ($unit_user as $key => $value) {
					$unit_user_lists[] = $value['membership_unit']['id'];
				}

				if (in_array($unit_user_lists, $unit_lists)) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}


		if($id == NULL OR $this->current_user->id == $id){
			if(! (group_has_role('users', 'view_own_account') OR group_has_role('users', 'view_own_profile')
				OR group_has_role('users', 'view_all_account') OR group_has_role('users', 'view_all_profile'))){

				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}else{
			if(! (group_has_role('users', 'view_all_account') OR group_has_role('users', 'view_all_profile'))){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// get CRM group ID
		$crm_group = $this->group_m->get_group_by_slug('crm');
		$id_crm_group = $crm_group->id;

		$this->template
			->append_css('module::users.css')
			->build('admin/profile_view', array(
				'user' => $user,
				'id_crm_group' => $id_crm_group,
			));
	}
	
	/**
	 * Method for handling different form actions
	 */
	public function action()
	{
		// Pyro demo version restrction
		if (PYRO_DEMO)
		{
			$this->session->set_flashdata('notice', lang('global:demo_restrictions'));
			redirect('admin/users');
		}

		// Determine the type of action
		switch ($this->input->post('btnAction'))
		{
			case 'activate':
				$this->activate();
				break;
			case 'delete':
				$this->delete();
				break;
			default:
				redirect('admin/users');
				break;
		}
	}

	/**
	 * Create a new user
	 */
	public function create()
	{
		// check permission
		role_or_die('users', 'create_users');

		// pupulate list of users which group = CRA
		$cra_group = $this->group_m->get_group_by_slug('cra');
		$id_cra_group = $cra_group->id;
		$cra_users = $this->user_m->get_many_by(array('group_id' => $id_cra_group));

		$crm_group = $this->group_m->get_group_by_slug('crm');
		$id_crm_group = $crm_group->id;

		// Extra validation for basic data
		$this->validation_rules['email']['rules'] .= '|callback__email_check';
		$this->validation_rules['password']['rules'] .= '|required';
		// $this->validation_rules['username']['rules'] .= '|callback__username_check';

		// Add custom validation rules here
		// ....
		// 
		// $this->validation_rules['field_name'] = array(
		//	'field' => 'field_name',
		//	'label' => 'lang:field_name',
		//	'rules' => 'required'
		// );
		if($this->input->post('group_id') == $id_crm_group){
			$this->validation_rules['id_cra'] = array(
				'field' => 'id_cra',
				'label' => 'lang:user:cra_label',
				'rules' => 'required'
			);
		}

		if($this->input->post('group_id') == $id_cra_group){
			$this->validation_rules['no_hp'] = array(
				'field' => 'no_hp',
				'label' => 'lang:user:no_hp',
				'rules' => 'required|is_natural|max_length[13]|callback__no_hp_check'
			);
		}

		// Set the validation rules
		$this->form_validation->set_rules($this->validation_rules);

		$email = strtolower($this->input->post('email'));
		$password = $this->input->post('password');
		$username = $this->input->post('username');
		$group_id = $this->input->post('group_id');
		$activate = $this->input->post('active');

		// keep non-admins from creating admin accounts. If they aren't an admin then force new one as a "user" account
		$group_id = ($this->current_user->group !== 'admin' and $group_id == 1) ? 2 : $group_id;

		// Get user profile data.
		$profile_data = array();
		$profile_data['display_name'] = $this->input->post('display_name');
		if($group_id == $id_cra_group){
			$profile_data['no_hp'] = $this->input->post('no_hp');
		}

		if($this->input->post('id_cra') != ''){
			$profile_data['id_cra'] = $this->input->post('id_cra');
		}else{
			$profile_data['id_cra'] = NULL;
		}

		// Add custom profile fields here
		// ...
		// 
		// $profile_data['field_name'] = $this->input->post('field_name');
		if($_POST){
			if(strlen($_FILES['picture_id']['name']) > 100){
				$_POST['picture_file_name'] = $_FILES['picture_id']['name'];
				$this->form_validation->set_rules('picture_file_name', 'File <b>'.lang('user:picture_id').'</b>', 'max_length[100]');
			}
		}

		if ($this->form_validation->run() !== false)
		{
			$this->load->library('files/files');
			$id_folder = Files::get_id_by_name('Public');


			if($_FILES['picture_id']['name']!=''){
				$data = Files::upload($id_folder, $_FILES['picture_id']['name'], 'picture_id');
				$profile_data['picture_id'] = $data['data']['id'];
			}

			$profile_data['is_complete'] = 0;

			
			if ($activate === '2')
			{
				// we're sending an activation email regardless of settings
				Settings::temp('activation_email', true);
			}
			else
			{
				// we're either not activating or we're activating instantly without an email
				Settings::temp('activation_email', false);
			}

			$group = $this->group_m->get($group_id);

			if (Settings::get('auto_username'))
			{
				if ($this->input->post('first_name') and $this->input->post('last_name'))
				{
					$this->load->helper('url');
					$username = url_title($this->input->post('first_name').'.'.$this->input->post('last_name'), '-', true);

						// do they have a long first name + last name combo?
					if (strlen($username) > 19)
					{
							// try only the last name
						$username = url_title($this->input->post('last_name'), '-', true);

						if (strlen($username) > 19)
						{
								// even their last name is over 20 characters, snip it!
							$username = substr($username, 0, 20);
						}
					}
				}
				else
				{
						// If there is no first name/last name combo specified, let's
						// user the identifier string from their email address
					$email_parts = explode('@', $email);
					$username = $email_parts[0];
				}

					// Usernames absolutely need to be unique, so let's keep
					// trying until we get a unique one
				$i = 1;

					// make sure that we don't go over our 20 char username even with a 2 digit integer added
				$username = substr($username, 0, 18).$i;

				$username_base = $username;

				while ($this->db->where('username', $username)
					->count_all_results('users') > 0)
				{
						// make sure that we don't go over our 20 char username even with a 2 digit integer added
					$username = substr($username_base, 0, 18).$i;

					++$i;
				}
			}
			else
			{
					// The user specified a username, so let's use that.
				$username = $this->input->post('username');
			}

			// Register the user (they are activated by default if an activation email isn't requested)
			if ($user_id = $this->ion_auth->register($username, $password, $email, $group_id, $profile_data, NULL, $group->name))
			{
				if ($activate === '0')
				{
					// admin selected Inactive
					$this->ion_auth_model->deactivate($user_id);
				}

				// Fire an event. A new user has been created. 
				Events::trigger('user_created', $user_id);

				// insert all membership for this user
				$organization_memberships = $this->input->post('organization_membership');

				if(isset($organization_memberships) AND is_array($organization_memberships)){
					foreach($organization_memberships as $unit_id){
						$this->memberships_m->create_membership($user_id, $unit_id);
					}
				}

				// Set the flashdata message and redirect
				$this->session->set_flashdata('success', $this->ion_auth->messages());

				// Redirect back to the form or main page
				$this->input->post('btnAction') === 'save_exit' ? redirect('admin/users?'.$_SERVER['QUERY_STRING']) : redirect('admin/users/edit/'.$user_id.'?'.$_SERVER['QUERY_STRING']);
			}
			// Error
			else
			{
				$this->template->error_string = $this->ion_auth->errors();
			}
		}
		else
		{
			// Dirty hack that fixes the issue of having to
			// re-add all data upon an error
			if ($_POST)
			{
				$member = (object) $_POST;
			}

		}

		if ( ! isset($member))
		{
			$member = new stdClass();
		}

		// Loop through each validation rule
		foreach ($this->validation_rules as $rule)
		{
			$member->{$rule['field']} = set_value($rule['field']);
		}

		// Get organizations values
		$root_units = $this->units_m->get_units_by_level(0);
		$units = $this->organization->build_serial_tree(array(), $root_units);

		if (! group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			foreach ($units as $key => $value) {
				$unit_list[$value['id']] = $value;
			}

			$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);

			foreach ($memberships_current_user['entries'] as $value) {
				$memberships_unit_list[$value['membership_unit']['id']] = $value['membership_unit']['id'];
			}

			if(isset($memberships_unit_list)) {
				$units = array_intersect_key($unit_list, $memberships_unit_list);

				sort($units);
			}
		}

		// get provinsi and city
		$this->load->model('location/provinsi_m');
		$this->load->model('location/kota_m');

		$provinsi = $this->provinsi_m->get_provinsi();
		$id_kota = '0';
		$id_provinsi = '0';
		if($this->input->post('id_kota')) {
			$id_kota = $this->input->post('id_kota');
		}
		$provinsi_kota = $this->kota_m->get_provinsi_by_kota($id_kota);
		if(count($provinsi_kota)==1) {
			$id_provinsi = $provinsi_kota[0]['id'];
		}
		$kota = $this->kota_m->get_kota_by_provinsi($id_provinsi);
		
		$this->template
			->title($this->module_details['name'], lang('user:add_title'))
			->set('member', $member)
			->set('display_name', set_value('display_name', $this->input->post('display_name')))
			->set('units', $units)
			->set('provinsi',$provinsi)
			->set('provinsi_kota',$id_provinsi)
			->set('kota',$kota)
			->set('cra_users', $cra_users)
			->set('id_crm_group', $id_crm_group)
			->set('id_cra_group', $id_cra_group)
			->set('mode', 'create')
			->build('admin/form');
	}

	/**
	 * Edit an existing user
	 *
	 * @param int $id The id of the user.
	 */
	public function edit($id = 0, $forced = '')
	{
		// check permission
		if(!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			
			$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);
			if(count($memberships_current_user['entries'])!=0) {
				if (count($memberships_current_user['entries']) < 1) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}

				$unit_current_user = $memberships_current_user['entries'];

				foreach ($unit_current_user as $key => $value) {
					$unit_lists[] = $value['membership_unit']['id'];
				}

				$memberships_user = $this->organization->get_membership_by_user($id);
				
				if (count($memberships_user['entries']) < 1) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}

				$unit_user = $memberships_user['entries'];

				foreach ($unit_user as $key => $value) {
					$unit_user_lists[] = $value['membership_unit']['id'];
				}

				if (in_array($unit_user_lists, $unit_lists)) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}

		if($id == NULL OR $this->current_user->id == $id){
			if(! (group_has_role('users', 'edit_own_account') OR group_has_role('users', 'edit_own_profile')
				OR group_has_role('users', 'edit_all_account') OR group_has_role('users', 'edit_all_profile'))){

				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}else{
			if(! (group_has_role('users', 'edit_all_account') OR group_has_role('users', 'edit_all_profile'))){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		$this->load->library('files/files');

		// pupulate list of users which group = CRA
		$cra_group = $this->group_m->get_group_by_slug('cra');
		$id_cra_group = $cra_group->id;
		$cra_users = $this->user_m->get_many_by(array('group_id' => $id_cra_group));

		$crm_group = $this->group_m->get_group_by_slug('crm');
		$id_crm_group = $crm_group->id;
		
		// Get the user's data
		if ( ! ($member = $this->ion_auth->get_user($id)))
		{
			$this->session->set_flashdata('error', lang('user:edit_user_not_found_error'));
			redirect('admin/users');
		}
		
		// Check to see if we are changing usernames
		$old_username = $member->username;
		if ($member->username != $this->input->post('username'))
		{
			$this->validation_rules['username']['rules'] .= '|callback__username_check';
		}

		// Check to see if we are changing emails
		if ($member->email != $this->input->post('email'))
		{
			$this->validation_rules['email']['rules'] .= '|callback__email_check';
		}

		// Add custom validation rules here
		// ....
		// 
		// $this->validation_rules['field_name'] = array(
		//	'field' => 'field_name',
		//	'label' => 'lang:field_name',
		//	'rules' => 'required'
		// );
		if($this->input->post('group_id') == $id_crm_group){
			$this->validation_rules['id_cra'] = array(
				'field' => 'id_cra',
				'label' => 'lang:user:cra_label',
				'rules' => 'required'
			);
		}

		if($this->input->post('group_id') == $id_cra_group){
			$this->validation_rules['no_hp'] = array(
				'field' => 'no_hp',
				'label' => 'lang:user:no_hp',
				'rules' => 'required|is_natural|max_length[13]'
			);

			if ($member->no_hp != $this->input->post('no_hp'))
			{
				$this->validation_rules['no_hp']['rules'] .= '|callback__no_hp_check';
			}
		}

		// Set the validation rules
		$this->form_validation->set_rules($this->validation_rules);

		if ($this->form_validation->run() === true)
		{
			if (PYRO_DEMO)
			{
				$this->session->set_flashdata('notice', lang('global:demo_restrictions'));
				redirect('admin/users');
			}

			// Get the POST data
			$update_data['username'] = $this->input->post('username');
			$update_data['email'] = $this->input->post('email');
			$update_data['active'] = $this->input->post('active');
			
			// allow them to update their one group but keep users with user editing privileges from escalating their accounts to admin
			$update_data['group_id'] = ($this->current_user->group !== 'admin' and $this->input->post('group_id') == 1) ? $member->group_id : $this->input->post('group_id');

			if ($update_data['active'] === '2')
			{
				$this->ion_auth->activation_email($id);
				unset($update_data['active']);
			}
			else
			{
				$update_data['active'] = (bool) $update_data['active'];
			}

			// Password provided, hash it for storage
			if ($this->input->post('password'))
			{
				$update_data['password'] = $this->input->post('password');
			}

			// Grab profile data
			$profile_data = array();
			$profile_data['display_name'] = $this->input->post('display_name');
			if($update_data['group_id'] == $id_cra_group){
				$profile_data['no_hp'] = $this->input->post('no_hp');
			}
			$profile_data['is_complete'] = 1;

			// Add custom profile fields here
			// ...
			// 
			// $profile_data['field_name'] = $this->input->post('field_name');
			if($this->input->post('id_cra') != ''){
				$profile_data['id_cra'] = $this->input->post('id_cra');
			}else{
				$profile_data['id_cra'] = NULL;
			}

			$id_folder = Files::get_id_by_name('Public');
			if($id_folder == NULL){
				Files::create_folder(0, 'Public', 'local', '', 0);
				$id_folder = Files::get_id_by_name('Public');
			}

			if($_FILES['picture_id']['name']!=''){
				$data = Files::upload($id_folder, $_FILES['picture_id']['name'], 'picture_id');
				$profile_data['picture_id'] = $data['data']['id'];
			}

			if($this->input->post('delete_pic')==1) {
				$profile_data['picture_id'] = NULL;	
			}

			if ($this->ion_auth->update_user($id, $update_data, $profile_data))
			{
				// Update organization membership
				$organization_memberships = $this->input->post('organization_membership');

				// delete all membership for this user
				$this->memberships_m->delete_membership_by_user($id);

				// insert all membership for this user
				if(isset($organization_memberships) AND is_array($organization_memberships)){
					foreach($organization_memberships as $unit_id){
						$this->memberships_m->create_membership($id, $unit_id);
					}
				}

				// Fire an event. A user has been updated. 
				Events::trigger('user_updated', $id);

				$this->session->set_flashdata('success', $this->ion_auth->messages());
			}
			else
			{
				$this->session->set_flashdata('error', $this->ion_auth->errors());
			}

			// Redirect back to the form or main page
			if ($this->session->userdata('page')) {
				$page = $this->session->userdata('page');
				$this->session->unset_userdata('page');
			} else {
				$page = '';
			}
			$this->input->post('btnAction') === 'save_exit' ? redirect('admin/users/index/'.$page.'?'.$_SERVER['QUERY_STRING']) : redirect('admin/users/edit/'.$id.'?'.$_SERVER['QUERY_STRING']);
		}

		// Loop through each validation rule
		foreach ($this->validation_rules as $rule)
		{
			if ($this->input->post($rule['field']) !== null)
			{
				$member->{$rule['field']} = set_value($rule['field']);
			}
		}

		// Get organizations values
		$root_units = $this->units_m->get_units_by_level(0);
		$units = $this->organization->build_serial_tree(array(), $root_units);

		$memberships = $this->organization->get_membership_by_user($id);

		if (! group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			foreach ($units as $key => $value) {
				$unit_list[$value['id']] = $value;
			}

			$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);

			foreach ($memberships_current_user['entries'] as $value) {
				$memberships_unit_list[$value['membership_unit']['id']] = $value['membership_unit']['id'];
			}

			if (isset($memberships_unit_list)) {
				$units = array_intersect_key($unit_list, $memberships_unit_list);

				sort($units);
			}
		}
		$member->old_username = $old_username;
		$title = sprintf(lang('user:edit_title'), $old_username);
		if($forced=='forced') {
			$this->template->set('forced','forced');
			$title = 'Lengkapi data profil pengguna';
		}
		// get user profile
		$user_profile = $this->ion_auth_model->profile($member->email);
		$data['fields'] = (array) $user_profile;
		$this->template
			->title($this->module_details['name'], $title)
			->set('display_name', $member->display_name)
			->set('member', $member)
			->set('units', $units)
			->set('memberships', $memberships)
			->set('cra_users', $cra_users)
			->set('id_crm_group', $id_crm_group)
			->set('id_cra_group', $id_cra_group)
			->set('mode', 'edit')
			->build('admin/form', $data);
	}

	/**
	 * Show a user preview
	 *
	 * @param	int $id The ID of the user.
	 */
	public function preview($id = 0)
	{
		$user = $this->ion_auth->get_user($id);

		$this->template
			->set_layout('modal', 'admin')
			->set('user', $user)
			->build('admin/preview');
	}

	/**
	 * Activate users
	 *
	 * Grabs the ids from the POST data (key: action_to).
	 */
	public function activate()
	{
		// Activate multiple
		if ( ! ($ids = $this->input->post('action_to')))
		{
			$this->session->set_flashdata('error', lang('user:activate_error'));
			redirect('admin/users');
		}

		$activated = 0;
		$to_activate = 0;
		foreach ($ids as $id)
		{
			if ($this->ion_auth->activate($id))
			{
				$activated++;
			}
			$to_activate++;
		}
		$this->session->set_flashdata('success', sprintf(lang('user:activate_success'), $activated, $to_activate));

		if ($this->session->userdata('page')) {
			$page = $this->session->userdata('page');
			$this->session->unset_userdata('page');
		} else {
			$page = '';
		}
		redirect('admin/users/index/'.$page.'?'.$_SERVER['QUERY_STRING']);
	}

	/**
	 * Delete an existing user
	 *
	 * @param int $id The ID of the user to delete
	 */
	public function delete($id = 0)
	{
		// check permission
		if(!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			
			$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);
			if (count($memberships_current_user['entries']) < 1) {
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}

			$unit_current_user = $memberships_current_user['entries'];

			foreach ($unit_current_user as $key => $value) {
				$unit_lists[] = $value['membership_unit']['id'];
			}

			$memberships_user = $this->organization->get_membership_by_user($id);
			
			if (count($memberships_user['entries']) < 1) {
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}

			$unit_user = $memberships_user['entries'];

			foreach ($unit_user as $key => $value) {
				$unit_user_lists[] = $value['membership_unit']['id'];
			}

			if (in_array($unit_user_lists, $unit_lists)) {
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		role_or_die('users', 'delete_users');
		
		if (PYRO_DEMO)
		{
			$this->session->set_flashdata('notice', lang('global:demo_restrictions'));
			redirect('admin/users');
		}

		$ids = ($id > 0) ? array($id) : $this->input->post('action_to');

		if ( ! empty($ids))
		{
			$deleted = 0;
			$to_delete = 0;
			$deleted_ids = array();
			foreach ($ids as $id)
			{
				// Make sure the admin is not trying to delete themself
				if ($this->ion_auth->get_user()->id == $id)
				{
					$this->session->set_flashdata('notice', lang('user:delete_self_error'));
					continue;
				}

				// Make sure the user is not linked to personel
				$personel = $this->personel_m->get_personel_by_account($id);
				if($personel) 
				{
					$this->session->set_flashdata('notice', lang('user:delete_linked_personel'));
					continue;
				}

				// Make sure the user is not linked to membership
				$memberships_user = $this->organization->get_membership_by_user($id);
			
				if (count($memberships_user['entries']) > 0)
				{
					$this->session->set_flashdata('notice', lang('user:delete_error_membership'));
					continue;
				}

				if ($this->ion_auth->delete_user($id))
				{
					$deleted_ids[] = $id;
					$deleted++;
				}
				$to_delete++;
			}

			if ($to_delete > 0)
			{
				// Fire an event. One or more users have been deleted. 
				Events::trigger('user_deleted', $deleted_ids);

				$this->session->set_flashdata('success', sprintf(lang('user:mass_delete_success'), $deleted, $to_delete));
			}
		}
		// The array of id's to delete is empty
		else
		{
			$this->session->set_flashdata('error', lang('user:mass_delete_error'));
		}

		if ($this->session->userdata('page')) {
			$page = $this->session->userdata('page');
			$this->session->unset_userdata('page');
		} else {
			$page = '';
		}
		redirect('admin/users/index/'.$page.'?'.$_SERVER['QUERY_STRING']);
	}

	/**
	 * Username check
	 *
	 * @author Ben Edmunds
	 *
	 * @param string $username The username.
	 *
	 * @return bool
	 */
	public function _username_check()
	{
		if ($this->ion_auth->username_check($this->input->post('username')))
		{
			$this->form_validation->set_message('_username_check', lang('user:error_username'));
			return false;
		}
		return true;
	}

	/**
	 * Email check
	 *
	 * @author Ben Edmunds
	 *
	 * @param string $email The email.
	 *
	 * @return bool
	 */
	public function _email_check()
	{
		if ($this->ion_auth->email_check($this->input->post('email')))
		{
			$this->form_validation->set_message('_email_check', lang('user:error_email'));
			return false;
		}

		return true;
	}

	public function _no_hp_check()
	{
    $this->db->where('no_hp', $this->input->post('no_hp'));
    $query = $this->db->get('default_profiles');
    if(count($query->result_array()) > 0){
    	$this->form_validation->set_message('_no_hp_check', lang('user:error_no_hp'));
			return false;
    }
	}

	/**
	 * Check that a proper group has been selected
	 *
	 * @author Stephen Cozart
	 *
	 * @param int $group
	 *
	 * @return bool
	 */
	public function _group_check($group)
	{
		if ( ! $this->group_m->get($group))
		{
			$this->form_validation->set_message('_group_check', lang('regex_match'));
			return false;
		}
		return true;
	}

	function import_user()
	{
		// Check Permission to create and import user
		if (!group_has_role('users', 'create_users') OR !group_has_role('users', 'import_users')) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin/users');
		}


		// Get Groups
		if (!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			$memberships = $this->organization->get_membership_by_user($this->current_user->id);
			$data['membership_units'] = $memberships['entries'];

			if(count($memberships['entries'])>0) {
				foreach ($memberships['entries'] as $key => $value) {
					$unit_lists[] = $value['membership_unit']['id'];
				}

				$units = implode(',', $unit_lists);

				$res_available_groups = $this->group_m->get_available_groups_by_units($unit_lists);
				$available_groups = array();
				foreach ($res_available_groups as $value) {
					$available_groups = array_merge($available_groups, json_decode($value['available_groups']));
				}
				$available_groups = implode(",", $available_groups);

				$group = $this->group_m->where_not_in('name', 'admin')->get_all(array('available_groups' => $available_groups));
			} else {
				$group = $this->group_m->where_not_in('name', 'admin')->get_all();
			}
		} else {
			$group = $this->group_m->where_not_in('name', 'admin')->get_all();
		}
	
		$row_skipped = 0;
		$row_inserted = 0;
		$row_error = 0;
		$row = 1;
		$err_message = '';

		if($this->input->post()!=null){

			$this->form_validation->set_rules('group_name', 'Nama Grup', 'callback_validate_group');
			
			if ($this->form_validation->run() !== false){

				$name = $_FILES['file']['name'];

				// group id
		        if($this->input->post('group_name')){
		        	$group_name = $this->input->post('group_name');
		        	$group_obj = $this->group_m->get_group_by_slug($group_name);
		        	if($group_obj != NULL){
		        		$group_id = $group_obj->id;
		        	}else{
		        		$group_id = NULL;
		        	}
		        }else{
					$group_id = NULL;
		        }
				
				// file name is not NULL
				if($name!=null){
					if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
						
						$skip_status = $this->input->post('header');
						$temp = array();
						$error = array();

					    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

					    	// Variable initialization for field validation
				        	$is_current_row_valid = TRUE;
				        	$error_msg_arr = array();
					    	$skip_current_row = FALSE;

					    	// Check if current row has enough columns/fields
					    	if(($group_name == 'cra' AND count($data) < 5)
					    		OR ($group_name == 'crm' AND count($data) < 6)
					    	){
					    		$error[$row_error] = lang('user:row').' '.$row.': '.lang('user:row_format_incorrect');
					        	$row_error++;

					    		continue;
					    	}

					        // account fields
					        $values['email'] = trim($data[0]);
					        $values['salt'] = $this->ion_auth_model->salt(trim($data[1]));
					        $raw_password = trim($data[1]);
					        $values['password'] = $this->ion_auth_model->hash_password($raw_password, $values['salt']);
					        $values['username'] = trim($data[2]);
					        $values['group_id'] = $group_id;
					        $values['active'] = $this->input->post('active');
					        $values['created_on'] = time();
					        $values['last_login'] = 0;

					        // profile fields
					        // @custom add custom profile data here
					        $values['display_name'] = trim($data[3]);
					        $values['no_hp'] = trim($data[4]);
					        
					        if($group_name == 'crm'){
					        	$values['id_cra'] = NULL;
					        	$cra_display_name = trim($data[5]);
					        	$cra_profile_obj = $this->profile_m->get_profile(array(
					        		'display_name' => $cra_display_name
					        	));
					        	if($cra_profile_obj != NULL){
					        		$values['id_cra'] = $cra_profile_obj->user_id;
					        	}
					        }

					        // skip header
					        if($skip_status == 'yes'){
					        	$skip_status = 'no';
					        }else{
					        	// Email: check if NULL
								$is_email_null = ($values['email'] == NULL OR $values['email'] == '');
								if($is_email_null){
									$error_msg_arr[] = lang('user:email_empty');
						        	$is_current_row_valid = FALSE;
								}

								// Email: check if already used
								$is_email_exist = $this->ion_auth_model->email_check($values['email']);
						        if($is_email_exist){
						        	$skip_current_row = TRUE;
					        	}

					        	// Email: check if exceed max length
						        if(strlen($values['email']) > 60){
						        	$error_msg_arr[] = sprintf(lang('user:email_exceed_max_length'), 60);
						        	$is_current_row_valid = FALSE;
					        	}

					        	// Username: check if NULL
								$is_username_null = ($values['username'] == NULL OR $values['username'] == '');
								if($is_username_null){
									$error_msg_arr[] = lang('user:username_empty');
						        	$is_current_row_valid = FALSE;
								}

								// Username: check if exceed max length
						        if(strlen($values['username']) > 20){
						        	$error_msg_arr[] = sprintf(lang('user:username_exceed_max_length'), 20);
						        	$is_current_row_valid = FALSE;
					        	}

								// Username: check if already used
								$is_username_exist = $this->ion_auth_model->username_check($values['username']);
						        if($is_username_exist){
						        	$skip_current_row = TRUE;
						        }

						        // Password: check if NULL
								if($raw_password == NULL OR $raw_password == ''){
									$error_msg_arr[] = lang('user:password_empty');
						        	$is_current_row_valid = FALSE;
								}

								// Display Name: check if NULL
								if($values['display_name'] == NULL OR $values['display_name'] == ''){
									$error_msg_arr[] = lang('user:display_name_empty');
						        	$is_current_row_valid = FALSE;
								}

								// Phone number: correct format or just empty
								if($values['no_hp'] != NULL 
									AND $values['no_hp'] != '' 
									AND !preg_match("/^[0-9\-\(\)\/\+\s]*$/", $values['no_hp'])){

									$error_msg_arr[] = lang('user:no_hp_format_incorrect');
						        	$is_current_row_valid = FALSE;
								}

								// ID CRA: check if not exist (NULL)
								if($group_name == 'crm'){
									if($values['id_cra'] == NULL OR $values['id_cra'] == ''){
										$error_msg_arr[] = lang('user:id_cra_not_found');
							        	$is_current_row_valid = FALSE;
									}
								}

						        // Evaluation of all field validation
						        if($is_current_row_valid AND !$skip_current_row){
						        	$temp[$row_inserted] = $values;
						        	$row_inserted++;
						        }else{
						        	if($skip_current_row){
						        		$row_skipped++;
						        	}elseif(!$is_current_row_valid){
						        		$error[$row_error] = lang('user:row').' '.$row.': '.implode("; ", $error_msg_arr);
						        		$row_error++;
						        	}
						        }
					        }
					        $row++;
					    }
		            	
		            	if($row_error != 0){
		            		for ($i = 0; $i < sizeof($error); $i++) {
			            		$err_message .= $error[$i]."<br>"; 
			            	}
			            	$err_message .= lang('user:err_import');
			            	$this->session->set_flashdata('error', $err_message);
					    	fclose($handle);
					    	
					    	redirect('admin/users/import_user');
		            	}else{
		            		$this->db->trans_start();

		            		for ($i=0; $i < sizeof($temp); $i++) {
		            			$retval = $this->ion_auth_model->import_insert($temp[$i]);
		            			if($retval == TRUE){
		            				$user_id = $this->db->insert_id();
		            				$this->ion_auth_model->import_profile($user_id, $temp[$i]);

		            				// set memberships of user which imported in the same unit with current user
		            				// if current user only has permission to manage his own units users
		            				
			            			if (!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			            				$membership_unit = $this->organization->get_membership_by_user($this->current_user->id);
			            				if (count($membership_unit['entries'])>1) {
			            					$data_membership = array('membership_unit' => $this->input->post('membership_unit'), 'membership_user'=>$user_id);
			            				} else if (count($membership_unit['entries'])==1){
				            				$data_membership = array('membership_unit' => $membership_unit['entries'][0]['membership_unit']['id'], 'membership_user' => $user_id);
				            			}

				            			if(isset($data_membership)) {
				            				$this->user_m->set_membership_user($data_membership);
				            			}
			            			}
		            			}
		            		}

		            		$this->db->trans_complete();
		            		$result = $this->db->trans_status();
		            		if($result){
			            		$this->session->set_flashdata('success', $row_inserted.' '.sprintf(lang('user:scc_import'), $row_inserted, $row_skipped, $row_error));
			            	}
			            	fclose($handle);
			            	
			            	redirect('admin/users/index');
		            	}
					}
				}

			}else{
				$this->session->set_flashdata('error', lang('user:group_empty'));
		    	redirect('admin/users/import_user');
			}
		}
			
		if(isset($data['membership_units'])) {
			$data['units'] = $data['membership_units'];
		} else {
			$data['units'] = array();
		}

		$this->template
			->title($this->module_details['name'], lang('user:add_title'))
			->set('group',$group)
			->build('admin/import_user', $data);
	}

	function validate_group()
	{
		// user group
        if($this->input->post('group_name')){
        	$group_name = $this->input->post('group_name');
        	$group_obj = $this->group_m->get_group_by_slug($group_name);
        	if($group_obj != NULL){
        		$group_id = $group_obj->id;
        	}else{
        		$group_id = NULL;
        	}
        }else{
			$group_id = NULL;
        }

        if($group_id != NULL){
        	return TRUE;
        }else{
        	return FALSE;
        }
	}

	public function download_contoh($group_name = NULL){
		role_or_die('users', 'create_users');

		$this->load->helper('download');

		if($group_name == NULL){
			$file_name = 'cra_user_import_example.csv';
		}elseif($group_name == 'cra'){ // group = CRA
			$file_name = 'cra_user_import_example.csv';
		}elseif($group_name == 'crm'){ // group = CRM
			$file_name = 'crm_user_import_example.csv';
		}

		force_download($file_name, file_get_contents(base_url().'uploads/'.$file_name));
		
		redirect('admin/user/import_user');
	}

	public function download_users()
	{
		$base_where = array('active' => 0);

		// ---------------------------
		// User Filters
		// ---------------------------

		// Determine active param
		$base_where['active'] = $this->input->get('f_module') ? (int)$this->input->get('f_active') : $base_where['active'];

		// Determine organization unit param
		$data['membership_units'] = array();
		if(!group_has_role('users', 'manage_users') AND group_has_role('users', 'manage_own_unit_users')) {
			$memberships = $this->organization->get_membership_by_user($this->current_user->id);
			$data['membership_units'] = $memberships['entries'];

			foreach ($memberships['entries'] as $key => $value) {
				$data['unit_lists'][] = $value['membership_unit']['id'];
			}

			if (!isset($_GET['f_unit']) AND isset($data['unit_lists'])) {
				$_GET['f_unit'] = implode(',', $data['unit_lists']);
			}
		}

		$base_where = $this->input->get('f_unit') ? $base_where + array('unit_id' => $this->input->get('f_unit')) : $base_where;

		// Determine group param
		$base_where = $this->input->get('f_group') ? $base_where + array('group_id' => (int)$this->input->get('f_group')) : $base_where;

		// Keyphrase param
		$base_where = $this->input->get('f_keywords') ? $base_where + array('name' => $this->input->get('f_keywords')) : $base_where;

		//Skip admin
		$skip_admin = ( $this->current_user->group != 'admin' ) ? 'admin' : '';

		// Using this data, get the relevant results
		$this->db->order_by('active', 'desc');
		$this->db->order_by('created_on', 'desc');

		if($skip_admin!='') {
			$this->db->where_not_in('groups.name', $skip_admin);
		}

		$users = $this->user_m->get_many_by($base_where);
		$user2 = array();
		$id_user = array();
		
		foreach ($users as $value) {
			$user2[$value->id] = (array)$value;

			$id_user[] = $value->id;
		}

		$memberships_list = $this->memberships_m->get_memberships_by_multi_user($id_user);
		$unit_list = array();

		foreach ($memberships_list as $value) {
			$unit_list[$value['membership_user']][] = $value;
		}

		foreach ($user2 as $key => $value) {
			if(array_key_exists($key, $unit_list)) {
				$user2[$key]['units'] = $unit_list[$key];
			} else {
				$user2[$key]['units'] = array();
			}

			$user2[$key] = (object)$user2[$key];
		}
		sort($user2);
		$complete_data = $user2;
		
		foreach ($complete_data as $key => $value) {
			if(is_array($value)) {
				$value = (object)$value;
				if(!isset($value->group_name)) {
					$value->group_name = $value->group_description;
				}
			}
			$data[$key]['email'] = $value->email;
			$data[$key]['nama lengkap'] = $value->display_name;
			$data[$key]['group pengguna'] = $value->group_name;
			$data[$key]['tanggal daftar'] = date('Y-m-d',$value->created_on);
		}
		
		if(!isset($data)) {
			$data[] = array('Tidak ada pengguna baru pada '.date('d-m-Y',strtotime($date)));
		}
		if(array_key_exists("membership_units", $data)) {
			unset($data['membership_units']);
		}
		download_user($data);
	}
}