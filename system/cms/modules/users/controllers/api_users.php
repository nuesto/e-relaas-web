<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User controller for the users module (frontend)
 *
 * @author		 Phil Sturgeon
 * @author		PyroCMS Dev Team
 * @package		PyroCMS\Core\Modules\Users\Controllers
 */
class Api_users extends API2_Controller
{
	/**
	 * Constructor method
	 *
	 * @return \Users
	 */
	public function __construct()
	{
		parent::__construct();

		// Load the required classes
		$this->load->model(array('user_m','profile_m'));
		$this->load->helper('user');
		$this->lang->load('user');
		$this->load->library('form_validation');
	}

    // public function _auth_override_check(){
    //   parent::_auth_override_check();
    // 	return true;
    // }

	/**
	 * Show the current user's profile
	 */
	public function index_get()
	{
		$username = $this->input->server('PHP_AUTH_USER');
		$user_data = $this->user_m->get(array('username'=>$username));
		$this->response(array('data' => $user_data, 'status'=>'200'),200);
		exit;
	}

	/**
	 * Show the current user's profile
	 */
	public function token_get()
	{
		$username = $this->input->server('PHP_AUTH_USER');
		$user = $this->db->where('username', $username)
				->get('users')
            	->row();

		$token = $this->db->where('id', $user->id)
				//->where('created_on', date())
				->get('api_keys')
  				->row();

	    if(count($token) < 1){
	    	$date = new DateTime();
	    	$token = 'Basic '.md5($user->api_key.rand(0, 99999) );
	    	$api_key = array(
	    		'date_created' => $date->getTimestamp(),
	    		'key' => $token,
	    		'ignore_limits' => 0,
	    		'level' => 0,
	    		'id' => $user->id,
	    	);
	    	$this->db->insert('api_keys', $api_key);
	    	$this->response(array('token'=>$token, 'status'=>'200'),200);
	    }
	    exit;
	}

	public function login()
	{
	    header('Access-Control-Allow-Headers: Content-Type');
	    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	    if(!$this->input->get('testing')){
	    	$params = json_decode(file_get_contents('php://input'),true);
	    	$_POST = $params;
	    }


		if(!$this->input->post('email')){
			$_POST['email'] = '';
		}
		if(!$this->input->post('password')){
			$_POST['password'] = '';
		}
		
		$user = (object) array(
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password')
		);

		$validation = array(
			array(
				'field' => 'email',
				'label' => 'Username',
				'rules' => 'required|trim|callback__check_login'
			),
			array(
				'field' => 'password',
				'label' => lang('global:password'),
				'rules' => 'required|min_length['.$this->config->item('min_password_length', 'ion_auth').']|max_length['.$this->config->item('max_password_length', 'ion_auth').']'
			),
		);

		// Call validation and set rules
		$this->load->library('form_validation');
		$this->form_validation->set_rules($validation);

  		$result = array('status'=>'error','messages'=>array());
  		$return = false;
		// If the validation worked, or the user is already logged in
		if ($this->form_validation->run() or $this->ion_auth->logged_in())
		{
			// if they were trying to go someplace besides the
			// dashboard we'll have stored it in the session
	      	$user = $this->db->where('username', $user->email)
	      			->or_where('email',$user->email)
					->get('users')
	            	->row();

	      	if(count($user) > 0){
	      		$user_profile = $this->ion_auth_model->profile($user->email);
				// $path = gravatar($user->email, 200, 'g',true);
				$path = 'http:'.base_url().'files/thumb/'.$user_profile->picture_id.'/36/36/fit';
				$type = pathinfo($path, PATHINFO_EXTENSION);
				// die();
				if($user_profile->picture_id != ""){
					$data = file_get_contents($path);
					$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
					$user->imgbase64 = $base64;
				}
	      		$return = true;
	      	}

	        if($return){
	    		$result = array('status'=>'success','messages'=>lang('user:logged_in'),'data'=>(array)$user);
	      	}else{
	    		$result = array('status'=>'error','messages'=>lang('user:logged_failed'),'data'=>(array)$user);
	      	}
		} else {
      		$validation_errors = explode("<p>", validation_errors());
			foreach ($validation_errors as $key => $value) {
				$value = trim($value);
				if(empty($value)) {
					unset($validation_errors[$key]);
				} else {
					$validation_errors[$key] = $value;
				}
			}
			$result = array('status'=>'error','messages'=>$validation_errors);
    	}

    	_output($result);
	}

	public function get_profile($id){

		$this->authorize_api_access();
		$user = $this->ion_auth->get_user($id);
		$user_profile = $this->ion_auth_model->profile($user->email);
		// $path = gravatar($user->email, 200, 'g',true);
		$path = 'http:'.base_url().'files/thumb/'.$user_profile->picture_id.'/36/36/fit';
		$type = pathinfo($path, PATHINFO_EXTENSION);
		// die();
		if($user_profile->picture_id != ""){
			$data = file_get_contents($path);
			$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
			$user->imgbase64 = $base64;
		}
		// dump($path, $data);
		// die();
		$result = $user;
		$status = '200';

		if(empty($result)) {
			$result = array('status'=>'error', 'message'=>'User tidak ditemukan');
	    	$status = '200';
		}

		_output($result,$status);
	}

	/**
	 * Logout
	 */
	public function logout()
	{
		$this->load->language('users/user');
		$this->ion_auth->logout();
		echo json_encode(array('status'=>'success','messages'=>lang('user:logged_out')));
	}

	/**
	 * Callback From: login()
	 *
	 * @param string $email The Email address to validate
	 *
	 * @return bool
	 */
	public function _check_login($email)
	{ 
		if ($this->ion_auth->login($email, $this->input->post('password'), (bool)$this->input->post('remember')))
		{
			Events::trigger('post_admin_login');

			return true;
		}

		Events::trigger('login_failed', $email);
		error_log('Login failed for user '.$email);

		$this->form_validation->set_message('_check_login', $this->ion_auth->errors());
		return false;
	}


	public function reset_pass($code = null){
		header('Access-Control-Allow-Headers: Content-Type');
	    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	    if(!$this->input->get('testing')){
	    	$params = json_decode(file_get_contents('php://input'),true);
	    	$_POST = $params;
	    }


  		$result = array('status'=>'error','messages'=>array());

		if(!$this->input->post('email')){
			$_POST['email'] = '';
		}

		if ($this->input->post('email'))
		{

			$email = (string) $this->input->post('email');
			if ( ! ($user_meta = $this->ion_auth->get_user_by_email($email)))
			{
				$user_meta = $this->ion_auth->get_user_by_username($email);
			}

			// have we found a user?
			if ($user_meta)
			{
				$new_password = $this->ion_auth->forgotten_password($user_meta->email);

				if ($new_password)
				{

					$result = array('status'=>'success','message'=>lang('forgot_password_successful'));
				}
				else
				{
					$result = array('status'=>'error','message'=>$this->ion_auth->errors());
				}
			}
			else
			{
				$result = array('status'=>'error','message'=>lang('user:forgot_incorrect'));
			}
		}else{
			$result = array('status'=>'error','message'=>'Email tidak boleh kosong');
		}

		// code is supplied in url so lets try to reset the password
		if ($code)
		{
			// verify reset_code against code stored in db
			$reset = $this->ion_auth->forgotten_password_complete($code);

			// did the password reset?
			if ($reset)
			{
				$result = array('status'=>'success','message'=>lang('user:new_pass_has_sended'));
			}
			else
			{
				// nope, set error message
				$result = array('status'=>'error','message'=>$this->ion_auth->errors());
			}
		}

		$status = '200';
		_output($result,$status);
	}

}