<div class="page-header">
	<h1><?php echo lang('global:plugins');?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<h4 class="header smaller lighter blue col-sm-12"><?php echo lang('addons:plugins:add_on_plugins');?></h4>
<?php echo $this->load->view('admin/plugins/_table', array('plugins' => $plugins), true) ?>

<h4 class="header smaller lighter blue col-sm-12"><?php echo lang('addons:plugins:core_plugins');?></h4>
<?php echo $this->load->view('admin/plugins/_table', array('plugins' => $core_plugins), true) ?>

<section id="plugin-docs" style="display:none">
	<?php echo $this->load->view('admin/plugins/_docs', array('plugins' => array($plugins, $core_plugins)), true) ?>
</section>