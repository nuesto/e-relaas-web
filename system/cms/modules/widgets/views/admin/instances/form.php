<?php echo form_open(uri_string(), 'class="widget-form"') ?>

	<div class="page-header">
		<h1><?php echo $widget->title ?></h1>
	</div>

	<?php
		echo form_hidden('widget_id', $widget->id);
		echo isset($widget->instance_id) ? form_hidden('widget_instance_id', $widget->instance_id) : null;
		echo isset($error) && $error ? $error : null;
	?>

	<div class="form-group">
    <label>
    	<?php echo lang('widgets:instance_title') ?>
    </label>
      <?php echo form_input('title', set_value('title', isset($widget->instance_title) ? $widget->instance_title : ''), 'class="form-control"') ?>
  </div>

  <?php echo $form ? $form : null ?>

  <?php if (isset($widget_areas)): ?>
	<div class="form-group">
    <label>
    	<?php echo lang('widgets:widget_area') ?>
    </label>
      <?php echo form_dropdown('widget_area_id', $widget_areas, $widget->widget_area_id, 'class="form-control"') ?>
  </div>
	<?php
		else:
			echo form_hidden('widget_area_id', $area_id);
		endif;
	?>

	<div class="form-group">
      <label class="checkbox-inline">
			  <?php
			  	echo form_checkbox('show_title', true, isset($widget->options['show_title']) ? $widget->options['show_title'] : false).' '.lang('widgets:show_title');
			  ?>
			</label>
  </div>

	<div class="form-group">
      <label class="checkbox-inline">
			  <?php
			  	echo form_checkbox('has_container', true, isset($widget->options['has_container']) ? $widget->options['has_container'] : false).' '.lang('widgets:has_container');
			  ?>
			</label>
  </div>

	<div class="clearfix form-actions">
		<div class="col-md-offset-3 col-md-9">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
		</div>
	</div>
<?php echo form_close() ?>