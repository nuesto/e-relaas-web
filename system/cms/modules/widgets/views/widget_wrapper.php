<?php 
$view_uri = $widget->options['view_uri'];
$query_string = $this->input->server('QUERY_STRING');
if(strpos($view_uri, '?') > 0){
	$data_source_url = $view_uri.'&'.$query_string;
}else{
	$data_source_url = $view_uri.'?'.$query_string;
}
?>

<?php if (isset($widget->options['has_container'])): ?>
<div class="widget <?php if (isset($widget->options['color_scheme'])) echo 'widget-'.$widget->options['color_scheme'] ?> widget-<?php echo $widget->slug ?>" data-source="<?php echo site_url($data_source_url); ?>">
	<?php if ($widget->options['show_title']): ?>
	<div class="widget-head clearfix">
	  <h4><?php echo $widget->instance_title ?></h4>
	  <div class="widget-tools pull-right">
			<a href="#" class="widget-refresh"><i class="fa fa-refresh"></i></a>
			<a href="#" class="widget-minimize"><i class="fa fa-chevron-up"></i></a>
	  </div>
	</div>
	<?php
		endif;
		echo $widget->body;
	?>
</div>
<?php
	else:
		echo $widget->body;
	endif;
?>
