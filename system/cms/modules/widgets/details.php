<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Widgets Module
 *
 * @author PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Widgets
 */
class Module_Widgets extends Module {

	public $version = '1.3.2';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'Widgets',
				'br' => 'Widgets',
				'pt' => 'Widgets',
				'cs' => 'Widgety',
				'da' => 'Widgets',
				'de' => 'Widgets',
				'el' => 'Widgets',
				'es' => 'Widgets',
                            'fa' => 'ویجت ها',
				'fi' => 'Vimpaimet',
				'fr' => 'Widgets',
				'id' => 'Widget',
				'it' => 'Widgets',
				'lt' => 'Papildiniai',
				'nl' => 'Widgets',
				'ru' => 'Виджеты',
				'sl' => 'Vtičniki',
				'tw' => '小組件',
				'cn' => '小组件',
				'hu' => 'Widget-ek',
				'th' => 'วิดเจ็ต',
				'se' => 'Widgetar',
				'ar' => 'الودجتس',
			),
			'description' => array(
				'en' => 'Manage small sections of self-contained logic in blocks or "Widgets".',
				'ar' => 'إدارة أقسام صغيرة من البرمجيات في مساحات الموقع أو ما يُسمّى بالـ"ودجتس".',
				'br' => 'Gerenciar pequenas seções de conteúdos em bloco conhecidos como "Widgets".',
				'pt' => 'Gerir pequenas secções de conteúdos em bloco conhecidos como "Widgets".',
				'cs' => 'Spravujte malé funkční části webu neboli "Widgety".',
				'da' => 'Håndter små sektioner af selv-opretholdt logik i blokke eller "Widgets".',
				'de' => 'Verwaltet kleine, eigentständige Bereiche, genannt "Widgets".',
				'el' => 'Διαχείριση μικρών τμημάτων αυτόνομης προγραμματιστικής λογικής σε πεδία ή "Widgets".',
				'es' => 'Manejar pequeñas secciones de lógica autocontenida en bloques o "Widgets"',
                            'fa' => 'مدیریت قسمت های کوچکی از سایت به طور مستقل',
				'fi' => 'Hallitse pieniä osioita, jotka sisältävät erillisiä lohkoja tai "Vimpaimia".',
				'fr' => 'Gérer des mini application ou "Widgets".',
				'id' => 'Mengatur bagian-bagian kecil dari blok-blok yang memuat sesuatu atau dikenal dengan istilah "Widget".',
				'it' => 'Gestisci piccole sezioni di logica a se stante in blocchi o "Widgets".',
				'lt' => 'Nedidelių, savarankiškų blokų valdymas.',
				'nl' => 'Beheer kleine onderdelen die zelfstandige logica bevatten, ofwel "Widgets".',
				'ru' => 'Управление небольшими, самостоятельными блоками.',
				'sl' => 'Urejanje manjših delov blokov strani ti. Vtičniki (Widgets)',
				'tw' => '可將小段的程式碼透過小組件來管理。即所謂 "Widgets"，或稱為小工具、部件。',
				'cn' => '可将小段的程式码透过小组件来管理。即所谓 "Widgets"，或称为小工具、部件。',
				'hu' => 'Önálló kis logikai tömbök vagy widget-ek kezelése.',
				'th' => 'จัดการส่วนเล็ก ๆ ในรูปแบบของตัวเองในบล็อกหรือวิดเจ็ต',
				'se' => 'Hantera små sektioner med egen logik och innehåll på olika delar av webbplatsen.',
			),
			'frontend' 	=> false,
			'backend'  	=> true,
			'menu'	  	=> 'content',
			'skip_xss'	=> true,

			'sections' => array(
			    'instances' => array(
				    'name' => 'widgets:instances',
				    'uri' => 'admin/widgets/index',
				),
				'areas' => array(
				    'name' => 'widgets:areas',
				    'uri' => 'admin/widgets/areas',
				    'shortcuts' => array(
						array(
						    'name' => 'widgets:add_area',
						    'uri' => 'admin/widgets/areas/create',
						),
				    ),
			    ),
		    ),
		);
	}

	public function install()
	{
		$this->dbforge->drop_table('widget_areas');
		$this->dbforge->drop_table('widget_instances');
		$this->dbforge->drop_table('widgets');

		$tables = array(
			'widget_areas' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true,),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true,),
				'title' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true,),
			),

			'widget_instances' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true,),
				'title' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true,),
				'widget_id' => array('type' => 'INT', 'constraint' => 11, 'null' => true,),
				'widget_area_id' => array('type' => 'INT', 'constraint' => 11, 'null' => true,),
				'options' => array('type' => 'TEXT'),
				'order' => array('type' => 'INT', 'constraint' => 10, 'default' => 0,),
				'created_on' => array('type' => 'INT', 'constraint' => 11, 'default' => 0,),
				'updated_on' => array('type' => 'INT', 'constraint' => 11, 'default' => 0,),
			),

			'widgets' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true,),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 100, 'default' => '',),
				'title' => array('type' => 'TEXT', 'constraint' => 100,),
				'description' => array('type' => 'TEXT', 'constraint' => 100,),
				'author' => array('type' => 'VARCHAR', 'constraint' => 100, 'default' => '',),
				'website' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => '',),
				'version' => array('type' => 'VARCHAR', 'constraint' => 20, 'default' => 0,),
				'enabled' => array('type' => 'INT', 'constraint' => 1, 'default' => 1,),
				'order' => array('type' => 'INT', 'constraint' => 10, 'default' => 0,),
				'updated_on' => array('type' => 'INT', 'constraint' => 11, 'default' => 0,),
			),
		);

		if ( ! $this->install_tables($tables))
		{
			return false;
		}

		// Add the default data
		$default_widget_areas = array(
			'title' => 'Sidebar',
			'slug' 	=> 'sidebar',
		);

		if ( ! $this->db->insert('widget_areas', $default_widget_areas))
		{
			return false;
		}

		return true;
	}

	public function uninstall()
	{
		// This is a core module, lets keep it around.
		return false;
	}

	public function upgrade($old_version)
	{
		switch($old_version){
			case "1.2.0" :
				$new_field = array('module'=>array('type'=>'VARCHAR(100)', 'null'=>true));
				$this->dbforge->add_column('widgets', $new_field, 'title');
				break;
			case '1.2.1':
				// convert all tables to InnoDB engine
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('widgets')." ENGINE=InnoDB");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('widget_areas')." ENGINE=InnoDB");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('widget_instances')." ENGINE=InnoDB");
				break;
			case '1.3.0':
				// ------------------------------------------
				// CREATE FK FOR `widget_instances`.`widget_id`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row

				// parent widget
				$this->db->select('id');
				$existing_ids = $this->db->get('widgets')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('widget_id', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('widget_instances');

				// parent area
				$this->db->select('id');
				$existing_ids = $this->db->get('widget_areas')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('widget_area_id', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('widget_instances');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('widgets')." MODIFY `id` INT(11) UNSIGNED AUTO_INCREMENT");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('widget_areas')." MODIFY `id` INT(11) UNSIGNED AUTO_INCREMENT");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('widget_instances')." MODIFY `widget_id` INT(11) UNSIGNED NULL DEFAULT NULL");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('widget_instances')." MODIFY `widget_area_id` INT(11) UNSIGNED NULL DEFAULT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('widget_instances')." ADD FOREIGN KEY(`widget_id`) REFERENCES ".$this->db->dbprefix('widgets')."(`id`)");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('widget_instances')." ADD FOREIGN KEY(`widget_area_id`) REFERENCES ".$this->db->dbprefix('widget_areas')."(`id`)");

				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '1.3.1';
				break;

			case '1.3.1':
				// load library we need
				$this->load->library('widgets/widgets');

				// alter foreign key restriction
				$this->db->query("ALTER TABLE `default_widget_instances` DROP FOREIGN KEY `default_widget_instances_ibfk_1`, DROP FOREIGN KEY `default_widget_instances_ibfk_2`;");
				$this->db->query("ALTER TABLE `default_widget_instances` ADD CONSTRAINT `default_widget_instances_ibfk_1` FOREIGN KEY (`widget_id`) REFERENCES `default_widgets` (`id`) ON UPDATE CASCADE ON DELETE CASCADE, ADD CONSTRAINT `default_widget_instances_ibfk_2` FOREIGN KEY (`widget_area_id`) REFERENCES `default_widget_areas` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;");

				$this->db->trans_start();

				// delete existing widget that we want to add
				$this->widgets->delete_widget('dashboard');
				$this->widgets->delete_widget('statbox');

				// add Dashboard Widget
				$widget['title'] = 'Dashboard Widget';
				$widget['slug'] = 'dashboard';
				$widget['module'] = 'widgets';
				$widget['description'] = 'Display widget on Dashboard';
				$widget['author'] = 'asatrya';
				$widget['website'] = 'http://nuesto.net';
				$widget['version'] = '1.0';
				$this->widgets->add_widget($widget);

				// add Statbox Widget
				$widget['title'] = 'Statbox Widget';
				$widget['slug'] = 'statbox';
				$widget['module'] = 'widgets';
				$widget['description'] = 'Display Statbox on Dashboard';
				$widget['author'] = 'asatrya';
				$widget['website'] = 'http://nuesto.net';
				$widget['version'] = '1.0';
				$this->widgets->add_widget($widget);

				// create area dashboard stats
				$area_dashboard_stats = array(
					'title'	=> 'Dashboard Stats',
					'slug'	=> 'dashboard-stats'
				);
				$this->widgets->add_area($area_dashboard_stats);

				// create area dashboard left
				$area_dashboard_left = array(
					'title'	=> 'Dashboard Left',
					'slug'	=> 'dashboard-left'
				);
				$this->widgets->add_area($area_dashboard_left);
				// create area dashboard right
				$area_dashboard_right = array(
					'title'	=> 'Dashboard Right',
					'slug'	=> 'dashboard-right'
				);
				$this->widgets->add_area($area_dashboard_right);

				// create area dashboard middle left
				$area_dashboard_middle_left = array(
					'title'	=> 'Dashboard Middle Left',
					'slug'	=> 'dashboard-middle-left'
				);
				$this->widgets->add_area($area_dashboard_middle_left);
				// create area dashboard middle right
				$area_dashboard_middle_right = array(
					'title'	=> 'Dashboard Middle Right',
					'slug'	=> 'dashboard-middle-right'
				);
				$this->widgets->add_area($area_dashboard_middle_right);

				// create area dashboard bottom left
				$area_dashboard_bottom_left = array(
					'title'	=> 'Dashboard Bottom Left',
					'slug'	=> 'dashboard-bottom-left'
				);
				$this->widgets->add_area($area_dashboard_bottom_left);

				// create area dashboard bottom right
				$area_dashboard_bottom_right = array(
					'title'	=> 'Dashboard Bottom Right',
					'slug'	=> 'dashboard-bottom-right'
				);
				$this->widgets->add_area($area_dashboard_bottom_right);
				
				$this->db->trans_complete();

				if ($this->db->trans_status() === FALSE)
				{
					// no version increment
					$this->version = '1.3.1';
				}else{
					// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
					$this->version = '1.3.2';
				}

				break;
		}
		return true;
	}

}
