<?php

$lang['cp:nav_organization'] = 'Organisasi';

/*****************************************************
 * STREAMS: units
 *****************************************************/
$lang['organization:units:singular'] = 'Satuan Organisasi';
$lang['organization:units:plural'] = 'Satuan Organisasi';
$lang['organization:units:no_entry'] = 'Tidak terdapat entri Satuan Organisasi';
$lang['organization:units:new'] = 'Tambah Satuan Organisasi';
$lang['organization:units:edit'] = 'Sunting Satuan Organisasi';
$lang['organization:units:view'] = 'Detil Satuan Organisasi';
$lang['organization:units:submit_success'] = 'Satuan Organisasi berhasil dimasukkan.';
$lang['organization:units:submit_failure'] = 'Gagal memasukkan Satuan Organisasi.';
$lang['organization:units:deleted'] = 'Satuan Organisasi telah dihapus.';
$lang['organization:units:delete_fail_has_child'] = 'Satuan ini tidak dapat dihapus karena memiliki satuan bawahan.';
$lang['organization:units:select'] = '-- Pilih Satuan Organisasi --';

/*****************************************************
 * STREAMS: types
 *****************************************************/
$lang['organization:types:singular'] = 'Tipe';
$lang['organization:types:plural'] = 'Tipe';
$lang['organization:types:no_entry'] = 'Tidak terdapat entri Tipe';
$lang['organization:types:new'] = 'Tambah Tipe';
$lang['organization:types:edit'] = 'Sunting Tipe';
$lang['organization:types:view'] = 'Detil Tipe';
$lang['organization:types:submit_success'] = 'Tipe berhasil dimasukkan.';
$lang['organization:types:submit_failure'] = 'Gagal memasukkan Tipe.';
$lang['organization:types:deleted'] = 'Tipe telah dihapus.';
$lang['organization:types:deleted_fail'] = 'Gagal menghapus Tipe.';

/*****************************************************
 * STREAMS: memberships
 *****************************************************/
$lang['organization:memberships:singular'] = 'Keanggotaan';
$lang['organization:memberships:plural'] = 'Keanggotaan';
$lang['organization:memberships:no_entry'] = 'Tidak terdapat Anggota';
$lang['organization:memberships:new'] = 'Tambah Anggota';
$lang['organization:memberships:edit'] = 'Sunting Keanggotaan';
$lang['organization:memberships:view'] = 'Detil Keanggotaan';
$lang['organization:memberships:submit_success'] = 'Anggota berhasil ditambahkan.';
$lang['organization:memberships:submit_failure'] = 'Gagal memasukkan Anggota.';
$lang['organization:memberships:deleted'] = 'Keanggotaan telah dihapus.';

/*****************************************************
 * STREAMS: titles
 *****************************************************/
$lang['organization:titles:singular'] = 'Jabatan';
$lang['organization:titles:plural'] = 'Jabatan';
$lang['organization:titles:no_entry'] = 'Tidak terdapat entri Jabatan';
$lang['organization:titles:new'] = 'Tambah Jabatan';
$lang['organization:titles:edit'] = 'Sunting Jabatan';
$lang['organization:titles:view'] = 'Detil Jabatan';
$lang['organization:titles:submit_success'] = 'Jabatan berhasil dimasukkan.';
$lang['organization:titles:submit_failure'] = 'Gagal memasukkan Jabatan.';
$lang['organization:titles:deleted'] = 'Jabatan telah dihapus.';


/*****************************************************
 * FIELDS
 *****************************************************/
$lang['organization:unit_name'] = 'Nama Satuan Organisasi';
$lang['organization:unit_slug'] = 'Slug';
$lang['organization:unit_abbrevation'] = 'Singkatan';
$lang['organization:unit_description'] = 'Deskripsi';
$lang['organization:unit_type'] = 'Tipe';
$lang['organization:unit_level'] = 'Level';
$lang['organization:unit_sort_order'] = 'Urutan';
$lang['organization:choose_type'] = 'Pilih Tipe Unit';
$lang['organization:type_name'] = 'Nama Tipe';
$lang['organization:type_slug'] = 'Slug';
$lang['organization:type_level'] = 'Level';
$lang['organization:type_description'] = 'Deskripsi';
$lang['organization:available_groups'] = 'Grup Tersedia';
$lang['organization:unit_parents'] = 'Satuan Induk';
$lang['organization:membership_unit'] = 'Satuan Organisasi';
$lang['organization:membership_user'] = 'Anggota';
$lang['organization:title_name'] = 'Nama Jabatan';
$lang['organization:title_description'] = 'Deskripsi Jabatan';

/*****************************************************
 * DEFAULT FIELDS
 *****************************************************/
$lang['organization:created'] = 'Dibuat';
$lang['organization:updated'] = 'Diperbaharui';
$lang['organization:created_by'] = 'Dibuat oleh';

/*****************************************************
 * STREAMS (GENERAL)
 *****************************************************/
$lang['organization:streams'] = 'Streams';
$lang['organization:view_options'] = 'View Options';
$lang['organization:field_assignments'] = 'Field Assignments';
$lang['organization:new_assignment'] = 'New Field Assignment';
$lang['organization:edit_assignment'] = 'Edit Field Assignment';

/*****************************************************
 * GLOBAL
 *****************************************************/
$lang['organization:back'] = 'Kembali';
$lang['organization:confirm_delete'] = 'Apakah Anda yakin akan menghapus?';
$lang['organization:detail'] = 'Detil';
$lang['organization:all_hieararchy'] = 'Seluruh Hirarki';

$lang['organization:membership_user_already_registered'] = 'Pengguna sudah menjadi anggota unit ini.';
$lang['organization:count_member'] = '# Anggota';
$lang['organization:you'] = '+Anda';