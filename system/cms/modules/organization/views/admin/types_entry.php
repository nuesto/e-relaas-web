<div class="page-header">
	<h1><?php echo lang('organization:types:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/organization/types/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('organization:back') ?>
		</a>
		
		<?php if(group_has_role('organization', 'edit_all_types')){ ?>
			<a href="<?php echo site_url('admin/organization/types/edit/'.$types->id); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('organization', 'edit_own_types')){ ?>
			<?php if($types->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/organization/types/edit/'.$types->id); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('organization', 'delete_all_types')){ ?>
			<a href="<?php echo site_url('admin/organization/types/delete/'.$types->id); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('organization', 'delete_own_types')){ ?>
			<?php if($types->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/organization/types/delete/'.$types->id); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:type_name'); ?></div>
		<?php if($types->type_name){ ?>
		<div class="entry-detail-value"><?php echo $types->type_name; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>
	
	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:type_slug'); ?></div>
		<?php if($types->type_slug){ ?>
		<div class="entry-detail-value"><?php echo $types->type_slug; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>
	
	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:type_level'); ?></div>
		<?php if($types->type_level){ ?>
		<div class="entry-detail-value"><?php echo $types->type_level; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:type_description'); ?></div>
		<?php if($types->type_description){ ?>
		<div class="entry-detail-value"><?php echo $types->type_description; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

</div>