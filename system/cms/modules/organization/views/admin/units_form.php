<div class="page-header">
	<h1><?php echo lang('organization:units:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string(), 'class="streams_form"'); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="unit_name"><?php echo lang('organization:unit_name'); ?> *</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('unit_name') != NULL){
					$value = $this->input->post('unit_name');
				}elseif($mode == 'edit'){
					$value = $fields->unit_name;
				}
			?>
			<input name="unit_name" type="text" value="<?php echo $value; ?>" class="form-control" id="unit_name" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="unit_abbrevation"><?php echo lang('organization:unit_abbrevation'); ?> </label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('unit_abbrevation') != NULL){
					$value = $this->input->post('unit_abbrevation');
				}elseif($mode == 'edit'){
					$value = $fields->unit_abbrevation;
				}
			?>
			<input name="unit_abbrevation" type="text" value="<?php echo $value; ?>" class="form-control" id="unit_abbrevation" />
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="unit_slug"><?php echo lang('organization:unit_slug');?> *</label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('unit_slug') != NULL){
					$value = $this->input->post('unit_slug');
				}elseif($mode == 'edit'){
					$value = $fields->unit_slug;
				}
			?>
			<input name="unit_slug" type="text" value="<?php echo $value; ?>" class="form-control" id="unit_slug" />
			<script type="text/javascript">$('#unit_name').slugify({slug:'#unit_slug', type:'-'});</script>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="unit_description"><?php echo lang('organization:unit_description'); ?> </label>

		<div class="col-sm-10">
			<?php 
				$value = NULL;
				if($this->input->post('unit_description') != NULL){
					$value = $this->input->post('unit_description');
				}elseif($mode == 'edit'){
					$value = $fields->unit_description;
				}
			?>
			<textarea name="unit_description" class="form-control" id="unit_description" row="10" cols="40"><?php echo $value; ?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="unit_type"><?php echo lang('organization:unit_type'); ?> *</label>

		<div class="col-sm-3">
			<?php 
				$value = NULL;
				if($this->input->post('unit_type') != NULL){
					$value = $this->input->post('unit_type');
				}elseif($mode == 'edit'){
					$value = $fields->unit_type;
				}
			?>
			
			<select name="unit_type" class="form-control" id="unit_type">
				<option value="">-- <?php echo lang('organization:choose_type'); ?> --</option>
			<?php foreach ($unit_type as $type) { ?>
				<option value="<?php echo $type['id']; ?>" <?php if($type['id'] == $value) echo 'selected'; ?>><?php echo $type['name']; ?></option>
			<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="unit_sort_order"><?php echo lang('organization:unit_sort_order');?> </label>

		<div class="col-sm-1">
			<select class="form-control" id="unit_sort_order" name="unit_sort_order">
				<?php for($i = -20; $i <= 20; $i++){ ?>
				<option <?php if($i == 0){echo 'selected';} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="unit_parents"><?php echo lang('organization:unit_parents'); ?> </label>

		<div id="unit_parents_container" class="col-sm-10">
			<p id="unit-parents" class="form-control-static"><?php echo lang('organization:loading'); ?></p>
			<img id="unit-loader" src="<?php echo base_url("addons/shared_addons/modules/organization/img/ajax-loader.gif"); ?>">
		</div>
		
		<script>
			$(document).ready(function(){
				$('#unit-loader').hide();
				// make get request
				var type_id = $('#unit_type').val();
				$("#unit-parents").load("<?php echo site_url('admin/organization/units/ajax_unit_parents_checkbox') ?>" + '/' + type_id, function(){
					//$("#unit_parents").html('Loading..');
				});
			});
			
			$('#unit_type').change(function(){
				var type_id = $('#unit_type').val();
				$("#unit-parents").hide();
				$('#unit-loader').show();
				$("#unit-parents").load("<?php echo site_url('admin/organization/units/ajax_unit_parents_checkbox') ?>" + '/' + type_id, function(){
					//$("#unit_parents").html('Loading..');
					$('#unit-loader').hide();
					$("#unit-parents").show();
				});
			});
		</script>
	</div>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $fields->id;?>" name="row_edit_id" /><?php } ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url(isset($return) ? $return : 'admin/streams/entries/index/'.$stream->id); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>