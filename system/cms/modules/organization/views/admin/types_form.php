<div class="page-header">
	<h1><?php echo lang('organization:types:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string(), 'class="streams_form"'); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="type_name"><?php echo lang('organization:type_name');?> *</label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('type_name') != NULL){
					$value = $this->input->post('type_name');
				}elseif($mode == 'edit'){
					$value = $fields->type_name;
				}
			?>
			<input name="type_name" type="text" value="<?php echo $value; ?>" class="form-control" id="type_name" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="type_slug"><?php echo lang('organization:type_slug');?> *</label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('type_slug') != NULL){
					$value = $this->input->post('type_slug');
				}elseif($mode == 'edit'){
					$value = $fields->type_slug;
				}
			?>
			<input name="type_slug" type="text" value="<?php echo $value; ?>" class="form-control" id="type_slug" />
			<script type="text/javascript">$('#type_name').slugify({slug:'#type_slug', type:'-'});</script>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="type_level"><?php echo lang('organization:type_level'); ?> *</label>

		<div class="col-sm-1">
			<?php
				$value = NULL;
				if($this->input->post('type_level') != NULL){
					$value = $this->input->post('type_level');
				}elseif($mode == 'edit'){
					$value = $fields->type_level;
				}
			?>

			<select class="form-control" id="type_level" name="type_level">
				<?php for($i = 0; $i <= 10; $i++){ ?>
				<option <?php if($value == $i){echo 'selected';} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="type_description"><?php echo lang('organization:type_description'); ?> </label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('type_description') != NULL){
					$value = $this->input->post('type_description');
				}elseif($mode == 'edit'){
					$value = $fields->type_description;
				}
			?>
			<textarea name="type_description" class="form-control" id="type_description" row="10" cols="40"><?php echo $value; ?></textarea>
		</div>
	</div>

	<div class="form-group">

		<label class="col-sm-2 control-label no-padding-right" for="type_description"><?php echo lang('organization:available_groups'); ?> </label>

		<div class="col-sm-10">
			<?php
				$value = NULL;
				if($this->input->post('available_groups') != NULL){
					$value = $this->input->post('available_groups');
				}elseif($mode == 'edit'){
					$value = json_decode($fields->available_groups);
					if(!$value) {
						$value = array();
					}
				}

			?>
			<p class="form-control-static">
			<?php foreach ($groups as $group) {
				if(!is_null($value)){
			?>
				<input type="checkbox" name="available_groups[]" value="<?php echo $group->id; ?>" <?php if (in_array($group->id, $value)) echo 'checked'; ?>> <?php echo $group->description; ?><br />
			<?php
				}else{
			?>
				<input type="checkbox" name="available_groups[]" value="<?php echo $group->id; ?>"> <?php echo $group->description; ?><br />
			<?php
				}
			?>
			<?php } ?>
			</p>
		</div>
	</div>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $fields->id;?>" name="row_edit_id" /><?php } ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button type="submit" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url(isset($return) ? $return : 'admin/streams/entries/index/'.$stream->id); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>