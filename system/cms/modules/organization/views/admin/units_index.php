<div class="page-header">
	<h1><?php echo lang('organization:units:plural'); ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<div class="tabbable tab-link">
	<ul class="nav nav-tabs">
		<li class="<?php if($filter_type == NULL){echo 'active';} ?>">
			<a href="<?php echo site_url('admin/organization/units/index'); ?>"><?php echo lang('organization:all_hieararchy'); ?></a>
		</li>

		<?php foreach($types as $type){ ?>
		<li class="<?php if($filter_type == $type['slug']){echo 'active';} ?>">
			<a href="<?php echo site_url('admin/organization/units/index').'?type='.$type['slug']; ?>"><?php echo $type['name']; ?></a>
		</li>
		<?php } ?>
	</ul>
</div>

<?php if ($units['total'] > 0): ?>

<table id="units_list" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			
			<th><?php echo lang('organization:unit_name'); ?></th>

			<?php if($filter_type == NULL){ ?>
				<th><?php echo lang('organization:unit_type'); ?></th>
			<?php }else{ ?>
				<th><?php echo lang('organization:unit_parents'); ?></th>
			<?php } ?>

			<th width="300"></th>
		</tr>
	</thead>
	<tbody>
		<?php $count = 1; ?>
		<?php foreach ($units['entries'] as $units_entry): ?>
		<tr data-tt-id="<?php echo $units_entry['id']; ?>" <?php echo (isset($units_entry['parent_id'])) ? 'data-tt-parent-id="'.$units_entry['parent_id'].'"' : ''; ?>>
			
			<td>
			<?php 
			if($units_entry['unit_abbrevation'] != NULL AND $units_entry['unit_abbrevation'] != ''){
				$units_entry['unit_name'] .= ' ('.$units_entry['unit_abbrevation'].')';
			}
			echo $units_entry['unit_name'];
			?>
			</td>

			<?php if($filter_type == NULL){ ?>
				<td><?php echo $units_entry['type_name']; ?></td>
			<?php }else{ ?>
				<td><?php echo $units_entry['parent_name']; ?></td>
			<?php } ?>

			<?php $members = $this->organization->get_membership_by_unit($units_entry['id']); ?>
			
			<td class="actions">

			<?php 
			if(group_has_role('organization', 'view_all_units')){
				echo anchor('admin/organization/units/view/' . $units_entry['id'], lang('global:view'), 'class="btn btn-xs btn-info view"');
			}elseif(group_has_role('organization', 'view_own_units')){
				if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
					echo anchor('admin/organization/units/view/' . $units_entry['id'], lang('global:view'), 'class="btn btn-xs btn-info view"');
				}
			}
			?>
			<?php 
			if(group_has_role('organization', 'edit_all_units')){
				echo anchor('admin/organization/units/edit/' . $units_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');
			}elseif(group_has_role('organization', 'edit_own_units')){
				if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
					echo anchor('admin/organization/units/edit/' . $units_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');
				}
			}
			?>
			<?php
			if(group_has_role('organization', 'view_all_memberships')){
				echo anchor('admin/organization/memberships/index/' . $units_entry['id'], lang('organization:memberships:singular').' <span class="badge badge-yellow">'.$members['total'].'</span>', 'class="btn btn-xs btn-info view"');
			}elseif(group_has_role('organization', 'view_own_memberships')){
				if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
					echo anchor('admin/organization/memberships/index/' . $units_entry['id'], lang('organization:memberships:singular').' <span class="badge badge-yellow">'.$members['total'].'</span>', 'class="btn btn-xs btn-info view"');
				}
			}
			?>
			<?php 
			if(group_has_role('organization', 'delete_all_units')){
				echo anchor('admin/organization/units/delete/' . $units_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
			}elseif(group_has_role('organization', 'delete_own_units')){
				if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
					echo anchor('admin/organization/units/delete/' . $units_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
				}
			}
			?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<?php echo $units['pagination']; ?>

<script type="text/javascript">
$('#units_list').treetable(
	{expandable : true});
</script>

<?php else: ?>
<div class="well"><?php echo lang('organization:units:no_entry'); ?></div>
<?php endif;?>