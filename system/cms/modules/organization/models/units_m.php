<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Units model
 *
 * @author Aditya Satrya
 */
class Units_m extends MY_Model {
	
	public function get_units($params = NULL)
	{
		$this->db->select('organization_units.id AS id');
		$this->db->select('organization_units.created AS created');
		$this->db->select('organization_units.updated AS updated');
		$this->db->select('organization_units.created_by AS created_by');
		$this->db->select('organization_units.ordering_count AS ordering_count');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_units.unit_type AS unit_type');
		$this->db->select('organization_units.unit_sort_order AS unit_sort_order');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		$this->db->select('organization_units_units.units_id AS parent_id');
		$this->db->select('parent_units.unit_name AS parent_name');
		$this->db->select('parent_units.unit_abbrevation AS parent_abbrevation');
		$this->db->select('parent_units.unit_slug AS parent_slug');
		
		if(isset($params)) {
			$this->db->like($params);
		}

		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		$this->db->join('organization_units_units', 'organization_units.id = organization_units_units.row_id', 'left');
		$this->db->join('organization_units parent_units', 'parent_units.id = organization_units_units.units_id', 'left');

		$this->db->order_by('parent_name', 'asc');
		$this->db->order_by('unit_name', 'asc');
		
		$res = $this->db->get('organization_units')->result_array();
		return $res;
	}

	public function get_units_by_type($type_slug)
	{
		$this->db->select('organization_units.id AS id');
		$this->db->select('organization_units.created AS created');
		$this->db->select('organization_units.updated AS updated');
		$this->db->select('organization_units.created_by AS created_by');
		$this->db->select('organization_units.ordering_count AS ordering_count');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_units.unit_type AS unit_type');
		$this->db->select('organization_units.unit_sort_order AS unit_sort_order');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		$this->db->select('organization_types.available_groups AS available_groups');
		$this->db->select('organization_units_units.units_id AS parent_id');
		$this->db->select('parent_units.unit_name AS parent_name');
		$this->db->select('parent_units.unit_abbrevation AS parent_abbrevation');
		$this->db->select('parent_units.unit_slug AS parent_slug');
		
		$this->db->where('organization_types.type_slug', $type_slug);

		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		$this->db->join('organization_units_units', 'organization_units.id = organization_units_units.row_id', 'left');
		$this->db->join('organization_units parent_units', 'parent_units.id = organization_units_units.units_id', 'left');

		$this->db->order_by('parent_name', 'asc');
		$this->db->order_by('unit_name', 'asc');
		
		$res = $this->db->get('organization_units')->result_array();
		return $res;
	}
	
	public function get_units_by_level($level)
	{
		$this->db->select('organization_units.id AS id');
		$this->db->select('organization_units.created AS created');
		$this->db->select('organization_units.updated AS updated');
		$this->db->select('organization_units.created_by AS created_by');
		$this->db->select('organization_units.ordering_count AS ordering_count');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_units.unit_type AS unit_type');
		$this->db->select('organization_units.unit_sort_order AS unit_sort_order');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		$this->db->select('organization_types.available_groups AS available_groups');
		
		$this->db->where('organization_types.type_level', $level);
		$this->db->or_where('organization_types.type_level', NULL);
		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		
		$res = $this->db->get('organization_units')->result_array();
		return $res;
	}
	
	public function get_units_by_parent($parent_id)
	{
		$this->db->select('organization_units.id AS id');
		$this->db->select('organization_units.created AS created');
		$this->db->select('organization_units.updated AS updated');
		$this->db->select('organization_units.created_by AS created_by');
		$this->db->select('organization_units.ordering_count AS ordering_count');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_units.unit_type AS unit_type');
		$this->db->select('organization_units.unit_sort_order AS unit_sort_order');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		$this->db->select('organization_types.available_groups AS available_groups');
		$this->db->select('organization_units_units.units_id AS parent_id');

		$this->db->where('organization_units_units.units_id', $parent_id);
	
		$this->db->join('organization_units', 'organization_units.id = organization_units_units.row_id', 'left');
		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		
		$res = $this->db->get('organization_units_units')->result_array();
		
		return $res;
	}
	
	public function get_units_by_ids($where)
	{
		$this->db->select('id,unit_name');
		if ($where!='') $this->db->where($where);
		$this->db->join('users', 'organization_units.created_by=users.id');
		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		$query = $this->db->get('organization_units');
		$result = $query->result_array();
		
        return $result;
	}

	public function get_units_by_id($id)
	{
		$this->db->select('organization_units.*, users.username as created_by_username, users.id as created_by_user_id, users.email as created_by_email');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		$this->db->select('organization_types.available_groups AS available_groups');
		
		if ($id!='') $this->db->where('organization_units.id', $id);
		$this->db->join('users', 'organization_units.created_by=users.id');
		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		$query = $this->db->get('organization_units');

		$result = $query->row();
		
        return $result;
	}

	public function get_parent_units_by_id($id)
	{
		$this->db->select('organization_units.*, users.username as created_by_username, users.id as created_by_user_id, users.email as created_by_email');
		if ($id!='') $this->db->where('organization_units_units.row_id', $id);
		$this->db->join('users', 'organization_units.created_by=users.id');
		$this->db->join('organization_units_units', 'organization_units_units.units_id=organization_units.id', 'LEFT');
		$query = $this->db->get('organization_units');

		$result = $query->result_array();
		
        return $result;
	}

	public function insert_units($values) {
		$unit_parents = array();
		if(isset($values['unit_parents'])) {
			$unit_parents = $values['unit_parents'];
			unset($values['unit_parents']);
		}

		$values['created'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;

		$this->db->insert('organization_units', $values);
		$id_unit = $this->db->insert_id();
		
		if(count($unit_parents)>0 and $unit_parents != '') {
			if(is_array($unit_parents)) {
				foreach ($unit_parents as $p) {
					if($p!='') {
						$this->db->insert('organization_units_units', array('row_id' => $id_unit, 'units_id'=>$p));
					}
				}
			} else {
				$this->db->insert('organization_units_units', array('row_id' => $id_unit, 'units_id'=>$unit_parents));
			}
		}

		return true;
	}

	public function update_units($values, $row_id) {
		$unit_parents = array();
		if(isset($values['unit_parents']) && $values['unit_parents'] != '') {
			$unit_parents = $values['unit_parents'];
		}
		
		unset($values['row_edit_id']);
		unset($values['unit_parents']);

		$values['updated'] = date("Y-m-d H:i:s");

		$this->db->where('id', $row_id);
		$this->db->update('organization_units', $values);
		
		if(count($unit_parents)>0) {
			$this->db->where('row_id', $row_id);
			$this->db->delete('organization_units_units');

			if(is_array($unit_parents)) {
				foreach ($unit_parents as $p) {
					$this->db->insert('organization_units_units', array('row_id' => $row_id, 'units_id'=>$p));
				}
			} else {
				$this->db->insert('organization_units_units', array('row_id' => $row_id, 'units_id'=>$unit_parents));
			}
		}

		return true;
	}

	public function delete_units_by_id($id)
	{
		$this->db->where('membership_unit', $id);
		$this->db->delete('organization_memberships');

		$this->db->where('row_id', $id);
		$this->db->delete('organization_units_units');

		$this->db->where('id', $id);
		$this->db->delete('organization_units');
	}
	
}