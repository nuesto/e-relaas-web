<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['organization/admin/units(:any)'] = 'admin_units$1';
$route['organization/admin/types(:any)'] = 'admin_types$1';
$route['organization/admin/memberships(:any)'] = 'admin_memberships$1';
$route['organization/admin/titles(:any)'] = 'admin_titles$1';
