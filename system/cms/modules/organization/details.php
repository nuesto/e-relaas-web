<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Organization extends Module
{
    public $version = '1.4.4';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Organization',
			'id' => 'Organization',
		);
		$info['description'] = array(
			'en' => 'Module to manage organization',
			'id' => 'Module to manage organization',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		$info['menu'] = 'organization';
		$info['roles'] = array(
			'access_units_backend', 'view_all_units', 'view_own_units', 'edit_all_units', 'edit_own_units', 'delete_all_units', 'delete_own_units', 'create_units',
			'access_types_backend', 'view_all_types', 'view_own_types', 'edit_all_types', 'edit_own_types', 'delete_all_types', 'delete_own_types', 'create_types',
			'view_all_memberships', 'view_own_memberships', 'delete_all_memberships', 'delete_own_memberships', 'create_all_units_memberships', 'create_own_units_memberships',
		);

		if(group_has_role('organization', 'access_units_backend')){
			$info['sections']['units']['name'] = 'organization:units:plural';
			$info['sections']['units']['uri'] = 'admin/organization/units/index';

			if(group_has_role('organization', 'create_units')){
				$info['sections']['units']['shortcuts']['create'] = array(
					'name' => 'organization:units:new',
					'uri' => 'admin/organization/units/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('organization', 'access_types_backend')){
			$info['sections']['types']['name'] = 'organization:types:plural';
			$info['sections']['types']['uri'] = 'admin/organization/types/index';

			if(group_has_role('organization', 'create_types')){
				$info['sections']['types']['shortcuts']['create'] = array(
					'name' => 'organization:types:new',
					'uri' => 'admin/organization/types/create',
					'class' => 'add'
				);
			}
		}

		return $info;
    }

    /**
     * Admin menu
     *
     * If a module has an admin_menu function, then
     * we simply run that and allow it to manipulate the
     * menu array
     */
    public function admin_menu(&$menu_items){
        
    }

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

        // Table organization_types

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'type_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 256,
            ),
            'type_slug' => array(
                'type' => 'VARCHAR',
                'constraint' => 256,
            ),
            'type_description' => array(
                'type' => 'LONGTEXT',
            ),
            'type_level' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'available_groups' => array(
                'type' => 'TEXT',
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'ordering_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('organization_types', TRUE);

        // Table organization_units

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'unit_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 256,
            ),
            'unit_abbrevation' => array(
                'type' => 'VARCHAR',
                'constraint' => 256,
            ),
            'unit_slug' => array(
                'type' => 'VARCHAR',
                'constraint' => 256,
            ),
            'unit_description' => array(
                'type' => 'LONGTEXT',
            ),
            'unit_type' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'unit_sort_order' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'ordering_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('organization_units', TRUE);
        $this->db->query("CREATE INDEX types_index ON default_organization_units(unit_type)");

        // Table organization_units_units

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'row_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'units_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'ordering_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('organization_units_units', TRUE);

        // Table organization_titles

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'title_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 256,
            ),
            'title_description' => array(
                'type' => 'LONGTEXT',
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'ordering_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('organization_titles', TRUE);

        // Table organization_memberships

        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ),
            'membership_unit' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'membership_user' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'membership_title' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'membership_is_head' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'ordering_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('organization_memberships', TRUE);

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
        $this->load->dbforge();

        $this->dbforge->drop_table('organization_memberships');
        $this->dbforge->drop_table('organization_titles');
        $this->dbforge->drop_table('organization_units_units');
        $this->dbforge->drop_table('organization_units');
        $this->dbforge->drop_table('organization_types');

        // $this->load->driver('Streams');

        // // For this teardown we are using the simple remove_namespace
        // // utility in the Streams API Utilties driver.
        // $this->streams->utilities->remove_namespace('organization');

        return true;
    }

    public function upgrade($old_version)
    {
      switch($old_version){

        case '1.0':

            // add fields

            $fields = array(
                'type_slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 256,
                ),
                'type_level' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                ),
            );

            $this->dbforge->add_column('organization_types', $fields);

            $fields = array(
                'unit_slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 256,
                ),
            );

            $this->dbforge->add_column('organization_units', $fields);

            // delete fields

            $this->dbforge->drop_column('organization_units', 'unit_level');

        case '1.1':

            // add fields

            $fields = array(
                    'available_groups' => array(
                    'type' => 'TEXT',
                ),
            );

            $this->dbforge->add_column('organization_types', $fields);

        case '1.2':
            // convert all tables to InnoDB engine
            $this->db->query("ALTER TABLE `default_organization_units` ENGINE=InnoDB");
            $this->db->query("ALTER TABLE `default_organization_units_units` ENGINE=InnoDB");
            $this->db->query("ALTER TABLE `default_organization_types` ENGINE=InnoDB");
            $this->db->query("ALTER TABLE `default_organization_titles` ENGINE=InnoDB");
            $this->db->query("ALTER TABLE `default_organization_memberships` ENGINE=InnoDB");

        case '1.3.0':
            $constraint = $this->db->query("SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = '".$this->db->database."' AND REFERENCED_TABLE_NAME = '".$this->db->dbprefix('organization_units')."' AND TABLE_NAME = '".$this->db->dbprefix('blog')."'");
            
            $blog_fk = $constraint->row_array();

            if(isset($blog_fk['CONSTRAINT_NAME'])) {
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('blog')." DROP FOREIGN KEY `".$blog_fk['CONSTRAINT_NAME']."`");
            }
            
            // change column id data type
            $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_units')." CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL auto_increment");

            // stop at this version
            // to continue please upgrade again
            $this->version = '1.4.1';
            break;

        case '1.4.0':
            // to continue please upgrade again
            $this->version = '1.4.1';
            break;

        case '1.4.1':
                // ------------------------------------------
                // CREATE FK FOR `organization_memberships`.`membership_unit`
                // CREATE FK FOR `organization_memberships`.`membership_user`
                // CREATE FK FOR `organization_memberships`.`membership_title`
                // ------------------------------------------

                // Step 1 - create index (if not yet an index)
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_memberships')." ADD INDEX(`membership_unit`)");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_memberships')." ADD INDEX(`membership_title`)");

                // Step 2 - delete all child rows that have no match FK reference in parent row

                // parent widget
                $this->db->select('id');
                $existing_ids = $this->db->get('organization_units')->result();

                $existing_ids_arr = array();
                foreach ($existing_ids as $existing_id) {
                    $existing_ids_arr[] = (int)$existing_id->id;
                }

                if(count($existing_ids_arr) > 0){
                    $this->db->where_not_in('membership_unit', $existing_ids_arr);
                }else{
                    $this->db->where(true);
                }
                $this->db->delete('organization_memberships');

                // parent widget
                $this->db->select('id');
                $existing_ids = $this->db->get('users')->result();

                $existing_ids_arr = array();
                foreach ($existing_ids as $existing_id) {
                    $existing_ids_arr[] = (int)$existing_id->id;
                }

                if(count($existing_ids_arr) > 0){
                    $this->db->where_not_in('membership_user', $existing_ids_arr);
                }else{
                    $this->db->where(true);
                }
                $this->db->delete('organization_memberships');

                // parent widget
                $this->db->select('id');
                $existing_ids = $this->db->get('organization_titles')->result();

                $existing_ids_arr = array();
                foreach ($existing_ids as $existing_id) {
                    $existing_ids_arr[] = (int)$existing_id->id;
                }

                if(count($existing_ids_arr) > 0){
                    $this->db->where_not_in('membership_title', $existing_ids_arr);
                }else{
                    $this->db->where(true);
                }
                $this->db->delete('organization_memberships');

                // Step 3 - equalize child's column data type to parent's
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_memberships')." CHANGE COLUMN `membership_unit` `membership_unit` INT(11) UNSIGNED NULL");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_memberships')." CHANGE COLUMN `membership_user` `membership_user` INT(11) UNSIGNED NULL");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_memberships')." CHANGE COLUMN `membership_title` `membership_title` INT(11) NULL");

                // Step 4 - create references
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_memberships')." ADD FOREIGN KEY(`membership_unit`) REFERENCES ".$this->db->dbprefix('organization_units')."(`id`)");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_memberships')." ADD FOREIGN KEY(`membership_user`) REFERENCES ".$this->db->dbprefix('users')."(`id`)");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_memberships')." ADD FOREIGN KEY(`membership_title`) REFERENCES ".$this->db->dbprefix('organization_titles')."(`id`)");
                
                
                // force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
                $this->version = '1.4.2';
                break;

            case '1.4.2':
                // ------------------------------------------
                // CREATE FK FOR `organization_units`.`unit_type`
                // ------------------------------------------

                // Step 1 - create index (if not yet an index)
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_units')." ADD INDEX(`unit_type`)");

                // Step 2 - delete all child rows that have no match FK reference in parent row

                // parent widget
                $this->db->select('id');
                $existing_ids = $this->db->get('organization_types')->result();

                $existing_ids_arr = array();
                foreach ($existing_ids as $existing_id) {
                    $existing_ids_arr[] = (int)$existing_id->id;
                }

                if(count($existing_ids_arr) > 0){
                    $this->db->where_not_in('unit_type', $existing_ids_arr);
                }else{
                    $this->db->where(true);
                }
                $this->db->delete('organization_units');

                // Step 3 - equalize child's column data type to parent's
                // no need
                
                // Step 4 - create references
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_units')." ADD FOREIGN KEY(`unit_type`) REFERENCES ".$this->db->dbprefix('organization_types')."(`id`)");
                
                // force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
                $this->version = '1.4.3';
                break;

            case '1.4.3':
                // ------------------------------------------
                // CREATE FK FOR `organization_units_units`.`row_id`
                // CREATE FK FOR `organization_units_units`.`units_id`
                // ------------------------------------------

                // Step 1 - create index (if not yet an index)
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_units_units')." ADD INDEX(`row_id`)");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_units_units')." ADD INDEX(`units_id`)");

                // Step 2 - delete all child rows that have no match FK reference in parent row

                // parent widget
                $this->db->select('id');
                $existing_ids = $this->db->get('organization_units')->result();

                $existing_ids_arr = array();
                foreach ($existing_ids as $existing_id) {
                    $existing_ids_arr[] = (int)$existing_id->id;
                }

                if(count($existing_ids_arr) > 0){
                    $this->db->where_not_in('row_id', $existing_ids_arr);
                    $this->db->or_where_not_in('units_id', $existing_ids_arr);
                }else{
                    $this->db->where(true);
                }
                $this->db->delete('organization_units_units');

                // Step 3 - equalize child's column data type to parent's
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_units_units')." CHANGE COLUMN `row_id` `row_id` INT(11) UNSIGNED NOT NULL");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_units_units')." CHANGE COLUMN `units_id` `units_id` INT(11) UNSIGNED NOT NULL");
                
                // Step 4 - create references
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_units_units')." ADD FOREIGN KEY(`row_id`) REFERENCES ".$this->db->dbprefix('organization_units')."(`id`)");
                $this->db->query("ALTER TABLE ".$this->db->dbprefix('organization_units_units')." ADD FOREIGN KEY(`units_id`) REFERENCES ".$this->db->dbprefix('organization_units')."(`id`)");
                
                // force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
                $this->version = '1.4.4';
                break;
	    }

		return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}
