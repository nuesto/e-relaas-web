<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Organization Module
 *
 * Module to manage organization
 *
 */
class Admin_units extends Admin_Controller
{
    // This will set the active section tab
    protected $section = 'units';

	private $units_arr = array();
	private $sorted_units_arr = array();

    public function __construct()
    {
        parent::__construct();

		// Check permission
		if(! group_has_role('organization', 'access_units_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('organization');
        $this->load->library('Organization');
		
		$this->load->model('units_m');
		$this->load->model('types_m');
    }

    /**
	 * List all Units
     *
     * @return	void
     */
    public function index()
    {
		// Check permission
		if(! group_has_role('organization', 'view_all_units') AND ! group_has_role('organization', 'view_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// Get types
		$data['types'] = $this->types_m->get_all_types();

		// Get units
		$data['filter_type'] = $this->input->get('type');

		if($data['filter_type'] == NULL){
	    	$root_units = $this->units_m->get_units_by_level(0);

	    	$units = $this->organization->build_serial_tree(array(), $root_units);
		}else{
			$units = $this->units_m->get_units_by_type($data['filter_type']);
		}
		
		$data['units']['total'] = count($units);
		$data['units']['entries'] = $units;
		$data['units']['pagination'] = '';

		// -------------------------------------
		// Restric users
		// -------------------------------------
		// Eliminate not owned unit
		if(! group_has_role('organization', 'view_all_units')){
			$temp_arr = array();
			foreach($data['units']['entries'] as $unit){
				if($this->organization->is_member($this->current_user->id, $unit['id'])){
					$temp_arr[] = $unit;
				}
			}
			$data['units']['entries'] = $temp_arr;
			$data['units']['total'] = count($temp_arr);
		}

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->append_css('module::treetable/jquery.treetable.css');
        $this->template->append_css('module::organization.css');
        $this->template->append_js('module::treetable/jquery.treetable.js');

        $this->template->title(lang('organization:units:plural'));
		$this->template->build('admin/units_index', $data);
    }
	
	/**
     * Display one Units
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // Check permission
		if(! group_has_role('organization', 'view_all_units') AND ! group_has_role('organization', 'view_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Get our entry. We are simply specifying
        $data['units'] =  $this->units_m->get_units_by_id($id);
		$data['units']->unit_type = (object) $this->types_m->get_type_by_id($data['units']->unit_type);
		
		// Check view all/own permission
		if(! group_has_role('organization', 'view_all_units')){
			if(! $this->organization->is_member($this->current_user->id, $id)){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// Get multiple relationship

		$unit_parents = $this->units_m->get_parent_units_by_id($data['units']->id);
		foreach ($unit_parents as $key => $value) {
			$unit_parents[$key]['unit_type'] = (object) $this->types_m->get_type_by_id($value['unit_type']);
		}

		$data['units']->unit_parents = $unit_parents;

		// // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('organization:units:view'));
        $this->template->build('admin/units_entry', $data);
    }
	
	/**
     * Create a new Units entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// Check permission
		if(! group_has_role('organization', 'create_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_units('new')){	
				$this->session->set_flashdata('success', lang('organization:units:submit_success'));

				redirect('admin/organization/units/index');
			}else{
				$data['messages']['error'] = lang('organization:units:submit_failure');
			}
		}

		// Load everything we need
		$data['unit_type'] = $this->types_m->get_all_types();
		$data['mode'] = 'new'; //'new', 'edit'
		$data['return'] = 'admin/organization/units/index';
		
		// Build the form page.
		$this->template->append_js('jquery/jquery.slugify.js');
        $this->template->title(lang('organization:units:new'));
        $this->template->build('admin/units_form', $data);
    }
	
	/**
     * Edit a Units entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Units to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // Check permission
		if(! group_has_role('organization', 'edit_all_units') AND ! group_has_role('organization', 'edit_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('organization', 'edit_all_units')){
			if(! $this->organization->is_member($this->current_user->id, $id)){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_units('edit', $id)){	
				$this->session->set_flashdata('success', lang('organization:units:submit_success'));

				redirect('admin/organization/units/index');
			}else{
				$data['messages']['error'] = lang('organization:units:submit_failure');
			}
		}
		
		// Load everything we need
		
		$data['fields'] = $this->units_m->get_units_by_id($id);
		$data['unit_type'] = $this->types_m->get_all_types();
		$data['mode'] = 'edit'; //'new', 'edit'
		$data['return'] = 'admin/organization/units/index';
		
		// Build the form page.
		$this->template->append_js('jquery/jquery.slugify.js');
        $this->template->title(lang('organization:units:edit'));
        $this->template->build('admin/units_form', $data);
    }
	
	/**
     * Delete a Units entry
     * 
     * @param   int [$id] The id of Units to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// Check permission
		if(! group_has_role('organization', 'delete_all_units') AND ! group_has_role('organization', 'delete_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('organization', 'delete_all_units')){
			if(! $this->organization->is_member($this->current_user->id, $id)){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$children = $this->units_m->get_units_by_parent($id);
		if(count($children) > 0){
			$this->session->set_flashdata('error', lang('organization:units:delete_fail_has_child'));
		}else{
			$this->units_m->delete_units_by_id($id);
			$this->session->set_flashdata('success', lang('organization:units:deleted'));
		}
		
		redirect('admin/organization/units/index');
    }
	
	public function ajax_unit_parents_checkbox($type_id = 0){
		$this->config->load('organization');
		$allow_multi_parent = $this->config->item('allow_multi_parent');

		$type = $this->types_m->get_type_by_id($type_id);
		if(isset($type->type_level) AND $type->type_level > 0){
			$parent_level = $type->type_level - 1;			
			$units = $this->units_m->get_units_by_level($parent_level);
			
			if ($allow_multi_parent) {
				foreach($units as $unit){
					echo '<input type="checkbox" name="unit_parents[]" id="unit_parents" value="'.$unit['id'].'" /> '.$unit['unit_name'].'<br />';
				}
			} else {
				echo '<select name="unit_parents" id="unit_parents">';
				foreach($units as $unit){
					echo '<option value="'.$unit['id'].'">'.$unit['unit_name'].'</option>';
				}
				echo '</select>';
			}
		}else{
			echo '<input name="unit_parents" value="" type="hidden" />';
			echo '<p id="unit_parents" class="form-control-static" />-</p>';
		}
	}

	/**
     * Insert or update Units entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_units($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('unit_name', lang('organization:unit_name'), 'required');
		$this->form_validation->set_rules('unit_slug', lang('organization:unit_slug'), 'required');
		$this->form_validation->set_rules('unit_type', lang('organization:unit_type'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->units_m->insert_units($values);
				
			}
			else
			{
				$result = $this->units_m->update_units($values, $row_id);
			}
		}
		
		return $result;
	}
}