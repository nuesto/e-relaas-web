<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Organization Module
 *
 * Module to manage organization
 *
 */
class Admin_memberships extends Admin_Controller
{
    // This will set the active section tab
    protected $section = 'units';

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('organization');
		$this->lang->load('users/user');
		$this->load->library('Organization');

		$this->load->model('units_m');
		$this->load->model('memberships_m');
		$this->load->model('groups/group_m');
		$this->config->load('organization');

    }

    /**
	 * List all Memberships
     *
     * @return	void
     */
    public function index($unit_id = 0)
    {
		// Check permission
		if(! group_has_role('organization', 'view_all_memberships') AND ! group_has_role('organization', 'view_own_memberships')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		if(! group_has_role('organization', 'view_all_memberships')){
			if(! $this->organization->is_member($this->current_user->id, $unit_id)){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// Get unit entries
		$data['unit'] = $this->units_m->get_units_by_id($unit_id);
		if(!isset($data['unit'])) {
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// Get our entries
		$data['memberships']['entries'] = $this->memberships_m->get_membership_by_unit($unit_id);
		$data['memberships']['total'] = count($data['memberships']['entries']);
		$data['memberships']['pagination'] = '';

		// Get non-member users
		// - limit groups
		$available_groups = $this->group_m->get_available_groups_by_units(array($unit_id));

		$group_list = array();

		$limit_groups = array();

		$skip_groups = array();

		if(isset($available_groups[0]['available_groups']) AND $available_groups[0]['available_groups']!='') {
			foreach ($available_groups as $ag) {
				$tmp_group_list = json_decode($ag['available_groups']);
				if(is_array($tmp_group_list)) {
					foreach ($tmp_group_list as $gl) {
						array_push($group_list, $gl);
					}
				}
			}
			$groups = $this->group_m->get_all(array('available_groups'=>implode(",", $group_list)));

			foreach ($groups as $group) {
				array_push($limit_groups, $group->name);
			}
		} else {
			$groups = $this->group_m->get_all();
			foreach ($groups as $key => $value) {
				$skip_groups[] = $value->name;
			}
		}

		// - skip groups
		if($this->current_user->group != 'admin'){
			$skip_groups[] = 'admin';
		}
		if($this->current_user->group != 'site_admin'){
			$skip_groups[] = 'site_admin';
		}

		// - skip users
		$skip_user_ids = array();

		$data['non_member_users'] = $this->memberships_m->get_non_member_users($unit_id, $limit_groups, $skip_groups, $skip_user_ids);
		
		// get unit of current user and user entries
		// unit of current user
		$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);
		$unit_lists = array();
		if(count($memberships_current_user['entries'])!=0) {
			if (count($memberships_current_user['entries']) < 1) {
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}

			$unit_current_user = $memberships_current_user['entries'];

			foreach ($unit_current_user as $key => $value) {
				$unit_lists[] = $value['membership_unit']['id'];
			}
		}

		$id_user = array();

		if(count($data['memberships']['entries'])>0) {
			$users = $data['memberships']['entries'];
			foreach ($users as $value) {
				$user2[$value['user_id']] = $value;

				$id_user[] = $value['user_id'];
			}

			$memberships_list = $this->memberships_m->get_memberships_by_multi_user($id_user);
			$unit_list = array();

			foreach ($memberships_list as $value) {
				$unit_list[$value['membership_user']][] = $value;
			}

			foreach ($user2 as $key => $value) {
				if(array_key_exists($key, $unit_list)) {
					$user2[$key]['units'] = $unit_list[$key];
				} else {
					$user2[$key]['units'] = array();
				}

				$user2[$key] = $user2[$key];
			}

			$data['memberships']['entries'] = $user2;
		}
		
        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('organization:memberships:plural'));
        $this->template->set('available_groups', $available_groups);
        $this->template->set('current_user_units', $unit_lists);
		$this->template->build('admin/memberships_index', $data);
    }
	
	public function create_process($unit_id)
	{
		$user_id = $this->input->post('membership_user');

		if($user_id!='') {
			// check membership
			$is_member = $this->memberships_m->check_membership($user_id, $unit_id);
			if($is_member){
				// Set the flashdata message and redirect
				$this->session->set_flashdata('error', lang('organization:membership_user_already_registered'));

				redirect('admin/organization/memberships/index/'.$unit_id);
			}

			// create membership
			$allow_multi_membership = $this->config->item('allow_multi_membership');

			if(!$allow_multi_membership) {
				$this->memberships_m->delete_membership_by_user($user_id);
			}

			$this->memberships_m->create_membership($user_id, $unit_id);

			// Set the flashdata message and redirect
			$this->session->set_flashdata('success', lang('organization:memberships:submit_success'));
		}
		redirect('admin/organization/memberships/index/'.$unit_id);
	}
	
	/**
     * Delete a Memberships entry
     * 
     * @param   int [$id] The id of Memberships to be deleted
     * @return  void
     */
    public function delete($id = 0, $user_id = 0)
    {
		// Check permission
		if(! group_has_role('organization', 'delete_all_memberships') AND ! group_has_role('organization', 'delete_own_memberships')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		$membership_entry = $this->memberships_m->get_membership_by_id($id);
		
		if(! group_has_role('organization', 'delete_all_memberships')){
			$memberships_current_user = $this->organization->get_membership_by_user($this->current_user->id);
			if(count($memberships_current_user['entries'])!=0) {
				if (count($memberships_current_user['entries']) < 1) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}

				$unit_current_user = $memberships_current_user['entries'];

				foreach ($unit_current_user as $key => $value) {
					$unit_lists[] = $value['membership_unit']['id'];
				}

				$memberships_user = $this->organization->get_membership_by_user($user_id);
				
				if (count($memberships_user['entries']) < 1) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}

				$unit_user = $memberships_user['entries'];

				foreach ($unit_user as $key => $value) {
					$unit_user_lists[] = $value['membership_unit']['id'];
				}

				if (in_array($unit_user_lists, $unit_lists)) {
					$this->session->set_flashdata('error', lang('cp:access_denied'));
					redirect('admin');
				}
			}
		}

		// Get will-be-deleted entry
		
		$this->memberships_m->delete_membership_by_id($id);
        $this->session->set_flashdata('success', lang('organization:memberships:deleted'));

        if(! group_has_role('organization', 'view_all_memberships')){
			redirect('admin/organization/units/index/');
		}else{
			redirect('admin/organization/memberships/index/'.$membership_entry->unit_id);
		}
    }

}