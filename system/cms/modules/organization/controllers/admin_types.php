<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Organization Module
 *
 * Module to manage organization
 *
 */
class Admin_types extends Admin_Controller
{
    // This will set the active section tab
    protected $section = 'types';

    public function __construct()
    {
        parent::__construct();

		// Check permission
		if(! group_has_role('organization', 'access_types_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('organization');

        $this->load->model('types_m');
        $this->load->model('groups/group_m');
        $this->load->helper('groups/groups');
    }

    /**
	 * List all Types
     *
     * @return	void
     */
    public function index()
    {
		// Check permission
		if(! group_has_role('organization', 'view_all_types') AND ! group_has_role('organization', 'view_own_types')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
        // -------------------------------------
		// Users
		// -------------------------------------
		if(! group_has_role('organization', 'view_all_types')){
			$params['restrict_user'] = 'current'; //'current', user id, username
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'admin/organization/types/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['total_rows'] = $this->types_m->count_all_types();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['types']['entries'] = $this->types_m->get_all_types($pagination_config);
		$data['types']['total'] = count($data['types']['entries']);
		$data['types']['pagination'] = $this->pagination->create_links();

		// Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('organization:types:plural'));
		$this->template->build('admin/types_index', $data);
    }
	
	/**
     * Create a new Types entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// Check permission
		if(! group_has_role('organization', 'create_types')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_types('new')){	
				$this->session->set_flashdata('success', lang('organization:types:submit_success'));
				
				redirect('admin/organization/types/index');
			}else{
				$data['messages']['error'] = lang('organization:types:submit_failure');
			}
		}
		
		// Load everything we need

		$data['unit_type'] = $this->types_m->get_all_types();
		$data['groups'] = $this->group_m->get_all();
		$data['mode'] = 'new'; //'new', 'edit'
		$data['return'] = 'admin/organization/types/index';
		
		// Build the form page.
		$this->template->append_js('jquery/jquery.slugify.js');

        $this->template->title(lang('organization:types:new'));
        $this->template->build('admin/types_form', $data);
    }
	
	/**
     * Edit a Types entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Types to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // Check permission
		if(! group_has_role('organization', 'edit_all_types') AND ! group_has_role('organization', 'edit_own_types')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('organization', 'edit_all_types')){
			$created_by_user_id = $this->types_m->get_type_by_id($id)->created_by;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_types('edit', $id)){	
				$this->session->set_flashdata('success', lang('organization:types:submit_success'));
				
				redirect('admin/organization/types/index');
			}else{
				$data['messages']['error'] = lang('organization:types:submit_failure');
			}
		}
		
		// Load everything we need
		
		$data['fields'] = $this->types_m->get_type_by_id($id);
		$data['unit_type'] = $this->types_m->get_all_types();
		$data['groups'] = $this->group_m->get_all();
		$data['mode'] = 'edit'; //'new', 'edit'
		$data['return'] = 'admin/organization/types/index';
		
		// Build the form page.
		$this->template->append_js('jquery/jquery.slugify.js');

        $this->template->title(lang('organization:types:edit'));
        $this->template->build('admin/types_form', $data);
    }
    
    /**
     * Delete a Types entry
     * 
     * @param   int [$id] The id of Types to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// Check permission
		if(! group_has_role('organization', 'delete_all_types') AND ! group_has_role('organization', 'delete_own_types')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('organization', 'delete_all_types')){
			$created_by_user_id = $this->types_m->get_type_by_id($id)->created_by;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
        if ($this->types_m->delete_types_by_id($id)) {
			$this->session->set_flashdata('success', lang('organization:units:deleted'));
		} else {
			$this->session->set_flashdata('error', lang('organization:units:delete_fail'));
		}
 
        redirect('admin/organization/types/index');
    }

    /**
     * Insert or update Types entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_types($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// convert value of available_groups from array to json
		$values['available_groups'] = (isset($values['available_groups']) ) ? json_encode($values['available_groups']) : '[]';

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('type_name', lang('organization:type_name'), 'required');
		$this->form_validation->set_rules('type_slug', lang('organization:type_slug'), 'required');
		$this->form_validation->set_rules('type_level', lang('organization:type_level'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->types_m->insert_types($values);
			}
			else
			{
				$result = $this->types_m->update_types($values, $row_id);
			}
		}
		
		return $result;
	}
}