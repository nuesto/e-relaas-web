<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Organization Library
 *
 * @author Aditya Satrya
 */

class Organization
{
	/**
	 * The Construct
	 */
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->driver('Streams');
		$this->CI->load->model('organization/memberships_m');
	}

	public function is_member($user_id, $unit_id)
	{
		// Get our entries
		
		$memberships = $this->CI->memberships_m->get_membership_by_unit($unit_id);

		// Check if member
		foreach ($memberships as $membership) {
			if($membership['user_id'] == $user_id) {
				return TRUE;
			}
		}

		return FALSE;
	}

	public function get_membership_by_unit($unit_id)
	{
		// Get our entries
		$members['entries'] = $this->CI->memberships_m->get_membership_by_unit($unit_id);
		$members['total'] = count($members['entries']);

		return $members;
	}

	public function get_membership_by_user($user_id)
	{
		// Get our entries
		// $params = array();
		// $params['stream'] = 'memberships';
		// $params['namespace'] = 'organization';
		// $params['where'] = 'membership_user = '.$user_id;
		// $params['order_by'] = 'membership_unit';
		// $params['sort'] = 'asc';

		// // Get stream entries
  //       $units = $this->CI->streams->entries->get_entries($params);
  //       dump(json_encode($units));

        $members = $this->CI->memberships_m->get_memberships_by_user($user_id);
		foreach ($members as $key => $member) {
			$new_membership['id'] = $member['membership_id'];
			$new_membership['unit_id'] = $member['unit_id'];
			$new_membership['created'] = $member['created'];
			$new_membership['updated'] = $member['updated'];
			$new_membership['membership_title'] = $member['membership_title'];
			$new_membership['membership_is_head'] = $member['membership_is_head'];
			$new_membership['membership_unit'] = array(
				'id' => $member['unit_id'],
				'unit_name' => $member['unit_name'],
				'unit_slug' => $member['unit_slug'],
				'unit_abbrevation' => $member['unit_abbrevation'],
				'unit_description' => $member['unit_description'],
				'unit_type' => $member['unit_type_id'],
				);
			$new_membership['membership_user'] = array(
				'user_id' => $member['user_id'],
				'user_email' => $member['user_email'],
				'user_username' => $member['user_username'],
				);
			unset($members[$key]);
			$members['entries'][$key] = $new_membership;
		}

		if(!isset($members['entries'])) {
			$members['entries'] = array();
		}
		$members['total'] = (isset($members['entries'])) ? count($members['entries']) : 0;
		return $members;
	}

	public function build_serial_tree($serial_tree, $units, $level = 0)
	{
		foreach($units as $unit){
			$prefix = '';
			for($i = 0; $i < $level; $i++){
				$prefix .= '';
			}

			$unit['unit_name'] = $prefix.$unit['unit_name'];

			$serial_tree[] = $unit;
			$children = $this->CI->units_m->get_units_by_parent($unit['id']);

			if(count($children) == 0){
				continue;
			}else{
				$serial_tree = $this->build_serial_tree($serial_tree, $children, $level+1);
			}
		}

		return $serial_tree;
	}
}