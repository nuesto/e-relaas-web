<div class="page-header">
	<h1><?php echo $module_details['name'] ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<?php if ($keywords): ?>
    <table class="table table-striped table-bordered table-hover" cellspacing="0">
		<thead>
			<tr>
				<th width="40%"><?php echo lang('keywords:name');?></th>
				<th width="200"></th>
			</tr>
		</thead>
		
		<tbody>
		<?php foreach ($keywords as $keyword):?>
			<tr>
				<td><?php echo $keyword->name ?></td>
				<td class="actions">
				<?php echo anchor('admin/keywords/edit/'.$keyword->id, lang('global:edit'), 'class="btn btn-xs btn-info edit"') ?>
				<?php if ( ! in_array($keyword->name, array('user', 'admin'))): ?>
					<?php echo anchor('admin/keywords/delete/'.$keyword->id, lang('global:delete'), 'class="confirm btn btn-xs btn-danger delete"') ?>
				<?php endif ?>
				</td>
			</tr>
		<?php endforeach;?>
		</tbody>
    </table>
	
	<?php $this->load->view('admin/partials/pagination') ?>

<?php else: ?>
	<div class="well"><?php echo lang('keywords:no_keywords');?></div>
<?php endif;?>