<div class="page-header">
	<h1><?php echo sprintf(lang('templates:clone_title'), $template_name) ?></h1>
</div>

<?php echo form_open(current_url(), 'class="form-horizontal"') ?>
<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('templates:choose_lang_label') ?></label>
	<div class="col-sm-10">
		<?php echo form_dropdown('lang', $lang_options) ?>
	</div>
</div>
<div class="clearfix form-actions">
	<div class="col-md-offset-1 col-md-9">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
	</div>
</div>
<?php echo form_close() ?>