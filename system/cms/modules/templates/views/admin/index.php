<?php if(!empty($templates)): ?>

<div class="col-sm-12">
	<div class="page-header">
		<h1><?php echo lang('templates:default_title') ?></h1>
		
		<?php file_partial('shortcuts'); ?>
	</div>
	
	<?php echo form_open('admin/templates/action') ?>

	<table border="0" class="table table-striped table-bordered table-hover" cellspacing="0">
		<thead>
			<tr>
				<th class="center" width="30"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
				<th><?php echo lang('name_label') ?></th>
				<th class="collapse"><?php echo lang('global:description') ?></th>
				<th class="collapse"><?php echo lang('templates:language_label') ?></th>
				<th width="220"></th>
			</tr>
		</thead>

		<tbody>
		
	<?php foreach ($templates as $template): ?>
		<?php if($template->is_default): ?>
			<tr>
				<td class="center"><?php echo form_checkbox('action_to[]', $template->id);?></td>
				<td><?php echo $template->name ?></td>
				<td class="collapse"><?php echo $template->description ?></td>
				<td class="collapse"><?php echo $template->lang ?></td>
				<td class="actions">
				<div class="buttons buttons-small align-center">
					<?php echo anchor('admin/templates/preview/' . $template->id, lang('buttons:preview'), 'target="_blank" class="btn btn-xs btn-info preview"') ?>
					<?php echo anchor('admin/templates/edit/' . $template->id, lang('buttons:edit'), 'class="btn btn-xs btn-info edit"') ?>
					<?php echo anchor('admin/templates/create_copy/' . $template->id, lang('buttons:clone'), 'class="btn btn-xs btn-info clone"') ?>
				</div>
				</td>
			</tr>
		<?php endif ?>
	<?php endforeach ?>
	</tbody>
	</table>
	<?php echo form_close() ?>
 
	<div class="table_action_buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )) ?>
	</div>
	
</div>

<div class="col-sm-12">
	<hr />
	
	<div class="page-header">
		<h1><?php echo lang('templates:user_defined_title') ?></h1>
	</div>
	
	<?php echo form_open('admin/templates/delete') ?>
		
	<table border="0" class="table table-striped table-bordered table-hover" cellspacing="0">
		<thead>
			<tr>
				<th class="center" width="30"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
				<th><?php echo lang('name_label') ?></th>
				<th><?php echo lang('global:description') ?></th>
				<th><?php echo lang('templates:language_label') ?></th>
				<th width="220"></th>
			</tr>
		</thead>

		<tbody>
	
	<?php foreach ($templates as $template): ?>
		<?php if(!$template->is_default): ?>
			<tr>
				<td class="center"><?php echo form_checkbox('action_to[]', $template->id);?></td>
				<td><?php echo $template->name ?></td>
				<td><?php echo $template->description ?></td>
				<td><?php echo $template->lang ?></td>
				<td class="actions">
				<div class="buttons buttons-small align-center">
					<?php echo anchor('admin/templates/preview/' . $template->id, lang('buttons:preview'), 'target="_blank" class="btn btn-xs btn-info preview"') ?>
					<?php echo anchor('admin/templates/edit/' . $template->id, lang('buttons:edit'), 'class="btn btn-xs btn-info edit"') ?>
					<?php echo anchor('admin/templates/delete/' . $template->id, lang('buttons:delete'), 'class="btn btn-xs btn-danger confirm delete"') ?>
				</div>
				</td>
			</tr>
		<?php endif ?>
	<?php endforeach ?>
	
	
		</tbody>
	</table>

	<div class="table_action_buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )) ?>
	</div>
	
	<?php echo form_close() ?>
</div>
	
<?php else: ?>

<div class="page-header">
	<h1><?php echo lang('templates:list_title') ?></h1>
</div>

<div class="well"><?php echo lang('templates:currently_no_templates') ?></div>

<?php endif ?>