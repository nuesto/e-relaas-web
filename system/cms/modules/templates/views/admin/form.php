<?php if($this->method == 'edit' and ! empty($email_template)): ?>
	<div class="page-header">
    	<h1><?php echo sprintf(lang('templates:edit_title'), $email_template->name) ?></h1>
	</div>
<?php else: ?>
	<div class="page-header">
    	<h1><?php echo lang('templates:create_title') ?></h1>
	</div>
<?php endif ?>


<?php echo form_open(current_url(), 'class="form-horizontal"') ?>
	
	<?php if ( ! $email_template->is_default): ?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('name_label') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('name', $email_template->name, 'id="name"') ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('templates:slug_label') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('slug', $email_template->slug, 'id="slug"') ?>
			<script>
				$('#name').slugify({ slug: '#slug', type: '-' });
			</script>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('templates:language_label') ?></label>
		<div class="col-sm-10"><?php echo form_dropdown('lang', $lang_options, array($email_template->lang)) ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('desc_label') ?> <span>*</span></label>
		<div class="col-sm-10"><?php echo form_input('description', $email_template->description) ?></div>
	</div>

	<?php endif ?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('templates:subject_label') ?> <span>*</span></label>
		<div class="col-sm-10"><?php echo form_input('subject', $email_template->subject) ?></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('templates:body_label') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_textarea('body', $email_template->body, 'class="templates wysiwyg-advanced"') ?>
		</div>
	</div>

	<div class="clearfix form-actions">
		<div class="col-md-offset-3 col-md-9">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
		</div>
	</div>
			
<?php echo form_close() ?>