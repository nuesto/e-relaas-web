<?php defined('BASEPATH') or exit('No direct script access allowed');

$lang['navigation:role_manage_menus']    = 'Manage menus';
$lang['navigation:role_manage_groups']    = 'Manage groups';