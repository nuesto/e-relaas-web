(function($) {

	$(function() {
	
		// Used by Pages and Navigation and is available for third-party add-ons.
		// Module must load jquery/jquery.ui.nestedSortable.js and jquery/jquery.cooki.js
		pyro.sort_tree = function($item_list, $url, $cookie, data_callback, post_sort_callback, sortable_opts)
		{
			// set options or create a empty object to merge with defaults
			sortable_opts = sortable_opts || {};
			
			// collapse all ordered lists but the top level
			$item_list.find('ul').children().hide();

			// this gets ran again after drop
			var refresh_tree = function() {

				// add the minus icon to all parent items that now have visible children
				$item_list.find('li:has(li:visible)').removeClass().addClass('minus');

				// add the plus icon to all parent items with hidden children
				$item_list.find('li:has(li:hidden)').removeClass().addClass('plus');
				
				// Remove any empty ul elements
				$('.plus, .minus').find('ul').not(':has(li)').remove();
				
				// remove the class if the child was removed
				$item_list.find("li:not(:has(ul li))").removeClass();

				// call the post sort callback
				post_sort_callback && post_sort_callback();
			}
			refresh_tree();

			// set the icons properly on parents restored from cookie
			$($.cookie($cookie)).has('ul').toggleClass('minus plus');

			// show the parents that were open on last visit
			$($.cookie($cookie)).children('ul').children().show();

			// show/hide the children when clicking on an <li>
			$item_list.find('li').on('click', null, function()
			{
				$(this).children('ul').children().slideToggle('fast');

				$(this).has('ul').toggleClass('minus plus');

				var items = [];

				// get all of the open parents
				$item_list.find('li.minus:visible').each(function(){ items.push('#' + this.id) });

				// save open parents in the cookie
				$.cookie($cookie, items.join(', '), { expires: 1 });

				 return false;
			});
			
			// Defaults for nestedSortable
			var default_opts = {
				delay: 100,
				disableNesting: 'no-nest',
				forcePlaceholderSize: true,
				handle: 'div',
				helper:	'clone',
				items: 'li',
				opacity: .4,
				placeholder: 'placeholder',
				tabSize: 25,
				listType: 'ul',
				tolerance: 'pointer',
				toleranceElement: '> div',
				update: function(event, ui) {

					post = {};
					// create the array using the toHierarchy method
					post.order = $item_list.nestedSortable('toHierarchy');

					// pass to third-party devs and let them return data to send along
					if (data_callback) {
						post.data = data_callback(event, ui);
					}

					// Refresh UI (no more timeout needed)
					refresh_tree();

					$.post(SITE_URL + $url, post );
				}
			};

			// init nestedSortable with options
			$item_list.nestedSortable($.extend({}, default_opts, sortable_opts));
		}
		
		pyro.chosen = function()
		{
			// Non-mobile only
			if( ! pyro.is_mobile ){

				// Chosen
				$('select:not(.skip)').ready(function(){
					$(this).addClass('chzn').addClass('hidden').trigger("liszt:updated");
					$(".chzn").chosen();

					// This is a workaround for Chosen's visibility bug. In short if a select
					// is inside a hidden element Chosen sets the width to 0. This iterates through
					// the 0 width selects and sets a fixed width.
					$('.chzn-container').each(function(i, ele){
						if ($(ele).width() == 0) {
							$(ele).css('width', '236px');
							$(ele).find('.chzn-drop').css('width', '234px');
							$(ele).find('.chzn-search input').css('width', '200px');
							$(ele).find('.search-field input').css('width', '225px');
						}
					});
				});
			}
		}

		// load edit via ajax
		$('a.ajax').on('click', null, function(){
			// make sure we load it into the right one
			var id = $(this).attr('rel');
			
			if ($(this).hasClass('add')) {
				// if we're creating a new one remove the selected icon from link in the tree
				$('.group-'+ id +' #link-list a').removeClass('selected');
			}
			// Load the form
			$('div#link-details.group-'+ id +'').load($(this).attr('href'), '', function(){
				$('div#link-details.group-'+ id +'').fadeIn();
				// display the create/edit title in the header
				var title = $('#title-value-'+id).html();
				$('section.box .title h4.group-title-'+id).html(title);
				
				// Update Chosen
				pyro.chosen();
			});
			return false;
		});

		// show link details
		$('#link-list li a').on('click', null, function()
		{
			var id = $(this).attr('rel');
			link_id = $(this).attr('alt');
			$('.group-'+ id +' #link-list a').removeClass('selected');
			$(this).addClass('selected');

			// Load the details box in
			$('div#link-details.group-'+ id +'').load(SITE_URL + 'admin/navigation/ajax_link_details/' + link_id, '', function(){
				$('div#link-details.group-'+ id +'').fadeIn();
			});
			// Remove the title from the form pane.
			$('section.box .title h4.group-title-'+id).html('');

			return false;
		});
		
		$('.box:visible ul.sortable').ready(function(){
			$item_list		= $('.box:visible ul.sortable');
			$url			= 'admin/navigation/order';
			$cookie			= 'open_links';
			$data_callback	= function(event, ui) {
				// Grab the group id so we can update the right links
				return { 'group' : ui.item.parents('section.box').attr('rel') };
			}
			// $post_callback is available but not needed here
			
			// Get sortified
			pyro.sort_tree($item_list, $url, $cookie, $data_callback);
		});

	});

})(jQuery);
