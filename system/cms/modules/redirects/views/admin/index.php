<div class="page-header">
	<h1><?php echo lang('redirects:list_title') ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<?php if ($redirects): ?>

<?php echo form_open('admin/redirects/delete') ?>
	<table border="0" class="table table-striped table-bordered table-hover" cellspacing="0">
		<thead>
			<tr>
				<th width="15"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
				<th width="25"><?php echo lang('redirects:type');?></th>
				<th width="25%"><?php echo lang('redirects:from');?></th>
				<th><?php echo lang('redirects:to');?></th>
				<th width="200"></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($redirects as $redirect): ?>
			<tr>
			<td><?php echo form_checkbox('action_to[]', $redirect->id) ?></td>
			<td><?php echo $redirect->type;?></td>
			<td><?php echo str_replace('%', '*', $redirect->from);?></td>
			<td><?php echo $redirect->to;?></td>
			<td class="align-center">
			<div class="actions">
				<?php echo anchor('admin/redirects/edit/' . $redirect->id, lang('redirects:edit'), 'class="btn btn-xs btn-info edit"');?>
				<?php echo anchor('admin/redirects/delete/' . $redirect->id, lang('redirects:delete'), array('class'=>'confirm btn btn-xs btn-danger delete'));?>
			</div>
			</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	
	<?php $this->load->view('admin/partials/pagination') ?>

	<div class="table_action_buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )) ?>
	</div>
	<?php echo form_close() ?>

<?php else: ?>
	<div class="well"><?php echo lang('redirects:no_redirects');?></div>
<?php endif ?>