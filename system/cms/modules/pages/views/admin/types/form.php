<div class="page-header">
	<?php if ($this->method == 'create'): ?>
	<h1><?php echo lang('page_types:create_title');?></h1>
	<?php else: ?>
	<h1><?php echo sprintf(lang('page_types:edit_title'), $page_type->title);?></h1>
	<?php endif; ?>
</div>

<?php echo form_open('', 'class="form-horizontal"'); ?>
	
	<div class="tabbable">
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#page-layout-basic" data-toggle="tab"><span><?php echo lang('page_types:basic_info');?></span></a>
			</li>
			<li>
				<a href="#page-layout-meta" data-toggle="tab"><span><?php echo lang('pages:meta_label');?></span></a>
			</li>
			<li>
				<a href="#page-layout-css" data-toggle="tab"><span><?php echo lang('page_types:css_label');?></span></a>
			</li>
			<li>
				<a href="#page-layout-script" data-toggle="tab"><span><?php echo lang('pages:script_label');?></span></a>
			</li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane in active" id="page-layout-basic">
		
				<fieldset>
					
					<div class="form-group">

						<label class="col-sm-2 control-label no-padding-right" for="title"><?php echo lang('global:title');?> <span>*</span></label>
						
						<div class="col-sm-10">
							<?php echo form_input('title', $page_type->title, 'id="text" maxlength="60"'); ?>
						</div>
						
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:slug');?> <span>*</span></label>
						
						<div class="col-sm-10">
						<?php if ($this->method == 'create'): ?>
						
						<?php echo form_input('slug', $page_type->slug, 'id="slug" maxlength="60"'); ?>
						<script>
							$('#text').slugify({ slug: '#slug', type: '-' });
						</script>
						
						<?php else: ?>
						
						<em><?php echo $page_type->slug; ?></em>
						
						<?php endif; ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:description');?></label>
						<div class="col-sm-10"><?php echo form_input('description', $page_type->description, 'id="description"'); ?></div>
					</div>

					<?php if ($this->method == 'edit'): ?>
						<?php echo form_hidden('old_slug', $page_type->slug); ?>
					<?php endif; ?>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right"><?php echo lang('page_types:theme_layout_label');?> <span>*</span></label>
						<div class="col-sm-10"><?php echo form_dropdown('theme_layout', $theme_layouts, $page_type->theme_layout ? $page_type->theme_layout : 'default'); ?></div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right"><?php echo lang('page_types:save_as_files');?></label>
						<div class="col-sm-10">
							<?php echo form_checkbox('save_as_files', 'y', $page_type->save_as_files == 'y' ? true : false, 'id="save_as_files"'); ?>
							<br><small><?php echo lang('page_types:saf_instructions'); ?></small>
						</div>
					</div>
					
				</fieldset>
			
			</div>

			<div class="tab-pane" id="page-layout-layout">
		
				<fieldset>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right"><?php echo lang('page_types:layout'); ?> <span>*</span></label>
						<div class="col-sm-10">
							<?php echo form_textarea(array('id'=>'html_editor', 'name'=>'body', 'value' => ($page_type->body == '' ? '<h2>{{ title }}</h2>' : $page_type->body), 'rows' => 50, 'class' => 'form-control')); ?>
						</div>
					</div>

				</fieldset>

			</div>

			<!-- Meta data tab -->
			<div class="tab-pane" id="page-layout-meta">
			
				<fieldset>
			
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:meta_title_label');?></label>
						<div class="col-sm-10"><input type="text" id="meta_title" name="meta_title" maxlength="255" value="<?php echo $page_type->meta_title; ?>" /></div>
					</div>
									
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:meta_keywords_label');?></label>
						<div class="col-sm-10"><input type="text" id="meta_keywords" name="meta_keywords" maxlength="255" value="<?php echo $page_type->meta_keywords; ?>" /></div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:meta_desc_label');?></label>
						<div class="col-sm-10"><?php echo form_textarea(array('name' => 'meta_description', 'value' => $page_type->meta_description, 'rows' => 5)); ?></div>
					</div>
				</ul>
				
				</fieldset>

			</div>

			
			<!-- Design tab -->
			<div class="tab-pane" id="page-layout-css">
				
				<fieldset>
			
				<div class="form-group">
					<label class="col-sm-2 control-label no-padding-right"><?php echo lang('page_types:css_label'); ?></label>
					<div class="col-sm-10"><?php echo form_textarea('css', $page_type->css, 'class="css_editor" id="css"'); ?></div>
				</div>
				
				</fieldset>
				
			</div>
			
			<!-- Script tab -->
			<div class="tab-pane" id="page-layout-script">

				<fieldset>

				<div class="form-group">
					<label class="col-sm-2 control-label no-padding-right">JavaScript</label>
					<div class="col-sm-10"><?php echo form_textarea('js', $page_type->js, 'class="js_editor" id="js"'); ?></div>
				</div>

				</fieldset>

			</div>
		</div>
	</div>
	
	<div class="clearfix form-actions">
		<div class="col-md-offset-2 col-md-10">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )); ?>
		</div>
	</div>

<?php echo form_close(); ?>