<div class="page-header">
	<?php if ($this->method == 'create'): ?>
		<h1><?php echo lang('pages:create_title') ?></h1>
	<?php else: ?>
		<h1><?php echo sprintf(lang('pages:edit_title'), $page->title) ?></h1>
	<?php endif ?>
</div>

<?php $parent = ($parent_id) ? '&parent='.$parent_id : null ?>

<?php echo form_open_multipart(uri_string().'?page_type='.$this->input->get('page_type').$parent, 'id="page-form" data-mode="'.$this->method.'" class="form-horizontal"') ?>

<?php echo form_hidden('parent_id', empty($page->parent_id) ? 0 : $page->parent_id) ?>

<div class="tabs">

	<!--
	<ul class="tab-menu">
		<li><a href="#page-details"><span><?php echo lang('pages:details_label') ?></span></a></li>
		<?php if ($stream_fields): ?><li><a href="#page-content"><span><?php if ($page_type->content_label): echo lang_label($page_type->content_label); else: echo lang('pages:content_label'); endif ?></span></a></li><?php endif ?>
		<li><a href="#page-meta"><span><?php echo lang('pages:meta_label') ?></span></a></li>
		<li><a href="#page-design"><span><?php echo lang('pages:css_label') ?></span></a></li>
		<li><a href="#page-script"><span><?php echo lang('pages:script_label') ?></span></a></li>
		<li><a href="#page-options"><span><?php echo lang('pages:options_label') ?></span></a></li>
	</ul>
	-->

	<div class="form_inputs" id="page-details">
	
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php if ($page_type->title_label): echo lang_label($page_type->title_label); else: echo lang('global:title'); endif ?> <span>*</span></label>
			<?php echo form_hidden('title', $page->title); ?>
			<label class="col-sm-2"><?php echo $page->title; ?></label>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:slug') ?>  <span>*</span></label>
			
			<label class="col-sm-2"><?php echo $page->slug; ?></label>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:status_label') ?></label>
			
			<label class="col-sm-2"><?php echo $page->status; ?></label>
		</div>
		
	</div>

	<?php if ($stream_fields): ?>
	
	<!-- Content tab -->
	<div class="form_inputs" id="page-content">

		<?php foreach ($stream_fields as $field) echo $this->load->view('admin/partials/streams/form_single_display', array('field' => $field), true) ?>
	
	</div>

	<?php endif ?>

</div>

<input type="hidden" name="row_edit_id" value="<?php if ($this->method != 'create'): echo $page->entry_id; endif; ?>" />

<div class="col-sm-offset-2 col-sm-10">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )) ?>
</div>

<?php echo form_close() ?>