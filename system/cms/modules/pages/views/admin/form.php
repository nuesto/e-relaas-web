<?php Asset::js('ckeditor/ckeditor.js', 'wysiwyg'); ?>
<?php Asset::js('ckeditor/adapters/jquery.js', 'wysiwyg'); ?>
<?php echo Asset::render_js('wysiwyg') ?>

<script type="text/javascript">

	var instance;

	function update_instance()
	{
		instance = CKEDITOR.currentInstance;
	}

	(function($) {
		$(function(){

			pyro.init_ckeditor = function(){
				<?php echo $this->parser->parse_string(Settings::get('ckeditor_config'), $this, TRUE); ?>
			};
			pyro.init_ckeditor();

		});
	})(jQuery);
</script>
<div class="page-header">
	<?php if ($this->method == 'create'): ?>
		<h1><?php echo lang('pages:create_title') ?></h1>
	<?php else: ?>
		<h1><?php echo sprintf(lang('pages:edit_title'), $page->title) ?></h1>
	<?php endif ?>
</div>

<?php $parent = ($parent_id) ? '&parent='.$parent_id : null ?>

<?php echo form_open_multipart(uri_string().'?page_type='.$this->input->get('page_type').$parent, 'id="page-form" data-mode="'.$this->method.'" class="form-horizontal"') ?>

<?php echo form_hidden('parent_id', empty($page->parent_id) ? 0 : $page->parent_id) ?>

<div class="tabs">

	<!--
	<ul class="tab-menu">
		<li><a href="#page-details"><span><?php echo lang('pages:details_label') ?></span></a></li>
		<?php if ($stream_fields): ?><li><a href="#page-content"><span><?php if ($page_type->content_label): echo lang_label($page_type->content_label); else: echo lang('pages:content_label'); endif ?></span></a></li><?php endif ?>
		<li><a href="#page-meta"><span><?php echo lang('pages:meta_label') ?></span></a></li>
		<li><a href="#page-design"><span><?php echo lang('pages:css_label') ?></span></a></li>
		<li><a href="#page-script"><span><?php echo lang('pages:script_label') ?></span></a></li>
		<li><a href="#page-options"><span><?php echo lang('pages:options_label') ?></span></a></li>
	</ul>
	-->
	<?php
		$supported_lang = get_supported_lang();
		foreach ($supported_lang as $sl) {
			$tmp_lang = explode("=", $sl);
			$lang_list[$tmp_lang[0]] = $tmp_lang[1];
		}
		$public_lang = explode(",", $this->settings->get('site_public_lang'));
		array_unshift($public_lang, $this->settings->get('site_lang'));
		$public_lang = array_unique($public_lang);
		foreach ($public_lang as $pl) {
			$available_lang[] = array('code'=>$pl,'name'=>$lang_list[$pl]);
		}

		echo form_hidden('available_lang',implode(",", $public_lang));
	?>
	<?php
	foreach ($available_lang as $key => $al) { 
		$lang_code = '';
		if(count($available_lang)>1) {
			echo '<h2>'.$al['name'].'</h2>';
			$lang_code = '_'.$al['code'];
		}
		if(isset($page->translation[$al['code']])) {
			$trans_detail = json_decode($page->translation[$al['code']]->translation);
			$trans_body = json_decode($page->translation[$al['code']]->field_translation);
			if($this->input->post('title'.$lang_code)) {
				$v = $this->input->post('title'.$lang_code);
			} elseif (isset($trans_detail->{'title_'.$al['code']})) {
				$v = $trans_detail->{'title_'.$al['code']};
			} else {
				$v = '';
			}
			$page->title = $v;
			
			if($this->input->post('slug'.$lang_code)) {
				$v = $this->input->post('slug'.$lang_code);
			} elseif (isset($trans_detail->{'slug_'.$al['code']})) {
				$v = $trans_detail->{'slug_'.$al['code']};
			} else {
				$v = '';
			}
			$page->slug = $v;

			if($this->input->post('body'.$lang_code)) {
				$v = $this->input->post('body'.$lang_code);
			} elseif (isset($trans_body->{'body_'.$al['code']})) {
				$v = $trans_body->{'body_'.$al['code']};
			} else {
				$v = '';
			}
			$page->body = $v;
		} else {
			$page->title = ($this->input->post('title'.$lang_code)) ? $this->input->post('title'.$lang_code) : '';
			$page->slug = ($this->input->post('slug'.$lang_code)) ? $this->input->post('slug'.$lang_code) : '';
			$page->body = ($this->input->post('body'.$lang_code)) ? $this->input->post('body'.$lang_code) : '';
		}
	?>
	<div class="form_inputs" id="page-details">
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php if ($page_type->title_label): echo lang_label($page_type->title_label); else: echo lang('global:title'); endif ?> <span>*</span></label>
			
			<div class="col-sm-10"><?php echo form_input('title'.$lang_code, $page->title, 'id="title'.$lang_code.'" maxlength="60" class="form-control"') ?></div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:slug') ?>  <span>*</span></label>
			
			<div class="col-sm-10">
			<?php $value = ($key==0) ? $page->slug : $page->slug; ?>
			<?php if ( ! empty($page->parent_id)): ?>
				<?php echo site_url($parent_page->uri) ?>/
			<?php else: ?>
				<?php echo site_url() . (config_item('index_page') ? '/' : '') ?>
			<?php endif ?>

			<?php if ($this->method == 'edit'): ?>
				<?php echo form_hidden('old_slug', $page->slug) ?>
			<?php endif ?>

			<?php if (in_array($page->slug, array('home', '404'))): ?>
				<?php echo form_hidden('slug'.$lang_code, $page->slug) ?>
				<?php echo form_input('', $page->slug, 'id="slug'.$lang_code.'" size="20" disabled="disabled" class="form-control"') ?>
			<?php else: ?>
				<?php echo form_input('slug'.$lang_code, $page->slug, 'id="slug'.$lang_code.'" size="20" class="'.($this->method == 'edit' ? ' disabled' : '').'"') ?>
				
				<script>
					$('#title<?php echo $lang_code; ?>').slugify({ slug: '#slug<?php echo $lang_code; ?>', type: '-' });
				</script>
			<?php endif ?>

			<?php echo config_item('url_suffix') ?>
			
			</div>
		</div>

	</div>

	<!-- Content tab -->
	<div class="form_inputs" id="page-content">
		<div class="form-group">

			<label for="body" class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:body'); ?> </label>

			<div class="col-sm-10">
				<?php echo form_textarea('body'.$lang_code,(isset($page->body)) ? htmlspecialchars_decode($page->body) : '','cols="40" rows="10" class="wysiwyg-advanced form-control" id="body"'); ?>
			</div>
		</div>
	</div>

	<?php } ?>

	<?php
	if(count($available_lang)>1) {
		echo '<h2>General</h2>';
	}
	?>

	<div class="form_inputs" id="page-details-2">
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:status_label') ?></label>
			
			<div class="col-sm-10"><?php echo form_dropdown('status', array('draft'=>lang('pages:draft_label'), 'live'=>lang('pages:live_label')), $page->status, 'id="category_id" class="form-control"') ?></div>
		</div>
		
		<?php if ($this->method == 'create'): ?>
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:navigation_label') ?></label>
			
			<div class="col-sm-10"><?php echo form_multiselect('navigation_group_id[]', array(lang('global:select-none')) + $navigation_groups, $page->navigation_group_id) ?></div>
		</div>
		<?php endif ?>
	</div>

	<!-- Meta data tab -->
	<div class="form_inputs" id="page-meta">
		
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:meta_title_label') ?></label>
			
			<div class="col-sm-10"><input type="text" id="meta_title" name="meta_title" maxlength="255" value="<?php echo $page->meta_title ?>" class="form-control" /></div>
		</div>
						
		<?php if ( ! module_enabled('keywords')): ?>
			<?php echo form_hidden('keywords'); ?>
		<?php else: ?>
		
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:meta_keywords_label') ?></label>
			
			<div class="col-sm-10">
				<?php echo form_textarea(array('id' => 'meta_keywords', 'name' => 'meta_keywords', 'value' => $page->meta_keywords, 'rows' => 2, 'class' => 'form_control')); ?>
				<script>
					$(document).ready(function(){
						var tag_input = $('#keywords');
						// make tag input field
						tag_input.tag(
						  {
							placeholder:tag_input.attr('placeholder'),
							//enable typeahead by specifying the source array
							source: function(query, process){
								return $.getJSON('<?php echo base_url(); ?>admin/keywords/autocomplete', { query: query }, function (data) {
									return process(data);
								});
							}
						  }
						);
					});
				</script>
			</div>
		</div>
		
		<?php endif; ?>
		
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:meta_robots_no_index_label') ?></label>
			
			<div class="col-sm-10"><?php echo form_checkbox('meta_robots_no_index', true, $page->meta_robots_no_index == true, 'id="meta_robots_no_index"') ?></div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:meta_robots_no_follow_label') ?></label>
			
			<div class="col-sm-10"><?php echo form_checkbox('meta_robots_no_follow', true, $page->meta_robots_no_follow == true, 'id="meta_robots_no_follow"') ?></div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:meta_desc_label') ?></label>
			
			<div class="col-sm-10">
				<?php echo form_textarea(array('name' => 'meta_description', 'value' => $page->meta_description, 'rows' => 5, 'class' => 'form-control')) ?>
			</div>
		</div>

	</div>

	<!-- Design tab -->
	<div class="form_inputs" id="page-design">

		
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:css_label') ?></label>
			<div class="col-sm-10">
				<?php echo form_textarea('css', $page->css, 'class="css_editor form-control"') ?>
			</div>
		</div>
		
	</div>

	<!-- Script tab -->
	<div class="form_inputs" id="page-script">
	
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:js_label') ?></label>
			<div class="col-sm-10">
				<?php echo form_textarea('js', $page->js, 'class="js_editor form-control"') ?>
			</div>
		</div>

	</div>

	<!-- Options tab -->
	<div class="form_inputs" id="page-options">
	
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:access_label') ?></label>
			
			<div class="col-sm-10"><?php echo form_multiselect('restricted_to[]', array(0 => lang('global:select-any')) + $group_options, $page->restricted_to, 'size="'.(($count = count($group_options)) > 1 ? $count : 2).'"') ?></div>
		</div>
						
		<?php if ( ! module_enabled('comments')): ?>
			<?php echo form_hidden('comments_enabled'); ?>
		<?php else: ?>
		
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:comments_enabled_label') ?></label>
			
			<div class="col-sm-10"><?php echo form_checkbox('comments_enabled', true, $page->comments_enabled == true, 'id="comments_enabled"') ?></div>
		</div>
		
		<?php endif; ?>
						
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:rss_enabled_label') ?></label>
			
			<div class="col-sm-10"><?php echo form_checkbox('rss_enabled', true, $page->rss_enabled == true, 'id="rss_enabled"') ?></div>
		</div>
						
		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:is_home_label') ?></label>
			
			<div class="col-sm-10"><?php echo form_checkbox('is_home', true, $page->is_home == true, 'id="is_home"') ?></div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label no-padding-right"><?php echo lang('pages:strict_uri_label') ?></label>
			
			<div class="col-sm-10"><?php echo form_checkbox('strict_uri', 1, $page->strict_uri == true, 'id="strict_uri"') ?></div>
		</div>

	</div>

</div>

<input type="hidden" name="row_edit_id" value="<?php if ($this->method != 'create'): echo $page->entry_id; endif; ?>" />

<div class="col-sm-offset-2 col-sm-10">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel') )) ?>
</div>

<?php echo form_close() ?>