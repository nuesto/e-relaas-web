<?php defined('BASEPATH') or exit('No direct script access allowed');

// Page Permissions
$lang['pages:role_put_live']    = 'Put pages live';
$lang['pages:role_create_live']   = 'Create live pages';
$lang['pages:role_edit_live']   = 'Edit live pages';
$lang['pages:role_delete_live'] = 'Delete live articles';
$lang['pages:role_administrator_pages'] = 'Administrator Pages';

// Page Type Permissions
$lang['pages:role_create_types'] = 'Create Types';
$lang['pages:role_edit_types']   = 'Edit Types';
$lang['pages:role_delete_types'] = 'Delete Types';
$lang['pages:role_administrator_types'] = 'Administrator Types';