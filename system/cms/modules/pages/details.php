<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Pages Module
 *
 * @author PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Pages
 */
class Module_Pages extends Module
{
	public $version = '2.5.3';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Pages',
				'ar' => 'الصفحات',
				'br' => 'Páginas',
				'pt' => 'Páginas',
				'cs' => 'Stránky',
				'da' => 'Sider',
				'de' => 'Seiten',
				'el' => 'Σελίδες',
				'es' => 'Páginas',
                            'fa' => 'صفحه ها ',
				'fi' => 'Sivut',
				'fr' => 'Pages',
				'he' => 'דפים',
				'id' => 'Halaman',
				'it' => 'Pagine',
				'lt' => 'Puslapiai',
				'nl' => 'Pagina&apos;s',
				'pl' => 'Strony',
				'ru' => 'Страницы',
				'sl' => 'Strani',
				'tw' => '頁面',
				'cn' => '页面',
				'hu' => 'Oldalak',
				'th' => 'หน้าเพจ',
            	'se' => 'Sidor',
			),
			'description' => array(
				'en' => 'Add custom pages to the site with any content you want.',
				'ar' => 'إضافة صفحات مُخصّصة إلى الموقع تحتوي أية مُحتوى تريده.',
				'br' => 'Adicionar páginas personalizadas ao site com qualquer conteúdo que você queira.',
				'pt' => 'Adicionar páginas personalizadas ao seu site com qualquer conteúdo que você queira.',
				'cs' => 'Přidávejte vlastní stránky na web s jakýmkoliv obsahem budete chtít.',
				'da' => 'Tilføj brugerdefinerede sider til dit site med det indhold du ønsker.',
				'de' => 'Füge eigene Seiten mit anpassbaren Inhalt hinzu.',
				'el' => 'Προσθέστε και επεξεργαστείτε σελίδες στον ιστότοπό σας με ό,τι περιεχόμενο θέλετε.',
				'es' => 'Agrega páginas customizadas al sitio con cualquier contenido que tu quieras.',
                            'fa' => 'ایحاد صفحات جدید و دلخواه با هر محتوایی که دوست دارید',
				'fi' => 'Lisää mitä tahansa sisältöä sivustollesi.',
				'fr' => "Permet d'ajouter sur le site des pages personalisées avec le contenu que vous souhaitez.",
				'he' => 'ניהול דפי תוכן האתר',
				'id' => 'Menambahkan halaman ke dalam situs dengan konten apapun yang Anda perlukan.',
				'it' => 'Aggiungi pagine personalizzate al sito con qualsiesi contenuto tu voglia.',
				'lt' => 'Pridėkite nuosavus puslapius betkokio turinio',
				'nl' => "Voeg aangepaste pagina&apos;s met willekeurige inhoud aan de site toe.",
				'pl' => 'Dodaj własne strony z dowolną treścią do witryny.',
				'ru' => 'Управление информационными страницами сайта, с произвольным содержимым.',
				'sl' => 'Dodaj stran s kakršno koli vsebino želite.',
				'tw' => '為您的網站新增自定的頁面。',
				'cn' => '为您的网站新增自定的页面。',
				'th' => 'เพิ่มหน้าเว็บที่กำหนดเองไปยังเว็บไซต์ของคุณตามที่ต้องการ',
				'hu' => 'Saját oldalak hozzáadása a weboldalhoz, akármilyen tartalommal.',
            	'se' => 'Lägg till egna sidor till webbplatsen.',
			),
			'frontend' => true,
			'backend'  => true,
			'skip_xss' => true,
			'menu'	  => 'content',

			'roles' => array(
				'put_live', 'create_live', 'edit_live', 'delete_live', 'administrator_pages',
                'create_types', 'edit_types', 'delete_types', 'administrator_types'
			),
		);

		if(group_has_role('pages','put_live')
			OR group_has_role('pages','create_live')
			OR group_has_role('pages','edit_live')
			OR group_has_role('pages','delete_live')
			OR group_has_role('pages','administrator_pages')) {

			$info['sections']['pages']['name'] = 'pages:list_title';
			$info['sections']['pages']['uri'] = 'admin/pages/index';
		}

		if(group_has_role('pages','create_types')
			OR group_has_role('pages','edit_types')
			OR group_has_role('pages','delete_types')
			OR group_has_role('pages','administrator_types')) {

			$info['sections']['types']['name'] = 'page_types:list_title';
			$info['sections']['types']['uri'] = 'admin/pages/types';
		}

		// We check that the table exists (only when in the admin controller)
		// to avoid any pre 109 migration module class spawning issues.
		if (class_exists('Admin_controller') and $this->db->table_exists('page_types'))
		{
			// Shortcuts for New page

			// Do we have more than one page type? If we don't, no need to have a modal
			// ask them to choose a page type.
			if ($this->db->count_all('page_types') > 1)
			{
				if(group_has_role('pages','put_live')
					OR group_has_role('pages','create_live')
					OR group_has_role('pages','edit_live')
					OR group_has_role('pages','delete_live')
					OR group_has_role('pages','administrator_pages')) {

					$info['sections']['pages']['shortcuts'] = array(
						array(
							'name' => 'pages:create_title',
							'uri' => 'admin/pages/choose_type',
							'class' => 'add modal'
						)
					);
				}
			}
			else
			{
				if(group_has_role('pages','put_live')
					OR group_has_role('pages','create_live')
					OR group_has_role('pages','edit_live')
					OR group_has_role('pages','delete_live')
					OR group_has_role('pages','administrator_pages')) {

					// Get the one page type.
					$page_type = $this->db->limit(1)->select('id')->get('page_types')->row();

					$info['sections']['pages']['shortcuts'] = array(
						array(
							'name' => 'pages:create_title',
							'uri' => 'admin/pages/create?page_type='.$page_type->id,
							'class' => 'add'
						)
					);
				}
			}

			// Show the correct +Add button based on the page
			if ($this->uri->segment(4) == 'fields' and $this->uri->segment(5))
			{
				if(group_has_role('pages','create_types')
					OR group_has_role('pages','edit_types')
					OR group_has_role('pages','delete_types')
					OR group_has_role('pages','administrator_types')) {

					$info['sections']['types']['shortcuts'] = array(
								array(
								    'name' => 'streams:new_field',
								    'uri' => 'admin/pages/types/fields/'.$this->uri->segment(5).'/new_field',
								    'class' => 'add'
								)
						    );
				}
			}
			else
			{
				if(group_has_role('pages','create_types')
					OR group_has_role('pages','edit_types')
					OR group_has_role('pages','delete_types')
					OR group_has_role('pages','administrator_types')) {

					$info['sections']['types']['shortcuts'] = array(
								array(
								    'name' => 'pages:types_create_title',
								    'uri' => 'admin/pages/types/create',
								    'class' => 'add'
								)
							);
				}
			}
		}

		return $info;
	}

	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items){
		
	}

	public function install()
	{
		$this->dbforge->drop_table('page_types');
		$this->dbforge->drop_table('pages');

		// Just in case. If this is a new install, we
		// definiitely should not have a page_chunks table.
		$this->dbforge->drop_table('page_chunks');

		// We only need to do this if the def_page_fields table
		// has already been added.
		if ($this->db->table_exists('def_page_fields'))
		{
			$this->load->driver('Streams');
			$this->streams->utilities->remove_namespace('pages');
			$this->dbforge->drop_table('def_page_fields');
		}

		if ($this->db->table_exists('data_streams'))
		{
			$this->db->where('stream_namespace', 'pages')->delete('data_streams');
		}

		$this->load->helper('date');
		$this->load->config('pages/pages');

		$tables = array(
			'page_types' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => ''),
				'title' => array('type' => 'VARCHAR', 'constraint' => 60),
				'description' => array('type' => 'TEXT', 'null' => true),
				'stream_id' => array('type' => 'INT', 'constraint' => 11),
				'meta_title' => array('type' => 'VARCHAR', 'constraint' => 255, 'null' => true),
				'meta_keywords' => array('type' => 'CHAR', 'constraint' => 32, 'null' => true),
				'meta_description' => array('type' => 'TEXT', 'null' => true),
				'body' => array('type' => 'TEXT'),
				'css' => array('type' => 'TEXT', 'null' => true),
				'js' => array('type' => 'TEXT', 'null' => true),
				'theme_layout' => array('type' => 'VARCHAR', 'constraint' => 100, 'default' => 'default'),
				'updated_on' => array('type' => 'INT', 'constraint' => 11),
	            'save_as_files'     => array('type' => 'CHAR', 'constraint' => 1, 'default' => 'n'),
	            'content_label'     => array('type' => 'VARCHAR', 'constraint' => 60, 'null' => true),
	            'title_label'     => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => true)
			),
			'pages' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => '', 'key' => 'slug'),
				'class' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => ''),
				'title' => array('type' => 'VARCHAR', 'constraint' => 255, 'default' => ''),
				'uri' => array('type' => 'TEXT', 'null' => true),
				'parent_id' => array('type' => 'INT', 'constraint' => 11, 'default' => 0, 'key' => 'parent_id'),
				'type_id' => array('type' => 'VARCHAR', 'constraint' => 255),
				'entry_id' => array('type' => 'VARCHAR', 'constraint' => 255, 'null' => true),
				'css' => array('type' => 'TEXT', 'null' => true),
				'js' => array('type' => 'TEXT', 'null' => true),
				'meta_title' => array('type' => 'VARCHAR', 'constraint' => 255, 'null' => true),
				'meta_keywords' => array('type' => 'CHAR', 'constraint' => 32, 'null' => true),
				'meta_robots_no_index' => array('type' => 'TINYINT', 'constraint' => 1, 'null' => true),
				'meta_robots_no_follow' => array('type' => 'TINYINT', 'constraint' => 1, 'null' => true),
				'meta_description' => array('type' => 'TEXT', 'null' => true),
				'rss_enabled' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),
				'pages_enabled' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),
				'status' => array('type' => 'ENUM', 'constraint' => array('draft', 'live'), 'default' => 'draft'),
				'created_on' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'updated_on' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'restricted_to' => array('type' => 'VARCHAR', 'constraint' => 255, 'null' => true),
				'is_home' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),
				'strict_uri' => array('type' => 'TINYINT', 'constraint' => 1, 'default' => 1),
				'order' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
			),
		);

		if ( ! $this->install_tables($tables))
		{
			return false;
		}

		$this->load->driver('streams');

		$stream_id = $this->streams->streams->add_stream(
			'Default',
			'def_page_fields',
			'pages',
			null,
			'A simple page type with a WYSIWYG editor that will get you started adding content.'
		);

		// add the fields to the streams
		$this->streams->fields->add_fields(config_item('pages:default_fields'));

		// Insert the page type structures
		$page_type = 	array(
			'id' => 1,
			'title' => 'Default',
			'slug' => 'default',
			'description' => 'A simple page type with a WYSIWYG editor that will get you started adding content.',
			'stream_id' => $stream_id,
			'body' => '<div class="page-header"><h1><span>{{ page:title }}</span></h1></div>'."\n\n".'{{ body }}',
			'css' => '',
			'js' => '',
			'updated_on' => now()
		);

		if ( ! $this->db->insert('page_types', $page_type))
		{
			return false;
		}

		$def_page_type_id = $this->db->insert_id();

		$page_content = config_item('pages:default_page_content');
		$page_entries = array(
			'home' => array(
				'slug' => 'home',
				'title' => 'Home',
				'uri' => 'home',
				'parent_id' => 0,
				'type_id' => $def_page_type_id,
				'status' => 'live',
				'restricted_to' => '',
				'created_on' => now(),
				'is_home' => true,
				'order' => now()
			),
			'contact' => array(
				'slug' => 'contact',
				'title' => 'Contact',
				'uri' => 'contact',
				'parent_id' => 0,
				'type_id' => $def_page_type_id,
				'status' => 'live',
				'restricted_to' => '',
				'created_on' => now(),
				'is_home' => false,
				'order' => now()
			),
			'search' => array(
				'slug' => 'search',
				'title' => 'Search',
				'uri' => 'search',
				'parent_id' => 0,
				'type_id' => $def_page_type_id,
				'status' => 'live',
				'restricted_to' => '',
				'created_on' => now(),
				'is_home' => false,
				'order' => now()
			),
			'search-results' => array(
				'slug' => 'results',
				'title' => 'Results',
				'uri' => 'search/results',
				'parent_id' => 3,
				'type_id' => $def_page_type_id,
				'status' => 'live',
				'restricted_to' => '',
				'created_on' => now(),
				'is_home' => false,
				'strict_uri' => false,
				'order' => now()
			),
			'fourohfour' => array(
				'slug' => '404',
				'title' => 'Page missing',
				'uri' => '404',
				'parent_id' => 0,
				'type_id' => $def_page_type_id,
				'status' => 'live',
				'restricted_to' => '',
				'created_on' => now(),
				'is_home' => false,
				'order' => now()
			)
		);

		foreach ($page_entries as $key => $d)
		{
			// Contact Page
			$this->db->insert('pages', $d);
			$page_id = $this->db->insert_id();

			$this->db->insert('def_page_fields', $page_content[$key]);
			$entry_id = $this->db->insert_id();

			$this->db->where('id', $page_id);
			$this->db->update('pages', array('entry_id' => $entry_id));

			unset($page_id);
			unset($entry_id);
		}

		// add table translation for pages
				if(!$this->db->table_exists('pages_translation')) {
					$fields = array(
						'id_pages' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
						),
						'lang_code' => array(
							'type' => 'VARCHAR',
							'constraint' => 10,
							'null' => FALSE,
						),
						'lang_name' => array(
							'type' => 'VARCHAR',
							'constraint' => 100,
							'null' => FALSE,
						),
						'translation' => array(
							'type' => 'TEXT',
							'null' => TRUE,
						),
					);
					$this->dbforge->add_field($fields);
					$this->dbforge->create_table('pages_translation', TRUE);
					$this->db->query("CREATE INDEX page_trans_idx ON ".$this->db->dbprefix('pages_translation')."(id_pages)");
				}

				// add table translation for pages field
				if(!$this->db->table_exists('def_page_fields_translation')) {
					$fields = array(
						'id_page_fields' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
						),
						'lang_code' => array(
							'type' => 'VARCHAR',
							'constraint' => 10,
							'null' => FALSE,
						),
						'lang_name' => array(
							'type' => 'VARCHAR',
							'constraint' => 100,
							'null' => FALSE,
						),
						'translation' => array(
							'type' => 'TEXT',
							'null' => TRUE,
						),
					);
					$this->dbforge->add_field($fields);
					$this->dbforge->create_table('def_page_fields_translation', TRUE);
					$this->db->query("CREATE INDEX page_field_trans_idx ON ".$this->db->dbprefix('def_page_fields_translation')."(id_page_fields)");
				}

		return true;
	}

	public function uninstall()
	{
		// This is a core module, lets keep it around.
		return false;
	}

	public function upgrade($old_version)
	{
		switch ($old_version) {
			case '2.2.0':
				// Upgrade to be able manage multilanguage pages

				// add table translation for pages
				if(!$this->db->table_exists('pages_translation')) {
					$fields = array(
						'id_pages' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
						),
						'lang_code' => array(
							'type' => 'VARCHAR',
							'constraint' => 10,
							'null' => FALSE,
						),
						'lang_name' => array(
							'type' => 'VARCHAR',
							'constraint' => 100,
							'null' => FALSE,
						),
						'translation' => array(
							'type' => 'TEXT',
							'null' => TRUE,
						),
					);
					$this->dbforge->add_field($fields);
					$this->dbforge->create_table('pages_translation', TRUE);
					$this->db->query("CREATE INDEX page_trans_idx ON ".$this->db->dbprefix('pages_translation')."(id_pages)");
				}

				// add table translation for pages field
				if(!$this->db->table_exists('def_page_fields_translation')) {
					$fields = array(
						'id_page_fields' => array(
							'type' => 'INT',
							'constraint' => 11,
							'unsigned' => TRUE,
						),
						'lang_code' => array(
							'type' => 'VARCHAR',
							'constraint' => 10,
							'null' => FALSE,
						),
						'lang_name' => array(
							'type' => 'VARCHAR',
							'constraint' => 100,
							'null' => FALSE,
						),
						'translation' => array(
							'type' => 'TEXT',
							'null' => TRUE,
						),
					);
					$this->dbforge->add_field($fields);
					$this->dbforge->create_table('def_page_fields_translation', TRUE);
					$this->db->query("CREATE INDEX page_field_trans_idx ON ".$this->db->dbprefix('def_page_fields_translation')."(id_page_fields)");
				}

				// stop at this version
				// please upgrade again to continue
				$this->version ='2.3.0';
				break;

			case '2.3.0':
				// convert all tables to InnoDB engine
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages')." ENGINE=InnoDB");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages_translation')." ENGINE=InnoDB");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('page_layouts')." ENGINE=InnoDB");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('page_types')." ENGINE=InnoDB");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('def_page_fields')." ENGINE=InnoDB");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('def_page_fields_translation')." ENGINE=InnoDB");

				// stop at this version
				// please upgrade again to continue
				$this->version ='2.4.0';
				break;

			case '2.4.0':
				// ------------------------------------------
				// CREATE FK FOR `pages`.`type_id`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				$existing_idx = $this->db->query("SHOW INDEX FROM ".$this->db->dbprefix('pages')." WHERE KEY_NAME = 'pages_type_id_idx'")->result();

				if(count($existing_idx) == 0){
					$this->db->query("CREATE INDEX `pages_type_id_idx` ON ".$this->db->dbprefix('pages')."(`type_id`)");
				}

				// Step 2 - delete all child rows that have no match FK reference in parent row
				$this->db->select('id');
				$existing_type_ids = $this->db->get('page_types')->result();

				$existing_type_ids_arr = array();
				foreach ($existing_type_ids as $existing_type_id) {
					$existing_type_ids_arr[] = (int)$existing_type_id->id;
				}

				if(count($existing_type_ids_arr) > 0){
					$this->db->where_not_in('type_id', $existing_type_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('pages');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('page_types')." MODIFY `id` INT(11) UNSIGNED");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages')." MODIFY `type_id` INT(11) UNSIGNED NULL DEFAULT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages')." ADD CONSTRAINT `fk_pages_type_id` FOREIGN KEY(`type_id`) REFERENCES ".$this->db->dbprefix('page_types')."(`id`)");

				// ------------------------------------------
				// CREATE FK FOR `pages`.`entry_id`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				$existing_idx = $this->db->query("SHOW INDEX FROM ".$this->db->dbprefix('pages')." WHERE KEY_NAME = 'entry_id_idx'")->result();

				if(count($existing_idx) == 0){
					$this->db->query("CREATE INDEX `entry_id_idx` ON ".$this->db->dbprefix('pages')."(`entry_id`)");
				}

				// Step 2 - delete all child rows that have no match FK reference in parent row
				$this->db->select('id');
				$existing_entry_ids = $this->db->get('def_page_fields')->result();

				$existing_entry_ids_arr = array();
				foreach ($existing_entry_ids as $existing_entry_id) {
					$existing_entry_ids_arr[] = (int)$existing_entry_id->id;
				}

				if(count($existing_entry_ids_arr) > 0){
					$this->db->where_not_in('entry_id', $existing_entry_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->where('entry_id IS NOT NULL', NULL, false); // `entry_id` is allowed to have NULL value
				$this->db->delete('pages');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('def_page_fields')." MODIFY `id` INT(11) UNSIGNED");
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages')." MODIFY `entry_id` INT(11) UNSIGNED NULL DEFAULT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages')." ADD CONSTRAINT `fk_pages_entry_id` FOREIGN KEY(`entry_id`) REFERENCES ".$this->db->dbprefix('def_page_fields')."(`id`)");

				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '2.5.1';
				break;

			case '2.5.1':
				// ------------------------------------------
				// CREATE FK FOR `pages_translation`.`id_pages`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row
				$this->db->select('id');
				$existing_pages_ids = $this->db->get('pages')->result();

				$existing_pages_ids_arr = array();
				foreach ($existing_pages_ids as $existing_pages_id) {
					$existing_pages_ids_arr[] = (int)$existing_pages_id->id;
				}

				if(count($existing_pages_ids_arr) > 0){
					$this->db->where_not_in('id_pages', $existing_pages_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('pages_translation');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages')." MODIFY `id` INT(11) UNSIGNED");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages_translation')." ADD CONSTRAINT `fk_pages_trans_id_pages` FOREIGN KEY(`id_pages`) REFERENCES ".$this->db->dbprefix('pages')."(`id`)");

				// ------------------------------------------
				// CREATE FK FOR `def_page_fields_translation .`id_page_fields`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row
				$this->db->select('id');
				$existing_pages_ids = $this->db->get('def_page_fields')->result();

				$existing_pages_ids_arr = array();
				foreach ($existing_pages_ids as $existing_pages_id) {
					$existing_pages_ids_arr[] = (int)$existing_pages_id->id;
				}

				if(count($existing_pages_ids_arr) > 0){
					$this->db->where_not_in('id_page_fields', $existing_pages_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('def_page_fields_translation');

				// Step 3 - equalize child's column data type to parent's
				// no need

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('def_page_fields_translation')." ADD CONSTRAINT `fk_page_fields_trans_id_page_fields` FOREIGN KEY(`id_page_fields`) REFERENCES ".$this->db->dbprefix('def_page_fields')."(`id`)");

				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '2.5.2';
				break;

			case '2.5.2':
				// pages type
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages')." DROP foreign KEY `fk_pages_type_id`");

				$this->db->query("ALTER TABLE ".$this->db->dbprefix('page_types')." MODIFY `id` INT(11) UNSIGNED AUTO_INCREMENT");
				
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages')." ADD CONSTRAINT `fk_pages_type_id` FOREIGN KEY(`type_id`) REFERENCES ".$this->db->dbprefix('page_types')."(`id`)");

				// def_page_fields
				$constraint = $this->db->query("SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = '".$this->db->database."' AND REFERENCED_TABLE_NAME = '".$this->db->dbprefix('def_page_fields')."' AND TABLE_NAME = '".$this->db->dbprefix('pages')."'");
            
	            $blog_fk = $constraint->row_array();

	            if(isset($blog_fk['CONSTRAINT_NAME'])) {
	                $this->db->query("ALTER TABLE ".$this->db->dbprefix('pages')." DROP FOREIGN KEY `".$blog_fk['CONSTRAINT_NAME']."`");
	            }

				$this->db->query("ALTER TABLE ".$this->db->dbprefix('def_page_fields_translation')." DROP FOREIGN KEY `fk_page_fields_trans_id_page_fields`");

				$this->db->query("ALTER TABLE ".$this->db->dbprefix('def_page_fields')." MODIFY `id` INT(11) UNSIGNED AUTO_INCREMENT");

				$this->db->query("ALTER TABLE ".$this->db->dbprefix('def_page_fields_translation')." ADD CONSTRAINT `fk_page_fields_trans_id_page_fields` FOREIGN KEY(`id_page_fields`) REFERENCES ".$this->db->dbprefix('def_page_fields')."(`id`)");


				// pages_translation
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages_translation')." DROP FOREIGN KEY `fk_pages_trans_id_pages`");

				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages')." MODIFY `id` INT(11) UNSIGNED AUTO_INCREMENT");

				$this->db->query("ALTER TABLE ".$this->db->dbprefix('pages_translation')." ADD CONSTRAINT `fk_pages_trans_id_pages` FOREIGN KEY(`id_pages`) REFERENCES ".$this->db->dbprefix('pages')."(`id`)");

				$this->version = '2.5.3';
				break;

		}
		return true;
	}
}
