<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Location extends Module
{
    public $version = '1.1.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Location',
			'id' => 'Location',
		);
		$info['description'] = array(
			'en' => 'Manage data location of Indonesia (Province, Cities/District, Sub-District, and Villages)',
			'id' => 'Mengelola data lokasi di Indonesia (Provinsi, Kota/Kabupaten, kecamatan, dan kelurahan/Desa)',
		);
		$info['frontend'] = false;
		$info['backend'] = true;
		$info['menu'] = 'data';
		$info['roles'] = array('manage_location');
		
		// if(group_has_role('location', 'manage_location')){
		// 	$info['sections']['provinsi']['name'] = 'location:provinsi:plural';
		// 	$info['sections']['provinsi']['uri'] = 'admin/location/provinsi/index';
			
		// 	if(group_has_role('location', 'create_provinsi')){
		// 		$info['sections']['provinsi']['shortcuts']['create'] = array(
		// 			'name' => 'location:provinsi:new',
		// 			'uri' => 'admin/location/provinsi/create',
		// 			'class' => 'add'
		// 		);
		// 	}
		// }
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items){
		
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// provinsi
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'kode' => array(
				'type' => 'varchar',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'nama' => array(
				'type' => 'varchar',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'slug' => array(
				'type' => 'varchar',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('location_provinsi', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_location_provinsi(created_by)");


		// kota
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'kode' => array(
				'type' => 'varchar',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'nama' => array(
				'type' => 'varchar',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'slug' => array(
				'type' => 'varchar',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'id_provinsi' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('location_kota', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_location_kota(created_by)");


		// kecamatan
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'kode' => array(
				'type' => 'varchar',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'nama' => array(
				'type' => 'varchar',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'slug' => array(
				'type' => 'varchar',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'id_kota' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('location_kecamatan', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_location_kecamatan(created_by)");


		// kelurahan
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'kode' => array(
				'type' => 'varchar',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'nama' => array(
				'type' => 'varchar',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'slug' => array(
				'type' => 'varchar',
				'constraint' => '100',
				'default' => '',
				'null' => TRUE,
			),
			'id_kecamatan' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'created_on' => array(
				'type' => 'DATETIME',
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'updated_on' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'updated_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('location_kelurahan', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_location_kelurahan(created_by)");

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('location_provinsi');
        $this->dbforge->drop_table('location_kota');
        $this->dbforge->drop_table('location_kecamatan');
        $this->dbforge->drop_table('location_kelurahan');
        return true;
    }

    public function upgrade($old_version)
    {
    	switch ($old_version) {
    		case '1.0':
				// ------------------------------------------
				// CREATE FK FOR `location_kelurahan`.`id_kecamatan`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row

				// parent widget
				$this->db->select('id');
				$existing_ids = $this->db->get('location_kecamatan')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('id_kecamatan', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('location_kelurahan');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('location_kelurahan')." CHANGE COLUMN `id_kecamatan` `id_kecamatan` INT(11) UNSIGNED NOT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('location_kelurahan')." ADD FOREIGN KEY(`id_kecamatan`) REFERENCES ".$this->db->dbprefix('location_kecamatan')."(`id`)");

				// ------------------------------------------
				// CREATE FK FOR `location_kecamatan`.`id_kota`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row

				// parent widget
				$this->db->select('id');
				$existing_ids = $this->db->get('location_kota')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('id_kota', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('location_kecamatan');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('location_kecamatan')." CHANGE COLUMN `id_kota` `id_kota` INT(11) UNSIGNED NOT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('location_kecamatan')." ADD FOREIGN KEY(`id_kota`) REFERENCES ".$this->db->dbprefix('location_kota')."(`id`)");

				// ------------------------------------------
				// CREATE FK FOR `location_kota`.`id_provinsi`
				// ------------------------------------------

				// Step 1 - create index (if not yet an index)
				// no need

				// Step 2 - delete all child rows that have no match FK reference in parent row

				// parent widget
				$this->db->select('id');
				$existing_ids = $this->db->get('location_provinsi')->result();

				$existing_ids_arr = array();
				foreach ($existing_ids as $existing_id) {
					$existing_ids_arr[] = (int)$existing_id->id;
				}

				if(count($existing_ids_arr) > 0){
					$this->db->where_not_in('id_provinsi', $existing_ids_arr);
				}else{
					$this->db->where(true);
				}
				$this->db->delete('location_kota');

				// Step 3 - equalize child's column data type to parent's
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('location_kota')." CHANGE COLUMN `id_provinsi` `id_provinsi` INT(11) UNSIGNED NOT NULL");

				// Step 4 - create references
				$this->db->query("ALTER TABLE ".$this->db->dbprefix('location_kota')." ADD FOREIGN KEY(`id_provinsi`) REFERENCES ".$this->db->dbprefix('location_provinsi')."(`id`)");
				
				// force to stop at this version. to upgrade to higher version, do upgrade again from admin panel
				$this->version = '1.1.0';
				break;
    	}
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}