<?php defined('BASEPATH') OR exit('No direct script access allowed');

// labels
$lang['groups:title']					= 'Title';
$lang['groups:name']				    = 'Name';
$lang['groups:short_name']				= 'Short Name';
$lang['groups:force_fill_profile']				    = 'Force fill profile';
$lang['groups:enable_personel_link']				= 'Enable personel link';
$lang['groups:force_create_personel_link']			= 'Force create personel link';

// titles
$lang['groups:index_title']             = 'List Groups';
$lang['groups:add_title']               = 'Add Group';
$lang['groups:edit_title']              = 'Editing Group "%s"';

// messages
$lang['groups:no_groups']               = 'No groups found.';
$lang['groups:add_success']             = 'The group "%s" has been added.';
$lang['groups:add_error']               = 'The group "%s" could not be added.';
$lang['groups:edit_success']            = 'The group "%s" has been saved.';
$lang['groups:edit_error']              = 'The group "%s" could not be saved.';
$lang['groups:delete_success']          = 'The group was deleted successfully.';
$lang['groups:delete_error']            = 'There was an error deleting this group. You must delete all users associated with this group before deleting the group.';
$lang['groups:already_exist_error']     = 'A groups item with the name "%s" already exists.';
$lang['groups:move_success']          = 'The group was moved successfully';
$lang['groups:move_error']            = 'There was an error moving this group';
$lang['groups:move_up_error']            = 'There was an error moving this group. This is the first group';
$lang['groups:move_down_error']            = 'There was an error moving this group. This is the last group';

/* End of file group_lang.php */