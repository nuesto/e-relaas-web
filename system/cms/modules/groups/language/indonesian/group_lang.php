<?php defined('BASEPATH') OR exit('No direct script access allowed');

// labels
$lang['groups:title']					= 'Judul';
$lang['groups:name']				    = 'Nama';
$lang['groups:short_name']				= 'Nama Pendek';
$lang['groups:force_fill_profile']				    = 'Paksa untuk mengisi profile';
$lang['groups:enable_personel_link']				= 'Aktifkan personel link';
$lang['groups:force_create_personel_link']			= 'Paksa untuk membuat personel link';

// titles
$lang['groups:index_title']             = 'Daftar Grup';
$lang['groups:add_title']               = 'Tambah Grup';
$lang['groups:edit_title']              = 'Mengedit Grup "%s"';

// messages
$lang['groups:no_groups']               = 'Tidak ditemukan grup.';
$lang['groups:add_success']             = 'Grup "%s" sudah ditambahkan.';
$lang['groups:add_error']               = 'Grup "%s" tidak dapat ditambahkan.';
$lang['groups:edit_success']            = 'Grup "%s" telah disimpan.';
$lang['groups:edit_error']              = 'Grup "%s" tidak dapat disimpan.';
$lang['groups:delete_success']          = 'Grup berhasil dihapus.';
$lang['groups:delete_error']            = 'Terjadi kesalahan dalam penghapusan grup ini. Anda harus menghapus semua pengguna yang tergabungdengan grup ini sebelum menghapus grup.';
$lang['groups:already_exist_error']     = 'Grup dengan nama "%s" sudah ada.';
$lang['groups:move_success']          = 'Grup berhasil dipindahkan';
$lang['groups:move_error']            = 'Terjadi kesalahan dalam pemindahan grup ini';
$lang['groups:move_up_error']            = 'Terjadi kesalahan dalam pemindahan grup ini. Grup merupakan grup pertama';
$lang['groups:move_down_error']          = 'Terjadi kesalahan dalam pemindahan grup ini. Grup merupakan grup terakhir';

/* End of file group_lang.php */
