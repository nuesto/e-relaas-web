<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Group model
 *
 * @author		Phil Sturgeon
 * @author		PyroCMS Dev Team
 * @package		PyroCMS\Core\Modules\Groups\Models
 *
 */
class Group_m extends MY_Model
{
	/**
	 * Check a rule based on it's role
	 *
	 *
	 * @param string $role The role
	 * @param array $location
	 * @return mixed
	 */
	public function check_rule_by_role($role, $location)
	{
		// No more checking to do, admins win
		if ( $role == 1 )
		{
			return true;
		}

		// Check the rule based on whatever parts of the location we have
		if ( isset($location['module']) )
		{
			 $this->db->where('(module = "'.$location['module'].'" or module = "*")');
		}

		if ( isset($location['controller']) )
		{
			 $this->db->where('(controller = "'.$location['controller'].'" or controller = "*")');
		}

		if ( isset($location['method']) )
		{
			 $this->db->where('(method = "'.$location['method'].'" or method = "*")');
		}

		// Check which kind of user?
		$this->db->where('g.id', $role);

		$this->db->from('permissions p');
		$this->db->join('groups as g', 'g.id = p.group_id');

		$query = $this->db->get();

		return $query->num_rows() > 0;
	}

	/**
	 * Return an array of groups
	 *
	 *
	 * @param array $params Optional parameters
	 * @return array
	 */
	public function get_all($params = array())
	{
		if ( isset($params['except']) )
		{
			$this->db->where_not_in('name', $params['except']);
		}

		if ( isset($params['available_groups']) )
		{
			if ($params['available_groups'] == '') {
				$params['available_groups'] = '0';
			}
			$this->db->where('id IN ('.$params['available_groups'].')', NULL, FALSE);
		}

		$this->db->order_by('sort_order');

		return parent::get_all();
	}

	/**
	 * Return an object of group
	 *
	 *
	 * @param string $params
	 * @return object
	 */
	public function get_group_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->order_by('sort_order');

		return $this->db->get('groups')->row();
	}

	/**
	 * Return an object of group
	 *
	 *
	 * @param string $params
	 * @return object
	 */
	public function get_group_by_slug($slug)
	{
		$this->db->where('name', $slug);
		$this->db->order_by('sort_order');

		return $this->db->get('groups')->row();
	}

	/**
	 * Add a group
	 *
	 *
	 * @param array $input The data to insert
	 * @return array
	 */
	public function insert($input = array(), $skip_validation = false)
	{
		if(!isset($input['force_fill_profile'])) {
			$input['force_fill_profile'] = 0;
		}
		if(!isset($input['enable_personel_link'])) {
			$input['enable_personel_link'] = 0;
		}
		if(!isset($input['force_create_personel_link'])) {
			$input['force_create_personel_link'] = 0;
		}

		$this->db->select_max('sort_order');
		$res = $this->db->get('groups');

		$max_order = $res->row()->sort_order;

		return parent::insert(array(
			'name'			=> $input['name'],
			'description'	=> $input['description'],
			'sort_order'	=> $max_order+1,
			'force_fill_profile'	=> $input['force_fill_profile'],
			'enable_personel_link'	=> $input['enable_personel_link'],
			'force_create_personel_link'	=> $input['force_create_personel_link'],
		));
	}

	/**
	 * Update a group
	 *
	 *
	 * @param int $id The ID of the role
	 * @param array $input The data to update
	 * @return array
	 */
	public function update($id = 0, $input = array(), $skip_validation = false)
	{
		return parent::update($id, array(
			'name'			=> $input['name'],
			'description'	=> $input['description'],
			'force_fill_profile'	=> (isset($input['force_fill_profile'])) ? $input['force_fill_profile'] : 0,
			'enable_personel_link'	=> (isset($input['enable_personel_link'])) ? $input['enable_personel_link'] : 0,
			'force_create_personel_link'	=> (isset($input['force_create_personel_link'])) ? $input['force_create_personel_link'] : 0,
		));
	}

	/**
	 * Delete a group
	 *
	 *
	 * @param int $id The ID of the group to delete
	 * @return
	 */
	public function delete($id = 0, $order = 0)
	{
		$this->load->model('users/user_m');

		// don't delete the group if there are still users assigned to it
		if ($this->user_m->count_by(array('group_id' => $id)) > 0)
		{
			return false;
		}

		// Dont let them delete the "admin" group.
		// The interface does not have a delete button for these, this is just insurance
		$this->db->where_not_in('name', array('admin'));

		parent::delete($id);

		return $this->db->query('update '.$this->db->dbprefix('groups').' set `sort_order` = `sort_order`-1 where sort_order > '.$order);
	}

	function move_group($id, $order, $direction) {
		if($direction=='up') {
			$new_order = $order--;
		} elseif ($direction=='down') {
			$new_order = $order++;
		}

		$this->db->where('sort_order', $order);
		$this->db->update('groups', array('sort_order'=>$new_order));

		$this->db->where('id', $id);
		$this->db->update('groups', array('sort_order'=>$order));

		return true;
	}

	public function get_available_groups_by_units($unit_lists) {
		$this->db->select('available_groups');
		$this->db->where_in('organization_units.id', $unit_lists);

		$this->db->join('organization_units', 'organization_types.id=organization_units.unit_type');
		$res = $this->db->get('organization_types')->result_array();

		return $res;
	}
}
