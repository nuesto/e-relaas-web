<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('authorization_api')){
	function authorization_api($credential){
		$return = '';
		$return = true;
		return $return;
	}
}

if (!function_exists('delete_unused_field')){
  function delete_unused_field($array){
    $deleted = '';
    foreach ($array as $key => $value) {
      $deleted[$key] = $value;
      unset($deleted[$key]['created']);
      unset($deleted[$key]['updated']);
      unset($deleted[$key]['created_by']);
    }
    return $deleted;
  }
}


if (!function_exists('_output')){
  function _output($data, $status='200')
  {
    ci()->output
      ->enable_profiler(FALSE)
      ->set_status_header($status)
      ->set_content_type('application/json')
      ->set_output(json_encode($data, true));
  }
}