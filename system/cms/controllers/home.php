<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Home controller
 *
 * Description
 *
 */
class Home extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();

        redirect('admin');
    }
	
	public function index()
	{
		$this->template->title('Home')
			->set_breadcrumb('Home', '/')
			->set_layout('home.html')
			->build('home', array());
	}
}