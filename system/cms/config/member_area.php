<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Member Area
|--------------------------------------------------------------------------
|
| Define member area in the form of triplets <first>.<second>.<third>
|
| First: module name
| Second: controller name
| Third: method name
|
| if it applies to all controller or method, define with '*'
|
*/

$config['member_area'] = array(
	'dompet.*.*',
	'commerce.commerce_rekening.*',
	'commerce.commerce_order.my_order',
	'commerce.commerce_order.customer_info',
	'commerce.commerce_order.payment_method',
	'commerce.commerce_order.payment_choice',
	'users.users.*',
);

/*
|--------------------------------------------------------------------------
| Member Area Theme
|--------------------------------------------------------------------------
|
| member_area_theme: slug name of the theme
| member_area_theme_location: can have 2 options (shared_addons or default)
*/

$config['member_area_theme'] = 'unify_theme';
$config['member_area_theme_location'] = 'shared_addons';