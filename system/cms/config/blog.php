<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Empty-Organization
|--------------------------------------------------------------------------
|
| Allow a post does not have organization
|
|--------------------------------------------------------------------------*/
$config['with_organization'] = FALSE;
$config['allow_empty_organization']	= TRUE;