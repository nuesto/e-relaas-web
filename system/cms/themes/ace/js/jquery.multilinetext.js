/*
 * jQuery Multi Line Text Input
 * Version 0.0.2
 * http://github.com/staydecent/jquery-multilinetext
 *
 * Replaces a textarea with multiple text inputs, automatically 
 * appending a new input when the last is filled with content.
 *
 * Copyright (c) 2011 Adrian Unger (http://staydecent.ca)
 * Released under the MIT license.
*/

(function($) {

    $.fn.multilineText = function(options, callback) {

        var settings = {
            // Not sure what (if any) settings will be helpful
            'error-element': '#error_message'
        };

        var create_multilines = function(element) {
            /*
            Removes a textarea element, replacing it 
            with text inputs for each line.
            */
            var $this = $(element);

            var parent = $this.parent();
            var name = $this.attr('name');
            var text = $this.val();

            // Remove double newlines and create an array
            var lines = text.replace("\n\n", "\n").split("\n");
            // remove the textarea and inputs
            $this.remove();
            $('.' + name + '-mlText').remove();

            // variables for blank input
            var addInputButton = '<a class="mlText-add-input" href="#">Add input</a>';

            var firstBlankInput = '<div class="' + name + '-mlText">'
                + '<input type="text" class="" value="" name="' + name + '[]">'
                + '</div>';

            var blankInput = '<div class="' + name + '-mlText">'
                + '<input type="text" class="" value="" name="' + name + '[]">'
                + '&nbsp;<a href="#" class="mlText-remove-input">Remove</a>'
                + '</div>';

            // create a textfield for each line 
            var countContent = 0;
            for (var line in lines) {
                var value = $.trim(lines[line]);

                var appendedText = '<div class="' + name + '-mlText">'
                    + '<input type="text" class="" name="' + name + '[]" value="' + value + '">';
                if(countContent > 0){
                    appendedText += '&nbsp;<a href="#" class="mlText-remove-input">Remove</a>';
                }
                appendedText += '</div>';
                
                if (value !== "") {
                    parent.append(appendedText);
                    countContent++;
                }
            }

            // add blank input
            if(countContent == 0){
                parent.append(firstBlankInput);
            }
            parent.append(addInputButton);

            // add button
            $('div').on('click', '.mlText-add-input', function(event) {
                event.preventDefault();
                $(this).remove();
                parent.append(blankInput);
                parent.append(addInputButton);
            });

            // remove button
            $('div').on('click', '.mlText-remove-input', function(event) {
                event.preventDefault();
                $(this).parent().remove();
            });

            // ENTER keypress handling
            parent.on('keypress', '.' + name + '-mlText', function(event) {
                // ENTER keycode
                if(event.keyCode == 13){
                    event.preventDefault();
                    /* @todo
                    if ($(this).is(':last-child') && $(this).val() != '') {
                        // then clone the blank input
                        parent.append(blankInput);
                        $(this).next().focus();
                    }*/
                }
            });
        };

        return this.each(function() {
            if (options) {
                $.extend(settings, options);
            }

            var handler_set = false;

            // only operate on an element if it's a textarea
            if (this.tagName == "TEXTAREA") {
                create_multilines(this);
            }

            if (typeof callback == 'function') {
                callback.call(this); // brings the scope to the callback
            }
        });

    };

})(jQuery);