<div class="btn-group content-toolbar">
	
	<!-- SHORTCUT MENU (FROM SECTION) -->
	
	<?php if(isset($module_details['sections'][$active_section]['shortcuts'])){ ?>
	
	<?php foreach($module_details['sections'][$active_section]['shortcuts'] as $shortcut){ ?>
	
	<a class="btn btn-yellow btn-sm <?php echo (isset($shortcut['class'])) ? $shortcut['class'] : ''; ?>" href="<?php echo base_url() . $shortcut['uri'] . get_query_string(5) ?>" <?php echo (isset($shortcut['id'])) ? 'id="'.$shortcut['id'].'"' : ''; ?>>
		<?php if(isset($shortcut['class']) AND $shortcut['class'] == 'add'){ ?>
		<i class="icon-plus"></i>
		<?php } ?>
		
		<span class="no-text-shadow"><?php echo lang($shortcut['name']); ?></span>
	</a>
	
	<?php } //foreach ?>
	
	<?php } //if ?>
	
	<!-- SHORTCUT MENU (NOT FROM SECTION) -->
	
	<?php if(isset($module_details['shortcuts']) AND is_array($module_details['shortcuts'])){ ?>
	
	<?php foreach($module_details['shortcuts'] as $shortcut){ ?>
	
	<a class="btn btn-yellow btn-sm" href="<?php echo base_url() . $shortcut['uri'] . get_query_string(5) ?>">
		<?php if(isset($shortcut['class']) AND $shortcut['class'] == 'add'){ ?>
		<i class="icon-plus"></i>
		<?php } ?>
		
		<span class="no-text-shadow"><?php echo lang($shortcut['name']); ?></span>
	</a>
	
	<?php } //foreach ?>
	
	<?php } //if ?>
	
</div>