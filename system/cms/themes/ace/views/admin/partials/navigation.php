<?php  
$global_count_notif = 0;
?>

<ul class="nav nav-list">
	
	<li class="<?php if($this->uri->uri_string() == 'admin'){ ?>active<?php } ?>">
		<a href="<?php echo base_url(); ?>admin/">
			<i class="icon-dashboard"></i>
			<span class="menu-text"> <?php echo lang('global:dashboard'); ?> </span>
		</a>
	</li>
	
	<?php foreach ($menu_items as $key => $menu_item){ ?>
		
		<?php
		// skip these menu, won't displayed
		if($key == 'lang:cp:nav_profile'){
			continue;
		}
		
		// set default icon for core modules menu
		$icon_name = 'chevron-sign-right';
		if($key == 'lang:cp:nav_content'){
			$icon_name = 'edit';
		}elseif($key == 'lang:cp:nav_Gallery'){
			$icon_name = 'picture';
			if(count($menu_item)==1 and array_key_exists('Video', $menu_item)) {
				$icon_name = 'facetime-video';
			}
		}elseif($key == 'lang:cp:nav_structure'){
			$icon_name = 'list-alt';
		}elseif($key == 'lang:cp:nav_data'){
			$icon_name = 'list';
		}elseif($key == 'lang:cp:nav_users'){
			$icon_name = 'user';
		}elseif($key == 'lang:cp:nav_organization'){
			$icon_name = 'building';
		}elseif($key == 'lang:cp:nav_Human_Resource'){
			$icon_name = 'group';
		}elseif($key == 'lang:cp:nav_SMS Gateway API'){
			$icon_name = 'mobile-phone';
		}elseif($key == 'lang:cp:nav_addons'){
			$icon_name = 'plus';
		}elseif($key == 'lang:cp:nav_settings'){
			$icon_name = 'cog';
		}
		
		// set icon for addons modules menu here
		elseif($key == 'lang:cp:nav_Landing Page'){
			$icon_name = 'globe';
		}elseif($key == 'lang:cp:nav_Commerce'){
			$icon_name = 'shopping-cart';
		}elseif($key == 'lang:cp:nav_nasabah'){
			$icon_name = 'group';
		}elseif($key == 'lang:cp:nav_dpk'){
			$icon_name = 'money';
		}elseif($key == 'lang:cp:nav_produk'){
			$icon_name = 'book';
		}elseif($key == 'lang:cp:nav_kunjungan'){
			$icon_name = 'calendar';
		}
		
		?>

		<?php
		$urls = array();
		if(is_array($menu_item) AND array_key_exists('urls', $menu_item)) {
			$label = find_parent($menu_items, 'urls');
			$urls = $menu_item['urls'];
			$menu_item = array($label => $menu_item['urls'][0]);
			unset($menu_item['urls']);
			if(isset($menu_item['count_notif'])){
				$count_notif = $menu_item['count_notif'];
			}else{
				$count_notif = NULL;
			}
		}
		?>
		
		<!-- menu item is array and only has 1 sub menu -->
		<?php if(is_array($menu_item) AND count($menu_item) == 1){ ?>
			
			<!-- iterate the only 1 submenu -->
			<?php foreach($menu_item as $key_2 => $value_2){ ?>
				<?php
				if(is_array($value_2) AND count($value_2)==1 AND array_key_exists('urls', $value_2)) {
				
					$value_2[$key_2] = $value_2;
					unset($value_2['urls']);
				
				}else if(is_array($value_2) and count($value_2)>1 and $key_2!='urls'){ 
				
					$is_submenu_active = is_active_menu($value_2, $active_section); 
				?>

				<?php
				/*********************************************************************************
				 *	Menu Type A : Parent menu that has only 1 module under it
				 *********************************************************************************/
				?>
				<li class="<?php if($is_submenu_active === TRUE){ ?>active open<?php } ?>">
					<a href="<?php echo current_url(); ?>#" class="dropdown-toggle">
						<i class="icon-<?php echo $icon_name; ?>"></i>
						<span class="menu-text"> <?php echo lang_label($key); ?> </span>

						<?php 
						$count_notif_acc = count_notif_acc($value_2);
						if($count_notif_acc != NULL){ 
							echo '<span class="badge badge-danger">'.$count_notif_acc.'</span>';
							$global_count_notif += $count_notif_acc;
						}
						?>

						<b class="arrow icon-angle-down"></b>
					</a>
					
					<ul class="submenu">
					<?php foreach($value_2 as $key_3 => $value_3){ ?>
						<?php 
						if(is_array($value_3) AND array_key_exists('urls', $value_3)) {
							$label = find_parent($value_2, 'urls');
							$urls = $value_3['urls'];
							$value_3_first_url = $value_3['urls'][0];
						}

						$count_notif = NULL;
						if(isset($value_3['count_notif'])){
							$count_notif = $value_3['count_notif'];
						}
						?>

						<?php
						/*********************************************************************************
						 *	Menu Type A.1 : Sub menu / module's section / leaf menu
						 *********************************************************************************/
						?>
						<li class="<?php if(strpos($this->uri->uri_string(), $value_3_first_url) === 0 OR check_urls($urls, $this->uri->uri_string(),$value_3_first_url)){ ?>active open<?php } ?>">
							<a href="<?php echo base_url() . $value_3_first_url; ?>">
								<span class="menu-text"><?php echo lang_label($key_3); ?></span>
								<?php if($count_notif != NULL){ ?>
								<span class="badge badge-danger "><?php echo $count_notif; ?></span>
								<?php } ?>
							</a>
						</li>

					<?php } ?>
					</ul>
				</li>

				<?php }else{ ?>

					<?php
					/*********************************************************************************
					 *	Menu Type B : Parent menu that don't have sub menu (leaf menu)
					 *********************************************************************************/

					if(is_array($value_2) AND array_key_exists('urls', $value_2)) {
						$urls = $value_2['urls'];
						$value_2_first_url = $value_2['urls'][0];
					}else{
						$value_2_first_url = $value_2;
					}

					$count_notif = NULL;
					if(isset($value_2['count_notif'])){
						$count_notif = $value_2['count_notif'];
					}

					?>
					<li class="<?php if(strpos($this->uri->uri_string(), $value_2_first_url) === 0 OR check_urls($urls, $this->uri->uri_string(),$value_2_first_url)){ ?>active open<?php } ?>">
						<a href="<?php echo base_url() . $value_2_first_url; ?>">
							<i class="icon-<?php echo $icon_name; ?>"></i>
							<span class="menu-text"><?php echo lang_label($key); ?></span>

							<?php 
							if($count_notif != NULL){ 
								echo '<span class="badge badge-danger">'.$count_notif.'</span>';
								$global_count_notif += $count_notif;
							}
							?>
						</a>
					</li>

				<?php } ?>

			<?php } ?>

		<!-- menu item is array and has more than 1 module under it -->
		<?php }elseif (is_array($menu_item) AND count($menu_item) > 1) { ?>
			
			<?php $is_submenu_active = is_active_menu($menu_item, $active_section); ?>
			
			<?php
			/*********************************************************************************
			 *	Menu Type C : Parent menu that has more than 1 module under it
			 *********************************************************************************/
			?>
			<li class="<?php if($is_submenu_active === TRUE){ ?>active open<?php } ?>">
				<a href="<?php echo current_url(); ?>#" class="dropdown-toggle">
					<i class="icon-<?php echo $icon_name; ?>"></i>
					<span class="menu-text"> <?php echo lang_label($key); ?> </span>

					<?php 
					$count_notif_acc = count_notif_acc($menu_item);
					if($count_notif_acc != NULL){
						echo '<span class="badge badge-danger">'.$count_notif_acc.'</span>';
						$global_count_notif += $count_notif_acc;
					}
					?>

					<b class="arrow icon-angle-down"></b>
				</a>
				
				<ul class="submenu">
				
				<!-- iterate the all submenu -->
				<?php foreach ($menu_item as $lang_key => $uri) { ?>
					<?php
					if(is_array($uri) AND count($uri)==1 AND !array_key_exists('urls', $uri)) {
						$uri = reset($uri);
					}

					$urls2 = array();
					$count_notif = NULL;
					if(is_array($uri) AND array_key_exists('urls', $uri)) {
						$label = find_parent($menu_items, 'urls');
						$urls2 = $uri['urls'];
						if(isset($uri['count_notif'])){
							$count_notif = $uri['count_notif'];
						}
						$uri_first_url = $uri['urls'][0];
					}else{
						$uri_first_url = $uri;
					}
					?>

					<?php
					/*********************************************************************************
					 * Menu Type C.1 : Sub menu / module's section / leaf menu
					 * that exist under parent menu that has more than 1 module under it
					 *********************************************************************************/
					
					if(! is_array($uri_first_url)){
					?>
					<li class="<?php if(strpos($this->uri->uri_string(), $uri_first_url) === 0 OR check_urls($urls2, $this->uri->uri_string(),$uri_first_url) ){ ?>active open<?php } ?>">
					
						<a href="<?php echo site_url($uri_first_url); ?>">
							<i class="icon-double-angle-right"></i>
							<?php echo lang_label($lang_key); ?>
							<?php if($count_notif != NULL){ ?>
							<span class="badge badge-danger "><?php echo $count_notif; ?></span>
							<?php } ?>
						</a>
						
					</li>
					
					<?php 
					/***************************
					 *	Have submenu
					 ***************************/
					}else{ // if 
					?>
					
					<?php $is_section_active = is_active_menu($uri_first_url, $active_section); ?>
					
					<?php
					/*********************************************************************************
					 *	Menu Type C.2 : Sub menu (level 2) that has submenu (level 3)
					 *	this is a module menu
					 *********************************************************************************/
					?>
					<li class="<?php if($is_section_active){ ?>active open<?php } ?>">
					
					<a href="#" class="dropdown-toggle">
						<i class="icon-double-angle-right"></i>
						<?php echo lang_label($lang_key); ?>

						<?php if(count_notif_acc($uri_first_url) != NULL){ ?>
						<span class="badge badge-danger "><?php echo count_notif_acc($uri_first_url); ?></span>
						<?php } ?>

						<b class="arrow icon-angle-down"></b>
					</a>
					
					<ul class="submenu">
					
					<?php foreach($uri_first_url as $section_name => $section_uri){ ?>
						<?php
						if(is_array($section_uri) AND array_key_exists('urls', $section_uri)) {
							$link = $section_uri['urls'][0];
						} else {
							$link = $section_uri;
						}

						$count_notif = NULL;
						if(isset($section_uri['count_notif'])){
							$count_notif = $section_uri['count_notif'];
						}
						?>
						<?php $is_section_active = is_active_menu($link, $active_section); ?>

						<?php
						/*********************************************************************************
						 *	Menu Type C.2.1 : Sub menu / section menu / leaf menu in level 3
						 *********************************************************************************/
						?>
						<li class="<?php if($is_section_active){ ?>active<?php } ?>">
							<a href="<?php echo site_url($link); ?>">
								<i class="icon-chevron-sign-right"></i>
								<?php echo lang_label($section_name); ?>
								<?php if($count_notif != NULL){ ?>
								<span class="badge badge-danger "><?php echo $count_notif; ?></span>
								<?php } ?>
							</a>
						</li>
					
					<?php } // foreach ?>
					
					</ul>
					
					</li>
					
					<?php } // if ?>
					<?php unset($urls); ?>
				<?php } ?>

				</ul>
			</li>
		
		<!-- menu item is not an array -->
		<?php }else{ ?>
			<li class="<?php if(strpos($this->uri->uri_string(), $menu_item) === 0){ ?>active open<?php } ?>">
				<a href="<?php echo base_url() . $menu_item; ?>">
					<i class="icon-<?php echo $icon_name; ?>"></i>
					<span class="menu-text"><?php echo lang_label($key); ?></span>
				</a>
			</li>
			
		<?php } ?>
	<?php } ?>
	
</ul><!-- /.nav-list -->

<script type="text/javascript">
	$(document).ready(function(){
		var global_count_notif = <?php echo $global_count_notif; ?>;

		if(global_count_notif > 0){
			var title = $(document).prop('title');
			$(document).prop('title', '(' + global_count_notif + ') ' + title);
		}
	});
</script>