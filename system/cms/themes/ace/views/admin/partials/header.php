<div class="navbar navbar-default" id="navbar">
	<script type="text/javascript">
		try{ace.settings.check('navbar' , 'fixed')}catch(e){}
	</script>

	<div class="navbar-container" id="navbar-container">
		<div class="navbar-header pull-left">
			<a href="<?php echo base_url() . 'admin' ?>" class="navbar-brand">
				<small>
					<?php echo Asset::img('logo_alt.png', $this->settings->site_name, array(
						'style' => 'margin-top: -6px; max-height: 22px;', )) ?>
					<?php echo $this->settings->site_name; ?>
				</small>
			</a><!-- /.brand -->
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
		</div><!-- /.navbar-header -->

		<style type="text/css">
			@media (max-width: 767px) {
				#top-nav {
					display: table;
					width: 100%;
					border: 0;
				}
				#top-nav > li {
					display: table-cell;
					border: 0;
				}
			}
		</style>

		<div class="navbar-header pull-right" role="navigation">
			<ul id="top-nav" class="nav ace-nav">

				<li class="">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
						<?php if (isset($this->current_user->picture_id)) : ?>
							<img src="<?php echo base_url(); ?>files/thumb/<?php echo $this->current_user->picture_id; ?>/36/36/fit" class="nav-user-photo" />
						<?php else : ?>
							<?php echo Asset::img('default_avatar.png', $this->current_user->username, array('class' => 'nav-user-photo', 'width' => 36)) ?>
						<?php endif; ?>
						<span class="user-info">
							<?php echo $this->current_user->display_name; ?>
							<small>
								<?php 
								echo $this->current_user->group_description;

								$this->load->model('organization/memberships_m');
								$unit = $this->memberships_m->get_one_unit_by_member($this->current_user->id);
								if($unit != NULL){
									echo ', ';
									if($unit['abbrevation'] != ''){
										echo $unit['abbrevation'];
									}else{
										echo $unit['name'];
									}
								}
								?>
							</small>
						</span>

						<i class="icon-caret-down"></i>
					</a>

					<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<?php if(group_has_role('users', 'view_all_profile') OR group_has_role('users', 'view_own_profile')){ ?>
						<li>
							<a href="<?php echo base_url() . 'admin/users/view'; ?>">
								<i class="icon-user"></i>
								<?php echo lang_label('Profile'); ?>
							</a>
						</li>
						<?php } ?>

						<li class="divider"></li>

						<li>
							<a href="<?php echo base_url() . 'admin/logout'; ?>">
								<i class="icon-off"></i>
								<?php echo lang('logout_label'); ?>
							</a>
						</li>
					</ul>
				</li>
			</ul><!-- /.ace-nav -->
		</div><!-- /.navbar-header -->
	</div><!-- /.container -->
</div>