<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title><?php echo $template['title'].' - '.lang('cp:admin_title') ?></title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<link rel="icon" type="image/png" href="<?php echo base_url(); ?>system/cms/themes/ace/img/favicon.png" />

		<!-- -----------------
		STYLES
		------------------ -->

		<?php Asset::css('bootstrap.css'); ?>
		<?php Asset::css('font-awesome-3.2.1.min.css'); ?>
		<?php Asset::css('font-awesome.min.css'); ?>
		<?php Asset::css('ace-fonts.css'); ?>
		<?php Asset::css('ace.css'); ?>
		<?php Asset::css('chosen.css'); ?>
		<?php Asset::css('ace-rtl.min.css'); ?>
		<?php Asset::css('ace-skins.min.css'); ?>
		<?php Asset::css('customscroll.css'); ?>
		<?php echo Asset::render_css('global') ?>

		<!--[if IE 7]>
		<?php Asset::css('font-awesome-ie7-3.2.1.min.css', false, 'ie7_styles'); ?>
		<?php echo Asset::render_css('ie7_styles') ?>
		<![endif]-->

		<!--[if lte IE 8]>
		<?php Asset::css('ace-ie.min.css', false, 'ie8_styles'); ?>
		<?php echo Asset::render_css('ie8_styles') ?>
		<![endif]-->

		<!-- Inline style -->
		<style>
		.sidebar-fixed{
			overflow: y-scroll;
			bottom: 0;
		}
		</style>

		<!-- -----------------
		SCRIPTS
		------------------ -->

		<!--[if !IE]> -->
		<?php Asset::js('jquery-2.0.3.js', false, 'notie_scripts'); ?>
		<?php echo Asset::render_js('notie_scripts') ?>
		<!-- <![endif]-->

		<!--[if IE]>
		<?php Asset::js('jquery-1.10.2.js', false, 'ie_scripts'); ?>
		<?php echo Asset::render_js('ie_scripts') ?>
		<![endif]-->

		<?php Asset::js('jquery-ui-1.10.3.custom.min.js'); ?>
		<?php Asset::js('bootstrap.min.js'); ?>
		<?php Asset::js('typeahead-bs2.min.js'); ?>
		<?php Asset::js('jquery.customscroll.js'); ?>
		<?php Asset::js('ace-elements.js'); ?>
		<?php Asset::js('ace.min.js'); ?>
		<?php Asset::js('chosen.jquery.hacked.js'); ?>
		<?php Asset::js('ace-extra.js'); ?>
		<?php Asset::js('bootbox.min.js'); ?>
		<?php echo Asset::render_js('global') ?>

		<!--[if lt IE 9]>
		<?php Asset::js('html5shiv.js', false, 'ie9_scripts'); ?>
		<?php Asset::js('respond.min.js', false, 'ie9_scripts'); ?>
		<?php echo Asset::render_js('ie9_scripts') ?>
		<![endif]-->

		<!-- -----------------
		METADATA
		------------------ -->
		<?php file_partial('metadata'); ?>

		<!-- -----------------
		END OF METADATA
		------------------ -->

		<!-- inline scripts related to this page -->
		<script type="text/javascript">

			/****************************
			 *	TOUCH
			 ****************************/
			if("ontouchend" in document) document.write("<script src='<?php Asset::js('jquery.mobile.custom.min.js'); ?>'>"+"<"+"/script>");

			/****************************
			 *	CONFIRMATION DIALOG
			 ****************************/
			$(document).ready(function(){
				$('body').on('click', '.confirm', function(event) {
				   	event.preventDefault();
					var link = $(this).attr('href');;
					bootbox.confirm("<?php echo lang('global:dialog:confirm_message'); ?>", function(result) {
						if(result) {
							window.location.href = link;
						}
					}); 
				});
			});

			/****************************************************
			 *	Check all checkboxes in container table or grid
			 ****************************************************/
			$(document).ready(function(){
				$(".check-all").on('click', null, function () {
					var check_all		= $(this),
						all_checkbox	= $(this).is('.grid-check-all')
							? $(this).parents(".list-items").find(".grid input[type='checkbox']")
							: $(this).parents("table").find("tbody input[type='checkbox']");

					all_checkbox.each(function () {
						if (check_all.is(":checked") && ! $(this).is(':checked'))
						{
							$(this).click();
						}
						else if ( ! check_all.is(":checked") && $(this).is(':checked'))
						{
							$(this).click();
						}
					});

					// Check all?
					$(".table_action_buttons .btn").prop('disabled', false);
				});
			});

			/**********************************************
			 *	Table action buttons start out as disabled
			 **********************************************/
			$(document).ready(function(){
				$(".table_action_buttons .btn").prop('disabled', true);
			});

			/*****************************************
			 *	Enable/Disable table action buttons
			 *****************************************/
			$(document).ready(function(){
				$('input[name="action_to[]"], .check-all').on('click', null, function () {

					if( $('input[name="action_to[]"]:checked, .check-all:checked').length >= 1 ){
						$(".table_action_buttons .btn").prop('disabled', false);
					} else {
						$(".table_action_buttons .btn").prop('disabled', true);
					}
				});
			});

		</script>
	</head>

	<body>
		<?php file_partial('header'); ?>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">

				<div class="sidebar customscroll" id="sidebar" style="">

					<?php file_partial('navigation'); ?>

					<div class="sidebar-collapse hidden-xs" id="sidebar-collapse">
						<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
					</div>

					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					</script>
				</div>

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">

						<?php
						/********************************
						 * If no breadcrumbs override
						 ********************************/
						$breadcrumbs = $this->template->get_breadcrumbs();
						if(! isset($breadcrumbs) OR count($breadcrumbs) == 0){
						?>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<?php echo base_url() . 'admin'; ?>"><?php echo lang('breadcrumb_base_label'); ?></a>
							</li>

							<?php if($module_details AND isset($module_details['sections'][$active_section]) AND $this->uri->uri_string() == $module_details['sections'][$active_section]['uri']){ ?>

							<li>
								<a href="<?php echo base_url() . 'admin/' . $module_details['slug']; ?>"><?php echo $module_details['name']; ?></a>
							</li>
							<li class="active"><?php echo $template['title']; ?></li>

							<?php }elseif($module_details AND isset($module_details['sections'][$active_section])){ ?>

							<li>
								<a href="<?php echo base_url() . 'admin/' . $module_details['slug']; ?>"><?php echo $module_details['name']; ?></a>
							</li>
							<li>
								<?php $url = (is_array($module_details['sections'][$active_section]['uri'])) ? $module_details['sections'][$active_section]['uri']['urls'][0] : $module_details['sections'][$active_section]['uri']; ?>
								<a href="<?php echo base_url() . $url; ?>"><?php echo lang($module_details['sections'][$active_section]['name']); ?></a>
							</li>
							<li class="active"><?php echo $template['title']; ?></li>

							<?php }else{ ?>

							<li class="active"><?php echo $template['title']; ?></li>

							<?php } ?>

						</ul><!-- .breadcrumb -->

						<?php
						/********************************
						* If there is breadcrumbs override
						********************************/
						}else{
						?>

						<ul class="breadcrumb">

							<?php foreach($breadcrumbs as $breadcrumb){ ?>

							<?php
							if((isset($breadcrumb['name']) AND $breadcrumb['name'] != '') AND (isset($breadcrumb['uri']) AND $breadcrumb['uri'] != '')){
								echo '<li><a href="'.site_url($breadcrumb['uri']).'">'.$breadcrumb['name'].'</a></li>';
							}else{
								echo '<li class="active">'.$breadcrumb['name'].'</li>';
							}
							?>

							<?php } // foreach ?>

						</ul><!-- .breadcrumb -->

						<?php } // if ?>
					</div>

					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">

								<!-- PAGE CONTENT BEGINS -->

								<?php file_partial('notices'); ?>
								<?php echo $template['body']; ?>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

	</body>
</html>
