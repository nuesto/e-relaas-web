<!-- css module -->
<?php Asset::css('dashboard.css', false, 'dashboard'); ?>
<?php Asset::css('morris.css', false, 'dashboard'); ?>
<?php echo Asset::render_css('dashboard') ?>

<div class="main-header clearfix">
	<div class="page-header">
		<h1 class="no-margin">Dashboard</h1>
		<span><i class="icon-double-angle-right"></i> overview &amp; stats</span>
	</div><!-- /page-title -->
</div>

<div class="row">
	<div class="col-xs-12">

		<?php echo form_open(site_url('admin'), 'name="dashboard_period" method="get"'); ?>

		<?php 
			$value = NULL;
			if($this->input->post('year') != NULL){
				$value = $this->input->post('year');
			}elseif($this->input->get('f-year') != NULL){
				$value = $this->input->get('f-year');
			}else{
				$value = date('Y');
			}
		?>
		
		<select class="" name="f-year">
			<option value=""><?php echo lang('global:select-year'); ?></option>
			<?php 
				foreach ($all_years as $year_entry) {
					$selected = '';
					if($year_entry == $value) {
						$selected = 'selected="selected"';
					}
					echo '<option value="'.$year_entry.'" '.$selected.'>'.$year_entry.'</option>';
				}
			?>
		</select>

		<?php 
			$value = NULL;
			if($this->input->post('month') != NULL){
				$value = $this->input->post('month');
			}elseif($this->input->get('f-month') != NULL){
				$value = $this->input->get('f-month');
			}else{
				$value = date('m');
			}
		?>
		
		<select class="" name="f-month">
			<option value=""><?php echo lang('global:select-month'); ?></option>
			<?php 
				$all_months = array(NULL, 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
				unset($all_months[0]);
				foreach ($all_months as $month_index => $month_name) {
					$selected = '';
					if($month_index == $value) {
						$selected = 'selected="selected"';
					}
					echo '<option value="'.$month_index.'" '.$selected.'>'.$month_name.'</option>';
				}
			?>
		</select>

		<?php echo form_close(); ?>
	</div>
</div>

<div class="row statsbox-wrapper">
	{{ widgets:area slug="dashboard-stats" }}
</div><!-- /panel-stats -->

<div class="row">

	<div class="col-md-8">

		{{ widgets:area slug="dashboard-left" }}

	  <div class="row">

	  	<div class="col-md-6">
	  		{{ widgets:area slug="dashboard-middle-left" }}
	  	</div> <!-- /.col -->

	  	<div class="col-md-6">
	  		{{ widgets:area slug="dashboard-middle-right" }}
	  	</div> <!-- /.col -->

	  </div>

	</div> <!-- /.col -->

	<div class="col-md-4">
		{{ widgets:area slug="dashboard-right" }}
	</div><!-- /.col -->

</div> <!-- /.row -->

<div class="row">

	<div class="col-md-6">
		{{ widgets:area slug="dashboard-bottom-left" }}
	</div><!-- /.col -->

	<div class="col-md-6">
		{{ widgets:area slug="dashboard-bottom-right" }}
	</div><!-- /.col -->

</div> <!-- /.row -->

<!-- inline scripts related to this page -->
<?php Asset::js('jquery.slimscroll.js', false, 'dashboard'); ?>
<?php Asset::js('jquery.easy-pie-chart.min.js', false, 'dashboard'); ?>
<?php Asset::js('jquery.sparkline.min.js', false, 'dashboard'); ?>
<?php Asset::js('flot/jquery.flot.min.js', false, 'dashboard'); ?>
<?php Asset::js('flot/jquery.flot.pie.min.js', false, 'dashboard'); ?>
<?php Asset::js('flot/jquery.flot.resize.min.js', false, 'dashboard'); ?>
<?php Asset::js('raphael-min.js', false, 'dashboard'); ?>
<?php Asset::js('morris.min.js', false, 'dashboard'); ?>
<?php echo Asset::render_js('dashboard') ?>
<script src="https://code.highcharts.com/highcharts.src.js"></script>

<script type="text/javascript">

	/**
	 * Number.prototype.format(n, x, s, c)
	 *
	 * @param integer n: length of decimal
	 * @param integer x: length of whole part
	 * @param mixed   s: sections delimiter
	 * @param mixed   c: decimal delimiter
	 */
	Number.prototype.formatNum = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
	};

	// function to refresh statbox widget
	function statsRefresh ($statsbox) {
		var $graphic = null,
				$spinner = $('<i class="loading-icon fa fa-lg fa-3x fa-spinner fa-pulse"></i>'),
				$overlay = $('<div class="loading-overlay"></div>').append($spinner);

		// create overlay spinner
		$spinner.css({ top: ($statsbox.innerHeight()/2) - ($spinner.outerHeight()/2) - 10 });
		$statsbox.append($overlay);

		// request ajax data
		// return ajax should be formated like this
		// {"items":[1,8,5,15,12,5,12,8,15,10,14,5],"total":89536}
		$.ajax({
			url: $statsbox.data('source'),
			dataType: 'json',
			success: function (data) {
				// update total counter
				var $total = $statsbox.find('.datas-text > span');
				$total.text(data.total);
				$({numberValue: 0}).animate({numberValue: data.total}, {
					duration: 2500,
					easing: 'linear',
					step: function() {
						$total.text((Math.ceil(this.numberValue).formatNum(0, 3, '.')));
					}
				});

				// update data graphic
				$graphic = $statsbox.find('.sparkinline');
				if ($graphic.length > 0 ) {
					$graphic.html(data.items.join(','));
					$graphic.sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });
				}
			},
			complete: function () {
				$overlay.remove();
			}
		});
	}

	// function to refresh normal widget
	function widgetRefresh ($widget,param) {
		param = param || '';
		var $head = null,
				$html = null,
				$scripts = null,
				$content = $widget.find('.widget-content'),
				$spinner = $('<i class="loading-icon fa fa-lg fa-3x fa-spinner fa-pulse"></i>'),
				$overlay = $('<div class="loading-overlay"></div>').append($spinner);

		// create overlay spinner
		if ($content.length > 0) {
			$spinner.css({ top: ($content.innerHeight()/2) - ($spinner.outerHeight()/2) - 10 });
			$content.append($overlay);
		}
		var url = $widget.data('source');

		// request ajax view
		$.ajax({
			url: url+param,
			success: function (data) {
				$head = $widget.find('.widget-head');
				$head.nextAll().remove();

				// create object
				$html = $('<div class="widget-content" />').html(data);

				// find inline script
				$scripts = $html.find('script');

				// append html
				$html.find('script').remove();
				$head.after($html);

				// append scripts
				// backup if script not executed
				// -------------------------------
				if ($scripts.length > 0) {
					$scripts.each(function (i) {
						window.eval.call(window, '(function ($widget) {'+$(this).text()+'})')($widget);
					});
				}
			},
			error: function () {
				$head = $widget.find('.widget-head');
				$head.nextAll().remove();

				// create object
				$html = $('<div class="widget-content" />').html('<div class="widget-not-loaded">Widget not loaded correctly.. <br> <a class="btn btn-link widget-refresh">refresh here</a></div>');

				$head.after($html);
			},
			complete: function () {
				$overlay.remove();
			}
		});
	}

	$(document).ready(function(){
		$('select[name=f-month]').change(function(){
			$('form[name=dashboard_period]').submit();
		});
		$('select[name=f-year]').change(function(){
			$('form[name=dashboard_period]').submit();
		});
	});

	jQuery(function($) {

		$('.sparkinline').sparkline('html', { type: 'bar', height: 'auto', enableTagOptions: true });

		$('.statsbox')

		.each(function (e) {
			var $this = $(this);
			statsRefresh($this);
		})

		.on('click', '.stats-refresh', function (e) {
			e.preventDefault();
			var $this = $(this);
			statsRefresh($this.closest('.statsbox'));
		});

		// ---------------------------------------------

		$('.widget')

		.each(function (e) {
			var $this = $(this);
			widgetRefresh($this);
		})

		.on('click', '.widget-refresh', function (e) {
			e.preventDefault();
			var $this = $(this);
			widgetRefresh($this.closest('.widget'));
		})

		.on('click', '.widget-minimize', function (e) {
			e.preventDefault();
			var $widget = $(this).closest('.widget');
			$widget.find('.widget-content').slideToggle();
			$widget.find('.widget-tools > .widget-minimize > i')
							.toggleClass('fa-chevron-down', 'fa-chevron-up');
		});

	})
</script>