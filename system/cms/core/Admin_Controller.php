<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is the basis for the Admin class that is used throughout PyroCMS.
 * 
 * Code here is run before admin controllers
 * 
 * @copyright   Copyright (c) 2012, PyroCMS LLC
 * @package PyroCMS\Core\Controllers
 */
class Admin_Controller extends MY_Controller {

	/**
	 * Admin controllers can have sections, normally an arbitrary string
	 *
	 * @var string 
	 */
	protected $section = null;

	/**
	 * Load language, check flashdata, define https, load and setup the data 
	 * for the admin theme
	 */
	public function __construct()
	{
		parent::__construct();

		// Load the Language files ready for output
		$this->lang->load('admin');
		$this->lang->load('buttons');
		
		// Show error and exit if the user does not have sufficient permissions
		if ( ! self::_check_access())
		{
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect();
		}

		// If the setting is enabled redirect request to HTTPS
		if ($this->settings->admin_force_https and strtolower(substr(current_url(), 4, 1)) != 's')
		{
			redirect(str_replace('http:', 'https:', current_url()).'?session='.session_id());
		}

		$this->load->helper('admin_theme');
		
		ci()->admin_theme = $this->theme_m->get_admin();
		
		// Using a bad slug? Weak
		if (empty($this->admin_theme->slug))
		{
			show_error('This site has been set to use an admin theme that does not exist.');
		}

		// make a constant as this is used in a lot of places
		defined('ADMIN_THEME') or define('ADMIN_THEME', $this->admin_theme->slug);
			
		// Set the location of assets
		Asset::add_path('theme', $this->admin_theme->web_path.'/');
		Asset::set_path('theme');
		
		// grab the theme options if there are any
		ci()->theme_options = $this->pyrocache->model('theme_m', 'get_values_by', array(array('theme' => ADMIN_THEME)));
	
		// Active Admin Section (might be null, but who cares)
		$this->template->active_section = $this->section;
		
		Events::trigger('admin_controller');

		// -------------------------------------
		// Build Admin Navigation
		// -------------------------------------
		// We'll get all of the backend modules
		// from the DB and run their module items.
		// -------------------------------------

		if (is_logged_in())
		{
			// get all modules that have backend side
			$modules = $this->module_m->get_all(array(
				'is_backend' => true,
			));

			// Here's our menu array.
			$menu_items = array();

			// iterate all loaded modules
			$module_names = array();
			foreach ($modules as $module)
			{				
				// store as vocabulary
				$module_names[$module['slug']] = $module['name'];

				// Load all module's lang
				$current_lang = $this->config->config['language'];
				$file_lang_arr = array();
				foreach(glob($module['path'].'/language/'.$current_lang.'/*.*') as $filepath){
					$file = substr($filepath, strrpos($filepath, '/') + 1);
					$file_lang = explode('_lang.php', $file);
					$file_lang_arr[] = $module['slug'].'/'.$file_lang[0];
				}
				$this->load->language($file_lang_arr);
				
				// we use the regular way of checking out the details.php data.
				// except the current user is admin, we will only process the modules that have
				// menu definition or the current user has permission to access the module
				if ($this->current_user->group == 'admin' OR ($module['menu'] and (isset($this->permissions[$module['slug']]))))
				{
					// Legacy module routing. This is just a rough
					// re-route and modules should change using their 
					// upgrade() details.php functions.
					if ($module['menu'] == 'utilities') $module['menu'] = 'data';
					if ($module['menu'] == 'design') $module['menu'] = 'structure';

					// load sections
					if(isset($module['sections']) AND is_array($module['sections']) AND count($module['sections']) > 1 AND $module != NULL){
						foreach($module['sections'] as $section_slug => $section_info){
							$menu_items['lang:cp:nav_'.$module['menu']]['module_slug:'.$module['slug']]['lang:'.$section_info['name']] = $section_info['uri'];
							
							// if 'count_notif' is defined in details.php
							if(isset($section_info['count_notif'])){
								// count notif value is get via call to library function
								// the module, library, and function is defined in the details.php
								// and the library and function must be implemented in the module
								$module_name = $section_info['count_notif']['module'];
								$library_name = $section_info['count_notif']['library'];
								$method_name = $section_info['count_notif']['method'];

								// load the library and call function
								$this->load->library($module_name.'/'.$library_name);
								$count_notif = $this->{$library_name}->{$method_name}();
								
								// set the value
								$menu_items['lang:cp:nav_'.$module['menu']]['module_slug:'.$module['slug']]['lang:'.$section_info['name']]['count_notif'] = $count_notif;
							}
						}
					}else{
						$menu_items['lang:cp:nav_'.$module['menu']]['module_slug:'.$module['slug']] = 'admin/'.$module['slug'];
					}

					// If a module has an admin_menu function, then 
					// we simply run that and allow it to manipulate the 
					// menu array. 
					if (method_exists($module['module'], 'admin_menu')) 
					{ 
						$module['module']->admin_menu($menu_items);
					}
				}
			}

			// convert to module slug to module name
			foreach ($menu_items as $module_menu_parent => $module_menu_tree) {
				foreach ($module_menu_tree as $module_menu_tree_key => $module_menu_tree_value) {
					$replacable_slug = explode(":", $module_menu_tree_key);
					if(isset($replacable_slug[0]) 
						AND $replacable_slug[0] == 'module_slug' 
						AND isset($replacable_slug[1]) 
						AND isset($module_names[$replacable_slug[1]])){

						$menu_items[$module_menu_parent][$module_names[$replacable_slug[1]]] = $module_menu_tree_value;
						unset($menu_items[$module_menu_parent][$module_menu_tree_key]);
					}
				}
			}

			// We always have our 
			// edit profile links and such.
			$menu_items['lang:cp:nav_profile'] = array(
				'lang:cp:edit_profile_label'		=> 'edit-profile',
				'lang:cp:logout_label'				=> 'admin/logout'
			);

			// initialize array that will hold menu in ordered fashion
			$ordered_menu = array();
			
			// load the module menu order configuration
			$this->template->menu_order = $this->config->item('menu_order');
			// iterate the menu order config
			foreach ($this->template->menu_order as $order)
			{
				if (isset($menu_items[$order]))
				{
					// if founded in menu_items, it will be added to ordered_menu array
					$ordered_menu[$order] = $menu_items[$order];
					// and removed from menu_items array
					unset($menu_items[$order]);
				}
			}

			// check if there's any items left in menu_items array
			$ordered_menu = array_merge_recursive($ordered_menu, $menu_items);
			
			// And there we go! These are the admin menu items.
			$this->template->menu_items = $ordered_menu;
			

			$this->_check_group_setting();
		}

		// ------------------------------
		
		// Template configuration
		$this->template
			->enable_parser(false)
			->set('theme_options', $this->theme_options)
			->set_theme(ADMIN_THEME)
			->set_layout('default', 'admin');

		// trigger the run() method in the selected admin theme
		$class = 'Theme_'.ucfirst($this->admin_theme->slug);
		call_user_func(array(new $class, 'run'));
	}

	/**
	 * Checks to see if a user object has access rights to the admin area.
	 *
	 * @return boolean 
	 */
	private function _check_access()
	{
		// These pages get past permission checks
		$ignored_pages = array('admin/login', 'admin/logout', 'admin/help');

		// Check if the current page is to be ignored
		$current_page = $this->uri->segment(1, '') . '/' . $this->uri->segment(2, 'index');

		// Dont need to log in, this is an open page
		if (in_array($current_page, $ignored_pages))
		{
			return true;
		}

		if ( ! $this->current_user)
		{
			// save the location they were trying to get to
			redirect('admin/login?redirect='.$this->uri->uri_string());
		}

		// Admins can go straight in
		if ($this->current_user->group === 'admin')
		{
			return true;
		}

		// Well they at least better have permissions!
		if ($this->current_user)
		{
			// We are looking at the index page. Show it if they have ANY admin access at all
			if ($current_page == 'admin/index' && $this->permissions)
			{
				return true;
			}

			// Check if the current user can view that page
			return array_key_exists($this->module, $this->permissions);
		}

		// god knows what this is... erm...
		return false;
	}

	function _check_group_setting() {
		if ($this->db->field_exists('force_fill_profile', 'groups') AND $this->db->field_exists('force_create_personel_link', 'groups') AND $this->db->field_exists('enable_personel_link', 'groups')) {
			$this->load->helper('groups/groups');
			
			$this->load->model('users/user_m');
			$this->load->model('users/profile_m');		
			$this->load->model('human_resource/personel_m');

			// get current user's groups and profile
			$groups = get_setting_by_group($this->current_user->group_id);
			$profile = $this->profile_m->get_profile(array('user_id'=>$this->current_user->id));

			// check if current user's group intended to force to fill/complete their profile
			if($groups->force_fill_profile==1) {

				// check if current user's profile is exist but not completed yet
				if(isset($profile->is_complete) AND $profile->is_complete!=1) {

					// will not redirect if going to forced edit profile or logout page
					$exception_uri = array('admin/users/edit/'.$this->current_user->id.'/forced','admin/logout');
					if(!in_array($this->uri->uri_string(), $exception_uri)) {
						redirect('admin/users/edit/'.$this->current_user->id.'/forced');
					}
				}
			}

			// this IF statement should pass the condition if meet one of the following conditions:
			// 1. current user's group intended to force to create personel object
			// and current user's profile is exist and completed
			// 2. current user's group intended to force to create personel object
			// but not intented to force user to fill their profile
			if(($groups->force_create_personel_link==1 AND (isset($profile->is_complete) AND $profile->is_complete==1)) 
				OR ($groups->force_create_personel_link==1 AND $groups->force_fill_profile==0)) {

				// get current user's personnel data
				$personel = $this->personel_m->get_personel_by_account($this->current_user->id);

				// will not redirect if going to following urls:
				// 1. forced personnel create page
				// 2. forced personnel edit page
				// 3. logout
				$exception_uri = array('admin/human_resource/personel/create/forced','admin/human_resource/personel/edit/'.$personel['id'].'/forced','admin/logout');
				if(!in_array($this->uri->uri_string(), $exception_uri)) {

					// @TODO explain this
					if(!preg_match('/(get_)/', $this->uri->segment('4'))) {
						
						// IF statement should pass if current user's personel object is not yet created
						if(!isset($personel)) {
							redirect('admin/human_resource/personel/create/forced');

						// ELSE IF statement should pass if current user 's personnel object is already created
						// but the data is not completed yet
						} elseif(isset($personel) AND $personel['is_complete']!=1) {
							redirect('admin/human_resource/personel/edit/'.$personel['id'].'/forced');
						}
					}
				}
			}
		}
	}

}