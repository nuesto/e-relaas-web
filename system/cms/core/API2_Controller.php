<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Code here is run before frontend controllers
 *
 * @author      qSmart Team
 * @copyright   Copyright (c) 2015, PT. Nuesto Technology Indonesia
 * @package			PyroCMS\Core\Controllers
 */
class API2_Controller extends Public_Controller
{
		/**
		* Public controllers can have sections, normally an arbitrary string
		*
		* @var string
		*/

		/**
		* Loads the gazillion of stuff, in Flash Gordon speed.
		* @todo Document properly please.
		*/
	public function __construct()
	{
		parent::__construct();
    	$this->load->model(array('users/user_m', 'permissions/permission_m'));
    	$this->load->helper(array('api_auth','users/user'));
		header('Access-Control-Allow-Origin: *');

	}

	protected function authorize_api_access(){

	    $has_access = false;
	    if($this->input->get('metod')){
	      	$method = $this->input->get('metod');
		    $data = $this->input->$method();
		    $api = array();
			if(isset($data['api_user']) && isset($data['api_key'])){
				//pemeriksaan hak akses berdasarkan api key dan username
				$api = array('user'=>$data['api_user'], 'key'=>$data['api_key']);
				$has_access = api_has_role($api);
			}
	    }

		if($has_access==false){
			header('Content-Type: application/json');
			header("HTTP Error 401 Unauthorized");
			die(json_encode(array("status"=>"error","error"=>"You don't have access to this page.")));
		}else{
			return api_get_user($api);
		}
	}
}
