# Pyro Base

## Description

Pyro Base is edited based on PyroCMS Community Edition. PyroCMS is a CMS built using the CodeIgniter PHP framework. Using an MVC architecture
it was built with modularity in mind. Lightweight, themeable and dynamic.

PyroCMS Community Edition is free to use, redistribute and/or modify for any purpose whether personal or commercial however you must retain the copyright in your source code and in the footer of the administration.

## Installation

Run http://example.com/installer to run the web-based installation script.